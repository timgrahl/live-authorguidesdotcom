<?php

$classmap = array(
    'EDD_SL_Plugin_Updater'                     => 'vendor/edd/EDD_SL_Plugin_Updater.php',
    'ZippyCourses_PluginUpdater'                => 'app/Updater/Updater.php',
    
    // Installer
    'ZippyCourses_Installer'                    => 'app/Installer/Installer.php',

    // Inversion of Control container
    'Zippy_API'                                 => 'lib/API/API.php',

    // Inversion of Control container
    'Zippy_IoC'                                 => 'lib/IoC/IoC.php',

    // Routes
    'ZippyCourses_Routes'                       => 'app/Routes/Routes.php',
    
    // Database
    'ZippyCourses_Database'                     => 'app/Database/Database.php',
    'Zippy_DatabaseQuery'                       => 'lib/Database/Query.php',
    'Zippy_DatabaseQueries'                     => 'lib/Database/Queries.php',
    'Zippy_DatabaseTable'                       => 'lib/Database/Table.php',
    'Zippy_DatabaseTables'                      => 'lib/Database/Tables.php',
    'Zippy_DatabaseTableColumn'                 => 'lib/Database/Column.php',
    'Zippy_DatabaseTableColumns'                => 'lib/Database/Columns.php',

    // Helpers
    'Zippy_View'                                => 'lib/Views/View.php',
    'Zippy_Repository'                          => 'lib/Helpers/Repositories/Repository.php',
    'Zippy_RepositoryView'                      => 'lib/Views/RepositoryView.php',
    'Zippy_RepositoryObject'                    => 'lib/Helpers/Repositories/RepositoryObject.php',
    'Zippy_Utilities'                           => 'lib/Helpers/Utilities/Utilities.php',

    'Zippy_Cache'                               => 'lib/Helpers/Cache.php',
    'ZippyCourses_Cache'                        => 'app/Cache/Cache.php',
    'Zippy_Log'                                 => 'lib/Log/Log.php',
    'Zippy_Activity'                            => 'lib/Activity/Activity.php',
    'ZippyCourses_Analytics'                    => 'app/Analytics/Analytics.php',
    'Zippy_Mailer'                              => 'lib/Mailer/Mailer.php',
    'Zippy_Email'                               => 'lib/Mailer/Email.php',
    'ZippyCourses_CorePages'                    => 'app/CorePages/CorePages.php',
    
    // Models
    'Zippy_Model'                               => 'lib/Models/Model.php',
    'Zippy_Widget_Model'                        => 'lib/Models/Widget/Widget.php',
    'Zippy_Course'                              => 'lib/Models/Course/Course.php',
    'Zippy_Courses'                             => 'lib/Models/Course/Courses.php',
    'Zippy_CourseConfig'                        => 'lib/Models/Course/Config/Config.php',
    'Zippy_CourseSchedulingConfig'              => 'lib/Models/Course/Config/Scheduling.php',
    'Zippy_CourseAccessConfig'                  => 'lib/Models/Course/Config/Access.php',
    'Zippy_CourseCertificate'                  => 'lib/Models/Course/Certificate/Certificate.php',

    'Zippy_Entry'                               => 'lib/Models/Entry/Entry.php',
    'Zippy_Entries'                             => 'lib/Models/Entry/Entries.php',
    'Zippy_Forum'                               => 'lib/Models/Forum/Forum.php',

    // Integrations
    'ZippyCourses_ForumIntegrations'            => 'app/Forums/Integrations.php',
    'Zippy_ForumIntegration'                    => 'lib/Integrations/Forum.php',
    
    'Zippy_Quiz'                                => 'lib/Models/Quiz/Quiz.php',

    // Access
    'Zippy_Access'                              => 'lib/Access/Access.php',

    // Apps
    'ZippyCourses'                              => 'app/ZippyCourses.php',
    'Zippy'                                     => 'lib/Zippy.php',

    // File Streams
    'Zippy_FileStream'                          => 'lib/FileStreams/FileStream.php',
    'Zippy_Curl_FileStream'                     => 'lib/FileStreams/Curl.php',
    'Zippy_Socket_FileStream'                   => 'lib/FileStreams/Socket.php',
    'Zippy_DirectDownload_FileStream'           => 'lib/FileStreams/DirectDownload.php',

    'ZippyCourses_SecureDownload'               => 'app/Files/Download.php',

    // Sessions
    'Zippy_Sessions'                            => 'lib/Sessions/Sessions.php',

    // Charts
    'Zippy_Chart'                               => 'lib/Charts/Chart.php',
    'ZippyCourses_StudentActivity_Chart'        => 'app/Analytics/Charts/StudentActivity.php',
    'ZippyCourses_CourseActivity_Chart'         => 'app/Analytics/Charts/CourseActivity.php',
    'ZippyCourses_CourseJoin_Chart'             => 'app/Analytics/Charts/CourseJoin.php',
    'ZippyCourses_Order_Chart'                  => 'app/Analytics/Charts/Order.php',
    'ZippyCourses_Registration_Chart'           => 'app/Analytics/Charts/Registration.php',

    // Analytics
    'ZippyCourses_Remote_Analytics'             => 'app/Analytics/Remote.php',

    // Data Table
    'Zippy_DataTable'                           => 'lib/Charts/DataTable.php',
    'Zippy_Dataset'                             => 'lib/Analytics/Dataset.php',
    'ZippyCourses_CourseRecentViews_Dataset'    => 'app/Analytics/Datasets/CourseRecentViews.php',
    'ZippyCourses_CourseAllViews_Dataset'       => 'app/Analytics/Datasets/CourseAllViews.php',
    'ZippyCourses_StudentViews_DataTable'       => 'app/Analytics/DataTables/StudentViews.php',
    'ZippyCourses_StudentAllViews_Dataset'      => 'app/Analytics/Datasets/StudentAllViews.php',
    'ZippyCourses_StudentRecentViews_Dataset'   => 'app/Analytics/Datasets/StudentRecentViews.php',
    'ZippyCourses_StudentDownloads_Dataset'     => 'app/Analytics/Datasets/StudentDownloads.php',
    'ZippyCourses_StudentCompletion_Dataset'  => 'app/Analytics/Datasets/StudentCompletion.php',
    'ZippyCourses_StudentViews_DataTable'       => 'app/Analytics/DataTables/StudentViews.php',
    'ZippyCourses_StudentLessonsCompleted_Dataset'     => 'app/Analytics/Datasets/StudentLessonsCompleted.php',

    // Notices
    'ZippyCourses_Alerts'                      => 'app/Notices/Notices.php',
    'ZippyCourses_AdminNotices'                 => 'app/UX/Admin/Notices.php',
    'Zippy_AdminNotice'                         => 'lib/UX/AdminNotices/AdminNotice.php',
    'Zippy_AdminNotices'                        => 'lib/UX/AdminNotices/AdminNotices.php',

    // Form Resources
    'Zippy_Form'                                => 'lib/Forms/Form.php',
    'Zippy_OrderForm'                           => 'lib/Forms/OrderForm.php',
    'Zippy_Forms'                               => 'lib/Forms/Forms.php',
    'Zippy_FormView'                            => 'lib/Views/FormView.php',
    'Zippy_FormFieldView'                       => 'lib/Views/FieldView.php',
    'Zippy_FieldsRepository'                    => 'lib/Forms/Fields/FieldsRepository.php',
    'Zippy_FormField'                           => 'lib/Forms/Fields/Field.php',

    // Forms
    'ZippyCourses_Forms'                        => 'app/Forms/Forms.php',
    
    // Form Processors
    'Zippy_FormProcessor'                       => 'lib/FormProcessors/FormProcessor.php',

    // Shortcodes
    'ZippyCourses_Shortcodes'                   => 'app/Shortcodes/Shortcodes.php',
    'Zippy_Shortcode'                           => 'lib/Shortcodes/Shortcode.php',
    'Zippy_ShortcodeView'                       => 'lib/Shortcodes/ShortcodeView.php',

    // Validator
    'Zippy_Validator'                           => 'lib/Validators/Validator.php',
    
    // Admin Pages
    'Zippy_AdminPage'                           => 'lib/UX/AdminPages/AdminPage.php',
    'Zippy_AdminPages'                          => 'lib/UX/AdminPages/AdminPages.php',
    'ZippyCourses_AdminPages'                   => 'app/UX/Admin/AdminPages.php',
    
    // Admin UX
    'ZippyCourses_AdminUserExperience'          => 'app/UX/Admin/UserExperience.php',
    'ZippyCourses_AdminNavWalker'               => 'app/UX/Admin/NavWalker.php',

    // List Tables
    'Zippy_ListTable'                           => 'lib/UX/ListTables/ListTable.php',
    'ZippyCourses_Students_ListTable'           => 'app/UX/ListTables/Students.php',
    'ZippyCourses_QuizResults_ListTable'        => 'app/UX/ListTables/QuizResults.php',
    'Zippy_ListTableColumns'                    => 'lib/UX/ListTables/TableColumns.php',
    'Zippy_ListTableColumn'                     => 'lib/UX/ListTables/TableColumn.php',
    'Zippy_ListTableFilter'                     => 'lib/UX/ListTables/Filter.php',
    'Zippy_ListTableFilters'                    => 'lib/UX/ListTables/Filters.php',
    
    // Settings
    'Zippy_SettingsSections'                    => 'lib/Settings/SettingsSections.php',
    'Zippy_SettingsSection'                     => 'lib/Settings/SettingsSection.php',
    'Zippy_SettingsField'                       => 'lib/Settings/SettingsField.php',
    'Zippy_SettingsPage'                        => 'lib/Settings/SettingsPage.php',
    'Zippy_SettingsPages'                       => 'lib/Settings/SettingsPages.php',
    'ZippyCourses_Settings'                     => 'app/Settings/Settings.php',

    // Post Types
    'Zippy_PostType'                            => 'lib/PostTypes/PostType.php',
    'Zippy_PostTypes'                           => 'lib/PostTypes/PostTypes.php',
    'ZippyCourses_PostTypes'                    => 'app/PostTypes/PostTypes.php',

    // Tabs
    'Zippy_Tabs'                                => 'lib/UX/Tabs/Tabs.php',
    'Zippy_TabsView'                            => 'lib/Views/TabsView.php',
    'Zippy_TabView'                             => 'lib/Views/TabView.php',
    'Zippy_SettingsTab'                         => 'lib/UX/Tabs/SettingsTab.php',
    'Zippy_SettingsTabs'                        => 'lib/UX/Tabs/SettingsTabs.php',
    'Zippy_SettingsTabsView'                    => 'lib/Views/SettingsTabsView.php',
    'Zippy_SettingsTabView'                     => 'lib/Views/SettingsTabView.php',

    'Zippy_Pages_Tab'                           => 'lib/UX/Tabs/PagesTab.php',
    'Zippy_Pages_Tabs'                          => 'lib/UX/Tabs/PagesTabs.php',
    'Zippy_Pages_TabsView'                      => 'lib/Views/PagesTabsView.php',
    'Zippy_Pages_TabView'                       => 'lib/Views/PagesTabView.php',
    'Zippy_Tab'                                 => 'lib/UX/Tabs/Tab.php',
    'Zippy_PostTypeTab'                         => 'lib/UX/Tabs/PostTypeTab.php',
    'ZippyCourses_PostTypeTabs'                 => 'app/UX/Tabs/PostTypeTabs.php',

    // Panels
    'Zippy_Panels'                              => 'lib/UX/Panels/Panels.php',
    'Zippy_PanelsView'                          => 'lib/Views/PanelsView.php',
    'Zippy_PanelView'                           => 'lib/Views/PanelView.php',
    'Zippy_SettingsPanel'                       => 'lib/UX/Panels/SettingsPanel.php',
    'Zippy_SettingsPanels'                      => 'lib/UX/Panels/SettingsPanels.php',
    'Zippy_SettingsPanelsView'                  => 'lib/Views/SettingsPanelsView.php',
    'Zippy_SettingsPanelView'                   => 'lib/Views/SettingsPanelView.php',
    'Zippy_Panel'                               => 'lib/UX/Panels/Panel.php',
    'Zippy_Panels'                              => 'lib/UX/Panels/Panels.php',
    'Zippy_PostTypePanel'                       => 'lib/UX/Panels/PostTypePanel.php',
    'ZippyCourses_PostTypePanels'               => 'app/UX/Panels/PostTypePanels.php',

    // Metaboxes
    'ZippyCourses_MetaboxRegions'               => 'app/Metaboxes/MetaboxRegions.php',
    'ZippyCourses_Metaboxes'                    => 'app/Metaboxes/Metaboxes.php',
    'Zippy_Metabox'                             => 'lib/Metaboxes/Metabox.php',
    'Zippy_Metaboxes'                           => 'lib/Metaboxes/Metaboxes.php',
    'Zippy_MetaboxRegion'                       => 'lib/Metaboxes/MetaboxRegion.php',
    'Zippy_MetaboxRegionSection'                => 'lib/Metaboxes/MetaboxRegionSection.php',

    // Views
    'Zippy_FileView'                            => 'lib/Views/FileView.php',

    // Events
    'Zippy_Dispatcher'                          => 'lib/Helpers/Events/Dispatcher.php',
    'Zippy_Listener'                            => 'lib/Helpers/Events/Listener.php',
    'Zippy_Event'                               => 'lib/Helpers/Events/Event.php',

    // Users
    'ZippyCourses_Student'                      => 'app/Users/Student.php',
    'ZippyCourses_Students'                     => 'app/Users/Students.php',
    'Zippy_User'                                => 'lib/Users/User.php',
    'Zippy_Users'                               => 'lib/Users/Users.php',
    'Zippy_Customer'                            => 'lib/Users/Customer.php',
    'ZippyCourses_NewStudentListener'           => 'app/Listeners/NewStudent.php',
    
    // Structs
    'Zippy_MetaboxData'                         => 'lib/Metaboxes/MetaboxData.php',
    'Zippy_MetaboxChildData'                    => 'lib/Metaboxes/MetaboxChildData.php',

    // Middleware
    'Zippy_Middleware'                          => 'lib/Middleware/Middleware.php',
    'Zippy_Middlewares'                         => 'lib/Middleware/Middlewares.php',
    'Zippy_MiddlewareRule'                      => 'lib/Middleware/Rule.php',
    'Zippy_MiddlewareRules'                     => 'lib/Middleware/Rules.php',
    'ZippyCourses_Middleware'                  => 'app/Middleware/Middleware.php',

    // Email Lists
    'Zippy_EmailListIntegration'                => 'lib/Integrations/EmailListIntegration.php',
    'Zippy_EmailListIntegrationRepository'      => 'lib/Integrations/EmailListIntegrationRepository.php',
    
    'Zippy_EmailList'                           => 'lib/Integrations/EmailList.php',
    'Zippy_EmailListRepository'                 => 'lib/Integrations/EmailListRepository.php',
    'Zippy_EmailListIntegrationAPI'             => 'lib/Integrations/EmailListIntegrationAPI.php',
    'ZippyCourses_EmailListIntegrations'        => 'app/Integrations/Email/EmailListIntegrations.php',

    // Payment Gateways
    'Zippy_PaymentGatewayIPN'                   => 'lib/Integrations/PaymentGateway/IPN.php',
    'Zippy_PaymentGatewayIntegration'           => 'lib/Integrations/PaymentGateway/Integration.php',
    'Zippy_PaymentGatewayIntegrations'          => 'lib/Integrations/PaymentGateway/Integrations.php',
    'Zippy_PaymentGateway'                      => 'lib/Integrations/PaymentGateway/PaymentGateway.php',
    'Zippy_PaymentGateways'                     => 'lib/Integrations/PaymentGateway/PaymentGateways.php',
    'Zippy_PaymentGatewayListener'              => 'lib/Integrations/PaymentGateway/Listener.php',
    'Zippy_PaymentGatewayAPI'                   => 'lib/Integrations/PaymentGateway/API.php',
    'ZippyCourses_PaymentGatewayIntegrations'   => 'app/Integrations/PaymentGateway/PaymentGatewayIntegrations.php',
    
    // Cart
    'Zippy_Product'                             => 'lib/Cart/Product.php',
    'ZippyCourses_Products'                     => 'app/Cart/Products.php',
    'Zippy_Currencies'                          => 'lib/Cart/Currencies.php',
    'Zippy_Order'                               => 'lib/Cart/Order.php',
    'Zippy_Orders'                              => 'lib/Cart/Orders.php',
    'Zippy_Transaction'                         => 'lib/Cart/Transaction.php',
    'Zippy_Transactions'                        => 'lib/Cart/Transactions.php',
    'Zippy_TransactionProcessor'                => 'lib/Cart/Processor.php',
    'Zippy_PaymentGatewayTransactionAdapter'    => 'lib/Cart/TransactionAdapter.php',
    'ZippyCourses_ProductListener'              => 'app/Listeners/Product.php',

    // Courses
    'ZippyCourses_Course'                       => 'app/Courses/Course.php',
    'ZippyCourses_Courses'                      => 'app/Courses/Courses.php',

    // Ajax
    'ZippyCourses_Ajax'                         => 'app/Ajax/Ajax.php',
    'Zippy_Ajax'                                => 'lib/Ajax/Ajax.php',

    // Views
    'ZippyCourses_CourseEntries_View'           => 'app/Views/CourseEntries.php',
    'ZippyCourses_Unit_View'                    => 'app/Views/Course/Unit.php',
    'ZippyCourses_Entry_View'                   => 'app/Views/Course/Entry.php',
    'ZippyCourses_CourseEntries_View'           => 'app/Views/CourseEntries.php',
    'ZippyCourses_UnitEntries_View'             => 'app/Views/UnitEntries.php',
    'ZippyCourses_OrderTransaction_View'        => 'app/Views/Order/Transaction.php',
    'ZippyCourses_OrderDetails_View'            => 'app/Views/Order/Details.php',
    'ZippyCourses_OrderTransactions_View'       => 'app/Views/Order/Transactions.php',
    'ZippyCourses_StudentOrders_View'           => 'app/Views/Student/Orders.php',
    'ZippyCourses_StudentOrder_View'            => 'app/Views/Student/Order.php',
    'ZippyCourses_Course_Navigation_View'       => 'app/Views/Course/Navigation.php',
    'ZippyCourses_Certificate_View'             => 'app/Views/Course/Certificate.php',


    'ZippyCourses_Widgets'                      => 'app/Widgets/Widgets.php',
    
    'ZippyCourses_RSS'                          => 'app/RSS/RSS.php',
    'ZippyCourses_License'                      => 'app/License/License.php',

    'ZippyCourses_ZippyCourses_Exporter'        => 'app/Exporters/ZippyCourses.php',
    'ZippyCourses_ZippyCourses_Importer'        => 'app/Importers/ZippyCourses.php',
    'ZippyCourses_Student_Importer'             => 'app/Importers/Student.php',
    'ZippyCourses_WishlistMember_Importer'      => 'app/Importers/WishlistMember.php',
    'ZippyCourses_WishlistMemberStudents_Importer' => 'app/Importers/WishlistMemberStudents.php',
    'ZippyCourses_Image_Importer'               => 'app/Importers/Image.php',
    
    // Quiz Results
    'ZippyCourses_QuizResults_Quiz'             => 'app/Analytics/QuizResults/Quiz.php',
    'ZippyCourses_QuizResults_Quizzes'          => 'app/Analytics/QuizResults/Quizzes.php',
    'ZippyCourses_QuizResults_Question'         => 'app/Analytics/QuizResults/Question.php',
    'ZippyCourses_QuizResults_Questions'        => 'app/Analytics/QuizResults/Questions.php',
    'ZippyCourses_QuizResults_Answer'           => 'app/Analytics/QuizResults/Answer.php',
    'ZippyCourses_QuizResults_Answers'          => 'app/Analytics/QuizResults/Answers.php',
    'ZippyCourses_QuizResults_Result'           => 'app/Analytics/QuizResults/Result.php',
    'ZippyCourses_QuizResults_Results'          => 'app/Analytics/QuizResults/Results.php',

    // MIgrations
    'Zippy_DatabaseMigration'                   => 'lib/Database/Migration/Migration.php',
    'Zippy_MigrationStep'                       => 'lib/Database/Migration/Step.php',
    'Zippy_MigrationSteps'                      => 'lib/Database/Migration/Steps.php',
    'ZippyCourses_v1_0_0_DatabaseMigration'     => 'app/Database/Migrations/v1_0_0/Migration.php',
    'ZippyCourses_v1_0_0_Course_MigrationStep'  => 'app/Database/Migrations/v1_0_0/Steps/Course.php',
    'ZippyCourses_v1_0_0_Order_MigrationStep'   => 'app/Database/Migrations/v1_0_0/Steps/Order.php',
    'ZippyCourses_v1_0_0_Posts_MigrationStep'   => 'app/Database/Migrations/v1_0_0/Steps/Posts.php',
    'ZippyCourses_v1_0_0_Product_MigrationStep' => 'app/Database/Migrations/v1_0_0/Steps/Product.php',
    'ZippyCourses_v1_0_0_Quiz_MigrationStep'    => 'app/Database/Migrations/v1_0_0/Steps/Quiz.php',
    'ZippyCourses_v1_0_0_User_MigrationStep'    => 'app/Database/Migrations/v1_0_0/Steps/User.php',
    'ZippyCourses_v1_0_0_Transaction_MigrationStep'  => 'app/Database/Migrations/v1_0_0/Steps/Transaction.php',
    'ZippyCourses_v1_0_0_Settings_MigrationStep'  => 'app/Database/Migrations/v1_0_0/Steps/Settings.php',
    'ZippyCourses_v1_0_0_QuizQuestions_MigrationStep'  => 'app/Database/Migrations/v1_0_0/Steps/QuizQuestions.php',
    'ZippyCourses_v1_0_0_Activity_MigrationStep'  => 'app/Database/Migrations/v1_0_0/Steps/Activity.php',
    'ZippyCourses_v1_0_0_LandingPages_MigrationStep'  => 'app/Database/Migrations/v1_0_0/Steps/LandingPages.php',

    // Customizer
    'ZippyCourses_Customizer'                   => 'app/Customizer/Customizer.php',
    'Zippy_Customizer'                          => 'lib/Customizer/Customizer.php',
    
    'Zippy_Customizer_Panel'                    => 'lib/Customizer/Panel.php',
    'Zippy_Customizer_Panels'                   => 'lib/Customizer/Panels.php',
    
    'Zippy_Customizer_Section'                  => 'lib/Customizer/Section.php',
    'Zippy_Customizer_Sections'                 => 'lib/Customizer/Sections.php',

    'Zippy_Customizer_Control'                  => 'lib/Customizer/Control.php',
    'Zippy_Customizer_Controls'                 => 'lib/Customizer/Controls.php',

);

return apply_filters('zippy_classmap', $classmap);
