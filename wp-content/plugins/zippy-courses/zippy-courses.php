<?php
/*
Plugin Name: Zippy Courses
Plugin URI:  https://zippycourses.com/
Description: A solution for building, selling and running courses - easily - on your WordPress site.
Version:     1.3.2
Author:      Zippy Courses <support@zippycourses.com>
Author URI:  https://zippycourses.com/
Text Domain: zippy-courses
Domain Path: /lang
*/

require_once(dirname(__FILE__) . '/autoload.php');

add_action('plugins_loaded', array('Zippy', 'setup'), 9);

function strip_empty_entries($data, $field)
{
    if ($field != 'entries') {
        return $data;
    }

    return is_array($data) ? strip_empties($data) : array();
}
add_filter('zippy_fetch_meta_data', 'strip_empty_entries', 10, 2);

function get_quizzes($data, $field)
{
    if ($field != 'quizzes') {
        return $data;
    }

    global $wpdb;

    $zippy = Zippy::instance();
    $output = array();

    $quiz = new stdClass;
    $quiz->text = '';
    $quiz->value = 0;

    $output[] = $quiz;

    $invalid_post_status_list = $zippy->utilities->getInvalidPostStatusList();
    $quizzes = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type = 'quiz' AND post_status NOT IN ($invalid_post_status_list)");

    foreach ($quizzes as $q) {
        $quiz = new stdClass;
        $quiz->text = $q->post_title;
        $quiz->value = $q->ID;

        $output[] = $quiz;
    }
    return $output;
}
add_filter('zippy_fetch_meta_data', 'get_quizzes', 10, 2);

function strip_empties($entries)
{
    global $wpdb;

    foreach ($entries as $key => &$value) {
        $exists = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE ID = %s", $value->ID));

        if ($exists !== null) {
            if (!empty($value->entries)) {
                $value->entries = strip_empties($value->entries);
            }
        } else {
            unset($entries[$key]);
        }
    }

    return $entries;
}

register_activation_hook(__FILE__, array('ZippyCourses_Installer', 'activate'));
