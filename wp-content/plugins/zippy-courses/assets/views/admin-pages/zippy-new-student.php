<?php
if (!current_user_can('create_users')) {
    wp_die(__('Cheatin&#8217; uh?'));
}

$zippy = Zippy::instance();

$form = $zippy->forms->get('new_student');
$view = $zippy->make('form_view', array($form));
?>

<div class="wrap">
    <h2 id="add-new-student"><?php _e('Add New Student', ZippyCourses::TEXTDOMAIN); ?></h2>
    
    <?php $view->show(); ?>

</div><!-- /wrap -->
<div style="clear: both;"></div>
