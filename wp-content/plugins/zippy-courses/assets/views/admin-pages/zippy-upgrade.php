<?php

$zippy = Zippy::instance();

$migration = $zippy->make('migration_v1_0_0');
$migration->analyze();

$action = isset($_GET['zippy-upgrade-action']) ? $_GET['zippy-upgrade-action'] : '';
?>

<div class="wrap">
    <h2><?php _e('Upgrading Zippy Courses', ZippyCourses::TEXTDOMAIN); ?></h2>
    <?php if ($action == 'start'): ?> 
    <script type="text-template" id="zippy-upgrade-progress-tmpl">
        <div class="zippy-upgrader text-center">
            
            <h3><?php _e("Your Upgrade is in Progress", ZippyCourses::TEXTDOMAIN); ?></h3>
            
            <div class="zippy-spinner spinner visible"></div>

            <h4 v-show="current_step.length > 0"><?php printf(__("Importing %s...", ZippyCourses::TEXTDOMAIN), '{{ current_step }}'); ?></h4>
            <h4 v-show="current_step.length == 0"><?php _e("Importing Data...", ZippyCourses::TEXTDOMAIN); ?></h4>
            
            <hr/>

            <p class="description"><strong><?php _e('Please do not leave this page during the upgrade, as it may cause unexpected results.', ZippyCourses::TEXTDOMAIN); ?></strong></p>
            
            <p class="description"><strong><?php _e('If you have a large number of students, lessons or quizzes (and quiz results), are on an older version of PHP, or are on shared hosting, it may take a bit longer.  Average database upgrade times are 1-2 minutes for small sites and 5-10 for larger sites.', ZippyCourses::TEXTDOMAIN); ?></strong></p>

            <p class="description"><strong><?php printf(__('Thank you for your patience!  If you need any assistance or run into any issues, please contact %s,', ZippyCourses::TEXTDOMAIN), '<a href="mailto:support@zippycourses.com">Zippy Courses Support</a>.'); ?></strong></p>

        </div>
    </script>
    <?php else: ?>
        <p>
        <a href="<?php echo admin_url('?page=zippy-upgrade&zippy-upgrade-action=start') ?>" class="button button-primary button-lg"><?php _e('Start the Upgrade', ZippyCourses::TEXTDOMAIN); ?></a></p>
        </p>
    <?php endif; ?>

</div>

