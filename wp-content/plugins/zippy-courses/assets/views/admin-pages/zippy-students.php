<?php

$zippy = Zippy::instance();

$students_table = new ZippyCourses_Students_ListTable();
$students_table->prepare_items();
?>
<div class="wrap">
    <?php do_action( 'zippy_students_top' ); ?>
    <form method="get" action="<?php echo admin_url( 'admin.php?page=zippy-students'); ?>" class="zippy-list-table">
        <?php
        $students_table->search_box(__('Search', ZippyCourses::TEXTDOMAIN), 'zippy-student-search');
        $students_table->display();
        ?>
        <input type="hidden" name="page" value="zippy-students" />
    </form>
    <?php do_action( 'zippy_students_bottom' ); ?>
</div>

<?php
