<?php

global $wp_settings_fields, $wp_settings_sections;

$zippy = Zippy::instance();

$tabs = $zippy->make('settings_tabs');
$tabs->generate('zippy_settings');

$tabs_view = $zippy->make('settings_tabs_view', array($tabs));
$tabs_view->show();
