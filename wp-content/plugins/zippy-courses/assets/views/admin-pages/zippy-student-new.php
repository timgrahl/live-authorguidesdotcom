<?php
/**
 * Users administration panel.
 *
 * @package WordPress
 * @subpackage Administration
 */

if (!current_user_can('create_users')) {
    wp_die(__('Cheatin&#8217; uh?'));
}

?>

<div class="wrap">
    <h2 id="add-new-student">Add New Student</h2>
    
    <form action="" method="post" name="createstudent" id="createstudent" class="validate new-student-form">

            <input name="action" type="hidden" value="createstudent">

            <table class="form-table">

            <tbody><tr class="form-field form-required">
                <th scope="row"><label for="user_login">Username <span class="description">(required)</span></label></th>
                <td><input name="user_login" type="text" id="user_login" value="" aria-required="true"></td>
            </tr>
            <tr class="form-field form-required">
                <th scope="row"><label for="email">E-mail <span class="description">(required)</span></label></th>
                <td><input name="user_email" type="text" id="user_email" value=""></td>
            </tr>
            <tr class="form-field">
                <th scope="row"><label for="first_name">First Name </label></th>
                <td><input name="first_name" type="text" id="first_name" value=""></td>
            </tr>
            <tr class="form-field">
                <th scope="row"><label for="last_name">Last Name </label></th>
                <td><input name="last_name" type="text" id="last_name" value=""></td>
            </tr>
            
            <tr class="form-field form-required">
                <th scope="row"><label for="pass1">Password <span class="description">(required)</span></label></th>
                <td>
                    <input class="hidden" value=" "><!-- #24364 workaround -->
                    <input name="pass1" type="password" id="pass1" autocomplete="off"></td>
            </tr>
            <tr class="form-field form-required">
                <th scope="row"><label for="pass2">Repeat Password <span class="description">(required)</span></label></th>
                <td>
                <input name="pass2" type="password" id="pass2" autocomplete="off">
                <br>
                <div id="pass-strength-result" style="display: block;">Strength indicator</div>
                <p class="description indicator-hint" style="clear: both;">Hint: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ &amp; ).</p>
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="send_password">Send Login Information?</label></th>
                <td><label for="send_password"><input type="checkbox" name="send_password" id="send_password" value="1"> Send this user your registration email.</label></td>
            </tr>
            
            <input type="hidden" name="role" id="role" value="subscriber" />

            </tbody></table>

            <?php wp_nonce_field( 'create-student', '_wpnonce_create-student' ); ?>

            <p class="submit"><input type="submit" id="createusersub" class="button button-primary" value="Add New Student "></p>

    </form>

</div><!-- /wrap -->
<div style="clear: both;"></div>
<?php

