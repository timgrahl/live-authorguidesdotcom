<?php

$zippy = Zippy::instance();

$tabs = $zippy->make('pages_tabs');
$tabs->generate($zippy->utilities->admin->getAnalyticsTabs());

$view = $zippy->make('pages_tabs_view', array($tabs));
$view->show();