<?php

global $wpdb;

$zippy = Zippy::instance();

$view = 'profile';

$quiz    = filter_input(INPUT_GET, 'quiz');

if (!empty($quiz)) {
    $results = $zippy->utilities->quiz->getResult($quiz);

    if (is_object($results)) {
        $view = 'quiz-results';
    }
}

switch ($view) {
    case 'quiz-results':
        include (ZippyCourses::$path . 'assets/views/admin-pages/student/quiz-results.php');
        break;
    case 'profile':
    default:
        include (ZippyCourses::$path . 'assets/views/admin-pages/student/profile.php');
        break;
}
