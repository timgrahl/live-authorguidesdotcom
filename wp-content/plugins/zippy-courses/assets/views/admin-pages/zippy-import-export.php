<?php

$zippy = Zippy::instance();

$import_view = $zippy->make('file_view', array('file' => ZippyCourses::$path . 'assets/views/admin-pages/import-export/import.php'));
$import_tab  = $zippy->make('tab', array('import', __('Import', ZippyCourses::TEXTDOMAIN), ''));
$import_tab->setView($import_view);

$export_view = $zippy->make('file_view', array('file' => ZippyCourses::$path . 'assets/views/admin-pages/import-export/export.php'));
$export_tab = $zippy->make('tab', array('export', __('Export', ZippyCourses::TEXTDOMAIN), ''));
$export_tab->setView($export_view);

$tabs = $zippy->make('pages_tabs');

$tabs->add($import_tab);
$tabs->add($export_tab);

$view = $zippy->make('pages_tabs_view', array($tabs));
$view->show();
