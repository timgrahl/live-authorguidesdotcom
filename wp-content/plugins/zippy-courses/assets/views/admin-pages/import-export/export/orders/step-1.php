<?php

$zippy = Zippy::instance();
$product_ids    = $zippy->utilities->product->getAllProductIds();

?>

<h2><?php _e('Zippy Courses Orders Exporter', ZippyCourses::TEXTDOMAIN); ?></h2>


<p><?php _e('<strong>Exports:</strong> zippy-orders.csv', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('<strong>Includes:</strong> Order ID numbers, dates, User Names, Products, payment type and amounts paid.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('This file can be used to export orders made in your site, so that these data can be integrated with your existing accounting systems. If you want to import their course data as well, please use the "All Data" panel on the left.', ZippyCourses::TEXTDOMAIN); ?></p>

<div class="zippy-order-export">

    <h3><?php _e('Export Orders from the following products:', ZippyCourses::TEXTDOMAIN); ?></h3>
        <div class="zippy-order-products">
        <ul>
                <li><label><input type="checkbox" value="all" class="zippy-toggle-all" /> <?php _e('All Products', ZippyCourses::TEXTDOMAIN); ?></label></li>
                <hr>
                <?php foreach ($product_ids as $product_id): ?>
                <li><label><input type="checkbox" value="<?php echo $product_id; ?>" name="product_download[]" /> <?php echo get_the_title($product_id); ?></label></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <hr>

    <h3><?php _e('Export Orders between specific dates:', ZippyCourses::TEXTDOMAIN); ?></h3>
    <p>Start Date: <input v-datepicker="order_range_start_date" id="zippy_order_start_date"> 
     End Date: <input v-datepicker="order_range_end_date" id="zippy_order_end_date"></p>
    <p class="text-center">
        <a href="<?php echo admin_url('admin-post.php?action=export_zippy_orders_csv'); ?>"
        class="button button-primary button-lg"
        data-url="<?php echo admin_url('admin-post.php?action=export_zippy_orders_csv'); ?>">
            <?php _e('Download Export File', ZippyCourses::TEXTDOMAIN); ?>
        </a>
    </p>
</div>
