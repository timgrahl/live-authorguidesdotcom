<?php

$zippy = Zippy::instance();

$course_ids     = $zippy->utilities->course->getAllCourseIds();
$product_ids    = $zippy->utilities->product->getAllProductIds();
?>

<h2><?php _e('Zippy Courses Student Exporter', ZippyCourses::TEXTDOMAIN); ?></h2>

<p><?php _e('<strong>Exports:</strong> zippy-students.csv', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('<strong>Includes:</strong> Student\'s first name, last name, username, email and join date.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('This file can be used to export students from a particular course or product, or all of your students, so that you can import their personal data into other systems. If you want to import their course data as well, please use the "All Data" panel on the left.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><em><?php _e('<strong>Note:</strong> This file will not include any course access information.', ZippyCourses::TEXTDOMAIN); ?></em></p>

<div class="zippy-student-course-export">
    <h3><?php _e('Export Students from the following courses:', ZippyCourses::TEXTDOMAIN); ?></h3>

    <ul>
        <li><label><input type="checkbox" value="all" name="course_download[]" class="zippy-toggle-all" /> <?php _e('All Courses', ZippyCourses::TEXTDOMAIN); ?></label></li>
        <li><label><input type="checkbox" value="0" name="course_download[]">
            <?php _e('Students with no Courses'); ?>
        </label></li>
        <?php foreach ($course_ids as $course_id): ?>
        <li><label><input type="checkbox" value="<?php echo $course_id; ?>" name="course_download[]" /> <?php echo get_the_title($course_id); ?></label></li>
        <?php endforeach; ?>
    </ul>

    <p class="text-center">
        <a href="<?php echo admin_url('admin-post.php?action=export_zippy_student_csv'); ?>" class="button button-primary button-lg" data-url="<?php echo admin_url('admin-post.php?action=export_zippy_student_csv'); ?>" disabled>
            <?php _e('Download Export File', ZippyCourses::TEXTDOMAIN); ?>
        </a>
    </p>
</div>

<div class="zippy-student-product-export">
    <h3><?php _e('Export Students from the following products:', ZippyCourses::TEXTDOMAIN); ?></h3>

    <ul>
        <li><label><input type="checkbox" value="all" name="product_download[]" class="zippy-toggle-all" /> <?php _e('All Products', ZippyCourses::TEXTDOMAIN); ?></label></li>
        <?php foreach ($product_ids as $product_id): ?>
        <li><label><input type="checkbox" value="<?php echo $product_id; ?>" name="product_download[]" /> <?php echo get_the_title($product_id); ?></label></li>
        <?php endforeach; ?>
    </ul>

    <p class="text-center">
        <a href="<?php echo admin_url('admin-post.php?action=export_zippy_student_csv'); ?>" class="button button-primary button-lg" data-url="<?php echo admin_url('admin-post.php?action=export_zippy_student_csv'); ?>" disabled>
            <?php _e('Download Export File', ZippyCourses::TEXTDOMAIN); ?>
        </a>
    </p>
</div>
