<h2><?php _e('Export All Zippy Courses Data', ZippyCourses::TEXTDOMAIN); ?></h2>

<p><?php _e('<strong>Exports:</strong> zippy-courses.json', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('<strong>Includes:</strong> Students, Courses, Units, Lessons, Pricing Options, Quizzes, Orders and Payments with all of their associations.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('This file is intended for use with the Zippy Importer, and is useful for moving all of the Zippy Courses data from one installation to another.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><em><?php _e('<strong>Note:</strong> This does not include posts or pages.', ZippyCourses::TEXTDOMAIN); ?></em></p>

<p class="text-center">
    <a href="<?php echo admin_url('admin-post.php?action=export_zippy_courses'); ?>" class="button button-primary button-lg">
        <?php _e('Download Export File', ZippyCourses::TEXTDOMAIN); ?>
    </a>
</p>
