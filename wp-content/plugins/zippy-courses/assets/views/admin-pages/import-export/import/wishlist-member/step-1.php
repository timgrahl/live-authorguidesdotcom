<?php
$zippy = Zippy::instance();

?>
<h2><?php _e('WishList Member Importer', ZippyCourses::TEXTDOMAIN); ?></h2>

<div class="zippy-alert zippy-alert-warning">
<h3><?php _e('Before You Begin...', ZippyCourses::TEXTDOMAIN); ?></h3>
<p><?php _e('Please create the Courses and Tiers that you would like to be able to connect your old WishList Member Membership Levels to.', ZippyCourses::TEXTDOMAIN); ?></p>
<p><?php _e('Once this is done, you will be ready to import your WishList Member content.', ZippyCourses::TEXTDOMAIN); ?></p>
</div>

<h3><?php _e('Step 1: Import Your WishList Content', ZippyCourses::TEXTDOMAIN); ?></h3>

<p><?php _e('In this step, you will import your protected Posts and Pages from your WishList Member site.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('During the import process, you will be able to create new Courses and tell Zippy Courses where to include your old content in your new Zippy Courses setup.', ZippyCourses::TEXTDOMAIN); ?></p>

<?php

$form = $zippy->forms->get('import');
$form->addHiddenField('zippy_import_action')
    ->setValue('import-wlm-content')
    ->setSavable(false);

$view = $zippy->make('form_view', array($form));

$view->show();

?>

<hr/>

<h3><?php _e('Step 2: Import Your WishList Users from Another Site', ZippyCourses::TEXTDOMAIN); ?></h3>

<p><?php _e('In this step, you will import your students and connect your old WishList Member Levels to your Courses in Zippy Courses.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('Make sure you have imported your old WishList Content in the step above or created all of the courses you\'ll need for your User import.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('Note: If you need multiple Tiers or Pricing Options for your Courses, make sure you have gone in and created them before importing your Users.', ZippyCourses::TEXTDOMAIN); ?></p>

<?php

$form = $zippy->forms->get('import_wishlist_students');
$form->addHiddenField('zippy_import_action')
    ->setValue('import-wlm-students')
    ->setSavable(false);

$view = $zippy->make('form_view', array($form));

$view->show();

?>

<hr/>

<h3><?php _e('Step 2 (alternate): Update Existing User Permissions', ZippyCourses::TEXTDOMAIN); ?></h3>

<p><?php _e('You can also import permissions for existing users on your site by clicking the Import Permissions button below.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><a href="<?php echo admin_url('/admin.php?page=zippy-import-export&importer=wishlist-member-students&step=2b'); ?>"><?php _e('Migrate Existing Students', ZippyCourses::TEXTDOMAIN); ?></a></p>
<hr/>

<h3><?php _e('Need the WishList Member Exporter for Zippy Courses?', ZippyCourses::TEXTDOMAIN); ?></h3>

<p><?php _e('You will need the WishList Member Exporter for Zippy Courses plugin to export your WishList Member content and users to use with the import tools above.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('Once you\'ve downloaded the plugin, install it on your WishList Member site, and visit the WLM Export link under the Tools menu in your WordPress admin.', ZippyCourses::TEXTDOMAIN); ?></p>

<p style="text-align: center;">
    <a href="https://zippycourses.com/wp-content/uploads/2015/02/zippy-wlm-exporter.zip" class="button">Download Exporter Plugin</a>
</p>