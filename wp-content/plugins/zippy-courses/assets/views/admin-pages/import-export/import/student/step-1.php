<?php
$zippy = Zippy::instance();

$form = $zippy->forms->get('import_student');
$view = $zippy->make('form_view', array($form));

?>
<h2>Student Importer</h2>

<p><?php printf(__('Use the form below to import your new data. You can use <a href="%s">this file</a> as a starting point for creating your CSV.', ZippyCourses::TEXTDOMAIN), admin_url('admin-post.php?action=export_zippy_sample_student_csv')); ?></p>

<p><?php _e('This Importer will import your students based on your CSV and will allow you to assign them access to Courses and Bundles.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php printf(__('<strong>IMPORTANT:</strong> Students will be send the "Registration Email" in your site after they are imported. If you do not want your students to be notified of their accounts, disable this email from your <a href="%s">System Email Settings</a>', ZippyCourses::TEXTDOMAIN), admin_url('admin.php?page=zippy-settings&tab=zippy_settings_system_emails')); ?></p>


<?php

$view->show();

