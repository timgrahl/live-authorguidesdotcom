<?php

$zippy = Zippy::instance();

$attachments = urldecode(filter_input(INPUT_GET, 'attachments'));
$attachments = array_filter(explode(',', $attachments));

$importer = new ZippyCourses_Image_Importer($attachments);
$importer->import();

$imported = $importer->getImported();

if (count($imported)) : ?>

    <h1><?php _e('Congratulations!', ZippyCourses::TEXTDOMAIN); ?></h1>

    <p><?php printf(__('You imported and updated references to %s images.', ZippyCourses::TEXTDOMAIN), count($imported)); ?></p>
    <p>
        <a href="<?php echo admin_url('admin.php?page=zippy-courses'); ?>" class="button button-primary">
            <?php _e('Return to the Zippy Courses Dashboard', ZippyCourses::TEXTDOMAIN); ?>
        </a>
    </p>

<?php else : ?>

    <h1><?php _e('Oh no!', ZippyCourses::TEXTDOMAIN); ?></h1>
    
    <p><?php _e('The Importer had trouble importing your images.', ZippyCourses::TEXTDOMAIN); ?></p>
    <p><?php _e('You may need to manually copy them to your server and re-assign them.', ZippyCourses::TEXTDOMAIN); ?></p>
    <p>
        <a href="<?php echo admin_url('admin.php?page=zippy-import-export'); ?>" class="button button-primary">
            <?php _e('Return &amp; Try Again', ZippyCourses::TEXTDOMAIN); ?>
        </a>
        <a href="<?php echo admin_url('admin.php?page=zippy-courses'); ?>" class="button">
            <?php _e('Zippy Courses Dashboard', ZippyCourses::TEXTDOMAIN); ?>
        </a>
    </p>

<?php endif;
