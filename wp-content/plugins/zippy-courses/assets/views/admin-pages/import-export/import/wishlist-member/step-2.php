<?php

$zippy = Zippy::instance();

$file     = urldecode(filter_input(INPUT_GET, 'src'));
$importer = new ZippyCourses_WishlistMember_Importer($file);
$importer->analyze();

$levels = $importer->getLevels();

$courses_and_tiers = json_decode(json_encode($zippy->utilities->course->getAllCoursesAndTiers()));

foreach ($courses_and_tiers as &$course) {
    $course->existing = true;
}

$form = $zippy->forms->get('import_wishlist');

$form->fields->get('zippy_import_file')->setValue($file);

$form->setSubmitText(__('Import Content', ZippyCourses::TEXTDOMAIN));
$form->addSubmitClass('button');
$form->addSubmitClass('button-primary');
$form->addSubmitClass('button-lg');

$view = $zippy->make('form_view', array($form));

?>

<h1><?php _e('Congratulations! Your data was imported successfully.', ZippyCourses::TEXTDOMAIN); ?></h1>

<p><?php _e('Please choose the Courses and Tiers on this site that you would like to connect each of your old WishList Membership Levels with.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('Use the <strong>Next</strong> and <strong>Previous</strong> buttons to navigate between the levels.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><em><?php _e('<strong>Note:</strong> Any imported content that belongs to multiple WishList Membership Levels will be added as Additional Content to the Courses and Tiers selected for that level.', ZippyCourses::TEXTDOMAIN); ?></em></p>

<div class="zippy-wlm-importer-builder"></div>

<div class="zippy-wlm-importer-builder-footer">
    <?php $view->show(); ?>
</div>


<script type="text/template" id="zippy-wlm-import-levels"><?php echo json_encode($levels); ?></script>

<script type="text/template" id="zippy-wlm-importer-tmpl">
<div>

    <div class="zippy-wlm-import-courses">
        <header>
            <h3><?php _e('New Courses', ZippyCourses::TEXTDOMAIN); ?></h3>
            <button class="button button-primary" v-on="click : addCourse">
                <?php _e('Add Course', ZippyCourses::TEXTDOMAIN); ?>
            </button>
        </header>

        <course v-repeat="courses"></course>
    </div>

    <div class="zippy-wlm-import-levels">
        <header>
            <h3><?php _e('WishList Membership Levels', ZippyCourses::TEXTDOMAIN); ?></h3>
        </header>
        <level courses="{{ courses }}" v-repeat="levels"></level>
    </div>
</div>
</script>

<script type="text/template" id="zippy-wlm-importer-level-tmpl">
<div class="zippy-wlm-importer-level">
    <label>{{ title }}:</label> <select options="course_list" v-model="course"></select>
</div>
</script>

<script type="text/template" id="zippy-wlm-importer-course-tmpl">
<div class="zippy-wlm-importer-course">
{{ $index + 1 }}. <input v-model="title" placeholder="<?php _e('Enter New Course Title'); ?>" />
</div>
</script>

<?php
/*
Courses
---------------------------------
+ Create a New Course

Levels
---------------------------------
Level Name : [Course] [Tier]
*/