<?php

$zippy = Zippy::instance();

$file     = urldecode(filter_input(INPUT_GET, 'src'));
$importer = new ZippyCourses_ZippyCourses_Importer($file);

$importer->import();
$imported = $importer->getImported();
$attachment_ids = isset($imported['attachments']) ? $imported['attachments'] : array();

?>

<h1><?php _e('Congratulations! Your data was imported successfully.', ZippyCourses::TEXTDOMAIN); ?></h1>

<p><?php _e('You imported:', ZippyCourses::TEXTDOMAIN); ?></p>

<ul>
<?php
foreach ($imported as $type => $items) {
    if (isset($items['errors'])) {
        unset($items['errors']);
    }

    $label = ucwords(str_replace(array('-', '_'), ' ', $type));
    $last = substr($label, -1);

    if ($last !== 's') {
        $label .= 's';
    }

    echo '<li>' . count($items) . ' ' . $label . '</li>';
}
?>
</ul>

<p><?php _e('There are a few next steps to consider:', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('Because of standard WordPress security measures, your Users will need to change their passwords. You should refer them to the Forgot Password page to handle this process.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('If the URL of your Lesson Downloads has changed, you will need to update them manually. They still refer to their old locations.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><?php _e('Your images have not been imported, as this is a bandwidth and CPU intensive action. You can do so below, if you would like.', ZippyCourses::TEXTDOMAIN); ?></p>

<?php if(count($attachment_ids)): ?>
<h2><?php _e('Image Importer', ZippyCourses::TEXTDOMAIN); ?></h2>

<p><?php _e('Click "Import Images" to begin the import process. Please be aware that this can take awhile, especially if there are many image references in your import file.', ZippyCourses::TEXTDOMAIN); ?></p>

<?php $url = admin_url('/admin.php?page=zippy-import-export&importer=zippy-courses&step=3&attachments=' . implode(',', $attachment_ids)); ?>

<p><a href="<?php echo $url; ?>" class="button button-primary"><?php _e('Import Images', ZippyCourses::TEXTDOMAIN); ?></a></p>

<?php endif; ?>

<p><a href="<?php echo admin_url('admin.php?page=zippy-courses'); ?>" class="button button-primary"><?php _e('Return to Zippy Courses Dashboard', ZippyCourses::TEXTDOMAIN); ?></a></p>


