<?php

$zippy = Zippy::instance();

$file     = urldecode(filter_input(INPUT_GET, 'src'));
$importer = new ZippyCourses_WishlistMember_Importer($file);

if (($summary = get_transient('zippy_wlm_import_summary')) == null) {
    $valid = false;
}

$summary = json_decode(stripslashes($summary));
$importer->setStructureCourses($summary->courses);
$importer->setStructureLevels($summary->levels);

$importer->analyze();
$importer->import();

$imported = $importer->getImported();
?>

<h1><?php _e('Congratulations! You have imported your WishList Member content into Courses!', ZippyCourses::TEXTDOMAIN); ?></h1>

<p><?php _e('You imported and created:', ZippyCourses::TEXTDOMAIN); ?></p>

<ul>
<?php
foreach ($imported as $type => $items) {
    if (isset($items['errors'])) {
        unset($items['errors']);
    }

    $label = ucwords(str_replace(array('-', '_'), ' ', $type));
    $last = substr($label, -1);

    if ($last !== 's') {
        $label .= 's';
    }

    echo '<li>' . count($items) . ' ' . $label . '</li>';
}
?>
</ul>

<p><?php _e('Your next step is to edit the basic details of all of your new Courses and Products to start selling.', ZippyCourses::TEXTDOMAIN); ?></p>
<p><?php _e('You can also import your WishList Member students and assign them to your new Courses!', ZippyCourses::TEXTDOMAIN); ?></p>

<p>
    <a href="' . admin_url('admin.php?page=zippy-courses') . '" class="button button-primary">
        <?php _e('Return to the Zippy Courses Dashboard', ZippyCourses::TEXTDOMAIN); ?>
    </a>
</p>

<?php
