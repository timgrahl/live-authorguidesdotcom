<?php

$zippy = Zippy::instance();

?>

<h1><?php _e('Congratulations! You have given your imported students access to products.', ZippyCourses::TEXTDOMAIN); ?></h1>

<p><?php _e('You are all done with the Student Import process.  Please use the buttons below to move on.', ZippyCourses::TEXTDOMAIN); ?></p>

<p>
    <a href="<?php echo admin_url('admin.php?page=zippy-courses'); ?>" class="button-primary">
        <?php _e('Return to the Zippy Courses Dashboard', ZippyCourses::TEXTDOMAIN); ?>
    </a> 
    <a href="<?php echo admin_url('admin.php?page=zippy-students'); ?>" class="button">
        <?php _e('View All Students', ZippyCourses::TEXTDOMAIN); ?>
    </a> 
</p>
