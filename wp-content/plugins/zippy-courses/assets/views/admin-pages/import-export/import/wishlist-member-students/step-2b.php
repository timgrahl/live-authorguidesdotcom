<?php

$zippy = Zippy::instance();

$importer = new ZippyCourses_WishlistMemberStudents_Importer();
$importer->migrate();

?>

<h1><?php _e('Congratulations! Your students were migrated successfully.', ZippyCourses::TEXTDOMAIN); ?></h1>

<p>
    <a href="<?php echo admin_url('admin.php?page=zippy-courses'); ?>" class="button button-primary">
        <?php _e('Return to the Zippy Courses Dashboard', ZippyCourses::TEXTDOMAIN); ?>
    </a>
</p>
