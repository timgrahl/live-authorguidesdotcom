<?php

$zippy = Zippy::instance();

$file     = urldecode(filter_input(INPUT_GET, 'src'));
$importer = new ZippyCourses_WishlistMemberStudents_Importer($file);

$importer->import();
$imported = $importer->getImported();
$errors = $importer->getErrors();
?>

<h1><?php _e('Congratulations! Your students were imported successfully.', ZippyCourses::TEXTDOMAIN); ?></h1>

<p><?php printf(__('You imported %s students!', ZippyCourses::TEXTDOMAIN), count($imported['users'])); ?></p>

<?php if(count($errors)): ?>

<p><?php _e('There were a few errors that occurred during the import process:', ZippyCourses::TEXTDOMAIN); ?></p>

<ul style="list-style: disc; padding-left: 20px;">
<?php
foreach ($errors as $err) {
    echo '<li>' . $err . '</li>';
}
?>
</ul>
<?php endif; ?>

<p><?php _e('You can now edit your students and grant them access to other courses.', ZippyCourses::TEXTDOMAIN); ?></p>

<p>
    <a href="' . admin_url('admin.php?page=zippy-courses') . '" class="button button-primary">
        <?php _e('Return to the Zippy Courses Dashboard', ZippyCourses::TEXTDOMAIN); ?>
    </a>
</p>