<?php

$zippy = Zippy::instance();

$file     = urldecode(filter_input(INPUT_GET, 'src'));
$importer = new ZippyCourses_Student_Importer($file);

$importer->import();

$imported   = $importer->getImported();
$errors     = $importer->getErrors();
$all_students = $importer->getAllValidStudents();

$products = $zippy->utilities->product->getAllProductIds();
$product_options = array();

foreach ($products as $product_id) {
    $product_options[$product_id] = get_the_title($product_id);
}
$form = $zippy->forms->get('import_student_grant_access');

$form->fields->get('student_ids')->setValue(implode(',', $all_students));
$form->fields->get('products')->setOptions($product_options);

$form->addSubmitClass('button');
$form->addSubmitClass('button-primary');

$view = $zippy->make('form_view', array($form));

?>

<h1><?php _e('Congratulations! Your students were imported successfully.', ZippyCourses::TEXTDOMAIN); ?></h1>

<p><?php printf(__('You imported %s students!', ZippyCourses::TEXTDOMAIN), count($imported)); ?></p>

<?php if(count($errors)): ?>

<p><?php _e('There were a few errors that occurred during the import process:', ZippyCourses::TEXTDOMAIN); ?></p>

<ul style="list-style: disc; padding-left: 20px;">
<?php
foreach ($errors as $err) {
    echo '<li>' . $err . '</li>';
}
?>
</ul>

<p><?php _e('Users that already existed in your site can still be added to a product below:', ZippyCourses::TEXTDOMAIN); ?></p>

<?php endif; ?>
<?php if (count($all_students) && count($products)): ?>
    <h2><?php _e('Grant Access to Students', ZippyCourses::TEXTDOMAIN); ?></h2>
    <p><?php _e('You may now give the imported students access to products on your site by selecting them below and clicking on the <strong>Grant Access</strong> button.', ZippyCourses::TEXTDOMAIN); ?></p>
    <p><?php printf(
                __('Access will be granted to <strong>all imported students.</strong>  To grant access to an individual student, please visit their student profile, available by visiting the %s admin page.', ZippyCourses::TEXTDOMAIN),
                '<a href="' . admin_url('admin.php?page=zippy-students') . '">' . __('Students', ZippyCourses::TEXTDOMAIN)  . '</a>'
            ); ?>
    </p>
    <?php $view->show(); ?>
<?php endif; ?>

<p>
    <a href="<?php echo admin_url('admin.php?page=zippy-courses'); ?>" class="button">
        <?php _e('Return to the Zippy Courses Dashboard', ZippyCourses::TEXTDOMAIN); ?>
    </a> 
</p>
