<?php

$zippy = Zippy::instance();

$step       = filter_input(INPUT_GET, 'step');
$importer   = filter_input(INPUT_GET, 'importer');
$src        = filter_input(INPUT_GET, 'src');

if (empty($step) && empty($importer)) {
    $import_panels = $zippy->make('panels');

    $zippy_import_panel_view = $zippy->make('file_view', array('file' => ZippyCourses::$path . 'assets/views/admin-pages/import-export/import/zippy-courses/step-1.php'));
    $zippy_import_panel      = $zippy->make('panel', array('zippy-courses', 'Zippy Courses', ''));
    $zippy_import_panel->setView($zippy_import_panel_view);
    $import_panels->add($zippy_import_panel);

    $wlm_import_panel_view = $zippy->make('file_view', array('file' => ZippyCourses::$path . 'assets/views/admin-pages/import-export/import/wishlist-member/step-1.php'));
    $wlm_import_panel      = $zippy->make('panel', array('wishlist-member', 'Wishlist Member', ''));
    $wlm_import_panel->setView($wlm_import_panel_view);
    $import_panels->add($wlm_import_panel);

    $csv_import_panel_view = $zippy->make('file_view', array('file' => ZippyCourses::$path . 'assets/views/admin-pages/import-export/import/student/step-1.php'));
    $csv_import_panel      = $zippy->make('panel', array('csv', 'Student CSV', ''));
    $csv_import_panel->setView($csv_import_panel_view);
    $import_panels->add($csv_import_panel);

    $import_panels_view = $zippy->make('panels_view', array($import_panels));

    $import_panels_view->show();
} else {

    $path = ZippyCourses::$path . "assets/views/admin-pages/import-export/import/$importer/step-$step.php";

    if (is_readable($path)) {
        $view = $zippy->make('file_view', array('file' => $path));
        $view->show();
    } else {
        // Show error
    }
}
