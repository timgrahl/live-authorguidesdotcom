<?php

$zippy = Zippy::instance();

$export_panels = $zippy->make('panels');

$zippy_export_panel_view = $zippy->make('file_view', array('file' => ZippyCourses::$path . 'assets/views/admin-pages/import-export/export/zippy-courses/step-1.php'));
$zippy_export_panel      = $zippy->make('panel', array('zippy-courses', __('All Data', ZippyCourses::TEXTDOMAIN), ''));
$zippy_export_panel->setView($zippy_export_panel_view);
$export_panels->add($zippy_export_panel);


$students_export_panel_view = $zippy->make('file_view', array('file' => ZippyCourses::$path . 'assets/views/admin-pages/import-export/export/students/step-1.php'));
$students_export_panel      = $zippy->make('panel', array('students', __('Students', ZippyCourses::TEXTDOMAIN), ''));
$students_export_panel->setView($students_export_panel_view);
$export_panels->add($students_export_panel);

$students_export_panel_view = $zippy->make('file_view', array('file' => ZippyCourses::$path . 'assets/views/admin-pages/import-export/export/orders/step-1.php'));
$students_export_panel      = $zippy->make('panel', array('orders', __('Orders', ZippyCourses::TEXTDOMAIN), ''));
$students_export_panel->setView($students_export_panel_view);
$export_panels->add($students_export_panel);


$export_panels_view = $zippy->make('panels_view', array($export_panels));

echo $export_panels_view->render();
