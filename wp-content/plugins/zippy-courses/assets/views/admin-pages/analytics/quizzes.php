<?php

global $wpdb;

$zippy = Zippy::instance();

$quiz_ids = $zippy->utilities->quiz->getAllQuizIds();
$active_quiz_id = (int) filter_input(INPUT_GET, 'quiz');
?>

<?php
$quizzes_table = new ZippyCourses_QuizResults_ListTable();
$quizzes_table->prepare_items();
?>
<div class="wrap">
    <?php do_action('zippy_quiz_analytics_top'); ?>
    <form method="get" action="<?php echo admin_url('admin.php?page=zippy-analytics&tab=quiz-submissions'); ?>" class="zippy-list-table">
        <?php
        $quizzes_table->display();
        ?>
        <input type="hidden" name="page" value="zippy-analytics" />
        <input type="hidden" name="tab" value="quiz-submissions" />
    </form>
    <?php do_action('zippy_quiz_analytics_bottom'); ?>
</div>
<?php
