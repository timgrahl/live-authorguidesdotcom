<?php
// Variables
global $wpdb;

$zippy = Zippy::instance();

$course_ids = $zippy->utilities->course->getAllCourseIds();
$active_course_id = (int) filter_input(INPUT_GET, 'course');
?>

<div class="zippy-courses-analytics">
<label><strong><?php _e('Select Course', ZippyCourses::TEXTDOMAIN); ?>:</strong></label> 
<select class="zippy-analytics-course-dropdown">
    <option value="0"><?php _e('All Courses', ZippyCourses::TEXTDOMAIN); ?></option>
    <?php foreach ($course_ids as $course_id) : ?>
        <option value="<?php echo $course_id; ?>" <?php selected($course_id, $active_course_id); ?>><?php echo get_the_title($course_id); ?></option>
    <?php endforeach; ?>
</select>

<hr>

<?php
if ($active_course_id === 0) {
    $order_chart = new ZippyCourses_Order_Chart();
    $order_chart->fetchData();

    $registration_chart = new ZippyCourses_Registration_Chart();
    $registration_chart->fetchData();

    $num_students        = $zippy->utilities->student->getNumberOfStudents();
    $num_courses         = count($course_ids);
    $num_entries         = $zippy->utilities->entry->getNumberOfEntries();
    $num_units           = $zippy->utilities->entry->getNumberOfUnits();
    $num_orders          = $zippy->utilities->orders->getNumberOfOrders();
    $num_recent_orders   = count($zippy->utilities->orders->getRecentOrderIds());
    $num_recent_avg_val  = count($zippy->utilities->orders->getAverageValueOfOrders($zippy->utilities->orders->getRecentOrderIds()));
    $num_recent_students = count($zippy->utilities->student->getRecentStudentIds());

    ?>
    <div class="zippy-courses-course-analytics">
        <div class="zippy-course-analytics-charts">
            <div class="zippy-course-analytics-chart">
                <h3><?php _e('New Students', ZippyCourses::TEXTDOMAIN); ?></h3>
                <?php $registration_chart->show(); ?>
            </div>
            <div class="zippy-course-analytics-chart">
                <h3><?php _e('New Orders', ZippyCourses::TEXTDOMAIN); ?></h3>
                <?php $order_chart->show(); ?>
            </div>
        </div>

        <div class="zippy-course-analytics-tables">
            <h3><?php _e('At a Glance', ZippyCourses::TEXTDOMAIN); ?></h3>

            <div class="zippy-course-activity-table">
            <table class="zippy-data-table">
                <tbody>
                    <tr>
                        <th><?php _e('Number of Courses', ZippyCourses::TEXTDOMAIN); ?></th>
                        <td><?php echo $num_courses; ?></td>
                    </tr>
                    <tr>
                        <th><?php _e('Number of Units', ZippyCourses::TEXTDOMAIN); ?></th>
                        <td><?php echo $num_units; ?></td>
                    </tr>
                    <tr>
                        <th><?php _e('Number of Lessons (and other Course Entries)', ZippyCourses::TEXTDOMAIN); ?></th>
                        <td><?php echo $num_entries; ?></td>
                    </tr>

                    <tr>
                        <th>
                            <?php _e('Number of Students', ZippyCourses::TEXTDOMAIN); ?>
                        </th>
                        <td><?php echo $num_students; ?></td>
                    </tr>
                    <tr>
                        <th>
                            <?php _e('Number of New Students', ZippyCourses::TEXTDOMAIN); ?>
                            <span>(<?php _e('In last 30 days', ZippyCourses::TEXTDOMAIN); ?>)</span>
                        </th>
                        <td>
                            <?php echo $num_recent_students; ?>
                            (<?php printf(__('%s per day', ZippyCourses::TEXTDOMAIN), number_format_i18n($num_recent_students / 30, 2)); ?>)
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php _e('Number of Orders', ZippyCourses::TEXTDOMAIN); ?>
                        </th>
                        <td><?php echo $num_orders; ?></td>
                    </tr>
                    <tr>
                        <th>
                            <?php _e('Number of Recent Orders', ZippyCourses::TEXTDOMAIN); ?>
                            <span>(<?php _e('In last 30 days', ZippyCourses::TEXTDOMAIN); ?>)</span>
                        </th>
                        <td>
                            <?php echo $num_recent_orders; ?>
                            (<?php printf(__('%s per day', ZippyCourses::TEXTDOMAIN), number_format_i18n($num_recent_orders / 30, 2)); ?>)
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php _e('Average Value per Order', ZippyCourses::TEXTDOMAIN); ?>
                            <span>(<?php _e('In last 30 days', ZippyCourses::TEXTDOMAIN); ?>)</span>
                        </th>
                        <td>$<?php echo $num_recent_avg_val; ?></td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
<?php
} else {
    include (dirname(__FILE__) . '/course.php');
}
?>
</div>