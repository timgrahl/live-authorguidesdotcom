<?php

$course_id = (int) filter_input(INPUT_GET, 'course');

$all_views_dataset = new ZippyCourses_CourseAllViews_Dataset($course_id);
$recent_views_dataset = new ZippyCourses_CourseRecentViews_Dataset($course_id);

$mb_data = new stdClass;
$mb_data->recent_views = array_values($recent_views_dataset->getData());
$mb_data->all_views = array_values($all_views_dataset->getData());

$view_chart = new ZippyCourses_CourseActivity_Chart($course_id);
$view_chart->fetchData();

$join_chart = new ZippyCourses_CourseJoin_Chart($course_id);
$join_chart->fetchData();
?>

<div class="zippy-courses-course-analytics">
    <div class="zippy-course-analytics-charts">
        <div class="zippy-course-analytics-chart">
            <h3><?php _e('Course Views', ZippyCourses::TEXTDOMAIN); ?></h3>
            <?php $view_chart->show(); ?>
        </div>
        <div class="zippy-course-analytics-chart">
            <h3><?php _e('New Students in Course', ZippyCourses::TEXTDOMAIN); ?></h3>
            <?php $join_chart->show(); ?>
        </div>
    </div>

    <div class="zippy-course-analytics-tables">
        <script type="text/json" class="zippy-mb-data"><?php echo json_encode($mb_data); ?></script>
        <div class="zippy-course-activity-table">
            <?php include (ZippyCourses::$path . 'assets/views/templates/admin/analytics/course-activity-table.php'); ?>
        </div>
    </div>    
</div>
