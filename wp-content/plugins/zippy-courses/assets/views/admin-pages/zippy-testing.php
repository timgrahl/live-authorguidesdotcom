<?php

global $wpdb;

$zippy = Zippy::instance();

$data = get_post_meta(14289, 'certificate', true);

$certificate = new Zippy_CourseCertificate($data);

echo $certificate->render();
