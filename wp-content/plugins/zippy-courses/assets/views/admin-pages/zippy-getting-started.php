<div class="wrap about-wrap">

    <h1><?php _e('Welcome to Zippy Courses', ZippyCourses::TEXTDOMAIN); ?></h1>

    <div class="about-text">
        <p><?php _e('Thank you for installing Zippy Courses. You\'re a few short steps away from selling courses online.', ZippyCourses::TEXTDOMAIN); ?></p>
    </div>

    <h3><?php _e('Here\'s How to Get Started Quickly', ZippyCourses::TEXTDOMAIN); ?></h3>


    <div class="feature-section">

        <h4><?php _e('1. Configure Your Zippy Courses Settings', ZippyCourses::TEXTDOMAIN); ?></h4>

        <p><?php _e('Don\'t worry, there aren\'t many of them.', ZippyCourses::TEXTDOMAIN); ?></p>

        <p><?php _e('First, head over to the settings page and enter your License Key.  You should have received this when you purchased this plugin. By entering your license key, you\'ll get access to security and feature updates, as well as a direct line to <a href="https://zippycourses.com/contact/" target="_blank">Customer Support</a>.', ZippyCourses::TEXTDOMAIN); ?></p>

        <p><?php printf(__('Next, click over to the <a href="%s">Payment Settings</a>. Choose your payment gateway of choice and enter your account details.  This will allow you to sell the courses you create - and get paid for it!', ZippyCourses::TEXTDOMAIN), admin_url('admin.php?page=zippy-settings&tab=zippy_settings_payment')); ?></p>

        <p><?php printf(__('Lastly and optionally (really, you don\'t have to do anything else if you don\'t want to), you can enable a third party email service such as AWeber or MailChimp using the <a href="%s">Email Lists settings</a> and personalize the emails that your users receive after registration by visiting the <a href="%s">System Email settings</a>', ZippyCourses::TEXTDOMAIN), admin_url('admin.php?page=zippy-settings&tab=zippy_settings_email_lists'), admin_url( 'admin.php?page=zippy-settings&tab=zippy_settings_email_lists')); ?></p>

        <p><?php _e('All data associated with a payment can now be edited as well, including the customer\'s billing address.', ZippyCourses::TEXTDOMAIN); ?></p>

        <p><a href="<?php echo admin_url('admin.php?page=zippy-settings'); ?>" class="button button-primary"><?php _e('Visit Your Zippy Courses Settings', ZippyCourses::TEXTDOMAIN); ?></a></p>

        <h4><?php _e('2.  Create a Course', ZippyCourses::TEXTDOMAIN); ?></h4>

        <p><?php _e('Zippy Courses makes the creation and operation of courses on your WordPress site extremely simple.', ZippyCourses::TEXTDOMAIN); ?></p>

        <p><?php printf(__('<a href="%s">Visit the Dashboard</a> and hit "Add New". Fill out the details of your course and when you\'re ready, start clicking "Add Lesson" and "Add Unit" to create new content within your course.', ZippyCourses::TEXTDOMAIN), admin_url( 'admin.php?page=zippy-courses' )); ?></p>

        <p><?php _e('After all of the lessons are created, you have two options: ', ZippyCourses::TEXTDOMAIN); ?></p>
                        
        <ul>
            <li><?php _e('Visit the Products tab in Zippy Courses to create a product for selling your course.', ZippyCourses::TEXTDOMAIN); ?></li>
            <li><?php _e('Edit the content of each Unit or Lesson by clicking on the "Edit" button (it looks like a pencil).', ZippyCourses::TEXTDOMAIN); ?></li>
        </ul>

        <p><?php _e('And lastly, if you have chosen to use a Zippy Courses Email list integration, you can control which list Course Members will be added to when they join the course in the Email Lists tab.', ZippyCourses::TEXTDOMAIN); ?></p>

        <p><?php _e('Don\'t worry.  We have made every effort to make this process simple and straight forward. No secret levers to pull.  If you ever get lost, feel free to view the documentation or contact support.', ZippyCourses::TEXTDOMAIN); ?></p>

        <p><a href="<?php echo admin_url('admin.php?page=zippy-courses'); ?>" class="button button-primary"><?php _e('Create a Course', ZippyCourses::TEXTDOMAIN); ?></a> <a href="https://zippycourses.com/docs/" target="_blank" class="button"><?php _e('View Documentation', ZippyCourses::TEXTDOMAIN); ?></a></p>
         
        <h4><?php _e('3.  Start Selling', ZippyCourses::TEXTDOMAIN); ?></h4>
                        
        <p><?php _e('If you didn\'t in Step 2, click on the "Products" tab to create a product to sell your course. When you create a product, you can choose how you\'d like to sell your course: as a one time payment, a subscription or a payment plan.  You can create multiple payment options (such as a one time payment AND a payment plan) to give your customers options.', ZippyCourses::TEXTDOMAIN); ?></p>

        <p><?php _e('Once the Product is created, you can click the "View Product" button next to any Product to see the sales page and get the URL to send your customers to.', ZippyCourses::TEXTDOMAIN); ?></p>

    </div>

    <h3><?php _e('That\'s it?', ZippyCourses::TEXTDOMAIN); ?></h3>

    <div class="feature-section">

        <h4><em><?php _e('Yes.', ZippyCourses::TEXTDOMAIN); ?></em></h4>

        <p><?php _e('We made every effort to make this as painless to get up and running.  If your content is ready to go and you have a PayPal, you can literally be selling your first course within 1-2 hours - give or take a bit depending on your copy and paste speed =P', ZippyCourses::TEXTDOMAIN); ?></p>


        <h4><?php _e('If you ever have a question or need a hand...', ZippyCourses::TEXTDOMAIN); ?></h4>

        <p><?php _e('You can always refer back to the <a href="https://zippycourses.com/docs/">documentation</a> or <a href="https://zippycourses.com/contact/">contact support</a>. We\'ll always do whatever we can to help.', ZippyCourses::TEXTDOMAIN); ?></p>

    </div>

    <h2 style="text-align: center;"><?php _e('Thanks again!', ZippyCourses::TEXTDOMAIN); ?></h2>

    <h3 style="text-align: center;"><?php _e('And welcome to Zippy Courses', ZippyCourses::TEXTDOMAIN); ?></h3>

</div>
