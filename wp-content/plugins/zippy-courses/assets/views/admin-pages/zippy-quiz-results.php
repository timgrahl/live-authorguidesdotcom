<?php

global $wpdb;

$zippy = Zippy::instance();

$quiz_submission_id = filter_input(INPUT_GET, 'id');

$row = $wpdb->get_row("SELECT * FROM $wpdb->usermeta WHERE umeta_id = $quiz_submission_id");

if (is_object($row)) {
    $student = $zippy->make('student', array($row->user_id));
    $student->fetch();

    $result = $zippy->utilities->maybeJsonDecode($row->meta_value);
    echo '<div class="wrap">';
    echo '<h2>' . sprintf(__('Results for %s Quiz', ZippyCourses::TEXTDOMAIN), '<strong>' . get_the_title($result->quiz) . '</strong>') . '</h2>';

    echo '<h3>' . __('Student', ZippyCourses::TEXTDOMAIN) . ': <span style="font-weight: 400;">' . $student->getFullName() . '</span></h3>';
    echo '<h3>' . __('Score', ZippyCourses::TEXTDOMAIN) . ': <span style="font-weight: 400;">' . $result->score . '%</span></h3>';

    echo '<hr/>';

    $i = 1;
    foreach ($result->questions as $question) {
        echo "<h4>$i. $question->question</h4>";
        if ($question->correct == 'true') {
            echo '<p><strong>' . __('Given Answer:', ZippyCourses::TEXTDOMAIN) . '</strong> ' . $question->answer->correct->content . '</p>';
        } else {
            echo '<p><s><strong>' . __('Given Answer:', ZippyCourses::TEXTDOMAIN) . '</strong> '  . $question->answer->given->content . '</s><br/>';
            echo '<strong>' . __('Correct Answer:', ZippyCourses::TEXTDOMAIN) . '</strong> '  . $question->answer->correct->content . '</p>';
        }
        $i++;
    }

    $profile_url        = admin_url('admin.php?page=zippy-student&ID=' . $student->getId());
    $submissions_url    = admin_url('admin.php?page=zippy-analytics&tab=quiz-submissions');
        
    echo '<p>';
        echo '<a href="' . $submissions_url . '" class="button button-primary">&laquo; ' . __('Back to All Quiz Submissions', ZippyCourses::TEXTDOMAIN) . '</a> ';
        echo '<a href="' . $profile_url . '" class="button">' . __('View Student Profile', ZippyCourses::TEXTDOMAIN) . '</a> ';
    echo '</p>';

    echo '</div>';
}
