<?php

$zippy = Zippy::instance();

$user_id = filter_input(INPUT_GET, 'ID');
$quiz    = filter_input(INPUT_GET, 'quiz');
$results = $zippy->utilities->quiz->getResult($quiz);

if (!$results || !$user_id) {
    return;
}

$user = get_userdata($user_id);
$timestamp = $zippy->utilities->datetime->getDateFromTimestamp($results->submitted);

?>

<h1>
<?php
    printf(
        __('Results of %s quiz for %s', ZippyCourses::TEXTDOMAIN),
        '<strong>' . get_the_title($results->quiz) . '</strong>',
        '<strong>' . $user->display_name . '</strong>'
    );
?>
</h1>

<?php foreach ($results->questions as $key => $question) : ?>
    <div class="zippy-quiz-question">
        <h3><?php echo ($key + 1); ?>. <?php echo $question->question; ?></h3>
        <p>
            <?php if (!$question->correct): ?><s><?php endif; ?>
            <strong><?php _e('Selected Answer', ZippyCourses::TEXTDOMAIN); ?>:</strong> <?php echo $question->answer->given->content; ?>
            <?php if (!$question->correct): ?></s><?php endif; ?>
            <br/>
            <strong><?php _e('Correct Answer', ZippyCourses::TEXTDOMAIN); ?>:</strong> <?php echo $question->answer->correct->content; ?>
        </p>
    </div>
<?php endforeach; ?>

<p>
    <a href="<?php echo admin_url('admin.php?page=zippy-student&ID=' . $user_id); ?>" class="button button-primary">Return to Student Profile</a>
    <a href="<?php echo admin_url('edit.php?post_type=quiz'); ?>" class="button">Return to Quizzes</a>
</p>