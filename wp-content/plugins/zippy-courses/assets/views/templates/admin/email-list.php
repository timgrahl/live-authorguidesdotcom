<?php 
$id = filter_input(INPUT_GET, 'post');
$type = get_post_type($id);
?>

<script type="text/template" id="email-list-integration-tmpl">
    <div class="zippy-email-integration">
        <?php if ($type == 'course'): ?>
        <div class="zippy-mb-actions">
            <label><input type="checkbox" v-model="per_tier" /> <?php printf(__('Integrate email lists by Tier, instead of for the entire %s.', ZippyCourses::TEXTDOMAIN), ucwords($type)); ?></label>
        </div>

        <ul v-if="per_tier" class="course-tier-email-list">
            <li
                v-repeat="tiers"
                lists="lists"
                inline-template
            >
                <h4><strong><?php _e('TIER:', ZippyCourses::TEXTDOMAIN); ?></strong> {{ title | uppercase }}</h4>
                <strong><?php _e('List:', ZippyCourses::TEXTDOMAIN); ?></strong> <select v-model="list" options="lists"></select>
            </li>
        </ul>
        <?php endif; ?>

        <ul v-if="!per_tier" class="course-tier-email-list">
            <li
                class="tier-email-list"
                inline-template>
                <strong><?php _e('List:', ZippyCourses::TEXTDOMAIN); ?></strong> <select v-model="list" options="lists"></select>
            </li>
        </ul>
    </div>
</script>
