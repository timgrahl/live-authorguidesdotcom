<script type="text/template" id="widget-settings-template">
<div>
    <div class="zippy-widget-settings-control">
        <p>
            <label><?php _e('Widget Visibility', ZippyCourses::TEXTDOMAIN); ?>:</label>
            <select v-model="type">
                <option value="0"><?php _e('All', ZippyCourses::TEXTDOMAIN); ?></option>
                <option value="1"><?php _e('Students Only', ZippyCourses::TEXTDOMAIN); ?></option>
                <option value="2"><?php _e('Non-Students Only', ZippyCourses::TEXTDOMAIN); ?></option>
                <option value="3"><?php _e('Per Course &amp; Tier', ZippyCourses::TEXTDOMAIN); ?></option>
            </select>
        </p>
    </div>

    <div class="zippy-widget-settings-courses" v-show="type == '3'">
        <h4><?php _e('Courses &amp; Tiers', ZippyCourses::TEXTDOMAIN); ?></h4>
        <course v-repeat="courses" class="zippy-widget-settings-course" inline-template>
            <div>
                <p class="course-title">{{ title }}</p>

                <ul v-show="tiers.length > 0">
                    <tier v-repeat="tiers" class="zippy-widget-settings-course-tier" inline-template>
                        <li><input type="checkbox" v-model="access" /> {{ title }}</li>
                    </tier>
                </ul>

                <p v-show="tiers.length == 0"><em><?php _e('This course has no tiers', ZippyCourses::TEXTDOMAIN); ?></em></p>
            </div>
        </course>
    </div>
   
</div>
</script>
