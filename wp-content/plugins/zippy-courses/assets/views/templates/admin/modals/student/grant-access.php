<?php
$zippy = Zippy::instance();
$student_id = filter_input(INPUT_GET, 'ID');
$products = array();

$course_ids = $zippy->utilities->course->getAllCourseIds();

if ($student_id) {
    $student = $zippy->make('student', array($student_id));
    $student->fetch();

    $student_products = $student !== null
        ? $student->getProductsOwned()
        : array();
}

?>
<div class="modal" id="zippy-student-grant-access-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form method="GET">
            <input type="hidden" name="page" value="zippy-student" />
            <input type="hidden" name="ID" value="<?php echo $student_id; ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Grant Access', ZippyCourses::TEXTDOMAIN); ?></h4>
                </div>
                <div class="modal-body">
                    <p><?php _e('Which product would you like to give this student access to?', ZippyCourses::TEXTDOMAIN); ?></p>
                   
                    <select name="grant_access">
                        <option></option>
                    <?php foreach ($course_ids as $course_id): ?>
                        <optgroup label="<?php echo get_the_title($course_id); ?>">
                        <?php 
                        $product_ids = $zippy->utilities->course->getProductIdsForCourse($course_id);

                        foreach ($product_ids as $product_id) :
                            if (in_array($product_id, $student_products)) : ?>
                                <option value="<?php echo $product_id; ?>" disabled>
                                    <?php echo get_the_title($product_id); ?> 
                                    (<?php _e('Owned', ZippyCourses::TEXTDOMAIN); ?>)
                                </option>
                            <?php else : ?>
                                <option value="<?php echo $product_id; ?>"><?php echo get_the_title($product_id); ?></option>
                            <?php endif;
                        endforeach;
                        
                        ?>
                        </optgroup>
                    <?php endforeach; ?>
                    </select>

                </div>
                <div class="modal-footer">

                    <button class="button" data-dismiss="modal"><?php _e('Close', ZippyCourses::TEXTDOMAIN); ?></button>
                    <input type="submit" class="button button-primary" value="<?php _e('Grant Access', ZippyCourses::TEXTDOMAIN); ?>" />
                </div>      
            </div>
        </form>
    </div>
</div>

