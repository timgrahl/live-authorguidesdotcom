<script type="text-template" id="zippy-access-bulk-edit-modal-tmpl">
    <div class="modal" id="zippy-access-bulk-edit-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Bulk Edit Tier Access for Course Entries', ZippyCourses::TEXTDOMAIN); ?></h4>
                </div>
                
                <div class="modal-body">
                    <div class="modal-section">
                        <p style="margin: 0;"><?php _e('Select the tiers you would like to give bulk access to', ZippyCourses::TEXTDOMAIN); ?>:</p>

                        <ul style="margin: 1em 0;">
                            <li v-repeat="tiers">
                                <label><input type="checkbox" v-model="selected" /> {{ title }}</label>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="button" data-dismiss="modal"><?php _e('Cancel', ZippyCourses::TEXTDOMAIN); ?></button>
                    <button class="button button-primary" v-on="click : bulkAssign"><?php _e('Assign Teirs', ZippyCourses::TEXTDOMAIN); ?></button>
                </div>      
            </div>
        </div>
    </div>
</script>