<script type="text/template" id="zippy-add-additional-content-modal-tmpl">
    <div class="modal" id="zippy-add-additional-content-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Add Existing Content', ZippyCourses::TEXTDOMAIN); ?></h4>
                </div>
                <div class="modal-body" style="padding-bottom: 0;">
                    <ul class="zippy-post-types-list">
                        <li v-repeat="post_type : post_types">
                            <h4 v-on="click : switchPostType" v-class="active : post_type.active">
                                {{ post_type.label }}
                                <span style="font-weight: 300;" v-show="selectedCount(post_type.post_type) > 0">({{ selectedCount(post_type.post_type) }} <?php _e('selected', ZippyCourses::TEXTDOMAIN); ?> <span v-on="click : clearSelected(post_type.post_type)" class="inline-clear-selection">&times;</span>)</span>

                            </h4>
                        </li>
                    </ul>
                    
                    <div class="zippy-post-type-items">

                        <notice
                            v-repeat="messages"
                        ></notice>
                        
                        <h3>{{ active_post_type_label }} <input v-model="title_filter" placeholder="<?php _e('Filter', ZippyCourses::TEXTDOMAIN); ?> {{ active_post_type_label }}" /></h3>

                        <div v-if="active_list.length > 0">
                            <ul>
                                <li
                                    v-repeat="item : active_list">
                                    <label>
                                        <input type="checkbox" value="{{ item.ID }}" v-model="item.selected" />
                                        <template v-if="item.title.length == 0"><em><?php _e('(no title)', ZippyCourses::TEXTDOMAIN); ?></em></template>
                                        <template v-if="item.title.length > 0">{{ item.title }}</template>
                                    </label>
                                </li>   
                            </ul>
                        </div>

                        <div v-if="active_list.length < 1">
                            <p><em><?php _e('There are no additional entries of this post type to add.', ZippyCourses::TEXTDOMAIN); ?></em></p>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button class="button" data-dismiss="modal"><?php _e('Cancel', ZippyCourses::TEXTDOMAIN); ?></button>
                    <button class="button button-primary" v-on="click : addAdditionalContent"><?php _e('Add Additional Content', ZippyCourses::TEXTDOMAIN); ?></button>
                </div>      
            </div>
        </div>
    </div>
</script>