<script type="text/template" id="zippy-access-config-modal-tmpl">
    <div class="modal" id="zippy-access-config-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Tier Access Configuration', ZippyCourses::TEXTDOMAIN); ?></h4>
                </div>
                <div class="modal-body">

                    <div class="modal-section">
                        <h4><?php _e('Control the Tiers of content in this Course using...', ZippyCourses::TEXTDOMAIN); ?></h4>

                        <ul>
                            <li><p><label><input type="radio" name="access[use]" v-model="config.access.use" value="unit" /> <?php _e('Units', ZippyCourses::TEXTDOMAIN); ?></label></p></li>
                            <li><p><label><input type="radio" name="access[use]" v-model="config.access.use" value="item"/> <?php _e('Lessons', ZippyCourses::TEXTDOMAIN); ?></label></p></li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="button button-primary" data-dismiss="modal"><?php _e('Close', ZippyCourses::TEXTDOMAIN); ?></button>
                </div>      
            </div>
        </div>
    </div>
</script>