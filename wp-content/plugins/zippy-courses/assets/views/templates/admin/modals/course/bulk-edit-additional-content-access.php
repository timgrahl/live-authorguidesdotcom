<script type="text/template" id="zippy-bulk-edit-additional-content-access-modal-tmpl">
    <div class="modal" id="zippy-bulk-edit-additional-content-access-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Bulk Edit Tier Access for Additional Content', ZippyCourses::TEXTDOMAIN); ?></h4>
                </div>
                <div class="modal-body">

                    <?php _e('Every Entry in the Course should have access to the following Tiers:', ZippyCourses::TEXTDOMAIN); ?>

                    <ul>
                        <li v-repeat="tiers">
                            <input type="checkbox" v-model="selected"> {{ title }}
                        </li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button class="button" data-dismiss="modal"><?php _e('Cancel', ZippyCourses::TEXTDOMAIN); ?></button>
                    <button class="button button-primary" v-on="click : bulkAssign"><?php _e('Assign Tiers', ZippyCourses::TEXTDOMAIN); ?></button>
                </div>      
            </div>
        </div>
    </div>
</script>