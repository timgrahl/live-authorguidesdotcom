<script type="text/template" id="zippy-delete-additional-content-entry-modal-tmpl">
    <div class="modal" id="zippy-delete-additional-content-entry-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Delete Additional Content Entry', ZippyCourses::TEXTDOMAIN); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="modal-section">
                        <h4>
                        <?php printf(_e('Are you sure you want to delete <em>%s</em>?', ZippyCourses::TEXTDOMAIN), '{{ passed_data.title }}'); ?>
                        </h4>

                        <p class="description"><?php _e('<strong>Note:</strong> Deleting an Entry from your Course\'s Additional Content will perform a "soft delete," removing it from the Course, but keeping it on your site in case you need it again.', ZippyCourses::TEXTDOMAIN); ?></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="button" data-dismiss="modal"><?php _e('Cancel', ZippyCourses::TEXTDOMAIN); ?></button>
                    <button class="button button-primary" v-on="click : deleteEntry"><?php _e('Yes, delete this entry.', ZippyCourses::TEXTDOMAIN); ?></button>
                </div>      
            </div>
        </div>
    </div>
</script>