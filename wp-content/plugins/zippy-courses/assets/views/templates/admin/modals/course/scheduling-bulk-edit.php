<script type="text/template" id="zippy-scheduling-bulk-edit-modal-tmpl">
    <div class="modal" id="zippy-scheduling-bulk-edit-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Every', ZippyCourses::TEXTDOMAIN); ?> {{ item_type | capitalize }}<sup v-if="item_type == 'unit'">*</sup> <?php _e('in the Course should be available...', ZippyCourses::TEXTDOMAIN); ?></h4>
                </div>
                <div class="modal-body">
                    <span v-if="config.scheduling.control.type == 'previous'"><?php _e('on', ZippyCourses::TEXTDOMAIN); ?></span>

                    <ul class="scheduling-days" v-if="config.scheduling.control.type == 'previous'">
                        <li 
                            v-repeat="day : days">
                            <label 
                                v-class="active : scheduling_options.day_of_week === day.key">
                                <input 
                                    type="radio" 
                                    value="{{ day.key }}" 
                                    v-model="scheduling_options.day_of_week" />
                                {{ day.val }}
                            </label>
                        </li>
                    </ul>

                    <span v-if="config.scheduling.control.type == 'previous'"><?php _e('or', ZippyCourses::TEXTDOMAIN); ?></span>

                    <input v-model="scheduling_options.num_days"> 

                    <?php printf(__('day(s) after the previous %s', ZippyCourses::TEXTDOMAIN), '{{ item_type | capitalize }}'); ?><sup v-if="item_type == 'unit'">*</sup>.

                    <p class="footnote" v-if="item_type == 'unit'"><sup>*</sup>: <?php _e('If an Entry is contained within your Course, but is not part of a Unit, it\'s scheduling will be affected by this change.', ZippyCourses::TEXTDOMAIN); ?></p>
                </div>
                <div class="modal-footer">
                    <button class="button" data-dismiss="modal"><?php _e('Cancel', ZippyCourses::TEXTDOMAIN); ?></button>
                    <button class="button button-primary" v-on="click : bulkSchedule"><?php _e('Apply Scheduling to Course', ZippyCourses::TEXTDOMAIN); ?></button>
                </div>      
            </div>
        </div>
    </div>
</script>
