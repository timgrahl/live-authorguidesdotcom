<script type="text/template" id="zippy-add-unit-modal-tmpl">
    <div class="modal" id="zippy-add-unit-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Add a New Unit', ZippyCourses::TEXTDOMAIN); ?></h4>
                </div>
                <div class="modal-body">
                    <p><?php _e('How many lessons should your new Unit have?', ZippyCourses::TEXTDOMAIN); ?></p>

                    <p>
                        <input type="number" min="0" v-model="num_lessons" number/>
                        <button
                            class="button button-primary"
                            v-on="click : addUnitWithLessons"
                        ><?php _e('Add Unit', ZippyCourses::TEXTDOMAIN); ?></button>
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="button button-primary" data-dismiss="modal"><?php _e('Close', ZippyCourses::TEXTDOMAIN); ?></button>
                </div>      
            </div>
        </div>
    </div>
</script>