<script type="text/template" id="zippy-delete-entry-modal-tmpl">
    <div class="modal" id="zippy-delete-entry-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Delete Entry', ZippyCourses::TEXTDOMAIN); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="modal-section">
                        <h4><?php printf(__('Are you sure you want to delete <em>%s</em>?', ZippyCourses::TEXTDOMAIN), '{{ passed_data.title }}'); ?></h4>

                        <ul v-if="passed_data.post_type == 'unit'">
                            <li><label><input type="radio" v-model="soft_delete" value="yes" /><?php _e('Yes, but keep the Unit\'s Entries as part of the Course.', ZippyCourses::TEXTDOMAIN); ?></label></li>
                            <li><label><input type="radio"  v-model="soft_delete" value="no" /><?php _e('Yes, and remove all of the Unit\'s Entries from the course as well.', ZippyCourses::TEXTDOMAIN); ?></label></li>
                        </ul>

                        <ul v-if="passed_data.post_type != 'unit'">
                            <li><label><input type="radio" v-model="soft_delete" value="yes" /><?php _e('Yes, delete this entry.', ZippyCourses::TEXTDOMAIN); ?></label></li>
                        </ul>

                        <p class="description"><?php _e('<strong>Note:</strong> Deleting an Entry from your Course will perform a "soft delete," removing it from the Course, but keeping it on your site in case you need it again.', ZippyCourses::TEXTDOMAIN); ?></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="button" data-dismiss="modal"><?php _e('Cancel', ZippyCourses::TEXTDOMAIN); ?></button>
                    <button class="button button-primary" v-on="click : deleteEntry"><?php _e('Close &amp; Delete', ZippyCourses::TEXTDOMAIN); ?></button>
                </div>
            </div>
        </div>
    </div>
</script>
