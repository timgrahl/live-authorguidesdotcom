<script type="text/template" id="zippy-scheduling-config-modal-tmpl">
    <div class="modal" id="zippy-scheduling-config-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Scheduling Configuration', ZippyCourses::TEXTDOMAIN); ?></h4>
                </div>
                <div class="modal-body">

                    <scheduling-type
                        class="modal-section"
                        scheduling="{{ config.scheduling }}">
                    </scheduling-type>

                    <scheduling-control
                        class="modal-section"
                        scheduling="{{ config.scheduling }}">
                    </scheduling-control>
                    
                    <scheduling-start-type
                        class="modal-section"
                        v-show="config.scheduling.type != 'all'"
                        scheduling="{{ config.scheduling }}">
                    </scheduling-start-type>
                    
                    <scheduling-end-type
                        class="modal-section"
                        scheduling="{{ config.scheduling }}">
                    </scheduling-end-type>

                    <scheduling-method
                        class="modal-section"
                        v-show="config.scheduling.type != 'all'"
                        scheduling="{{ config.scheduling }}">
                    </scheduling-method>

                </div>
                <div class="modal-footer">
                    <button class="button button-primary" data-dismiss="modal"><?php _e('Close', ZippyCourses::TEXTDOMAIN); ?></button>
                </div>      
            </div>
        </div>
    </div>
</script>

<script type="text/template" id="zippy-scheduling-control-section-tmpl">
    <div class="modal-section">
        <h4><?php _e('Control the scheduling of this Course using...', ZippyCourses::TEXTDOMAIN); ?></h4>

        <ul>
            <li><p><label><input type="radio" name="scheduling[control][use]" v-model="scheduling.control.use" value="unit" /> <?php _e('Units', ZippyCourses::TEXTDOMAIN); ?></label></p></li>
            <li><p><label><input type="radio" name="scheduling[control][use]" v-model="scheduling.control.use" value="item"/> <?php _e('Lessons', ZippyCourses::TEXTDOMAIN); ?></label></p></li>
        </ul>
    </div>
</script>

<script type="text/template" id="zippy-scheduling-method-section-tmpl">
    <div class="modal-section">
        <h4><?php _e('Scheduling is based on the time since...', ZippyCourses::TEXTDOMAIN); ?></h4>

        <ul>
            <li><p><label><input type="radio" name="scheduling[control][type]" v-model="scheduling.control.type" value="previous"/> <?php _e('the last lesson was made available.', ZippyCourses::TEXTDOMAIN); ?></label></p></li>
            <li><p><label><input type="radio" name="scheduling[control][type]" v-model="scheduling.control.type" value="start"/> <?php _e('the beginning of the Course.', ZippyCourses::TEXTDOMAIN); ?></label></p></li>
        </ul>
    </div>
</script>

<script type="text/template" id="zippy-scheduling-start-type-section-tmpl">
<div class="modal-section">
    <h4><?php _e('When does the course begin?', ZippyCourses::TEXTDOMAIN); ?></h4>
    <ul>
        <li><label><input type="radio" name="scheduling[start][type]" v-model="scheduling.start.type" value="join" /> <?php _e('when a student joins the course.', ZippyCourses::TEXTDOMAIN); ?></label></li>
        <li><label><input type="radio" name="scheduling[start][type]" v-model="scheduling.start.type" value="date"/> <?php _e('on a fixed start date:', ZippyCourses::TEXTDOMAIN); ?> </label> <input v-datepicker="scheduling.start.date" placeholder="MM/DD/YYYY" /></li>
    </ul>

    <p class="description"><?php _e('<strong>Note:</strong> If you sell a Product that uses <a href="">Launch Windows</a>, a student that purchases that Product will begin this course on the Launch Window\'s Start Date.', ZippyCourses::TEXTDOMAIN); ?></p>
</div>
</script>

<script type="text/template" id="zippy-scheduling-type-section-tmpl">
<div class="modal-section">
    <h4><?php _e('What type of scheduling does this course use?', ZippyCourses::TEXTDOMAIN); ?></h4>
        
    <ul>
        <li><p><label><input type="radio" name="scheduling[type]" v-model="scheduling.type" value="drip" /> <?php _e('Drip Scheduling', ZippyCourses::TEXTDOMAIN); ?></label></p></li>
        <li><p><label><input type="radio" name="scheduling[type]" v-model="scheduling.type" value="all"/> <?php _e('All Access', ZippyCourses::TEXTDOMAIN); ?></label></p></li>
    </ul>
</div>
</script>

<script type="text/template" id="zippy-scheduling-end-type-section-tmpl">
<div class="modal-section">
    <h4><?php _e('When does access to this course expire?', ZippyCourses::TEXTDOMAIN); ?></h4>
    <ul>
        <li><label><input type="radio" name="scheduling.end.type" v-model="scheduling.end.type" value="none" /> <?php _e('Access to this course does not expire.', ZippyCourses::TEXTDOMAIN); ?></label></li>
        <li v-show="scheduling.start.type == 'join'">
            <label><input type="radio" name="scheduling.end.type" v-model="scheduling.end.type" value="duration"/> <input type="number" class="zippy-input-sm" min="0" step="1" v-model="scheduling.end.duration" /> <?php _e(' days after a student joins the course.', ZippyCourses::TEXTDOMAIN); ?> </label>
        </li>
        <li v-show="scheduling.start.type == 'date'">
            <label><input type="radio" name="scheduling.end.type" v-model="scheduling.end.type" value="date"/> <?php _e('On a fixed date:', ZippyCourses::TEXTDOMAIN); ?> </label> <input v-datepicker="scheduling.end.date" placeholder="MM/DD/YYYY" />
        </li>
    </ul>

    <p class="description"><?php _e('<strong>Note:</strong> If you sell a Product that uses <a href="">Launch Windows</a>, a student that purchases that Product will begin this course on the Launch Window\'s Start Date.', ZippyCourses::TEXTDOMAIN); ?></p>
</div>
</script>