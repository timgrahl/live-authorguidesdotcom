<div class="zippy-sortable">
    <download
        v-repeat="entries"
        inline-template
    >
        <div class="entry download">
            <span class="dnd-handle"></span>
            <span class="zippy-entry-delete" v-on="click : $parent.entries.$remove($index)">&times;</span>

            <div class="zippy-row">
                <div class="zippy-col-lg-3 zippy-col-sm-12">
                    <label><?php _e('Title:', ZippyCourses::TEXTDOMAIN); ?></label>
                    <input v-model="title" />    
                </div>
                
                <div class="zippy-col-lg-3 zippy-col-sm-12">
                    <label><?php _e('File Type:', ZippyCourses::TEXTDOMAIN); ?></label>
                    <select v-model="type">
                        <option value="video"><?php _e('Video', ZippyCourses::TEXTDOMAIN); ?></option>
                        <option value="audio"><?php _e('Audio', ZippyCourses::TEXTDOMAIN); ?></option>
                        <option value="pdf"><?php _e('PDF', ZippyCourses::TEXTDOMAIN); ?></option>
                        <option value="image"><?php _e('Image', ZippyCourses::TEXTDOMAIN); ?></option>
                        <option value="slides"><?php _e('Slides', ZippyCourses::TEXTDOMAIN); ?></option>
                        <option value="transcript"><?php _e('Transcript', ZippyCourses::TEXTDOMAIN); ?></option>
                        <option value="other"><?php _e('Other', ZippyCourses::TEXTDOMAIN); ?></option>
                    </select>
                </div>

                <div class="zippy-col-lg-4 zippy-col-sm-12">
                    <label><?php _e('File URL:', ZippyCourses::TEXTDOMAIN); ?></label>
                    <input v-model="url" />

                    <div class="file-actions">
                        <a
                            class="button"
                            target="_blank"
                            v-attr="href : url"
                            v-if="url != ''"
                        >View File</a>
                        <a
                            class="button"
                            v-if="url != ''"
                            v-on="click : removeFile"
                        >Remove File</a>
                        <a
                            class="button"
                            v-if="url == ''"
                            v-on="click : openFileFrame"
                        >Upload File</a>                        
                    </div>
                </div>
            </div>
        </div>
    </download>
</div>