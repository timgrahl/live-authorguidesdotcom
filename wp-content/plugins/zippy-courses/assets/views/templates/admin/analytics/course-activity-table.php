<course-activity-table inline-template>
    <h3><?php _e('Course Activity', ZippyCourses::TEXTDOMAIN); ?></h3>
    <a
        href=""
        class="zippy-tab"
        v-on="click : toggleTimespan"
        v-class="active : mode == 'recent'"
    ><?php _e('Recent', ZippyCourses::TEXTDOMAIN); ?></a>
    <a
        href=""
        class="zippy-tab"
        v-on="click : toggleTimespan"
        v-class="active : mode == 'all'"
    ><?php _e('All Time', ZippyCourses::TEXTDOMAIN); ?></a>
    <table class="zippy-data-table">
        <thead>
            <tr>
                <th v-on="click : sortBy('title')"><?php _e('Title', ZippyCourses::TEXTDOMAIN); ?></th>
                <th v-on="click : sortBy('views')"><?php _e('Views', ZippyCourses::TEXTDOMAIN); ?></th>
                <th v-on="click : sortBy('duration')"><?php _e('Duration', ZippyCourses::TEXTDOMAIN); ?></th>
                <th v-on="click : sortBy('avg_duration')"><?php _e('Average Duration', ZippyCourses::TEXTDOMAIN); ?></th>
            </tr>
        </thead>

        <tbody>
            <tr v-repeat="views | orderBy sortKey reverse">
                <td>{{ title }}</td>
                <td>{{ views }}</td>
                <td>{{ getHumanReadableTime(duration) }}</td>
                <td>{{ getHumanReadableTime(avg_duration) }}</td>
            </tr>
        </tbody>
    </table>
    </div>
</course-activity-table>
