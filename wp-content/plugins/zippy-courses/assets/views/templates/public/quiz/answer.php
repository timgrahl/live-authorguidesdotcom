<li class="answer" v-show="content.length > 0"> 
    <label><input
        type="radio"
        name="question[{{ question }}]"
        value="{{ ID }}"
        v-model="selection"
        v-on="click : $dispatch('change_answer', ID)"
        number
    /> {{ content }}</label>
</li>