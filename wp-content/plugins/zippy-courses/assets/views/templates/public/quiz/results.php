<?php 

global $post;

?>

<quiz-results
    questions="{{@ questions}}"
    passing="{{ passing }}"
    num-questions="{{ numQuestions }}"
    num-correct="{{ numCorrect }}"
    score="{{ score }}"
    v-show="currentView == 'results'"
    inline-template
>
    <div class="zippy-quiz-results">
        <div v-if="!passing" class="zippy-alert zippy-alert-error">
            <p><?php _e('Unfortunately, you did not pass the quiz. Try again? You can do it!', ZippyCourses::TEXTDOMAIN); ?></p>
        </div>

        <p>
        <?php
        printf(
            __("You scored a %s&#37; by answering %s out of %s questions correctly.", ZippyCourses::TEXTDOMAIN),
            '{{ score }}',
            '{{ numCorrect }}',
            '{{ numQuestions }}'
        );
        ?>
        </p>

        <p>
            <a
                href="<?php echo get_permalink($post->id); ?>"
                class="zippy-button"
                v-show="!passing"
            ><?php _e('Return to Lesson', ZippyCourses::TEXTDOMAIN); ?></a>

            <button
                class="zippy-button"
                v-on="click : $dispatch('resetQuiz')"
                v-show="!passing"
            ><?php _e('Try Again', ZippyCourses::TEXTDOMAIN); ?></button>
                
            <button
                class="zippy-button zippy-button-primary"
                v-on="click : $dispatch('resetQuiz')"
                v-show="passing"
            ><?php _e('Next Lesson', ZippyCourses::TEXTDOMAIN); ?></button>
        </p>
    </div>
</quiz-results>