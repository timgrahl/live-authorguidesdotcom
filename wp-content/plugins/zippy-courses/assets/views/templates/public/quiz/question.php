<div class="question" v-show="$index === current">
    <h4>{{ title }}</h4>

    <ul>
        <answer v-repeat="answers" question="{{ id }}" selection="{{ answer }}" inline-template>
            <?php include('answer.php'); ?>
        </answer>
    </ul>
</div>