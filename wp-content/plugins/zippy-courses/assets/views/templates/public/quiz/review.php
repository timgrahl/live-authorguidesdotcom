<quiz-review 
    questions="{{@ questions}}"
    passing="{{ passing }}"
    num-questions="{{ numQuestions }}"
    num-correct="{{ numCorrect }}"
    score="{{ score }}"
    v-show="currentView == 'review'"
    inline-template
>
    <div class="quiz-review">
        <div v-repeat="question in questions">
            <h4 class="title">{{ question.title }}</h4>
            <p>
            
                <strong><?php _e('Your Answer', ZippyCourses::TEXTDOMAIN); ?>:</strong> 
                    <review-question 
                    question="{{@ question}}"
                    inline-template>
                        {{ getSelectedAnswer }}
                    </review-question>
                <small><a v-on="click : $dispatch('changeAnswer', id)">(<?php _e('Change', ZippyCourses::TEXTDOMAIN); ?>)</a></small>
            </p>
        </div>

        <button
            v-on="click : submitQuiz"
        ><?php _e('Submit Quiz', ZippyCourses::TEXTDOMAIN); ?></button>
    </div>
</quiz-review>