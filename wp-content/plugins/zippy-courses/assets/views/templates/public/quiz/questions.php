<div v-show="currentView == 'questions'">
    <quiz-questions
        questions="{{@ questions }}"
        inline-template
    >
        <div class="zippy-quiz-questions">
            <question v-repeat="questions" current="{{ current }}" inline-template>
                <?php include('question.php'); ?>
            </question>
        </div>

        <div class="zippy-quiz-nav">
            <button
                class="zippy-quiz-prev"
                v-on="click : previousQuestion"
                v-if="current > 0"
            ><?php _e('Previous Question', ZippyCourses::TEXTDOMAIN); ?></button>

            <button
                class="zippy-quiz-next"
                v-on="click : nextQuestion"
                v-attr="disabled : currentQuestion.answer === null"
                v-if="(current + 1) < questions.length"
            ><?php _e('Next Question', ZippyCourses::TEXTDOMAIN); ?></button>

            <button
                class="zippy-quiz-review"
                v-on="click : $dispatch('setView', 'review')"
                v-attr="disabled : currentQuestion.answer === null"
                v-if="(current + 1) == questions.length"
            ><?php _e('Review Your Answers', ZippyCourses::TEXTDOMAIN); ?></button>
        </div>
    </quiz-questions>
</div>