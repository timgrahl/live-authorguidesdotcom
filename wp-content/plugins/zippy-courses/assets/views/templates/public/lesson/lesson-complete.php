<?php
$obj    = get_post_type_object(get_post_type());
$label  = $obj->labels->singular_name;
?>
<script type="text/template" id="entry-complete-button-tmpl">
<div>
    <a
        href=""
        v-on="click : completeEntry"
        v-show="!completed"
        class="zippy-button zippy-button-primary zippy-entry-complete-toggle"
    ><?php printf(__('Mark %s Complete', ZippyCourses::TEXTDOMAIN), $label); ?></a>
    <a
        href=""
        v-on="click : uncompleteEntry"
        v-show="completed"
        class="zippy-button button-primary zippy-entry-complete-toggle"
    ><?php _e('Completed!', ZippyCourses::TEXTDOMAIN); ?></a>
</div>
</script>
