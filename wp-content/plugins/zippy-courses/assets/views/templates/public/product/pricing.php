<?php $zippy = Zippy::instance(); ?>
<script type="text/template" id="course-pricing-tmpl">
    <div class="zippy-course-pricing">
        <p class="zippy-product-selection">
            <label><?php _e('Product:', ZippyCourses::TEXTDOMAIN); ?></label>
            <select v-if="hasMultipleOptions" v-model="option" options="options" class="zippy-pricing-option">
            </select>
            <span v-if="!hasMultipleOptions">{{options[0].text}}</span>
        </p>
        <p class="zippy-product-price">
            <label><?php _e('Price:', ZippyCourses::TEXTDOMAIN); ?></label>
            <span v-html="price"></span>
        </p>
        <p>
            <a href="<?php echo $zippy->core_pages->getUrl('buy'); ?>{{ option }}?buy-now" class="zippy-button zippy-button-primary zippy-buy-product">
                <?php  echo apply_filters('zippy_buy_button_text', __('Buy Now', ZippyCourses::TEXTDOMAIN))?>
            </a>
        </p>
    </div>
</script>
