<?php

global $post;

$zippy = Zippy::instance();
$entry_ids = $zippy->utilities->entry->getAllEntryIds();

$student = $zippy->cache->get('student');

$completed = $student !== null && is_object($post) ? $student->isCompleted($post->ID) : false;

$html = '';

if (is_object($post) && in_array($post->ID, $entry_ids)) {
    if (!empty($title)) {
        $html .= $before_title . $title . $after_title;
    }
    
    $html .= '<div class="zippy-complete-entry-region" data-id="' . $post->ID . '" data-status="' . (int) $completed . '"></div>';
}

echo apply_filters('zippy_filter_complete_entry_widget_html', $html, $student, $completed);
