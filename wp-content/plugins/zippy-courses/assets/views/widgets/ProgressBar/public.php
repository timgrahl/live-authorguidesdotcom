<?php
    
global $current_user;
$zippy = Zippy::instance();
$student = $zippy->cache->get('student');
if (empty($student)) {
    return;
}

if ($student->id == 0) {
    $student = $zippy->make('student', array($current_user->ID));
    $student->fetch();
}
$title = apply_filters(
    'widget_title',
    (!empty($title)
        ? $title
        : 'Course Progress'
    ),
    $instance,
    $this->id_base
);
$html = $before_title . $title . $after_title;
foreach ($student->getCourses()->all() as $course) {
    if (empty($course)) {
        continue;
    }
    $progress = $student->getCourseProgress($course);
    $course_title = get_the_title($course->id);
    $html .= '<p class="zippy-progress-bar-course-title"><a href="' . get_permalink($course->id) . '">' . $course_title . '</a></p>';
    $html .='    <div class="zippy-progress">';
    $html.= '        <div class="zippy-progress-bar" role="progressbar" aria-valuenow="' . $progress;
    $html .= '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $progress;
    $html .= '%">';
    
    $html.=  $progress . '% ' . __('Complete', ZippyCourses::TEXTDOMAIN);
    $html.='        </div>';
    if ($progress == 0) {
            $html.= '<span class="zippy-progress-bar-empty">';
            $html.= '0% ' . __('Complete', ZippyCourses::TEXTDOMAIN);
            $html.= '</span>';
    }
    $html.='    </div>';
}

echo apply_filters('zippy_filter_progress_bar_html', $html);
