<?php

global $post, $current_user;

$zippy = Zippy::instance();
$student = $zippy->cache->get('student');
if (!$student && $current_user->ID > 0) {
    $student = $zippy->make('student', array($current_user->ID));
}
if (is_object($post) && $zippy->core_pages->is($post->ID, 'register')) {
    return;
}

$html = '';

if ($student !== null) {
    $title = apply_filters(
        'widget_title',
        (!empty($account_title)
            ? $account_title
            : __('Hi', ZippyCourses::TEXTDOMAIN) . ', ' . $student->getFirstName()
        ),
        $instance,
        $this->id_base
    );

    $html .= $before_title . $title . $after_title;

    $html .= '<ul class="zippy-user-links">';
    $html .= '<li><a href="' . $zippy->core_pages->getUrl('dashboard') . '">' .
        __('View Course Dashboard', ZippyCourses::TEXTDOMAIN) .
    '</a></li>';
    $html .= '<li><a href="' . $zippy->core_pages->getUrl('account') . '">' .
        __('Your Account', ZippyCourses::TEXTDOMAIN) .
    '</a></li>';
    $html .= '<li><a href="' . wp_logout_url(apply_filters('zippy_courses_logout_redirect_url', home_url())) . '">' .
        __('Logout', ZippyCourses::TEXTDOMAIN) .
    '</a></li>';
    $html .= '</ul>';
    
} else {
    $title = apply_filters(
        'widget_title',
        (!empty($login_title)
            ? $login_title
            : __('Login', ZippyCourses::TEXTDOMAIN)
        ),
        $instance,
        $this->id_base
    );

    if (!empty($title)) {
        $html .= $before_title . $title . $after_title;
    }
    
    $html .= do_shortcode('[zippy_login_form]');
}

echo apply_filters('zippy_filter_login_account_widget_html', $html, $student);
