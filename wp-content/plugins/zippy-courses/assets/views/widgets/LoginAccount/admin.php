<p>
    <label for="<?php echo $this->get_field_id('login_title'); ?>"><?php _e('Login Title:', ZippyCourses::TEXTDOMAIN); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'login_title'); ?>" name="<?php echo $this->get_field_name('login_title'); ?>" type="text" value="<?php echo esc_attr($login_title); ?>" />
</p>

<p>
    <label for="<?php echo $this->get_field_id('account_title'); ?>"><?php _e('Account Title:', ZippyCourses::TEXTDOMAIN); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'account_title'); ?>" name="<?php echo $this->get_field_name('account_title'); ?>" type="text" value="<?php echo esc_attr($account_title); ?>" />
    <br/><small><?php _e('Leave empty for it to say "Hi, First Name"', ZippyCourses::TEXTDOMAIN); ?></small>
</p>