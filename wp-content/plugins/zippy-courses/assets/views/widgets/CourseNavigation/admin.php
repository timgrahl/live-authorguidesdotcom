<p>
    <label for="<?php echo $this->get_field_id('course_title'); ?>"><?php _e('Title for Courses:', ZippyCourses::TEXTDOMAIN); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'course_title'); ?>" name="<?php echo $this->get_field_name('course_title'); ?>" type="text" value="<?php echo esc_attr($course_title); ?>" />
</p>

<p>
    <label for="<?php echo $this->get_field_id('unit_title'); ?>"><?php _e('Title for Units:' , ZippyCourses::TEXTDOMAIN); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'unit_title'); ?>" name="<?php echo $this->get_field_name('unit_title'); ?>" type="text" value="<?php echo esc_attr($unit_title); ?>" />
</p>

<p>
    <label for="<?php echo $this->get_field_id('entry_title'); ?>"><?php _e('Title for Entries:', ZippyCourses::TEXTDOMAIN); ?></label> 
    <input class="widefat" id="<?php echo $this->get_field_id( 'entry_title'); ?>" name="<?php echo $this->get_field_name('entry_title'); ?>" type="text" value="<?php echo esc_attr($entry_title); ?>" />
</p>