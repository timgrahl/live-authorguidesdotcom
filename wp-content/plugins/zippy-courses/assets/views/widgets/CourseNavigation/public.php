<?php

global $post;

$zippy = Zippy::instance();
$student = $zippy->cache->get('student');

$middleware = $zippy->middleware->get('student-access');

if (is_object($post) && $student !== null && $middleware->run($post)) {
    $has_navigation = $zippy->utilities->entry->isCourseEntry($post->ID) || $post->post_type == 'course';

    if ($has_navigation) {
        switch ($post->post_type) {
            case 'course':
                $context = 'course';
                break;
            case 'unit':
                $context = 'unit';
                break;
            default:
                $context = 'entry';
                break;
        }

        $course_id      = $context == 'course' ? $post->ID : $zippy->utilities->entry->getCourseId($post->ID);
        $course         = $student->getCourse($course_id);

        if ($course === null) {
            return;
        }

        if ($context == 'entry') {
            $unit  = $zippy->utilities->entry->getEntryUnit($course, $post->ID);

            if ($unit === null) {
                $context = 'course';
            }
        }

        switch ($context) {
            case 'entry':
                $title = apply_filters(
                    'widget_title',
                    (empty($instance['entry_title'])
                        ? __('Other Entries in this Unit', ZippyCourses::TEXTDOMAIN)
                        : $instance['entry_title']
                    ),
                    $instance,
                    $this->id_base
                );

                break;
            case 'unit':
                
                $title = apply_filters(
                    'widget_title',
                    (empty( $instance['unit_title'] )
                        ? __('Entries in this Unit', ZippyCourses::TEXTDOMAIN)
                        : $instance['unit_title']
                    ),
                    $instance,
                    $this->id_base
                );
                break;
            case 'course':
            default:
                $title = apply_filters(
                    'widget_title',
                    (empty($instance['course_title'])
                        ? __('Course Navigation', ZippyCourses::TEXTDOMAIN)
                        : $instance['course_title']
                    ),
                    $instance,
                    $this->id_base
                );

                break;
        }


        $html = '';

        $view = new ZippyCourses_Course_Navigation_View($course, $context);
        $nav = $view->render();

        if (!empty($nav)) {
            if (!empty($title)) {
                $html .= $before_title . $title . $after_title;
            }
            
            $html .= $nav;
            
            if ($context != 'course') {
                $html .= '<p><a href="' . get_permalink($course->getId()) . '" class="zippy-button">';
                    $html .= apply_filters('zippy_text_back_to_course', __('&laquo; Back to Course', ZippyCourses::TEXTDOMAIN));
                $html .= '</a></p>';
            }
        }

        echo apply_filters('zippy_filter_course_navigation_widget_html', $html, $course, $context);
    }
}
