<?php

$zippy = Zippy::instance();
$student = $zippy->cache->get('student');

$html = '';

if ($student !== null) {
    if (!empty($title)) {
        $html .= $before_title . $title . $after_title;
    }
    $settings       = get_option('zippy_customizer_course_options', array());
    $show_progress  = isset($settings['show_progress']) ? $settings['show_progress'] == 1 : true;

    foreach ($student->getCourses()->all() as $course) {
        $html .= '<p class="zippy-course-link"><a href="' . get_permalink($course->getId()) . '">';
        $html .= get_the_title($course->getId()) . '</a>';
        if ($show_progress) {
            $html .= '<br/><small class="zippy-your-courses-progress">' . $student->getCourseProgress($course) . '% ';
            $html .= __('complete', ZippyCourses::TEXTDOMAIN) . '</small>';
        }
        $html .= '</p>';
    }
}

echo apply_filters('zippy_filter_your_courses_widget_html', $html, $student);
