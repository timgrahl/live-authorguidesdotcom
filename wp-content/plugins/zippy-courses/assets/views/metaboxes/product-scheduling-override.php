<?php
/*
Id: zippy-product-scheduling-override
Name: Scheduling Override
Post Types: product
Fields: overrides_enabled, scheduling_override, expiration_override, expiration
Context: normal
Priority: default
Version: 1.0.0
*/

$zippy = Zippy::instance();
?>

<div class="zippy-mb-actions">
    <label><input type="checkbox" value="1" name="overrides_enabled" <?php checked($overrides_enabled, 1); ?> /> <?php _e('Override course scheduling details for owners of this product.', ZippyCourses::TEXTDOMAIN); ?></label>    
</div>

<div class="zippy-table-container">
    <table class="zippy-table zippy-product-scheduling-overrides-table">
        <thead>
            <tr>
                <th>Feature</th>
                <th>Override Enabled?</th>
                <th>Settings</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>
                    All Access
                    <p class="description">Make all content in any course(s) attached to this product available immediately to any owners.</p>
                </th>
                <td>
                    <select name="scheduling_override">
                        <option value="0" <?php selected(0, $scheduling_override); ?>>
                            <?php _e('No', ZippyCourses::TEXTDOMAIN); ?>
                        </option>
                        <option value="1" <?php selected(1, $scheduling_override); ?>>
                            <?php _e('Yes', ZippyCourses::TEXTDOMAIN); ?>
                        </option>
                    </select>
                </td>
                <td><span class="zippy-ghost"><?php _e('Not Applicable', ZippyCourses::TEXTDOMAIN); ?></span></td>
            </tr>
            <tr>
                <th>
                    <?php _e('Expiration Date', ZippyCourses::TEXTDOMAIN); ?>
                    <p class="description"><?php _e('Remove access to the course(s) controlled by this product on a given date or after a certain number of days of access.', ZippyCourses::TEXTDOMAIN); ?></p>
                </th>
                <td>
                    <select name="expiration_override">
                        <option value="0" <?php selected(0, $expiration_override); ?>>
                            <?php _e('No', ZippyCourses::TEXTDOMAIN); ?>
                        </option>
                        <option value="1" <?php selected(1, $expiration_override); ?>>
                            <?php _e('Yes', ZippyCourses::TEXTDOMAIN); ?>
                        </option>
                    </select>
                </td>
                <td>
                    <?php
                    printf(
                        __(
                            'Access to the course expires %s days after a student purchases this product.',
                            ZippyCourses::TEXTDOMAIN
                        ),
                        '<input name="expiration" type="number" min="1" value="' . $expiration . '" class="zippy-input-sm" />'
                    );
                    ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>