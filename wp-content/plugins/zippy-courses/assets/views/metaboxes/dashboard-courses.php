<?php
/*
Id: zippy-dashboard-courses
Name: Your Courses
Post Types: zippy-courses
Context: zippy_admin_dashboard_left
Priority: default
Version:     1.0.0
*/

global $wpdb;

$ignored_statuses = array('"auto-draft"','"trash"', '"inherit"');
$ignored_status_list = implode(', ', $ignored_statuses);

$course_ids = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s AND post_status NOT IN ($ignored_status_list)", 'course'));

$courses = $zippy->make('courses');
$courses->fetchPostsById($course_ids);

$data = array('courses' => array());

foreach ($courses->all() as $course) {
    $obj = new stdClass;
    $obj->id    = $course->getId();
    $obj->title = get_the_title($obj->id);

    $data['courses'][] = $obj;
}
?>

<div class="vue"></div>

<script type="text/json" class="zippy-mb-data"><?php echo json_encode($data); ?></script>

<script type="text/template" id="dashboard-courses-tmpl">
    <div class="zippy-dashboard-courses">

        <div class="zippy-mb-actions">
            <input placeholder="<?php _e('Search...', ZippyCourses::TEXTDOMAIN); ?>" class="course-search" v-model="searchText" />
            <a href="{{ urls.new }}" class="button button-primary">{{ button }}</a>
        </div>

        <course v-repeat="paged_courses"></course>

        <div class="zippy-mb-footer">
            <button
                class="button"
                v-on="click : prevPage"
            >&laquo;</button>
            <span class="course-paging">{{ current_page }} of {{ max_pages }}</span>
            <button
                class="button"
                v-on="click : nextPage"
            >&raquo;</button>
        </div>
    </div>
</script>

<script type="text/template" id="dashboard-courses-course-tmpl">
<div class="course">
    <div class="course-actions">
        <a href="{{ urls.edit }}" class="button button-primary"><?php _e('Edit', ZippyCourses::TEXTDOMAIN); ?></a>
        <a href="{{ urls.view }}" target="_blank" class="button"><?php _e('View', ZippyCourses::TEXTDOMAIN); ?></a>
        <a href="{{ urls.analytics }}" class="button"><?php _e('Analytics', ZippyCourses::TEXTDOMAIN); ?></a>
        <a href="{{ urls.delete }}" v-on="click : deleteCourse" class="button button-delete"><?php _e('Delete', ZippyCourses::TEXTDOMAIN); ?></a>
    </div>

    <h4><a href="{{ urls.edit }}" v-html="title"></a></h4>
</div>
</script>