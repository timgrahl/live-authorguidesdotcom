<?php
/*
Id: zippy-forum-access
Name: Zippy Courses Permissions
Post Types: forum
Fields: zippy_forum_access, zippy_forum_access_enabled
Context: normal
Priority: default
Version:     1.0.0
*/

$zippy = Zippy::instance();

$mb_data = new stdClass;
$mb_data->zippy_forum_access = $zippy_forum_access;
$mb_data->zippy_forum_access_enabled = $zippy_forum_access_enabled ? $zippy_forum_access_enabled : 0;
$mb_data->courses = $zippy->utilities->course->getAllCoursesAndTiers();
?>

<div class="vue"></div>
<input type="hidden" name="zippy_forum_access" value="" />
<input type="hidden" name="zippy_forum_access_enabled" value="" />

<script type="text/json" class="zippy-mb-data"><?php echo json_encode($mb_data); ?></script>

<script type="text/template" id="zippy-forum-access-tmpl">
    <div class="zippy-mb-actions">
        <label><?php _e('Enable per Course control for this forum?', ZippyCourses::TEXTDOMAIN); ?></label>
        <select v-model="enabled">
            <option value="0"><?php _e('No', ZippyCourses::TEXTDOMAIN); ?></option>
            <option value="1"><?php _e('Yes', ZippyCourses::TEXTDOMAIN); ?></option>
        </select>
    </div>

    <course v-show="enabled == 1" v-repeat="courses"></course>
</script>

<script type="text/template" id="zippy-forum-course-tmpl">
<div class="zippy-forum-course">
    <h4>{{ title }}</h4>
    <ul>
        <li v-repeat="tiers">
            <label><input type="checkbox" v-model="checked" value="1" /> {{ title }}</label>
        </li>
    </ul>
</div>
</script>