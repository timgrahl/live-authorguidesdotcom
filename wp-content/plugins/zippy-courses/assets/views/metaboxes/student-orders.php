<?php
/*
Id: zippy-student-orders
Name: Orders
Post Types: zippy-student
Context: zippy_student_right
Priority: default
Order: 9
Version:     1.0.0
*/

$zippy = Zippy::instance();

$student_id = filter_input(INPUT_GET, 'ID');

if ($student_id === false || $student_id === null) {
    return;
}

$query_id = "zippy_owner_{$student_id}_orders";
$query = $zippy->queries->get($query_id);

if ($query) {
    $query->flushResults();
}
$student = $zippy->make('student', array($student_id));
$student->fetch();

$view = new ZippyCourses_StudentOrders_View($student);
$view->show();
