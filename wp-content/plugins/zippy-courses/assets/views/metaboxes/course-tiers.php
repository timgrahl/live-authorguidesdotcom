<?php
/*
Id: zippy-course-tiers
Name: Tiers
Help: Set the tiers for your course?
Data: tiers
Fields: tiers:json
Post Types: course
Context: zippy_course_entries_side
Priority: default
Version:     1.0.0
*/
?>

<div class="vue"></div>
<input type="hidden" name="tiers" value="<?php echo htmlentities(json_encode($tiers)); ?>" />

<script type="text/template" id="course-tier-tmpl">
    <div>
        <div class="zippy-mb-actions">
            <button class="button button-primary" v-on="click : newTier"><?php _e('Add Tier', ZippyCourses::TEXTDOMAIN); ?></button>    
        </div>

        <p class="description"><?php _e('Tiers gives you the ability to allow different levels of access for each piece of content within your course.', ZippyCourses::TEXTDOMAIN); ?></p>

        <ul class="course-tiers">
            <li class="tier" v-repeat="tiers">
                <h5>
                    <input type="text" v-model="title" placeholder="<?php _e('Enter Tier Title', ZippyCourses::TEXTDOMAIN); ?>" class="entry-title tier-title" />
                    <span class="zippy-entry-delete" v-on="click : destroy" v-show="$parent.tiers.length > 1">&times;</span>
                </h5>
            </li>
        </ul>
    </div>
</script>
