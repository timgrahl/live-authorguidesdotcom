<?php
/*
Id: zippy-order-emails
Name: Order Emails
Post Types: zippy_order
Context: side
Priority: default
Order: 9
Version:     1.0.0
*/

global $wpdb;
global $post;
$zippy = Zippy::instance();
$order          = $zippy->cache->get('order');
$has_owner = $order->hasOwner();


?>

<notice v-repeat="messages"></notice>
<p><button class="button" <?php echo $has_owner ? 'disabled' : ''; ?> v-on="click : resend"><?php _e('Resend Registration Details', ZippyCourses::TEXTDOMAIN); ?></button></p>
<?php if ($has_owner) { ?>
    <p><em><?php _e('This order has already been claimed.', ZippyCourses::TEXTDOMAIN); ?></em></p>
<?php } ?>

