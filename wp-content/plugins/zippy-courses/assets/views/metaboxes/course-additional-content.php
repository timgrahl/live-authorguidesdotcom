<?php
/*
Id: zippy-course-additional-content
Name: Additional Content
Data: additional_content
Fields: additional_content:json
Post Types: course
Context: zippy_course_additional_content_main
Priority: default
Version:     1.0.0
*/
?>

<div class="vue"></div>

<input type="hidden" name="additional_content" value="<?php echo htmlentities(json_encode($additional_content)); ?>" />

<script type="text/template" id="course-additional-content-tmpl">
<div class="zippy-course-additional-content">
    <div class="zippy-mb-actions">
        <div class="zippy-mb-action-details">
            <h4><?php _e('Tier Access', ZippyCourses::TEXTDOMAIN); ?></h4>
            <p><strong><?php _e('Actions', ZippyCourses::TEXTDOMAIN); ?>:</strong> 
                <a 
                    href="" 
                    v-on="click : triggerModal" 
                    data-modal="#zippy-bulk-edit-additional-content-access-modal"
                ><?php _e('Bulk Edit', ZippyCourses::TEXTDOMAIN); ?></a>
            </p>
        </div>

        <button
            class="button button-primary"
            v-on="click : triggerModal"
            data-modal="#zippy-add-additional-content-modal"
        ><?php _e('Add Content', ZippyCourses::TEXTDOMAIN); ?></button>
    </div>

    <notice v-repeat="messages"></notice>

    <ul class="course-entries zippy-sortable">
        <entry
            v-repeat="additional_content"
            config="{{ config }}"
            course-tiers="{{ tiers }}"
        ></entry>
    </ul>
</div>
</script>