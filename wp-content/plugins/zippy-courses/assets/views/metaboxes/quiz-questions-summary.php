<?php
/*
Id: zippy-quiz-summary
Name: Questions Summary
Post Types: quiz
Context: zippy_quiz_results_main
Priority: default
Version: 1.0.0
*/

$zippy = Zippy::instance();
$quiz_id = filter_input(INPUT_GET, 'post');

if (empty($quiz_id)) {
?>
<p class="description"><?php _e('This quiz has not been taken by any students yet.', ZippyCourses::TEXTDOMAIN); ?></p>
<?php
    return;
}

$results = new ZippyCourses_QuizResults_Results;
$results->fetchByQuiz($quiz_id);

$summary = $results->exportSummary();

$average_score = $zippy->utilities->quiz->getAverageScore($quiz_id);

$i = 1;
?>

<?php if ($results->count()): ?>
<div class="zippy-mb-actions">
    <p><strong><?php _e('Times Taken', ZippyCourses::TEXTDOMAIN); ?>:</strong> <?php echo $summary->count; ?></p>
    <p><strong><?php _e('Average Score', ZippyCourses::TEXTDOMAIN); ?>:</strong> <?php echo number_format_i18n($average_score, 2); ?>%</p>
</div>

<?php foreach ($summary->questions as $question): ?>
    <?php 
    $correct_rate = $question->response_count > 0
        ? ($question->correct_response_count / $question->response_count) * 100
        : 0;
    ?>
    <div class="zippy-quiz-question-summary">
        <h3><?php echo "$i. {$question->question}"; ?></h3>
        <p class="description"><?php printf(
            __('This question was answered correctly %s  of the time.', ZippyCourses::TEXTDOMAIN),
            floor($correct_rate) . '%'
        ); ?></p>
        <table class="zippy-data-table">
            <thead>
            <tr>
                <th><?php _e('Answer', ZippyCourses::TEXTDOMAIN); ?></th>
                <th><?php _e('Times Answer Was Given', ZippyCourses::TEXTDOMAIN); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($question->answers as $answer): ?>
                <tr>
                    <td><?php echo $answer->answer; ?></td>
                    <td><?php echo $answer->count; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            
        </table>
    </div>
    <?php $i++; ?>
<?php endforeach; ?>

<?php else: ?>
    <br/><p class="description"><?php _e('This quiz has not been taken by any students yet.', ZippyCourses::TEXTDOMAIN); ?></p>
<?php endif; ?>
