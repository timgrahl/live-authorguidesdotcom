<?php
/*
Id: zippy-quiz-questions
Name: Quiz Questions
Data: questions
Fields: questions:json
Post Types: quiz
Context: normal
Priority: default
Version:     1.0.0
*/
?>

<div class="vue"></div>

<input type="hidden" name="questions" value="<?php echo htmlentities(json_encode($questions)); ?>" />

<script type="text/template" id="mb-quiz-questions-tmpl">
<div>
    <div class="zippy-quiz-questions">
        <div class="zippy-mb-actions">
            <button class="button button-primary" v-on="click : addQuestion"><?php _e('Add New Question', ZippyCourses::TEXTDOMAIN); ?></button>
        </div>

        <div class="zippy-sortable">
            <question v-repeat="entries" inline-template>
                <div class="entry question">
                    <span class="dnd-handle" ></span>
                    <span class="zippy-entry-delete" v-on="click : $parent.entries.$remove($index)">&times;</span>
                    <span class="index"><?php _e('Question', ZippyCourses::TEXTDOMAIN); ?> #{{ $index + 1 }}</span>

                    <h4><input v-model="post.post_title" placeholder="<?php _e('Enter Your Question Here', ZippyCourses::TEXTDOMAIN); ?>" lazy/></h4>

                    <answer
                        question="{{ post.ID }}"
                        correct="{{ correct }}"
                        v-repeat="answers"
                        inline-template
                    >
                        <div class="answer">
                            <input 
                                type="radio"
                                name="question[{{ question }}]"
                                value="{{ ID }}"
                                v-model="correct"
                                v-on="click : $dispatch('change_answer', ID)"
                            /> 
                            <input type="text" v-model="content" lazy /> 
                            <span 
                                class="zippy-entry-delete"
                                v-on="click : deleteThis"
                                v-show="$parent.answers.length > 1"
                            >&times;</span>
                        </div>

                    </answer>

                    <p><button class="button button" v-on="click : addAnswer"><?php _e('New Answer', ZippyCourses::TEXTDOMAIN); ?></button></p>
                </div>

            </question>
        </div>
    </div>

    <component is="{{ active_question }}"></component>
</div>
</script>