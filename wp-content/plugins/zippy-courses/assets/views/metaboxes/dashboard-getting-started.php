<?php
/*
Id: zippy-dashboard-getting-started
Name: Getting Started With Zippy Courses
Post Types: zippy-courses
Context: zippy_admin_dashboard_right
Priority: default
Version:     1.0.0
*/

$zippy = Zippy::instance();

$license_key = $zippy->utilities->admin->getLicenseKey();
$completed = array(
    'license_key'   => !empty($license_key),
    'setup_payment' => apply_filters('zippy_active_gateway_configured', false),
    'setup_course'  => $zippy->utilities->course->getCount() > 0,
    'setup_entry'   => count($zippy->utilities->entry->getAllEntryIds()) > 0,
    'setup_product' => $zippy->utilities->product->getCount() > 0,
    'make_sale'     => ($zippy->utilities->orders->getCount() > 0)
);


?>

<ul class="zippy-getting-started">
    <li class="<?php echo isset($completed['license_key']) && $completed['license_key'] ? 'done' : ''; ?>">
        <?php _e('Enter Your License Key', ZippyCourses::TEXTDOMAIN); ?>
        <a href="<?php echo admin_url('admin.php?page=zippy-settings' ); ?>" class="button button-primary">»</a>
    </li>

    <li class="<?php echo isset($completed['setup_payment']) && $completed['setup_payment']  ? 'done' : ''; ?>">
        <?php _e('Configure Payment Settings', ZippyCourses::TEXTDOMAIN); ?>
        <a href="<?php echo admin_url('admin.php?page=zippy-settings&tab=zippy_settings_payment' ); ?>" class="button button-primary">»</a>
    </li>

    <li class="<?php echo isset($completed['setup_course']) && $completed['setup_course']  ? 'done' : ''; ?>">
        <?php _e('Create Your First Course', ZippyCourses::TEXTDOMAIN); ?>
        <a href="<?php echo admin_url('post-new.php?post_type=course' ); ?>" class="button button-primary">»</a>
    </li>

    <li class="<?php echo isset($completed['setup_entry']) && $completed['setup_entry']  ? 'done' : ''; ?>">
        <?php _e('Create a Lesson (edit a course to add a lesson)', ZippyCourses::TEXTDOMAIN); ?>
        <a href="<?php echo admin_url('post-new.php?post_type=course' ); ?>" class="button button-primary">»</a>
    </li>

    <li class="<?php echo isset($completed['setup_product']) && $completed['setup_product']  ? 'done' : ''; ?>">
        <?php _e('Create a Product', ZippyCourses::TEXTDOMAIN); ?>
        <a href="<?php echo admin_url('edit.php?post_type=product' ); ?>" class="button button-primary">»</a>
    </li>

    <li class="<?php echo isset($completed['make_sale']) && $completed['make_sale']  ? 'done' : ''; ?>">
        <?php _e('Make Your First Sale', ZippyCourses::TEXTDOMAIN); ?>
        <a href="<?php echo admin_url('edit.php?post_type=product' ); ?>" class="button button-primary">»</a>
    </li>

</ul>