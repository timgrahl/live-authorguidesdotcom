<?php
/*
Id: zippy-student-ip
Name: Recent IP Addresses
Post Types: zippy-student
Context: zippy_student_left
Priority: default
Version:     1.0.0
*/

$zippy = Zippy::instance();

$student_id = filter_input(INPUT_GET, 'ID');

if ($student_id === false || $student_id === null) {
    return;
}

$student = $zippy->make('student', array('ID' => $student_id));
$ip_addresses = $zippy->utilities->student->getIpAddressesSortedByDate($student);
?>

<h3><?php _e("Recent IP Addresses", ZippyCourses::TEXTDOMAIN); ?></h3>

<ul>
<?php 
for ($i=0; $i < 5; $i++) {
    if (!isset($ip_addresses[$i])) {
        break;
    }
    
    $ip = $ip_addresses[$i];
    $date = $zippy->utilities->datetime->getDateFromTimestamp($ip['time']);
    echo '<p>';
        echo '<a href="http://www.infosniper.net/index.php?ip_address=' . $ip['address'] . '" target="_blank">' . $ip['address'] . '</a><br/>';
        echo '<strong>' . __('Last Visit: ', ZippyCourses::TEXTDOMAIN) . '</strong>' . $date->format('M d, Y g:ia');
    echo '</p>';
}
?>
</ul>