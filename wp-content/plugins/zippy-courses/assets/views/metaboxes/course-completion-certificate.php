<?php
/*
Id: zippy-course-completion-certificate
Name: Certificate of Completion
Help: Add a Certificate of Completion to your Course
Data: certificate
Fields: certificate:json
Post Types: course
Context: zippy_course_entries_main
Priority: default
Version:     1.3.0
*/

global $post;
$permalink = get_permalink($post);
?>

<div class="vue"></div>
<input type="hidden" id="zippy-course-permalink" value="<?php echo $permalink ?>">

<script type="text/template" id="course-certificate-tmpl">
    <input type="hidden" name="certificate" value="{{$data | json}}" />
    <div>
        <div class="zippy-mb-actions">
            <label>
                <input type="checkbox" v-model="enabled"> 
                <?php _e('Add a Certificate of Completion to this Course?', ZippyCourses::TEXTDOMAIN); ?>
            </label>
        </div>
        <div class="zippy-certificates-area" v-show="enabled">
            <table class="zippy-table">
              <tbody>
              <tr>
                <td>
                <label>
                    <?php _e('Certificate Title', ZippyCourses::TEXTDOMAIN); ?>
                  <input type="text" placeholder="Certificate of Completion" v-model="title">
                </label>
                </td>
                <td>
                  <label>
                      <?php _e('Institution Name', ZippyCourses::TEXTDOMAIN); ?>
                      <input type="text" placeholder="<?php echo get_bloginfo(); ?>" v-model="institution">
                  </label>
                </td>
                <td>
                  <label>
                      <?php _e('Instructor Name', ZippyCourses::TEXTDOMAIN); ?>
                      <input type="text" v-model="instructor">
                  </label>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="zippy-certificate-preview">
                    <button class="button" v-on="click:previewCertificate"><?php _e('Preview Certificate', ZippyCourses::TEXTDOMAIN) ?></button>
                  </div>
                </td>  
              </tr>
              </tbody>
            </table>           
    </div>
</script>
