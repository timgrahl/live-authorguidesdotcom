<?php
/*
Id: zippy-order-notes
Name: Notes
Post Types: zippy_order
Context: normal
Priority: default
Version:     1.0.0
*/

global $wpdb, $post;

$zippy = Zippy::instance();

$html = '';

$notes = array_filter((array) get_post_meta($post->ID, 'notes', true));

if (count($notes)) {
    foreach ($notes as $note) {
        $html .= '<p>';
        $html .= '<strong>Zippy Bot</strong> - <em>' . date_i18n(get_option('date_format'), $note['timestamp']) . '</em><br/>';
        $html .= $note['content'];
        $html .= '</p>';
    }
} else {
    $html = '<p><em>' . __('This order has no notes.', ZippyCourses::TEXTDOMAIN) . '</em></p>';
}

echo $html;
