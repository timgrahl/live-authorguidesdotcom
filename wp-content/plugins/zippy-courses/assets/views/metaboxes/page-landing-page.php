<?php
/*
Id: zippy-landing-page-details
Name: Landing Page Details
Post Types: page
Context: normal
Data: landing_page
Fields: landing_page:json
Priority: default
Version:     1.0.0
*/

global $wpdb;

$zippy = Zippy::instance();

$services = $zippy->make('email_list_integration_repository');

$lists = $services->toArray('name');
foreach ($services->all() as $service) {
    if ($service->isEnabled()) {
        $lists[$service->getId()] = $service->api->getLists()->toJSONList('name');
    } else {
        $services->remove($service->getId());
    }
}

$products = $zippy->utilities->product->getAll()->toJSONList('title');

$services_data = new stdClass;
    $services_data->services = $services->toJSONList('name');
    $services_data->lists = $lists;
    $services_data->products = $products;

?>

<div class="vue"></div>
<input type="hidden" name="landing_page" value="<?php echo htmlentities(json_encode($landing_page)); ?>" />
<script type="text/json" id="zippy-email-list-services-data"><?php echo json_encode($services_data); ?></script>

<script type="text/template" id="mb-landing-page-tmpl">
    <div class="landing-page-details">
        
        <p><strong><?php _e('Landing Page Type', ZippyCourses::TEXTDOMAIN); ?>:</strong> <select v-model="type" options="types"></select></p>

        <hr/>

        <div class="landing-page-product-settings" v-if="type == 'product'">
            <h4><?php _e('Product Settings', ZippyCourses::TEXTDOMAIN); ?></h4>

            <p><?php _e('Which product would you like to give access to when someone registers from this landing page?', ZippyCourses::TEXTDOMAIN); ?></p>
            <select v-model="product" options="products"></select>
        </div>

        <div class="landing-page-email-list-settings" v-if="type == 'list'">
            <h4><?php _e('Email List Settings', ZippyCourses::TEXTDOMAIN); ?></h4>

            <p><strong><?php _e('Service', ZippyCourses::TEXTDOMAIN); ?>:</strong> <select v-model="service" options="services"></select>

            <p v-if="service != ''"><strong><?php _e('List', ZippyCourses::TEXTDOMAIN); ?>:</strong> <select v-model="list" options="service_lists"></select></p>
            
            <p><strong><?php _e('Thank You Page URL', ZippyCourses::TEXTDOMAIN); ?>:</strong> <input v-model="thank_you_url" placeholder="<?php _e('Thank You Page URL', ZippyCourses::TEXTDOMAIN); ?>" /></p>
        </div>

        <hr/>

        <div class="landing-page-visitor-settings">
            <h4><?php _e('Visitor Form', ZippyCourses::TEXTDOMAIN); ?></h4>

            <p><input v-model="visitor.headline" placeholder="<?php _e('Headline', ZippyCourses::TEXTDOMAIN); ?>" />
            <p class="description"><?php _e('Prompt will display above the registration form.', ZippyCourses::TEXTDOMAIN); ?></p>

            <p><input v-model="visitor.button" placeholder="<?php _e('Button Text', ZippyCourses::TEXTDOMAIN); ?>" /></p>
            <p class="description"><?php _e('This will be the text that displays in the button of the Registration form button.', ZippyCourses::TEXTDOMAIN); ?></p>

            <p><input v-model="disclaimer" placeholder="<?php _e('Disclaimer', ZippyCourses::TEXTDOMAIN); ?>" /></p>
            <p class="description"><?php _e('This text will be displayed below the form as a Disclaimer.', ZippyCourses::TEXTDOMAIN); ?></p>
        </div>
        
        <div class="landing-page-student-settings">
            <h4><?php _e('Student Form', ZippyCourses::TEXTDOMAIN); ?></h4>

            <p><input v-model="student.headline" placeholder="<?php _e('Headline', ZippyCourses::TEXTDOMAIN); ?>" /></p>
            <p class="description"><?php _e('Prompt will display above the registration form.', ZippyCourses::TEXTDOMAIN); ?></p>

            <p><input v-model="student.button" placeholder="<?php _e('Button Text', ZippyCourses::TEXTDOMAIN); ?>" /></p>
            <p class="description"><?php _e('This will be the text that displays in the button of the Join button.', ZippyCourses::TEXTDOMAIN); ?></p>  
        </div>
    </div>
</script>