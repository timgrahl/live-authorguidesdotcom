<?php
/*
Id: zippy-entry-downloads
Name: Downloads
Fields: downloads:json
Data: downloads
Post Types: lesson, unit, post, page
Context: normal
Priority: default
Version: 1.0.0
*/
?>

<div class="zippy-downloads">
    <div class="zippy-mb-actions">
        <button
            class="button button-primary"
            v-on="click : addDownload"
        ><?php _e('Add Download', ZippyCourses::TEXTDOMAIN); ?></button>
    </div>

    <div v-show="entries.length > 0"> 
        <?php include(ZippyCourses::$path . 'assets/views/templates/admin/download.php'); ?>
    </div>

    <div v-show="entries.length == 0"> 
        <p class="description"><?php
            _e(
                "Click the <strong>Add Download</strong> button to add a download to this entry.",
                ZippyCourses::TEXTDOMAIN
            );
        ?></p>
    </div>
</div>

<input type="hidden" name="downloads" value="<?php echo htmlentities(json_encode($downloads)); ?>" />
