<?php
/*
Id: zippy-student-courses
Name: Courses
Post Types: zippy-student
Context: zippy_student_right
Priority: default
Order: 9
Version:     1.0.0
*/

$student_id          = filter_input(INPUT_GET, 'ID');

if ($student_id === false || $student_id === null) {
    return;
}

$zippy = Zippy::instance();

$query_id = "zippy_owner_{$student_id}_orders";
$query = $zippy->queries->get($query_id);

if ($query) {
    $query->flushResults();
}

$student = $zippy->make('student', array('ID' => $student_id));
$zippy->cache->set('student', $student);
$student->fetch();

$courses = $student->getCourses()->all();
$student_data = array(
    'courses' => $zippy->utilities->student->getCourseMetaboxData($student)
);
?>

<div class="zippy-student-courses">
    <course v-repeat="courses" class="zippy-student-course" inline-template>
        <div>
            <h3>
                <a href="{{ edit_url }}" v-html="title"></a>
            </h3>

            <div class="course-progress-bar">
                <span class="progress-bar" style="width: {{ progress }}%;"></span>
                <span class="progress-percentage">{{ progress }}%</span>
            </div>

            <p><strong>Tiers: </strong>
                <tier course="{{ id }}" v-repeat="tiers" inline-template>
                    <span
                        class="course-tier"
                    >
                        <span v-class="tier-revoked : !status">{{ title }}</span>
                        <span v-show="!status">
                            (<a
                                href="#"
                                v-on="click : restoreTierAccess"
                                class="restore-access"
                            ><?php _e('Restore Access', ZippyCourses::TEXTDOMAIN); ?></a>)
                        </span>
                        <span v-show="status">
                            (<a
                                href="#"
                                v-on="click : removeTierAccess"
                                class="remove-access"
                            ><?php _e('Remove Access', ZippyCourses::TEXTDOMAIN); ?></a>)
                        </span>
                    </span>
                    <span v-if="$index + 1 < $parent.tiers.length">, </span>
                </tier>

            </p>
            <p>
                <strong><?php _e('Start Date', ZippyCourses::TEXTDOMAIN); ?>:</strong>
                <span
                    v-show="!ux.edit_start_date"
                >
                     {{ start_date }}
                    (<a
                        href=""
                        v-on="click : toggleEditStartDate"
                    ><?php _e('Edit', ZippyCourses::TEXTDOMAIN); ?></a>)
                </span>
                <span
                    v-show="ux.edit_start_date"
                >
                    <input 
                        v-datepicker="start_date"
                        placeholder="MM/DD/YYYY"
                    />
                    <button
                        class="button buttom-primary"
                        v-on="click : saveStartDate"
                    ><?php _e('Save', ZippyCourses::TEXTDOMAIN); ?></button>
                    <button
                        class="button"
                        v-on="click : cancelEditStartDate"
                    ><?php _e('Cancel', ZippyCourses::TEXTDOMAIN); ?></button>
                </span>

            </p>
        </div>
    </course>
</div>

<?php
// We are using this instead of Data in the header because Data is not set up to handle non Post metaboxes
echo '<script type="text/json" class="zippy-mb-data">' . json_encode($student_data) . '</script>';
