<?php
/*
Id: zippy-transaction-details
Name: Details
Post Types: transaction
Context: normal
Priority: default
Version:     1.0.0
*/

$zippy = Zippy::instance();

// TODO: Add currency formatting.

$transaction          = $zippy->cache->get('transaction');
$view = new ZippyCourses_OrderTransaction_View($transaction);
$mb_data = $view->getMetaboxData();

?>
<table class="zippy-table zippy-order-transaction-table" id="zippy-order-transaction" >
    <thead>
        <tr>
            <th><?php  _e('Transaction ID', ZippyCourses::TEXTDOMAIN); ?></th>
            <th> <?php _e('Vendor ID', ZippyCourses::TEXTDOMAIN); ?></th>
            <th><?php _e('Type', ZippyCourses::TEXTDOMAIN); ?></th>
            <th> <?php _e('Total', ZippyCourses::TEXTDOMAIN); ?></th>
            <th><?php  _e('Status', ZippyCourses::TEXTDOMAIN);?></th>
        </tr>
    </thead>
    <tr>
        <td> <?php echo $transaction->getTitle(); ?></td>
        <td> <?php echo $transaction->getVendorid(); ?></td>

        <td> <?php echo $view->renderType();?></td>
        <td v-if="!ux.edit_transaction_amount">
            <?php  echo $zippy->currencies->getSymbol($transaction->getCurrency());?>{{ transaction.total }} <a v-on="click: toggleEditTransactionAmount" href="#">(Edit)</a></td>
        <td v-if="ux.edit_transaction_amount">
            <input v-model="transaction.total"> <br>
            <a href="#" class="button" v-on="click: saveTransactionAmount">Update</a>
            <a href="#" class="button" v-on="click: cancelEditTransactionAmount">Cancel</a>
        </td>
        <td> <?php echo ucfirst($transaction->getStatus()); ?></td>
    </tr>
</table>

<?php echo '<script type="text/json" id="zippy-transaction-data" class="zippy-mb-data">' . $mb_data . '</script>'; ?>