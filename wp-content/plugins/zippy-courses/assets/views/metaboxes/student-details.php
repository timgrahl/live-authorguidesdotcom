<?php
/*
Id: zippy-student-details
Name: Details
Post Types: zippy-student
Context: zippy_student_left
Priority: default
Order: 9
Version:     1.0.0
*/

$zippy = Zippy::instance();

$student_id = filter_input(INPUT_GET, 'ID');

if ($student_id === false || $student_id === null) {
    return;
}

$student = $zippy->make('student', array('ID' => $student_id));
$student->fetch();
?>

<h2><?php echo $student->getFullName(); ?></h2>

<p class="small">Username: <?php echo $student->getUsername(); ?></p>

<input type="hidden" name="studentID" id="studentID" value="<?php echo $student->getId(); ?>" />