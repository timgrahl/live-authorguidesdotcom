<?php
/*
Id: zippy-product-existing-access
Name: Edit Existing Course Access
Post Types: product
Data: product_existing_access
Fields: product_existing_access:json
Context: zippy_product_advanced_main
Priority: default
Version: 1.3.0
*/

$zippy = Zippy::instance();
$courses = $zippy->utilities->course->getAllCoursesAndTiers();

?>
<script type="text/template" id="zippy-product-existing-access-courses-data"><?php echo json_encode($courses); ?></script>

<input type="hidden" name="product_existing_access" value="
    <?php echo htmlentities(json_encode($product_existing_access)); ?>
    "/>


<p class="description">
    <?php _e('These settings let you use this product to check or modify a student\'s access to other courses or tiers in your site. You can set requirements for which students can join your course, and remove their access to other courses or tiers with this purchase.', ZippyCourses::TEXTDOMAIN); ?>
</p>
<p class="description">
    <em><?php _e('Note: These settings will only be used when a new student joins this Product. Students who have already purchased this product will be unaffected by these changes', ZippyCourses::TEXTDOMAIN); ?></em>
</p>

<div class="vue"></div>



<script type="text/template" id="zippy-product-existing-access-templ">

    <div class="zippy-mb-actions">
        <label>
            <input type="checkbox" name="enabled" v-model="enabled"/> 
            <?php _e('Allow this product to view and modify a student\'s existing course access', ZippyCourses::TEXTDOMAIN); ?>
        </label>
    </div>
    <div class="enabled-content" v-show="enabled">
        <h3><?php _e('Require Pre-Existing Access', ZippyCourses::TEXTDOMAIN); ?></h3>
        <div class="zippy-mb-actions">
            <label>
                <input v-model="requiredAccess" name="requiredAccess" type="checkbox"> 
                <?php _e('In order for a student to purchase this product, they must already have access to:', ZippyCourses::TEXTDOMAIN); ?>
            </label>
        </div>
        <require-course></require-course>
        <hr>
        <h3><?php _e('Revoke Access When Purchased', ZippyCourses::TEXTDOMAIN); ?></h3>
        <div class="zippy-mb-actions">
            <label>
                <input type="checkbox" name="revokeAccess" v-model="revokeAccess"/> 
                <?php _e('Remove the student from this course/tier when this product is purchased:', ZippyCourses::TEXTDOMAIN); ?>
            </label>
        </div>
        <remove-course></remove-course>
    </div>

    
</script>

<script type="text/template" id="zippy-courses-existing-access-courses-templ">
    <div class="zippy-row">
        <div class="zippy-col-sm-5 zippy-col-xs-12">
            <label><?php _e('Course:', ZippyCourses::TEXTDOMAIN); ?></label>
            <select
            v-model="course"
            v-on="change: updateParent"
            options="courses_list"></select>
        </div>
        <div class="zippy-col-sm-5 zippy-col-xs-12">
            <label><?php _e('Tier:', ZippyCourses::TEXTDOMAIN); ?></label> 
            <select
            v-model="tier"
            v-show="tiers_list.length > 0"
            v-on="change: updateParent"
            options="tiers_list"></select>
            <select
            v-show="tiers_list.length < 1"
            disabled="disabled"
            >
            <option><?php _e('Please select a course first.', ZippyCourses::TEXTDOMAIN); ?></option>
        </select>
    </div>
</div>
</script>

