<?php
/*
Id: zippy-course-public-enable
Name: Enable Public Settings
Fields: public
Post Types: course
Context: zippy_course_public_details_main
Priority: high
Order: 1
Version:     1.0.0
*/

?>

<h2 class="zippy-mb-tab-title"><?php _e(
    'Configure the Public Details of Your Course',
    ZippyCourses::TEXTDOMAIN
    ); ?></h2>

<p class="zippy-lead"><?php _e('Should a visitor be able to see information about your Course if they haven\'t purchased it? Should this Course be visible in the Course Directory?  Use this tab to set the Public Details for this Course.', ZippyCourses::TEXTDOMAIN); ?></p>

<p><label><input type="checkbox" id="zippy-enable-public-details" name="public" value="1" <?php checked('1', $public); ?>/>
<?php _e('Make this Course <strong>publicly visible</strong> and available in the ', ZippyCourses::TEXTDOMAIN); ?>
<strong><?php _e('Course Directory', ZippyCourses::TEXTDOMAIN); ?>?</strong></label></p>

