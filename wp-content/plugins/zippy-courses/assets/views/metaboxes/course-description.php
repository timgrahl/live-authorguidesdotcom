<?php
/*
Id: zippy-course-description
Name: Course Description
Data: public_featured_media
Post Types: course
Context: normal
Priority: high
Version: 1.0.0
*/

global $post;
?>

<textarea name="excerpt" id="excerpt"><?php echo $post->post_excerpt; ?></textarea>

<p class="description">
<?php _e('The Course Description will appear on the Dashboard as a brief synopsis of what this Course is about.', ZippyCourses::TEXTDOMAIN); ?>
<br><strong><?php _e('Recommended length', ZippyCourses::TEXTDOMAIN); ?>:</strong> <?php _e('160 characters', ZippyCourses::TEXTDOMAIN); ?>.</p>
