<?php
/*
Id: zippy-course-product-launch-windows-enable
Name: Launch Your Product Over and Over
Post Types: product
Context: zippy_product_launch_windows_main
Priority: high
Version: 1.0.0
*/

?>
<p class="description"><?php _e('If you will be launching this Product (or the Course and Tier assigned to it) multiple times, instead of allowing it to be for sale all the time, Launch Windows are the perfect tool for you.  When using Launch Windows, this product will only be available for purchase during the Open Date and Close Date of each Launch Window, and every purchase made during that Launch Window will begin the course on the Start Date.', ZippyCourses::TEXTDOMAIN); ?></p>
