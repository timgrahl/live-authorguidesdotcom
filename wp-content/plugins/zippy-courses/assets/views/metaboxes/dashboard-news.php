<?php
/*
Id: zippy-dashboard-news
Name: News & Updates
Post Types: zippy-courses
Context: zippy_admin_dashboard_left
Priority: default
Order: 11
Version:     1.0.0
*/
$zippy = Zippy::instance();

$rss = $zippy->rss->getNewsFeed();

$license = new ZippyCourses_License;
$version = $license->getLatestVersionNumber();

$html = '<div class="zippy-version-update-info">';
$html .= '<p style="margin-right: 120px;"><strong>Currently Installed Version:</strong> ' . ZippyCourses::VERSION . '<br/>';

if ($version) {
    $html .= '<strong>Latest Version Available:</strong> ' . $version;
}

$html .= '</p>';

if ($version && version_compare($version, ZippyCourses::VERSION) < 0) {
    $license_key = $license->getLicenseKey();
    if (!empty($license_key)) {
        $html .= '<a href="' . admin_url('plugins.php') . '"  class="zippy-update-btn button button-primary">Update Now</a>';
    } else {
        $html .= '<a href="' . admin_url('admin.php?page=zippy-settings') . '"  class="zippy-update-btn button button-primary">' .
            __('Install License Key', ZippyCourses::TEXTDOMAIN) .
        '</a>';
    }
}
$html .= '</div>';

if ($rss && !is_wp_error($rss)) {
    // Figure out how many total items there are, but limit it to 5.
    $maxitems = $rss->get_item_quantity(5);

    // Build an array of all the items, starting with element 0 (first element).
    $rss_items = $rss->get_items(0, $maxitems);

    $html .= '<ul class="zippy-courses-news-updates">';
    if ($maxitems == 0) {
        $html .= '<li>' . __('No updates recently!', ZippyCourses::TEXTDOMAIN) . '</li>';
    } else {
        foreach ($rss_items as $item) {
            $html .= '<li>';
                $html .= '<span>' . $item->get_date('j F Y')  . '</span>';
                $html .= '<a href="' . esc_url($item->get_permalink()) . '">';
                    $html .= esc_html($item->get_title());
                $html .= '</a>';
            $html .= '</li>';
        }
    }
    $html .= '</ul>';
}

echo $html;
