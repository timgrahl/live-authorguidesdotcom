<?php
/*
Id: zippy-product-access
Name: Access
Post Types: product
Data: access
Fields: access:json
Context: normal
Priority: default
Version: 1.0.0
*/

$zippy = Zippy::instance();

$courses = $zippy->utilities->course->getAllCoursesAndTiers();
?>
<div class="vue"></div>

<input type="hidden" name="access" value="<?php echo htmlentities(json_encode($access)); ?>" />

<script type="text/template" id="zippy-product-courses-data"><?php echo json_encode($courses); ?></script>
<script type="text/template" id="zippy-product-course-tmpl">
    
    <div class="zippy-mb-actions">
        <label><input type="checkbox" v-model="enable_bundle" /> <?php _e('Include multiple courses and tiers in this product.', ZippyCourses::TEXTDOMAIN); ?></label>
    </div>

    <div class="zippy-row" v-show="!enable_bundle">
        <div class="zippy-col-sm-5 zippy-col-xs-12">
            <label><?php _e('Course:', ZippyCourses::TEXTDOMAIN); ?></label>
            <select
                v-model="course"
                options="courses_list"></select>
        </div>

        <div class="zippy-col-sm-5 zippy-col-xs-12">
            <label><?php _e('Tier:', ZippyCourses::TEXTDOMAIN); ?></label> 

            <select
                v-model="tier"
                v-show="tiers_list.length > 0"
                options="tiers_list"></select>

            <select
                v-show="tiers_list.length < 1"
                disabled="disabled"
            >
                <option><?php _e('Please select a course first.', ZippyCourses::TEXTDOMAIN); ?></option>
            </select>
        </div>
    </div>

    <div class="zippy-row" v-show="enable_bundle">
        <p class="description zippy-col-xs-12"><?php _e('Please select the tiers of each course you would like to include in this product.', ZippyCourses::TEXTDOMAIN); ?></p>
        <div v-repeat="bundles" class="zippy-col-xs-12 bundle-course">
            <h4>{{ title }}</h4>
            <div v-repeat="tiers" class="bundle-tier">
                <label><input type="checkbox" v-model="checked" />  {{ title }}</label>
            </div>
        </div>
    </div>
</script>

