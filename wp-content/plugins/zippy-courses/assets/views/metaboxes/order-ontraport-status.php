<?php
/*
Id: zippy-order-ontraport-status
Name: Ontraport Status
Post Types: zippy_order
Context: side
Priority: default
Version:     1.0.0
*/

global $wpdb;
$zippy = Zippy::instance();
?>

<notice v-repeat="messages"></notice>
<p><button class="button" v-on="click : resend"><?php _e('Clear Ontraport Status', ZippyCourses::TEXTDOMAIN); ?></button></p>
