<?php
/*
Id: zippy-course-product-launch-windows
Name: Launch Windows
Help: Assign the pricing of for your product.
Data: launch_windows
Fields: launch_windows:json
Post Types: product
Context: zippy_product_launch_windows_main
Priority: default
Version: 1.0.0
*/

?>

<div class="vue"></div>

<input type="hidden" name="launch_windows" value="<?php echo htmlentities(json_encode($launch_windows)); ?>" />

<script type="text/template" id="mb-launch-windows-tmpl">
    <div class="zippy-mb-actions">
        <button
            class="button button-primary"
            v-class="disabled : !enabled"
            v-on="click : addLaunchWindow"
        ><?php _e('Add Launch Window', ZippyCourses::TEXTDOMAIN); ?></button>

        <label><input type="checkbox" v-model="enabled" /> <?php _e('Yes, use Launch Windows for this product!', ZippyCourses::TEXTDOMAIN); ?></label>
    </div>

    <p v-if="enabled && launch_windows.length === 0" class="zippy-get-started"><?php _e('Click the <strong>Add Launch Window</strong> button to get started!', ZippyCourses::TEXTDOMAIN); ?></p>

    <ul v-if="enabled && launch_windows.length > 0" class="zippy-launch-windows-list">
        <launch-window
            v-repeat="launch_windows"
            class="zippy-launch-window"
            inline-template
        >
            <div class="zippy-launch-window">
                <span class="zippy-entry-delete" v-on="click : destroy">&times;</span>

                <table class="zippy-table zippy-launch-window-table">
                    <tr>
                        <td><label><span><?php _e('Open Date', ZippyCourses::TEXTDOMAIN); ?>:</span> <input type="text" v-datepicker="open_date" class="zippy-input-md" /></label></td>
                        <td><label><span><?php _e('Close Date', ZippyCourses::TEXTDOMAIN); ?>:</span> <input type="text" v-datepicker="close_date" class="zippy-input-md" /></label></td>
                    </tr>
                    <tr>
                        <td><label><span><?php _e('Start Date', ZippyCourses::TEXTDOMAIN); ?>:</span> <input type="text" v-datepicker="start_date" class="zippy-input-md" /></label></td>
                        <td>
                            <label><span><?php _e('Expiration Date', ZippyCourses::TEXTDOMAIN); ?>:</span> <input type="text" v-datepicker="expiration_date" class="zippy-input-md" /></label>
                            <p class="description"><?php _e('Leave blank if access to this product should not expire when purchased during this launch window.', ZippyCourses::TEXTDOMAIN); ?></p>
                            </td>
                    </tr>
                </table>                
            </div>
        </launch-window>
    </ul>
</script>