<?php
/*
Id: zippy-course-entries
Name: Course Entries
Help: Set the tiers for your course?
Data: entries, config, quizzes, existing_content
Fields: entries:json, config:json
Post Types: course
Context: zippy_course_entries_main
Priority: default
Version:     1.0.0
*/

?>

<div class="vue"></div>

<input type="hidden" name="entries" value="<?php echo htmlentities(json_encode($entries)); ?>" />
<input type="hidden" name="config" value="<?php echo htmlentities(json_encode($config)); ?>" />

<script type="text/template" id="course-entries-tmpl">
<div class="zippy-course-entries">
    <div class="zippy-mb-actions">

        <div class="zippy-mb-action-details">
            <h4><?php _e('Scheduling', ZippyCourses::TEXTDOMAIN); ?></h4>
            <p><strong><?php _e('Method:', ZippyCourses::TEXTDOMAIN); ?></strong> 
                <span v-show="config.scheduling.type == 'drip'"><?php printf(__('Drip per %s', ZippyCourses::TEXTDOMAIN), '{{ config.scheduling.control.use | capitalize }}'); ?></span>
                <span v-show="config.scheduling.type != 'drip'"><?php _e('All Access', ZippyCourses::TEXTDOMAIN); ?></span>
            </p>
            <p><strong><?php _e('Actions:', ZippyCourses::TEXTDOMAIN); ?></strong> 
                <a 
                    href="" 
                    v-on="click : triggerModal" 
                    data-modal="#zippy-scheduling-config-modal"
                ><?php _e('Configure', ZippyCourses::TEXTDOMAIN); ?></a> | 
                <a 
                    href="" 
                    v-on="click : triggerModal" 
                    data-modal="#zippy-scheduling-bulk-edit-modal"
                ><?php _e('Bulk Edit', ZippyCourses::TEXTDOMAIN); ?></a>
            </p>
        </div>

        <div class="zippy-mb-action-details">
            <h4><?php _e('Access Tiers', ZippyCourses::TEXTDOMAIN); ?></h4>
            <p><strong><?php _e('Method:', ZippyCourses::TEXTDOMAIN); ?></strong> 
                <?php printf(__('Access per %s', ZippyCourses::TEXTDOMAIN), '{{ config.access.use | capitalize }}'); ?></span>
            </p>
            <p><strong><?php _e('Actions:', ZippyCourses::TEXTDOMAIN); ?></strong> 
                <a 
                    href="" 
                    v-on="click : triggerModal" 
                    data-modal="#zippy-access-config-modal"
                ><?php _e('Configure', ZippyCourses::TEXTDOMAIN); ?></a> | 
                <a 
                    href="" 
                    v-on="click : triggerModal" 
                    data-modal="#zippy-access-bulk-edit-modal"
                ><?php _e('Bulk Edit', ZippyCourses::TEXTDOMAIN); ?></a>
            </p>
        </div>

        <div class="zippy-spinner spinner"></div>
        <button class="button button-primary" v-on="click : fetchNewEntry"><?php _e('Add Lesson', ZippyCourses::TEXTDOMAIN); ?></button>
        
        <button
            class="button button-primary"
            v-on="click : triggerModal" 
            data-modal="#zippy-add-unit-modal"
        ><?php _e('Add Unit', ZippyCourses::TEXTDOMAIN); ?></button>
        
        <button 
            class="button" 
            v-on="click : triggerModal"
            data-modal="#zippy-add-existing-content-modal"
        ><?php _e('Add Existing Content', ZippyCourses::TEXTDOMAIN); ?></button>
    </div>

    <notice
        v-repeat="messages"
    ></notice>

    <ul class="course-entries zippy-sortable">
        <entry
            v-repeat="entries"
            config="{{ config }}"
            course-tiers="{{ tiers }}"
            quizzes="{{ quizzes }}"
        ></entry>
    </ul>
</div>
</script>

<script type="text/template" id="course-entry-tmpl">
    <div class="entry">
        <span class="dnd-handle"></span>
        <h5 class="entry-meta">
            <span
                class="entry-post-type"
            >{{ post_type | uppercase }}</span>
            
            <span
                class="entry-post-status"
                v-if="post.post_status == 'draft'"
            >({{ post.post_status | uppercase }})</span>
        </h5>

        <h4 class="entry-title">
            <input 
                type="text" 
                placeholder="{{ post_type | capitalize }} Title" 
                class="entry-title {{ post_type | lowercase }}-title"
                v-model="title" 
                lazy
            />
        </h4>

        <div class="entry-controls">
            <a 
                href="<?php echo home_url(); ?>?p={{ ID }}" 
                target="_blank" 
                class="entry-control entry-view"
                v-class="active : show_access"
            ><?php _e('View', ZippyCourses::TEXTDOMAIN); ?></a>

            <a
                href="/wp-admin/post.php?post={{ ID }}&action=edit"
                class="entry-control entry-edit"
                v-on="click : saveAndGoToEdit"
            ><?php _e('Edit', ZippyCourses::TEXTDOMAIN); ?></a>

            <a 
                href="" 
                class="entry-control entry-scheduling"
                v-show="controls.has_scheduling"
                v-on="click : toggleScheduling" 
                v-class="active : show_scheduling"
            ><?php _e('Scheduling', ZippyCourses::TEXTDOMAIN); ?></a>

            <a 
                href="" 
                class="entry-control entry-quiz"
                v-on="click : toggleQuiz" 
                v-class="active : ux.show_quiz"
            ><?php _e('Quiz', ZippyCourses::TEXTDOMAIN); ?></a>
           
            <a
                href=""
                v-on="click : toggleAccess"
                v-class="active : show_access"
                v-if="controls.has_access ||
                      (post_type !== 'unit' && (
                        config.access.use == 'item' ||
                        $parent.post_type !== 'unit')) ||
                      (post_type === 'unit' && (
                        config.access.use == 'unit' ||
                        entries.length < 1))"
                class="entry-control entry-access"
            ><?php _e('Access', ZippyCourses::TEXTDOMAIN); ?></a>
            <a
                v-on="click : toggleEntries"
                v-if="post_type == 'unit'"
                v-class="active : show_entries, open : ux.show_entries"
                href=""
                class="entry-control entry-entries"
            ><?php _e('Entries', ZippyCourses::TEXTDOMAIN); ?></a>

            <a 
                href="#"
                v-on="click : triggerModal" 
                v-if="controls.delete_mode == 'entry'"
                data-modal="#zippy-delete-entry-modal"
                class="entry-control entry-delete" 
            ><?php _e('Delete', ZippyCourses::TEXTDOMAIN); ?></a>

            <a 
                href="#"
                v-on="click : triggerModal" 
                v-if="controls.delete_mode == 'additional_content'"
                data-modal="#zippy-delete-additional-content-entry-modal"
                class="entry-control entry-delete"
            ><?php _e('Delete', ZippyCourses::TEXTDOMAIN); ?></a>
        </div>

        <scheduling
            num-days="{{@ scheduling.num_days }}"
            day-of-week="{{@ scheduling.day_of_week }}"
            completion-required="{{@ scheduling.required }}"
            exempt="{{@ scheduling.exempt }}"
            show-details="{{ showSchedulingDetails }}"
            config="{{ config.scheduling }}"
            v-if="ux.show_scheduling"
            v-transition="expand"
        ></scheduling>

        <quiz
            quizzes="{{ quizzes }}"
            quiz="{{@ quiz.ID }}"
            pass="{{@ quiz.pass }}"
            required="{{@ quiz.required }}"
            v-if="ux.show_quiz"
            v-transition="expand"
        ></quiz>

        <access
            tiers="{{@ access.tiers }}"
            v-if="ux.show_access"
            v-transition="expand"
        ></access>

        <div class="entry-section" v-if="post_type == 'unit' && ux.show_entries" v-style="entriesStyles">
            <ul class="zippy-sortable">
                <entry
                    v-repeat="entries"
                    config="{{ config }}"
                    course-tiers="{{ courseTiers }}"
                    quizzes="{{ quizzes }}"
                ></entry>
            </ul>    
        </div>
    </div>
</script>

<script type="text/template" id="entry-section-quiz">
<div class="entry-section quiz">
    <h4 class="entry-section-title"><?php _e('Quiz', ZippyCourses::TEXTDOMAIN); ?></h4>

    <select v-model="quiz" options="quizzes"></select>

    <p><label><input type="checkbox" v-model="required.completion" v-name="required.completion" value="1"/> <?php _e('Required', ZippyCourses::TEXTDOMAIN); ?></label></p>
    
    <p><label><input type="checkbox" v-model="required.pass" v-name="required.pass" value="1"/> <?php _e('Required Passing Grade', ZippyCourses::TEXTDOMAIN); ?></label></p>
    
    <p v-show="required.pass"><?php _e('Passing Grade:', ZippyCourses::TEXTDOMAIN); ?> <input v-model="pass" number/>%</p>
</div>
</script>

<script type="text/template" id="entry-section-access">
    <div class="entry-section access">
        <h4 class="entry-section-title"><?php _e('Access Tiers', ZippyCourses::TEXTDOMAIN); ?></h4>

        <ul>
            <li v-repeat="tier : tiers">
                <input type="checkbox" v-model="tier.selected"> {{tier.title}}
            </li>
        </ul>
    </div>
</script>

<script type="text/template" id="entry-section-scheduling">
    <div class="entry-section scheduling">
        <h4 class="entry-section-title"><?php _e('Scheduling', ZippyCourses::TEXTDOMAIN); ?></h4>

        <div>
            <div v-if="config.type == 'drip' && !exempt">
                <?php printf(__('This %s will be available', ZippyCourses::TEXTDOMAIN), '{{ post_type | capitalize }}'); ?>

                <div  v-if="config.control.type == 'previous'" style="display: inline-block;">
                <ul class="scheduling-days">
                    <li 
                        v-repeat="day : days">
                        <label 
                            v-class="active : dayOfWeek === day.key">
                            <input 
                                type="radio" 
                                value="{{ day.key }}" 
                                v-model="dayOfWeek" />
                            {{ day.val }}
                        </label>
                    </li>
                </ul>

                <?php _e('or', ZippyCourses::TEXTDOMAIN); ?>
                </div>

                <input type="number" class="zippy-input-sm scheduling-num-days" v-model="numDays" />

                <span
                    class="zippy-schedule-control-type"
                    v-if="config.control.type == 'previous'"
                ><?php _e('days after the previous entry.', ZippyCourses::TEXTDOMAIN); ?></span>
                <span 
                    class="zippy-schedule-control-type"
                    v-if="config.control.type == 'start'"
                ><?php _e('days after the course begins.', ZippyCourses::TEXTDOMAIN); ?></span>

            </div> 

            <div v-if="config.type == 'all'">
                <?php printf(__('Course Scheduling is set to <strong>All Access</strong>.  This %s will be available as soon as someone joins the Course.', ZippyCourses::TEXTDOMAIN), '{{ post_type | capitalize }}'); ?>
            </div>

        </div>
        <div class="entry-completion-required">
            <label><input type="checkbox" v-model="completionRequired" /> <?php printf(__('Students must complete this %s to continue to the next entry.', ZippyCourses::TEXTDOMAIN), '{{ post_type | capitalize }}'); ?></label>
        </div>
        <div class="entry-scheduling-exempt" v-show="config.type == 'drip'">
            <p><label><input type="checkbox" v-model="exempt" /> <?php _e('Do not apply scheduling to this entry, and make it available immediately upon joining the course.', ZippyCourses::TEXTDOMAIN); ?></label></p>
        </div>
    </div>
</script>
