<?php
/*
Id: zippy-order-status
Name: Status
Post Types: zippy_order
Fields: status
Context: side
Priority: default
Order: 9
Version:     1.0.0
*/

global $wpdb;

$zippy = Zippy::instance();

?>
<select name="status">
<?php foreach ($zippy->utilities->cart->getOrderStatusList() as $key => $text) : ?>
    <option value="<?php echo $key; ?>" <?php selected($key, $status); ?>><?php echo $text; ?></option>';
<?php endforeach; ?>
</select>
