<?php
/*
Id: zippy-student-activity
Name: Activity
Post Types: zippy-student
Context: zippy_student_right
Priority: default
Version:     1.0.0
*/

$student_id = filter_input(INPUT_GET, 'ID');

if ($student_id === false || $student_id === null) {
    return;
}

$chart = new ZippyCourses_StudentActivity_Chart($student_id);
$chart->fetchData();

$all_views_dataset = new ZippyCourses_StudentAllViews_Dataset($student_id);
$all_views_dataset->fetchData();

$recent_views_dataset = new ZippyCourses_StudentRecentViews_Dataset($student_id);
$recent_views_dataset->fetchData();

$downloads_dataset = new ZippyCourses_StudentDownloads_Dataset($student_id);
$downloads_dataset->fetchData();

$lessons_completed_dataset = new ZippyCourses_StudentLessonsCompleted_Dataset($student_id);
$lessons_completed_dataset->fetchData();

$completion_dataset = new ZippyCourses_StudentCompletion_Dataset($student_id);
$completion_dataset->fetchData();

$mb_data = new stdClass;
$mb_data->recent_views = array_values($recent_views_dataset->getData());
$mb_data->all_views = array_values($all_views_dataset->getData());
$mb_data->downloads = array_values($downloads_dataset->getData());
$mb_data->lessons_completed = array_values($lessons_completed_dataset->getData());
$mb_data->completion_data = array_values($completion_dataset->getData());

?>

<div class="zippy-student-activity">

    <?php echo $chart->render(); ?>

    <div>
    <student-activity-table inline-template>
        <h3><?php _e('Student Activity', ZippyCourses::TEXTDOMAIN); ?></h3>
        <a
            href=""
            class="zippy-tab"
            v-on="click : toggleTimespan"
            v-class="active : mode == 'recent'"
        ><?php _e('Recent', ZippyCourses::TEXTDOMAIN); ?></a>
        <a
            href=""
            class="zippy-tab"
            v-on="click : toggleTimespan"
            v-class="active : mode == 'all'"
        ><?php _e('All Time', ZippyCourses::TEXTDOMAIN); ?></a>
        <table class="zippy-data-table">
            <thead>
                <tr>
                    <th v-on="click : sortBy('title')">Title</th>
                    <th v-on="click : sortBy('views')">Views</th>
                    <th v-on="click : sortBy('duration')">Duration</th>
                    <th v-on="click : sortBy('avg_duration')">Average Duration</th>
                </tr>
            </thead>

            <tbody>
                <tr v-repeat="views | orderBy sortKey reverse">
                    <td v-html="title"></td>
                    <td>{{ views }}</td>
                    <td>{{ getHumanReadableTime(duration) }}</td>
                    <td>{{ getHumanReadableTime(avg_duration) }}</td>
                </tr>
            </tbody>
        </table>
    </div>

    </student-activity-table>
    <student-lesson-completion-table inline-template>
        <h3><?php _e('Units and Lessons Completed', ZippyCourses::TEXTDOMAIN); ?></h3>
        <span class="lesson-buttons" v-repeat="lessons_completed">

            <a
            href=""
            class="zippy-tab"
            v-on="click : toggleCourse(ID, $event)"
            v-class = "active : ID === active_course.ID"
            v-html="title"
            ></a>           
        </span>
        <table class="zippy-data-table" v-show="studentHasCompletedLessons">
            <thead>
                <tr>
                    <th>
                         {{ active_course.title }}
                     </th>                            
                 </tr>
             </thead>
            <tr v-show="activeCourseHasLessons" v-repeat="active_course.lessons">
                <td v-html="title"></td>
            </tr>
        </table>     
        <table class="zippy-data-table" v-show="!studentHasCompletedLessons">
            <tr>
                <td colspan="2"><em><?php _e('This student has not marked any lessons as complete.', ZippyCourses::TEXTDOMAIN); ?></em></td>
            </tr>
        </table>
    </student-lesson-completion-table>
    <div>
    <student-downloads-table inline-template>
        <h3><?php _e('Student Downloads', ZippyCourses::TEXTDOMAIN); ?></h3>
        <table class="zippy-data-table">
            <thead>
                <tr>
                    <th v-on="click : sortBy('title')">Title</th>
                    <th v-on="click : sortBy('views')">Views</th>
                    <th v-on="click : sortby('last_viewed')">Last Accessed</th>
                </tr>
            </thead>

            <tbody>
                <tr v-show="downloads.length > 0" v-repeat="downloads | orderBy sortKey reverse">
                    <td v-html="title"></td>
                    <td>{{ views }}</td>
                    <td>{{ last_viewed }}</td>
                </tr>

                <tr v-show="downloads.length == 0">
                    <td colspan="2"><em><?php _e('This student has not downloaded any files.', ZippyCourses::TEXTDOMAIN); ?></em></td>
                </tr>            
            </tbody>
        </table>
    </div>
    </student-downloads-table>
    <student-completion-certificate-table inline-template>
        <h3><?php _e('Courses Completed', ZippyCourses::TEXTDOMAIN); ?></h3>
        <table class="zippy-data-table">
            <thead>
                <tr>
                    <th v-on="click : sortBy('title')">Title</th>
                    <th v-on="click : sortBy('views')">Completion Date</th>
                </tr>
            </thead>

            <tbody>
                <tr v-show="completion_data.length > 0" v-repeat="completion_data | orderBy sortKey reverse">
                    <td v-html="title"></td>
                    <td>{{ completion_date }}</td>
                </tr>

                <tr v-show="completion_data.length == 0">
                    <td colspan="2">
                        <em><?php _e('This student has not completed any courses.', ZippyCourses::TEXTDOMAIN); ?></em>
                    </td>
                </tr>            
            </tbody>
        </table>        
    </student-completion-certificate-table>
    </div>


<!-- <div id="student-downloads-table"></div> -->

<script type="text/json" class="zippy-mb-data"><?php echo json_encode($mb_data); ?></script>

