<?php
/*
Id: zippy-course-public-description
Name: Course Description
Fields: public_excerpt
Data: public_excerpt
Post Types: course
Context: zippy_course_public_details_main
Priority: default
Order: 9
Version: 1.0.0
*/

?>

<textarea name="public_excerpt" id="public_excerpt"><?php echo $public_excerpt; ?></textarea>

<p class="description">
<?php _e('The Public Course Description will appear in the Course Directory as a brief synopsis of what this Course is about.', ZippyCourses::TEXTDOMAIN); ?>  
<strong><?php _e('Recommended length', ZippyCourses::TEXTDOMAIN); ?>:</strong> <?php _e('160 characters', ZippyCourses::TEXTDOMAIN); ?>.</p>
