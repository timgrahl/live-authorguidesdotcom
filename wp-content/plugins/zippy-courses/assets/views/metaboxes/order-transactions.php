<?php
/*
Id: zippy-order-transactions
Name: Transactions
Children: transactions:order_id
Post Types: zippy_order
Context: normal
Priority: default
Version:     1.0.0
*/

$zippy = Zippy::instance();

$order          = $zippy->cache->get('order');
$view           = new ZippyCourses_OrderTransactions_View($order);

$view->show();
