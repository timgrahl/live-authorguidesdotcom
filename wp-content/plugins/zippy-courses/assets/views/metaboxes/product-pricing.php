<?php
/*
Id: zippy-product-pricing
Name: Pricing
Help: Assign the pricing of for your product.
Data: pricing
Fields: pricing:json
Post Types: product
Context: side
Priority: default
Order: 8
Version: 1.0.0
*/
?>

<div class="vue"></div>

<input type="hidden" name="pricing" value="<?php echo htmlentities(json_encode($pricing)); ?>" />