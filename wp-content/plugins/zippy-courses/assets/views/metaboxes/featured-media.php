<?php
/*
Id: zippy-featured-media
Name: Featured Media
Data: featured_media
Fields: featured_media:json,_thumbnail_id
Post Types: all
Context: side
Priority: default
Version: 1.0.0
*/
?>

<?php $output = '<div class="vue"></div>'; ?>
<?php echo apply_filters('admin_post_thumbnail_html', $output); ?>
<input type="hidden" name="featured_media" value="<?php echo htmlentities(json_encode($featured_media)); ?>" />

<script type="text/template" id="featured-media-tmpl">
    <div class="zippy-featured-media">
        <ul class="zippy-featured-media-options">
            <li>
                <label v-class="active : featured_media.type == 'image'">
                    <input type="radio" value="image" v-model="featured_media.type" /> 
                    <?php _e('Image', ZippyCourses::TEXTDOMAIN); ?>
                </label>
            </li>

            <li>
                <label v-class="active : featured_media.type == 'video'">
                    <input type="radio" value="video" v-model="featured_media.type" /> 
                    <?php _e('Video', ZippyCourses::TEXTDOMAIN); ?>
                </label>
            </li>
        </ul>

        <featured-image
            image="{{@ featured_media.image }}"
            v-if="featured_media.type == 'image'"
            class="zippy-featured-media-container"
            inline-template
        >
            <div>
                <p>
                    <a
                        v-on="click : openFileFrame"
                        v-if="!image.url"
                        href=""><?php _e('Set featured image', ZippyCourses::TEXTDOMAIN); ?></a>
                </p>

                <div class="zippy-preview" v-if="image.url">
                    <img v-attr="src: image.url" />
                    <p><a
                    v-on="click : removeFeaturedImage"
                    v-if="image.url"
                    href=""><?php _e('Remove featured image', ZippyCourses::TEXTDOMAIN); ?></a></p>
                </div>
            </div>
        </featured-image>

        <featured-video
            video="{{@ featured_media.video }}"
            v-if="featured_media.type == 'video'"
            class="zippy-featured-media-container"
            inline-template
        >
            <div>
                <p style="margin-top: 0;"><label><?php _e('Embed Code', ZippyCourses::TEXTDOMAIN); ?>:</label></p>
                <textarea
                    class="zippy-embed-code code"
                    v-model="video"
                    v-validate="embeddable"
                    lazy></textarea>    

                <div
                    class="zippy-preview"
                    v-if="valid"
                    v-html="video | stripslashes">
                </div>
            </div>

        </featured-video>
        <input type="hidden" name="_thumbnail_id" v-model="featured_media.image.ID" />
        
    </div>


</script>