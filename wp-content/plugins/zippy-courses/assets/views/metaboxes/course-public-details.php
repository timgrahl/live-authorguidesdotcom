<?php
/*
Id: zippy-course-public-content
Name: Content
Fields: public_content
Data: public_content
Post Types: course
Context: zippy_course_public_details_main
Priority: default
Order: 8
Version:     1.0.0
*/

wp_editor($public_content, 'zippy-course-public-content-editor', array(
    'textarea_name' => 'public_content'
));
