<?php
/*
Id: zippy-product-confirmation-messages
Post Types: product
Name: Custom Confirmation Messages
Data: confirmation_messages
Fields: confirmation_messages:json
Post Types: product
Context: zippy_product_confirmation_messages_main
Priority: default
*/

$zippy = Zippy::instance();
$thank_you_page_default = $zippy->utilities->post->getDefaultThankYouText();
$registration_reminder_default = $zippy->utilities->email->getSystemEmailText('registration_reminder');
$registration_reminder_default_subject = $zippy->utilities->email->getSystemEmailSubject('registration_reminder');

$purchase_receipt_default = $zippy->utilities->email->getSystemEmailText('purchase_receipt');
$purchase_receipt_default_subject = $zippy->utilities->email->getSystemEmailSubject('purchase_receipt');

?>
<p><em><?php _e('Set a custom confirmation or email message for this product. If no option is selected, your default settings will be applied.', ZippyCourses::TEXTDOMAIN); ?></em></p>

<hr>

<div class="vue"></div>

<input type="hidden" name="confirmation_messages" value="<?php echo htmlentities(json_encode($confirmation_messages)); ?>" />
<input type="hidden" id="zippy_default_thank_you_message" value="<?php echo htmlentities($thank_you_page_default); ?>">
<input type="hidden" id="zippy_default_purchase_receipt_message" value="<?php echo htmlentities($purchase_receipt_default); ?>">
<input type="hidden" id="zippy_default_purchase_receipt_subject" value="<?php echo htmlentities($purchase_receipt_default_subject); ?>">

<input type="hidden" id="zippy_default_registration_reminder_message" value="<?php echo htmlentities($registration_reminder_default); ?>">
<input type="hidden" id="zippy_default_registration_reminder_subject" value="<?php echo htmlentities($registration_reminder_default_subject); ?>">


<script type="text/template" id="zippy-product-confirmation-message-templ">



<div class="zippy-thank-you-text">
    <h2 class="zippy-confirmation-message-heading"><?php _e('Thank You Page', ZippyCourses::TEXTDOMAIN); ?></h2>
    <div class="zippy-mb-actions">
        <label>
            <input type="checkbox" v-model="custom_thank_you_enable"> <?php _e('Set a Custom Thank-You Page Message', ZippyCourses::TEXTDOMAIN); ?> 
        </label>
    </div>

    <div v-show="custom_thank_you_enable">
        <textarea v-model="custom_thank_you_text" id="custom_thank_you_text" cols="30" rows="10"></textarea>
         <p><em><strong><?php _e('Important Shortcodes', ZippyCourses::TEXTDOMAIN); ?>:</strong>
        <br>
        <?php _e('Registration:', ZippyCourses::TEXTDOMAIN); ?> 
            [zippy_button type="register" style="primary"]
        <br>
        <?php _e('Login:', ZippyCourses::TEXTDOMAIN); ?> 
            [zippy_button type="register-login"]
        </em></p>
    </div>
</div>

<hr>

<div class="zippy-purchase-receipt-email">
    <h2 class="zippy-confirmation-message-heading"><?php _e('Purchase Receipt Email', ZippyCourses::TEXTDOMAIN); ?></h2>
    <div class="zippy-mb-actions">
        <label>
            <input type="checkbox" v-model="custom_purchase_receipt_enable">
            <?php _e('Set a Custom Purchase Receipt Email', ZippyCourses::TEXTDOMAIN); ?>
        </label>
    </div>
    <div v-show="custom_purchase_receipt_enable">
        <h5><?php _e('Subject', ZippyCourses::TEXTDOMAIN); ?></h5>
        <input type="text" class="zippy-custom-email-subject" v-model="custom_purchase_receipt_subject">
        <h5><?php _e('Body', ZippyCourses::TEXTDOMAIN); ?></h5>
        <textarea v-model="custom_purchase_receipt_text" id="custom_purchase_receipt_text" cols="30" rows="10"></textarea>
        <p><em><strong><?php _e('Important Shortcodes', ZippyCourses::TEXTDOMAIN); ?>:</strong>
        <br>
        <?php _e('Order Receipt:', ZippyCourses::TEXTDOMAIN); ?> 
            [order_receipt]
        <br>
        <?php _e('Order History URL:', ZippyCourses::TEXTDOMAIN); ?> 
            [order_history_url]
        </em></p>
    </div>
</div>

<hr>

<div class="zippy-registration-reminder-email">
    <h2 class="zippy-confirmation-message-heading"><?php _e('Registration Reminder Email', ZippyCourses::TEXTDOMAIN); ?></h2>
    <div class="zippy-mb-actions">
        <label>
            <input type="checkbox" v-model="custom_registration_reminder_enable"> <?php _e('Set a Custom Registration Reminder Email', ZippyCourses::TEXTDOMAIN); ?>
        </label>
    </div>
    <div v-show="custom_registration_reminder_enable">
        <h5><?php _e('Subject', ZippyCourses::TEXTDOMAIN); ?></h5>
        <input type="text" class="zippy-custom-email-subject" v-model="custom_registration_reminder_subject">
        <h5><?php _e('Body', ZippyCourses::TEXTDOMAIN); ?></h5>
        <textarea v-model="custom_registration_reminder_text" id="custom_registration_reminder_text" cols="30" rows="10"></textarea>
        <p><em><strong><?php _e('Important Shortcodes', ZippyCourses::TEXTDOMAIN); ?>:</strong>
        <br>
        <?php _e('Registration URL:', ZippyCourses::TEXTDOMAIN); ?> 
            [registration_claim_url]
        <br>
        <?php _e('Login URL:', ZippyCourses::TEXTDOMAIN); ?> 
            [login_claim_url]
        </em></p>
    </div>
</div>
</script>