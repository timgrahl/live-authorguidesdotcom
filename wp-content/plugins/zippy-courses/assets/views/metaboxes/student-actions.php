<?php
/*
Id: zippy-student-actions
Name: Actions
Post Types: zippy-student
Context: zippy_student_left
Priority: default
Version:     1.0.0
*/

$user_id = (int) filter_input(INPUT_GET, 'ID');
?>

<h3>Actions</h3>
<p><a href="<?php echo get_edit_user_link($user_id); ?>" class="button"><?php _e('Edit Profile', ZippyCourses::TEXTDOMAIN); ?></a></p>
<p><a href=""  data-toggle="modal" data-target="#zippy-student-grant-access-modal" class="button"><?php _e('Grant Access', ZippyCourses::TEXTDOMAIN); ?></a></p>
