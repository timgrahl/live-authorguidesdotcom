<?php
/*
Id: zippy-course-directory-order
Name: Directory Order
Fields: directory_order
Post Types: course
Context: zippy_course_public_details_side
Priority: default
Order: 9
Version: 1.0.0
*/

global $post;

$directory_order = !empty($directory_order) ? $directory_order : 0;
?>

<p>
    <label>
        <?php _e('Order:', ZippyCourses::TEXTDOMAIN); ?> 
        <input type="number" name="directory_order" value="<?php echo $directory_order; ?>" />
    </label>
</p>

<p>
    <em>
        <small>
        <?php
        _e('Use this field to control the order it displays within the Course Directory. Lower numbers will appear higher on the directory.', ZippyCourses::TEXTDOMAIN);
        ?>
        </small>
    </em>
</p>
