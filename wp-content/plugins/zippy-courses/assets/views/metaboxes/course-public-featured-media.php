<?php
/*
Id: zippy-course-public-featured-media
Name: Featured Media
Fields: public_featured_media:json
Data: public_featured_media
Post Types: course
Context: zippy_course_public_details_side
Priority: default
Version: 1.0.0
*/
?>

<div class="vue"></div>

<input type="hidden" name="public_featured_media" value="<?php echo htmlentities(json_encode($public_featured_media)); ?>" />