<?php
/*
Id: zippy-order-details
Name: Order Details
Post Types: zippy_order
Context: normal
Priority: default
Order: 9
Version:     1.0.0
*/

global $wpdb;

$zippy = Zippy::instance();
$order = $zippy->cache->get('order');
$registration_claim_url = $zippy->utilities->orders->getRegistrationUrl($order);
$login_claim_url = $zippy->utilities->orders->getLoginUrl($order);


$gateway      = '';
$gateways     = $zippy->make('payment_gateway_integrations');
$gateway_list = $gateways->toArray('name');

if (!$order->transactions->count()) {
    $order->fetchTransactions();
}

$transaction = $order->transactions->count() ? $order->transactions->first() : null;

if ($transaction !== null) {
    $gateway = $transaction->getGateway();

    if (isset($gateway_list[$gateway])) {
        $gateway = $gateway_list[$gateway];
    }
}



$users = $zippy->utilities->user->getOwnerList();
?>

<table class="zippy-table zippy-order-details-table">
    <thead>
        <tr>
            <th><?php _e('Customer', ZippyCourses::TEXTDOMAIN); ?></th>
            <th><?php _e('Product', ZippyCourses::TEXTDOMAIN); ?></th>
            <th><?php _e('Gateway', ZippyCourses::TEXTDOMAIN); ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <?php if ($order->getOwner() < 1): ?>
                    
                    <?php if ($order->getCustomer() !== null): ?>
                    <p><strong><?php _e('Customer Details:', ZippyCourses::TEXTDOMAIN); ?></strong> <?php echo $order->getCustomer()->getFullName(); ?> (<?php echo $order->getCustomer()->getEmail(); ?>)</p>
                    <?php endif; ?>

                    <p><strong><?php _e('Assign to Owner:', ZippyCourses::TEXTDOMAIN); ?></strong> <select name="post_author_override">
                    <?php foreach ($users as $user_id => $name): ?>
                        <option value="<?php echo $user_id; ?>" <?php selected($user_id, $order->getOwner()); ?>><?php echo $name; ?></option>
                    <?php endforeach; ?>
                    </select> 
                <?php else: ?>
                <?php 
                    $student = $zippy->make('student', array((int) $order->getOwner()));
                    $student->fetch();
                ?>
                <a href="<?php echo get_edit_user_link($student->getId()); ?>"><?php echo $student->getFullName(); ?></a>
                <?php endif; ?>
            </td>
            <td>
                <?php if ($order->getProduct() !== null): ?>
                <a href="<?php echo get_edit_post_link($order->getProduct()->getId()); ?>">
                    <?php echo $order->getProduct()->getTitle(); ?>
                </a>
                <?php endif; ?>
            </td>
            <td>
                <?php echo $gateway; ?>
            </td>
        </tr>
        <?php if ($order->getOwner() < 1): ?>
        <tr>
            <td colspan=3>
                    <p><strong><?php _e('Registration URL', ZippyCourses::TEXTDOMAIN); ?>:</strong> <?php echo $registration_claim_url; ?>
                    <p><strong><?php _e('Login URL', ZippyCourses::TEXTDOMAIN); ?>:</strong> <?php echo $login_claim_url ?>
                    <p><strong></strong></p>
            </td>
        </tr>
        <?php endif; ?>
    </tbody>
</table>
