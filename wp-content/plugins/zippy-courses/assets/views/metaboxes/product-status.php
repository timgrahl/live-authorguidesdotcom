<?php
/*
Id: zippy-product-status
Name: Status
Help: Assign whether this product is active.
Fields: status
Post Types: product
Context: side
Priority: default
Version: 1.0.0
*/
?>

<select name="status">
    <option value="active" <?php selected('active', $status); ?>><?php _e('Active', ZippyCourses::TEXTDOMAIN); ?></option>
    <option value="inactive" <?php selected('inactive', $status); ?>><?php _e('Inactive', ZippyCourses::TEXTDOMAIN); ?></option>
</select>