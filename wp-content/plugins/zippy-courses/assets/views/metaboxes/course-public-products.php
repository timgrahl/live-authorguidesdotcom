<?php
/*
Id: zippy-course-public-products
Name: Products
Data: products
Fields: products:json
Post Types: course
Context: zippy_course_public_details_main
Priority: default
Version:     1.0.0
*/
?>

<div class="vue"></div>

<input type="hidden" name="products" value="<?php echo htmlentities(json_encode($products)); ?>" />

<script type="text/template" id="course-products-tmpl">
    <div class="zippy-course-products">
        <p class="zippy-lead"><?php _e('The Products below are being used to sell this Course. Please use the dropdown to the right of each Product to indicate whether they should be included in the Course Directory. You can drag and drop the products to set the product order you\'d like to display on your Course Directory', ZippyCourses::TEXTDOMAIN); ?></p>

        <table class="zippy-course-product-table">
            <thead>
                <tr>
                    <th><?php _e('Title', ZippyCourses::TEXTDOMAIN); ?></th>
                    <th><?php _e('Include in Directory', ZippyCourses::TEXTDOMAIN); ?></th>
                </tr>
            </thead>
            <tbody class="zippy-sortable-products">
                <tr v-show="courseProducts.length > 0" class="sortable-product" v-repeat="courseProducts">
                    <td class="zippy-course-products-title" data-ID="{{ ID }}" data-index="{{ list_order }}">
                        <span class="dnd-handle"></span>
                        <span class="zippy-course-products-title-text">{{ title }}
                    </td>

                    <td>
                        <select v-model="selected" options="activeOptions"></select>
                    </td>
                </tr>
                <tr v-show="courseProducts.length == 0">
                    <td colspan="2"><em><?php _e('There are no products for this course yet.', ZippyCourses::TEXTDOMAIN); ?> <a v-on="click : saveAndGo" href="<?php echo admin_url('post-new.php?post_type=product'); ?>"><?php _e('Click here', ZippyCourses::TEXTDOMAIN); ?></a> <?php _e('to add a new product', ZippyCourses::TEXTDOMAIN); ?></em>.</td>
                </tr>
            </tbody>
        </table>
    </div>
</script>
