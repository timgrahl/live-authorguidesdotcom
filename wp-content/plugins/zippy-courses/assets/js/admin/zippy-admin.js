(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var sortable = require('sortable_mixin');
var savePostMeta = require('save_post_meta_mixin');

module.exports = {
    name: 'additional_content',
    el: function() { return '#zippy-course-additional-content .inside .vue'; },
    mixins: [sortable],
    template: $('#course-additional-content-tmpl').text(),
    props: ['entries', 'additional_content', 'tiers'],
    data: function() {
        return {
            simplified: [],
            messages: []
        }
    },
    components: {
        entry: require('./../entry'),
        notice: require('notice')
    },
    watch: {
        entries: {
            handler: 'removeEntriesFromAdditionalContent',
            deep: true
        }
    },
    events: {
        show_message: 'showNotice',
        add_additional_content: 'addItems',
        bulk_assign_tiers_to_additional_content: 'bulkAssignTiers',
        delete_additional_content_entry: 'deleteEntry'
    },
    created: function() {
    },
    compiled: function() {
        this.initSortable();
    },
    attached: function() {
        this.$dispatch('child_loaded', 'additional_content');
    },
    methods: {
        removeEntriesFromAdditionalContent: function(val, oldVal) {
            var that = this;
            val.forEach(function(entry) {
                var record = _.findWhere(that.additional_content, {ID: entry.ID});

                if (record !== undefined) {
                    that.additional_content.$remove(record);
                }
            });
        },
        showNotice: function(receiver, notice) {
            if (receiver != 'course_entries') {
                return;
            }

            this.messages.push(notice);
        },
        triggerModal: function(e) {
            e.preventDefault();
            this.$dispatch('trigger_modal', $(e.currentTarget).data('modal'));
        },
        addItems: function(items) {
            var that = this;

            this.$resource(ajaxurl).get(
                {
                    action: 'get_items',
                    items: items
                }, 
                function (items, status, request) {
                    items.forEach(function(item) {
                        item.controls = {};
                        item.controls.has_access = true;
                        item.controls.has_scheduling = false;

                        that.additional_content.push(item);
                    });
                }
            );
        },
        bulkAssignTiers: function(tiers) {
            this.additional_content.forEach(function(entry) {
                entry.access.tiers.forEach(function(tier) {
                    if (tiers.indexOf(parseInt(tier.ID)) !== -1) {
                        tier.selected = true;
                    }
                });
            });
        },
        newEntry: function(type) {
            this.additional_content.push({ post_type : type });
        },
        deleteEntry: function(entry) {
            var that = this;

            var record = _.findWhere(this.additional_content, {ID: entry.ID});

            if (record !== undefined) {
                this.additional_content.$remove(entry);
            }

            this.$dispatch('entry_updated', this);
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./../entry":5,"notice":79,"save_post_meta_mixin":93,"sortable_mixin":96}],2:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');

module.exports = {
    name: 'course-email-lists',
    components: {
        service: require('./service')
    },
    data: function() {
        return {
            services : [],
        }
    },
    created: function() {
        this.buildServices();
    },
    methods: {
        /**
         * Fetch the metadata from $wpdb->postmeta for this course
         * @return void
         */
        buildServices: function() {
            var that = this;

            this.services.forEach(function(service) {
                var el = $('.vue[email-integration="' + service.id + '"]').first();

                if (el.length > 0) {
                    that.$addChild(
                        { 
                            el: el[0],
                            data: service
                        },
                        that.$options.components.service
                    ).$mount();
                }
            });
            
            this.$dispatch('child_loaded', 'email_lists');
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./service":3,"save_post_meta_mixin":93}],3:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'email-list-service',
    template: $('#email-list-integration-tmpl').text(),
    data: function() {
        return {
            fetched: false,
            service: '',
            per_tier: false,
            list: '0',
            lists: [],
            tiers: [],
            course_tiers: []
        }
    },
    watch: {
        course_tiers: {
            handler: 'syncTiers',
            deep : true
        }
    },
    compiled: function() {
        var data = $(this.$el).siblings('.zippy-mb-data').text();

        if (data !== undefined && data !== '') {
            var key = this.id + '_lists';

            data = JSON.parse(data);

            if (data[key] !== undefined) {
                this.lists = data[key];
            }

            if (data.email_lists !== undefined) {
                var record = _.findWhere(data.email_lists, {service: this.id});

                if (record !== undefined) {
                    this.per_tier   = record.per_tier;
                    this.list       = record.list;
                    this.tiers      = record.tiers;
                }
            }

            this.syncTiers(this.course_tiers);
            this.fetched = true;
        }
    },
    methods: {
        syncTiers: function(tiers) {
            var that = this;

            var existing = this.tiers.filter(function(tier) {
                return _.findWhere(tiers, {ID: tier.ID});
            });

            tiers = tiers.map(function(tier) {
                return {
                    ID: tier.ID, 
                    title: tier.title,
                    list: '0'
                };
            });

            // Resync the selected
            tiers.forEach(function(tier) {
                var record = _.findWhere(existing, {ID: tier.ID});
                if (record !== undefined) {
                    var index = _.indexOf(existing, record);
                    tier.list = existing[index].list;
                }
            });

            this.tiers = tiers;
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],4:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var sortable = require('sortable_mixin');
var savePostMeta = require('save_post_meta_mixin');

module.exports = {
    name: 'entries',
    el: function() { return '#zippy-course-entries .inside .vue'; },
    mixins: [sortable],
    template: $('#course-entries-tmpl').text(),
    props:['quizzes', 'tiers'],
    data: function() {
        return {
            simplified: [],
            messages: []
        }
    },
    components: {
        entry: require('./../entry'),
        notice: require('notice')
    },
    events: {
        show_message: 'showNotice',
        import_entries: 'importItems'
    },
    created: function() {
        this.$on('add_entries', this.addItems);
        this.$on('set_scheduling', this.bulkSetScheduling);
        this.$on('add_tiers', this.bulkAddTiers);
        this.$on('add_unit', this.addUnit);
        this.$on('delete_entry', this.deleteEntry);
        $('#zippy-course-entries').on('keydown', this.tabToNextEntry);
    },
    compiled: function() {
        this.initSortable();

        this.$watch('config', function(newVal, oldVal) {
            this.$broadcast('config-update', newVal);
        }, { deep : true });
    },
    attached: function() {
        this.$dispatch('child_loaded', 'entries');
    },
    methods: {
        showNotice: function(receiver, notice) {
            if (receiver != 'course_entries') {
                return;
            }

            this.messages.push(notice);
        },
        triggerModal: function(e) {
            e.preventDefault();
            this.$dispatch('trigger_modal', $(e.currentTarget).data('modal'));
        },
        preselectFirstTier: function() {
            return tier = {
                ID: this.tiers[0].ID,
                title: this.tiers[0].title,
                selected: true
            };
        },
        addItems: function(items) {
            var that = this;
            var selected_tier = this.preselectFirstTier();

            this.$resource(ajaxurl).get(
                {
                    action: 'get_items',
                    items: items
                }, 
                function (items, status, request) {
                    items.forEach(function(item) {
                        if (item.access == undefined) {
                            item.access = {};
                        }

                        if (item.access.tiers == undefined) {
                            item.access.tiers = [selected_tier];
                        }
                        
                        that.entries.push(item);
                    });
                }
            );
        },
        importItems: function(items) {
            var that = this;
            var selected_tier = this.preselectFirstTier();

            this.$resource(ajaxurl).get(
                {
                    action: 'import_items',
                    items: items
                }, 
                function (items, status, request) {
                    items.forEach(function(item) {
                        if (item.access == undefined) {
                            item.access = {};
                        }

                        if (item.access.tiers == undefined) {
                            item.access.tiers = [selected_tier];
                        }

                        that.entries.push(item);
                    });
                }
            );
        },
        bulkAssignTiers: function(tiers) {
            this.entries.forEach(function(entry) {
                entry.access.tiers.forEach(function(tier) {
                    if (tiers.indexOf(parseInt(tier.ID)) !== -1) {
                        tier.selected = true;
                    }
                });
            });
        },
        newEntry: function(type) {
            this.entries.push({ post_type : type });
        },
        newLesson: function(e) {
            e.preventDefault();
            this.newEntry('lesson');

            this.$emit('show_message', 'course_entries', {type: 'success', content: 'You have added a new Lesson to your course!'});
        },
        fetchNewEntry: function(e) {
            e.preventDefault();

            var that = this;
            var selected_tier = this.preselectFirstTier();

            $('.zippy-course-entries .zippy-mb-actions .zippy-spinner').addClass('visible');

            this.$resource(window.ajaxurl).get(
                {action: 'new_entry', id: 0, post_type: 'lesson'}, 
                function (data, status, request) {
                    if (data.access == undefined) {
                        data.access = {};
                    }

                    if (data.access.tiers == undefined) {
                        data.access.tiers = [selected_tier];
                    }

                    // If this is the first entry, set the num_days to 0 by default
                    if (data.scheduling == undefined) {
                        data.scheduling = {};
                    }

                    if (that.entries.length === 0) {
                        data.scheduling.num_days = 0;    
                    }

                    that.entries.push(data);

                    $('.zippy-course-entries .zippy-mb-actions .zippy-spinner').removeClass('visible');
                }
            ).error(
                function (data, status, request) {}
            );
        },
        fetchNewUnitAndEntries: function(num_lessons) {
            var that = this;
            var selected_tier = this.preselectFirstTier();

            $('.zippy-course-entries .zippy-mb-actions .zippy-spinner').addClass('visible');

            this.$resource(window.ajaxurl).get(
                {action: 'new_unit', id: 0, post_type: 'unit', num_children: num_lessons}, 
                function (data, status, request) {
                    data.ux = {
                        show_entries: true
                    };

                    if (data.access == undefined) {
                        data.access = {};
                    }

                    if (data.access.tiers == undefined) {
                        data.access.tiers = [selected_tier];
                    }

                    // If this is the first entry, set the num_days to 0 by default
                    if (data.scheduling == undefined) {
                        data.scheduling = {};
                    }
                    
                    if (that.entries.length === 0) {
                        data.scheduling.num_days = 0;    
                    }


                    data.entries.forEach(function(entry) {
                        if (entry.access == undefined) {
                            entry.access = {};
                        }

                        if (entry.access.tiers == undefined) {
                            entry.access.tiers = [selected_tier];
                        }      

                        if (entry.scheduling == undefined) {
                            entry.scheduling = {};
                        }
                    
                        if (that.entries.length === 0 && entry == data.entries[0]) {
                            entry.scheduling.num_days = 0;    
                        }
                    });

                    that.entries.push(data);

                    $('.zippy-course-entries .zippy-mb-actions .zippy-spinner').removeClass('visible');
                }
            ).error(
                function (data, status, request) {}
            );
        },
        addUnit: function(num_lessons) {
           this.fetchNewUnitAndEntries(num_lessons)
        },
        newUnitWithlessons: function(num_lessons) {
            var entries = [];
            for (var i = num_lessons; i > 0; i--) {
                entries.push({post_type: 'lesson', ID: 0});
            };            

            this.entries.push({
                ux: {
                    show_entries: true,
                },
                post_type: 'unit',
                entries: entries
            });            
        },
        deleteEntry: function(entry, soft_delete) {
            var that = this;
            var record = _.findWhere(this.entries, {ID: entry.ID});

            if (record !== undefined) {
                if (soft_delete && entry.post_type == 'unit') {
                    entry.entries.forEach(function(subentry) {
                        that.entries.push(subentry);
                    });
                }

                this.entries.$remove(entry);
            }

            this.$dispatch('entry_updated', this);
        },
        bulkAddTiers: function(tiers) {

            this.entries.forEach(function(entry) {
                entry.access.tiers.forEach(function(tier) {
                    if(tiers.indexOf(tier.ID) > -1) {
                        tier.selected = true;
                    }
                });

                entry.entries.forEach(function(subentry) {
                    subentry.access.tiers.forEach(function(tier) {
                        if(tiers.indexOf(tier.ID) > -1) {
                            tier.selected = true;
                        }
                    });
                });
            });
        },
        bulkSetScheduling: function(scheduling) {
            var type    = scheduling.day_of_week !== null ? 'day_of_week' : 'increment';
            var level   = this.config.scheduling.control.use;

            if (type == 'increment') {
                if (this.config.scheduling.control.type == 'previous') {
                    this.entries.forEach(function(entry) {
                        entry.scheduling.num_days = scheduling.num_days;

                        entry.entries.forEach(function(subentry) {
                            subentry.scheduling.num_days = scheduling.num_days;
                        });
                    });
                } else {
                    var start = 0,
                        increment = parseInt(scheduling.num_days);
                    
                    scheduling.num_days = 0;

                    this.entries.forEach(function(entry) {
                        if (level == 'item') {
                            if (entry.post_type == 'unit') {
                                entry.entries.forEach(function(subentry) {
                                    subentry.scheduling.num_days = start;
                                    start += increment;
                                });
                            } else {
                                entry.scheduling.num_days = start;
                                start += increment;
                            }
                        } else {
                            entry.scheduling.num_days = start;
                            start += increment;
                        }
                    });
                }                
            } else {
                this.entries.forEach(function(entry) {
                    entry.scheduling.day_of_week = scheduling.day_of_week;

                    entry.entries.forEach(function(subentry) {
                        subentry.scheduling.day_of_week = scheduling.day_of_week;
                    });
                });
            }
        },
        tabToNextEntry: function(e) {
            // If this is a tab key press, intercept it
            if (e.keyCode === 9) {
                e.preventDefault;
                var target = $(e.target);
                if (target.hasClass('entry-title')) {
                    var index = $('input.entry-title').index(target);
                    var next = $('input.entry-title').slice(index+1,index+2);                    
                    var nextEntry = $(next);
                    // Set a small timeout to avoid issues with chrome executing too fast.
                    setTimeout(function() {
                        nextEntry.focus();
                    }, 100);
                }
            }
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./../entry":5,"notice":79,"save_post_meta_mixin":93,"sortable_mixin":96}],5:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var sortable = require('sortable_mixin');

module.exports = {
    name: 'course-entry',
    template: $('#course-entry-tmpl').text(),
    replace: true,
    mixins: [sortable],
    components: {
        access: require('./sections/access'),
        quiz: require('./sections/quiz'),
        scheduling: require('./sections/scheduling')
    },
    props: ['courseTiers', 'config', 'quizzes'],
    events: {
        change_schedule_control_use: 'changeScheduleUse'
    },
    data: function() {
        return {
            ID: 0,
            title: '',
            post_type: '',
            scheduling: {
                day_of_week: null,
                num_days: '1',
                required: false,
                exempt: false
            },
            entries: [],
            config: {},
            /*courseTiers: [],*/
            ux: {
                show_access: false,
                show_scheduling: false,
                show_entries: false,
                show_quiz: false,
            },
            controls: {
                has_scheduling: true,
                has_quiz: true,
                has_access: false,
                delete_mode: 'entry'
            },
            access: {
                tiers: []
            },
            quiz: {
                ID: 0,
                pass: 0,
                required: {
                    completion: false,
                    pass: false
                },                
            },
            post: {},
            quizzes: []
        }
    },
    computed: {
        entriesStyles: function() {
            return this.ux.show_entries ? { height : 'auto', padding: '1em' } : { height : '0px', padding: '0em 1em' };
        },
        entries_list: function() {
            if (this.$.entry !== undefined) {
                var entries = this.$.entry.map(function(entry) {
                    return entry.simplified;
                });

                return entries;
            }

            return [];
        },
        simplified: function() {
            var that = this;

            return {
                ID: this.ID,
                title: this.title,
                post_type: this.post_type,
                entries: this.entries_list,
                tiers: this.access.tiers.filter(function(tier) {
                    return tier.selected;
                }).map(function(tier) {
                    return tier.ID
                }),
                ux: { show_entries: this.ux.show_entries },
                scheduling: this.scheduling,
            }
        },
        showSchedulingDetails: function() {
            return this.post_type !== 'unit';
        }
    },
    watch: {
        courseTiers: {
            handler: 'syncTiers',
            deep: true,
            immediate: true
        }
    },
    created: function() {
        var that = this;

        this.$watch('ux.show_entries', this.refreshSortable);

        this.$watch('scheduling.num_days', function(newVal, oldVal) {
            if (newVal !== null) {
                this.scheduling.day_of_week = null;
            }
        });

        this.$watch('scheduling.day_of_week', function(newVal, oldVal) {
            if (newVal !== null) {
                this.scheduling.num_days = null;
            }
        });

        this.$on('delete_entry', this.deleteEntry);
    },
    compiled: function() {
        this.$watch('title', this.save, { deep: true });

        this.initSortable();

        $(this.$el).addClass('entry-' + this.post_type);

        this.$watch('simplified', function() { this.$dispatch('entry_updated', this); }, { deep: true });
    },
    attached: function() {
        this.$dispatch('entry_updated', this);
    },
    methods: {
        triggerModal: function(e) {
            e.preventDefault();

            this.$dispatch('trigger_modal', $(e.currentTarget).data('modal'), this);
        },
        destroy: function(e) {
            e.preventDefault();

            if (this.entries.length === 0) {
                this.$parent.entries.$remove(this.$index);
            } else {

            }
        },
        toggleScheduling: function(e) {
            e.preventDefault();
            this.ux.show_scheduling = !this.ux.show_scheduling;
        },
        toggleQuiz: function(e) {
            e.preventDefault();
            this.ux.show_quiz = !this.ux.show_quiz;
        },
        toggleEntries: function(e) {
            e.preventDefault();
            this.ux.show_entries = !this.ux.show_entries;
        },
        refreshSortable: function(newVal, oldVal) {
            if (newVal) {
                this.initSortable();                
            }
        },
        toggleAccess: function(e) {
            e.preventDefault();
            this.ux.show_access = !this.ux.show_access;
        },
        save: function() {
            var that = this;
            
            this.post.post_title = this.title;
            this.post.ID = this.ID;
            
            // save item
            this.$resource(ajaxurl).save(
                { action: 'save_post' }, 
                { post: this.post }, 
                function (data, status, request) {}
            ).error(
                function (data, status, request) {}
            );
        },
        syncTiers: function(val) {

            if (this.access.tiers.length > 0 && val.length < 1) {
                return;
            }

            var existing = this.access.tiers
                .filter(function(tier) {
                    return tier.selected;
                })
                .map(function(tier) {
                    return tier.ID;
                });

            // Map the new ones to a set of base objects
            var tiers = val.map(function(tier) {
                return {
                    ID: tier.ID,
                    title: tier.title,
                    selected: false
                };
            });

            // Resync the selected
            tiers.forEach(function(tier) {
                if (existing.indexOf(tier.ID) > -1) {
                    tier.selected = true;
                }
            });

            this.access.tiers = tiers;
        },
        deleteEntry: function(entry, soft_delete) {
            var record      = _.findWhere(this.entries, {ID: entry.ID});

            if (record !== undefined) {
                this.entries.$remove(entry);
            }

            this.$dispatch('entry_updated', this);
        },
        saveAndGoToEdit: function(e) {
            e.preventDefault();

            window.onbeforeunload = null;

            $( '#post' ).prepend( '<input type="hidden" name="zippy_save_and_go_to" value="' + this.ID + '" />' );
            $( '#post' ).submit();
        },
        changeScheduleUse: function(val) {
            if (this.post_type == 'unit') {
                this.controls.has_scheduling = (this.entries.length == 0 || val == 'unit');
            } else {
                this.controls.has_scheduling = (this.$parent.$options.name == 'entries' || val == 'item');                    
            }
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./sections/access":6,"./sections/quiz":7,"./sections/scheduling":8,"sortable_mixin":96}],6:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

module.exports = {
    name: 'entry-access-section',
    template: $('#entry-section-access').text(),
    props: ['tiers']
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],7:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'quiz',
    template: $('#entry-section-quiz').text(),
    /*props: ['quizzes', 'quiz', 'pass', 'pass-required', 'completion-required'],*/
    props: ['quizzes', 'quiz', 'pass', 'required'],
    compiled: function() {
        // this.$dispatch('child_loaded', 'quiz');
    },
    created: function() {
    },
    methods: {
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],8:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

module.exports = {
    name: 'entry-scheduling-section',
    template: $('#entry-section-scheduling').text(),
    props: ['day-of-week', 'config', 'num-days', 'days', 'completion-required', 'exempt', 'ux', 'show-details'],
    data: function() {
        return {
            days: [
                {key: '1', val: 'M'},
                {key: '2', val: 'T'},
                {key: '3', val: 'W'},
                {key: '4', val: 'Th'},
                {key: '5', val: 'F'},
                {key: '6', val: 'Sa'},
                {key: '7', val: 'Su'}
            ],
        }
    },
    compiled: function() {
        this.$watch('day-of-week', this.clearNumDays);
        this.$watch('num-days', this.clearDayOfWeek);
    },
    methods: {
        clearNumDays: function(newVal, oldVal) {
            if (newVal !== null) {
                this.scheduling.num_days = null;
            }
        },
        clearDayOfWeek: function(newVal, oldVal) {
            if (newVal !== null) {
                this.scheduling.day_of_week = null;
            }
        },
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],9:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');

module.exports = {
    name: 'course',
    components: {
        modals      : require('./modals'),
        tiers       : require('./tiers'),
        entries     : require('./entries'),
        products    : require('./products'),
        email_lists : require('./email_lists'),
        'additional-content' : require('./additional-content'),
    },
    mixins: [savePostMetaMixin],
    data: function() {
        return {
            children_loaded: [
                { name: 'entries', loaded: false },
                { name: 'tiers', loaded: false },
                { name: 'products', loaded: false },
                { name: 'email_lists', loaded: false },
                { name: 'additional_content', loaded: false },
            ],
            entries: [],
            additional_content: [],
            existing_content: [],
            tiers: [],
            config: {
                scheduling: {
                    type: 'drip',
                    start: {
                        type: 'join',
                        date: ''
                    },
                    end: {
                        type: 'none',
                        date: '',
                        duration: '0'
                    },
                    control: {
                        type: 'previous',
                        use: 'item'
                    }
                },
                access: {
                    use: 'item'
                },                
            },
            simplified: {
                entries: [],
                additional_content: []
            },
            products: [],
            quizzes: [],
            email_lists: [],
        }
    },
    watch: {
    },
    computed: {
        meta: function() {
            return {
                tiers               : this.tiers,
                entries             : this.simplified.entries,
                config              : this.config,
                products            : this.products,
                email_lists         : this.email_lists_simplified,
                additional_content  : this.simplified.additional_content
            }
        },
        loaded: function() {
            return this.children_loaded.filter(function(child) {
                return !child.loaded;
            }).length < 1;
        },
        course_entries: function() {
            return this.entries.simplified;
        },
        email_lists_simplified: function() {
            return this.email_lists.map(function(service) {
                return { 
                    service: service.id,
                    per_tier: service.per_tier,
                    list: service.list,
                    tiers: service.tiers
                };
            });
        }
    },
    compiled: function() {
    },
    created: function() {

        var that = this,
            data;
    
        this.events();
        this.setupConfig();
        this.setupQuizzes();
        this.setupEntries();
        this.setupExistingContent();
        this.setupAdditionalContent();
        this.setupTiers();
        this.setupModals();
        this.setupProducts();
        this.setupEmailLists();
       
    },
    compiled: function() {
        this.watchers();
    },
    methods: {
        events: function() {
            this.$on('change_schedule_control_use', this.changeScheduleControlUse);
            this.$on('child_loaded', this.childLoaded);
            this.$on('entry_updated', this.updateSimplified);
            this.$on('entry_updated', this.updatePostList);
            this.$on('delete_entry', this.deleteEntry);
            this.$on('delete_additional_content_entry', this.deleteAdditionalContentEntry);
            this.$on('bulk_assign_tiers', this.bulkAssignTiers);
            this.$on('bulk_assign_tiers_to_additional_content', this.bulkAssignTiersToAdditionalContent);
            this.$on('bulk_schedule', this.bulkSchedule);
            this.$on('add_unit', this.addUnit); 
            this.$on('add_items_to_course', this.addItems);
            this.$on('add_additional_content', this.addAdditionalContent);
            this.$on('import_items', this.importItems);
            this.$on('trigger_modal', function(modal, data) {
                this.$broadcast('toggle_modal', modal, data);
            });
        },
        watchers: function() {
            this.$watch('entries', this.mapEntries, { deep : true });
        },
        setupEmailLists: function() {
            var that = this;

            if ($('.vue[email-integration]').length > 0) {
                var data = $('.vue[email-integration]').first().siblings('.zippy-mb-data').text();

                if (data !== undefined) {
                    data = JSON.parse(data);

                    if (data.email_lists !== undefined && Array.isArray(data.email_lists)) {
                        data.email_lists.forEach(function(service) {
                            var my_service = {
                                id: service.service,
                                service: service.service,
                                list: service.list,
                                course_tiers: that.tiers,
                                tiers: service.tiers,
                                per_tier: service.per_tier
                            };
                            
                            that.email_lists.push(my_service);
                        });
                    }
                }
            }

            // Set up any items not already in the list 
            $('.vue[email-integration]').each(function(index, element) {
                var service = $(element).attr('email-integration');
                var record  = _.findWhere(that.email_lists, {id: service});

                if (record === undefined) {
                    var my_service = {
                        id: service,
                        service: service,
                        list: '0',
                        course_tiers: that.tiers
                    };

                    that.email_lists.push(my_service);
                }
            });

            this.$addChild(
                    { data: { services: this.email_lists }},
                    this.$options.components.email_lists
                )
                .$mount()
                .$after('#poststuff');            

            if( $('.vue[email-integration]').length < 1 ) {
                this.childLoaded('email_lists');
                return;
            }
        },
        setupConfig: function() {
            var that = this;
            var data = $('#zippy-course-entries .inside .zippy-mb-data').first().text();

            if (data.length > 0) {
                data = JSON.parse(data);

                if (data.config.scheduling !== undefined && data.config.scheduling.end === undefined) {
                    data.config.scheduling.end = {
                        type: 'none',
                        date: '',
                        duration: '0'
                    };
                }
            }

            this.config = typeof(data) == 'object' && typeof(data.config) == 'object' && data.config.length == this.config.length
                ? data.config
                : this.config;
        },
        setupTiers: function() {
            var that = this;
            var data = $('#zippy-course-tiers .inside .zippy-mb-data').first().text();
            if (data.length) {
                data = JSON.parse(data);
            }

            /**
             * When setting up tiers, we add them to the existing tiers object one at a time instead of
             * replacing the tiers array, so that the original getters and setters are correctly connected.
             */
            var tiers = Array.isArray(data.tiers) ? data.tiers : [{ID: 1, title: 'Basic'}];

            tiers.forEach(function(tier) {
                that.tiers.push(tier);
            });

            this.$addChild(
                { data: { tiers: this.tiers } },
                this.$options.components.tiers
            );
        },
        setupEntries: function() {
            var that = this;
            var data = $('#zippy-course-entries .inside .zippy-mb-data').first().text();

            
            if (data.length) {
                data = JSON.parse(data);
            }
            
            var entries = Array.isArray(data.entries) && data.entries.length > 0 
                ? data.entries 
                : [];

            var mapTiersForEntry = function(entry) {
                var tiers = [];
                entry.tiers.forEach(function(tier) {
                    tiers.push({ID: tier, selected: true});
                });

                entry.access = {
                    tiers: tiers
                };

                return entry;
            };

            entries.forEach(function(entry) {
                entry = mapTiersForEntry(entry);

                if (entry.entries.length > 0) {
                    entry.entries.forEach(function(subentry) {
                        subentry = mapTiersForEntry(subentry);
                    });
                }

                that.entries.push(entry);
            });

            this.$addChild(
                { data: {
                    entries: this.entries,
                    tiers: this.tiers,
                    config: this.config,
                    quizzes: this.quizzes
                }}, 
                this.$options.components.entries
            );
        },
        setupExistingContent: function() {
            var that = this;
            var data = $('#zippy-course-entries .inside .zippy-mb-data').first().text();
            
            if (data.length) {
                data = JSON.parse(data);
            }

            var existing_content = Array.isArray(data.entries) && data.existing_content.length > 0 
                ? data.existing_content 
                : [];

            existing_content.forEach(function(entry) {
                that.existing_content.push(entry);
            });
        },
        setupAdditionalContent: function() {
            var that = this;
            var data = $('#zippy-course-additional-content .inside .zippy-mb-data').first().text();

            if (data.length) {
                data = JSON.parse(data);
            }

            var additional_content = Array.isArray(data.additional_content) && data.additional_content.length > 0 
                ? data.additional_content
                : [];

            additional_content.forEach(function(entry) {
                
                var tiers = [];
                entry.tiers.forEach(function(tier) {
                    tiers.push({ID: tier, selected: true});
                });

                entry.access = {
                    tiers: tiers
                };
                that.additional_content.push(entry);
            });

            this.additional_content = this.additional_content.map(function(entry) {
                entry.controls = {};
                entry.controls.has_access = true;
                entry.controls.has_scheduling = false;
                entry.controls.delete_mode = 'additional_content';
                return entry;
            });

            this.$addChild(
                { data: {
                    additional_content: this.additional_content,
                    entries: this.entries,
                    tiers: this.tiers
                }}, 
                this.$options.components['additional-content']
            );
        },
        setupProducts: function() {
            var that = this;
            var data = $('#zippy-course-public-products .inside .zippy-mb-data').first().text();
            if (data.length) {
                data = JSON.parse(data);
            }

            this.products = Array.isArray(data.products) ? data.products : [];

            this.$addChild(
                { 
                    data: { 
                        products : this.products 
                    }
                },
                this.$options.components.products
            );
        },
        setupQuizzes: function() {
            var that = this;
            var data = $('#zippy-course-entries .inside .zippy-mb-data').first().text();
            if (data.length) {
                data = JSON.parse(data);
            }

            var quizzes = data.quizzes !== undefined && Array.isArray(data.quizzes) ? data.quizzes : [];

            quizzes.forEach(function(quiz) {
                that.quizzes.push(quiz);
            });
        },
        setupModals: function() {
            var that = this;

            var modals = this.$addChild(
                { 
                    data: {
                        config: that.config,
                        tiers: that.tiers,
                        entries: this.entries,
                        additional_content: this.additional_content,
                        existing_content: this.existing_content
                    }
                },
                this.$options.components.modals
            )
            .$mount()
            .$after('#poststuff');
        },
        childLoaded: function(name) {
            if (name == 'entries') {
                this.updateSimplified();
            }

            var record = _.findWhere(this.children_loaded, {name: name});

            if (record !== undefined) {
                record.loaded = true;
            }
        },
        deleteEntry: function(entry, soft_delete) {
            this.$broadcast('delete_entry', entry, soft_delete);
        },
        deleteAdditionalContentEntry: function(entry) {
            this.$broadcast('delete_additional_content_entry', entry);
        },
        addUnit: function(num_lessons) {
            this.$broadcast('add_unit', num_lessons);
        },
        addItems: function(items) {
            this.$broadcast('add_entries', items);
        },
        addAdditionalContent: function(items) {
            this.$broadcast('add_additional_content', items);
        },
        importItems: function(items) {
            this.$broadcast('import_entries', items);
        },
        updateSimplified: function() {
            this.simplified.entries = this.mapEntries(this.entries);
            this.simplified.additional_content = this.mapAdditionalContent(this.additional_content);
        },
        updatePostList: function(entry) {
            this.$broadcast('entry_updated', entry);
        },
        mapEntries: function(entries) {
            var that = this;
            return entries.map(function(entry) {
                return {
                    ID: entry.ID,
                    title: entry.title,
                    post_type: entry.post_type,
                    entries: that.mapEntries(entry.entries),
                    tiers: entry.access.tiers.filter(function(tier) {
                        return tier.selected;
                    }).map(function(tier) {
                        return tier.ID
                    }),
                    ux: {
                        show_entries: entry.ux !== undefined && entry.ux.show_entries !== undefined ? entry.ux.show_entries : true
                    },
                    scheduling: entry.scheduling,
                    quiz: entry.quiz
                }
            });
        },
        mapAdditionalContent: function(entries) {
            var that = this;
            return entries.map(function(entry) {
                return {
                    ID: entry.ID,
                    title: entry.title,
                    post_type: entry.post_type,
                    tiers: entry.access.tiers.filter(function(tier) {
                        return tier.selected;
                    }).map(function(tier) {
                        return tier.ID
                    })
                }
            });
        },
        bulkAssignTiers: function(tiers) {
            this.$broadcast('add_tiers', tiers);
        },
        bulkAssignTiersToAdditionalContent: function(tiers) {
            this.$broadcast('bulk_assign_tiers_to_additional_content', tiers);
        },
        bulkSchedule: function(scheduling) {
            this.$broadcast('set_scheduling', scheduling);
        },
        changeScheduleControlUse: function(val) {
            this.$broadcast('change_schedule_control_use', val);
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./additional-content":1,"./email_lists":2,"./entries":4,"./modals":18,"./products":26,"./tiers":27,"save_post_meta_mixin":93}],10:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'access-bulk-edit-modal',
    template: $('#zippy-access-bulk-edit-modal-tmpl').text(),
    props: ['courseTiers'],
    data: function() {
        return {
            tiers: []
        }
    },
    watch: {
        courseTiers: {
            handler: 'syncTiers',
            deep: true,
            immediate: true
        }
    },
    computed: {
        selected: function () {
            return this.tiers.filter(function(tier) {
                return tier.selected;
            }).map(function(tier) {
                return tier.ID;
            });
        }
    },
    methods: {
        syncTiers: function(tiers) {
            this.tiers = tiers.map(function(tier) {
                return {
                    ID: tier.ID, 
                    title: tier.title,
                    selected: false
                };
            });
        },
        bulkAssign: function(e) {
            e.preventDefault();

            this.$dispatch('bulk_assign_tiers', this.selected);
            
            this.tiers.forEach(function(tier) {
                tier.selected = false;
            });

            $('#zippy-access-bulk-edit-modal').modal('hide');
        }
    }
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],11:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'access-config-modal',
    template: $('#zippy-access-config-modal-tmpl').text(),
    data: function() {
        return {}
    },
    computed: {
    },
    compiled: function() {
    },
    created: function() {
    },
    methods: {
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],12:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'new-unit-modal',
    template: $('#zippy-add-unit-modal-tmpl').text(),
    data: function() {
        return {
            num_lessons: 0
        }
    },
    computed: {
    },
    compiled: function() {
    },
    created: function() {
    },
    methods: {
        addUnitWithLessons: function(e) {
            e.preventDefault();
            this.$dispatch('add_unit', this.num_lessons);
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],13:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'course-additional-content-modal',
    template: $('#zippy-add-additional-content-modal-tmpl').text(),
    data: function() {
        return {
            list: [],
            existing_content: [],
            title_filter: '',
            post_types: [],
            messages: [],
        }
    },
    components: {
        notice: require('notice')
    },
    watch: {
        mode: 'switchMode'
    },
    events: {
        show_message: 'showNotice'
    },
    computed: {
        selected: function() {
            return this.list
                .filter(function (item) {
                    return item.selected
                })
                .map(function (item) {
                    return item.ID
                });
        },
        selected_counts: function() {
            var that = this;

            var post_types = this.post_types
                .filter(function(post_type) {
                    return that.selectedCount(post_type.post_type);
                })
                .map(function(post_type) {
                    post_type.count = that.selectedCount(post_type.post_type);
                    return post_type;
                });

            return post_types;
        },
        active_post_type: function() {
            var active = this.post_types.filter(function(post_type) {
                return post_type.active;
            }).shift();

            return active.post_type !== undefined ? active.post_type : '';
        },
        active_post_type_label: function() {
            var active = this.post_types.filter(function(post_type) {
                return post_type.active;
            }).shift();

            return active.label !== undefined ? active.label : '';
        },
        active_list: function() {
            var that = this;

            return this.list
                .filter(function(item) {
                    return item.post_type == that.active_post_type;
                })
                .filter(function(item) {
                    return that.existing_list.indexOf(parseInt(item.ID)) === -1;
                });                
        },
        existing_list: function() {
            var that = this,
                existing = [];

            this.additional_content.forEach(function(entry) {
                existing.push(entry.ID);
            });

            this.entries.forEach(function(entry) {
                existing.push(entry.ID);
            });

            return existing;
        }
    },
    compiled: function() {
    },
    created: function() {
        this.fetchPostTypes();
        this.fetchEntries();

        this.$on('entry_updated', this.updateEntry);
    },
    methods: {
        switchMode: function() {
            this.list = this.list.map(function(item) {
                item.selected = false;
                return item;
            });
        },
        showNotice: function(receiver, notice) {
            if (receiver != 'add_existing_content_modal') {
                return;
            }

            this.messages.push(notice);
        },
        selectedCount: function(post_type) {
            return this.list
                .filter(function (item) {
                    return item.post_type == post_type;
                })
                .filter(function (item) {
                    return item.selected
                })
                .length;
        },
        clearSelected: function(post_type) {
            this.list.map(function(item) {
                if(item.post_type == post_type) {
                    item.selected = false;
                }

                return item;
            });
        },
        switchPostType: function(e) {
            e.preventDefault();

            this.post_types.map(function(post_type) {
                post_type.active = false;
                return post_type;
            });

            e.targetVM.post_type.active = true;
        },
        addAdditionalContent: function(e) {
            e.preventDefault();
            
            // $('.modal.in').modal('hide');
            
            this.$dispatch('add_additional_content', this.selected);
            var message = 'You have added ' + this.selected.length + ' entries to your course!';

            this.$emit(
                'show_message', 
                'add_existing_content_modal', 
                {
                    type: 'success', 
                    content: message
                }
            );

            this.list.forEach(function(item) {
                item.selected = false;
            });
        },
        fetchPostTypes: function() {
            var that = this;

            this.$resource(ajaxurl).get({ action: 'get_post_types'}, 
                function (post_types, status, request) {
                    var i = 0;
                    that.post_types = post_types.map(function(post_type) {
                        post_type.active = i === 0 ? true : false;
                        i++;

                        return post_type;
                    });
                }
            );
        },
        fetchEntries: function() {
            this.$set('list', this.existing_content);
        },
        updateEntry: function(entry) {
            var record = _.findWhere(this.list, { ID: entry.id });

            if (record !== undefined) {
                record.title = entry.title;
            }
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"notice":79}],14:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'bulk-edit-additional-content-access-modal',
    template: $('#zippy-bulk-edit-additional-content-access-modal-tmpl').text(),
    props: ['courseTiers'],
    data: function() {
        return {
            tiers: []
        }
    },
    watch: {
        courseTiers: {
            handler: 'syncTiers',
            deep: true,
            immediate: true
        }
    },
    computed: {
        selected: function () {
            return this.tiers.filter(function(tier) {
                return tier.selected;
            }).map(function(tier) {
                return tier.ID;
            });
        }
    },
    created: function() {
        this.$on('push_tiers', this.syncTiers);
        this.$dispatch('get_tiers');
    },
    methods: {
        syncTiers: function(tiers) {
            this.tiers = tiers.map(function(tier) {
                return {
                    ID: tier.ID, 
                    title: tier.title,
                    selected: false
                };
            });
        },
        bulkAssign: function(e) {
            e.preventDefault();

            this.$dispatch('bulk_assign_tiers_to_additional_content', this.selected);
            
            this.tiers.forEach(function(tier) {
                tier.selected = false;
            });
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],15:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'delete-entry-modal',
    template: $('#zippy-delete-additional-content-entry-modal-tmpl').text(),
    data: function() {
        return {
            passed_data: {}
        }
    },
    methods: {
        deleteEntry: function(e) {
            e.preventDefault();
            $('.modal.in').modal('hide');

            this.$dispatch('delete_additional_content_entry', this.passed_data);
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],16:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'delete-entry-modal',
    template: $('#zippy-delete-entry-modal-tmpl').text(),
    data: function() {
        return {
            soft_delete: 'yes',
            passed_data: {}
        }
    },
    methods: {
        deleteEntry: function(e) {
            e.preventDefault();
            $('.modal.in').modal('hide');
            this.$dispatch('delete_entry', this.passed_data, (this.soft_delete == 'yes'));
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],17:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'course-existing-content-modal',
    template: $('#zippy-add-existing-content-modal-tmpl').text(),
    data: function() {
        return {
            list: [],
            title_filter: '',
            post_types: [],
            existing_content: [],
            messages: [],
            mode: 'import',
            modes: [
                {text: 'Add', value: 'add'},
                {text: 'Import', value: 'import'},
            ]
        }
    },
    components: {
        notice: require('notice')
    },
    watch: {
        mode: 'switchMode'
    },
    events: {
        show_message: 'showNotice'
    },
    computed: {
        selected: function() {
            return this.list
                .filter(function (item) {
                    return item.selected
                })
                .map(function (item) {
                    return item.ID
                });
        },
        selected_counts: function() {
            var that = this;

            var post_types = this.post_types
                .filter(function(post_type) {
                    return that.selectedCount(post_type.post_type);
                })
                .map(function(post_type) {
                    post_type.count = that.selectedCount(post_type.post_type);
                    return post_type;
                });

            return post_types;
        },
        active_post_type: function() {
            var active = this.post_types.filter(function(post_type) {
                return post_type.active;
            }).shift();

            return active.post_type !== undefined ? active.post_type : '';
        },
        active_post_type_label: function() {
            var active = this.post_types.filter(function(post_type) {
                return post_type.active;
            }).shift();

            return active.label !== undefined ? active.label : '';
        },
        active_list: function() {
            var that = this;

            return this.list
                .filter(function(item) {
                    return item.post_type == that.active_post_type;
                })
                .filter(function(item) {
                    return that.mode == 'add' ? item.in_use === false : item.in_use === true;
                })
                .filter(function(item) {
                    return that.course_entries_list.indexOf(parseInt(item.ID)) === -1;
                });
                
        },
        course_entries_list: function() {
            var that = this,
                list = [];

            this.entries.forEach(function(entry) {
                list.push(entry.ID);

                if (entry.entries.length > 0) {
                    entry.entries.forEach(function(child_entry) {
                        list.push(child_entry.ID);
                    });
                }
            });

            return list;
        }
    },
    compiled: function() {
    },
    created: function() {
        this.fetchPostTypes();
        this.fetchEntries();

        this.$on('entry_updated', this.updateEntry);
    },
    methods: {
        switchMode: function() {
            this.list = this.list.map(function(item) {
                item.selected = false;
                return item;
            });
        },
        showNotice: function(receiver, notice) {
            if (receiver != 'add_existing_content_modal') {
                return;
            }

            this.messages.push(notice);
        },
        selectedCount: function(post_type) {
            return this.list
                .filter(function (item) {
                    return item.post_type == post_type;
                })
                .filter(function (item) {
                    return item.selected
                })
                .length;
        },
        clearSelected: function(post_type) {
            this.list.map(function(item) {
                if(item.post_type == post_type) {
                    item.selected = false;
                }

                return item;
            });
        },
        switchPostType: function(e) {
            e.preventDefault();

            this.post_types.map(function(post_type) {
                post_type.active = false;
                return post_type;
            });

            e.targetVM.post_type.active = true;
        },
        addToCourse: function(e) {
            e.preventDefault();
            
            $('.modal.in').modal('hide');
            
            if (this.mode == 'add') {
                this.$dispatch('add_items_to_course', this.selected);
                var message = 'You have added ' + this.selected.length + ' entries to your course!';
            } else {
                this.$dispatch('import_items', this.selected);
                var message = 'You have imported ' + this.selected.length + ' entries to your course!'
            }

            this.$emit(
                'show_message', 
                'add_existing_content_modal', 
                {
                    type: 'success', 
                    content: message
                }
            );

            this.list.forEach(function(item) {
                item.selected = false;
            });
        },
        fetchPostTypes: function() {
            var that = this;

            this.$resource(ajaxurl).get({ action: 'get_post_types'}, 
                function (post_types, status, request) {
                    var i = 0;
                    that.post_types = post_types.map(function(post_type) {
                        post_type.active = i === 0 ? true : false;
                        i++;

                        return post_type;
                    });
                }
            );
        },
        fetchEntries: function() {
            this.$set('list', this.existing_content);
        },
        updateEntry: function(entry) {
            var record = _.findWhere(this.list, { ID: entry.id });

            if (record !== undefined) {
                record.title = entry.title;
            }
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"notice":79}],18:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

module.exports = {
    name: 'course-modals',
    components: {
        addExistingContent: require('./existing-content'),
        addAdditionalContent: require('./additional-content'),
        accessBulkEdit: require('./access-bulk-edit'),
        accessConfig: require('./access-config'),
        schedulingBulkEdit: require('./scheduling-bulk-edit'),
        schedulingConfig: require('./scheduling-config'),
        addUnit: require('./add-unit'),
        deleteEntry: require('./delete-entry'),
        deleteAdditionalContentEntry: require('./delete-additional-content-entry'),
        bulkEditAdditionalContentAccess: require('./bulk-edit-additional-content-access')
    },
    created: function() {
        this.$on('toggle_modal', this.toggle);
    },
    compiled: function() {
        
        var that = this,
            modals = [
                'addExistingContent',
                'addAdditionalContent',
                'accessBulkEdit', 
                'bulkEditAdditionalContentAccess',
                'accessConfig', 
                'schedulingBulkEdit', 
                'schedulingConfig', 
                'addUnit', 
                'deleteEntry',
                'deleteAdditionalContentEntry',
            ];

        modals.forEach(function(modal) {

            var data = {
                config: that.config,
                courseTiers: that.tiers
            }

            if (modal == 'addExistingContent') {
                data.entries = that.entries;
                data.existing_content = that.existing_content;
            }

            if (modal == 'addAdditionalContent') {
                data.entries = that.entries;
                data.additional_content = that.additional_content;
                data.existing_content = that.existing_content;
            }

            that.$addChild({ data : data },
            that.$options.components[modal])
            .$mount().$after('#poststuff');
        });
    },
    methods: {
        toggle: function(modal_id, data) {
            var modal = $(modal_id)[0]['__vue__'];
            $(modal_id).modal('toggle');

            if(modal.$options.name == 'deleteEntry') {
                modal.$set('soft_delete', 'yes');
            }

            if(data !== undefined) {
                modal.$set('passed_data', data.$data);
            }
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./access-bulk-edit":10,"./access-config":11,"./add-unit":12,"./additional-content":13,"./bulk-edit-additional-content-access":14,"./delete-additional-content-entry":15,"./delete-entry":16,"./existing-content":17,"./scheduling-bulk-edit":19,"./scheduling-config":20}],19:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'scheduling-bulk-edit-modal',
    template: $('#zippy-scheduling-bulk-edit-modal-tmpl').text(),
    data: function() {
        return {
            days: [
                {key: '1', val: 'M'},
                {key: '2', val: 'T'},
                {key: '3', val: 'W'},
                {key: '4', val: 'Th'},
                {key: '5', val: 'F'},
                {key: '6', val: 'Sa'},
                {key: '7', val: 'Su'}
            ],
            scheduling_options: {
                day_of_week: null,
                num_days: '1'
            },
        }
    },
    computed: {
        item_type: function() {
            // return (this.scheduling.control.use == 'item') ? 'Entry' : 'Unit';
            return (this.config.scheduling.control.use == 'item') ? 'item' : 'unit';
        }
    },
    compiled: function() {
        
    },
    created: function() {
        this.$watch('scheduling_options.num_days', function(newVal, oldVal) {
            if (newVal !== null) {
                this.scheduling_options.day_of_week = null;
            }
        });

        this.$watch('scheduling_options.day_of_week', function(newVal, oldVal) {
            if (newVal !== null) {
                this.scheduling_options.num_days = null;
            }
        });
    },
    methods: {
        bulkSchedule: function(e) {
            e.preventDefault();
            
            $('.modal.in').modal('hide');
            this.$dispatch('bulk_schedule', this.scheduling_options);
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],20:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'scheduling-config-modal',
    template: $('#zippy-scheduling-config-modal-tmpl').text(),
    data: function() {
        return {}
    },
    components: {
        "scheduling-control": require('./sections/control'),
        "scheduling-start-type": require('./sections/start-type'),
        "scheduling-end-type": require('./sections/end-type'),
        "scheduling-method": require('./sections/method'),
        "scheduling-type": require('./sections/type'),
    },
    computed: {
    },
    compiled: function() {
    },
    created: function() {
    },
    methods: {
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./sections/control":21,"./sections/end-type":22,"./sections/method":23,"./sections/start-type":24,"./sections/type":25}],21:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'scheduling-control-modal-section',
    template: $('#zippy-scheduling-control-section-tmpl').text(),
    props: ['scheduling'],
    watch: {
        'scheduling.control.use': {
            handler: 'changeSchedulingControl',
            deep: true
        }
    },
    methods: {
        changeSchedulingControl: function(val) {
            this.$dispatch('change_schedule_control_use', val);
        }
    },
    compiled: function() {
        this.$dispatch('change_schedule_control_use', this.scheduling.control.use);
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],22:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'scheduling-end-type-modal-section',
    template: $('#zippy-scheduling-end-type-section-tmpl').text(),
    props: ['scheduling'],
    watch: {
        'scheduling.start.type': {
            handler: 'changeStartType',
            deep: true
        }
    },
    methods: {
        changeStartType: function() {
            if (this.scheduling.end.type == 'none') {
                return;
            }
            
            if (this.scheduling.start.type == 'join') {
                if (this.scheduling.end.duration == 0 || this.scheduling.end.duration == '') {
                    this.scheduling.end.type = 'none';
                } else {
                    this.scheduling.end.type = 'duration';                    
                }
            }

            if (this.scheduling.start.type == 'date') {
                if (this.scheduling.end.date == '') {
                    this.scheduling.end.type = 'none';
                } else {
                    this.scheduling.end.type = 'date';                    
                }
            }
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],23:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'scheduling-method-modal-section',
    template: $('#zippy-scheduling-method-section-tmpl').text(),
    props: ['scheduling'],
    computed: {
    },
    compiled: function() {
    },
    created: function() {
    },
    methods: {
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],24:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'scheduling-start-type-modal-section',
    template: $('#zippy-scheduling-start-type-section-tmpl').text(),
    props: ['scheduling'],
    computed: {
    },
    compiled: function() {
    },
    created: function() {
    },
    methods: {
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],25:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'scheduling-type-modal-section',
    template: $('#zippy-scheduling-type-section-tmpl').text(),
    props: ['scheduling'],
    compiled: function() {
    },
    created: function() {
    },
    methods: {
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],26:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var sortable = require('sortable_products_mixin');

module.exports = {
    name: 'course-products',
    el: function() { return '#zippy-course-public-products .inside .vue'; },
    mixins: [sortable],
    template: $('#course-products-tmpl').text(),
    props: ['products'],
    data: function() {
        return {
            products: [],
            courseProducts: [],
            activeOptions: [
                {value: '1', text: 'Yes'},
                {value: '0', text: 'No'},
            ],
            loaded: false
        }
    },
    watch: {
        courseProducts: {
            handler: 'updateProducts',
            deep: true
        }
    },
    created: function() {
        this.fetchCourseProducts();
    },
    methods: {
        saveAndGo: function(e) {
            e.preventDefault();

            window.onbeforeunload = null;

            $( '#post' ).prepend( '<input type="hidden" name="zippy_save_and_go_to" value="' + $(e.currentTarget).attr('href') + '" />' );
            $( '#post' ).submit();
        },
        fetchCourseProducts: function() {
            var that = this;

            this.$resource(ajaxurl).get({ action: 'get_course_products', id: $('#post_ID').val() }, 
                function (products, status, request) {
                    if (Array.isArray(products)) {
                        that.syncProducts(products);                        
                    }
                    this.initSortable();
 
                    that.loaded = true;
                    that.$dispatch('child_loaded', 'products');
                }
            );
        },
        syncProducts: function(products) {
            var that = this;

            products.forEach(function(product) {
                product.selected = that.products.indexOf(product.ID) > -1 ? '1' : '0';
            });

            this.courseProducts = products;
            this.sortByListOrder(this.courseProducts);
        },
        updateProducts: function(val, oldVal) {
            var that = this;
            val = _.sortBy(val, 'list_order');
            var products = val
                    .filter(function(product) {
                        return product.selected == '1';
                    })
                    .map(function(product) {
                        return product.ID;
                    });

            this.products.splice(0,this.products.length);

            products.forEach(function(product) {
                that.products.push(product);
            });
        },
        sortByListOrder: function(products) {
            this.courseProducts = _.sortBy(products, 'list_order');
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"sortable_products_mixin":95}],27:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'course-tiers',
    el: function() { return '#zippy-course-tiers .inside .vue'; },
    template: $('#course-tier-tmpl').text(),
    compiled: function() {
        this.$dispatch('child_loaded', 'tiers');
    },
    methods: {
        newTier: function(e) {
            e.preventDefault();

            var tier = {
                ID: this.getNextId(),
                title: ''
            };

            this.tiers.push(tier);
        },
        getNextId: function() {
            var last = _.max(this.tiers, function(tier) {
                return tier.ID;
            });

            return typeof last === 'object' ? (last.ID + 1) : 0;
        },
        destroy: function(e) {
            this.tiers.$remove(e.targetVM.$index);
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],28:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);


if ($('.zippy-student-course-export').length > 0) {
    var ZippyCourses_StudentCourseExport = new Vue({
        name: 'student-course-export',
        el: '.zippy-student-course-export',
        created: function() {
        },
        compiled: function() {
            $('.zippy-student-course-export .button-lg').on('click', this.exportStudentsByCourse);
            $('.zippy-student-course-export .zippy-toggle-all').on('click', this.toggleAllCourses);
            $('.zippy-student-course-export input[type="checkbox"]').not('.zippy-toggle-all').on('click', this.checkToggleAll);
        },
        methods: {
            exportStudentsByCourse: function(e) {
                e.preventDefault();

                var ids = [];
                var url = $(e.currentTarget).data('url');

                $('.zippy-student-course-export input[type="checkbox"]:checked').not('.zippy-toggle-all').each(function(index, element) {
                    ids.push($(element).val());
                });
                
                url += '&type=course&ids=' + ids.join(',');

                window.location = url;
            },
            toggleAllCourses: function() {
                var checked = !$('.zippy-student-course-export .zippy-toggle-all').is(':checked');
                $('.zippy-student-course-export').find('input[type="checkbox"]').prop("checked", !checked);

                if (!checked) {
                    $('.zippy-student-course-export .button-lg').removeAttr('disabled');
                } else {
                    $('.zippy-student-course-export .button-lg').attr('disabled', 'disabled');
                }
            },
            checkToggleAll: function() {
                var length      = $('.zippy-student-course-export input[type="checkbox"]').not('.zippy-toggle-all').length;
                var num_checked = $('.zippy-student-course-export input[type="checkbox"]:checked').not('.zippy-toggle-all').length;
                var is_checked  = length == num_checked;

                $('.zippy-student-course-export .zippy-toggle-all').prop('checked', is_checked);

                if (num_checked > 0) {
                    $('.zippy-student-course-export .button-lg').removeAttr('disabled');
                } else {
                    $('.zippy-student-course-export .button-lg').attr('disabled', 'disabled');
                }
            },
        }
    });
}

if ($('.zippy-student-product-export').length > 0) {
    var ZippyCourses_StudentProductExport = new Vue({
        name: 'student-product-export',
        el: '.zippy-student-product-export',
        created: function() {
        },
        compiled: function() {
            $('.zippy-student-product-export .button-lg').on('click', this.exportStudentsByProduct);
            $('.zippy-student-product-export .zippy-toggle-all').on('click', this.toggleAllProducts);
            $('.zippy-student-product-export input[type="checkbox"]').not('.zippy-toggle-all').on('click', this.checkToggleAll);
        },
        methods: {
            exportStudentsByProduct: function(e) {
                e.preventDefault();

                var ids = [];
                var url = $(e.currentTarget).data('url');

                $('.zippy-student-product-export input[type="checkbox"]:checked').not('.zippy-toggle-all').each(function(index, element) {
                    ids.push($(element).val());
                });

                url += '&type=product&ids=' + ids.join(',');

                window.location = url;
            },
            toggleAllProducts: function() {
                var checked = !$('.zippy-student-product-export .zippy-toggle-all').is(':checked');
                $('.zippy-student-product-export').find('input[type="checkbox"]').prop("checked", !checked);

                if (!checked) {
                    $('.zippy-student-product-export .button-lg').removeAttr('disabled');
                } else {
                    $('.zippy-student-product-export .button-lg').attr('disabled', 'disabled');
                }
            },
            checkToggleAll: function() {
                var length      = $('.zippy-student-product-export input[type="checkbox"]').not('.zippy-toggle-all').length;
                var num_checked = $('.zippy-student-product-export input[type="checkbox"]:checked').not('.zippy-toggle-all').length;
                var is_checked  = length == num_checked;

                $('.zippy-student-product-export .zippy-toggle-all').prop('checked', is_checked);

                if (num_checked > 0) {
                    $('.zippy-student-product-export .button-lg').removeAttr('disabled');
                } else {
                    $('.zippy-student-product-export .button-lg').attr('disabled', 'disabled');
                }
            },
        }
    });
}
if ($('.zippy-order-export').length > 0) {
    var ZippyCourses_OrderExport = new Vue({
        name: 'zippy-order-export',
        el: '.zippy-order-export',
        created: function() {
        },
        compiled: function() {
            $('.zippy-order-export .button-lg').on('click', this.exportOrders);
            $('.zippy-order-export .zippy-toggle-all').on('click', this.toggleAllProductOrders);
            $('.zippy-order-export input[type="checkbox"]').not('.zippy-toggle-all').on('click', this.checkToggleAll);

        },
        methods: {
            exportOrders: function(e) {
                e.preventDefault();
                var ids = [];
                var startDate = $('#zippy_order_start_date').val();
                startDate = (Date.parse(startDate));
                startDate = startDate / 1000;
                var endDate = $('#zippy_order_end_date').val();
                endDate = (Date.parse(endDate)) / 1000;

                var url = $(e.currentTarget).data('url');

                $('.zippy-order-export input[type="checkbox"]:checked').not('.zippy-toggle-all').each(function(index, element) {
                    ids.push($(element).val());
                });

                url += '&type=orders&ids=' + ids.join(',');
                url += '&start_date=' + startDate + '&end_date=' + endDate;

                window.location = url;
            },
            toggleAllProductOrders: function() {
                var checked = !$('.zippy-order-export .zippy-order-products .zippy-toggle-all').is(':checked');
                $('.zippy-order-export .zippy-order-products').find('input[type="checkbox"]').prop("checked", !checked);
            },
            checkToggleAll: function() {
                var length      = $('.zippy-order-export input[type="checkbox"]').not('.zippy-toggle-all').length;
                var num_checked = $('.zippy-order-export input[type="checkbox"]:checked').not('.zippy-toggle-all').length;
                var is_checked  = length == num_checked;

                $('.zippy-order-export .zippy-toggle-all').prop('checked', is_checked);
            },
        }
    });
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],29:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);


if ($('.zippy-list-table').length > 0) {
    var ZippyCourses_ListTable = new Vue({
        name: 'list-tables',
        el: '.zippy-list-table',
        created: function() {
        },
        compiled: function() {
            $('.zippy-list-table .zippy-submit-list-table-filters').on('click', this.filterQuiz);
            $('.zippy-list-table-filter').on('change', this.syncFilters);
        },
        methods: {
            filterQuiz: function(e) {
                e.preventDefault();
                $('.zippy-list-table').submit();
            },
            syncFilters: function(e) {
                var name = $(e.currentTarget).attr('name');
                var val  = $(e.currentTarget).val();

                $('select[name="' + name + '"]').val(val);
            }
        }
    });
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],30:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('.zippy-courses-analytics').length > 0) {
    var ZippyCourses_CourseActivity_Metabox = new Vue({
        el: '.zippy-courses-analytics',
        events: {
        },
        components: {
            courseActivityTable: require('./views-table'),
        },
        created: function() {
            var that = this;
            $('.zippy-analytics-course-dropdown').on('change', function() {
                that.switchCourse(parseInt($(this).val()));
            });
        },
        methods: {
            switchCourse: function(course_id) {
                var url = ZippyCourses.admin_url + 'admin.php?page=zippy-analytics&tab=courses';

                if (course_id !== 0) {
                    url += '&course=' + course_id;
                }

                window.location = url;
            }
        }
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./views-table":31}],31:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'course-activity-table',
    data: function() {
        return {
            all_views: [],
            recent_views: [],
            reverse: false,
            sortKey: '',
            mode: 'recent'
        }
    },
    created: function() {
        var recent_views = [];
        var all_views = [];
        var that = this;
        var data = JSON.parse($('.zippy-course-analytics-tables .zippy-mb-data').text());

        this.all_views    = (Array.isArray(data.all_views)) ? data.all_views : all_views;
        this.recent_views = (Array.isArray(data.recent_views)) ? data.recent_views : recent_views;
    },
    computed: {
        views: function() {
            if (this.mode === 'recent') {
                return this.recent_views;
            } else {
                return this.all_views;
            }
        }
    },
    methods: {
        toggleTimespan: function(e) {
            e.preventDefault();
            var mode = (this.mode == 'recent') ? 'all' : 'recent';
            this.mode = mode;
        },
        sortBy: function(sortKey) {
            this.reverse = (this.sortKey == sortKey) ? !this.reverse : false;
            this.sortKey = sortKey;
        },
        getHumanReadableTime: function(ms)
        {
            var output = '',
                x,
                seconds,
                minutes,
                hours,
                days;

            x = ms / 1000;
            seconds = Math.ceil(x % 60);

            x /= 60;
            minutes = Math.floor(x % 60);

            x /= 60;
            hours = Math.floor(x % 24);

            x /= 24;
            days = Math.floor(x);

            if (days > 0) {
                output += ' ' + days + ' d';
            }

            if (hours > 0) {
                output += ' ' + hours + ' hr';
            }

            if (minutes > 0) {
                output += ' ' + minutes + ' min';
            }

            if (seconds > 0) {
                output += ' ' + seconds + ' sec';
            }
            
            if (output.trim().length == 0) {
                output = '0 sec';
            }
            
            return output.trim();
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],32:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('#zippy-course-completion-certificate').length > 0) {
    var ZippyCourses_CourseCompletionCertificate_Metabox = new Vue({
        el: function() { return '#zippy-course-completion-certificate .inside .vue'; },
        template: $('#course-certificate-tmpl').text(),
        data: function() {
            return {
                enabled: false,
                title: '',
                institution: '',
                instructor: '',
                requirement: 0,
                permalink: ''
            }
        },
        events: {
            'hook:created' : 'setup'
        },
        computed: {
            meta: function() {
                return {
                    certificate: {
                        enabled: this.enabled,
                        title: this.title,
                        institution: this.institution,
                        instructor: this.instructor,
                    }
                };
            }
        },
        components: {
        },
        created: function() {

        },
        methods: {
            setup: function() {
                var data = $('#zippy-course-completion-certificate .zippy-mb-data').text();
                if (data.length > 0) {
                    data = JSON.parse(data);
                    data = data.certificate;
                } else {
                    data = {};
                }
                this.enabled = data.enabled !== undefined ? data.enabled : false;
                this.title = data.title !== undefined ? data.title : '';
                this.institution = data.institution !== undefined ? data.institution : '';
                this.instructor = data.instructor !== undefined ? data.instructor : '';
                this.permalink = document.getElementById('zippy-course-permalink').value;
            },
            previewCertificate: function(e) {
                e.preventDefault();
                var certificateURL = this.permalink + 'certificate';
                var queryData = {
                    preview_certificate: true,
                    title: this.title,
                    institution: this.institution,
                    instructor: this.instructor,
                }
                var serialized = $.param(queryData);
                window.open(certificateURL + '?' + serialized);
            },
        },
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],33:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'dashboard-course',
    template: $('#dashboard-courses-course-tmpl').text(),
    data: function() {
        return {
            id: 0,
            title: ''
        }
    },
    computed: {
        urls: function() {
            return {
                edit        : window.ZippyCourses.admin_url + 'post.php?post=' + this.id + '&action=edit',
                view        : window.ZippyCourses.site_url + '?p=' + this.id,
                analytics   : window.ZippyCourses.admin_url + 'admin.php?page=zippy-analytics&tab=courses&course=' + this.id,
                delete      : window.ZippyCourses.admin_url + '#'
            };
        },
    },
    created: function() {
    },
    compiled: function() {
    },
    methods: {
        deleteCourse: function(e) {
            e.preventDefault();

            var confirm_delete = confirm("Are you sure you want to delete this course? This cannot be undone.");
            var that = this;

            if (confirm_delete) {

                that.$parent.courses.$remove(that.$index);

                this.$resource(window.ajaxurl).delete(
                    { action: 'delete_course' }, 
                    { id: this.id }, 
                    function (data, status, request) {}
                ).error(
                    function (data, status, request) {}
                );
            }
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],34:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('#zippy-dashboard-courses').length > 0) {
    var ZippyCourses_DashboardCoursesMetabox = new Vue({
        el: '#zippy-dashboard-courses .inside .vue',
        name: 'dashboard-courses-metabox',
        template: $('#dashboard-courses-tmpl').text(),
        components: {
            'course'  : require('./course')
        },
        data: function() {
            return {
                courses: [],
                searchText: '',
                button: window.ZippyCourses.strings.new_course_button,
                current_page: 1,
                posts_per_page: parseInt(window.ZippyCourses.options.posts_per_page),
                urls: {
                    new: window.ZippyCourses.admin_url + 'post-new.php?post_type=course'
                }
            }
        },
        computed: {
            paged_courses: function() {
                var start = (this.current_page - 1) * this.posts_per_page,
                    end = start + this.posts_per_page,
                    regex = new RegExp(this.searchText, 'ig'),
                    courses = this.courses.filter(function(course) {
                        return course.title.match(regex);
                    });

                return courses.slice(start, end);
            },
            max_pages: function() {
                var that = this,
                    regex = new RegExp(that.searchText, 'ig'),
                    length = this.courses.filter(function(course) {
                        return course.title.match(regex);
                    }).length,
                    max = Math.ceil(length / this.posts_per_page);

                return max > 0 ? max : 1;
            },
        },
        events: {
        },
        watch: {
            current_page: {
                handler: function(val, oldVal) {
                    if (this.current_page < 1 ) {
                        this.current_page = 1;
                    }
                }
            },
            max_pages: {
                handler: function(val, oldVal) {
                    if (this.current_page > val) {
                        this.current_page = val;
                    }
                }
            }
        },
        created: function() {
        },
        compiled: function() {
            this.setup();
            this.current_page = 1;
        },
        methods: {
            setup: function() {
                var that = this,
                    data = JSON.parse($(this.$el).parent().find('.zippy-mb-data').text());
                    courses = data.courses;
                    
                this.courses = courses;
            },
            nextPage: function(e) {
                e.preventDefault();
                if (this.current_page < this.max_pages) {
                    this.current_page++;
                }
            },
            prevPage: function(e) {
                e.preventDefault();
                if (this.current_page > 1) {
                    this.current_page--;
                }
            }
        }
    });
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./course":33}],35:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var wp = (typeof window !== "undefined" ? window['wp'] : typeof global !== "undefined" ? global['wp'] : null);

module.exports = {
    name: 'download',
    data: function() {
        return {
            uid: '',
            title: '',
            type: 'video',
            url: ''                
        }
    },
    events: {
        generateUid: 'generateUid'
    },
    created: function() {
        this.setupFileFrame();

        if (this.uid === '' || this.uid === undefined) {
            this.$emit('generateUid');
        }
    },
    methods: {
        openFileFrame: function(e) {
            e.preventDefault();
            this.file_frame.open();            
        },
        removeFile: function(e) {
            e.preventDefault();
            this.url = '';
        },
        selectFile: function() {
            var attachment = this.file_frame.state().get('selection').first().toJSON();
            this.url = attachment.url;
        },
        setupFileFrame: function() {
            // Create the media frame.
            this.file_frame = wp.media.frames.file_frame = wp.media({
                title: 'Choose a file',
                button: {
                    text: 'Select',
                },
                multiple: false  // Set to true to allow multiple files to be selected
            });

            this.file_frame.on('select', this.selectFile);
        },
        generateUid: function() {
            var that = this;
            // save item
            this.$resource(window.ajaxurl).get(
                {
                    action: 'generate_file_uid'
                }, 
                function (data) {
                    that.uid = data;
                }
            );
        }
    },
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],36:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');
var sortableMixin = require('sortable_mixin');

if ($('.zippy-downloads').length > 0) {
    var ZippyCourses_DownloadsMetabox = new Vue({
        el: '.zippy-downloads',
        data: {
            entries: []
        },
        mixins: [savePostMetaMixin, sortableMixin],
        events: {
        },
        computed: {
            meta: function() {
                return {
                    downloads: this.entries
                }
            }
        },
        components: {
            download: require('./download')
        },
        created: function () {
            $(this.$el).css({'background':'yellow'});
            var data = JSON.parse($('#zippy-entry-downloads .zippy-mb-data').text());
            
            if (data.downloads !== undefined && Array.isArray(data.downloads)) {
                this.entries = data.downloads;
            }

        },
        compiled: function () {
            // $(this.$el).show();
            this.initSortable();
        },
        methods: {
            addDownload: function(e) {
                e.preventDefault();

                console.log("Wtf?");

                this.entries.push({});
            }
        }
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./download":35,"save_post_meta_mixin":93,"sortable_mixin":96}],37:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var wp = (typeof window !== "undefined" ? window['wp'] : typeof global !== "undefined" ? global['wp'] : null);

module.exports = {
    name: 'featured-image',
    props: ['image'],
    compiled: function() {
    },
    created: function() {
        // Create the media frame.
        if (wp.media == undefined || wp.media.frames == undefined) {
            return;
        }
        
        this.file_frame = wp.media.frames.file_frame = wp.media({
            title: 'Choose a file',
            button: {
                text: 'Select',
            },
            multiple: false  // Set to true to allow multiple files to be selected
        });

        this.file_frame.on('select', this.selectImage);
    },
    methods: {
        openFileFrame: function(e) {
            e.preventDefault();
            this.file_frame.open();            
        },
        removeFeaturedImage: function(e) {
            e.preventDefault();
            this.image = { ID: 0 };
        },
        selectImage: function() {
            var attachment = this.file_frame.state().get('selection').first().toJSON();
            this.image = {
                ID: attachment.id,
                url: attachment.url
            };
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],38:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'featured-video',
    filters: {
        stripslashes: require('stripslashes_filter')
    },
    props: ['video'],
    events: {
        fit_vid: 'fitVideo'
    },
    watch: {
        video: 'fitVideo',
        'validation.video.valid': 'fitVideo'
    },
    validator: {
        validates: {
            embeddable: function (val) {
                var regex = new RegExp("^<(iframe|object|embed)\\b[^>]*>(.*?)(</\\1>)?");
                return regex.test(val);
            }
        }
    },
    methods: {
        fitVideo: function() {
            $('.zippy-preview').fitVids({ customSelector: 'iframe, object, embed' });
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"stripslashes_filter":91}],39:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');

module.exports = {
    name: 'featured-media-metabox',
    template: $('#featured-media-tmpl').text(),
    mixins: [savePostMetaMixin],
    components: {
        'featured-image'  : require('./featured-image'),
        'featured-video'  : require('./featured-video'),
    },
    data: function() {
        return {
            loaded: false,
            meta_key: 'featured_media',
            featured_media: {
                type: 'image',
                image: {
                    ID: 0,
                    url: ''
                },
                video: ''
            },
        }
    },
    events: {

    },
    watch: {
        'featured_media.type' : {
            handler: function() {
                this.$broadcast('fit_vid');
            }
        }
    },
    computed: {
        meta: function() {
            var that = this,
                obj = {};
            
            var media = {
                type: this.featured_media.type,
                image: {
                    ID: this.featured_media.image.ID,
                    url: this.featured_media.image.url
                },
                video: this.addSlashes(this.featured_media.video)
            };

            obj[this.meta_key] = media;
            
            return obj;
        }
    },
    created: function() {
    },
    compiled: function() {
        this.setup();
    },
    methods: {
        setup: function() {
            var that = this,
                data = JSON.parse($(this.$el).parent().find('.zippy-mb-data').text()),
                isSetup = (data[this.meta_key].length === this.featured_media.length);
                
            this.featured_media = isSetup ? data[this.meta_key] : this.featured_media;
            if (this.featured_media.video !== undefined) {
                this.featured_media.video = this.stripSlashes(this.featured_media.video);
            }

            this.loaded = true;
        },
        addSlashes: function(str) {
            return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
        },
        stripSlashes: function(str) {
            return str.replace(new RegExp("\\\\", "g"), "");
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./featured-image":37,"./featured-video":38,"save_post_meta_mixin":93}],40:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'course',
    template: $('#zippy-forum-course-tmpl').text(),
    data: function() {
        return {
            tiers: []
        }
    },
    events: {
    },
    created: function() {
    },
    methods: {
    },
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],41:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');

if ($('#zippy-forum-access').length > 0) {
    var ZippyCourses_ForumAccess_Metabox = new Vue({
        el: '#zippy-forum-access .inside .vue',
        template: $('#zippy-forum-access-tmpl').text(),
        data: {
            enabled: 0,
            courses: []
        },
        mixins: [savePostMetaMixin],
        events: {
        },
        computed: {
            meta: function() {
                return {
                    zippy_forum_access_enabled: this.enabled,
                    zippy_forum_access: this.courses
                        .filter(function(course) {
                            return course.tiers.filter(function(tier) {
                                return tier.checked;
                            }).length > 0;
                        }).map(function(course) {
                            return {
                                id: course.id,
                                tiers: course.tiers.filter(function(tier) {
                                    return tier.checked;
                                }).map(function(tier) {
                                    return tier.id
                                })
                            };
                        })
                };
            }
        },
        components: {
            course: require('./course')
        },
        created: function () {
            this.setupData();
        },
        compiled: function () {},
        methods: {
            setupData: function() {
                var data = $('#zippy-forum-access .zippy-mb-data').text();
                var that = this;

                if (data != undefined) {
                    data = JSON.parse(data);

                    if (data.courses !== undefined && Array.isArray(data.courses)) {
                        var courses  = data.courses;

                        courses.forEach(function(course) {
                            course.tiers.forEach(function(tier) {
                                tier.checked = false;
                            });
                        });

                        this.courses = courses;
                    }

                    this.enabled = parseInt(data.zippy_forum_access_enabled);
                    console.log(this.enabled, data.zippy_forum_access_enabled);

                    if (data.zippy_forum_access !== undefined && typeof data.zippy_forum_access === 'object') {
                        data.zippy_forum_access.forEach(function(course) {
                            var record = _.findWhere(that.courses, {id: course.id});

                            if (record !== undefined) {
                                course.tiers.forEach(function(tier) {
                                    var tier_record = _.findWhere(record.tiers, {id: tier});

                                    if (tier_record !== undefined) {
                                        tier_record.checked = true;
                                    }
                                });
                            }
                        });
                    }                    
                }
            }
        }
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./course":40,"save_post_meta_mixin":93}],42:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
require('./downloads');
require('./dashboard-courses');
require('./student-activity');
require('./student-courses');
require('./course-activity');
require('./order-emails');
require('./order-ontraport-status');
require('./order-transactions');
require('./product/access');
require('./product/existing-access');
require('./product/scheduling-overrides');
require('./product/confirmation-messages');
require('./forum-access');
require('./landing-page');
require('./course-completion');

module.exports = {
    name: 'metaboxes',
    components: {
        featured_media      : require('./featured-media'),
    },
    created: function() {
        this.featuredMediaMetabox();
    },
    methods: {        
        featuredMediaMetabox: function() {
            if ($('#zippy-featured-media').length > 0) {
                this.$addChild({}, this.$options.components.featured_media).$mount('#zippy-featured-media .inside .vue');
            }

            if ($('#zippy-course-public-featured-media').length > 0) {
                this.$addChild(
                    {
                        data: {
                            meta_key: 'public_featured_media'
                        }
                    }, 
                    this.$options.components.featured_media
                ).$mount('#zippy-course-public-featured-media .inside .vue');                
            }
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./course-activity":30,"./course-completion":32,"./dashboard-courses":34,"./downloads":36,"./featured-media":39,"./forum-access":41,"./landing-page":43,"./order-emails":44,"./order-ontraport-status":45,"./order-transactions":46,"./product/access":47,"./product/confirmation-messages":48,"./product/existing-access":49,"./product/scheduling-overrides":52,"./student-activity":55,"./student-courses":60}],43:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');

if ($('#zippy-landing-page-details').length > 0) {
    var ZippyCourses_LandingPage_Metabox = new Vue({
        el: '#zippy-landing-page-details .inside .vue',
        name: 'landing-page-metabox',
        template: $('#mb-landing-page-tmpl').text(),
        mixins: [savePostMetaMixin],
        data: function() {
            return {
                type: 'product',
                types: [
                    {text: 'Product', value: 'product'},
                    {text: 'Email List', value: 'list'}
                ],
                thank_you_url: '',
                service: '',
                services: [],
                list: [],
                lists: [],
                product: '',
                products: [],
                visitor: {
                    headline: '',
                    button: '',
                },
                student: {
                    headline: '',
                    button: '',
                },
                disclaimer: '',
                loaded: false
            }
        },
        watch: {
            meta: {
                handler: 'savePostMeta',
                deep: true
            }
        },
        computed: {
            service_lists: function() {
                return this.lists[this.service];
            },
            meta: function() {
                return {
                    landing_page: {
                        type: this.type,
                        visitor: this.visitor,
                        student: this.student,
                        product: this.product,
                        list: this.list,
                        service: this.service,
                        thank_you_url: this.thank_you_url,
                        disclaimer: this.disclaimer,
                    }
                }
            }
        },
        created: function() {
            var services = JSON.parse($('#zippy-email-list-services-data').text());
            
            this.services   = services.services;
            this.lists      = services.lists;
            this.products   = services.products;

            var data = $('#zippy-landing-page-details .inside .zippy-mb-data').text();
            if (data.length > 0) {
                data = JSON.parse(data);
            }


            if (typeof data.landing_page === "object") {

                if (data.landing_page.list !== undefined) {
                    this.list = data.landing_page.list;
                }

                if (data.landing_page.service !== undefined) {
                    this.service = data.landing_page.service;
                }

                if (data.landing_page.product !== undefined) {
                    this.product = data.landing_page.product;
                }

                if (data.landing_page.type !== undefined) {
                    this.type = data.landing_page.type;
                }

                if (data.landing_page.visitor !== undefined) {
                    this.visitor = data.landing_page.visitor;
                }

                if (data.landing_page.student !== undefined) {
                    this.student = data.landing_page.student;
                }

                if (data.landing_page.disclaimer !== undefined) {
                    this.disclaimer = data.landing_page.disclaimer;
                }

                if (data.landing_page.thank_you_url !== undefined) {
                    this.thank_you_url = data.landing_page.thank_you_url;
                }
            }

            if ($('#page_template').length > 0) {
                $('#page_template').on('change', this.toggleLandingPage);    
                this.toggleLandingPage();
            }
        },
        compiled: function() {
        },
        methods: {
            toggleLandingPage: function() {
                var template = $('#page_template').val();

                if (template == 'templates/landing-page.php') {
                    $('#zippy-landing-page-details').show();
                } else {
                    $('#zippy-landing-page-details').hide();
                }
            }
        }
    });
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"save_post_meta_mixin":93}],44:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('#zippy-order-emails').length > 0) {
    var ZippyCourses_OrderEmails_Metabox = new Vue({
        el: '#zippy-order-emails .inside',
        data: function() {
            return {
                messages: []
            }
        },
        components: {
            notice: require('notice')
        },
        events: {
            show_message: 'showNotice',
        },
        methods: {
            showNotice: function(receiver, notice) {
                this.messages.push({ content: notice });
            },
            resend: function(e) {
                e.preventDefault();
                
                var that = this;

                this.$resource(ajaxurl).save(
                    { action: 'resend_registration_email' }, 
                    {
                        order_id: $('#post_ID').val()
                    }, 
                    function (data, status, request) {
                        that.$emit('show_message', 'test', window.ZippyCourses.strings.resend_registration_reminder_email);
                    }
                ).error(
                    function (data, status, request) {}
                );
            }
        }
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"notice":79}],45:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('#zippy-order-ontraport-status').length > 0) {

    var gateway = $('.zippy-order-details-table tbody tr:first-of-type td:nth-child(3n)').text().trim();
    if (gateway != 'Ontraport') {
        $('#zippy-order-ontraport-status').hide();
        return;
    } else {
        $('#zippy-order-ontraport-status').show();
    }

    var ZippyCourses_OrderOntraportStatus_Metabox = new Vue({
        el: '#zippy-order-ontraport-status .inside',
        data: function() {
            return {
                messages: []
            }
        },
        components: {
            notice: require('notice')
        },
        events: {
            show_message: 'showNotice',
        },
        methods: {
            showNotice: function(receiver, notice) {
                this.messages.push({ content: notice });
            },
            resend: function(e) {
                e.preventDefault();
                
                var that = this;

                this.$resource(ajaxurl).save(
                    { action: 'clear_ontraport_status' }, 
                    {
                        order_id: $('#post_ID').val()
                    }, 
                    function (data, status, request) {
                        that.$emit('show_message', 'test', window.ZippyCourses.strings.clear_ontraport_status);
                    }
                ).error(
                    function (data, status, request) {}
                );
            }
        }
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"notice":79}],46:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('#zippy-order-transaction').length > 0) {
    var ZippyCourses_OrderTransactions_Metabox = new Vue({
        el: '#zippy-order-transaction',
        data: function() {
            return {
                transaction: [],
                temp_transaction_total: 0,
                ux: {
                    edit_transaction_amount: false
                }
            }
        },
        components: {

        },
        
        events: {
        },
        
        created: function() {
            this.setupData();
        },

        methods: {
            setupData: function() {
                var data = JSON.parse($('.zippy-mb-data').text());                
                
                if (data.transaction !== undefined) {
                    this.transaction = data.transaction;
                }
            },
            toggleEditTransactionAmount: function(e) {
                if (e !== undefined) {
                    e.preventDefault();    
                }            
                this.temp_transaction_total = this.transaction.total;
                this.ux.edit_transaction_amount = !this.ux.edit_transaction_amount;
            },
            cancelEditTransactionAmount: function(e) {
                e.preventDefault();
                this.transaction.total = this.temp_transaction_total;
                this.ux.edit_transaction_amount = false;
            },
            saveTransactionAmount: function() {
                // If a non-numeric value is entered, don't save it
                if (isNaN(this.transaction.total) || (this.transaction.total === '')) {
                    this.transaction.total = this.temp_transaction_total;
                } else {
                    this.$resource(ajaxurl).save(
                        { action: 'update_transaction_amount' }, 
                        {
                            transaction_id: this.transaction.id,
                            transaction_amount: this.transaction.total
                        }, 
                        function (data, status, request) {}
                        ).error(
                        function (data, status, request) {}
                        );
                }
                        this.toggleEditTransactionAmount();
            }
        }
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],47:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');

if ($('#zippy-product-access').length > 0) {
    var ZippyCourses_ProductAccess_Metabox = new Vue({
        name: 'product-access',
        el: function() { return '#zippy-product-access .inside .vue'; },
        template: $('#zippy-product-course-tmpl').text(),
        mixins: [savePostMetaMixin],
        props: ['course', 'tier'],
        data: function() {
            return {
                enable_bundle: true,
                courses: [],
                course: 0,
                tier: 0,
                bundles: []
            }
        },
        events: {
            'hook:created' : 'setup'
        },
        computed: {
            meta: function() {
                return {
                    access: {
                        mode: (this.enable_bundle ? 'bundle' : 'product'),
                        course: this.course,
                        tier: this.tier,
                        bundle: this.bundles.map(function(bundle) {
                            return {
                                id: bundle.id,
                                tiers: bundle.tiers
                                    .filter(function(tier) {
                                        return tier.checked;
                                    }).
                                    map(function(tier) {
                                        return tier.id
                                    })
                            };
                        })                    
                    }
                };
            },
            tiers_list: function() {
                var record = _.findWhere(this.courses, {id: this.course});

                if (record === undefined) {
                    return [];
                } else {
                    var tiers = record.tiers.map(function(tier) {
                        return {text: tier.title, value: tier.id};
                    });

                    return tiers;
                }
            },
            courses_list: function() {
                var courses = this.courses.map(function(course) {
                    return { value: course.id, text: course.title }
                });

                return courses;
            }
        },
        methods: {
            setup: function() {
                var courses = $('#zippy-product-courses-data').text();
                if (courses.length > 0) {
                    courses = JSON.parse(courses);
                } else {
                    courses = [];
                }

                var data = $('#zippy-product-access .zippy-mb-data').text();
                if (data.length > 0) {
                    data = JSON.parse(data);
                } else {
                    data = {};
                }

                var access = data.access !== undefined ? data.access : {};

                this.enable_bundle = (access.mode == 'bundle');
                this.course = access.course !== undefined ? access.course : 0;
                this.tier = access.tier !== undefined ? access.tier + '' : 0;


                if (this.course === 0 && Array.isArray(courses) && courses[0].id !== undefined) {
                    this.course = courses[0].id;

                    if (this.tier === 0 && Array.isArray(courses[0].tiers) && courses[0].tiers[0].id !== undefined) {
                        this.tier = courses[0].tiers[0].id;
                    }
                }

                if (Array.isArray(courses)) {
                    this.courses = courses;
                    this.setupBundles(courses);                
                }
            },
            setupBundles: function(courses) {
                var that = this;
                var data = JSON.parse($('#zippy-product-access .zippy-mb-data').text());

                var bundles = [];

                courses.forEach(function(course) {
                    var bundle = {
                        id: course.id,
                        title: course.title,
                        tiers: []
                    };

                    course.tiers.forEach(function(tier) {
                        bundle.tiers.push({
                            id: tier.id,
                            title: tier.title,
                            checked: false
                        });
                    });

                    bundles.push(bundle);
                });

                if (data.access !== undefined && Array.isArray(data.access.bundle)) {
                    data.access.bundle.forEach(function(course) {
                        var c = _.findWhere(bundles, {id: course.id});

                        if (c !== undefined) {
                            course.tiers.forEach(function(tier) {
                                var t = _.findWhere(c.tiers, {id: tier});

                                if (t !== undefined) {
                                    t.checked = true;
                                }
                            });
                        }
                    });
                }

                that.bundles = bundles;
            }
        }
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"save_post_meta_mixin":93}],48:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');



if ($('#zippy-product-confirmation-messages').length > 0) {
    var ZippyCourses_ProductConfirmationMessages_Metabox = new Vue({
        name: 'product-confirmation-messages',
        el: '#zippy-product-confirmation-messages .inside .vue',
        template: $('#zippy-product-confirmation-message-templ').text(),
        mixins: [savePostMetaMixin],
        data: function() {
            return {
                custom_thank_you_enable : false,
                custom_thank_you_text: '',

                custom_purchase_receipt_enable: false,
                custom_purchase_receipt_subject: '',
                custom_purchase_receipt_text: '',

                custom_registration_reminder_enable: false,
                custom_registration_reminder_text: '',
                custom_registration_reminder_subject: '',
            }
        },
        events: {
            'hook:created' : 'setup'
        },
        computed: {
            meta: function() {
                return {
                    confirmation_messages: {
                        custom_thank_you_enable: this.custom_thank_you_enable,
                        custom_thank_you_text: this.custom_thank_you_text,

                        custom_purchase_receipt_enable: this.custom_purchase_receipt_enable,
                        custom_purchase_receipt_text: this.custom_purchase_receipt_text,
                        custom_purchase_receipt_subject: this.custom_purchase_receipt_subject,

                        custom_registration_reminder_enable: this.custom_registration_reminder_enable,
                        custom_registration_reminder_text: this.custom_registration_reminder_text,  
                        custom_registration_reminder_subject: this.custom_registration_reminder_subject,                    
                    }
                }
            }
        },
        methods: {
            setup: function() {
                var data = $('#zippy-product-confirmation-messages .zippy-mb-data').text();
                if (data.length > 0) {
                    data = JSON.parse(data);
                    data = data.confirmation_messages;
                } else {
                    data = {};
                }
                var zippy_default_thank_you_message = document.getElementById('zippy_default_thank_you_message').value;
                var zippy_default_purchase_receipt_message = document.getElementById('zippy_default_purchase_receipt_message').value;
                var zippy_default_registration_reminder_message = document.getElementById('zippy_default_registration_reminder_message').value;
                var zippy_default_purchase_receipt_subject = document.getElementById('zippy_default_purchase_receipt_subject').value;
                var zippy_default_registration_reminder_subject = document.getElementById('zippy_default_registration_reminder_subject').value;


                this.custom_thank_you_enable = data.custom_thank_you_enable !== undefined ? data.custom_thank_you_enable : false;
                this.custom_thank_you_text = data.custom_thank_you_text !== undefined ? this.setMessage(data.custom_thank_you_text, zippy_default_thank_you_message) : zippy_default_thank_you_message;

                this.custom_purchase_receipt_enable = data.custom_purchase_receipt_enable !== undefined ? data.custom_purchase_receipt_enable : false;
                this.custom_purchase_receipt_text = data.custom_purchase_receipt_text !== undefined ? this.setMessage(data.custom_purchase_receipt_text, zippy_default_purchase_receipt_message) : zippy_default_purchase_receipt_message;
                this.custom_purchase_receipt_subject = data.custom_purchase_receipt_subject !== undefined ? this.setMessage(data.custom_purchase_receipt_subject, zippy_default_purchase_receipt_subject) : zippy_default_purchase_receipt_subject;


                this.custom_registration_reminder_enable = data.custom_registration_reminder_enable !== undefined ? data.custom_registration_reminder_enable : false;
                this.custom_registration_reminder_text = data.custom_registration_reminder_text !== undefined ? this.setMessage(data.custom_registration_reminder_text, zippy_default_registration_reminder_message) : zippy_default_registration_reminder_message;
                this.custom_registration_reminder_subject = data.custom_registration_reminder_subject !== undefined ? this.setMessage(data.custom_registration_reminder_subject, zippy_default_registration_reminder_subject) : zippy_default_registration_reminder_subject;
             
            },
            setMessage: function(custom_value, default_value) {
                if (custom_value === '') {
                    return default_value;
                }
                return custom_value;
            }
        }
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"save_post_meta_mixin":93}],49:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');

if ($('#zippy-product-existing-access').length > 0) {
    var ZippyCourses_ProductExistingAccessRequirement_Metabox = new Vue({
        name: 'product-existing-access',
        el: function() { return '#zippy-product-existing-access .inside .vue'; },
        template: $('#zippy-product-existing-access-templ').text(),
        mixins: [savePostMetaMixin],
        props: ['course', 'tier'],
        components: {
            'remove-course': require('./remove-course'),
            'require-course': require('./require-course')
        },
        data: function() {
            return {
                enabled: false,
                revokeAccess: false,
                requiredAccess: false,
                requiredCourses: [],
                revokedCourses: []
            }
        },
        events: {
            'hook:created' : 'setup',
        },
        computed: {
            meta: function() {
                return {
                    product_existing_access: {
                        enabled: this.enabled,
                        revokeAccess: this.revokeAccess,
                        requiredAccess: this.requiredAccess,
                        requiredCourses: this.requiredCourses,
                        revokedCourses: this.revokedCourses
                    }
                };
            },
        },
        methods: {
            setup: function() {
                var data = $('#zippy-product-existing-access .zippy-mb-data').text();
            
                if (data.length > 0) {
                    data = JSON.parse(data);
                    data = data.product_existing_access;
                } else {
                    data = {};
                }

                this.enabled = data.enabled !== undefined ? data.enabled : false;
                this.revokeAccess = data.revokeAccess !== undefined ? data.revokeAccess : false;
                this.requiredAccess = data.requiredAccess !== undefined ? data.requiredAccess : false;
                this.requiredCourses = data.requiredCourses !== undefined ? data.requiredCourses : [];
                this.revokedCourses = data.revokedCourses !== undefined ? data.revokedCourses :[];

            }
        },

        created: function() {
            this.$on('update-remove-course', function(removedCourse) {
                this.revokedCourses = removedCourse;
            });
            this.$on('update-require-course', function(requiredCourse) {
                this.requiredCourses = requiredCourse;
            });
        }
    });
}   

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./remove-course":50,"./require-course":51,"save_post_meta_mixin":93}],50:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

var courseAccessMixin = require('course_access_mixin');


module.exports =  {
    name: 'remove-course',
    template: $('#zippy-courses-existing-access-courses-templ').text(),
    props: ['course', 'tier'],
    mixins: [courseAccessMixin],
    events: {
        'hook:created' : 'setup'
    },
    methods: {
        setup: function() {
            var courses = $('#zippy-product-existing-access-courses-data').text();
            if (courses.length > 0) {
                courses = JSON.parse(courses);
            } else {
                courses = [];
            }

            var data = $('#zippy-product-existing-access .zippy-mb-data').text();
            if (data.length > 0) {
                data = JSON.parse(data);
                data = data.product_existing_access;
            } else {
                data = {};
            }

            var revokedCourses = data.revokedCourses !== undefined ? data.revokedCourses : {};

            this.course = revokedCourses.course !== undefined ? revokedCourses.course : 0;
            this.tier = revokedCourses.tier !== undefined ? revokedCourses.tier + '' : 0;

            if (this.course === 0 && Array.isArray(courses) && courses[0].id !== undefined) {
                this.course = courses[0].id;

                if (this.tier === 0 && Array.isArray(courses[0].tiers) && courses[0].tiers[0].id !== undefined) {
                    this.tier = courses[0].tiers[0].id;
                }
            }

            if (Array.isArray(courses)) {
                this.courses = courses;
            }
            
            this.updateParent();
        },
        updateParent: function() {
            var courseData = {
                    course : this.course,
                    tier : this.tier
                };
            this.$dispatch('update-remove-course', courseData);
        }        
    }
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"course_access_mixin":92}],51:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

var courseAccessMixin = require('course_access_mixin');


module.exports =  {
    name: 'require-course',
    template: $('#zippy-courses-existing-access-courses-templ').text(),
    props: ['course', 'tier'],
    mixins: [courseAccessMixin],
    events: {
        'hook:created' : 'setup'
    },
    methods: {
        setup: function() {
            var courses = $('#zippy-product-existing-access-courses-data').text();
            if (courses.length > 0) {
                courses = JSON.parse(courses);
            } else {
                courses = [];
            }

            var data = $('#zippy-product-existing-access .zippy-mb-data').text();
            if (data.length > 0) {
                data = JSON.parse(data);
                data = data.product_existing_access;
            } else {
                data = {};
            }

            var requiredCourses = data.requiredCourses !== undefined ? data.requiredCourses : {};

            this.course = requiredCourses.course !== undefined ? requiredCourses.course : 0;
            this.tier = requiredCourses.tier !== undefined ? requiredCourses.tier + '' : 0;

            if (this.course === 0 && Array.isArray(courses) && courses[0].id !== undefined) {
                this.course = courses[0].id;

                if (this.tier === 0 && Array.isArray(courses[0].tiers) && courses[0].tiers[0].id !== undefined) {
                    this.tier = courses[0].tiers[0].id;
                }
            }

            if (Array.isArray(courses)) {
                this.courses = courses;
            }
            this.updateParent();
        },
        updateParent: function() {
            var courseData = {
                    course : this.course,
                    tier : this.tier
                };
            this.$dispatch('update-require-course', courseData);
        }        
    }
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"course_access_mixin":92}],52:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('#zippy-product-scheduling-override').length > 0) {
    var ZippyCourses_ProductSchedulingOverrides_Metabox = new Vue({
        name: 'product-scheduling-overrides',
        el: '#zippy-product-scheduling-override .inside',
        data: function() {
        },
        events: {
            'hook:created' : 'setup'
        },
        methods: {
            setup: function() {
                $('#zippy-product-scheduling-override input[name="overrides_enabled"]').on('change', this.toggleSettingsVis);
                this.toggleSettingsVis();
            },
            toggleSettingsVis: function() {
                if ($('#zippy-product-scheduling-override input[name="overrides_enabled"]').is(':checked')) {
                    $('.zippy-product-scheduling-overrides-table').show();
                } else {
                    $('.zippy-product-scheduling-overrides-table').hide();
                }
            }
        }
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],53:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'student-course-completion-table',
    data: function() {
        return {
            completion_data: [],
            reverse: false,
            sortKey: ''
        }
    },
    created: function() {
        var completion_data = [];
        var data = JSON.parse($('#zippy-student-activity .inside .zippy-mb-data').text());
        this.completion_data    = (Array.isArray(data.completion_data)) ? data.completion_data : completion_data;
    },
    computed: {
        views: function() {
            if (this.mode === 'recent') {
                return this.all_views;
            } else {
                return this.recent_views;
            }
        }
    },
    methods: {
        sortBy: function(sortKey) {
            this.reverse = (this.sortKey == sortKey) ? !this.reverse : false;
            this.sortKey = sortKey;
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],54:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'student-downloads-table',
    data: function() {
        return {
            downloads: [],
            reverse: false,
            sortKey: ''
        }
    },
    created: function() {
        var downloads = [];
        var that = this;
        var data = JSON.parse($('#zippy-student-activity .inside .zippy-mb-data').text());

        this.downloads    = (Array.isArray(data.downloads)) ? data.downloads : downloads;
    },
    computed: {
        views: function() {
            if (this.mode === 'recent') {
                return this.all_views;
            } else {
                return this.recent_views;
            }
        }
    },
    methods: {
        sortBy: function(sortKey) {
            this.reverse = (this.sortKey == sortKey) ? !this.reverse : false;
            this.sortKey = sortKey;
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],55:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('.zippy-student-activity').length > 0) {
    var ZippyCourses_StudentActivity_Metabox = new Vue({
        el: '.zippy-student-activity',
        events: {
        },
        components: {
            studentActivityTable: require('./views-table'),
            studentDownloadsTable: require('./downloads-table'),
            studentLessonCompletionTable: require('./lesson-completion-table'),
            studentCompletionCertificateTable: require('./completion-certificate-table'),
        }
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./completion-certificate-table":53,"./downloads-table":54,"./lesson-completion-table":56,"./views-table":57}],56:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'student-lesson-completion-table',
    data: function() {
        return {
            lessons_completed: [],
            active_course: []
        }
    },
    created: function() {
        var lessons_completed = [];
        var that = this;
        var data = JSON.parse($('#zippy-student-activity .inside .zippy-mb-data').text());
        this.lessons_completed    = (Array.isArray(data.lessons_completed)) ? data.lessons_completed : lessons_completed;
        this.active_course = _.first(this.lessons_completed);
    },
    computed: {
        activeCourseHasLessons: function() {
            if (this.active_course.lessons.length > 0) {
                return true;
            }
            return false;
        },

        studentHasCompletedLessons: function() {
        if (this.lessons_completed.length > 0) {
                return true;
            }
            return false;          
        }
    },
    methods: {
        toggleCourse: function(id, e) {
            e.preventDefault();
            var that = this;
            this.active_course = _.find(that.lessons_completed, function(obj) { return obj.ID == id })
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],57:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'student-activity-table',
    data: function() {
        return {
            all_views: [],
            recent_views: [],
            reverse: false,
            sortKey: '',
            mode: 'recent'
        }
    },
    created: function() {
        var recent_views = [];
        var all_views = [];
        var that = this;
        var data = JSON.parse($('#zippy-student-activity .inside .zippy-mb-data').text());

        this.all_views    = (Array.isArray(data.all_views)) ? data.all_views : all_views;
        this.recent_views = (Array.isArray(data.recent_views)) ? data.recent_views : recent_views;
    },
    computed: {
        views: function() {
            if (this.mode === 'recent') {
                return this.all_views;
            } else {
                return this.recent_views;
            }
        }
    },
    methods: {
        toggleTimespan: function(e) {
            e.preventDefault();
            var mode = (this.mode == 'recent') ? 'all' : 'recent';
            this.mode = mode;
        },
        sortBy: function(sortKey) {
            this.reverse = (this.sortKey == sortKey) ? !this.reverse : false;
            this.sortKey = sortKey;
        },
        getHumanReadableTime: function(ms)
        {
            var output = '',
                x,
                seconds,
                minutes,
                hours,
                days;

            x = ms / 1000;
            seconds = Math.ceil(x % 60);

            x /= 60;
            minutes = Math.floor(x % 60);

            x /= 60;
            hours = Math.floor(x % 24);

            x /= 24;
            days = Math.floor(x);

            if (days > 0) {
                output += ' ' + days + ' d';
            }

            if (hours > 0) {
                output += ' ' + hours + ' hr';
            }

            if (minutes > 0) {
                output += ' ' + minutes + ' min';
            }

            if (seconds > 0) {
                output += ' ' + seconds + ' sec';
            }
            
            if (output.trim().length == 0) {
                output = '0 sec';
            }
            
            return output.trim();
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],58:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'course',
    data: function() {
        return {
            id: 0,
            title: '',
            tiers: [],
            start_date: '',
            temp_start_date: '',
            progress: 0,
            ux: {
                edit_start_date: false
            }
        }
    },
    components: {
        tier: require('./tier')
    },
    computed: {
        edit_url: function() {
            return window.ZippyCourses.admin_url + 'post.php?post=' + this.id.toString() + '&action=edit';
        }
    },
    methods: {
        toggleEditStartDate: function(e) {
            if (e !== undefined) {
                e.preventDefault();    
            }            

            this.temp_start_date = this.start_date;
            this.ux.edit_start_date = !this.ux.edit_start_date;
        },
        cancelEditStartDate: function(e) {
            e.preventDefault();
            
            this.start_date = this.temp_start_date;
            this.ux.edit_start_date = false;
        },
        saveStartDate: function() {
            // save item
            this.$resource(ajaxurl).save(
                { action: 'save_student_course_start_date' }, 
                {
                    user_id: $('#studentID').val(),
                    course_id: this.id,
                    start_date: this.start_date
                }, 
                function (data, status, request) {}
            ).error(
                function (data, status, request) {}
            );

            this.toggleEditStartDate();
        }
    },
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./tier":59}],59:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'tier',
    props: ['course'],
    data: function() {
        return {
            id: 0,
            title: '',
            access: true
        }
    },
    methods: {
        restoreTierAccess: function(e) {
            e.preventDefault();
            var that = this;
            
            this.$resource(ajaxurl).save(
                {action: 'restore_student_tier_access'}, 
                {
                    user_id: $('#studentID').val(),
                    course_id: this.course,
                    tier_id: this.id
                }, 
                function (data, status, request) {
                    that.status = true;
                }
            ).error(
                function (data, status, request) {}
            );
        },
        removeTierAccess: function(e) {
            e.preventDefault();
            var that = this;

            this.$resource(ajaxurl).save(
                {action: 'remove_student_tier_access'}, 
                {
                    user_id: $('#studentID').val(),
                    course_id: this.course,
                    tier_id: this.id
                }, 
                function (data, status, request) {
                    that.status = false
                }
            ).error(
                function (data, status, request) {}
            );
        },
    },
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],60:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
require('datepicker_directive');

if ($('.zippy-student-courses').length > 0) {
    var ZippyCourses_StudentCourses_Metabox = new Vue({
        el: '.zippy-student-courses',
        data: {
            courses: []
        },
        events: {
            'hook:created': 'setupData'
        },
        components: {
            course: require('./course')
        },
        methods: {
            setupData: function() {
                var data = JSON.parse($('#zippy-student-courses .zippy-mb-data').text());                

                if (data.courses !== undefined && Array.isArray(data.courses)) {
                    this.courses = data.courses;
                }
            }
        }
    });
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./course":58,"datepicker_directive":90}],61:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('.zippy-admin-notice').length > 0) {
    var ZippyCourses_AdminNotice = new Vue({
        name: 'zippy-admin-notice',
        created: function() {
            $('.dismiss-zippy-news-item').on('click', this.dismissNews);
        },
        methods: {
            dismissNews: function(e) {
                e.preventDefault();
                
                var that = this;
                var url = $(e.currentTarget).attr('href');
                var notice = $(e.currentTarget).parents('.zippy-admin-notice');

                $(notice).fadeTo( 100 , 0, function() {
                    $(notice).slideUp( 100, function() {
                        $(notice).remove();
                    });
                });

                this.$resource(window.ajaxurl).save(
                    { action: 'zippy_dismiss_news_item' }, 
                    { url: url },
                    function (items, status, request) {

                    }
                );
            }
        }
    });
}


}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],62:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');

module.exports = {
    name: 'product-email-lists',
    components: {
        service: require('./service')
    },
    data: function() {
        return {
            services : [],
        }
    },
    created: function() {
        this.buildServices();
    },
    methods: {
        /**
         * Fetch the metadata from $wpdb->postmeta for this product
         * @return void
         */
        buildServices: function() {
            var that = this;

            this.services.forEach(function(service) {
                var el = $('.vue[email-integration="' + service.id + '"]').first();

                if (el.length > 0) {
                    that.$addChild(
                        { 
                            el: el[0],
                            data: service
                        },
                        that.$options.components.service
                    ).$mount();
                }
            });
            
            this.$dispatch('child_loaded', 'email_lists');
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./service":63,"save_post_meta_mixin":93}],63:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'email-list-service',
    template: $('#email-list-integration-tmpl').text(),
    data: function() {
        return {
            id: '',
            fetched: false,
            service: '',
            per_tier: false,
            course: '0',
            lists: [],
            tiers: [],
        }
    },
    compiled: function() {
        var data = $(this.$el).siblings('.zippy-mb-data').text();

        if (data !== undefined && data !== '') {
            var key = this.id + '_lists';

            if (data.length > 0) {
                data = JSON.parse(data);
            }

            if (data[key] !== undefined) {
                this.lists = data[key];
            }

            if (data.email_lists !== undefined) {
                var record = _.findWhere(data.email_lists, {service: this.id});

                if (record !== undefined) {
                    this.course = record.list;
                }
            }
        }
    },
    methods: {}
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],64:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');

module.exports = {
    name: 'product',
    components: {
        pricing             : require('./pricing'),
        'launch-windows'    : require('./launch-windows'),
        email_lists         : require('./email_lists'),
    },
    mixins: [savePostMetaMixin],
    data: function() {
        return {
            children_loaded: [
                { name: 'email_lists', loaded: false },
                { name: 'launch_windows', loaded: false },
            ],
            pricing: {
                type: 'free',
                price: {
                    free: {
                        amount: '0.00'
                    },
                    single: {
                        amount: '0.00'
                    },
                    subscription: {
                        frequency: 'month',
                        amount: '0.00'
                    },
                    payment_plan: {
                        frequency: 'month',
                        num_payments: '0',
                        amount: '0.00'
                    }                    
                }
            },
            launch_windows: {
                launch_windows: [],
                enabled: false
            },
            email_lists: []
        }
    },
    events: {
        child_loaded: 'childLoaded'
    },
    computed: {
        loaded: function() {
            return this.children_loaded.filter(function(child) {
                return !child.loaded;
            }).length < 1;
        },
        meta: function() {
            var that = this
            return {
                pricing: {
                    type: this.pricing.type,
                    price: this.pricing.price
                },
                email_lists : this.email_lists_simplified,
                launch_windows: this.launch_windows
            }
        },
        email_lists_simplified: function() {
            return this.email_lists.map(function(service) {
                return { service: service.id, list: service.list };
            });
        }
    },
    created: function() {
        this.setupPricing();
        this.setupLaunchWindows();
    },
    compiled: function() {
        this.setupEmailLists();
    },
    methods: {        
        setupLaunchWindows: function() {
            var data = $('#zippy-course-product-launch-windows .inside .zippy-mb-data').first().text();
            if (data.length > 0) {
                data = JSON.parse(data);
            }

            this.launch_windows = typeof(data) == 'object' && typeof(data.launch_windows) == 'object' && Object.keys(data.launch_windows).length == 2 
                ? data.launch_windows 
                : { launch_windows: [], enabled: false };

            this.$addChild({
                data: this.launch_windows
            }, this.$options.components['launch-windows']);        
        },
        setupPricing: function() {
            var data = $('#zippy-product-pricing .inside .zippy-mb-data').first().text();
            if (data.length > 0) {
                data = JSON.parse(data);
            }

            this.pricing.type = data.pricing.type !== undefined ? data.pricing.type : 'free';
            this.pricing.price = data.pricing.price !== undefined ? data.pricing.price : this.pricing.price;

            this.$addChild({ data: this.pricing }, this.$options.components['pricing']);
        },
        setupEmailLists: function() {
            var that = this;

            if ($('.vue[email-integration]').length > 0) {
                var data = $('.vue[email-integration]').first().siblings('.zippy-mb-data').text();

                if (data !== undefined) {
                    data = JSON.parse(data);

                    if (data.email_lists !== undefined && Array.isArray(data.email_lists)) {
                        data.email_lists.forEach(function(service) {
                            var my_service = {
                                id: service.service,
                                service: service.service,
                                list: service.list,
                            };
                            
                            that.email_lists.push(my_service);
                        });
                    }
                }
            }

            // Set up any items not already in the list 
            $('.vue[email-integration]').each(function(index, element) {
                var service = $(element).attr('email-integration');
                var record  = _.findWhere(that.email_lists, {id: service});

                if (record === undefined) {
                    that.email_lists.push({
                        id: service,
                        service: service,
                        list: 0
                    });
                }
            });

            this.$addChild(
                    { data: { services: this.email_lists }},
                    this.$options.components.email_lists
                )
                .$mount()
                .$after('#poststuff');            

            if( $('.vue[email-integration]').length < 1 ) {
                this.childLoaded('email_lists');
                return;
            }
        },
        childLoaded: function(name) {
            var record = _.findWhere(this.children_loaded, {name: name});

            if (record !== undefined) {
                record.loaded = true;
            }
        },
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./email_lists":62,"./launch-windows":65,"./pricing":67,"save_post_meta_mixin":93}],65:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'launch-windows',
    el: function() { return '#zippy-course-product-launch-windows .inside .vue'; },
    template: $('#mb-launch-windows-tmpl').text(),
    components: {
        'launch-window'   : require('./launch-window'),
    },
    data: function() {
        return {
            launch_windows: [],
            enabled: false
        }
    },
    compiled: function() {
        this.$dispatch('child_loaded', 'launch_windows');
    },
    methods: {        
        addLaunchWindow: function(e) {
            e.preventDefault();
            this.launch_windows.push({});
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./launch-window":66}],66:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

module.exports = {
    name: 'launch-window',
    data: function() {
        return {
            open_date: '',
            close_date: '',
            start_date: '',
            expiration_date: '',
        }
    },
    compiled: function() {
        
    },
    created: function() {
    },
    methods: {
        destroy: function() {
            this.$parent.launch_windows.$remove(this.$index);
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],67:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'pricing',
    el: function() { return '#zippy-product-pricing .inside .vue'; },
    template: require('./template.html'),
    props: ['type', 'price'],
    data: function() {
        return {
            types: [
                {text: 'Free', value: 'free'},
                {text: 'Single Payment', value: 'single'},
                {text: 'Subscription', value: 'subscription'},
                {text: 'Payment Plan', value: 'payment-plan'}
            ],
            frequencies: [
                {text: 'Yearly', value: 'year'},
                {text: 'Monthly', value: 'month'},
                {text: 'Weekly', value: 'week'},
                {text: 'Daily', value: 'day'},
            ]
        }
    },
    computed: {
    },
    components: {
        'payment-single': require('./single'),
        'payment-subscription': require('./subscription'),
        'payment-plan': require('./plan'),
    },
    created: function() {
    },
    compiled: function() {
        this.single_payment     = this.$addChild({}, this.$options.components.payment_type);
        this.subscription       = this.$addChild({}, this.$options.components.subscription);
        this.payment_plan       = this.$addChild({}, this.$options.components.payment_plan);
    },
    methods: {
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./plan":68,"./single":70,"./subscription":72,"./template.html":74}],68:[function(require,module,exports){
module.exports = {
    name: 'payment-plan',
    template: require('./template.html'),
    props: ['amount', 'frequency', 'frequencies', 'num-payments'],
    computed: {
        plan_total: function() {
            return parseFloat(this.amount * this.numPayments).toFixed(2);;
        }
    }
}

},{"./template.html":69}],69:[function(require,module,exports){
module.exports = '<p><label>Price: <input type="number" v-model="amount" step="0.01" min="0.01" /></label></p>\n<p><label>Frequency: <select v-model="frequency" options="$parent.frequencies"></select></label></p>\n<p><label># of Payments: <input type="number" v-model="numPayments" step="1" min="1" /></label></p>\n<p>Total: ${{ plan_total }}</p>';
},{}],70:[function(require,module,exports){
module.exports = {
    name: 'single-payment',
    template: require('./template.html'),
    props: ['amount']
}

},{"./template.html":71}],71:[function(require,module,exports){
module.exports = '<p><label>Price: <input type="number" v-model="amount" step="0.01" min="0.01" lazy /></label></p>';
},{}],72:[function(require,module,exports){
module.exports = {
    name: 'subscription',
    template: require('./template.html'),
    props: ['amount', 'frequency', 'frequencies']
}

},{"./template.html":73}],73:[function(require,module,exports){
module.exports = '<p><label>Price: <input type="number" v-model="amount" step="0.01" min="0.01" lazy /></label></p>\n<p><label>Frequency: <select v-model="frequency" options="frequencies"></select></label></p>';
},{}],74:[function(require,module,exports){
module.exports = '<select class="zippy-pricing-type" v-model="type" options="types"></select>\n\n<payment-single\n    amount="{{@ price.single.amount }}"\n    v-if="type == \'single\'"></payment-single>\n\n<payment-subscription\n    amount="{{@ price.subscription.amount }}"\n    frequency="{{@ price.subscription.frequency }}"\n    frequencies="{{ frequencies }}"\n    v-if="type == \'subscription\'"></payment-subscription>\n\n<payment-plan\n    amount="{{@ price.payment_plan.amount }}"\n    frequency="{{@ price.payment_plan.frequency }}"\n    frequencies="{{ frequencies }}"\n    num-payments="{{@ price.payment_plan.num_payments }}"\n    v-if="type == \'payment-plan\'"></payment-plan>';
},{}],75:[function(require,module,exports){
module.exports = {
    name: 'answer',
    props: ['question', 'correct'],
    data: function () {
        'use strict';
        return {
            ID: 0,
            content: ''
        };
    },
    methods: {
        deleteThis: function() {
            this.$dispatch('deleteAnswer', this.$index);
        }
    }
};
},{}],76:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMetaMixin = require('save_post_meta_mixin');

module.exports = {
  name: 'quiz',
  components: {
    questions: require('./questions'),
    question: require('./question'),
    answer: require('./answer'),
  },
  data: function () {
    return {
      children_loaded: [{
        name: 'questions',
        loaded: false
      }, ],
      questions: []
    }
  },
  events: {
    'hook:created': function () {
      this.$addChild({}, this.$options.components['questions']);
    },
    child_loaded: 'childLoaded'
  },
  computed: {
    loaded: function () {
      return this.children_loaded.filter(function (child) {
        return !child.loaded;
      }).length < 1;
    },
    meta: function () {
      var that = this
      return {
        questions: this.questions
      }
    }
  },
  watch: {
    meta: {
      handler: 'savePostMeta',
      deep: true
    }
  },
  methods: {
    childLoaded: function (name) {
      var record = _.findWhere(this.children_loaded, {
        name: name
      });

      if (record !== undefined) {
        record.loaded = true;
      }
    },
  }
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./answer":75,"./question":77,"./questions":78,"save_post_meta_mixin":93}],77:[function(require,module,exports){
(function (global){
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);
var savePostMixin = require('save_post_mixin');

module.exports = {
  name: 'quiz-question',
  mixins: [savePostMixin],
  data: function () {
    return {
      title: '',
      post: {
        ID: 0,
        post_type: 'quiz_question',
        post_title: ''
      },
      correct: 0,
      answers: [
        { ID: 0, content: ''}
      ]
    };
  },
  watch: {
    answers: {
      handler: function(val) {
        var correct_answer = _.findWhere(this.answers, {ID: this.correct});

        if (correct_answer === undefined) {
          this.correct = this.answers[0].ID;
        }
      },
      deep: true
    }
  },
  events: {
    'hook:created': 'newQuestion',
    'change_answer': function (answer_id) {
      this.correct = answer_id;
    },
    deleteAnswer: function(index) {
      this.answers.$remove(index);
    }
  },
  computed: {
    meta: function () {
      return {
        correct: this.correct,
        answers: this.answers.map(function (answer) {
          return {
            ID: answer.ID,
            content: answer.content.replace(/\\/g, '\\\\')
              .replace(/\u0008/g, '\\b')
              .replace(/\t/g, '\\t')
              .replace(/\n/g, '\\n')
              .replace(/\f/g, '\\f')
              .replace(/\r/g, '\\r')
          };
        })
      };
    }
  },
  methods: {
    addAnswer: function (e) {
      e.preventDefault();

      this.answers.push({
        ID: this.getNextAnswerId(),
        content: ''
      });
    },
    getNextAnswerId: function () {
      var last = _.max(this.answers, function (answer) {
        return answer.ID;
      });

      return typeof last === 'object' ? (last.ID + 1) : 0;
    },
    newQuestion: function () {
      if (this.post.ID !== 0) {
        return;
      }

      var that = this;
      var ajaxurl = window.ajaxurl;

      // save item
      this.$resource(ajaxurl).get({
        action: 'new_entry',
        id: 0,
        post_type: that.post.post_type
      },
      function (data) {
        that.post = data.post;
      });
    }
  }
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"save_post_mixin":94}],78:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var sortable = require('sortable_mixin');
var savePostMetaMixin = require('save_post_meta_mixin');

module.exports = {
  name: 'quiz-questions',
  el: function () { return '#zippy-quiz-questions .inside .vue'; },
  template: $('#mb-quiz-questions-tmpl').text(),
  mixins: [sortable, savePostMetaMixin],
  compiled: function () {
    this.initSortable();
  },
  data: function () {
    return {
      entries: [] // We call it entries because the sortable mixin demands it
    };
  },
  events: {
    'hook:created': 'setupQuestions'
  },
  watch: {
    meta: {
      handler: 'savePostMeta',
      deep: true
    }
  },
  computed: {
    meta: function () {
      return {
        questions: this.entries.map(function (question) {
          return { ID: question.post.ID };
        })
      };
    }
  },
  methods: {
    addQuestion: function (e) {
      e.preventDefault();
      this.entries.push({});
    },
    setupQuestions: function () {
      var that = this;
      var data = JSON.parse($('#zippy-quiz-questions .inside .zippy-mb-data').text());

      var questions = Array.isArray(data.questions) ? data.questions : [];

      questions.map(function(question) {
        question.post.post_title = $("<textarea/>").html(question.post.post_title).text();
      });

      questions.forEach(function (question) {
        that.entries.push(question);
      });
    }
  }
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"save_post_meta_mixin":93,"sortable_mixin":96}],79:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'notice',
    template: require('./template.html'),
    data: function() {
        return {
            type: 'success',
            content: '',
            duration: 3000,
            timered: true,
            timer: null,
        }
    },
    events: {
    },
    compiled: function() {
        if (this.timered) {
            this.setTimer();    
        }
    },
    methods: {
        /**
         * Set a timer for the notice to disappear after
         */
        setTimer: function() {
            var that = this;
            this.timer = window.setTimeout(this.clearTimer, this.duration);
        },
        clearTimer: function() {
            var that = this;
            $(this.$el).slideUp('slow', function() {
                window.clearTimeout(that.timer);
                that.$parent.messages.$remove(that.$index);
            });
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./template.html":80}],80:[function(require,module,exports){
module.exports = '<div\n    class="zippy-alert"\n    v-class="\n        danger : type == \'error\',\n        success : type == \'success\',\n        warning : type == \'warning\'\n    "\n>\n    {{ content }}\n</div>';
},{}],81:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('#zippy-upgrade-progress-tmpl').length > 0) {
    var ZippyCourses_Upgrader = new Vue({
        name: 'zippy-upgrader',
        el: '.wrap',
        template: $('#zippy-upgrade-progress-tmpl').text(),
        data: {
            current_step: ''
        },
        created: function() {
            this.checkProgress();
        },
        compiled: function() {
        },
        methods: {
            checkProgress: function()
            {
                var that = this;

                this.$resource(ajaxurl).get(
                    {
                        action: 'zippy_check_upgrade_progress'
                    }, 
                    function (data, status, request) {
                        if (data == 'complete') {
                            that.completeUpgrade();
                        } else {
                            that.current_step = data;
                            that.checkProgress();
                        }
                    }
                );
            },
            completeUpgrade: function() {
                this.$resource(ajaxurl).get(
                    { action: 'zippy_complete_upgrade' }, 
                    function (data, status, request) {
                        window.location = ZippyCourses.admin_url;
                    }
                );
            }
        }
    });
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],82:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('.zippy-panels-list').length > 0) {
    var ZippyCourses_UX_Panels = new Vue({
        name: 'zippy-panels',
        el: '.zippy-panels-list',
        created: function() {
            $('.zippy-panels-list a').on('click', this.switchPanel);
            this.toggleActivePanel();
        },
        methods: {
            toggleActivePanel: function() {
                var ZippyCourses = localStorage.getItem('ZippyCourses');
                ZippyCourses = ZippyCourses !== null ? JSON.parse(ZippyCourses) : {};

                if (ZippyCourses !== null && ZippyCourses.ux !== undefined && ZippyCourses.ux.panel !== undefined) {
                    if ($('a[data-panel-id="' + ZippyCourses.ux.panel + '"]').length > 0) {
                        $('a[data-panel-id="' + ZippyCourses.ux.panel + '"]').click();
                    }
                }
            },
            switchPanel: function(e) {
                e.preventDefault();

                var panel = $(e.currentTarget).data('panelId');

                var ZippyCourses = localStorage.getItem('ZippyCourses');
                ZippyCourses = ZippyCourses !== null ? JSON.parse(ZippyCourses) : {};
1
                if (ZippyCourses.ux === undefined) {
                    ZippyCourses.ux = {};
                }

                ZippyCourses.ux.panel = panel;

                localStorage.setItem('ZippyCourses', JSON.stringify(ZippyCourses));
            }
        }
    });
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],83:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'widgets',
    components: {
        settings: require('./settings')
    },
    created: function() {
    },
    compiled: function() {
        this.widgetSettings();
    },
    methods: {
        initializeWidgetData: function(element) {
            var course_data = JSON.parse($('#widget-settings-course-tier-data').text());
            var courses = [];

            if (course_data.courses !== undefined && Array.isArray(course_data.courses)) {
                courses = course_data.courses;
                courses.forEach(function(course) {
                    if (Array.isArray(course.tiers)) {
                        course.tiers.forEach(function(tier) {
                            tier.access = false;
                        });                                
                    } else {
                        course.tiers = [];
                    }
                });
            }

            var settings = JSON.parse($(element).find('.zippy-widget-settings').val());
            var type = settings.type !== undefined ? settings.type : 0;
            
            console.log(settings);

            if (settings.selected !== undefined && Array.isArray(settings.selected)) {
                courses.forEach(function(course) {

                    settings_course = _.findWhere(settings.selected, {id: course.id});

                    if (settings_course !== undefined) {
                        course.tiers.forEach(function(tier) {
                            if (Array.isArray(settings_course.tiers)) {
                                settings_course.tiers.forEach(function(settings_course_tier) {
                                    if (settings_course_tier == tier.id) {
                                        tier.access = true;
                                    }
                                });
                            }
                        });                        
                    }
                });
            }

            return {courses: courses, type: type};
        },
        widgetSettings: function() {
            var that = this;

            if ($('.widget-liquid-right .widget').length > 0) {
                $('.widget-liquid-right .widget').each(function(index, element) {
                    var target = '#' + $(element).attr('id') + ' .zippy-widget-settings-region';
                    that.$addChild({data: that.initializeWidgetData(element), el: target}, that.$options.components['settings']);
                });
            }

            $(document).on('widget-added', function(e, widget) {
                var target = '#' + $(widget).attr('id') + ' .zippy-widget-settings-region';
                that.$addChild({data: that.initializeWidgetData(widget), el: target}, that.$options.components['settings']);
            });

            $(document).on('widget-updated', function(e, widget) {
                var target = '#' + $(widget).attr('id') + ' .zippy-widget-settings-region';
                that.$addChild({data: that.initializeWidgetData(widget), el: target}, that.$options.components['settings']);
            });
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./settings":86}],84:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'course',
    data: function() {
        return {
            id: 0,
            title: '',
            tiers: []
        }
    },
    computed: {
    },
    methods: {
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],85:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'tier',
    props: ['course'],
    data: function() {
        return {
            id: 0,
            title: '',
            access: false
        }
    },
    methods: {
    },
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],86:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'widget-settings',
    template: $('#widget-settings-template').text(),
    components: {
        course: require('./course'),
        tier: require('./course/tier')
    },
    data: function() {
        return {
            courses: [],
            type: 2
        }
    },
    watch: {
        settings: {
            handler: 'updateSettings',
            deep: true
        }
    },
    computed: {
        settings: function() {
            return {
                selected: this.courses.map(function(course) {
                    return {
                        id: course.id,
                        tiers: course.tiers
                            .filter(function(tier) {
                                return tier.access
                            })
                            .map(function(tier) {
                                return tier.id
                            })
                    };
                }),
                type: this.type
            };
        }
    },
    attached: function () {
        $(this.$el).parent().prepend($(this.$el));
    },
    methods: {
        updateSettings: function(val) {
            $(this.$el).parents('.widget').find('.zippy-widget-settings').val(JSON.stringify(val));
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./course":84,"./course/tier":85}],87:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'course',
    template: $('#zippy-wlm-importer-course-tmpl').text(),
    data: function() {
        return {
            id: 0,
            title: ''
        }
    },
    created: function() {
    },
    compiled: function() {
    },
    methods: {
    }
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],88:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

if ($('.zippy-wlm-importer-builder').length > 0) {
    var ZippyCourses_WishlistMemberImporter = new Vue({
        name: 'wlm-importer',
        el: '.zippy-wlm-importer-builder',
        template: $('#zippy-wlm-importer-tmpl').text(),
        components: {
            level: require('./level'),
            course: require('./course'),
        },
        data: {
            current: 0,
            levels: [],
            courses: [
                {id: 0, title: 'Your Course'}
            ]
        },
        watch: {
            summary: {
                handler: function(val) {
                    $('input[name="summary"]').val(JSON.stringify(val));
                },
                deep: true
            }
        },
        computed: {
            summary: function() {
                return {
                    courses: this.courses,
                    levels: this.levels.map(function(level) {
                        return {
                            id: level.id,
                            title: level.title,
                            course: level.course
                        }
                    })
                };
            }
        },
        created: function() {
            var that = this;

            this.levels   = JSON.parse($('#zippy-wlm-import-levels').text());
        },
        methods: {
            addCourse: function(e) {
                e.preventDefault();

                var max = _.max(this.courses, function(course) {
                    return course.id;
                });
                var next_id = max === undefined ? 0 : max.id + 1;

                this.courses.push({
                    id: next_id,
                    title: ''
                });
            }
        }
    });
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./course":87,"./level":89}],89:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    name: 'level',
    template: $('#zippy-wlm-importer-level-tmpl').text(),
    props: ['courses'],
    data: function() {
        return {
            id: 0,
            title: '',
            course: 0
        }
    },
    computed: {
        course_list: function() {
            return this.courses.map(function(course) {
                return {
                    text: (course.title != '' ? course.title : course.id + 1),
                    value: course.id
                }
            });
        }
    },
    created: function() {
    },
    compiled: function() {
    },
    methods: {
    }
};

// Have courses and tiers be the same, ahve
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],90:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

Vue.directive('datepicker', {
  bind: function () {
    var vm = this.vm;
    var key = this.expression;
    var that = this;

    $(this.el).datepicker({
      onSelect: function (date) {
        vm.$set(key, date);
      }
    });

    $(this.el).on('change', function() {
      if ($(that.el).val().trim() == '') {
        vm.$set(key, '');
      }
    })
  },
  update: function (val, oldVal) {
    $(this.el).datepicker('setDate', val);
  }
});

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],91:[function(require,module,exports){
module.exports = function(value) {
    return value.replace(new RegExp("\\\\", "g"), "");
}

},{}],92:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
        data: function() {
            return {
                courses: [],
                course: 0,
                tier: 0,
            }
        },
        computed: {
            meta: function() {
                return {
                    access: {
                        course: this.course,
                        tier: this.tier,                 
                    }
                };
            },
            tiers_list: function() {
                var record = _.findWhere(this.courses, {id: this.course});

                if (record === undefined) {
                    return [];
                } else {
                    var tiers = record.tiers.map(function(tier) {
                        return {text: tier.title, value: tier.id};
                    });

                    return tiers;
                }
            },
            courses_list: function() {
                var courses = this.courses.map(function(course) {
                    return { value: course.id, text: course.title }
                });

                return courses;
            }
        },
        methods: {
        }

};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],93:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

module.exports = {
    watch: {
        meta: {
            handler: 'syncMetaFields',
            deep: true
        }
    },
    created: function() {
        // $(document).on('heartbeat-tick', this.savePostMeta);
    },
    methods: {
        savePostMeta: function(e, data) {
            var resource = this.$resource(ajaxurl);
            var that = this;

            if (that.loaded) {
                resource.save(
                    { action: 'save_post_meta' }, 
                    {
                        id: $('#post_ID').val(),
                        meta: that.meta
                    },
                    function (data, status, request) {
                    }
                ).error(
                    function (data, status, request) {}
                ); 
            }
        },
        syncMetaFields: function(val, oldVal) {
            var that = this;
            for (var k in val) {
                if (val.hasOwnProperty(k)) {
                    if ($('input[name="' + k + '"]').length > 0) {
                        if (typeof val[k] == 'object') {
                            $('input[name="' + k + '"]').val(JSON.stringify(val[k]));
                        } else {
                            var stripped = that.addSlashes(val[k]);
                            $('input[name="' + k + '"]').val(stripped);
                        }                        
                    }
                }
            }
        },
        addSlashes: function(str) {
            return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],94:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

module.exports = {
  watch: {
    post: {
      handler: 'savePost',
      deep: true
    },
    meta: {
      handler: 'savePost',
      deep: true
    }
  },
  methods: {
    savePost: function () {
      var ajaxurl = window.ajaxurl;

      // save item
      this.$resource(ajaxurl).save({
        action: 'save_post'
      }, {
        post: this.post,
        meta: this.meta
      },
      function () {}
      );
    }
  }
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],95:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
var _ = (typeof window !== "undefined" ? window['_'] : typeof global !== "undefined" ? global['_'] : null);

module.exports = {
    methods: {
        initSortable: function() {
            var that = this;
            $(this.$el).find('.zippy-sortable-products').sortable({
                items: '> .sortable-product',
                handle: '.dnd-handle',
                forcePlaceholderSize: true,
                opacity: .75,
                 start: function(e, ui) {
                },
                stop: function(e, ui) {
                    $('.sortable-product').each(function(index, item) {
                        var courseProducts = that.$data.courseProducts;
                        var currentProductId = $(item).find('.zippy-course-products-title').data('id');
                        for (var product of courseProducts) {
                            if (product.ID == currentProductId) {
                                var currentProduct = product;
                            }
                        }
                        currentProduct.list_order = index;
                    });
                },
            });
        },
    }
}
// 
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],96:[function(require,module,exports){
(function (global){
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

module.exports = {
    methods: {
        initSortable: function() {

            var that = this;
            $(this.$el).find('.zippy-sortable').sortable({
                items: '> .entry',
                handle: '.dnd-handle',
                connectWith: '.zippy-sortable',
                forcePlaceholderSize: true,
                opacity: .75,
                 start: function(e, ui) {
                    var vm = $(this).data('vm');
                    var index = ui.item.index();
                    var entry = vm.entries[index];

                    ui.item.data('start', index);
                    ui.item.data('start_length', $(e.target).children().not('.ui-sortable-placeholder').length);
                    ui.item.data('original_owner', $(e.target));
                    ui.item.data('entry', entry);
                },
                stop: function(e, ui) {
                    var start = ui.item.data('start'),
                        start_length = ui.item.data('start_length'),
                        end = ui.item.index(),
                        original_owner = ui.item.data('original_owner'),
                        new_length = $(original_owner).children().length;


                    if ($(this)[0] == original_owner[0] && start_length == new_length) {
                        var vm = $(this).data('vm');
                        vm.entries.splice(end, 0, vm.entries.splice(start, 1)[0]);

                        that.$dispatch('entry_updated');
                    } else {
                    }

                },
                receive: function(e, ui) {
                    var start = ui.item.data('start'),
                        end = ui.item.index(),
                        entry = ui.item.data('entry');
                    
                    if( ui.item.hasClass( 'entry-unit' ) ) {
                        ui.sender.sortable('cancel');
                    } else {
                        var vm = $(this).data('vm');
                        vm.entries.splice(end, 0, entry);                        
                    }
                },
                remove: function(e, ui) {
                    var vm = $(this).data('vm');
                    var start = ui.item.data('start'),
                        end = ui.item.index(),
                        entry = ui.item.data('entry');
                    
                    if(!ui.item.hasClass('entry-unit')) {
                        vm.destroyEntry(entry);
                    }                   
                },
                create: function(e, ui) {
                    $(this).data('vm', that);
                }
            });
        },
        destroyEntry: function(entry) {
            var record = _.findWhere(this.entries, {ID: entry.ID});

            if (record !== undefined) {
                this.entries.$remove(record);
            }
        }
    }
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],97:[function(require,module,exports){
(function (global){
require('vue-resource');
var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

var validator = require('vue-validator');
Vue.use(validator);

var $ = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);
require('./directives/datepicker.js');
require('./admin/components/list-tables');
require('./admin/components/export');
require('./admin/components/wlm-importer');
require('./admin/components/ux/panels');
require('./admin/components/upgrade');
require('./admin/components/notifications');

var ZippyCoursesApp = new Vue({
    el: '#wpbody',
    data: {}, 
    components: {
        course: require('./admin/components/course'),
        product: require('./admin/components/product'),
        quiz: require('./admin/components/quiz'),
        metaboxes: require('./admin/components/metaboxes'),
        widgets: require('./admin/components/widgets'),
        widgetSettings: require('./admin/components/widgets/settings'),
    },
    compiled: function() {
        if ($('#post_type').val() == 'course') {
            this.$addChild({}, this.$options.components.course).$mount();
        }

        if ($('#post_type').val() == 'quiz') {
            this.$addChild({}, this.$options.components.quiz).$mount();
        }
        
        if ($('#post_type').val() == 'product') {
            this.$addChild({}, this.$options.components.product).$mount('#poststuff');
        }

        this.$addChild({}, this.$options.components.widgets).$mount();
        this.$addChild({}, this.$options.components.metaboxes).$mount();

        this.renderCharts();
    },
    methods: {
        renderCharts: function() {
            if($('.zippy-chart').length > 0 ) {

                var Chart = require('chart.js');
                
                $('.zippy-chart').each( function(i, e) {

                    var labels = $(e).data( 'keys' );
                    var values = $(e).data( 'values' );

                    var data = {
                        labels: labels,
                        datasets: [
                            {
                                label: "Visits",
                                fillColor: "rgba(220,220,220,0.2)",
                                strokeColor: "rgba(220,220,220,1)",
                                pointColor: "rgba(220,220,220,1)",
                                pointStrokeColor: "#fff",
                                pointHighlightFill: "rgba(0, 115, 170,1)",
                                pointHighlightStroke: "rgba(0, 115, 170,1)",
                                data: values
                            }
                        ]
                    };

                    var ctx = $( e ).find( 'canvas' ).get(0).getContext("2d");

                    var myLineChart = new Chart(ctx).Line(data, {
                        bezierCurve : false,
                        datasetStrokeWidth : 3,
                        responsive: true,
                        tooltipTemplate: "Visits: <%= value %>",
                        scaleBeginAtZero: true
                    });
                });

            }
        }
    },
});

var ZippyCustomer_Support = new Vue({
    el: '.wp-customizer',
    components: {
        widgets: require('./admin/components/widgets'),
    },
    compiled: function() {
        this.$addChild({}, this.$options.components.widgets).$mount();
    },
    methods: {
    }
});

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./admin/components/course":9,"./admin/components/export":28,"./admin/components/list-tables":29,"./admin/components/metaboxes":42,"./admin/components/notifications":61,"./admin/components/product":64,"./admin/components/quiz":76,"./admin/components/upgrade":81,"./admin/components/ux/panels":82,"./admin/components/widgets":83,"./admin/components/widgets/settings":86,"./admin/components/wlm-importer":88,"./directives/datepicker.js":90,"chart.js":98,"vue-resource":112,"vue-validator":123}],98:[function(require,module,exports){
/*!
 * Chart.js
 * http://chartjs.org/
 * Version: 1.0.2
 *
 * Copyright 2015 Nick Downie
 * Released under the MIT license
 * https://github.com/nnnick/Chart.js/blob/master/LICENSE.md
 */


(function(){

	"use strict";

	//Declare root variable - window in the browser, global on the server
	var root = this,
		previous = root.Chart;

	//Occupy the global variable of Chart, and create a simple base class
	var Chart = function(context){
		var chart = this;
		this.canvas = context.canvas;

		this.ctx = context;

		//Variables global to the chart
		var computeDimension = function(element,dimension)
		{
			if (element['offset'+dimension])
			{
				return element['offset'+dimension];
			}
			else
			{
				return document.defaultView.getComputedStyle(element).getPropertyValue(dimension);
			}
		}

		var width = this.width = computeDimension(context.canvas,'Width');
		var height = this.height = computeDimension(context.canvas,'Height');

		// Firefox requires this to work correctly
		context.canvas.width  = width;
		context.canvas.height = height;

		var width = this.width = context.canvas.width;
		var height = this.height = context.canvas.height;
		this.aspectRatio = this.width / this.height;
		//High pixel density displays - multiply the size of the canvas height/width by the device pixel ratio, then scale.
		helpers.retinaScale(this);

		return this;
	};
	//Globally expose the defaults to allow for user updating/changing
	Chart.defaults = {
		global: {
			// Boolean - Whether to animate the chart
			animation: true,

			// Number - Number of animation steps
			animationSteps: 60,

			// String - Animation easing effect
			animationEasing: "easeOutQuart",

			// Boolean - If we should show the scale at all
			showScale: true,

			// Boolean - If we want to override with a hard coded scale
			scaleOverride: false,

			// ** Required if scaleOverride is true **
			// Number - The number of steps in a hard coded scale
			scaleSteps: null,
			// Number - The value jump in the hard coded scale
			scaleStepWidth: null,
			// Number - The scale starting value
			scaleStartValue: null,

			// String - Colour of the scale line
			scaleLineColor: "rgba(0,0,0,.1)",

			// Number - Pixel width of the scale line
			scaleLineWidth: 1,

			// Boolean - Whether to show labels on the scale
			scaleShowLabels: true,

			// Interpolated JS string - can access value
			scaleLabel: "<%=value%>",

			// Boolean - Whether the scale should stick to integers, and not show any floats even if drawing space is there
			scaleIntegersOnly: true,

			// Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
			scaleBeginAtZero: false,

			// String - Scale label font declaration for the scale label
			scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

			// Number - Scale label font size in pixels
			scaleFontSize: 12,

			// String - Scale label font weight style
			scaleFontStyle: "normal",

			// String - Scale label font colour
			scaleFontColor: "#666",

			// Boolean - whether or not the chart should be responsive and resize when the browser does.
			responsive: false,

			// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
			maintainAspectRatio: true,

			// Boolean - Determines whether to draw tooltips on the canvas or not - attaches events to touchmove & mousemove
			showTooltips: true,

			// Boolean - Determines whether to draw built-in tooltip or call custom tooltip function
			customTooltips: false,

			// Array - Array of string names to attach tooltip events
			tooltipEvents: ["mousemove", "touchstart", "touchmove", "mouseout"],

			// String - Tooltip background colour
			tooltipFillColor: "rgba(0,0,0,0.8)",

			// String - Tooltip label font declaration for the scale label
			tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

			// Number - Tooltip label font size in pixels
			tooltipFontSize: 14,

			// String - Tooltip font weight style
			tooltipFontStyle: "normal",

			// String - Tooltip label font colour
			tooltipFontColor: "#fff",

			// String - Tooltip title font declaration for the scale label
			tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

			// Number - Tooltip title font size in pixels
			tooltipTitleFontSize: 14,

			// String - Tooltip title font weight style
			tooltipTitleFontStyle: "bold",

			// String - Tooltip title font colour
			tooltipTitleFontColor: "#fff",

			// Number - pixel width of padding around tooltip text
			tooltipYPadding: 6,

			// Number - pixel width of padding around tooltip text
			tooltipXPadding: 6,

			// Number - Size of the caret on the tooltip
			tooltipCaretSize: 8,

			// Number - Pixel radius of the tooltip border
			tooltipCornerRadius: 6,

			// Number - Pixel offset from point x to tooltip edge
			tooltipXOffset: 10,

			// String - Template string for single tooltips
			tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",

			// String - Template string for single tooltips
			multiTooltipTemplate: "<%= value %>",

			// String - Colour behind the legend colour block
			multiTooltipKeyBackground: '#fff',

			// Function - Will fire on animation progression.
			onAnimationProgress: function(){},

			// Function - Will fire on animation completion.
			onAnimationComplete: function(){}

		}
	};

	//Create a dictionary of chart types, to allow for extension of existing types
	Chart.types = {};

	//Global Chart helpers object for utility methods and classes
	var helpers = Chart.helpers = {};

		//-- Basic js utility methods
	var each = helpers.each = function(loopable,callback,self){
			var additionalArgs = Array.prototype.slice.call(arguments, 3);
			// Check to see if null or undefined firstly.
			if (loopable){
				if (loopable.length === +loopable.length){
					var i;
					for (i=0; i<loopable.length; i++){
						callback.apply(self,[loopable[i], i].concat(additionalArgs));
					}
				}
				else{
					for (var item in loopable){
						callback.apply(self,[loopable[item],item].concat(additionalArgs));
					}
				}
			}
		},
		clone = helpers.clone = function(obj){
			var objClone = {};
			each(obj,function(value,key){
				if (obj.hasOwnProperty(key)) objClone[key] = value;
			});
			return objClone;
		},
		extend = helpers.extend = function(base){
			each(Array.prototype.slice.call(arguments,1), function(extensionObject) {
				each(extensionObject,function(value,key){
					if (extensionObject.hasOwnProperty(key)) base[key] = value;
				});
			});
			return base;
		},
		merge = helpers.merge = function(base,master){
			//Merge properties in left object over to a shallow clone of object right.
			var args = Array.prototype.slice.call(arguments,0);
			args.unshift({});
			return extend.apply(null, args);
		},
		indexOf = helpers.indexOf = function(arrayToSearch, item){
			if (Array.prototype.indexOf) {
				return arrayToSearch.indexOf(item);
			}
			else{
				for (var i = 0; i < arrayToSearch.length; i++) {
					if (arrayToSearch[i] === item) return i;
				}
				return -1;
			}
		},
		where = helpers.where = function(collection, filterCallback){
			var filtered = [];

			helpers.each(collection, function(item){
				if (filterCallback(item)){
					filtered.push(item);
				}
			});

			return filtered;
		},
		findNextWhere = helpers.findNextWhere = function(arrayToSearch, filterCallback, startIndex){
			// Default to start of the array
			if (!startIndex){
				startIndex = -1;
			}
			for (var i = startIndex + 1; i < arrayToSearch.length; i++) {
				var currentItem = arrayToSearch[i];
				if (filterCallback(currentItem)){
					return currentItem;
				}
			}
		},
		findPreviousWhere = helpers.findPreviousWhere = function(arrayToSearch, filterCallback, startIndex){
			// Default to end of the array
			if (!startIndex){
				startIndex = arrayToSearch.length;
			}
			for (var i = startIndex - 1; i >= 0; i--) {
				var currentItem = arrayToSearch[i];
				if (filterCallback(currentItem)){
					return currentItem;
				}
			}
		},
		inherits = helpers.inherits = function(extensions){
			//Basic javascript inheritance based on the model created in Backbone.js
			var parent = this;
			var ChartElement = (extensions && extensions.hasOwnProperty("constructor")) ? extensions.constructor : function(){ return parent.apply(this, arguments); };

			var Surrogate = function(){ this.constructor = ChartElement;};
			Surrogate.prototype = parent.prototype;
			ChartElement.prototype = new Surrogate();

			ChartElement.extend = inherits;

			if (extensions) extend(ChartElement.prototype, extensions);

			ChartElement.__super__ = parent.prototype;

			return ChartElement;
		},
		noop = helpers.noop = function(){},
		uid = helpers.uid = (function(){
			var id=0;
			return function(){
				return "chart-" + id++;
			};
		})(),
		warn = helpers.warn = function(str){
			//Method for warning of errors
			if (window.console && typeof window.console.warn == "function") console.warn(str);
		},
		amd = helpers.amd = (typeof define == 'function' && define.amd),
		//-- Math methods
		isNumber = helpers.isNumber = function(n){
			return !isNaN(parseFloat(n)) && isFinite(n);
		},
		max = helpers.max = function(array){
			return Math.max.apply( Math, array );
		},
		min = helpers.min = function(array){
			return Math.min.apply( Math, array );
		},
		cap = helpers.cap = function(valueToCap,maxValue,minValue){
			if(isNumber(maxValue)) {
				if( valueToCap > maxValue ) {
					return maxValue;
				}
			}
			else if(isNumber(minValue)){
				if ( valueToCap < minValue ){
					return minValue;
				}
			}
			return valueToCap;
		},
		getDecimalPlaces = helpers.getDecimalPlaces = function(num){
			if (num%1!==0 && isNumber(num)){
				return num.toString().split(".")[1].length;
			}
			else {
				return 0;
			}
		},
		toRadians = helpers.radians = function(degrees){
			return degrees * (Math.PI/180);
		},
		// Gets the angle from vertical upright to the point about a centre.
		getAngleFromPoint = helpers.getAngleFromPoint = function(centrePoint, anglePoint){
			var distanceFromXCenter = anglePoint.x - centrePoint.x,
				distanceFromYCenter = anglePoint.y - centrePoint.y,
				radialDistanceFromCenter = Math.sqrt( distanceFromXCenter * distanceFromXCenter + distanceFromYCenter * distanceFromYCenter);


			var angle = Math.PI * 2 + Math.atan2(distanceFromYCenter, distanceFromXCenter);

			//If the segment is in the top left quadrant, we need to add another rotation to the angle
			if (distanceFromXCenter < 0 && distanceFromYCenter < 0){
				angle += Math.PI*2;
			}

			return {
				angle: angle,
				distance: radialDistanceFromCenter
			};
		},
		aliasPixel = helpers.aliasPixel = function(pixelWidth){
			return (pixelWidth % 2 === 0) ? 0 : 0.5;
		},
		splineCurve = helpers.splineCurve = function(FirstPoint,MiddlePoint,AfterPoint,t){
			//Props to Rob Spencer at scaled innovation for his post on splining between points
			//http://scaledinnovation.com/analytics/splines/aboutSplines.html
			var d01=Math.sqrt(Math.pow(MiddlePoint.x-FirstPoint.x,2)+Math.pow(MiddlePoint.y-FirstPoint.y,2)),
				d12=Math.sqrt(Math.pow(AfterPoint.x-MiddlePoint.x,2)+Math.pow(AfterPoint.y-MiddlePoint.y,2)),
				fa=t*d01/(d01+d12),// scaling factor for triangle Ta
				fb=t*d12/(d01+d12);
			return {
				inner : {
					x : MiddlePoint.x-fa*(AfterPoint.x-FirstPoint.x),
					y : MiddlePoint.y-fa*(AfterPoint.y-FirstPoint.y)
				},
				outer : {
					x: MiddlePoint.x+fb*(AfterPoint.x-FirstPoint.x),
					y : MiddlePoint.y+fb*(AfterPoint.y-FirstPoint.y)
				}
			};
		},
		calculateOrderOfMagnitude = helpers.calculateOrderOfMagnitude = function(val){
			return Math.floor(Math.log(val) / Math.LN10);
		},
		calculateScaleRange = helpers.calculateScaleRange = function(valuesArray, drawingSize, textSize, startFromZero, integersOnly){

			//Set a minimum step of two - a point at the top of the graph, and a point at the base
			var minSteps = 2,
				maxSteps = Math.floor(drawingSize/(textSize * 1.5)),
				skipFitting = (minSteps >= maxSteps);

			var maxValue = max(valuesArray),
				minValue = min(valuesArray);

			// We need some degree of seperation here to calculate the scales if all the values are the same
			// Adding/minusing 0.5 will give us a range of 1.
			if (maxValue === minValue){
				maxValue += 0.5;
				// So we don't end up with a graph with a negative start value if we've said always start from zero
				if (minValue >= 0.5 && !startFromZero){
					minValue -= 0.5;
				}
				else{
					// Make up a whole number above the values
					maxValue += 0.5;
				}
			}

			var	valueRange = Math.abs(maxValue - minValue),
				rangeOrderOfMagnitude = calculateOrderOfMagnitude(valueRange),
				graphMax = Math.ceil(maxValue / (1 * Math.pow(10, rangeOrderOfMagnitude))) * Math.pow(10, rangeOrderOfMagnitude),
				graphMin = (startFromZero) ? 0 : Math.floor(minValue / (1 * Math.pow(10, rangeOrderOfMagnitude))) * Math.pow(10, rangeOrderOfMagnitude),
				graphRange = graphMax - graphMin,
				stepValue = Math.pow(10, rangeOrderOfMagnitude),
				numberOfSteps = Math.round(graphRange / stepValue);

			//If we have more space on the graph we'll use it to give more definition to the data
			while((numberOfSteps > maxSteps || (numberOfSteps * 2) < maxSteps) && !skipFitting) {
				if(numberOfSteps > maxSteps){
					stepValue *=2;
					numberOfSteps = Math.round(graphRange/stepValue);
					// Don't ever deal with a decimal number of steps - cancel fitting and just use the minimum number of steps.
					if (numberOfSteps % 1 !== 0){
						skipFitting = true;
					}
				}
				//We can fit in double the amount of scale points on the scale
				else{
					//If user has declared ints only, and the step value isn't a decimal
					if (integersOnly && rangeOrderOfMagnitude >= 0){
						//If the user has said integers only, we need to check that making the scale more granular wouldn't make it a float
						if(stepValue/2 % 1 === 0){
							stepValue /=2;
							numberOfSteps = Math.round(graphRange/stepValue);
						}
						//If it would make it a float break out of the loop
						else{
							break;
						}
					}
					//If the scale doesn't have to be an int, make the scale more granular anyway.
					else{
						stepValue /=2;
						numberOfSteps = Math.round(graphRange/stepValue);
					}

				}
			}

			if (skipFitting){
				numberOfSteps = minSteps;
				stepValue = graphRange / numberOfSteps;
			}

			return {
				steps : numberOfSteps,
				stepValue : stepValue,
				min : graphMin,
				max	: graphMin + (numberOfSteps * stepValue)
			};

		},
		/* jshint ignore:start */
		// Blows up jshint errors based on the new Function constructor
		//Templating methods
		//Javascript micro templating by John Resig - source at http://ejohn.org/blog/javascript-micro-templating/
		template = helpers.template = function(templateString, valuesObject){

			// If templateString is function rather than string-template - call the function for valuesObject

			if(templateString instanceof Function){
			 	return templateString(valuesObject);
		 	}

			var cache = {};
			function tmpl(str, data){
				// Figure out if we're getting a template, or if we need to
				// load the template - and be sure to cache the result.
				var fn = !/\W/.test(str) ?
				cache[str] = cache[str] :

				// Generate a reusable function that will serve as a template
				// generator (and which will be cached).
				new Function("obj",
					"var p=[],print=function(){p.push.apply(p,arguments);};" +

					// Introduce the data as local variables using with(){}
					"with(obj){p.push('" +

					// Convert the template into pure JavaScript
					str
						.replace(/[\r\t\n]/g, " ")
						.split("<%").join("\t")
						.replace(/((^|%>)[^\t]*)'/g, "$1\r")
						.replace(/\t=(.*?)%>/g, "',$1,'")
						.split("\t").join("');")
						.split("%>").join("p.push('")
						.split("\r").join("\\'") +
					"');}return p.join('');"
				);

				// Provide some basic currying to the user
				return data ? fn( data ) : fn;
			}
			return tmpl(templateString,valuesObject);
		},
		/* jshint ignore:end */
		generateLabels = helpers.generateLabels = function(templateString,numberOfSteps,graphMin,stepValue){
			var labelsArray = new Array(numberOfSteps);
			if (labelTemplateString){
				each(labelsArray,function(val,index){
					labelsArray[index] = template(templateString,{value: (graphMin + (stepValue*(index+1)))});
				});
			}
			return labelsArray;
		},
		//--Animation methods
		//Easing functions adapted from Robert Penner's easing equations
		//http://www.robertpenner.com/easing/
		easingEffects = helpers.easingEffects = {
			linear: function (t) {
				return t;
			},
			easeInQuad: function (t) {
				return t * t;
			},
			easeOutQuad: function (t) {
				return -1 * t * (t - 2);
			},
			easeInOutQuad: function (t) {
				if ((t /= 1 / 2) < 1) return 1 / 2 * t * t;
				return -1 / 2 * ((--t) * (t - 2) - 1);
			},
			easeInCubic: function (t) {
				return t * t * t;
			},
			easeOutCubic: function (t) {
				return 1 * ((t = t / 1 - 1) * t * t + 1);
			},
			easeInOutCubic: function (t) {
				if ((t /= 1 / 2) < 1) return 1 / 2 * t * t * t;
				return 1 / 2 * ((t -= 2) * t * t + 2);
			},
			easeInQuart: function (t) {
				return t * t * t * t;
			},
			easeOutQuart: function (t) {
				return -1 * ((t = t / 1 - 1) * t * t * t - 1);
			},
			easeInOutQuart: function (t) {
				if ((t /= 1 / 2) < 1) return 1 / 2 * t * t * t * t;
				return -1 / 2 * ((t -= 2) * t * t * t - 2);
			},
			easeInQuint: function (t) {
				return 1 * (t /= 1) * t * t * t * t;
			},
			easeOutQuint: function (t) {
				return 1 * ((t = t / 1 - 1) * t * t * t * t + 1);
			},
			easeInOutQuint: function (t) {
				if ((t /= 1 / 2) < 1) return 1 / 2 * t * t * t * t * t;
				return 1 / 2 * ((t -= 2) * t * t * t * t + 2);
			},
			easeInSine: function (t) {
				return -1 * Math.cos(t / 1 * (Math.PI / 2)) + 1;
			},
			easeOutSine: function (t) {
				return 1 * Math.sin(t / 1 * (Math.PI / 2));
			},
			easeInOutSine: function (t) {
				return -1 / 2 * (Math.cos(Math.PI * t / 1) - 1);
			},
			easeInExpo: function (t) {
				return (t === 0) ? 1 : 1 * Math.pow(2, 10 * (t / 1 - 1));
			},
			easeOutExpo: function (t) {
				return (t === 1) ? 1 : 1 * (-Math.pow(2, -10 * t / 1) + 1);
			},
			easeInOutExpo: function (t) {
				if (t === 0) return 0;
				if (t === 1) return 1;
				if ((t /= 1 / 2) < 1) return 1 / 2 * Math.pow(2, 10 * (t - 1));
				return 1 / 2 * (-Math.pow(2, -10 * --t) + 2);
			},
			easeInCirc: function (t) {
				if (t >= 1) return t;
				return -1 * (Math.sqrt(1 - (t /= 1) * t) - 1);
			},
			easeOutCirc: function (t) {
				return 1 * Math.sqrt(1 - (t = t / 1 - 1) * t);
			},
			easeInOutCirc: function (t) {
				if ((t /= 1 / 2) < 1) return -1 / 2 * (Math.sqrt(1 - t * t) - 1);
				return 1 / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1);
			},
			easeInElastic: function (t) {
				var s = 1.70158;
				var p = 0;
				var a = 1;
				if (t === 0) return 0;
				if ((t /= 1) == 1) return 1;
				if (!p) p = 1 * 0.3;
				if (a < Math.abs(1)) {
					a = 1;
					s = p / 4;
				} else s = p / (2 * Math.PI) * Math.asin(1 / a);
				return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * 1 - s) * (2 * Math.PI) / p));
			},
			easeOutElastic: function (t) {
				var s = 1.70158;
				var p = 0;
				var a = 1;
				if (t === 0) return 0;
				if ((t /= 1) == 1) return 1;
				if (!p) p = 1 * 0.3;
				if (a < Math.abs(1)) {
					a = 1;
					s = p / 4;
				} else s = p / (2 * Math.PI) * Math.asin(1 / a);
				return a * Math.pow(2, -10 * t) * Math.sin((t * 1 - s) * (2 * Math.PI) / p) + 1;
			},
			easeInOutElastic: function (t) {
				var s = 1.70158;
				var p = 0;
				var a = 1;
				if (t === 0) return 0;
				if ((t /= 1 / 2) == 2) return 1;
				if (!p) p = 1 * (0.3 * 1.5);
				if (a < Math.abs(1)) {
					a = 1;
					s = p / 4;
				} else s = p / (2 * Math.PI) * Math.asin(1 / a);
				if (t < 1) return -0.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * 1 - s) * (2 * Math.PI) / p));
				return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * 1 - s) * (2 * Math.PI) / p) * 0.5 + 1;
			},
			easeInBack: function (t) {
				var s = 1.70158;
				return 1 * (t /= 1) * t * ((s + 1) * t - s);
			},
			easeOutBack: function (t) {
				var s = 1.70158;
				return 1 * ((t = t / 1 - 1) * t * ((s + 1) * t + s) + 1);
			},
			easeInOutBack: function (t) {
				var s = 1.70158;
				if ((t /= 1 / 2) < 1) return 1 / 2 * (t * t * (((s *= (1.525)) + 1) * t - s));
				return 1 / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2);
			},
			easeInBounce: function (t) {
				return 1 - easingEffects.easeOutBounce(1 - t);
			},
			easeOutBounce: function (t) {
				if ((t /= 1) < (1 / 2.75)) {
					return 1 * (7.5625 * t * t);
				} else if (t < (2 / 2.75)) {
					return 1 * (7.5625 * (t -= (1.5 / 2.75)) * t + 0.75);
				} else if (t < (2.5 / 2.75)) {
					return 1 * (7.5625 * (t -= (2.25 / 2.75)) * t + 0.9375);
				} else {
					return 1 * (7.5625 * (t -= (2.625 / 2.75)) * t + 0.984375);
				}
			},
			easeInOutBounce: function (t) {
				if (t < 1 / 2) return easingEffects.easeInBounce(t * 2) * 0.5;
				return easingEffects.easeOutBounce(t * 2 - 1) * 0.5 + 1 * 0.5;
			}
		},
		//Request animation polyfill - http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
		requestAnimFrame = helpers.requestAnimFrame = (function(){
			return window.requestAnimationFrame ||
				window.webkitRequestAnimationFrame ||
				window.mozRequestAnimationFrame ||
				window.oRequestAnimationFrame ||
				window.msRequestAnimationFrame ||
				function(callback) {
					return window.setTimeout(callback, 1000 / 60);
				};
		})(),
		cancelAnimFrame = helpers.cancelAnimFrame = (function(){
			return window.cancelAnimationFrame ||
				window.webkitCancelAnimationFrame ||
				window.mozCancelAnimationFrame ||
				window.oCancelAnimationFrame ||
				window.msCancelAnimationFrame ||
				function(callback) {
					return window.clearTimeout(callback, 1000 / 60);
				};
		})(),
		animationLoop = helpers.animationLoop = function(callback,totalSteps,easingString,onProgress,onComplete,chartInstance){

			var currentStep = 0,
				easingFunction = easingEffects[easingString] || easingEffects.linear;

			var animationFrame = function(){
				currentStep++;
				var stepDecimal = currentStep/totalSteps;
				var easeDecimal = easingFunction(stepDecimal);

				callback.call(chartInstance,easeDecimal,stepDecimal, currentStep);
				onProgress.call(chartInstance,easeDecimal,stepDecimal);
				if (currentStep < totalSteps){
					chartInstance.animationFrame = requestAnimFrame(animationFrame);
				} else{
					onComplete.apply(chartInstance);
				}
			};
			requestAnimFrame(animationFrame);
		},
		//-- DOM methods
		getRelativePosition = helpers.getRelativePosition = function(evt){
			var mouseX, mouseY;
			var e = evt.originalEvent || evt,
				canvas = evt.currentTarget || evt.srcElement,
				boundingRect = canvas.getBoundingClientRect();

			if (e.touches){
				mouseX = e.touches[0].clientX - boundingRect.left;
				mouseY = e.touches[0].clientY - boundingRect.top;

			}
			else{
				mouseX = e.clientX - boundingRect.left;
				mouseY = e.clientY - boundingRect.top;
			}

			return {
				x : mouseX,
				y : mouseY
			};

		},
		addEvent = helpers.addEvent = function(node,eventType,method){
			if (node.addEventListener){
				node.addEventListener(eventType,method);
			} else if (node.attachEvent){
				node.attachEvent("on"+eventType, method);
			} else {
				node["on"+eventType] = method;
			}
		},
		removeEvent = helpers.removeEvent = function(node, eventType, handler){
			if (node.removeEventListener){
				node.removeEventListener(eventType, handler, false);
			} else if (node.detachEvent){
				node.detachEvent("on"+eventType,handler);
			} else{
				node["on" + eventType] = noop;
			}
		},
		bindEvents = helpers.bindEvents = function(chartInstance, arrayOfEvents, handler){
			// Create the events object if it's not already present
			if (!chartInstance.events) chartInstance.events = {};

			each(arrayOfEvents,function(eventName){
				chartInstance.events[eventName] = function(){
					handler.apply(chartInstance, arguments);
				};
				addEvent(chartInstance.chart.canvas,eventName,chartInstance.events[eventName]);
			});
		},
		unbindEvents = helpers.unbindEvents = function (chartInstance, arrayOfEvents) {
			each(arrayOfEvents, function(handler,eventName){
				removeEvent(chartInstance.chart.canvas, eventName, handler);
			});
		},
		getMaximumWidth = helpers.getMaximumWidth = function(domNode){
			var container = domNode.parentNode;
			// TODO = check cross browser stuff with this.
			return container.clientWidth;
		},
		getMaximumHeight = helpers.getMaximumHeight = function(domNode){
			var container = domNode.parentNode;
			// TODO = check cross browser stuff with this.
			return container.clientHeight;
		},
		getMaximumSize = helpers.getMaximumSize = helpers.getMaximumWidth, // legacy support
		retinaScale = helpers.retinaScale = function(chart){
			var ctx = chart.ctx,
				width = chart.canvas.width,
				height = chart.canvas.height;

			if (window.devicePixelRatio) {
				ctx.canvas.style.width = width + "px";
				ctx.canvas.style.height = height + "px";
				ctx.canvas.height = height * window.devicePixelRatio;
				ctx.canvas.width = width * window.devicePixelRatio;
				ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
			}
		},
		//-- Canvas methods
		clear = helpers.clear = function(chart){
			chart.ctx.clearRect(0,0,chart.width,chart.height);
		},
		fontString = helpers.fontString = function(pixelSize,fontStyle,fontFamily){
			return fontStyle + " " + pixelSize+"px " + fontFamily;
		},
		longestText = helpers.longestText = function(ctx,font,arrayOfStrings){
			ctx.font = font;
			var longest = 0;
			each(arrayOfStrings,function(string){
				var textWidth = ctx.measureText(string).width;
				longest = (textWidth > longest) ? textWidth : longest;
			});
			return longest;
		},
		drawRoundedRectangle = helpers.drawRoundedRectangle = function(ctx,x,y,width,height,radius){
			ctx.beginPath();
			ctx.moveTo(x + radius, y);
			ctx.lineTo(x + width - radius, y);
			ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
			ctx.lineTo(x + width, y + height - radius);
			ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
			ctx.lineTo(x + radius, y + height);
			ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
			ctx.lineTo(x, y + radius);
			ctx.quadraticCurveTo(x, y, x + radius, y);
			ctx.closePath();
		};


	//Store a reference to each instance - allowing us to globally resize chart instances on window resize.
	//Destroy method on the chart will remove the instance of the chart from this reference.
	Chart.instances = {};

	Chart.Type = function(data,options,chart){
		this.options = options;
		this.chart = chart;
		this.id = uid();
		//Add the chart instance to the global namespace
		Chart.instances[this.id] = this;

		// Initialize is always called when a chart type is created
		// By default it is a no op, but it should be extended
		if (options.responsive){
			this.resize();
		}
		this.initialize.call(this,data);
	};

	//Core methods that'll be a part of every chart type
	extend(Chart.Type.prototype,{
		initialize : function(){return this;},
		clear : function(){
			clear(this.chart);
			return this;
		},
		stop : function(){
			// Stops any current animation loop occuring
			cancelAnimFrame(this.animationFrame);
			return this;
		},
		resize : function(callback){
			this.stop();
			var canvas = this.chart.canvas,
				newWidth = getMaximumWidth(this.chart.canvas),
				newHeight = this.options.maintainAspectRatio ? newWidth / this.chart.aspectRatio : getMaximumHeight(this.chart.canvas);

			canvas.width = this.chart.width = newWidth;
			canvas.height = this.chart.height = newHeight;

			retinaScale(this.chart);

			if (typeof callback === "function"){
				callback.apply(this, Array.prototype.slice.call(arguments, 1));
			}
			return this;
		},
		reflow : noop,
		render : function(reflow){
			if (reflow){
				this.reflow();
			}
			if (this.options.animation && !reflow){
				helpers.animationLoop(
					this.draw,
					this.options.animationSteps,
					this.options.animationEasing,
					this.options.onAnimationProgress,
					this.options.onAnimationComplete,
					this
				);
			}
			else{
				this.draw();
				this.options.onAnimationComplete.call(this);
			}
			return this;
		},
		generateLegend : function(){
			return template(this.options.legendTemplate,this);
		},
		destroy : function(){
			this.clear();
			unbindEvents(this, this.events);
			var canvas = this.chart.canvas;

			// Reset canvas height/width attributes starts a fresh with the canvas context
			canvas.width = this.chart.width;
			canvas.height = this.chart.height;

			// < IE9 doesn't support removeProperty
			if (canvas.style.removeProperty) {
				canvas.style.removeProperty('width');
				canvas.style.removeProperty('height');
			} else {
				canvas.style.removeAttribute('width');
				canvas.style.removeAttribute('height');
			}

			delete Chart.instances[this.id];
		},
		showTooltip : function(ChartElements, forceRedraw){
			// Only redraw the chart if we've actually changed what we're hovering on.
			if (typeof this.activeElements === 'undefined') this.activeElements = [];

			var isChanged = (function(Elements){
				var changed = false;

				if (Elements.length !== this.activeElements.length){
					changed = true;
					return changed;
				}

				each(Elements, function(element, index){
					if (element !== this.activeElements[index]){
						changed = true;
					}
				}, this);
				return changed;
			}).call(this, ChartElements);

			if (!isChanged && !forceRedraw){
				return;
			}
			else{
				this.activeElements = ChartElements;
			}
			this.draw();
			if(this.options.customTooltips){
				this.options.customTooltips(false);
			}
			if (ChartElements.length > 0){
				// If we have multiple datasets, show a MultiTooltip for all of the data points at that index
				if (this.datasets && this.datasets.length > 1) {
					var dataArray,
						dataIndex;

					for (var i = this.datasets.length - 1; i >= 0; i--) {
						dataArray = this.datasets[i].points || this.datasets[i].bars || this.datasets[i].segments;
						dataIndex = indexOf(dataArray, ChartElements[0]);
						if (dataIndex !== -1){
							break;
						}
					}
					var tooltipLabels = [],
						tooltipColors = [],
						medianPosition = (function(index) {

							// Get all the points at that particular index
							var Elements = [],
								dataCollection,
								xPositions = [],
								yPositions = [],
								xMax,
								yMax,
								xMin,
								yMin;
							helpers.each(this.datasets, function(dataset){
								dataCollection = dataset.points || dataset.bars || dataset.segments;
								if (dataCollection[dataIndex] && dataCollection[dataIndex].hasValue()){
									Elements.push(dataCollection[dataIndex]);
								}
							});

							helpers.each(Elements, function(element) {
								xPositions.push(element.x);
								yPositions.push(element.y);


								//Include any colour information about the element
								tooltipLabels.push(helpers.template(this.options.multiTooltipTemplate, element));
								tooltipColors.push({
									fill: element._saved.fillColor || element.fillColor,
									stroke: element._saved.strokeColor || element.strokeColor
								});

							}, this);

							yMin = min(yPositions);
							yMax = max(yPositions);

							xMin = min(xPositions);
							xMax = max(xPositions);

							return {
								x: (xMin > this.chart.width/2) ? xMin : xMax,
								y: (yMin + yMax)/2
							};
						}).call(this, dataIndex);

					new Chart.MultiTooltip({
						x: medianPosition.x,
						y: medianPosition.y,
						xPadding: this.options.tooltipXPadding,
						yPadding: this.options.tooltipYPadding,
						xOffset: this.options.tooltipXOffset,
						fillColor: this.options.tooltipFillColor,
						textColor: this.options.tooltipFontColor,
						fontFamily: this.options.tooltipFontFamily,
						fontStyle: this.options.tooltipFontStyle,
						fontSize: this.options.tooltipFontSize,
						titleTextColor: this.options.tooltipTitleFontColor,
						titleFontFamily: this.options.tooltipTitleFontFamily,
						titleFontStyle: this.options.tooltipTitleFontStyle,
						titleFontSize: this.options.tooltipTitleFontSize,
						cornerRadius: this.options.tooltipCornerRadius,
						labels: tooltipLabels,
						legendColors: tooltipColors,
						legendColorBackground : this.options.multiTooltipKeyBackground,
						title: ChartElements[0].label,
						chart: this.chart,
						ctx: this.chart.ctx,
						custom: this.options.customTooltips
					}).draw();

				} else {
					each(ChartElements, function(Element) {
						var tooltipPosition = Element.tooltipPosition();
						new Chart.Tooltip({
							x: Math.round(tooltipPosition.x),
							y: Math.round(tooltipPosition.y),
							xPadding: this.options.tooltipXPadding,
							yPadding: this.options.tooltipYPadding,
							fillColor: this.options.tooltipFillColor,
							textColor: this.options.tooltipFontColor,
							fontFamily: this.options.tooltipFontFamily,
							fontStyle: this.options.tooltipFontStyle,
							fontSize: this.options.tooltipFontSize,
							caretHeight: this.options.tooltipCaretSize,
							cornerRadius: this.options.tooltipCornerRadius,
							text: template(this.options.tooltipTemplate, Element),
							chart: this.chart,
							custom: this.options.customTooltips
						}).draw();
					}, this);
				}
			}
			return this;
		},
		toBase64Image : function(){
			return this.chart.canvas.toDataURL.apply(this.chart.canvas, arguments);
		}
	});

	Chart.Type.extend = function(extensions){

		var parent = this;

		var ChartType = function(){
			return parent.apply(this,arguments);
		};

		//Copy the prototype object of the this class
		ChartType.prototype = clone(parent.prototype);
		//Now overwrite some of the properties in the base class with the new extensions
		extend(ChartType.prototype, extensions);

		ChartType.extend = Chart.Type.extend;

		if (extensions.name || parent.prototype.name){

			var chartName = extensions.name || parent.prototype.name;
			//Assign any potential default values of the new chart type

			//If none are defined, we'll use a clone of the chart type this is being extended from.
			//I.e. if we extend a line chart, we'll use the defaults from the line chart if our new chart
			//doesn't define some defaults of their own.

			var baseDefaults = (Chart.defaults[parent.prototype.name]) ? clone(Chart.defaults[parent.prototype.name]) : {};

			Chart.defaults[chartName] = extend(baseDefaults,extensions.defaults);

			Chart.types[chartName] = ChartType;

			//Register this new chart type in the Chart prototype
			Chart.prototype[chartName] = function(data,options){
				var config = merge(Chart.defaults.global, Chart.defaults[chartName], options || {});
				return new ChartType(data,config,this);
			};
		} else{
			warn("Name not provided for this chart, so it hasn't been registered");
		}
		return parent;
	};

	Chart.Element = function(configuration){
		extend(this,configuration);
		this.initialize.apply(this,arguments);
		this.save();
	};
	extend(Chart.Element.prototype,{
		initialize : function(){},
		restore : function(props){
			if (!props){
				extend(this,this._saved);
			} else {
				each(props,function(key){
					this[key] = this._saved[key];
				},this);
			}
			return this;
		},
		save : function(){
			this._saved = clone(this);
			delete this._saved._saved;
			return this;
		},
		update : function(newProps){
			each(newProps,function(value,key){
				this._saved[key] = this[key];
				this[key] = value;
			},this);
			return this;
		},
		transition : function(props,ease){
			each(props,function(value,key){
				this[key] = ((value - this._saved[key]) * ease) + this._saved[key];
			},this);
			return this;
		},
		tooltipPosition : function(){
			return {
				x : this.x,
				y : this.y
			};
		},
		hasValue: function(){
			return isNumber(this.value);
		}
	});

	Chart.Element.extend = inherits;


	Chart.Point = Chart.Element.extend({
		display: true,
		inRange: function(chartX,chartY){
			var hitDetectionRange = this.hitDetectionRadius + this.radius;
			return ((Math.pow(chartX-this.x, 2)+Math.pow(chartY-this.y, 2)) < Math.pow(hitDetectionRange,2));
		},
		draw : function(){
			if (this.display){
				var ctx = this.ctx;
				ctx.beginPath();

				ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2);
				ctx.closePath();

				ctx.strokeStyle = this.strokeColor;
				ctx.lineWidth = this.strokeWidth;

				ctx.fillStyle = this.fillColor;

				ctx.fill();
				ctx.stroke();
			}


			//Quick debug for bezier curve splining
			//Highlights control points and the line between them.
			//Handy for dev - stripped in the min version.

			// ctx.save();
			// ctx.fillStyle = "black";
			// ctx.strokeStyle = "black"
			// ctx.beginPath();
			// ctx.arc(this.controlPoints.inner.x,this.controlPoints.inner.y, 2, 0, Math.PI*2);
			// ctx.fill();

			// ctx.beginPath();
			// ctx.arc(this.controlPoints.outer.x,this.controlPoints.outer.y, 2, 0, Math.PI*2);
			// ctx.fill();

			// ctx.moveTo(this.controlPoints.inner.x,this.controlPoints.inner.y);
			// ctx.lineTo(this.x, this.y);
			// ctx.lineTo(this.controlPoints.outer.x,this.controlPoints.outer.y);
			// ctx.stroke();

			// ctx.restore();



		}
	});

	Chart.Arc = Chart.Element.extend({
		inRange : function(chartX,chartY){

			var pointRelativePosition = helpers.getAngleFromPoint(this, {
				x: chartX,
				y: chartY
			});

			//Check if within the range of the open/close angle
			var betweenAngles = (pointRelativePosition.angle >= this.startAngle && pointRelativePosition.angle <= this.endAngle),
				withinRadius = (pointRelativePosition.distance >= this.innerRadius && pointRelativePosition.distance <= this.outerRadius);

			return (betweenAngles && withinRadius);
			//Ensure within the outside of the arc centre, but inside arc outer
		},
		tooltipPosition : function(){
			var centreAngle = this.startAngle + ((this.endAngle - this.startAngle) / 2),
				rangeFromCentre = (this.outerRadius - this.innerRadius) / 2 + this.innerRadius;
			return {
				x : this.x + (Math.cos(centreAngle) * rangeFromCentre),
				y : this.y + (Math.sin(centreAngle) * rangeFromCentre)
			};
		},
		draw : function(animationPercent){

			var easingDecimal = animationPercent || 1;

			var ctx = this.ctx;

			ctx.beginPath();

			ctx.arc(this.x, this.y, this.outerRadius, this.startAngle, this.endAngle);

			ctx.arc(this.x, this.y, this.innerRadius, this.endAngle, this.startAngle, true);

			ctx.closePath();
			ctx.strokeStyle = this.strokeColor;
			ctx.lineWidth = this.strokeWidth;

			ctx.fillStyle = this.fillColor;

			ctx.fill();
			ctx.lineJoin = 'bevel';

			if (this.showStroke){
				ctx.stroke();
			}
		}
	});

	Chart.Rectangle = Chart.Element.extend({
		draw : function(){
			var ctx = this.ctx,
				halfWidth = this.width/2,
				leftX = this.x - halfWidth,
				rightX = this.x + halfWidth,
				top = this.base - (this.base - this.y),
				halfStroke = this.strokeWidth / 2;

			// Canvas doesn't allow us to stroke inside the width so we can
			// adjust the sizes to fit if we're setting a stroke on the line
			if (this.showStroke){
				leftX += halfStroke;
				rightX -= halfStroke;
				top += halfStroke;
			}

			ctx.beginPath();

			ctx.fillStyle = this.fillColor;
			ctx.strokeStyle = this.strokeColor;
			ctx.lineWidth = this.strokeWidth;

			// It'd be nice to keep this class totally generic to any rectangle
			// and simply specify which border to miss out.
			ctx.moveTo(leftX, this.base);
			ctx.lineTo(leftX, top);
			ctx.lineTo(rightX, top);
			ctx.lineTo(rightX, this.base);
			ctx.fill();
			if (this.showStroke){
				ctx.stroke();
			}
		},
		height : function(){
			return this.base - this.y;
		},
		inRange : function(chartX,chartY){
			return (chartX >= this.x - this.width/2 && chartX <= this.x + this.width/2) && (chartY >= this.y && chartY <= this.base);
		}
	});

	Chart.Tooltip = Chart.Element.extend({
		draw : function(){

			var ctx = this.chart.ctx;

			ctx.font = fontString(this.fontSize,this.fontStyle,this.fontFamily);

			this.xAlign = "center";
			this.yAlign = "above";

			//Distance between the actual element.y position and the start of the tooltip caret
			var caretPadding = this.caretPadding = 2;

			var tooltipWidth = ctx.measureText(this.text).width + 2*this.xPadding,
				tooltipRectHeight = this.fontSize + 2*this.yPadding,
				tooltipHeight = tooltipRectHeight + this.caretHeight + caretPadding;

			if (this.x + tooltipWidth/2 >this.chart.width){
				this.xAlign = "left";
			} else if (this.x - tooltipWidth/2 < 0){
				this.xAlign = "right";
			}

			if (this.y - tooltipHeight < 0){
				this.yAlign = "below";
			}


			var tooltipX = this.x - tooltipWidth/2,
				tooltipY = this.y - tooltipHeight;

			ctx.fillStyle = this.fillColor;

			// Custom Tooltips
			if(this.custom){
				this.custom(this);
			}
			else{
				switch(this.yAlign)
				{
				case "above":
					//Draw a caret above the x/y
					ctx.beginPath();
					ctx.moveTo(this.x,this.y - caretPadding);
					ctx.lineTo(this.x + this.caretHeight, this.y - (caretPadding + this.caretHeight));
					ctx.lineTo(this.x - this.caretHeight, this.y - (caretPadding + this.caretHeight));
					ctx.closePath();
					ctx.fill();
					break;
				case "below":
					tooltipY = this.y + caretPadding + this.caretHeight;
					//Draw a caret below the x/y
					ctx.beginPath();
					ctx.moveTo(this.x, this.y + caretPadding);
					ctx.lineTo(this.x + this.caretHeight, this.y + caretPadding + this.caretHeight);
					ctx.lineTo(this.x - this.caretHeight, this.y + caretPadding + this.caretHeight);
					ctx.closePath();
					ctx.fill();
					break;
				}

				switch(this.xAlign)
				{
				case "left":
					tooltipX = this.x - tooltipWidth + (this.cornerRadius + this.caretHeight);
					break;
				case "right":
					tooltipX = this.x - (this.cornerRadius + this.caretHeight);
					break;
				}

				drawRoundedRectangle(ctx,tooltipX,tooltipY,tooltipWidth,tooltipRectHeight,this.cornerRadius);

				ctx.fill();

				ctx.fillStyle = this.textColor;
				ctx.textAlign = "center";
				ctx.textBaseline = "middle";
				ctx.fillText(this.text, tooltipX + tooltipWidth/2, tooltipY + tooltipRectHeight/2);
			}
		}
	});

	Chart.MultiTooltip = Chart.Element.extend({
		initialize : function(){
			this.font = fontString(this.fontSize,this.fontStyle,this.fontFamily);

			this.titleFont = fontString(this.titleFontSize,this.titleFontStyle,this.titleFontFamily);

			this.height = (this.labels.length * this.fontSize) + ((this.labels.length-1) * (this.fontSize/2)) + (this.yPadding*2) + this.titleFontSize *1.5;

			this.ctx.font = this.titleFont;

			var titleWidth = this.ctx.measureText(this.title).width,
				//Label has a legend square as well so account for this.
				labelWidth = longestText(this.ctx,this.font,this.labels) + this.fontSize + 3,
				longestTextWidth = max([labelWidth,titleWidth]);

			this.width = longestTextWidth + (this.xPadding*2);


			var halfHeight = this.height/2;

			//Check to ensure the height will fit on the canvas
			if (this.y - halfHeight < 0 ){
				this.y = halfHeight;
			} else if (this.y + halfHeight > this.chart.height){
				this.y = this.chart.height - halfHeight;
			}

			//Decide whether to align left or right based on position on canvas
			if (this.x > this.chart.width/2){
				this.x -= this.xOffset + this.width;
			} else {
				this.x += this.xOffset;
			}


		},
		getLineHeight : function(index){
			var baseLineHeight = this.y - (this.height/2) + this.yPadding,
				afterTitleIndex = index-1;

			//If the index is zero, we're getting the title
			if (index === 0){
				return baseLineHeight + this.titleFontSize/2;
			} else{
				return baseLineHeight + ((this.fontSize*1.5*afterTitleIndex) + this.fontSize/2) + this.titleFontSize * 1.5;
			}

		},
		draw : function(){
			// Custom Tooltips
			if(this.custom){
				this.custom(this);
			}
			else{
				drawRoundedRectangle(this.ctx,this.x,this.y - this.height/2,this.width,this.height,this.cornerRadius);
				var ctx = this.ctx;
				ctx.fillStyle = this.fillColor;
				ctx.fill();
				ctx.closePath();

				ctx.textAlign = "left";
				ctx.textBaseline = "middle";
				ctx.fillStyle = this.titleTextColor;
				ctx.font = this.titleFont;

				ctx.fillText(this.title,this.x + this.xPadding, this.getLineHeight(0));

				ctx.font = this.font;
				helpers.each(this.labels,function(label,index){
					ctx.fillStyle = this.textColor;
					ctx.fillText(label,this.x + this.xPadding + this.fontSize + 3, this.getLineHeight(index + 1));

					//A bit gnarly, but clearing this rectangle breaks when using explorercanvas (clears whole canvas)
					//ctx.clearRect(this.x + this.xPadding, this.getLineHeight(index + 1) - this.fontSize/2, this.fontSize, this.fontSize);
					//Instead we'll make a white filled block to put the legendColour palette over.

					ctx.fillStyle = this.legendColorBackground;
					ctx.fillRect(this.x + this.xPadding, this.getLineHeight(index + 1) - this.fontSize/2, this.fontSize, this.fontSize);

					ctx.fillStyle = this.legendColors[index].fill;
					ctx.fillRect(this.x + this.xPadding, this.getLineHeight(index + 1) - this.fontSize/2, this.fontSize, this.fontSize);


				},this);
			}
		}
	});

	Chart.Scale = Chart.Element.extend({
		initialize : function(){
			this.fit();
		},
		buildYLabels : function(){
			this.yLabels = [];

			var stepDecimalPlaces = getDecimalPlaces(this.stepValue);

			for (var i=0; i<=this.steps; i++){
				this.yLabels.push(template(this.templateString,{value:(this.min + (i * this.stepValue)).toFixed(stepDecimalPlaces)}));
			}
			this.yLabelWidth = (this.display && this.showLabels) ? longestText(this.ctx,this.font,this.yLabels) : 0;
		},
		addXLabel : function(label){
			this.xLabels.push(label);
			this.valuesCount++;
			this.fit();
		},
		removeXLabel : function(){
			this.xLabels.shift();
			this.valuesCount--;
			this.fit();
		},
		// Fitting loop to rotate x Labels and figure out what fits there, and also calculate how many Y steps to use
		fit: function(){
			// First we need the width of the yLabels, assuming the xLabels aren't rotated

			// To do that we need the base line at the top and base of the chart, assuming there is no x label rotation
			this.startPoint = (this.display) ? this.fontSize : 0;
			this.endPoint = (this.display) ? this.height - (this.fontSize * 1.5) - 5 : this.height; // -5 to pad labels

			// Apply padding settings to the start and end point.
			this.startPoint += this.padding;
			this.endPoint -= this.padding;

			// Cache the starting height, so can determine if we need to recalculate the scale yAxis
			var cachedHeight = this.endPoint - this.startPoint,
				cachedYLabelWidth;

			// Build the current yLabels so we have an idea of what size they'll be to start
			/*
			 *	This sets what is returned from calculateScaleRange as static properties of this class:
			 *
				this.steps;
				this.stepValue;
				this.min;
				this.max;
			 *
			 */
			this.calculateYRange(cachedHeight);

			// With these properties set we can now build the array of yLabels
			// and also the width of the largest yLabel
			this.buildYLabels();

			this.calculateXLabelRotation();

			while((cachedHeight > this.endPoint - this.startPoint)){
				cachedHeight = this.endPoint - this.startPoint;
				cachedYLabelWidth = this.yLabelWidth;

				this.calculateYRange(cachedHeight);
				this.buildYLabels();

				// Only go through the xLabel loop again if the yLabel width has changed
				if (cachedYLabelWidth < this.yLabelWidth){
					this.calculateXLabelRotation();
				}
			}

		},
		calculateXLabelRotation : function(){
			//Get the width of each grid by calculating the difference
			//between x offsets between 0 and 1.

			this.ctx.font = this.font;

			var firstWidth = this.ctx.measureText(this.xLabels[0]).width,
				lastWidth = this.ctx.measureText(this.xLabels[this.xLabels.length - 1]).width,
				firstRotated,
				lastRotated;


			this.xScalePaddingRight = lastWidth/2 + 3;
			this.xScalePaddingLeft = (firstWidth/2 > this.yLabelWidth + 10) ? firstWidth/2 : this.yLabelWidth + 10;

			this.xLabelRotation = 0;
			if (this.display){
				var originalLabelWidth = longestText(this.ctx,this.font,this.xLabels),
					cosRotation,
					firstRotatedWidth;
				this.xLabelWidth = originalLabelWidth;
				//Allow 3 pixels x2 padding either side for label readability
				var xGridWidth = Math.floor(this.calculateX(1) - this.calculateX(0)) - 6;

				//Max label rotate should be 90 - also act as a loop counter
				while ((this.xLabelWidth > xGridWidth && this.xLabelRotation === 0) || (this.xLabelWidth > xGridWidth && this.xLabelRotation <= 90 && this.xLabelRotation > 0)){
					cosRotation = Math.cos(toRadians(this.xLabelRotation));

					firstRotated = cosRotation * firstWidth;
					lastRotated = cosRotation * lastWidth;

					// We're right aligning the text now.
					if (firstRotated + this.fontSize / 2 > this.yLabelWidth + 8){
						this.xScalePaddingLeft = firstRotated + this.fontSize / 2;
					}
					this.xScalePaddingRight = this.fontSize/2;


					this.xLabelRotation++;
					this.xLabelWidth = cosRotation * originalLabelWidth;

				}
				if (this.xLabelRotation > 0){
					this.endPoint -= Math.sin(toRadians(this.xLabelRotation))*originalLabelWidth + 3;
				}
			}
			else{
				this.xLabelWidth = 0;
				this.xScalePaddingRight = this.padding;
				this.xScalePaddingLeft = this.padding;
			}

		},
		// Needs to be overidden in each Chart type
		// Otherwise we need to pass all the data into the scale class
		calculateYRange: noop,
		drawingArea: function(){
			return this.startPoint - this.endPoint;
		},
		calculateY : function(value){
			var scalingFactor = this.drawingArea() / (this.min - this.max);
			return this.endPoint - (scalingFactor * (value - this.min));
		},
		calculateX : function(index){
			var isRotated = (this.xLabelRotation > 0),
				// innerWidth = (this.offsetGridLines) ? this.width - offsetLeft - this.padding : this.width - (offsetLeft + halfLabelWidth * 2) - this.padding,
				innerWidth = this.width - (this.xScalePaddingLeft + this.xScalePaddingRight),
				valueWidth = innerWidth/Math.max((this.valuesCount - ((this.offsetGridLines) ? 0 : 1)), 1),
				valueOffset = (valueWidth * index) + this.xScalePaddingLeft;

			if (this.offsetGridLines){
				valueOffset += (valueWidth/2);
			}

			return Math.round(valueOffset);
		},
		update : function(newProps){
			helpers.extend(this, newProps);
			this.fit();
		},
		draw : function(){
			var ctx = this.ctx,
				yLabelGap = (this.endPoint - this.startPoint) / this.steps,
				xStart = Math.round(this.xScalePaddingLeft);
			if (this.display){
				ctx.fillStyle = this.textColor;
				ctx.font = this.font;
				each(this.yLabels,function(labelString,index){
					var yLabelCenter = this.endPoint - (yLabelGap * index),
						linePositionY = Math.round(yLabelCenter),
						drawHorizontalLine = this.showHorizontalLines;

					ctx.textAlign = "right";
					ctx.textBaseline = "middle";
					if (this.showLabels){
						ctx.fillText(labelString,xStart - 10,yLabelCenter);
					}

					// This is X axis, so draw it
					if (index === 0 && !drawHorizontalLine){
						drawHorizontalLine = true;
					}

					if (drawHorizontalLine){
						ctx.beginPath();
					}

					if (index > 0){
						// This is a grid line in the centre, so drop that
						ctx.lineWidth = this.gridLineWidth;
						ctx.strokeStyle = this.gridLineColor;
					} else {
						// This is the first line on the scale
						ctx.lineWidth = this.lineWidth;
						ctx.strokeStyle = this.lineColor;
					}

					linePositionY += helpers.aliasPixel(ctx.lineWidth);

					if(drawHorizontalLine){
						ctx.moveTo(xStart, linePositionY);
						ctx.lineTo(this.width, linePositionY);
						ctx.stroke();
						ctx.closePath();
					}

					ctx.lineWidth = this.lineWidth;
					ctx.strokeStyle = this.lineColor;
					ctx.beginPath();
					ctx.moveTo(xStart - 5, linePositionY);
					ctx.lineTo(xStart, linePositionY);
					ctx.stroke();
					ctx.closePath();

				},this);

				each(this.xLabels,function(label,index){
					var xPos = this.calculateX(index) + aliasPixel(this.lineWidth),
						// Check to see if line/bar here and decide where to place the line
						linePos = this.calculateX(index - (this.offsetGridLines ? 0.5 : 0)) + aliasPixel(this.lineWidth),
						isRotated = (this.xLabelRotation > 0),
						drawVerticalLine = this.showVerticalLines;

					// This is Y axis, so draw it
					if (index === 0 && !drawVerticalLine){
						drawVerticalLine = true;
					}

					if (drawVerticalLine){
						ctx.beginPath();
					}

					if (index > 0){
						// This is a grid line in the centre, so drop that
						ctx.lineWidth = this.gridLineWidth;
						ctx.strokeStyle = this.gridLineColor;
					} else {
						// This is the first line on the scale
						ctx.lineWidth = this.lineWidth;
						ctx.strokeStyle = this.lineColor;
					}

					if (drawVerticalLine){
						ctx.moveTo(linePos,this.endPoint);
						ctx.lineTo(linePos,this.startPoint - 3);
						ctx.stroke();
						ctx.closePath();
					}


					ctx.lineWidth = this.lineWidth;
					ctx.strokeStyle = this.lineColor;


					// Small lines at the bottom of the base grid line
					ctx.beginPath();
					ctx.moveTo(linePos,this.endPoint);
					ctx.lineTo(linePos,this.endPoint + 5);
					ctx.stroke();
					ctx.closePath();

					ctx.save();
					ctx.translate(xPos,(isRotated) ? this.endPoint + 12 : this.endPoint + 8);
					ctx.rotate(toRadians(this.xLabelRotation)*-1);
					ctx.font = this.font;
					ctx.textAlign = (isRotated) ? "right" : "center";
					ctx.textBaseline = (isRotated) ? "middle" : "top";
					ctx.fillText(label, 0, 0);
					ctx.restore();
				},this);

			}
		}

	});

	Chart.RadialScale = Chart.Element.extend({
		initialize: function(){
			this.size = min([this.height, this.width]);
			this.drawingArea = (this.display) ? (this.size/2) - (this.fontSize/2 + this.backdropPaddingY) : (this.size/2);
		},
		calculateCenterOffset: function(value){
			// Take into account half font size + the yPadding of the top value
			var scalingFactor = this.drawingArea / (this.max - this.min);

			return (value - this.min) * scalingFactor;
		},
		update : function(){
			if (!this.lineArc){
				this.setScaleSize();
			} else {
				this.drawingArea = (this.display) ? (this.size/2) - (this.fontSize/2 + this.backdropPaddingY) : (this.size/2);
			}
			this.buildYLabels();
		},
		buildYLabels: function(){
			this.yLabels = [];

			var stepDecimalPlaces = getDecimalPlaces(this.stepValue);

			for (var i=0; i<=this.steps; i++){
				this.yLabels.push(template(this.templateString,{value:(this.min + (i * this.stepValue)).toFixed(stepDecimalPlaces)}));
			}
		},
		getCircumference : function(){
			return ((Math.PI*2) / this.valuesCount);
		},
		setScaleSize: function(){
			/*
			 * Right, this is really confusing and there is a lot of maths going on here
			 * The gist of the problem is here: https://gist.github.com/nnnick/696cc9c55f4b0beb8fe9
			 *
			 * Reaction: https://dl.dropboxusercontent.com/u/34601363/toomuchscience.gif
			 *
			 * Solution:
			 *
			 * We assume the radius of the polygon is half the size of the canvas at first
			 * at each index we check if the text overlaps.
			 *
			 * Where it does, we store that angle and that index.
			 *
			 * After finding the largest index and angle we calculate how much we need to remove
			 * from the shape radius to move the point inwards by that x.
			 *
			 * We average the left and right distances to get the maximum shape radius that can fit in the box
			 * along with labels.
			 *
			 * Once we have that, we can find the centre point for the chart, by taking the x text protrusion
			 * on each side, removing that from the size, halving it and adding the left x protrusion width.
			 *
			 * This will mean we have a shape fitted to the canvas, as large as it can be with the labels
			 * and position it in the most space efficient manner
			 *
			 * https://dl.dropboxusercontent.com/u/34601363/yeahscience.gif
			 */


			// Get maximum radius of the polygon. Either half the height (minus the text width) or half the width.
			// Use this to calculate the offset + change. - Make sure L/R protrusion is at least 0 to stop issues with centre points
			var largestPossibleRadius = min([(this.height/2 - this.pointLabelFontSize - 5), this.width/2]),
				pointPosition,
				i,
				textWidth,
				halfTextWidth,
				furthestRight = this.width,
				furthestRightIndex,
				furthestRightAngle,
				furthestLeft = 0,
				furthestLeftIndex,
				furthestLeftAngle,
				xProtrusionLeft,
				xProtrusionRight,
				radiusReductionRight,
				radiusReductionLeft,
				maxWidthRadius;
			this.ctx.font = fontString(this.pointLabelFontSize,this.pointLabelFontStyle,this.pointLabelFontFamily);
			for (i=0;i<this.valuesCount;i++){
				// 5px to space the text slightly out - similar to what we do in the draw function.
				pointPosition = this.getPointPosition(i, largestPossibleRadius);
				textWidth = this.ctx.measureText(template(this.templateString, { value: this.labels[i] })).width + 5;
				if (i === 0 || i === this.valuesCount/2){
					// If we're at index zero, or exactly the middle, we're at exactly the top/bottom
					// of the radar chart, so text will be aligned centrally, so we'll half it and compare
					// w/left and right text sizes
					halfTextWidth = textWidth/2;
					if (pointPosition.x + halfTextWidth > furthestRight) {
						furthestRight = pointPosition.x + halfTextWidth;
						furthestRightIndex = i;
					}
					if (pointPosition.x - halfTextWidth < furthestLeft) {
						furthestLeft = pointPosition.x - halfTextWidth;
						furthestLeftIndex = i;
					}
				}
				else if (i < this.valuesCount/2) {
					// Less than half the values means we'll left align the text
					if (pointPosition.x + textWidth > furthestRight) {
						furthestRight = pointPosition.x + textWidth;
						furthestRightIndex = i;
					}
				}
				else if (i > this.valuesCount/2){
					// More than half the values means we'll right align the text
					if (pointPosition.x - textWidth < furthestLeft) {
						furthestLeft = pointPosition.x - textWidth;
						furthestLeftIndex = i;
					}
				}
			}

			xProtrusionLeft = furthestLeft;

			xProtrusionRight = Math.ceil(furthestRight - this.width);

			furthestRightAngle = this.getIndexAngle(furthestRightIndex);

			furthestLeftAngle = this.getIndexAngle(furthestLeftIndex);

			radiusReductionRight = xProtrusionRight / Math.sin(furthestRightAngle + Math.PI/2);

			radiusReductionLeft = xProtrusionLeft / Math.sin(furthestLeftAngle + Math.PI/2);

			// Ensure we actually need to reduce the size of the chart
			radiusReductionRight = (isNumber(radiusReductionRight)) ? radiusReductionRight : 0;
			radiusReductionLeft = (isNumber(radiusReductionLeft)) ? radiusReductionLeft : 0;

			this.drawingArea = largestPossibleRadius - (radiusReductionLeft + radiusReductionRight)/2;

			//this.drawingArea = min([maxWidthRadius, (this.height - (2 * (this.pointLabelFontSize + 5)))/2])
			this.setCenterPoint(radiusReductionLeft, radiusReductionRight);

		},
		setCenterPoint: function(leftMovement, rightMovement){

			var maxRight = this.width - rightMovement - this.drawingArea,
				maxLeft = leftMovement + this.drawingArea;

			this.xCenter = (maxLeft + maxRight)/2;
			// Always vertically in the centre as the text height doesn't change
			this.yCenter = (this.height/2);
		},

		getIndexAngle : function(index){
			var angleMultiplier = (Math.PI * 2) / this.valuesCount;
			// Start from the top instead of right, so remove a quarter of the circle

			return index * angleMultiplier - (Math.PI/2);
		},
		getPointPosition : function(index, distanceFromCenter){
			var thisAngle = this.getIndexAngle(index);
			return {
				x : (Math.cos(thisAngle) * distanceFromCenter) + this.xCenter,
				y : (Math.sin(thisAngle) * distanceFromCenter) + this.yCenter
			};
		},
		draw: function(){
			if (this.display){
				var ctx = this.ctx;
				each(this.yLabels, function(label, index){
					// Don't draw a centre value
					if (index > 0){
						var yCenterOffset = index * (this.drawingArea/this.steps),
							yHeight = this.yCenter - yCenterOffset,
							pointPosition;

						// Draw circular lines around the scale
						if (this.lineWidth > 0){
							ctx.strokeStyle = this.lineColor;
							ctx.lineWidth = this.lineWidth;

							if(this.lineArc){
								ctx.beginPath();
								ctx.arc(this.xCenter, this.yCenter, yCenterOffset, 0, Math.PI*2);
								ctx.closePath();
								ctx.stroke();
							} else{
								ctx.beginPath();
								for (var i=0;i<this.valuesCount;i++)
								{
									pointPosition = this.getPointPosition(i, this.calculateCenterOffset(this.min + (index * this.stepValue)));
									if (i === 0){
										ctx.moveTo(pointPosition.x, pointPosition.y);
									} else {
										ctx.lineTo(pointPosition.x, pointPosition.y);
									}
								}
								ctx.closePath();
								ctx.stroke();
							}
						}
						if(this.showLabels){
							ctx.font = fontString(this.fontSize,this.fontStyle,this.fontFamily);
							if (this.showLabelBackdrop){
								var labelWidth = ctx.measureText(label).width;
								ctx.fillStyle = this.backdropColor;
								ctx.fillRect(
									this.xCenter - labelWidth/2 - this.backdropPaddingX,
									yHeight - this.fontSize/2 - this.backdropPaddingY,
									labelWidth + this.backdropPaddingX*2,
									this.fontSize + this.backdropPaddingY*2
								);
							}
							ctx.textAlign = 'center';
							ctx.textBaseline = "middle";
							ctx.fillStyle = this.fontColor;
							ctx.fillText(label, this.xCenter, yHeight);
						}
					}
				}, this);

				if (!this.lineArc){
					ctx.lineWidth = this.angleLineWidth;
					ctx.strokeStyle = this.angleLineColor;
					for (var i = this.valuesCount - 1; i >= 0; i--) {
						if (this.angleLineWidth > 0){
							var outerPosition = this.getPointPosition(i, this.calculateCenterOffset(this.max));
							ctx.beginPath();
							ctx.moveTo(this.xCenter, this.yCenter);
							ctx.lineTo(outerPosition.x, outerPosition.y);
							ctx.stroke();
							ctx.closePath();
						}
						// Extra 3px out for some label spacing
						var pointLabelPosition = this.getPointPosition(i, this.calculateCenterOffset(this.max) + 5);
						ctx.font = fontString(this.pointLabelFontSize,this.pointLabelFontStyle,this.pointLabelFontFamily);
						ctx.fillStyle = this.pointLabelFontColor;

						var labelsCount = this.labels.length,
							halfLabelsCount = this.labels.length/2,
							quarterLabelsCount = halfLabelsCount/2,
							upperHalf = (i < quarterLabelsCount || i > labelsCount - quarterLabelsCount),
							exactQuarter = (i === quarterLabelsCount || i === labelsCount - quarterLabelsCount);
						if (i === 0){
							ctx.textAlign = 'center';
						} else if(i === halfLabelsCount){
							ctx.textAlign = 'center';
						} else if (i < halfLabelsCount){
							ctx.textAlign = 'left';
						} else {
							ctx.textAlign = 'right';
						}

						// Set the correct text baseline based on outer positioning
						if (exactQuarter){
							ctx.textBaseline = 'middle';
						} else if (upperHalf){
							ctx.textBaseline = 'bottom';
						} else {
							ctx.textBaseline = 'top';
						}

						ctx.fillText(this.labels[i], pointLabelPosition.x, pointLabelPosition.y);
					}
				}
			}
		}
	});

	// Attach global event to resize each chart instance when the browser resizes
	helpers.addEvent(window, "resize", (function(){
		// Basic debounce of resize function so it doesn't hurt performance when resizing browser.
		var timeout;
		return function(){
			clearTimeout(timeout);
			timeout = setTimeout(function(){
				each(Chart.instances,function(instance){
					// If the responsive flag is set in the chart instance config
					// Cascade the resize event down to the chart.
					if (instance.options.responsive){
						instance.resize(instance.render, true);
					}
				});
			}, 50);
		};
	})());


	if (amd) {
		define(function(){
			return Chart;
		});
	} else if (typeof module === 'object' && module.exports) {
		module.exports = Chart;
	}

	root.Chart = Chart;

	Chart.noConflict = function(){
		root.Chart = previous;
		return Chart;
	};

}).call(this);

(function(){
	"use strict";

	var root = this,
		Chart = root.Chart,
		helpers = Chart.helpers;


	var defaultConfig = {
		//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
		scaleBeginAtZero : true,

		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines : true,

		//String - Colour of the grid lines
		scaleGridLineColor : "rgba(0,0,0,.05)",

		//Number - Width of the grid lines
		scaleGridLineWidth : 1,

		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines: true,

		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines: true,

		//Boolean - If there is a stroke on each bar
		barShowStroke : true,

		//Number - Pixel width of the bar stroke
		barStrokeWidth : 2,

		//Number - Spacing between each of the X value sets
		barValueSpacing : 5,

		//Number - Spacing between data sets within X values
		barDatasetSpacing : 1,

		//String - A legend template
		legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

	};


	Chart.Type.extend({
		name: "Bar",
		defaults : defaultConfig,
		initialize:  function(data){

			//Expose options as a scope variable here so we can access it in the ScaleClass
			var options = this.options;

			this.ScaleClass = Chart.Scale.extend({
				offsetGridLines : true,
				calculateBarX : function(datasetCount, datasetIndex, barIndex){
					//Reusable method for calculating the xPosition of a given bar based on datasetIndex & width of the bar
					var xWidth = this.calculateBaseWidth(),
						xAbsolute = this.calculateX(barIndex) - (xWidth/2),
						barWidth = this.calculateBarWidth(datasetCount);

					return xAbsolute + (barWidth * datasetIndex) + (datasetIndex * options.barDatasetSpacing) + barWidth/2;
				},
				calculateBaseWidth : function(){
					return (this.calculateX(1) - this.calculateX(0)) - (2*options.barValueSpacing);
				},
				calculateBarWidth : function(datasetCount){
					//The padding between datasets is to the right of each bar, providing that there are more than 1 dataset
					var baseWidth = this.calculateBaseWidth() - ((datasetCount - 1) * options.barDatasetSpacing);

					return (baseWidth / datasetCount);
				}
			});

			this.datasets = [];

			//Set up tooltip events on the chart
			if (this.options.showTooltips){
				helpers.bindEvents(this, this.options.tooltipEvents, function(evt){
					var activeBars = (evt.type !== 'mouseout') ? this.getBarsAtEvent(evt) : [];

					this.eachBars(function(bar){
						bar.restore(['fillColor', 'strokeColor']);
					});
					helpers.each(activeBars, function(activeBar){
						activeBar.fillColor = activeBar.highlightFill;
						activeBar.strokeColor = activeBar.highlightStroke;
					});
					this.showTooltip(activeBars);
				});
			}

			//Declare the extension of the default point, to cater for the options passed in to the constructor
			this.BarClass = Chart.Rectangle.extend({
				strokeWidth : this.options.barStrokeWidth,
				showStroke : this.options.barShowStroke,
				ctx : this.chart.ctx
			});

			//Iterate through each of the datasets, and build this into a property of the chart
			helpers.each(data.datasets,function(dataset,datasetIndex){

				var datasetObject = {
					label : dataset.label || null,
					fillColor : dataset.fillColor,
					strokeColor : dataset.strokeColor,
					bars : []
				};

				this.datasets.push(datasetObject);

				helpers.each(dataset.data,function(dataPoint,index){
					//Add a new point for each piece of data, passing any required data to draw.
					datasetObject.bars.push(new this.BarClass({
						value : dataPoint,
						label : data.labels[index],
						datasetLabel: dataset.label,
						strokeColor : dataset.strokeColor,
						fillColor : dataset.fillColor,
						highlightFill : dataset.highlightFill || dataset.fillColor,
						highlightStroke : dataset.highlightStroke || dataset.strokeColor
					}));
				},this);

			},this);

			this.buildScale(data.labels);

			this.BarClass.prototype.base = this.scale.endPoint;

			this.eachBars(function(bar, index, datasetIndex){
				helpers.extend(bar, {
					width : this.scale.calculateBarWidth(this.datasets.length),
					x: this.scale.calculateBarX(this.datasets.length, datasetIndex, index),
					y: this.scale.endPoint
				});
				bar.save();
			}, this);

			this.render();
		},
		update : function(){
			this.scale.update();
			// Reset any highlight colours before updating.
			helpers.each(this.activeElements, function(activeElement){
				activeElement.restore(['fillColor', 'strokeColor']);
			});

			this.eachBars(function(bar){
				bar.save();
			});
			this.render();
		},
		eachBars : function(callback){
			helpers.each(this.datasets,function(dataset, datasetIndex){
				helpers.each(dataset.bars, callback, this, datasetIndex);
			},this);
		},
		getBarsAtEvent : function(e){
			var barsArray = [],
				eventPosition = helpers.getRelativePosition(e),
				datasetIterator = function(dataset){
					barsArray.push(dataset.bars[barIndex]);
				},
				barIndex;

			for (var datasetIndex = 0; datasetIndex < this.datasets.length; datasetIndex++) {
				for (barIndex = 0; barIndex < this.datasets[datasetIndex].bars.length; barIndex++) {
					if (this.datasets[datasetIndex].bars[barIndex].inRange(eventPosition.x,eventPosition.y)){
						helpers.each(this.datasets, datasetIterator);
						return barsArray;
					}
				}
			}

			return barsArray;
		},
		buildScale : function(labels){
			var self = this;

			var dataTotal = function(){
				var values = [];
				self.eachBars(function(bar){
					values.push(bar.value);
				});
				return values;
			};

			var scaleOptions = {
				templateString : this.options.scaleLabel,
				height : this.chart.height,
				width : this.chart.width,
				ctx : this.chart.ctx,
				textColor : this.options.scaleFontColor,
				fontSize : this.options.scaleFontSize,
				fontStyle : this.options.scaleFontStyle,
				fontFamily : this.options.scaleFontFamily,
				valuesCount : labels.length,
				beginAtZero : this.options.scaleBeginAtZero,
				integersOnly : this.options.scaleIntegersOnly,
				calculateYRange: function(currentHeight){
					var updatedRanges = helpers.calculateScaleRange(
						dataTotal(),
						currentHeight,
						this.fontSize,
						this.beginAtZero,
						this.integersOnly
					);
					helpers.extend(this, updatedRanges);
				},
				xLabels : labels,
				font : helpers.fontString(this.options.scaleFontSize, this.options.scaleFontStyle, this.options.scaleFontFamily),
				lineWidth : this.options.scaleLineWidth,
				lineColor : this.options.scaleLineColor,
				showHorizontalLines : this.options.scaleShowHorizontalLines,
				showVerticalLines : this.options.scaleShowVerticalLines,
				gridLineWidth : (this.options.scaleShowGridLines) ? this.options.scaleGridLineWidth : 0,
				gridLineColor : (this.options.scaleShowGridLines) ? this.options.scaleGridLineColor : "rgba(0,0,0,0)",
				padding : (this.options.showScale) ? 0 : (this.options.barShowStroke) ? this.options.barStrokeWidth : 0,
				showLabels : this.options.scaleShowLabels,
				display : this.options.showScale
			};

			if (this.options.scaleOverride){
				helpers.extend(scaleOptions, {
					calculateYRange: helpers.noop,
					steps: this.options.scaleSteps,
					stepValue: this.options.scaleStepWidth,
					min: this.options.scaleStartValue,
					max: this.options.scaleStartValue + (this.options.scaleSteps * this.options.scaleStepWidth)
				});
			}

			this.scale = new this.ScaleClass(scaleOptions);
		},
		addData : function(valuesArray,label){
			//Map the values array for each of the datasets
			helpers.each(valuesArray,function(value,datasetIndex){
				//Add a new point for each piece of data, passing any required data to draw.
				this.datasets[datasetIndex].bars.push(new this.BarClass({
					value : value,
					label : label,
					x: this.scale.calculateBarX(this.datasets.length, datasetIndex, this.scale.valuesCount+1),
					y: this.scale.endPoint,
					width : this.scale.calculateBarWidth(this.datasets.length),
					base : this.scale.endPoint,
					strokeColor : this.datasets[datasetIndex].strokeColor,
					fillColor : this.datasets[datasetIndex].fillColor
				}));
			},this);

			this.scale.addXLabel(label);
			//Then re-render the chart.
			this.update();
		},
		removeData : function(){
			this.scale.removeXLabel();
			//Then re-render the chart.
			helpers.each(this.datasets,function(dataset){
				dataset.bars.shift();
			},this);
			this.update();
		},
		reflow : function(){
			helpers.extend(this.BarClass.prototype,{
				y: this.scale.endPoint,
				base : this.scale.endPoint
			});
			var newScaleProps = helpers.extend({
				height : this.chart.height,
				width : this.chart.width
			});
			this.scale.update(newScaleProps);
		},
		draw : function(ease){
			var easingDecimal = ease || 1;
			this.clear();

			var ctx = this.chart.ctx;

			this.scale.draw(easingDecimal);

			//Draw all the bars for each dataset
			helpers.each(this.datasets,function(dataset,datasetIndex){
				helpers.each(dataset.bars,function(bar,index){
					if (bar.hasValue()){
						bar.base = this.scale.endPoint;
						//Transition then draw
						bar.transition({
							x : this.scale.calculateBarX(this.datasets.length, datasetIndex, index),
							y : this.scale.calculateY(bar.value),
							width : this.scale.calculateBarWidth(this.datasets.length)
						}, easingDecimal).draw();
					}
				},this);

			},this);
		}
	});


}).call(this);

(function(){
	"use strict";

	var root = this,
		Chart = root.Chart,
		//Cache a local reference to Chart.helpers
		helpers = Chart.helpers;

	var defaultConfig = {
		//Boolean - Whether we should show a stroke on each segment
		segmentShowStroke : true,

		//String - The colour of each segment stroke
		segmentStrokeColor : "#fff",

		//Number - The width of each segment stroke
		segmentStrokeWidth : 2,

		//The percentage of the chart that we cut out of the middle.
		percentageInnerCutout : 50,

		//Number - Amount of animation steps
		animationSteps : 100,

		//String - Animation easing effect
		animationEasing : "easeOutBounce",

		//Boolean - Whether we animate the rotation of the Doughnut
		animateRotate : true,

		//Boolean - Whether we animate scaling the Doughnut from the centre
		animateScale : false,

		//String - A legend template
		legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"

	};


	Chart.Type.extend({
		//Passing in a name registers this chart in the Chart namespace
		name: "Doughnut",
		//Providing a defaults will also register the deafults in the chart namespace
		defaults : defaultConfig,
		//Initialize is fired when the chart is initialized - Data is passed in as a parameter
		//Config is automatically merged by the core of Chart.js, and is available at this.options
		initialize:  function(data){

			//Declare segments as a static property to prevent inheriting across the Chart type prototype
			this.segments = [];
			this.outerRadius = (helpers.min([this.chart.width,this.chart.height]) -	this.options.segmentStrokeWidth/2)/2;

			this.SegmentArc = Chart.Arc.extend({
				ctx : this.chart.ctx,
				x : this.chart.width/2,
				y : this.chart.height/2
			});

			//Set up tooltip events on the chart
			if (this.options.showTooltips){
				helpers.bindEvents(this, this.options.tooltipEvents, function(evt){
					var activeSegments = (evt.type !== 'mouseout') ? this.getSegmentsAtEvent(evt) : [];

					helpers.each(this.segments,function(segment){
						segment.restore(["fillColor"]);
					});
					helpers.each(activeSegments,function(activeSegment){
						activeSegment.fillColor = activeSegment.highlightColor;
					});
					this.showTooltip(activeSegments);
				});
			}
			this.calculateTotal(data);

			helpers.each(data,function(datapoint, index){
				this.addData(datapoint, index, true);
			},this);

			this.render();
		},
		getSegmentsAtEvent : function(e){
			var segmentsArray = [];

			var location = helpers.getRelativePosition(e);

			helpers.each(this.segments,function(segment){
				if (segment.inRange(location.x,location.y)) segmentsArray.push(segment);
			},this);
			return segmentsArray;
		},
		addData : function(segment, atIndex, silent){
			var index = atIndex || this.segments.length;
			this.segments.splice(index, 0, new this.SegmentArc({
				value : segment.value,
				outerRadius : (this.options.animateScale) ? 0 : this.outerRadius,
				innerRadius : (this.options.animateScale) ? 0 : (this.outerRadius/100) * this.options.percentageInnerCutout,
				fillColor : segment.color,
				highlightColor : segment.highlight || segment.color,
				showStroke : this.options.segmentShowStroke,
				strokeWidth : this.options.segmentStrokeWidth,
				strokeColor : this.options.segmentStrokeColor,
				startAngle : Math.PI * 1.5,
				circumference : (this.options.animateRotate) ? 0 : this.calculateCircumference(segment.value),
				label : segment.label
			}));
			if (!silent){
				this.reflow();
				this.update();
			}
		},
		calculateCircumference : function(value){
			return (Math.PI*2)*(Math.abs(value) / this.total);
		},
		calculateTotal : function(data){
			this.total = 0;
			helpers.each(data,function(segment){
				this.total += Math.abs(segment.value);
			},this);
		},
		update : function(){
			this.calculateTotal(this.segments);

			// Reset any highlight colours before updating.
			helpers.each(this.activeElements, function(activeElement){
				activeElement.restore(['fillColor']);
			});

			helpers.each(this.segments,function(segment){
				segment.save();
			});
			this.render();
		},

		removeData: function(atIndex){
			var indexToDelete = (helpers.isNumber(atIndex)) ? atIndex : this.segments.length-1;
			this.segments.splice(indexToDelete, 1);
			this.reflow();
			this.update();
		},

		reflow : function(){
			helpers.extend(this.SegmentArc.prototype,{
				x : this.chart.width/2,
				y : this.chart.height/2
			});
			this.outerRadius = (helpers.min([this.chart.width,this.chart.height]) -	this.options.segmentStrokeWidth/2)/2;
			helpers.each(this.segments, function(segment){
				segment.update({
					outerRadius : this.outerRadius,
					innerRadius : (this.outerRadius/100) * this.options.percentageInnerCutout
				});
			}, this);
		},
		draw : function(easeDecimal){
			var animDecimal = (easeDecimal) ? easeDecimal : 1;
			this.clear();
			helpers.each(this.segments,function(segment,index){
				segment.transition({
					circumference : this.calculateCircumference(segment.value),
					outerRadius : this.outerRadius,
					innerRadius : (this.outerRadius/100) * this.options.percentageInnerCutout
				},animDecimal);

				segment.endAngle = segment.startAngle + segment.circumference;

				segment.draw();
				if (index === 0){
					segment.startAngle = Math.PI * 1.5;
				}
				//Check to see if it's the last segment, if not get the next and update the start angle
				if (index < this.segments.length-1){
					this.segments[index+1].startAngle = segment.endAngle;
				}
			},this);

		}
	});

	Chart.types.Doughnut.extend({
		name : "Pie",
		defaults : helpers.merge(defaultConfig,{percentageInnerCutout : 0})
	});

}).call(this);
(function(){
	"use strict";

	var root = this,
		Chart = root.Chart,
		helpers = Chart.helpers;

	var defaultConfig = {

		///Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines : true,

		//String - Colour of the grid lines
		scaleGridLineColor : "rgba(0,0,0,.05)",

		//Number - Width of the grid lines
		scaleGridLineWidth : 1,

		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines: true,

		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines: true,

		//Boolean - Whether the line is curved between points
		bezierCurve : true,

		//Number - Tension of the bezier curve between points
		bezierCurveTension : 0.4,

		//Boolean - Whether to show a dot for each point
		pointDot : true,

		//Number - Radius of each point dot in pixels
		pointDotRadius : 4,

		//Number - Pixel width of point dot stroke
		pointDotStrokeWidth : 1,

		//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
		pointHitDetectionRadius : 20,

		//Boolean - Whether to show a stroke for datasets
		datasetStroke : true,

		//Number - Pixel width of dataset stroke
		datasetStrokeWidth : 2,

		//Boolean - Whether to fill the dataset with a colour
		datasetFill : true,

		//String - A legend template
		legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

	};


	Chart.Type.extend({
		name: "Line",
		defaults : defaultConfig,
		initialize:  function(data){
			//Declare the extension of the default point, to cater for the options passed in to the constructor
			this.PointClass = Chart.Point.extend({
				strokeWidth : this.options.pointDotStrokeWidth,
				radius : this.options.pointDotRadius,
				display: this.options.pointDot,
				hitDetectionRadius : this.options.pointHitDetectionRadius,
				ctx : this.chart.ctx,
				inRange : function(mouseX){
					return (Math.pow(mouseX-this.x, 2) < Math.pow(this.radius + this.hitDetectionRadius,2));
				}
			});

			this.datasets = [];

			//Set up tooltip events on the chart
			if (this.options.showTooltips){
				helpers.bindEvents(this, this.options.tooltipEvents, function(evt){
					var activePoints = (evt.type !== 'mouseout') ? this.getPointsAtEvent(evt) : [];
					this.eachPoints(function(point){
						point.restore(['fillColor', 'strokeColor']);
					});
					helpers.each(activePoints, function(activePoint){
						activePoint.fillColor = activePoint.highlightFill;
						activePoint.strokeColor = activePoint.highlightStroke;
					});
					this.showTooltip(activePoints);
				});
			}

			//Iterate through each of the datasets, and build this into a property of the chart
			helpers.each(data.datasets,function(dataset){

				var datasetObject = {
					label : dataset.label || null,
					fillColor : dataset.fillColor,
					strokeColor : dataset.strokeColor,
					pointColor : dataset.pointColor,
					pointStrokeColor : dataset.pointStrokeColor,
					points : []
				};

				this.datasets.push(datasetObject);


				helpers.each(dataset.data,function(dataPoint,index){
					//Add a new point for each piece of data, passing any required data to draw.
					datasetObject.points.push(new this.PointClass({
						value : dataPoint,
						label : data.labels[index],
						datasetLabel: dataset.label,
						strokeColor : dataset.pointStrokeColor,
						fillColor : dataset.pointColor,
						highlightFill : dataset.pointHighlightFill || dataset.pointColor,
						highlightStroke : dataset.pointHighlightStroke || dataset.pointStrokeColor
					}));
				},this);

				this.buildScale(data.labels);


				this.eachPoints(function(point, index){
					helpers.extend(point, {
						x: this.scale.calculateX(index),
						y: this.scale.endPoint
					});
					point.save();
				}, this);

			},this);


			this.render();
		},
		update : function(){
			this.scale.update();
			// Reset any highlight colours before updating.
			helpers.each(this.activeElements, function(activeElement){
				activeElement.restore(['fillColor', 'strokeColor']);
			});
			this.eachPoints(function(point){
				point.save();
			});
			this.render();
		},
		eachPoints : function(callback){
			helpers.each(this.datasets,function(dataset){
				helpers.each(dataset.points,callback,this);
			},this);
		},
		getPointsAtEvent : function(e){
			var pointsArray = [],
				eventPosition = helpers.getRelativePosition(e);
			helpers.each(this.datasets,function(dataset){
				helpers.each(dataset.points,function(point){
					if (point.inRange(eventPosition.x,eventPosition.y)) pointsArray.push(point);
				});
			},this);
			return pointsArray;
		},
		buildScale : function(labels){
			var self = this;

			var dataTotal = function(){
				var values = [];
				self.eachPoints(function(point){
					values.push(point.value);
				});

				return values;
			};

			var scaleOptions = {
				templateString : this.options.scaleLabel,
				height : this.chart.height,
				width : this.chart.width,
				ctx : this.chart.ctx,
				textColor : this.options.scaleFontColor,
				fontSize : this.options.scaleFontSize,
				fontStyle : this.options.scaleFontStyle,
				fontFamily : this.options.scaleFontFamily,
				valuesCount : labels.length,
				beginAtZero : this.options.scaleBeginAtZero,
				integersOnly : this.options.scaleIntegersOnly,
				calculateYRange : function(currentHeight){
					var updatedRanges = helpers.calculateScaleRange(
						dataTotal(),
						currentHeight,
						this.fontSize,
						this.beginAtZero,
						this.integersOnly
					);
					helpers.extend(this, updatedRanges);
				},
				xLabels : labels,
				font : helpers.fontString(this.options.scaleFontSize, this.options.scaleFontStyle, this.options.scaleFontFamily),
				lineWidth : this.options.scaleLineWidth,
				lineColor : this.options.scaleLineColor,
				showHorizontalLines : this.options.scaleShowHorizontalLines,
				showVerticalLines : this.options.scaleShowVerticalLines,
				gridLineWidth : (this.options.scaleShowGridLines) ? this.options.scaleGridLineWidth : 0,
				gridLineColor : (this.options.scaleShowGridLines) ? this.options.scaleGridLineColor : "rgba(0,0,0,0)",
				padding: (this.options.showScale) ? 0 : this.options.pointDotRadius + this.options.pointDotStrokeWidth,
				showLabels : this.options.scaleShowLabels,
				display : this.options.showScale
			};

			if (this.options.scaleOverride){
				helpers.extend(scaleOptions, {
					calculateYRange: helpers.noop,
					steps: this.options.scaleSteps,
					stepValue: this.options.scaleStepWidth,
					min: this.options.scaleStartValue,
					max: this.options.scaleStartValue + (this.options.scaleSteps * this.options.scaleStepWidth)
				});
			}


			this.scale = new Chart.Scale(scaleOptions);
		},
		addData : function(valuesArray,label){
			//Map the values array for each of the datasets

			helpers.each(valuesArray,function(value,datasetIndex){
				//Add a new point for each piece of data, passing any required data to draw.
				this.datasets[datasetIndex].points.push(new this.PointClass({
					value : value,
					label : label,
					x: this.scale.calculateX(this.scale.valuesCount+1),
					y: this.scale.endPoint,
					strokeColor : this.datasets[datasetIndex].pointStrokeColor,
					fillColor : this.datasets[datasetIndex].pointColor
				}));
			},this);

			this.scale.addXLabel(label);
			//Then re-render the chart.
			this.update();
		},
		removeData : function(){
			this.scale.removeXLabel();
			//Then re-render the chart.
			helpers.each(this.datasets,function(dataset){
				dataset.points.shift();
			},this);
			this.update();
		},
		reflow : function(){
			var newScaleProps = helpers.extend({
				height : this.chart.height,
				width : this.chart.width
			});
			this.scale.update(newScaleProps);
		},
		draw : function(ease){
			var easingDecimal = ease || 1;
			this.clear();

			var ctx = this.chart.ctx;

			// Some helper methods for getting the next/prev points
			var hasValue = function(item){
				return item.value !== null;
			},
			nextPoint = function(point, collection, index){
				return helpers.findNextWhere(collection, hasValue, index) || point;
			},
			previousPoint = function(point, collection, index){
				return helpers.findPreviousWhere(collection, hasValue, index) || point;
			};

			this.scale.draw(easingDecimal);


			helpers.each(this.datasets,function(dataset){
				var pointsWithValues = helpers.where(dataset.points, hasValue);

				//Transition each point first so that the line and point drawing isn't out of sync
				//We can use this extra loop to calculate the control points of this dataset also in this loop

				helpers.each(dataset.points, function(point, index){
					if (point.hasValue()){
						point.transition({
							y : this.scale.calculateY(point.value),
							x : this.scale.calculateX(index)
						}, easingDecimal);
					}
				},this);


				// Control points need to be calculated in a seperate loop, because we need to know the current x/y of the point
				// This would cause issues when there is no animation, because the y of the next point would be 0, so beziers would be skewed
				if (this.options.bezierCurve){
					helpers.each(pointsWithValues, function(point, index){
						var tension = (index > 0 && index < pointsWithValues.length - 1) ? this.options.bezierCurveTension : 0;
						point.controlPoints = helpers.splineCurve(
							previousPoint(point, pointsWithValues, index),
							point,
							nextPoint(point, pointsWithValues, index),
							tension
						);

						// Prevent the bezier going outside of the bounds of the graph

						// Cap puter bezier handles to the upper/lower scale bounds
						if (point.controlPoints.outer.y > this.scale.endPoint){
							point.controlPoints.outer.y = this.scale.endPoint;
						}
						else if (point.controlPoints.outer.y < this.scale.startPoint){
							point.controlPoints.outer.y = this.scale.startPoint;
						}

						// Cap inner bezier handles to the upper/lower scale bounds
						if (point.controlPoints.inner.y > this.scale.endPoint){
							point.controlPoints.inner.y = this.scale.endPoint;
						}
						else if (point.controlPoints.inner.y < this.scale.startPoint){
							point.controlPoints.inner.y = this.scale.startPoint;
						}
					},this);
				}


				//Draw the line between all the points
				ctx.lineWidth = this.options.datasetStrokeWidth;
				ctx.strokeStyle = dataset.strokeColor;
				ctx.beginPath();

				helpers.each(pointsWithValues, function(point, index){
					if (index === 0){
						ctx.moveTo(point.x, point.y);
					}
					else{
						if(this.options.bezierCurve){
							var previous = previousPoint(point, pointsWithValues, index);

							ctx.bezierCurveTo(
								previous.controlPoints.outer.x,
								previous.controlPoints.outer.y,
								point.controlPoints.inner.x,
								point.controlPoints.inner.y,
								point.x,
								point.y
							);
						}
						else{
							ctx.lineTo(point.x,point.y);
						}
					}
				}, this);

				ctx.stroke();

				if (this.options.datasetFill && pointsWithValues.length > 0){
					//Round off the line by going to the base of the chart, back to the start, then fill.
					ctx.lineTo(pointsWithValues[pointsWithValues.length - 1].x, this.scale.endPoint);
					ctx.lineTo(pointsWithValues[0].x, this.scale.endPoint);
					ctx.fillStyle = dataset.fillColor;
					ctx.closePath();
					ctx.fill();
				}

				//Now draw the points over the line
				//A little inefficient double looping, but better than the line
				//lagging behind the point positions
				helpers.each(pointsWithValues,function(point){
					point.draw();
				});
			},this);
		}
	});


}).call(this);

(function(){
	"use strict";

	var root = this,
		Chart = root.Chart,
		//Cache a local reference to Chart.helpers
		helpers = Chart.helpers;

	var defaultConfig = {
		//Boolean - Show a backdrop to the scale label
		scaleShowLabelBackdrop : true,

		//String - The colour of the label backdrop
		scaleBackdropColor : "rgba(255,255,255,0.75)",

		// Boolean - Whether the scale should begin at zero
		scaleBeginAtZero : true,

		//Number - The backdrop padding above & below the label in pixels
		scaleBackdropPaddingY : 2,

		//Number - The backdrop padding to the side of the label in pixels
		scaleBackdropPaddingX : 2,

		//Boolean - Show line for each value in the scale
		scaleShowLine : true,

		//Boolean - Stroke a line around each segment in the chart
		segmentShowStroke : true,

		//String - The colour of the stroke on each segement.
		segmentStrokeColor : "#fff",

		//Number - The width of the stroke value in pixels
		segmentStrokeWidth : 2,

		//Number - Amount of animation steps
		animationSteps : 100,

		//String - Animation easing effect.
		animationEasing : "easeOutBounce",

		//Boolean - Whether to animate the rotation of the chart
		animateRotate : true,

		//Boolean - Whether to animate scaling the chart from the centre
		animateScale : false,

		//String - A legend template
		legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
	};


	Chart.Type.extend({
		//Passing in a name registers this chart in the Chart namespace
		name: "PolarArea",
		//Providing a defaults will also register the deafults in the chart namespace
		defaults : defaultConfig,
		//Initialize is fired when the chart is initialized - Data is passed in as a parameter
		//Config is automatically merged by the core of Chart.js, and is available at this.options
		initialize:  function(data){
			this.segments = [];
			//Declare segment class as a chart instance specific class, so it can share props for this instance
			this.SegmentArc = Chart.Arc.extend({
				showStroke : this.options.segmentShowStroke,
				strokeWidth : this.options.segmentStrokeWidth,
				strokeColor : this.options.segmentStrokeColor,
				ctx : this.chart.ctx,
				innerRadius : 0,
				x : this.chart.width/2,
				y : this.chart.height/2
			});
			this.scale = new Chart.RadialScale({
				display: this.options.showScale,
				fontStyle: this.options.scaleFontStyle,
				fontSize: this.options.scaleFontSize,
				fontFamily: this.options.scaleFontFamily,
				fontColor: this.options.scaleFontColor,
				showLabels: this.options.scaleShowLabels,
				showLabelBackdrop: this.options.scaleShowLabelBackdrop,
				backdropColor: this.options.scaleBackdropColor,
				backdropPaddingY : this.options.scaleBackdropPaddingY,
				backdropPaddingX: this.options.scaleBackdropPaddingX,
				lineWidth: (this.options.scaleShowLine) ? this.options.scaleLineWidth : 0,
				lineColor: this.options.scaleLineColor,
				lineArc: true,
				width: this.chart.width,
				height: this.chart.height,
				xCenter: this.chart.width/2,
				yCenter: this.chart.height/2,
				ctx : this.chart.ctx,
				templateString: this.options.scaleLabel,
				valuesCount: data.length
			});

			this.updateScaleRange(data);

			this.scale.update();

			helpers.each(data,function(segment,index){
				this.addData(segment,index,true);
			},this);

			//Set up tooltip events on the chart
			if (this.options.showTooltips){
				helpers.bindEvents(this, this.options.tooltipEvents, function(evt){
					var activeSegments = (evt.type !== 'mouseout') ? this.getSegmentsAtEvent(evt) : [];
					helpers.each(this.segments,function(segment){
						segment.restore(["fillColor"]);
					});
					helpers.each(activeSegments,function(activeSegment){
						activeSegment.fillColor = activeSegment.highlightColor;
					});
					this.showTooltip(activeSegments);
				});
			}

			this.render();
		},
		getSegmentsAtEvent : function(e){
			var segmentsArray = [];

			var location = helpers.getRelativePosition(e);

			helpers.each(this.segments,function(segment){
				if (segment.inRange(location.x,location.y)) segmentsArray.push(segment);
			},this);
			return segmentsArray;
		},
		addData : function(segment, atIndex, silent){
			var index = atIndex || this.segments.length;

			this.segments.splice(index, 0, new this.SegmentArc({
				fillColor: segment.color,
				highlightColor: segment.highlight || segment.color,
				label: segment.label,
				value: segment.value,
				outerRadius: (this.options.animateScale) ? 0 : this.scale.calculateCenterOffset(segment.value),
				circumference: (this.options.animateRotate) ? 0 : this.scale.getCircumference(),
				startAngle: Math.PI * 1.5
			}));
			if (!silent){
				this.reflow();
				this.update();
			}
		},
		removeData: function(atIndex){
			var indexToDelete = (helpers.isNumber(atIndex)) ? atIndex : this.segments.length-1;
			this.segments.splice(indexToDelete, 1);
			this.reflow();
			this.update();
		},
		calculateTotal: function(data){
			this.total = 0;
			helpers.each(data,function(segment){
				this.total += segment.value;
			},this);
			this.scale.valuesCount = this.segments.length;
		},
		updateScaleRange: function(datapoints){
			var valuesArray = [];
			helpers.each(datapoints,function(segment){
				valuesArray.push(segment.value);
			});

			var scaleSizes = (this.options.scaleOverride) ?
				{
					steps: this.options.scaleSteps,
					stepValue: this.options.scaleStepWidth,
					min: this.options.scaleStartValue,
					max: this.options.scaleStartValue + (this.options.scaleSteps * this.options.scaleStepWidth)
				} :
				helpers.calculateScaleRange(
					valuesArray,
					helpers.min([this.chart.width, this.chart.height])/2,
					this.options.scaleFontSize,
					this.options.scaleBeginAtZero,
					this.options.scaleIntegersOnly
				);

			helpers.extend(
				this.scale,
				scaleSizes,
				{
					size: helpers.min([this.chart.width, this.chart.height]),
					xCenter: this.chart.width/2,
					yCenter: this.chart.height/2
				}
			);

		},
		update : function(){
			this.calculateTotal(this.segments);

			helpers.each(this.segments,function(segment){
				segment.save();
			});
			
			this.reflow();
			this.render();
		},
		reflow : function(){
			helpers.extend(this.SegmentArc.prototype,{
				x : this.chart.width/2,
				y : this.chart.height/2
			});
			this.updateScaleRange(this.segments);
			this.scale.update();

			helpers.extend(this.scale,{
				xCenter: this.chart.width/2,
				yCenter: this.chart.height/2
			});

			helpers.each(this.segments, function(segment){
				segment.update({
					outerRadius : this.scale.calculateCenterOffset(segment.value)
				});
			}, this);

		},
		draw : function(ease){
			var easingDecimal = ease || 1;
			//Clear & draw the canvas
			this.clear();
			helpers.each(this.segments,function(segment, index){
				segment.transition({
					circumference : this.scale.getCircumference(),
					outerRadius : this.scale.calculateCenterOffset(segment.value)
				},easingDecimal);

				segment.endAngle = segment.startAngle + segment.circumference;

				// If we've removed the first segment we need to set the first one to
				// start at the top.
				if (index === 0){
					segment.startAngle = Math.PI * 1.5;
				}

				//Check to see if it's the last segment, if not get the next and update the start angle
				if (index < this.segments.length - 1){
					this.segments[index+1].startAngle = segment.endAngle;
				}
				segment.draw();
			}, this);
			this.scale.draw();
		}
	});

}).call(this);
(function(){
	"use strict";

	var root = this,
		Chart = root.Chart,
		helpers = Chart.helpers;



	Chart.Type.extend({
		name: "Radar",
		defaults:{
			//Boolean - Whether to show lines for each scale point
			scaleShowLine : true,

			//Boolean - Whether we show the angle lines out of the radar
			angleShowLineOut : true,

			//Boolean - Whether to show labels on the scale
			scaleShowLabels : false,

			// Boolean - Whether the scale should begin at zero
			scaleBeginAtZero : true,

			//String - Colour of the angle line
			angleLineColor : "rgba(0,0,0,.1)",

			//Number - Pixel width of the angle line
			angleLineWidth : 1,

			//String - Point label font declaration
			pointLabelFontFamily : "'Arial'",

			//String - Point label font weight
			pointLabelFontStyle : "normal",

			//Number - Point label font size in pixels
			pointLabelFontSize : 10,

			//String - Point label font colour
			pointLabelFontColor : "#666",

			//Boolean - Whether to show a dot for each point
			pointDot : true,

			//Number - Radius of each point dot in pixels
			pointDotRadius : 3,

			//Number - Pixel width of point dot stroke
			pointDotStrokeWidth : 1,

			//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			pointHitDetectionRadius : 20,

			//Boolean - Whether to show a stroke for datasets
			datasetStroke : true,

			//Number - Pixel width of dataset stroke
			datasetStrokeWidth : 2,

			//Boolean - Whether to fill the dataset with a colour
			datasetFill : true,

			//String - A legend template
			legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

		},

		initialize: function(data){
			this.PointClass = Chart.Point.extend({
				strokeWidth : this.options.pointDotStrokeWidth,
				radius : this.options.pointDotRadius,
				display: this.options.pointDot,
				hitDetectionRadius : this.options.pointHitDetectionRadius,
				ctx : this.chart.ctx
			});

			this.datasets = [];

			this.buildScale(data);

			//Set up tooltip events on the chart
			if (this.options.showTooltips){
				helpers.bindEvents(this, this.options.tooltipEvents, function(evt){
					var activePointsCollection = (evt.type !== 'mouseout') ? this.getPointsAtEvent(evt) : [];

					this.eachPoints(function(point){
						point.restore(['fillColor', 'strokeColor']);
					});
					helpers.each(activePointsCollection, function(activePoint){
						activePoint.fillColor = activePoint.highlightFill;
						activePoint.strokeColor = activePoint.highlightStroke;
					});

					this.showTooltip(activePointsCollection);
				});
			}

			//Iterate through each of the datasets, and build this into a property of the chart
			helpers.each(data.datasets,function(dataset){

				var datasetObject = {
					label: dataset.label || null,
					fillColor : dataset.fillColor,
					strokeColor : dataset.strokeColor,
					pointColor : dataset.pointColor,
					pointStrokeColor : dataset.pointStrokeColor,
					points : []
				};

				this.datasets.push(datasetObject);

				helpers.each(dataset.data,function(dataPoint,index){
					//Add a new point for each piece of data, passing any required data to draw.
					var pointPosition;
					if (!this.scale.animation){
						pointPosition = this.scale.getPointPosition(index, this.scale.calculateCenterOffset(dataPoint));
					}
					datasetObject.points.push(new this.PointClass({
						value : dataPoint,
						label : data.labels[index],
						datasetLabel: dataset.label,
						x: (this.options.animation) ? this.scale.xCenter : pointPosition.x,
						y: (this.options.animation) ? this.scale.yCenter : pointPosition.y,
						strokeColor : dataset.pointStrokeColor,
						fillColor : dataset.pointColor,
						highlightFill : dataset.pointHighlightFill || dataset.pointColor,
						highlightStroke : dataset.pointHighlightStroke || dataset.pointStrokeColor
					}));
				},this);

			},this);

			this.render();
		},
		eachPoints : function(callback){
			helpers.each(this.datasets,function(dataset){
				helpers.each(dataset.points,callback,this);
			},this);
		},

		getPointsAtEvent : function(evt){
			var mousePosition = helpers.getRelativePosition(evt),
				fromCenter = helpers.getAngleFromPoint({
					x: this.scale.xCenter,
					y: this.scale.yCenter
				}, mousePosition);

			var anglePerIndex = (Math.PI * 2) /this.scale.valuesCount,
				pointIndex = Math.round((fromCenter.angle - Math.PI * 1.5) / anglePerIndex),
				activePointsCollection = [];

			// If we're at the top, make the pointIndex 0 to get the first of the array.
			if (pointIndex >= this.scale.valuesCount || pointIndex < 0){
				pointIndex = 0;
			}

			if (fromCenter.distance <= this.scale.drawingArea){
				helpers.each(this.datasets, function(dataset){
					activePointsCollection.push(dataset.points[pointIndex]);
				});
			}

			return activePointsCollection;
		},

		buildScale : function(data){
			this.scale = new Chart.RadialScale({
				display: this.options.showScale,
				fontStyle: this.options.scaleFontStyle,
				fontSize: this.options.scaleFontSize,
				fontFamily: this.options.scaleFontFamily,
				fontColor: this.options.scaleFontColor,
				showLabels: this.options.scaleShowLabels,
				showLabelBackdrop: this.options.scaleShowLabelBackdrop,
				backdropColor: this.options.scaleBackdropColor,
				backdropPaddingY : this.options.scaleBackdropPaddingY,
				backdropPaddingX: this.options.scaleBackdropPaddingX,
				lineWidth: (this.options.scaleShowLine) ? this.options.scaleLineWidth : 0,
				lineColor: this.options.scaleLineColor,
				angleLineColor : this.options.angleLineColor,
				angleLineWidth : (this.options.angleShowLineOut) ? this.options.angleLineWidth : 0,
				// Point labels at the edge of each line
				pointLabelFontColor : this.options.pointLabelFontColor,
				pointLabelFontSize : this.options.pointLabelFontSize,
				pointLabelFontFamily : this.options.pointLabelFontFamily,
				pointLabelFontStyle : this.options.pointLabelFontStyle,
				height : this.chart.height,
				width: this.chart.width,
				xCenter: this.chart.width/2,
				yCenter: this.chart.height/2,
				ctx : this.chart.ctx,
				templateString: this.options.scaleLabel,
				labels: data.labels,
				valuesCount: data.datasets[0].data.length
			});

			this.scale.setScaleSize();
			this.updateScaleRange(data.datasets);
			this.scale.buildYLabels();
		},
		updateScaleRange: function(datasets){
			var valuesArray = (function(){
				var totalDataArray = [];
				helpers.each(datasets,function(dataset){
					if (dataset.data){
						totalDataArray = totalDataArray.concat(dataset.data);
					}
					else {
						helpers.each(dataset.points, function(point){
							totalDataArray.push(point.value);
						});
					}
				});
				return totalDataArray;
			})();


			var scaleSizes = (this.options.scaleOverride) ?
				{
					steps: this.options.scaleSteps,
					stepValue: this.options.scaleStepWidth,
					min: this.options.scaleStartValue,
					max: this.options.scaleStartValue + (this.options.scaleSteps * this.options.scaleStepWidth)
				} :
				helpers.calculateScaleRange(
					valuesArray,
					helpers.min([this.chart.width, this.chart.height])/2,
					this.options.scaleFontSize,
					this.options.scaleBeginAtZero,
					this.options.scaleIntegersOnly
				);

			helpers.extend(
				this.scale,
				scaleSizes
			);

		},
		addData : function(valuesArray,label){
			//Map the values array for each of the datasets
			this.scale.valuesCount++;
			helpers.each(valuesArray,function(value,datasetIndex){
				var pointPosition = this.scale.getPointPosition(this.scale.valuesCount, this.scale.calculateCenterOffset(value));
				this.datasets[datasetIndex].points.push(new this.PointClass({
					value : value,
					label : label,
					x: pointPosition.x,
					y: pointPosition.y,
					strokeColor : this.datasets[datasetIndex].pointStrokeColor,
					fillColor : this.datasets[datasetIndex].pointColor
				}));
			},this);

			this.scale.labels.push(label);

			this.reflow();

			this.update();
		},
		removeData : function(){
			this.scale.valuesCount--;
			this.scale.labels.shift();
			helpers.each(this.datasets,function(dataset){
				dataset.points.shift();
			},this);
			this.reflow();
			this.update();
		},
		update : function(){
			this.eachPoints(function(point){
				point.save();
			});
			this.reflow();
			this.render();
		},
		reflow: function(){
			helpers.extend(this.scale, {
				width : this.chart.width,
				height: this.chart.height,
				size : helpers.min([this.chart.width, this.chart.height]),
				xCenter: this.chart.width/2,
				yCenter: this.chart.height/2
			});
			this.updateScaleRange(this.datasets);
			this.scale.setScaleSize();
			this.scale.buildYLabels();
		},
		draw : function(ease){
			var easeDecimal = ease || 1,
				ctx = this.chart.ctx;
			this.clear();
			this.scale.draw();

			helpers.each(this.datasets,function(dataset){

				//Transition each point first so that the line and point drawing isn't out of sync
				helpers.each(dataset.points,function(point,index){
					if (point.hasValue()){
						point.transition(this.scale.getPointPosition(index, this.scale.calculateCenterOffset(point.value)), easeDecimal);
					}
				},this);



				//Draw the line between all the points
				ctx.lineWidth = this.options.datasetStrokeWidth;
				ctx.strokeStyle = dataset.strokeColor;
				ctx.beginPath();
				helpers.each(dataset.points,function(point,index){
					if (index === 0){
						ctx.moveTo(point.x,point.y);
					}
					else{
						ctx.lineTo(point.x,point.y);
					}
				},this);
				ctx.closePath();
				ctx.stroke();

				ctx.fillStyle = dataset.fillColor;
				ctx.fill();

				//Now draw the points over the line
				//A little inefficient double looping, but better than the line
				//lagging behind the point positions
				helpers.each(dataset.points,function(point){
					if (point.hasValue()){
						point.draw();
					}
				});

			},this);

		}

	});





}).call(this);
},{}],99:[function(require,module,exports){
/**
 * Before Interceptor.
 */

var _ = require('../util');

module.exports = {

    request: function (request) {

        if (_.isFunction(request.beforeSend)) {
            request.beforeSend.call(this, request);
        }

        return request;
    }

};

},{"../util":122}],100:[function(require,module,exports){
/**
 * Base client.
 */

var _ = require('../../util');
var Promise = require('../../promise');
var xhrClient = require('./xhr');

module.exports = function (request) {

    var response = (request.client || xhrClient)(request);

    return Promise.resolve(response).then(function (response) {

        if (response.headers) {

            var headers = parseHeaders(response.headers);

            response.headers = function (name) {

                if (name) {
                    return headers[_.toLower(name)];
                }

                return headers;
            };

        }

        response.ok = response.status >= 200 && response.status < 300;

        return response;
    });

};

function parseHeaders(str) {

    var headers = {}, value, name, i;

    if (_.isString(str)) {
        _.each(str.split('\n'), function (row) {

            i = row.indexOf(':');
            name = _.trim(_.toLower(row.slice(0, i)));
            value = _.trim(row.slice(i + 1));

            if (headers[name]) {

                if (_.isArray(headers[name])) {
                    headers[name].push(value);
                } else {
                    headers[name] = [headers[name], value];
                }

            } else {

                headers[name] = value;
            }

        });
    }

    return headers;
}

},{"../../promise":115,"../../util":122,"./xhr":103}],101:[function(require,module,exports){
/**
 * JSONP client.
 */

var _ = require('../../util');
var Promise = require('../../promise');

module.exports = function (request) {
    return new Promise(function (resolve) {

        var callback = '_jsonp' + Math.random().toString(36).substr(2), response = {request: request, data: null}, handler, script;

        request.params[request.jsonp] = callback;
        request.cancel = function () {
            handler({type: 'cancel'});
        };

        script = document.createElement('script');
        script.src = _.url(request);
        script.type = 'text/javascript';
        script.async = true;

        window[callback] = function (data) {
            response.data = data;
        };

        handler = function (event) {

            if (event.type === 'load' && response.data !== null) {
                response.status = 200;
            } else if (event.type === 'error') {
                response.status = 404;
            } else {
                response.status = 0;
            }

            resolve(response);

            delete window[callback];
            document.body.removeChild(script);
        };

        script.onload = handler;
        script.onerror = handler;

        document.body.appendChild(script);
    });
};

},{"../../promise":115,"../../util":122}],102:[function(require,module,exports){
/**
 * XDomain client (Internet Explorer).
 */

var _ = require('../../util');
var Promise = require('../../promise');

module.exports = function (request) {
    return new Promise(function (resolve) {

        var xdr = new XDomainRequest(), response = {request: request}, handler;

        request.cancel = function () {
            xdr.abort();
        };

        xdr.open(request.method, _.url(request), true);

        handler = function (event) {

            response.data = xdr.responseText;
            response.status = xdr.status;
            response.statusText = xdr.statusText;

            resolve(response);
        };

        xdr.timeout = 0;
        xdr.onload = handler;
        xdr.onabort = handler;
        xdr.onerror = handler;
        xdr.ontimeout = function () {};
        xdr.onprogress = function () {};

        xdr.send(request.data);
    });
};

},{"../../promise":115,"../../util":122}],103:[function(require,module,exports){
/**
 * XMLHttp client.
 */

var _ = require('../../util');
var Promise = require('../../promise');

module.exports = function (request) {
    return new Promise(function (resolve) {

        var xhr = new XMLHttpRequest(), response = {request: request}, handler;

        request.cancel = function () {
            xhr.abort();
        };

        xhr.open(request.method, _.url(request), true);

        handler = function (event) {

            response.data = xhr.responseText;
            response.status = xhr.status;
            response.statusText = xhr.statusText;
            response.headers = xhr.getAllResponseHeaders();

            resolve(response);
        };

        xhr.timeout = 0;
        xhr.onload = handler;
        xhr.onabort = handler;
        xhr.onerror = handler;
        xhr.ontimeout = function () {};
        xhr.onprogress = function () {};

        if (_.isPlainObject(request.xhr)) {
            _.extend(xhr, request.xhr);
        }

        if (_.isPlainObject(request.upload)) {
            _.extend(xhr.upload, request.upload);
        }

        _.each(request.headers || {}, function (value, header) {
            xhr.setRequestHeader(header, value);
        });

        xhr.send(request.data);
    });
};

},{"../../promise":115,"../../util":122}],104:[function(require,module,exports){
/**
 * CORS Interceptor.
 */

var _ = require('../util');
var xdrClient = require('./client/xdr');
var xhrCors = 'withCredentials' in new XMLHttpRequest();
var originUrl = _.url.parse(location.href);

module.exports = {

    request: function (request) {

        if (request.crossOrigin === null) {
            request.crossOrigin = crossOrigin(request);
        }

        if (request.crossOrigin) {

            if (!xhrCors) {
                request.client = xdrClient;
            }

            request.emulateHTTP = false;
        }

        return request;
    }

};

function crossOrigin(request) {

    var requestUrl = _.url.parse(_.url(request));

    return (requestUrl.protocol !== originUrl.protocol || requestUrl.host !== originUrl.host);
}

},{"../util":122,"./client/xdr":102}],105:[function(require,module,exports){
/**
 * Header Interceptor.
 */

var _ = require('../util');

module.exports = {

    request: function (request) {

        request.method = request.method.toUpperCase();
        request.headers = _.extend({}, _.http.headers.common,
            !request.crossOrigin ? _.http.headers.custom : {},
            _.http.headers[request.method.toLowerCase()],
            request.headers
        );

        if (_.isPlainObject(request.data) && /^(GET|JSONP)$/i.test(request.method)) {
            _.extend(request.params, request.data);
            delete request.data;
        }

        return request;
    }

};

},{"../util":122}],106:[function(require,module,exports){
/**
 * Service for sending network requests.
 */

var _ = require('../util');
var Client = require('./client');
var Promise = require('../promise');
var interceptor = require('./interceptor');
var jsonType = {'Content-Type': 'application/json'};

function Http(url, options) {

    var client = Client, request, promise;

    Http.interceptors.forEach(function (handler) {
        client = interceptor(handler, this.$vm)(client);
    }, this);

    options = _.isObject(url) ? url : _.extend({url: url}, options);
    request = _.merge({}, Http.options, this.$options, options);
    promise = client(request).bind(this.$vm).then(function (response) {

        return response.ok ? response : Promise.reject(response);

    }, function (response) {

        if (response instanceof Error) {
            _.error(response);
        }

        return Promise.reject(response);
    });

    if (request.success) {
        promise.success(request.success);
    }

    if (request.error) {
        promise.error(request.error);
    }

    return promise;
}

Http.options = {
    method: 'get',
    data: '',
    params: {},
    headers: {},
    xhr: null,
    upload: null,
    jsonp: 'callback',
    beforeSend: null,
    crossOrigin: null,
    emulateHTTP: false,
    emulateJSON: false,
    timeout: 0
};

Http.interceptors = [
    require('./before'),
    require('./timeout'),
    require('./jsonp'),
    require('./method'),
    require('./mime'),
    require('./header'),
    require('./cors')
];

Http.headers = {
    put: jsonType,
    post: jsonType,
    patch: jsonType,
    delete: jsonType,
    common: {'Accept': 'application/json, text/plain, */*'},
    custom: {'X-Requested-With': 'XMLHttpRequest'}
};

['get', 'put', 'post', 'patch', 'delete', 'jsonp'].forEach(function (method) {

    Http[method] = function (url, data, success, options) {

        if (_.isFunction(data)) {
            options = success;
            success = data;
            data = undefined;
        }

        if (_.isObject(success)) {
            options = success;
            success = undefined;
        }

        return this(url, _.extend({method: method, data: data, success: success}, options));
    };
});

module.exports = _.http = Http;

},{"../promise":115,"../util":122,"./before":99,"./client":100,"./cors":104,"./header":105,"./interceptor":107,"./jsonp":108,"./method":109,"./mime":110,"./timeout":111}],107:[function(require,module,exports){
/**
 * Interceptor factory.
 */

var _ = require('../util');
var Promise = require('../promise');

module.exports = function (handler, vm) {

    return function (client) {

        if (_.isFunction(handler)) {
            handler = handler.call(vm, Promise);
        }

        return function (request) {

            if (_.isFunction(handler.request)) {
                request = handler.request.call(vm, request);
            }

            return when(request, function (request) {
                return when(client(request), function (response) {

                    if (_.isFunction(handler.response)) {
                        response = handler.response.call(vm, response);
                    }

                    return response;
                });
            });
        };
    };
};

function when(value, fulfilled, rejected) {

    var promise = Promise.resolve(value);

    if (arguments.length < 2) {
        return promise;
    }

    return promise.then(fulfilled, rejected);
}

},{"../promise":115,"../util":122}],108:[function(require,module,exports){
/**
 * JSONP Interceptor.
 */

var jsonpClient = require('./client/jsonp');

module.exports = {

    request: function (request) {

        if (request.method == 'JSONP') {
            request.client = jsonpClient;
        }

        return request;
    }

};

},{"./client/jsonp":101}],109:[function(require,module,exports){
/**
 * HTTP method override Interceptor.
 */

module.exports = {

    request: function (request) {

        if (request.emulateHTTP && /^(PUT|PATCH|DELETE)$/i.test(request.method)) {
            request.headers['X-HTTP-Method-Override'] = request.method;
            request.method = 'POST';
        }

        return request;
    }

};

},{}],110:[function(require,module,exports){
/**
 * Mime Interceptor.
 */

var _ = require('../util');

module.exports = {

    request: function (request) {

        if (request.emulateJSON && _.isPlainObject(request.data)) {
            request.headers['Content-Type'] = 'application/x-www-form-urlencoded';
            request.data = _.url.params(request.data);
        }

        if (_.isObject(request.data) && /FormData/i.test(request.data.toString())) {
            delete request.headers['Content-Type'];
        }

        if (_.isPlainObject(request.data)) {
            request.data = JSON.stringify(request.data);
        }

        return request;
    },

    response: function (response) {

        try {
            response.data = JSON.parse(response.data);
        } catch (e) {}

        return response;
    }

};

},{"../util":122}],111:[function(require,module,exports){
/**
 * Timeout Interceptor.
 */

module.exports = function () {

    var timeout;

    return {

        request: function (request) {

            if (request.timeout) {
                timeout = setTimeout(function () {
                    request.cancel();
                }, request.timeout);
            }

            return request;
        },

        response: function (response) {

            clearTimeout(timeout);

            return response;
        }

    };
};

},{}],112:[function(require,module,exports){
/**
 * Install plugin.
 */

function install(Vue) {

    var _ = require('./util');

    _.config = Vue.config;
    _.warning = Vue.util.warn;
    _.nextTick = Vue.util.nextTick;

    Vue.url = require('./url');
    Vue.http = require('./http');
    Vue.resource = require('./resource');
    Vue.Promise = require('./promise');

    Object.defineProperties(Vue.prototype, {

        $url: {
            get: function () {
                return _.options(Vue.url, this, this.$options.url);
            }
        },

        $http: {
            get: function () {
                return _.options(Vue.http, this, this.$options.http);
            }
        },

        $resource: {
            get: function () {
                return Vue.resource.bind(this);
            }
        },

        $promise: {
            get: function () {
                return function (executor) {
                    return new Vue.Promise(executor, this);
                }.bind(this);
            }
        }

    });
}

if (window.Vue) {
    Vue.use(install);
}

module.exports = install;

},{"./http":106,"./promise":115,"./resource":116,"./url":117,"./util":122}],113:[function(require,module,exports){
/**
 * Promises/A+ polyfill v1.1.4 (https://github.com/bramstein/promis)
 */

var _ = require('../util');

var RESOLVED = 0;
var REJECTED = 1;
var PENDING  = 2;

function Promise(executor) {

    this.state = PENDING;
    this.value = undefined;
    this.deferred = [];

    var promise = this;

    try {
        executor(function (x) {
            promise.resolve(x);
        }, function (r) {
            promise.reject(r);
        });
    } catch (e) {
        promise.reject(e);
    }
}

Promise.reject = function (r) {
    return new Promise(function (resolve, reject) {
        reject(r);
    });
};

Promise.resolve = function (x) {
    return new Promise(function (resolve, reject) {
        resolve(x);
    });
};

Promise.all = function all(iterable) {
    return new Promise(function (resolve, reject) {
        var count = 0, result = [];

        if (iterable.length === 0) {
            resolve(result);
        }

        function resolver(i) {
            return function (x) {
                result[i] = x;
                count += 1;

                if (count === iterable.length) {
                    resolve(result);
                }
            };
        }

        for (var i = 0; i < iterable.length; i += 1) {
            Promise.resolve(iterable[i]).then(resolver(i), reject);
        }
    });
};

Promise.race = function race(iterable) {
    return new Promise(function (resolve, reject) {
        for (var i = 0; i < iterable.length; i += 1) {
            Promise.resolve(iterable[i]).then(resolve, reject);
        }
    });
};

var p = Promise.prototype;

p.resolve = function resolve(x) {
    var promise = this;

    if (promise.state === PENDING) {
        if (x === promise) {
            throw new TypeError('Promise settled with itself.');
        }

        var called = false;

        try {
            var then = x && x['then'];

            if (x !== null && typeof x === 'object' && typeof then === 'function') {
                then.call(x, function (x) {
                    if (!called) {
                        promise.resolve(x);
                    }
                    called = true;

                }, function (r) {
                    if (!called) {
                        promise.reject(r);
                    }
                    called = true;
                });
                return;
            }
        } catch (e) {
            if (!called) {
                promise.reject(e);
            }
            return;
        }

        promise.state = RESOLVED;
        promise.value = x;
        promise.notify();
    }
};

p.reject = function reject(reason) {
    var promise = this;

    if (promise.state === PENDING) {
        if (reason === promise) {
            throw new TypeError('Promise settled with itself.');
        }

        promise.state = REJECTED;
        promise.value = reason;
        promise.notify();
    }
};

p.notify = function notify() {
    var promise = this;

    _.nextTick(function () {
        if (promise.state !== PENDING) {
            while (promise.deferred.length) {
                var deferred = promise.deferred.shift(),
                    onResolved = deferred[0],
                    onRejected = deferred[1],
                    resolve = deferred[2],
                    reject = deferred[3];

                try {
                    if (promise.state === RESOLVED) {
                        if (typeof onResolved === 'function') {
                            resolve(onResolved.call(undefined, promise.value));
                        } else {
                            resolve(promise.value);
                        }
                    } else if (promise.state === REJECTED) {
                        if (typeof onRejected === 'function') {
                            resolve(onRejected.call(undefined, promise.value));
                        } else {
                            reject(promise.value);
                        }
                    }
                } catch (e) {
                    reject(e);
                }
            }
        }
    });
};

p.then = function then(onResolved, onRejected) {
    var promise = this;

    return new Promise(function (resolve, reject) {
        promise.deferred.push([onResolved, onRejected, resolve, reject]);
        promise.notify();
    });
};

p.catch = function (onRejected) {
    return this.then(undefined, onRejected);
};

module.exports = Promise;

},{"../util":122}],114:[function(require,module,exports){
/**
 * URL Template v2.0.6 (https://github.com/bramstein/url-template)
 */

exports.expand = function (url, params, variables) {

    var tmpl = this.parse(url), expanded = tmpl.expand(params);

    if (variables) {
        variables.push.apply(variables, tmpl.vars);
    }

    return expanded;
};

exports.parse = function (template) {

    var operators = ['+', '#', '.', '/', ';', '?', '&'], variables = [];

    return {
        vars: variables,
        expand: function (context) {
            return template.replace(/\{([^\{\}]+)\}|([^\{\}]+)/g, function (_, expression, literal) {
                if (expression) {

                    var operator = null, values = [];

                    if (operators.indexOf(expression.charAt(0)) !== -1) {
                        operator = expression.charAt(0);
                        expression = expression.substr(1);
                    }

                    expression.split(/,/g).forEach(function (variable) {
                        var tmp = /([^:\*]*)(?::(\d+)|(\*))?/.exec(variable);
                        values.push.apply(values, exports.getValues(context, operator, tmp[1], tmp[2] || tmp[3]));
                        variables.push(tmp[1]);
                    });

                    if (operator && operator !== '+') {

                        var separator = ',';

                        if (operator === '?') {
                            separator = '&';
                        } else if (operator !== '#') {
                            separator = operator;
                        }

                        return (values.length !== 0 ? operator : '') + values.join(separator);
                    } else {
                        return values.join(',');
                    }

                } else {
                    return exports.encodeReserved(literal);
                }
            });
        }
    };
};

exports.getValues = function (context, operator, key, modifier) {

    var value = context[key], result = [];

    if (this.isDefined(value) && value !== '') {
        if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
            value = value.toString();

            if (modifier && modifier !== '*') {
                value = value.substring(0, parseInt(modifier, 10));
            }

            result.push(this.encodeValue(operator, value, this.isKeyOperator(operator) ? key : null));
        } else {
            if (modifier === '*') {
                if (Array.isArray(value)) {
                    value.filter(this.isDefined).forEach(function (value) {
                        result.push(this.encodeValue(operator, value, this.isKeyOperator(operator) ? key : null));
                    }, this);
                } else {
                    Object.keys(value).forEach(function (k) {
                        if (this.isDefined(value[k])) {
                            result.push(this.encodeValue(operator, value[k], k));
                        }
                    }, this);
                }
            } else {
                var tmp = [];

                if (Array.isArray(value)) {
                    value.filter(this.isDefined).forEach(function (value) {
                        tmp.push(this.encodeValue(operator, value));
                    }, this);
                } else {
                    Object.keys(value).forEach(function (k) {
                        if (this.isDefined(value[k])) {
                            tmp.push(encodeURIComponent(k));
                            tmp.push(this.encodeValue(operator, value[k].toString()));
                        }
                    }, this);
                }

                if (this.isKeyOperator(operator)) {
                    result.push(encodeURIComponent(key) + '=' + tmp.join(','));
                } else if (tmp.length !== 0) {
                    result.push(tmp.join(','));
                }
            }
        }
    } else {
        if (operator === ';') {
            result.push(encodeURIComponent(key));
        } else if (value === '' && (operator === '&' || operator === '?')) {
            result.push(encodeURIComponent(key) + '=');
        } else if (value === '') {
            result.push('');
        }
    }

    return result;
};

exports.isDefined = function (value) {
    return value !== undefined && value !== null;
};

exports.isKeyOperator = function (operator) {
    return operator === ';' || operator === '&' || operator === '?';
};

exports.encodeValue = function (operator, value, key) {

    value = (operator === '+' || operator === '#') ? this.encodeReserved(value) : encodeURIComponent(value);

    if (key) {
        return encodeURIComponent(key) + '=' + value;
    } else {
        return value;
    }
};

exports.encodeReserved = function (str) {
    return str.split(/(%[0-9A-Fa-f]{2})/g).map(function (part) {
        if (!/%[0-9A-Fa-f]/.test(part)) {
            part = encodeURI(part);
        }
        return part;
    }).join('');
};

},{}],115:[function(require,module,exports){
/**
 * Promise adapter.
 */

var _ = require('./util');
var PromiseObj = window.Promise || require('./lib/promise');

function Promise(executor, context) {

    if (executor instanceof PromiseObj) {
        this.promise = executor;
    } else {
        this.promise = new PromiseObj(executor.bind(context));
    }

    this.context = context;
}

Promise.all = function (iterable, context) {
    return new Promise(PromiseObj.all(iterable), context);
};

Promise.resolve = function (value, context) {
    return new Promise(PromiseObj.resolve(value), context);
};

Promise.reject = function (reason, context) {
    return new Promise(PromiseObj.reject(reason), context);
};

Promise.race = function (iterable, context) {
    return new Promise(PromiseObj.race(iterable), context);
};

var p = Promise.prototype;

p.bind = function (context) {
    this.context = context;
    return this;
};

p.then = function (fulfilled, rejected) {

    if (fulfilled && fulfilled.bind && this.context) {
        fulfilled = fulfilled.bind(this.context);
    }

    if (rejected && rejected.bind && this.context) {
        rejected = rejected.bind(this.context);
    }

    this.promise = this.promise.then(fulfilled, rejected);

    return this;
};

p.catch = function (rejected) {

    if (rejected && rejected.bind && this.context) {
        rejected = rejected.bind(this.context);
    }

    this.promise = this.promise.catch(rejected);

    return this;
};

p.finally = function (callback) {

    return this.then(function (value) {
            callback.call(this);
            return value;
        }, function (reason) {
            callback.call(this);
            return PromiseObj.reject(reason);
        }
    );
};

p.success = function (callback) {

    _.warn('The `success` method has been deprecated. Use the `then` method instead.');

    return this.then(function (response) {
        return callback.call(this, response.data, response.status, response) || response;
    });
};

p.error = function (callback) {

    _.warn('The `error` method has been deprecated. Use the `catch` method instead.');

    return this.catch(function (response) {
        return callback.call(this, response.data, response.status, response) || response;
    });
};

p.always = function (callback) {

    _.warn('The `always` method has been deprecated. Use the `finally` method instead.');

    var cb = function (response) {
        return callback.call(this, response.data, response.status, response) || response;
    };

    return this.then(cb, cb);
};

module.exports = Promise;

},{"./lib/promise":113,"./util":122}],116:[function(require,module,exports){
/**
 * Service for interacting with RESTful services.
 */

var _ = require('./util');

function Resource(url, params, actions, options) {

    var self = this, resource = {};

    actions = _.extend({},
        Resource.actions,
        actions
    );

    _.each(actions, function (action, name) {

        action = _.merge({url: url, params: params || {}}, options, action);

        resource[name] = function () {
            return (self.$http || _.http)(opts(action, arguments));
        };
    });

    return resource;
}

function opts(action, args) {

    var options = _.extend({}, action), params = {}, data, success, error;

    switch (args.length) {

        case 4:

            error = args[3];
            success = args[2];

        case 3:
        case 2:

            if (_.isFunction(args[1])) {

                if (_.isFunction(args[0])) {

                    success = args[0];
                    error = args[1];

                    break;
                }

                success = args[1];
                error = args[2];

            } else {

                params = args[0];
                data = args[1];
                success = args[2];

                break;
            }

        case 1:

            if (_.isFunction(args[0])) {
                success = args[0];
            } else if (/^(POST|PUT|PATCH)$/i.test(options.method)) {
                data = args[0];
            } else {
                params = args[0];
            }

            break;

        case 0:

            break;

        default:

            throw 'Expected up to 4 arguments [params, data, success, error], got ' + args.length + ' arguments';
    }

    options.data = data;
    options.params = _.extend({}, options.params, params);

    if (success) {
        options.success = success;
    }

    if (error) {
        options.error = error;
    }

    return options;
}

Resource.actions = {

    get: {method: 'GET'},
    save: {method: 'POST'},
    query: {method: 'GET'},
    update: {method: 'PUT'},
    remove: {method: 'DELETE'},
    delete: {method: 'DELETE'}

};

module.exports = _.resource = Resource;

},{"./util":122}],117:[function(require,module,exports){
/**
 * Service for URL templating.
 */

var _ = require('../util');
var ie = document.documentMode;
var el = document.createElement('a');

function Url(url, params) {

    var options = url, transform;

    if (_.isString(url)) {
        options = {url: url, params: params};
    }

    options = _.merge({}, Url.options, this.$options, options);

    Url.transforms.forEach(function (handler) {
        transform = factory(handler, transform, this.$vm);
    }, this);

    return transform(options);
};

/**
 * Url options.
 */

Url.options = {
    url: '',
    root: null,
    params: {}
};

/**
 * Url transforms.
 */

Url.transforms = [
    require('./template'),
    require('./legacy'),
    require('./query'),
    require('./root')
];

/**
 * Encodes a Url parameter string.
 *
 * @param {Object} obj
 */

Url.params = function (obj) {

    var params = [], escape = encodeURIComponent;

    params.add = function (key, value) {

        if (_.isFunction(value)) {
            value = value();
        }

        if (value === null) {
            value = '';
        }

        this.push(escape(key) + '=' + escape(value));
    };

    serialize(params, obj);

    return params.join('&').replace(/%20/g, '+');
};

/**
 * Parse a URL and return its components.
 *
 * @param {String} url
 */

Url.parse = function (url) {

    if (ie) {
        el.href = url;
        url = el.href;
    }

    el.href = url;

    return {
        href: el.href,
        protocol: el.protocol ? el.protocol.replace(/:$/, '') : '',
        port: el.port,
        host: el.host,
        hostname: el.hostname,
        pathname: el.pathname.charAt(0) === '/' ? el.pathname : '/' + el.pathname,
        search: el.search ? el.search.replace(/^\?/, '') : '',
        hash: el.hash ? el.hash.replace(/^#/, '') : ''
    };
};

function factory(handler, next, vm) {
    return function (options) {
        return handler.call(vm, options, next);
    };
}

function serialize(params, obj, scope) {

    var array = _.isArray(obj), plain = _.isPlainObject(obj), hash;

    _.each(obj, function (value, key) {

        hash = _.isObject(value) || _.isArray(value);

        if (scope) {
            key = scope + '[' + (plain || hash ? key : '') + ']';
        }

        if (!scope && array) {
            params.add(value.name, value.value);
        } else if (hash) {
            serialize(params, value, key);
        } else {
            params.add(key, value);
        }
    });
}

module.exports = _.url = Url;

},{"../util":122,"./legacy":118,"./query":119,"./root":120,"./template":121}],118:[function(require,module,exports){
/**
 * Legacy Transform.
 */

var _ = require('../util');

module.exports = function (options, next) {

    var variables = [], url = next(options);

    url = url.replace(/(\/?):([a-z]\w*)/gi, function (match, slash, name) {

        _.warn('The `:' + name + '` parameter syntax has been deprecated. Use the `{' + name + '}` syntax instead.');

        if (options.params[name]) {
            variables.push(name);
            return slash + encodeUriSegment(options.params[name]);
        }

        return '';
    });

    variables.forEach(function (key) {
        delete options.params[key];
    });

    return url;
};

function encodeUriSegment(value) {

    return encodeUriQuery(value, true).
        replace(/%26/gi, '&').
        replace(/%3D/gi, '=').
        replace(/%2B/gi, '+');
}

function encodeUriQuery(value, spaces) {

    return encodeURIComponent(value).
        replace(/%40/gi, '@').
        replace(/%3A/gi, ':').
        replace(/%24/g, '$').
        replace(/%2C/gi, ',').
        replace(/%20/g, (spaces ? '%20' : '+'));
}

},{"../util":122}],119:[function(require,module,exports){
/**
 * Query Parameter Transform.
 */

var _ = require('../util');

module.exports = function (options, next) {

    var urlParams = Object.keys(_.url.options.params), query = {}, url = next(options);

   _.each(options.params, function (value, key) {
        if (urlParams.indexOf(key) === -1) {
            query[key] = value;
        }
    });

    query = _.url.params(query);

    if (query) {
        url += (url.indexOf('?') == -1 ? '?' : '&') + query;
    }

    return url;
};

},{"../util":122}],120:[function(require,module,exports){
/**
 * Root Prefix Transform.
 */

var _ = require('../util');

module.exports = function (options, next) {

    var url = next(options);

    if (_.isString(options.root) && !url.match(/^(https?:)?\//)) {
        url = options.root + '/' + url;
    }

    return url;
};

},{"../util":122}],121:[function(require,module,exports){
/**
 * URL Template (RFC 6570) Transform.
 */

var UrlTemplate = require('../lib/url-template');

module.exports = function (options) {

    var variables = [], url = UrlTemplate.expand(options.url, options.params, variables);

    variables.forEach(function (key) {
        delete options.params[key];
    });

    return url;
};

},{"../lib/url-template":114}],122:[function(require,module,exports){
/**
 * Utility functions.
 */

var _ = exports, array = [], console = window.console;

_.warn = function (msg) {
    if (console && _.warning && (!_.config.silent || _.config.debug)) {
        console.warn('[VueResource warn]: ' + msg);
    }
};

_.error = function (msg) {
    if (console) {
        console.error(msg);
    }
};

_.trim = function (str) {
    return str.replace(/^\s*|\s*$/g, '');
};

_.toLower = function (str) {
    return str ? str.toLowerCase() : '';
};

_.isArray = Array.isArray;

_.isString = function (val) {
    return typeof val === 'string';
};

_.isFunction = function (val) {
    return typeof val === 'function';
};

_.isObject = function (obj) {
    return obj !== null && typeof obj === 'object';
};

_.isPlainObject = function (obj) {
    return _.isObject(obj) && Object.getPrototypeOf(obj) == Object.prototype;
};

_.options = function (fn, obj, options) {

    options = options || {};

    if (_.isFunction(options)) {
        options = options.call(obj);
    }

    return _.merge(fn.bind({$vm: obj, $options: options}), fn, {$options: options});
};

_.each = function (obj, iterator) {

    var i, key;

    if (typeof obj.length == 'number') {
        for (i = 0; i < obj.length; i++) {
            iterator.call(obj[i], obj[i], i);
        }
    } else if (_.isObject(obj)) {
        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                iterator.call(obj[key], obj[key], key);
            }
        }
    }

    return obj;
};

_.defaults = function (target, source) {

    for (var key in source) {
        if (target[key] === undefined) {
            target[key] = source[key];
        }
    }

    return target;
};

_.extend = function (target) {

    var args = array.slice.call(arguments, 1);

    args.forEach(function (arg) {
        merge(target, arg);
    });

    return target;
};

_.merge = function (target) {

    var args = array.slice.call(arguments, 1);

    args.forEach(function (arg) {
        merge(target, arg, true);
    });

    return target;
};

function merge(target, source, deep) {
    for (var key in source) {
        if (deep && (_.isPlainObject(source[key]) || _.isArray(source[key]))) {
            if (_.isPlainObject(source[key]) && !_.isPlainObject(target[key])) {
                target[key] = {};
            }
            if (_.isArray(source[key]) && !_.isArray(target[key])) {
                target[key] = [];
            }
            merge(target[key], source[key], deep);
        } else if (source[key] !== undefined) {
            target[key] = source[key];
        }
    }
}

},{}],123:[function(require,module,exports){
/**
 * Import(s)
 */

var validates = require('./lib/validates')
var _ = require('./lib/util')


/**
 * Export(s)
 */

module.exports = install


/**
 * Install plugin
 */

function install (Vue, options) {
  options = options || {}
  var componentName = options.component = options.component || '$validator'
  var directiveName = options.directive = options.directive || 'validate'
  var path = Vue.parsers.path
  var util = Vue.util

  function getVal (obj, keypath) {
    var ret = null
    try {
      ret = path.get(obj, keypath)
    } catch (e) { }
    return ret
  }


  Vue.directive(directiveName, {

    priority: 1024,

    bind: function () {
      var vm = this.vm
      var el = this.el
      var $validator = vm[componentName]
      var keypath = this._keypath = this._parseModelAttribute(el.getAttribute(Vue.config.prefix + 'model'))
      var validator = this.arg ? this.arg : this.expression
      var arg = this.arg ? this.expression : null

      var customs = (vm.$options.validator && vm.$options.validator.validates) || {}
      if (!this._checkDirective(validator, validates, customs)) {
        _.warn('specified invalid v-validate directive !! please check v-validator directive !!')
        this._ignore = true
        return
      }

      if (!$validator) {
        vm[componentName] = $validator = vm.$addChild({
          validator: vm.$options.validator
        }, Vue.extend(require('./lib/validator')))
      }

      var value = el.getAttribute('value')
      if (el.getAttribute('number') !== null) {
        value = util.toNumber(value)
      }
      this._init = value

      var validation = $validator._getValidationNamespace('validation')
      var init = value || vm.$get(keypath)
      var readyEvent = el.getAttribute('wait-for')

      if (readyEvent && !$validator._isRegistedReadyEvent(keypath)) {
        $validator._addReadyEvents(keypath, this._checkParam('wait-for'))
      }
      
      this._setupValidator($validator, keypath, validation, validator, el, arg, init)
    },

    update: function (val, old) {
      if (this._ignore) { return }

      var self = this
      var vm = this.vm
      var keypath = this._keypath
      var validator = this.arg ? this.arg : this.expression
      var $validator = vm[componentName]

      $validator._changeValidator(keypath, validator, val)
      if (!$validator._isRegistedReadyEvent(keypath)) { // normal
        this._updateValidator($validator, validator, keypath)
      } else { // wait-for
        vm.$once($validator._getReadyEvents(keypath), function (val) {
          $validator._setInitialValue(keypath, val)
          vm.$set(keypath, val)
          self._updateValidator($validator, validator, keypath)
        })
      }
    },

     
    unbind: function () {
      if (this._ignore) { return }

      var vm = this.vm
      var keypath = this._keypath
      var validator = this.arg ? this.arg : this.expression
      var $validator = vm[componentName]

      this._teardownValidator(vm, $validator, keypath, validator)
    },

    _parseModelAttribute: function (attr) {
      var res = Vue.parsers.directive.parse(attr)
      return res[0].arg ? res[0].arg : res[0].expression
    },

    _checkDirective: function (validator, validates, customs) {
      var items = Object.keys(validates).concat(Object.keys(customs))
      return items.some(function (item) {
        return item === validator
      })
    },

    _setupValidator: function ($validator, keypath, validation, validator, el, arg, init) {
      var vm = this.vm

      if (!getVal($validator[validation], keypath)) {
        $validator._defineModelValidationScope(keypath)
        if (el.tagName === 'INPUT' && el.type === 'radio') {
          if (getVal(vm, keypath) === init) {
            $validator._setInitialValue(keypath, init)
          }
        } else {
          $validator._setInitialValue(keypath, init)
        }
      }

      if (!getVal($validator[validation], [keypath, validator].join('.'))) {
        $validator._defineValidatorToValidationScope(keypath, validator)
        $validator._addValidator(keypath, validator, getVal(vm, arg) || arg)
      }
    },

    _updateValidator: function ($validator, validator, keypath) {
      var value = $validator.$get(keypath)
      var el = this.el

      if (this._init) {
        value = this._init
        delete this._init
      }

      if (el.tagName === 'INPUT' && el.type === 'radio') {
        if (value === $validator.$get(keypath)) {
          $validator._updateDirtyProperty(keypath, value)
        }
      } else {
        $validator._updateDirtyProperty(keypath, value)
      }

      $validator._doValidate(keypath, validator, $validator.$get(keypath))
    },

    _teardownValidator: function (vm, $validator, keypath, validator) {
      $validator._undefineValidatorToValidationScope(keypath, validator)
      $validator._undefineModelValidationScope(keypath, validator)
    }
  })
}

},{"./lib/util":124,"./lib/validates":125,"./lib/validator":126}],124:[function(require,module,exports){
/**
 * Utilties
 */


/**
 * warn
 *
 * @param {String} msg
 * @param {Error} [err]
 *
 */

exports.warn = function (msg, err) {
  if (window.console) {
    console.warn('[vue-validator] ' + msg)
    if (err) {
      console.warn(err.stack)
    }
  }
}

/**
 * Get target validatable object
 *
 * @param {Object} validation
 * @param {String} keypath
 * @return {Object} validatable object
 */

exports.getTarget = function (validation, keypath) {
  var last = validation
  var keys = keypath.split('.')
  var key, obj
  for (var i = 0; i < keys.length; i++) {
    key = keys[i]
    obj = last[key]
    last = obj
    if (!last) {
      break
    }
  }
  return last
}

},{}],125:[function(require,module,exports){
/**
 * Fundamental validate functions
 */


/**
 * required
 *
 * This function validate whether the value has been filled out.
 *
 * @param val
 * @return {Boolean}
 */

function required (val) {
  if (Array.isArray(val)) {
    return val.length > 0
  } else if (typeof val === 'number') {
    return true
  } else if ((val !== null) && (typeof val === 'object')) {
    return Object.keys(val).length > 0
  } else {
    return !val
      ? false
      : true
  }
}


/**
 * pattern
 *
 * This function validate whether the value matches the regex pattern
 *
 * @param val
 * @param {String} pat
 * @return {Boolean}
 */

function pattern (val, pat) {
  if (typeof pat !== 'string') { return false }

  var match = pat.match(new RegExp('^/(.*?)/([gimy]*)$'))
  if (!match) { return false }

  return new RegExp(match[1], match[2]).test(val)
}


/**
 * minLength
 *
 * This function validate whether the minimum length of the string.
 *
 * @param {String} val
 * @param {String|Number} min
 * @return {Boolean}
 */

function minLength (val, min) {
  return typeof val === 'string' &&
    isInteger(min, 10) &&
    val.length >= parseInt(min, 10)
}


/**
 * maxLength
 *
 * This function validate whether the maximum length of the string.
 *
 * @param {String} val
 * @param {String|Number} max
 * @return {Boolean}
 */

function maxLength (val, max) {
  return typeof val === 'string' &&
    isInteger(max, 10) &&
    val.length <= parseInt(max, 10)
}


/**
 * min
 *
 * This function validate whether the minimum value of the numberable value.
 *
 * @param {*} val
 * @param {*} arg minimum
 * @return {Boolean}
 */

function min (val, arg) {
  return !isNaN(+(val)) && !isNaN(+(arg)) && (+(val) >= +(arg))
}


/**
 * max
 *
 * This function validate whether the maximum value of the numberable value.
 *
 * @param {*} val
 * @param {*} arg maximum
 * @return {Boolean}
 */

function max (val, arg) {
  return !isNaN(+(val)) && !isNaN(+(arg)) && (+(val) <= +(arg))
}


/**
 * isInteger
 *
 * This function check whether the value of the string is integer.
 *
 * @param {String} val
 * @return {Boolean}
 * @private
 */

function isInteger (val) {
  return /^(-?[1-9]\d*|0)$/.test(val)
}


/**
 * export(s)
 */
module.exports = {
  required: required,
  pattern: pattern,
  minLength: minLength,
  maxLength: maxLength,
  min: min,
  max: max
}

},{}],126:[function(require,module,exports){
/**
 * Import(s)
 */

var validates = require('./validates')
var _ = require('./util')


/**
 * Export(s)
 */


/**
 * `v-validator` component with mixin
 */

module.exports = {
  inherit: true,

  created: function () {
    this._initValidationVariables()
    this._initOptions()
    this._mixinCustomValidates()
    this._defineProperties()
    this._defineValidationScope()
  },

  methods: {
    _getValidationNamespace: function (key) {
      return this.$options.validator.namespace[key]
    },

    _initValidationVariables: function () {
      this._validators = {}
      this._validates = {}
      this._initialValues = {}
      for (var key in validates) {
        this._validates[key] = validates[key]
      }
      this._validatorWatchers = {}
      this._readyEvents = {}
    },

    _initOptions: function () {
      var validator = this.$options.validator = this.$options.validator || {}
      var namespace = validator.namespace = validator.namespace || {}
      namespace.validation = namespace.validation || 'validation'
      namespace.valid = namespace.valid || 'valid'
      namespace.invalid = namespace.invalid || 'invalid'
      namespace.dirty = namespace.dirty || 'dirty'
    },

    _mixinCustomValidates: function () {
      var customs = this.$options.validator.validates
      for (var key in customs) {
        this._validates[key] = customs[key]
      }
    },

    _defineValidProperty: function (target, getter) {
      Object.defineProperty(target, this._getValidationNamespace('valid'), {
        enumerable: true,
        configurable: true,
        get: getter
      })
    },

    _undefineValidProperty: function (target) {
      delete target[this._getValidationNamespace('valid')]
    },

    _defineInvalidProperty: function (target) {
      var self = this
      Object.defineProperty(target, this._getValidationNamespace('invalid'), {
        enumerable: true,
        configurable: true,
        get: function () {
          return !target[self._getValidationNamespace('valid')]
        }
      })
    },

    _undefineInvalidProperty: function (target) {
      delete target[this._getValidationNamespace('invalid')]
    },

    _defineDirtyProperty: function (target, getter) {
      Object.defineProperty(target, this._getValidationNamespace('dirty'), {
        enumerable: true,
        configurable: true,
        get: getter
      })
    },

    _undefineDirtyProperty: function (target) {
      delete target[this._getValidationNamespace('dirty')]
    },

    _defineProperties: function () {
      var self = this

      var walk = function (obj, propName, namespaces) {
        var ret = false
        var keys = Object.keys(obj)
        var i = keys.length
        var key, last
        while (i--) {
          key = keys[i]
          last = obj[key]
          if (!(key in namespaces) && typeof last === 'object') {
            ret = walk(last, propName, namespaces)
            if ((propName === self._getValidationNamespace('valid') && !ret) ||
                (propName === self._getValidationNamespace('dirty') && ret)) {
              break
            }
          } else if (key === propName && typeof last !== 'object') {
            ret = last
            if ((key === self._getValidationNamespace('valid') && !ret) ||
                (key === self._getValidationNamespace('dirty') && ret)) {
              break
            }
          }
        }
        return ret
      }

      this._defineValidProperty(this.$parent, function () {
        var validationName = self._getValidationNamespace('validation')
        var validName = self._getValidationNamespace('valid')
        var namespaces = self.$options.validator.namespace

        return walk(this[validationName], validName, namespaces)
      })

      this._defineInvalidProperty(this.$parent)

      this._defineDirtyProperty(this.$parent, function () {
        var validationName = self._getValidationNamespace('validation')
        var dirtyName = self._getValidationNamespace('dirty')
        var namespaces = self.$options.validator.namespace

        return walk(this[validationName], dirtyName, namespaces)
      })
    },

    _undefineProperties: function () {
      this._undefineDirtyProperty(this.$parent)
      this._undefineInvalidProperty(this.$parent)
      this._undefineValidProperty(this.$parent)
    },

    _defineValidationScope: function () {
      this.$parent.$add(this._getValidationNamespace('validation'), {})
    },

    _undefineValidationScope: function () {
      var validationName = this._getValidationNamespace('validation')
      this.$parent.$delete(validationName)
    },

    _defineModelValidationScope: function (keypath) {
      var self = this
      var validationName = this._getValidationNamespace('validation')
      var dirtyName = this._getValidationNamespace('dirty')

      var keys = keypath.split('.')
      var last = this[validationName]
      var obj, key
      for (var i = 0; i < keys.length; i++) {
        key = keys[i]
        obj = last[key]
        if (!obj) {
          obj = {}
          last.$add(key, obj)
        }
        last = obj
      }
      last.$add(dirtyName, false)

      this._defineValidProperty(last, function () {
        var ret = true
        var validators = self._validators[keypath]
        var i = validators.length
        var validator
        while (i--) {
          validator = validators[i]
          if (last[validator.name]) {
            ret = false
            break
          }
        }
        return ret
      })
      this._defineInvalidProperty(last)
      
      this._validators[keypath] = []

      this._watchModel(keypath, function (val, old) {
        self._updateDirtyProperty(keypath, val)
        self._validators[keypath].forEach(function (validator) {
          self._doValidate(keypath, validator.name, val)
        })
      })
    },

    _undefineModelValidationScope: function (keypath, validator) {
      if (this.$parent) {
        var targetPath = [this._getValidationNamespace('validation'), keypath].join('.')
        var target = this.$parent.$get(targetPath)
        if (target && Object.keys(target).length === 3 &&
            this._getValidationNamespace('valid') in target &&
            this._getValidationNamespace('invalid') in target &&
            this._getValidationNamespace('dirty') in target) {
          this._unwatchModel(keypath)
          this._undefineDirtyProperty(target)
          this._undefineInvalidProperty(target)
          this._undefineValidProperty(target)
          removeValidationProperties(
            this.$parent.$get(this._getValidationNamespace('validation')),
            keypath
          )
        }
      }
    },

    _defineValidatorToValidationScope: function (keypath, validator) {
      var target = _.getTarget(this[this._getValidationNamespace('validation')], keypath)
      target.$add(validator, null)
    },

    _undefineValidatorToValidationScope: function (keypath, validator) {
      var validationName = this._getValidationNamespace('validation')
      if (this.$parent) {
        var targetPath = [validationName, keypath].join('.')
        var target = this.$parent.$get(targetPath)
        if (target) {
          target.$delete(validator)
        }
      }
    },

    _getInitialValue: function (keypath) {
      return this._initialValues[keypath]
    },

    _setInitialValue: function (keypath, val) {
      this._initialValues[keypath] = val
    },

    _addValidator: function (keypath, validator, arg) {
      this._validators[keypath].push({ name: validator, arg: arg })
    },

    _changeValidator: function (keypath, validator, arg) {
      var validators = this._validators[keypath]
      var i = validators.length
      while (i--) {
        if (validators[i].name === validator) {
          validators[i].arg = arg
          break
        }
      }
    },

    _findValidator: function (keypath, validator) {
      var found = null
      var validators = this._validators[keypath]
      var i = validators.length
      while (i--) {
        if (validators[i].name === validator) {
          found = validators[i]
          break
        }
      }
      return found
    },

    _watchModel: function (keypath, fn) {
      this._validatorWatchers[keypath] =
        this.$watch(keypath, fn, { deep: false, immediate: true })
    },

    _unwatchModel: function (keypath) {
      var unwatch = this._validatorWatchers[keypath]
      if (unwatch) {
        unwatch()
        delete this._validatorWatchers[keypath]
      }
    },
    
    _addReadyEvents: function (id, event) {
      this._readyEvents[id] = event
    },

    _getReadyEvents: function (id) {
      return this._readyEvents[id]
    },

    _isRegistedReadyEvent: function (id) {
      return id in this._readyEvents
    },

    _updateDirtyProperty: function (keypath, val) {
      var validationName = this._getValidationNamespace('validation')
      var dirtyName = this._getValidationNamespace('dirty')

      var target = _.getTarget(this[validationName], keypath)
      if (target) {
        target.$set(dirtyName, this._getInitialValue(keypath) !== val)
      }
    },

    _doValidate: function (keypath, validateName, val) {
      var validationName = this._getValidationNamespace('validation')

      var target = _.getTarget(this[validationName], keypath)
      var validator = this._findValidator(keypath, validateName)
      if (target && validator) {
        this._invokeValidator(
          this._validates[validateName],
          val, validator.arg,
          function (result) {
            target.$set(validateName, !result)
          })
      }
    },
    
    _invokeValidator: function (validator, val, arg, fn) {
      var future = validator.call(this, val, arg)
      if (typeof future === 'function') { // async
        if (future.resolved) {
          // cached
          fn(future.resolved)
        } else if (future.requested) {
          // pool callbacks
          future.pendingCallbacks.push(fn)
        } else {
          future.requested = true
          var fns = future.pendingCallbacks = [fn]
          future(function resolve () {
            future.resolved = true
            for (var i = 0, l = fns.length; i < l; i++) {
              fns[i](true)
            }
          }, function reject () {
            fn(false)
          })
        }
      } else { // sync
        fn(future)
      }
    }
  }
}

/**
 * Remove properties from target validation
 *
 * @param {Object} validation
 * @param {String} keypath
 */

function removeValidationProperties (validation, keypath) {
  var keys = keypath.split('.')
  var key, obj
  while (keys.length) {
    key = keys.pop()
    if (keys.length !== 0) {
      obj = _.getTarget(validation, keys.join('.'))
      obj.$delete(key)
    } else {
      validation.$delete(key)
    }
  }
}

},{"./util":124,"./validates":125}]},{},[97]);
