(function ($) {
    "use strict";

    $.validator.addMethod(
        "unique_email",
        function(value, element, params) {
            return $.validator.methods.remote.call(this, value, element, {
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'unique_email',
                    email: value
                }
            });
        },
        "That email is already in use on this site."
    );

    $.validator.addMethod(
        "unique_username",
        function(value, element, params) {
            return $.validator.methods.remote.call(this, value, element, {
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'unique_username',
                    username: value
                }
            });
        },
        "That username is already in use on this site."
    );

    var ZippyCoursesUX = {
        forms: {
            init: function() {
                $('.zippy-form').each(function(index, element) {
                    $(element).validate();

                    $(element).find('input').each(function(index, element) {
                        var rules = $(element).data('rules');

                        if(rules !== undefined) {
                            $(element).rules('add', rules, 'Fpp');
                        }
                    });
                });
            }
        },
        tabs: function() {
            var that = this;

            $('.zippy-post-type-tabs').on('click', 'a', function(e) {
                e.preventDefault();
            });

            $('.zippy-ux-tabs, .zippy-tabs-list').on('click', 'li', function() {
                $(this).addClass('active').siblings().removeClass('active');

                $(this).siblings().each(function() {
                    var children = $(this).data('children');
                    $(children).hide();
                });

                var children = $(this).data('children');
                if (children) {
                    $(children).show(0, function() {
                        var $visibleSideSortable = $( "div[id$='side-sortables']:visible" );

                        if($visibleSideSortable.length) {
                            $('#submitdiv').prependTo($visibleSideSortable);                            
                        }
                    });
                }

                var parent = $(this).parents('.zippy-tabs-container');
                var tab_id = $(this).data('tabId');
                if (tab_id) {
                    $(parent).find('.zippy-tab').hide();
                    $(parent).find('[data-tab-id="' + tab_id + '"]').show();
                }

                if ($(this).data('children') == '#zippy_course_public_details_region') {
                    that.togglePublicDetails();
                }
            });

            $('.zippy-post-type-tabs .active').click();
        },
        panels: function() {
            $('.zippy-panels-list').on('click', 'li a', function(e) {
                e.preventDefault();
                
                var parent = $(this).parents('.zippy-panels-container');
                var panel_id = $(this).data('panelId');

                $(parent).find('.zippy-panels-list a.active').removeClass('active');
                $(this).addClass('active');

                if (panel_id) {
                    $(parent).find('.zippy-panels .zippy-panel').removeClass('active');
                    $(parent).find('[data-panel-id="' + panel_id + '"]').addClass('active');
                }
            });
        },
        togglePublicDetails: function() {
            var exempt = ['zippy-course-public-enable', 'submitdiv'];

            if ($('#zippy-enable-public-details').is(':checked')) {
                $('#zippy_course_public_details_main-sortables .postbox, #zippy_course_public_details_side-sortables .postbox').each(function(index, element) {
                    $(element).removeClass('zippy-disabled-mb');                        
                });
            } else {
                $('#zippy_course_public_details_main-sortables .postbox, #zippy_course_public_details_side-sortables .postbox').each(function(index, element) {
                    if (exempt.indexOf($(element).attr('id')) === -1) {
                        $(element).addClass('zippy-disabled-mb');
                    }
                });
            }
        },
        course: {
            init: function() {
                if ($('#post_type').val() == 'course' && ($('#post_status').val() == 'publish' || $('#post_status').val() == 'draft')) {
                    var post_id = $('#post_ID').val();
                    $('.page-title-action').after('<a href="post.php?post=' + post_id + '&action=duplicate" class="page-title-action">Duplicate Course</a>');
                }
            }            
        },
        entries: {
            setReturn: function(e) {
               $( this ).before( '<input type="hidden" name="zippy_return" value="1" />' );
            }
        },
        events: function() {
            $('#zippy-enable-public-details').on('click', this.togglePublicDetails);
            $('.zippy-save-and-return .button-primary').on('click', this.entries.setReturn);
        },

        buttons: function() {
            $('#zippy-courses-samcart-key-reset-button').on('click', this.clearSamCartAPIKey);
        },
        clearSamCartAPIKey: function(e) {
            $('input[name="zippy_samcart_payment_gateway_settings[api_key]"]').val(null);
        },
        init: function()
        {
            this.events();
            this.tabs();
            this.forms.init();
            this.course.init();
            this.panels();
            this.buttons();
            this.togglePublicDetails();
        }
    }
    ZippyCoursesUX.init();
    
    $( document )
        .on( 'dfw-activate', function() {
            $('.zippy-ux-tabs, .zippy-tabs-list').addClass('invis');
        } )
        .on( 'dfw-deactivate', function() {
            $('.zippy-ux-tabs, .zippy-tabs-list').removeClass('invis');
        } )
        .on( 'dfw-on', function() {
            $('.zippy-ux-tabs, .zippy-tabs-list').addClass('invis');
        } )
        .on( 'dfw-off', function() {
            $('.zippy-ux-tabs, .zippy-tabs-list').removeClass('invis');
        } );

}(jQuery));