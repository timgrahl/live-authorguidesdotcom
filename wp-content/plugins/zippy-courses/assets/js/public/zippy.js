(function ($) {
    "use strict";

    $.validator.addMethod(
        "unique_email", 
        function(value, element, params) {
            var remote = $.validator.methods.remote.call(this, value, element, {
                url: Zippy.ajaxurl,
                type: 'POST',
                data: {
                    action: 'unique_email',
                    email: value
                }
            });
            if (!remote) {
                return true;
            } else {
                return remote;
            }
        },
        "That email is already in use on this site."
    );

    $.validator.addMethod(
        "unique_username",
        function(value, element, params) {
            var remote = $.validator.methods.remote.call(this, value, element, {
                url: Zippy.ajaxurl,
                type: 'POST',
                data: {
                    action: 'unique_username',
                    username: value
                }
            });
            if (!remote) {
                return true;
            } else {
                return remote;
            }
        
        },
        "That username is already in use on this site."
    );
   
    var ZippyCourses = {
        cookies: Cookies.noConflict(),
        analytics: {
            event: {
                ID: Zippy.analytics_id,
                duration: 0
            },
            startTime: new Date(),
            trackTime: function() {
                var end = new Date(),
                    duration = end - ZippyCourses.analytics.startTime,
                    cookie = ZippyCourses.analytics.setupCookie(),
                    analytics = cookie.analytics,
                    record = _.findWhere(analytics, {ID : ZippyCourses.analytics.event.ID });

                if (record !== undefined) {
                    record.duration = duration;
                } else {
                    ZippyCourses.analytics.event.duration = duration;
                    cookie.analytics.push(ZippyCourses.analytics.event);
                }

                ZippyCourses.cookies.set('zippy', cookie);
            },
            init: function() {
                window.setInterval(ZippyCourses.analytics.trackTime, 1000);
            },
            setupCookie: function() {
                var cookie = ZippyCourses.cookies.get('zippy');
                
                var data = {
                    analytics: []
                };

                return cookie !== undefined && cookie.analytics !== undefined ? JSON.parse(cookie) : data;
            }
        },
        ux: {
            forms: {
                init: function() {

                    $('.zippy-form').each(function(index, element) {
                        $(element).validate();

                        $(element).find('input').each(function(index, element) {
                            var rules = $(element).data('rules');

                            if(rules !== undefined) {
                                $(element).rules('add', rules, 'Fpp');
                            }
                        });
                    });
                }
            },
            init: function()
            {
                this.forms.init();
            }
        },
        init: function()
        {
            this.ux.init();
            this.analytics.init();
        }
    };

    ZippyCourses.init();
   
}(jQuery));