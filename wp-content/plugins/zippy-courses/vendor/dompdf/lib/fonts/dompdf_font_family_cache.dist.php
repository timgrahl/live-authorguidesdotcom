<?php return array (
  'serif' => array(
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'pinyon script' => array(
    'normal' => DOMPDF_FONT_DIR . '72fd9a700393775c2d6a6a99cd4ddfab',
  ),
) ?>