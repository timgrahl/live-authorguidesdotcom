<?php

class ZippyCourses_PostTypes
{
    public function __construct()
    {
        $this->addCoursePostType();
        $this->addUnitPostType();
        $this->addLessonPostType();
        $this->addQuizPostType();
        $this->addQuizQuestionPostType();
        $this->addProductPostType();
        $this->addOrderPostType();
        $this->addTransactionPostType();
    }

    private function addCoursePostType()
    {
        $zippy = Zippy::instance();
        
        $cpt = $zippy->make('post_type', array('course', __('Course', ZippyCourses::TEXTDOMAIN), __('Courses', ZippyCourses::TEXTDOMAIN)));

        $cpt->setParentMenu('zippy-courses');
        $cpt->setSupports(apply_filters('zippy_courses_cpt_course_support', array('title', 'editor')));
        $cpt->setShowInMenu(false);
        $cpt->setHasArchive(false);

        $zippy->post_types->add($cpt);

        return $cpt;
    }

    private function addUnitPostType()
    {
        $zippy = Zippy::instance();
        
        $cpt = $zippy->make('post_type', array('unit', __('Unit', ZippyCourses::TEXTDOMAIN), __('Units', ZippyCourses::TEXTDOMAIN)));

        $cpt->setParentMenu('zippy-courses');
        $cpt->setSupports(apply_filters('zippy_courses_cpt_unit_support',array('title', 'editor', 'excerpt', 'comments')));
        $cpt->setShowInMenu(false);
        $cpt->setHasArchive(false);
        
        $zippy->post_types->add($cpt);

        return $cpt;
    }

    private function addLessonPostType()
    {
        $zippy = Zippy::instance();
        
        $cpt = $zippy->make('post_type', array('lesson', __('Lesson', ZippyCourses::TEXTDOMAIN), __('Lessons', ZippyCourses::TEXTDOMAIN)));

        $cpt->setParentMenu('zippy-courses');
        $cpt->setSupports(apply_filters('zippy_courses_cpt_lesson_support', array('title', 'editor', 'excerpt', 'comments')));
        $cpt->setShowInMenu(false);
        $cpt->setHasArchive(false);

        $zippy->post_types->add($cpt);

        return $cpt;
    }

    private function addQuizPostType()
    {
        $zippy = Zippy::instance();
        
        $cpt = $zippy->make('post_type', array('quiz', __('Quiz', ZippyCourses::TEXTDOMAIN), __('Quizzes', ZippyCourses::TEXTDOMAIN)));

        $cpt->setParentMenu('zippy-courses');
        $cpt->setSupports(array('title'));
        $cpt->setExcludeFromSearch(true);
        $cpt->setHasArchive(false);

        $action_column = new ZippyCourses_QuizActions_ListTableColumn(
            'actions',
            __('Actions', ZippyCourses::TEXTDOMAIN)
        );
            $action_column->setPosition('date');
            $action_column->setPositionMethod('before');

        
        $score_column = new ZippyCourses_QuizScore_ListTableColumn(
            'score',
            __('Average Score', ZippyCourses::TEXTDOMAIN)
        );
            $score_column->setPosition('actions');
            $score_column->setPositionMethod('before');
            $score_column->setSortable(true);

        $entry_column = new ZippyCourses_QuizEntry_ListTableColumn(
            'course-entry',
            __('Course Entry', ZippyCourses::TEXTDOMAIN)
        );
            $entry_column->setPosition('score');
            $entry_column->setPositionMethod('before');

        $cpt->addTableColumn($action_column);
        $cpt->addTableColumn($score_column);
        $cpt->addTableColumn($entry_column);

        $zippy->post_types->add($cpt);

        return $cpt;
    }

    private function addQuizQuestionPostType()
    {
        $zippy = Zippy::instance();
        
        $cpt = $zippy->make('post_type', array('quiz_question', __('Quiz Question', ZippyCourses::TEXTDOMAIN), __('Quiz Questions', ZippyCourses::TEXTDOMAIN)));

        $cpt->setParentMenu('zippy-courses');
        $cpt->setSupports(array('title', 'editor'));
        $cpt->setShowInMenu(false);
        $cpt->setExcludeFromSearch(true);
        $cpt->setHasArchive(false);

        $zippy->post_types->add($cpt);

        return $cpt;
    }

    private function addOrderPostType()
    {
        $zippy = Zippy::instance();
        
        $cpt = $zippy->make('post_type', array('zippy_order', __('Order', ZippyCourses::TEXTDOMAIN), __('Orders', ZippyCourses::TEXTDOMAIN)));

        $cpt->setParentMenu('zippy-courses');
        $cpt->setSupports(array('title'));
        $cpt->setExcludeFromSearch(true);
        $cpt->setHasArchive(false);
        $cpt->setPublic(false);
        $cpt->setRewrite('orders');
        $cpt->setCapabilityType('page');

        $total_column = new ZippyCourses_OrderTotal_ListTableColumn(
            'total',
            __('Total', ZippyCourses::TEXTDOMAIN)
        );
            $total_column->setPosition('date');
            $total_column->setPositionMethod('before');

        
        $type_column = new ZippyCourses_OrderType_ListTableColumn(
            'type',
            __('Type', ZippyCourses::TEXTDOMAIN)
        );
            $type_column->setPosition('total');
            $type_column->setPositionMethod('before');

        $product_column = new ZippyCourses_OrderProduct_ListTableColumn(
            'product',
            __('Product', ZippyCourses::TEXTDOMAIN)
        );
            $product_column->setPosition('type');
            $product_column->setPositionMethod('before');


        $customer_column = new ZippyCourses_OrderCustomer_ListTableColumn(
            'customer',
            __('Customer', ZippyCourses::TEXTDOMAIN)
        );
            $customer_column->setPosition('product');
            $customer_column->setPositionMethod('before');

        $cpt->addTableColumn($total_column);
        $cpt->addTableColumn($type_column);
        $cpt->addTableColumn($product_column);
        $cpt->addTableColumn($customer_column);

        $product_filter = new ZippyCourses_OrderProduct_ListTableFilter(
            'product',
            __('Filter by Product...', ZippyCourses::TEXTDOMAIN)
        );

        $student_filter = new ZippyCourses_OrderStudent_ListTableFilter(
            'student',
            __('Filter by Student...', ZippyCourses::TEXTDOMAIN)
        );

        $cpt->addTableFilter($product_filter);
        $cpt->addTableFilter($student_filter);

        $zippy->post_types->add($cpt);

        return $cpt;
    }

    private function addTransactionPostType()
    {
        $zippy = Zippy::instance();
        
        $cpt = $zippy->make('post_type', array('transaction', __('Transaction', ZippyCourses::TEXTDOMAIN), __('Transactions', ZippyCourses::TEXTDOMAIN)));

        $cpt->setParentMenu('zippy-courses');
        $cpt->setSupports(array('title'));
        $cpt->setShowInMenu(false);
        $cpt->setExcludeFromSearch(true);
        $cpt->setHasArchive(false);

        $zippy->post_types->add($cpt);

        return $cpt;
    }

    private function addProductPostType()
    {
        $zippy = Zippy::instance();
        
        $cpt = $zippy->make('post_type', array('product', __('Product', ZippyCourses::TEXTDOMAIN), __('Products', ZippyCourses::TEXTDOMAIN)));

        $cpt->setParentMenu('zippy-courses');
        $cpt->setExcludeFromSearch(true);
        $cpt->setHasArchive(false);
        $cpt->setSupports(apply_filters('zippy_courses_cpt_product_support', array('title', 'editor')));

        $price_column = new ZippyCourses_ProductPrice_ListTableColumn(
            'price',
            __('Price', ZippyCourses::TEXTDOMAIN)
        );
            $price_column->setPosition('title');
            $price_column->setPositionMethod('after');

        
        $type_column = new ZippyCourses_ProductType_ListTableColumn(
            'type',
            __('Type', ZippyCourses::TEXTDOMAIN)
        );
            $type_column->setPosition('price');
            $type_column->setPositionMethod('after');

        $course_column = new ZippyCourses_ProductCourse_ListTableColumn(
            'course',
            __('Course', ZippyCourses::TEXTDOMAIN)
        );
            $course_column->setPosition('type');
            $course_column->setPositionMethod('after');


        $orders_column = new ZippyCourses_ProductOrders_ListTableColumn(
            'orders',
            __('No. of Orders', ZippyCourses::TEXTDOMAIN)
        );
            $orders_column->setPosition('course');
            $orders_column->setPositionMethod('after');

        $cpt->addTableColumn($price_column);
        $cpt->addTableColumn($type_column);
        $cpt->addTableColumn($course_column);
        $cpt->addTableColumn($orders_column);

        $course_filter = new ZippyCourses_ProductCourse_ListTableFilter(
            'course',
            __('Filter by Course...', ZippyCourses::TEXTDOMAIN)
        );

        $cpt->addTableFilter($course_filter);

        $zippy->post_types->add($cpt);
        
        return $cpt;
    }
}
