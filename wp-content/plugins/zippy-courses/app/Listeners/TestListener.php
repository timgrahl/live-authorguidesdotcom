<?php

class ZippyCourses_TestListener extends Zippy_Listener
{
    public function __construct()
    {
        $this->register();
    }

    protected function register()
    {
        $zippy = Zippy::instance();
        $zippy->events->register('NewStudent', $this);
    }

    public function handle(Zippy_Event $event)
    {
        // zdd($event);
    }
}
