<?php

class ZippyCourses_ProductListener extends Zippy_Listener
{
    public function __construct()
    {
        $this->register();
    }

    protected function register()
    {
        $zippy = Zippy::instance();
        $zippy->events->register('JoinProduct', $this);
        $zippy->events->register('LeaveProduct', $this);
    }

    public function handle(Zippy_Event $event)
    {
        $method = 'handle' . $event->getEventName();

        if (method_exists($this, $method)) {
            call_user_func_array(array($this, $method), array($event));
        }
    }

    public function handleJoinProduct(ZippyCourses_JoinProduct_Event $event)
    {
        $product = $event->product;
        $student = $event->student;
        if ($product->hasRevokedCourse()) {
            $this->modifyCourseAccess('revoke', $product, $student);
        }
    }

    public function handleLeaveProduct(ZippyCourses_LeaveProduct_Event $event)
    {
        $product = $event->product;
        $student = $event->student;

        if ($product->hasRevokedCourse()) {
            $this->modifyCourseAccess('restore', $product, $student);
        }
    }

    protected function modifyCourseAccess($action, Zippy_Product $product, ZippyCourses_Student $student)
    {
        $revoked_access = $product->getRevokedCourses();
        $course_id = intval($revoked_access->course);
        $tier_id = intval($revoked_access->tier);

        if ($action == 'revoke') {
            $student->removeTierAccess($course_id, $tier_id);
        } elseif ($action == 'restore') {
            $student->restoreTierAccess($course_id, $tier_id);
        }
    }
}
