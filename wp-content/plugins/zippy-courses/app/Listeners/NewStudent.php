<?php


class ZippyCourses_NewStudentListener extends Zippy_Listener
{
    public function __construct()
    {
        $this->register();
    }

    protected function register()
    {
        $zippy = Zippy::instance();
        $zippy->events->register('NewStudent', $this);
    }

    public function handle(Zippy_Event $event)
    {
        $this->registerAllUnclaimedOrders($event);
    }

    public function registerAllUnclaimedOrders(Zippy_Event $event)
    {
        $zippy = Zippy::instance();
        $new_student_id = $event->student->getId();
        $orders = $zippy->make('orders');
        $student = $zippy->make('student', array($new_student_id));

        $unclaimed_orders = $orders->getUnclaimedOrdersByEmail($student->getEmail());

        foreach ($unclaimed_orders as $order) {
            $order->setOwner($new_student_id);
            $order->save();
        }
    }
}
