<?php

class ZippyCourses_License
{
    const AUTHOR        = 'Zippy Courses';
    const STORE_URL     = 'https://zippycourses.com';
    const PRODUCT_NAME  = 'Zippy Courses Core Plugin';

    public $license_key;
    public $valid = false;

    public function __construct()
    {
        add_action('admin_init', array($this, 'checkLicenseStatus'));
    }

    public function getLicenseKey()
    {
        if ($this->license_key === null) {
            $settings = get_option('zippy_license_settings', array());
            $this->license_key =  isset($settings['license_key']) ? $settings['license_key'] : '';
        }

        return $this->license_key;
    }

    public function getLicense()
    {
        return get_option('zippy_license');
    }

    public function getInstalledVersionNumber()
    {
        return ZippyCourses::VERSION;
    }

    public function getLatestVersionNumber()
    {
        $zippy = Zippy::instance();

        if (($latest_version = get_transient('zippy_latest_version')) === false) {
            $latest_version = '0';
            $url = 'https://zippycourses.com/zippy-version.php';
            
            $response = wp_remote_get($url);

            if (!is_wp_error($response) && $response['response']['code'] == 200) {
                $body = json_decode($response['body']);
                
                $latest_version = isset($body->version) ? $body->version : '0';
            }
            
            set_transient('zippy_latest_version', $latest_version, 6 * HOUR_IN_SECONDS);
        }

        return $latest_version;
    }

    public function fetchLicenseStatus()
    {
        $zippy = Zippy::instance();

        $license_key = $this->getLicenseKey();
        if (empty($license_key)) {
            return;
        }
        
        $params = array(
            'edd_action'=> 'check_license',
            'license'   => $this->getLicenseKey(),
            'item_name' => urlencode(self::PRODUCT_NAME), // the name of our product in EDD,
            'url'       => home_url()
        );
        
        // Call the custom API.
        $response = wp_remote_get(add_query_arg($params, self::STORE_URL));

        if (!is_wp_error($response)) {
            $now = $zippy->utilities->datetime->getNow();

            $data =  json_decode(wp_remote_retrieve_body($response));

            update_option('zippy_license_check', $now->format('U'));

            return $this->_parseLicenseData($data);
        }

        return $response;
    }

    public function checkLicenseStatus()
    {
        $zippy = Zippy::instance();
        
        $now        = $zippy->utilities->datetime->getNow();

        $last_check = get_option('zippy_license_check', '');

        if (empty($last_check)) {
            $expires    = $zippy->utilities->datetime->getNow();
            $expires->modify('-1 hour');
        } else {
            $expires = $zippy->utilities->datetime->getDateFromTimestamp($last_check);
            $expires->modify('+36 hours');
        }

        if ($now->format('U') > $expires->format('U')) {
            $license = $this->fetchLicenseStatus();

            if (!is_wp_error($license)) {
                $this->processLicenseStatus($license);
            }
        } else {
            $license = (array) get_option('zippy_license', array());
            $this->processLicenseStatus($license);
        }
           
        return $this->isValid();
    }

    public function processLicenseStatus($license)
    {
        if (!isset($license['status'])) {
            $this->valid = false;
            return;
        }

        update_option('zippy_license', $license);

        switch ($license['status']) {
            case 'valid':
                $this->valid = true;
                break;
            case 'invalid':
                $this->valid = false;
                break;
            case 'site_inactive':
                $this->valid = false;
                break;
            case 'inactive':
                $license = $this->maybeActivate($license);
                break;
            default:
                break;
        }

        return $this->isValid();
    }

    public function isValid()
    {
        return $this->valid;
    }

    private function _parseLicenseData(stdClass $data)
    {
        $license = array();

        $license['status']         = $data->license;
        $license['expires']        = $data->expires;
        $license['payment_id']     = $data->payment_id;
        $license['customer_email'] = $data->customer_email;
        $license['customer_name']  = $data->customer_name;
        $license['limit']          = (int) $data->license_limit;
        $license['installations']  = $data->site_count;

        return $license;
    }

    public function activateLicenseKey()
    {
        $params = array(
            'edd_action'=> 'activate_license',
            'license'   => $this->getLicenseKey(),
            'item_name' => urlencode(self::PRODUCT_NAME), // the name of our product in EDD,
            'url'       => home_url()
        );
    
        $response = wp_remote_get(add_query_arg($params, self::STORE_URL));

        if (!is_wp_error($response) && $response['response']['code'] != 200) {
            $data =  json_decode(wp_remote_retrieve_body($response));
            return is_object($data) ? $this->_parseLicenseData($data) : new WP_Error("Could not activate license, please try again.");
        } else {
            return is_wp_error($response)
                ? $response
                : new WP_Error("Could not activate license, please try again.");
        }

        return $response;
    }

    public function maybeActivate($license)
    {
        if ($license['installations'] < $license['limit']) {
            $new_license = $this->activateLicenseKey();

            if (!is_wp_error($new_license)) {
                $this->processLicenseStatus($new_license);
            }
        }
    }

    public function getStatusMessage()
    {
        $zippy = Zippy::instance();

        $license        = $this->getLicense();
        $license_key    = $this->getLicenseKey();
        $message = '';

        if (empty($license)) {
            if (empty($license_key)) {
                return;
            } else {
                $this->checkLicenseStatus();
                $license = $this->getLicense();
            }
        }

        if (is_array($license) && $license['status'] == 'valid') {
            $expires = $zippy->utilities->datetime->getDate($license['expires']);
            $message = sprintf(__('Your license is valid! It expires on %s', ZippyCourses::TEXTDOMAIN), $expires->format('M d, Y'));
        } else {
            if ($license['status'] == 'invalid') {
                return __('Your license key is invalid.', ZippyCourses::TEXTDOMAIN);
            }

            $today   = $zippy->utilities->datetime->getToday();
            $expires = $zippy->utilities->datetime->getDate($license['expires']);
            
            if ($today->format('U') > $expires->format('U')) {
                $message = sprintf(__('Your license expired on %s.', ZippyCourses::TEXTDOMAIN), $expires->format('M d, Y'));
            } else {
                if ($license['installations'] >= $license['limit']) {
                    $message = __('Your license is in use on other sites and cannot be activated until the other sites are disabled.', ZippyCourses::TEXTDOMAIN);
                } else {
                    $message = __('Your license key is invalid.', ZippyCourses::TEXTDOMAIN);
                }
            }
        }
        
        return $message;
    }
}
