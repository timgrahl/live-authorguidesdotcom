<?php

class ZippyCourses_Routes
{
    /**
     * Singleton instance for our repository
     * @var ZippyCourses_Routes
     */
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        // add_filter( 'rewrite_rules_array', array( $this, 'register_rewrite_rules' ), 10, 1 );
        
        add_filter('query_vars', array($this, 'addQueryVars'));

        add_filter('rewrite_rules_array', array($this, 'addBuyRule'), 10, 1);
        add_filter('rewrite_rules_array', array($this, 'addRegisterRule'), 10, 1);
        add_filter('rewrite_rules_array', array($this, 'addPaymentNotificationRule'), 10, 1);
        add_filter('rewrite_rules_array', array($this, 'addApiRule'), 10, 1);
        add_filter('rewrite_rules_array', array($this, 'addSecureDownloadRule'), 10, 1);

        add_action('wp_loaded', array($this, 'refreshRules'));

        add_action('init', array($this, 'quizEndpoint'));
        add_action('init', array($this, 'certificateEndpoint'));

    }

    public function addQueryVars($vars)
    {
        $vars[] = 'buy';
        $vars[] = 'zippy_register';
        $vars[] = 'zippy_ipn';
        $vars[] = 'zippy_api_endpoint';
        $vars[] = 'zippy_api_version';
        $vars[] = 'zippy_file_uid';
        $vars[] = 'quiz';
        $vars[] = 'certificate';

        return $vars;
    }

    public function addBuyRule($rules)
    {
        $zippy = Zippy::instance();
        $slug = $zippy->core_pages->getSlug('buy');

        $new_rules = array();
        $new_rules['^' . $slug . '/([^/]+)$'] = 'index.php?pagename=' . $slug . '&buy=$matches[1]';
        
        return $new_rules + $rules;
    }

    public function addRegisterRule($rules)
    {
        $zippy = Zippy::instance();
        $slug = $zippy->core_pages->getSlug('register');

        $new_rules = array();
        $new_rules['^' . $slug . '/([^/]+)$'] = 'index.php?pagename=' . $slug . '&zippy_register=$matches[1]';
        
        return $new_rules + $rules;
    }

    public function addPaymentNotificationRule($rules)
    {
        
        $new_rules = array();
        $new_rules['^payment/notification/([^/]+)$'] = 'index.php?zippy_ipn=$matches[1]';

        return $new_rules + $rules;
    }

    public function addApiRule($rules)
    {
        
        $new_rules = array();
        $new_rules['^zippy-api/v([^/]+)/(.*)'] = 'index.php?zippy_api_version=$matches[1]&zippy_api_endpoint=$matches[2]';

        return $new_rules + $rules;
    }

    public function addSecureDownloadRule($rules)
    {
        
        $new_rules = array();
        $new_rules['^secure-download/([^/]+)$'] = 'index.php?zippy_file_uid=$matches[1]';

        return $new_rules + $rules;
    }

    public function refreshRules()
    {
        $zippy = Zippy::instance();

        $buy_slug = $zippy->core_pages->getSlug('buy');
        $reg_slug = $zippy->core_pages->getSlug('register');

        $rules = get_option('rewrite_rules');

        if (!isset($rules['^payment/notification/([^/]+)$']) ||
            !isset($rules['^secure-download/([^/]+)$']) ||
            !isset($rules['^zippy-api/v([^/]+)/(.*)']) ||
            !isset($rules['^' . $buy_slug . '/([^/]+)$']) ||
            !isset($rules['^' . $reg_slug . '/([^/]+)$']) || 
            !isset($rules['course/([^/]+)/certificate(/(.*))?/?$'])
        ) {
            global $wp_rewrite;
            $wp_rewrite->flush_rules();
        }
    }

    public function register_rewrite_rules($rules)
    {
        $slug = $zippy->core_pages->getSlug('register');

        $new_rules = array();
        $new_rules['^' . $slug . '/([^/]+)$'] = 'index.php?pagename=' . $slug . '&reg_id=$matches[1]';

        return $new_rules + $rules;
    }

    public function quizEndpoint()
    {
        add_rewrite_endpoint('quiz', EP_PAGES | EP_PERMALINK);
    }

    public function certificateEndpoint()
    {
        add_rewrite_endpoint('certificate', EP_PAGES | EP_PERMALINK);
    }
}
