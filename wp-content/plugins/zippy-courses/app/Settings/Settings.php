<?php

class ZippyCourses_Settings
{
    public function __construct()
    {
        if (is_admin()) {
            $this->createDefaults();
        }
    }

    private function createDefaults()
    {
        $this->createGeneralSettings();
        $this->createSystemEmailSettings();
        $this->createCorePagesSettings();
        $this->createPaymentGatewaySettings();
        $this->createEmailListSettings();
    }

    private function createGeneralSettings()
    {
        $zippy = Zippy::instance();
        
        $settings = get_option('zippy_advanced_settings', array());
        $api_key = isset($settings['api_key']) ? $settings['api_key'] :'';

        if (empty($api_key)) {
            $settings['api_key'] = Zippy_API::generateApiKey();
            update_option('zippy_advanced_settings', $settings);
        }

        $page = $zippy->settings_pages->fetch('zippy_settings_general');
            
            $section = $page->createSection('zippy_license_settings', __('Licensing', ZippyCourses::TEXTDOMAIN));
                $section->createField('license_key', __('License Key', ZippyCourses::TEXTDOMAIN));
                $section->createField('license_key_status', __('Status', ZippyCourses::TEXTDOMAIN), 'raw', '')
                        ->setValue($zippy->license->getStatusMessage());

                $api_key_description = '<p>' . $api_key . '</p><p><a href="' . admin_url('admin.php?page=zippy-settings&reset_api_key=1') . '" class="button">' . __('Reset API Key', ZippyCourses::TEXTDOMAIN) . '</a></p>';

            $section = $page->createSection('zippy_advanced_settings', __('Advanced', ZippyCourses::TEXTDOMAIN));
                $section->createField('api_key', __('API Key', ZippyCourses::TEXTDOMAIN), 'raw', '')
                        ->setValue($api_key_description);

    }

    private function createEmailListSettings()
    {
        $zippy = Zippy::instance();

        $page = $zippy->settings_pages->fetch('zippy_settings_email_lists');
    }

    private function createSystemEmailSettings()
    {
        $zippy = Zippy::instance();
        $this->prepareSystemEmails();


        $page = $zippy->settings_pages->fetch('zippy_settings_system_emails');

            $section = $page->createSection('zippy_system_emails_general_settings', __('General', ZippyCourses::TEXTDOMAIN));
                $section->createField('sender_email', __('Sender Email', ZippyCourses::TEXTDOMAIN));
                $section->createField('sender_name', __('Sender Name', ZippyCourses::TEXTDOMAIN));

            $section = $page->createSection('zippy_system_emails_registration_settings', __('Registration', ZippyCourses::TEXTDOMAIN));
                $section->createField('title', __('Title', ZippyCourses::TEXTDOMAIN));
                $section->createField('content', __('Content', ZippyCourses::TEXTDOMAIN), 'textarea');
                $section->createField('enabled', '', 'checkbox', array(1 => __('Send this email a student immediately after they register.', ZippyCourses::TEXTDOMAIN)));

            $section = $page->createSection('zippy_system_emails_forgot_password_settings', __('Forgot Password', ZippyCourses::TEXTDOMAIN));
                $section->createField('title', __('Title', ZippyCourses::TEXTDOMAIN));
                $section->createField('content', __('Content', ZippyCourses::TEXTDOMAIN), 'textarea');
                $section->createField('enabled', '', 'checkbox', array(1 => __('Send this email when a student attempts to reset their password.', ZippyCourses::TEXTDOMAIN)));

            $section = $page->createSection('zippy_system_emails_purchase_receipt_settings', __('Purchase Receipt', ZippyCourses::TEXTDOMAIN));
                $section->createField('title', __('Title', ZippyCourses::TEXTDOMAIN));
                $section->createField('content', __('Content', ZippyCourses::TEXTDOMAIN), 'textarea');
                $section->createField('enabled', '', 'checkbox', array(1 => __('Send this email to a customer after a purchase has been made.', ZippyCourses::TEXTDOMAIN)));

            $section = $page->createSection('zippy_system_emails_registration_reminder_settings', __('Registration Reminder', ZippyCourses::TEXTDOMAIN));
                $section->createField('title', __('Title', ZippyCourses::TEXTDOMAIN));
                $section->createField('content', __('Content', ZippyCourses::TEXTDOMAIN), 'textarea');
                $section->createField('enabled', '', 'checkbox', array(1 => __('Send this email after a Purchase, before Registration.', ZippyCourses::TEXTDOMAIN)));

            $section = $page->createSection('zippy_system_emails_new_student_settings', __('New Student Notification', ZippyCourses::TEXTDOMAIN));
                $section->createField('title', __('Title', ZippyCourses::TEXTDOMAIN));
                $section->createField('content', __('Content', ZippyCourses::TEXTDOMAIN), 'textarea');
                $section->createField('enabled', '', 'checkbox', array(1 => __('Send this email to the site administrator when a student joins the site.', ZippyCourses::TEXTDOMAIN)));

            $section = $page->createSection('zippy_system_emails_course_completion_settings', __('Course Completion Notification', ZippyCourses::TEXTDOMAIN));
                $section->createField('title', __('Title', ZippyCourses::TEXTDOMAIN));
                $section->createField('content', __('Content', ZippyCourses::TEXTDOMAIN), 'textarea');
                $section->createField('enabled', '', 'checkbox', array(1 => __('Send this email to the site administrator when a student completes a course.', ZippyCourses::TEXTDOMAIN)));

    }

    private function prepareSystemEmails()
    {
        $zippy = Zippy::instance();
        $email_settings = array(
            'zippy_system_emails_registration_settings',
            'zippy_system_emails_forgot_password_settings',
            'zippy_system_emails_purchase_receipt_settings',
            'zippy_system_emails_registration_reminder_settings',
            'zippy_system_emails_new_student_settings',
            'zippy_system_emails_course_completion_settings'
            );

        foreach ($email_settings as $email_option) {
            $setting = get_option($email_option, array());
            if ($zippy->utilities->email->emailExists($setting)) {
                continue;
            } else {
                $zippy->utilities->email->setDefaultEmail($email_option);
            }
        }
    }

    private function createPaymentGatewaySettings()
    {
        $zippy = Zippy::instance();

        $gateways = $zippy->make('payment');

        $options = $gateways->toArray('name');
        array_unshift($options, '');

        $page = $zippy->settings_pages->fetch('zippy_settings_payment');

            $section = $page->createSection('zippy_payment_general_settings', __('General', ZippyCourses::TEXTDOMAIN));
            $section->createField('payment_flow', __('Payment &amp; Registration', ZippyCourses::TEXTDOMAIN), 'select', array(
                '1' => __('Pay First', ZippyCourses::TEXTDOMAIN),
                '0' => __('Register First', ZippyCourses::TEXTDOMAIN),
            ));
            $section->createField('currency', __('Currency', ZippyCourses::TEXTDOMAIN), 'select', $zippy->currencies->getList());
            $section->createField('method', __('Payment Method', ZippyCourses::TEXTDOMAIN), 'select', $options);
    }

    private function createCorePagesSettings()
    {
        global $wpdb;

        $zippy = Zippy::instance();
        
        $invalid_post_statuses = array('"auto-draft"','"trash"', '"inherit"');
        $invalid_post_status_list = implode(', ', $invalid_post_statuses);

        $pages = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE post_type = 'page'  AND post_status NOT IN ($invalid_post_status_list) ORDER BY post_title");
        $pages_list = array(0 => '');

        foreach ($pages as $page) {
            $pages_list[$page->ID] = $page->post_title;
        }
        
        $page = $zippy->settings_pages->fetch('zippy_settings_core_pages');

            $section = $page->createSection('zippy_core_pages', __('General', ZippyCourses::TEXTDOMAIN));
            
                $section->createField('dashboard', __('Dashboard', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Dashboard</strong> page is the first page students will see after they log in, and will show a list of all of their courses.', ZippyCourses::TEXTDOMAIN));


                $section->createField('login', __('Login', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Login</strong> page is where students will log into their dashboard and where people will be redirected when trying to view restricted content.', ZippyCourses::TEXTDOMAIN));

                $section->createField('logout', __('Logout', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Logout</strong> page is where logged in users (yourself or your students) can visit to be automatically logged out of the site. Try putting it in a primary menu for good effect.', ZippyCourses::TEXTDOMAIN));

                $section->createField('register', __('Registration', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Register</strong> page is the page students will use to create their accounts.', ZippyCourses::TEXTDOMAIN));

                $section->createField('account', __('Account', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Account</strong> page is where a student can view their account details and view their order history.', ZippyCourses::TEXTDOMAIN));

                $section->createField('edit_account', __('Edit Account', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Edit Account</strong> page is where students can edit their Account Details.', ZippyCourses::TEXTDOMAIN));

                $section->createField('thank_you', __('Thank You', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Thank You</strong> page is the page that students will see after paying for their courses.', ZippyCourses::TEXTDOMAIN));

                $section->createField('buy', __('Buy Now', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Buy Now</strong> page is the page that students will visit in order to buy a course.', ZippyCourses::TEXTDOMAIN));

                $section->createField('course_directory', __('Course Directory', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Course Directory</strong> page is listing of all of your publicly available courses.', ZippyCourses::TEXTDOMAIN));

                $section->createField('order_history', __('Order History', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Order History</strong> page is where students will be able to view their purchases and view ongoing subscriptions.', ZippyCourses::TEXTDOMAIN));

                $section->createField('forgot_password', __('Forgot Password', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Forgot Password</strong> page where students go to retrieve their password.', ZippyCourses::TEXTDOMAIN));

                $section->createField('reset_password', __('Reset Password', ZippyCourses::TEXTDOMAIN), 'select', $pages_list)
                        ->setDescription(__('The <strong>Reset Password</strong> page is where users will be able to reset their passwords after they have requested a password reset.', ZippyCourses::TEXTDOMAIN));



    }
}
