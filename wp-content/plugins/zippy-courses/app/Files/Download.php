<?php

class ZippyCourses_SecureDownload
{
    public function __construct()
    {
        add_action('template_redirect', array($this, 'download'), 1);
    }

    public function download()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        $uid = get_query_var('zippy_file_uid', false);
        if (!$uid) {
            return;
        }

        $download = $this->_getDownload($uid);

        if ($download === null) {
            //
        }

        $url        = $download['url'];
        $url = str_replace(' ', '%20', $url);


        $entry_ids   = $download['entry_ids'];
        $has_access = false;
        $middleware = $zippy->middleware->get('student-access');

        foreach ($entry_ids as $entry_id) {
            $entry = new stdClass;
            $entry->id = $entry_id;
            $has_access = $middleware->run($entry);
            if ($has_access) {
                break;
            }
        }

        // Apply filter so that we can override the student access middleware if necessary
        $has_access = apply_filters(
            'zippy_downloads_have_student_access',
            $has_access,
            $student,
            $entry
        );
        if ($uid && $has_access) {
            $zippy->activity->insert($student->getId(), $uid, 'download');

            $fs  = $this->_getFileStream($url);
            $fs->download();
        } else {
            $zippy->sessions->flashMessage(
                __('You do not have permission to download this file.', ZippyCourses::TEXTDOMAIN),
                'error'
            );

            wp_redirect(home_url());
            exit;
        }
    }

    public function _getUrl($uid)
    {
        $zippy = Zippy::instance();

        $downloads = $zippy->utilities->entry->getAllDownloads();

        if (isset($downloads[$uid]) && isset($downloads[$uid]['url'])) {
            return $downloads[$uid]['url'];
        }

        return false;
    }

    public function _getDownload($uid)
    {
        $zippy = Zippy::instance();

        $downloads = $zippy->utilities->entry->getAllDownloads();

        return isset($downloads[$uid]) ? $downloads[$uid] : null;
    }

    public function _getFileStream($url)
    {
        $method = function_exists('curl_exec') ? 'curl' : 'socket';
        $method = apply_filters('zippy_filter_download_method', $method);

        switch ($method) {
            case 'curl':
                return new Zippy_Curl_FileStream($url);
                break;
            case 'socket':
                return new Zippy_Socket_FileStream($url);
            case 'direct':
                return new Zippy_DirectDownload_FileStream($url);
            default:
                break;
        }
    }
}
