<?php
/**
 * Id: student-access-exclude-completion
 * File Type: middleware
 * Class: ZippyCourses_StudentAccessExcludeCompletion_Middleware
 *
 * Detect whether a student has access based on a variety of factors, including course, tier, date and quiz.
 * Excludes the "Progress" check made by the ZippyCourses_StudentAccess_Middleware
 * @since 1.2.0
 */
class ZippyCourses_StudentAccessExcludeCompletion_Middleware extends ZippyCourses_StudentAccess_Middleware
{
    public function defaultRules()
    {
        $zippy = Zippy::instance();

        $rules = $zippy->make('middleware_rules');

        $rules->add(new Zippy_ItemRequiresAccessProtection_MiddlewareRule);
        $rules->add(new Zippy_UserIsAdmin_MiddlewareRule);
        $rules->add(new Zippy_ItemIsNotPubliclyVisible_MiddlewareRule);
        $rules->add(new Zippy_ItemIsCorePage_MiddlewareRule);
        $rules->add(new Zippy_UserIsLoggedIn_MiddlewareRule);
        $rules->add(new Zippy_StudentHasCourseAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasTierAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasAdditionalContentAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasDateAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasExpirationAccess_MiddlewareRule);
        // $rules->add(new Zippy_StudentHasProgressAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasQuizAccess_MiddlewareRule);

        $this->rules = apply_filters('zippy_middleware_rules', $rules, $this->getId());

        return $this;
    }
}
