<?php
/**
 * Id: quiz-access
 * File Type: middleware
 * Class: ZippyCourses_QuizAccess_Middleware
 *
 * Detect whether someone can visit a quiz directly
 *
 * @since 1.0.0
 */
class ZippyCourses_QuizAccess_Middleware extends Zippy_Middleware
{
    public function defaultRules()
    {
        $zippy = Zippy::instance();

        $rules = $zippy->make('middleware_rules');

        $rules->add(new Zippy_UserMustBeAdmin_MiddlewareRule);

        $this->rules = apply_filters('zippy_middleware_rules', $rules, $this->getId());

        return $this;
    }

    public function defaultErrorMessages()
    {
        $zippy = Zippy::instance();

        $this->error_messages = array(
            'before' => '<strong>' . __('You cannot access this content for the following reasons:', ZippyCourses::TEXTDOMAIN) . '</strong>',
            'after' => '<a href="' . $zippy->core_pages->getUrl('dashboard') . '">&laquo; ' . __('Return to Home page', ZippyCourses::TEXTDOMAIN) . '</a>'
        );

        return $this;
    }
}
