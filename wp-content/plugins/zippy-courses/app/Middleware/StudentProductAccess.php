<?php
/**
 * Id: student-existing-product-access
 * File Type: middleware
 * Class: ZippyCourses_StudentExistingProductAccess_Middleware
 *
 * Detect whether a student meets the requirements to purchase a product
 * which requires existing access to a different course
 *
 * @since 1.3.0
 */
class ZippyCourses_StudentExistingProductAccess_Middleware extends Zippy_Middleware
{
    public function defaultRules()
    {
        $zippy = Zippy::instance();

        $rules = $zippy->make('middleware_rules');
        
        $rules->add(new Zippy_ProductRequiresExistingAccess_MiddlewareRule);
        $rules->add(new Zippy_UserIsLoggedIn_MiddlewareRule);
        $rules->add(new Zippy_StudentOwnsRequiredCourses_MiddlewareRule);

        $this->rules = apply_filters('zippy_middleware_rules', $rules, $this->getId());

        return $this;
    }

    public function defaultErrorMessages()
    {
        $zippy = Zippy::instance();

        $this->error_messages = array(
            'before' => '<strong>' . __('You cannot purchase this product for the following reasons:', ZippyCourses::TEXTDOMAIN) . '</strong>',
            'after' => '<a href="' . $zippy->core_pages->getUrl('dashboard') . '">&laquo; ' . __('Return to Home page', ZippyCourses::TEXTDOMAIN) . '</a>'
        );

        return $this;
    }
}
