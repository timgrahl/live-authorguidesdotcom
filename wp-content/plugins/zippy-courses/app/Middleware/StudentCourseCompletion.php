<?php
/**
 * Id: student-course-completion
 * File Type: middleware
 * Class: ZippyCourses_StudentCourseCompletion_Middleware
 *
 * Detect whether a student has met the necessary requirements to view a course's completion certificate
 * Note: To make sure that all entries are accounted for,
 *  this middleware should only be run on the LAST entry of a course
 * @since 1.3.0
 */
class ZippyCourses_StudentCourseCompletion_Middleware extends Zippy_Middleware
{
    public function defaultRules()
    {
        $zippy = Zippy::instance();

        $rules = $zippy->make('middleware_rules');

        $rules->add(new Zippy_UserIsAdmin_MiddlewareRule);
        $rules->add(new Zippy_UserIsLoggedIn_MiddlewareRule);
        $rules->add(new Zippy_StudentHasCourseAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasAccessToAllEntries_MiddlewareRule);
        $rules->add(new Zippy_StudentHasViewedAllEntries_MiddlewareRule);

        $this->rules = apply_filters('zippy_middleware_rules', $rules, $this->getId());

        return $this;
    }
    public function defaultErrorMessages()
    {
        $zippy = Zippy::instance();

        $this->error_messages = array(
            'before' => '<strong>' . __('You have not completed this course for the following reasons:', ZippyCourses::TEXTDOMAIN) . '</strong>',
            'after' => '<a href="' . $zippy->core_pages->getUrl('dashboard') . '">&laquo; ' . __('Return to Home page', ZippyCourses::TEXTDOMAIN) . '</a>'
        );

        return $this;
    }
}
