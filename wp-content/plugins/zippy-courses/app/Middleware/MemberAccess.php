<?php
/**
 * Id: member-access
 * File Type: middleware
 * Class: ZippyCourses_MemberAccess_Middleware
 *
 * Detect whether a student has access based on a variety of factors, including course, tier, date and quiz.
 *
 * @since 1.0.0
 */
class ZippyCourses_MemberAccess_Middleware extends Zippy_Middleware
{
    public function setupCache()
    {
        parent::setupCache();

        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');
        if (!$student) {
            return;
        }

        $cache = $student->getMiddlewareCache($this->getId());

        if (is_array($cache)) {
            foreach ($cache as $id => $item) {
                $this->cache->set($id, $item['value']);
            }
        }
    }

    public function persistCache()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');
        if (!$student) {
            return;
        }

        $student->setMiddlewareCacheData($this->getId(), $this->cache->data);
    }

    public function defaultRules()
    {
        $zippy = Zippy::instance();

        $rules = $zippy->make('middleware_rules');

        $rules->add(new Zippy_ItemRequiresAccessProtection_MiddlewareRule);
        $rules->add(new Zippy_UserIsAdmin_MiddlewareRule);
        $rules->add(new Zippy_ItemIsNotPubliclyVisible_MiddlewareRule);
        $rules->add(new Zippy_UserIsLoggedIn_MiddlewareRule);

        $this->rules = apply_filters('zippy_middleware_rules', $rules, $this->getId());

        return $this;
    }

    public function defaultErrorMessages()
    {
        $zippy = Zippy::instance();

        $this->error_messages = array(
            'before' => '<strong>' . __('You cannot access this content for the following reasons:', ZippyCourses::TEXTDOMAIN) . '</strong>',
            'after' => '<a href="' . $zippy->core_pages->getUrl('dashboard') . '">&laquo; ' . __('Return to Home page', ZippyCourses::TEXTDOMAIN) . '</a>'
        );

        return $this;
    }
}
