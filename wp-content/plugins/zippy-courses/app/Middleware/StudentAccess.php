<?php
/**
 * Id: student-access
 * File Type: middleware
 * Class: ZippyCourses_StudentAccess_Middleware
 *
 * Detect whether a student has access based on a variety of factors, including course, tier, date and quiz.
 *
 * @since 1.0.0
 */
class ZippyCourses_StudentAccess_Middleware extends Zippy_Middleware
{
    public function defaultRules()
    {
        $zippy = Zippy::instance();

        $rules = $zippy->make('middleware_rules');

        $rules->add(new Zippy_ItemRequiresAccessProtection_MiddlewareRule);
        $rules->add(new Zippy_UserIsAdmin_MiddlewareRule);
        $rules->add(new Zippy_ItemIsNotPubliclyVisible_MiddlewareRule);
        $rules->add(new Zippy_ItemIsCorePage_MiddlewareRule);
        $rules->add(new Zippy_UserIsLoggedIn_MiddlewareRule);
        $rules->add(new Zippy_StudentHasCourseAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasTierAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasAdditionalContentAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasDateAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasExpirationAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasProgressAccess_MiddlewareRule);
        $rules->add(new Zippy_StudentHasQuizAccess_MiddlewareRule);

        $this->rules = apply_filters('zippy_middleware_rules', $rules, $this->getId());

        return $this;
    }

    public function defaultErrorMessages()
    {
        $zippy = Zippy::instance();

        $this->error_messages = array(
            'before' => '<strong>' . __('You cannot access this content for the following reasons:', ZippyCourses::TEXTDOMAIN) . '</strong>',
            'after' => '<a href="' . $zippy->core_pages->getUrl('dashboard') . '">&laquo; ' . __('Return to Home page', ZippyCourses::TEXTDOMAIN) . '</a>'
        );

        return $this;
    }
}
