<?php
/**
 * Id: user-order-access
 * File Type: middleware
 * Class: ZippyCourses_UserOrderAccess_Middleware
 *
 * Detect whether an order should grant access, based on it's status and various payment gateway rules.
 *
 * @since 1.0.0
 */
class ZippyCourses_UserOrderAccess_Middleware extends Zippy_Middleware
{
    public function defaultRules()
    {
        $zippy = Zippy::instance();

        $rules = $zippy->make('middleware_rules');

        $rules->add(new Zippy_UserIsOrderOwnerOrAdmin_MiddlewareRule);

        $this->rules = apply_filters('zippy_middleware_rules', $rules, $this->getId());

        return $this;
    }

    public function defaultErrorMessages()
    {
        $zippy = Zippy::instance();

        $this->error_messages = array(
            'before' => '<strong>' . __('You cannot view this order for the following reasons:', ZippyCourses::TEXTDOMAIN) . '</strong>',
            'after' => '<a href="' . $zippy->core_pages->getUrl('dashboard') . '">&laquo; ' . __('Return to Home page', ZippyCourses::TEXTDOMAIN) . '</a>'
        );

        return $this;
    }
}
