<?php

/**
 * This class is used to control all of the default access rules in Zippy Courses.
 *
 * We arbitrarily use post types to set up middleware restrictions.
 */
class ZippyCourses_Middleware
{
    public function __construct()
    {
        $zippy = Zippy::instance();

        $this->prepare();

        add_action('the_content', array($this, 'protectContent'));
        add_action('template_redirect', array($this, 'redirectVisitors'));
        add_action('template_redirect', array($this, 'quizzes'));
    }

    public function prepare()
    {
        $zippy = Zippy::instance();

        $files = $zippy->utilities->file->getClassFiles('middleware', 'Middleware', dirname(__FILE__));

        foreach ($files as $file) {
            $item = $zippy->makeFromFile($file, array('id'), array('id' => 'Id'));

            if ($item !== null) {
                $zippy->middleware->add($item);
            }
        }
    }

    public function protectContent($content)
    {
        global $post;
            
        $zippy      = Zippy::instance();
        $middleware = $zippy->middleware->get('student-access');

        if ($middleware === null) {
            return $content;
        }

        if (!$middleware->run($post)) {
            $content = $middleware->renderErrorMessages();
        }

        return $content;
    }

    public function redirectVisitors()
    {
        $zippy      = Zippy::instance();
        $middleware = $zippy->middleware->get('member-access');

        if ($middleware === null) {
            return;
        }

        if (!$middleware->run()) {
            $zippy->sessions->flashMessage(__('You must be a member to view that content. Please login below.', ZippyCourses::TEXTDOMAIN), 'error');

            $redirect_to = urlencode(get_permalink());
            wp_redirect($zippy->core_pages->getUrl('login') . "?redirect_to={$redirect_to}");
            exit;
        }

        return;
    }

    public function quizzes()
    {
        global $post;

        if (!is_object($post) || $post->post_type !== 'quiz') {
            return;
        }

        $zippy = Zippy::instance();

        $middleware = $zippy->middleware->get('quiz-access');

        if ($middleware === null) {
            return;
        }
        
        if (!$middleware->run()) {
            $redirect_to = urlencode($zippy->core_pages->getUrl('dashboard'));
            wp_redirect($zippy->core_pages->getUrl('login') . "?redirect_to={$redirect_to}");
            exit;
        }
    }
}
