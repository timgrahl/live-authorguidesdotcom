<?php

class ZippyCourses_CompleteLesson_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        global $post;

        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        $completed  = $student !== null && is_object($post) ? $student->isCompleted($post->ID) : false;

        $attributes = $this->parseAttributes($atts);
            
        $output = '<div class="zippy-complete-entry-region" data-id="' . $post->ID . '" data-status="' . (int) $completed . '"></div>';

        return $output;
    }

    public function parseAttributes($atts)
    {
        return shortcode_atts(
            array(
            ),
            $atts,
            $this->shortcode->getId()
        );
    }
}
