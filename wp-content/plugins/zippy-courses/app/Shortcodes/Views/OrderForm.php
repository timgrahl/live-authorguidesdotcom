<?php

class ZippyCourses_OrderForm_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        $zippy = Zippy::instance();

        $attributes = $this->parseAttributes($atts);

        $output = '';

        $requires_ssl = apply_filters('zippy_checkout_requires_ssl', __return_false());

        if ($requires_ssl && !is_ssl()) {
            $alert =  '';
            if (current_user_can('edit_others_posts')) {
                $alert .= '<div class="zippy-alert zippy-alert-warning">';
                $alert .= __('<strong>Warning:</strong> This product will be unavailable for purchase until an SSL certificate is detected.', ZippyCourses::TEXTDOMAIN);
                $alert .= '</div>';
            }

            $alert .= '<div class="zippy-alert zippy-alert-error">';
                $alert .= __('This product is not currently available for sale.', ZippyCourses::TEXTDOMAIN);
            $alert .= '</div>';

            return $alert;
        }

        $product_id = $attributes['product'];
        $product    = $zippy->utilities->product->getById($product_id);

        if ($product === null) {
            return;
        }

        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) && !empty($settings['method']) ? $settings['method'] : false;

        if (!$zippy->utilities->product->inLaunchWindow($product) || !$method) {
            $message = __('This product is not currently available for sale.', ZippyCourses::TEXTDOMAIN);

            return '<div class="zippy-alert zippy-alert-error">' . $message . '</div>';
        }

        // Create Form
        $form = $zippy->forms->get('order');
        if ($form->fields->get('product_id') !== null) {
            $form->fields->get('product_id')->setValue($attributes['product']);
        }

        $form->setSubmitText(apply_filters('zippy_buy_button_text',__('Buy Now', ZippyCourses::TEXTDOMAIN)));

        // Show Form
        $view = $zippy->make('form_view', array($form));
        
        $output .= $this->_renderPricingTable($product);
        $output .= $view->render();

        return $output;
    }

    public function parseAttributes($atts)
    {
        return shortcode_atts(
            array(
                'product' => '0'
            ),
            $atts,
            $this->shortcode->getId()
        );
    }

    private function _renderPricingTable(Zippy_Product $product)
    {
        $output = '';

        $output .= '<table class="zippy-table table table-striped">';
        
        $output .= '<thead>' .
            '<tr>' .
                '<th class="zippy-product-details">' . __('Product', ZippyCourses::TEXTDOMAIN) . '</th>' .
                '<th class="zippy-product-price">' . __('Price', ZippyCourses::TEXTDOMAIN) . '</th>' .
            '</tr>' .
        '</thead>';

        $output .= '<tbody>' .
            '<tr>' .
                '<td class="zippy-product-details">' .
                    '<p class="zippy-product-title">' . $product->getTitle() . '</p>' .
                '</td>' .
                '<td class="zippy-product-price">' .
                    apply_filters('zippy_filter_order_form_product_price_text', $product->getPriceString(), $product) .
                '</td>' .
            '</tr>' .
        '</tbody>';

        $output .= '</table>';

        return $output;
    }
}
