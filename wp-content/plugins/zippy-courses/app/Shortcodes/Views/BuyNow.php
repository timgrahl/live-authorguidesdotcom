<?php

class ZippyCourses_BuyNow_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        $zippy = Zippy::instance();

        extract(shortcode_atts(array(
            'product' => 0
        ), $atts));

        $classes = array('zippy-button', 'zippy-button-primary');

        $output = '';

        if ($product) {
            $product = $zippy->make('product', array($product));

            if ($product !== null) {
                $output = '<a href="' . $zippy->core_pages->getUrl('buy') . $product->getId() . '/?buy-now" class="' . implode(' ', $classes) . '">' . apply_filters('zippy_buy_button_text',__('Buy Now', ZippyCourses::TEXTDOMAIN)) . '</a>';
            }
        }

        return $output;
    }

    public function parseAttributes($atts)
    {
        return shortcode_atts(
            array(
            ),
            $atts,
            $this->shortcode->getId()
        );
    }
}
