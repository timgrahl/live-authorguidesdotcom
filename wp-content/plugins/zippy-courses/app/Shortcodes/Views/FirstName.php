<?php

class ZippyCourses_FirstName_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        return $student !== null ? $student->getFirstName() : '';
    }
}
