<?php

class ZippyCourses_ProgressBar_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        global $current_user;
        $zippy = Zippy::instance();
        $student = $zippy->make('student', array($current_user->ID));
        $student->fetch();
        
        $current_course = $this->getCourseID();
        extract(shortcode_atts(array(
            'course' => $current_course,
            'color' => '#337ab7'
        ), $atts));
        if (empty($course) || $student->id == 0) {
            return;
        }

        // Get the Course Progress of either the specified course, or the current course
        $course_id = !empty($atts['course']) ? $atts['course'] : $current_course;

        $course = $student->getCourse($course_id);


        // If the student doesn't have the course, return nothing
        if (!$course) {
            return;
        }

        $progress = $student->getCourseProgress($course);
        $course_title = get_the_title($course_id);
        
        $html ='<div class="zippy-progress">';
        $html.= '<div class="zippy-progress-bar" role="progressbar" aria-valuenow="' . $progress;
        $html .= '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $progress;
        $html .= '%; background-color:' . $color . ';">';
        if ($progress > 0) {
            $html.=         $progress . '% ' . __('Complete', ZippyCourses::TEXTDOMAIN);
        }
        $html.='        </div>';
        if ($progress == 0) {
                $html.= '<span class="zippy-progress-bar-empty">';
                $html.= '0% ' . __('Complete', ZippyCourses::TEXTDOMAIN);
                $html.= '</span>';
        }
        $html.='    </div>';


        return $html;
    }
    public function getCourseID()
    {
        global $post;
        $zippy = Zippy::instance();
        $post_type = get_post_type($post->ID);
        if ($post_type == 'course') {
            return $post->ID;
        }
        if (($post_type == 'unit') || ($post_type == 'lesson')) {
            $course_id = $zippy->utilities->entry->getCourseId($post->ID);
            return $course_id;
        }
        return null;
    }
    public function parseAttributes($atts)
    {
        return shortcode_atts(
            array(
            ),
            $atts,
            $this->shortcode->getId()
        );
    }
}
