<?php

class ZippyCourses_RegistrationForm_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        $zippy = Zippy::instance();

        $attributes = $this->parseAttributes($atts);

        // Create Form
        $form = $zippy->forms->get('registration');

        // Show Form
        $view = $zippy->make('form_view', array($form));

        return $view->render();
    }

    public function parseAttributes($atts)
    {
        return shortcode_atts(
            array(),
            $atts,
            $this->shortcode->getId()
        );
    }
}
