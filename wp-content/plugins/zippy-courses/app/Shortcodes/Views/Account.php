<?php

class ZippyCourses_Account_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        $attributes = $this->parseAttributes($atts);
        
        $output = '';

        $output .= '<h2>' . __('Your Profile', ZippyCourses::TEXTDOMAIN) . '</h2>';

        $output .= '<p>';
        $output .= $student->getFullName() . '<br/>';
        $output .= '<strong>' . __('Member since:', ZippyCourses::TEXTDOMAIN) . '</strong> ';
        $output .= $student->getJoinDateString() . '<br/>';
        $output .= '</p>';

        $output .= '<h2>' . __('Actions', ZippyCourses::TEXTDOMAIN) . '</h2>';
        

        $output .= '<p>';
        $output .= '<a class="zippy-edit-account-details-link" href="' . $zippy->core_pages->getUrl('edit_account') . '">' .
            __('Edit Account Details', ZippyCourses::TEXTDOMAIN) .
        '</a><br/>';
        $output .= '<a class="zippy-order-history-link" href="' . $zippy->core_pages->getUrl('order_history') . '">' .
            __('View Order History', ZippyCourses::TEXTDOMAIN) .
        '</a>';
        $output .= '</p>';

        return $output;
    }

    public function parseAttributes($atts)
    {
        return shortcode_atts(
            array(
            ),
            $atts,
            $this->shortcode->getId()
        );
    }
}
