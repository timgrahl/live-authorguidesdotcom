<?php

class ZippyCourses_LastName_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        return $student !== null ? $student->getLastName() : '';
    }
}
