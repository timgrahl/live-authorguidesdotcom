<?php

class ZippyCourses_EditAccount_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        return $this->renderForm();
    }

    private function renderForm()
    {
        $zippy          = Zippy::instance();

        $output = '';

        $edit_form = $zippy->forms->get('edit_account');
        $edit_view = $zippy->make('form_view', array($edit_form));
        
        $output .= '<h3>' . __('Update Account Details', ZippyCourses::TEXTDOMAIN) . '</h3>';
        $output .= $edit_view->render();

        $output .= '<h3>' . __('Change Your Password', ZippyCourses::TEXTDOMAIN) . '</h3>';
        $change_pw_form = $zippy->forms->get('change_password');
        $change_pw_view = $zippy->make('form_view', array($change_pw_form));
        $output .= '<p><em>' . __('You will be logged out, and will need to log back in, after changing your password.', ZippyCourses::TEXTDOMAIN) . '</em></p>';

        $output .= $change_pw_view->render();

        return $output;
    }
}
