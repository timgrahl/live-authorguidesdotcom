<?php

class ZippyCourses_ResetPassword_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        return $this->renderForm();
    }

    private function renderForm()
    {
        $zippy          = Zippy::instance();
        
        $form = $zippy->forms->get('reset_password');

        $view = $zippy->make('form_view', array($form));

        return $view->render();
    }
}
