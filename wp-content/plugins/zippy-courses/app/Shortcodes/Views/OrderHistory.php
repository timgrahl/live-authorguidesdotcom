<?php

class ZippyCourses_OrderHistory_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        return $this->renderForm();
    }

    private function renderForm()
    {
        $zippy          = Zippy::instance();

        $student = $zippy->cache->get('student');

        if ($student === null) {
            return;
        }

        $output = '';

        $orders = $student->getOrders();

        $output .= '<div class="zippy-table-container zippy-order-history-table-container">';
        $output .= '<table class="zippy-table zippy-order-history-table">';
            
        $output .=     '<thead>' .
                        '<tr>' .
                            '<th class="zippy-table-order-number">' . __('Order No.', ZippyCourses::TEXTDOMAIN) . '</th>' .
                            '<th class="zippy-table-order-date">' . __('Date', ZippyCourses::TEXTDOMAIN) . '</th>' .
                            '<th class="zippy-table-order-amount">' . __('Amount', ZippyCourses::TEXTDOMAIN) . '</th>' .
                            '<th class="zippy-table-order-type">' . __('Type', ZippyCourses::TEXTDOMAIN) . '</th>' .
                            '<th class="zippy-table-order-actions">' . __('Actions', ZippyCourses::TEXTDOMAIN) . '</th>' .
                        '</tr>' .
                    '</thead>' .
                    '<tbody>';

        foreach ($student->getOrders()->all() as $order) {
            $details = $order->getDetails();

            $symbol = isset($details['currency']) ? $zippy->currencies->getSymbol($details['currency']) : $zippy->currencies->getSymbol($default_currency);
            
            $output .= '<tr>';
                $output .= '<td class="zippy-table-order-number"><a href="' . get_permalink($order->getId()) . '">' . get_the_title($order->getId()) . '</a></td>';
                $output .= '<td class="zippy-table-order-date">' . $order->getDate()->format('M d, Y') . '</td>';
                $output .= '<td class="zippy-table-order-amount">' . $order->getTotalString() . '</td>';
                $output .= '<td class="zippy-table-order-type">' . $order->getTypeLabel() . '</td>';
                $output .= '<td class="zippy-table-order-actions"><a href="' . get_permalink($order->getId()) . '">' . __('View Order', ZippyCourses::TEXTDOMAIN) . '</a></td>';
            $output .= '</tr>';

        }

        $output .=     '</tbody>';

        $output .= '</table>';
        $output .= '</div>';

        $output .= '<a href="' . $zippy->core_pages->getUrl('account') . '" class="zippy-button">&laquo; ' . __('Your Account', ZippyCourses::TEXTDOMAIN) . '</a>';

        return $output;
    }
}
