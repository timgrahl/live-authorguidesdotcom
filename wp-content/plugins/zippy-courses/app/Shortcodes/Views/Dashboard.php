<?php

class ZippyCourses_Dashboard_ShortcodeView extends Zippy_ShortcodeView
{
    protected $course_count;
    public function render($atts, $content = "")
    {
        $zippy = Zippy::instance();

        $attributes = $this->parseAttributes($atts);
        $output  = $this->renderHeader();
        $output .= $this->renderCourses();
        $output .= $this->renderFooter();

        return $output;
    }

    private function renderHeader()
    {
        return '';
    }

    private function getFilteredCourses()
    {
        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');

        $courses = clone $student->getCourses();

        $middleware = $zippy->middleware->get('student-access');

        foreach ($courses->all() as $course) {
            $postdata = $course->getPostData();
            if (!$middleware->run($course) || $postdata->post_status == 'trash') {
                $courses->remove($course->getId());
            }
        }
        return $courses;
    }

    private function getCourses()
    {
        global $wpdb, $paged;

        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        $paged = $paged <= 1 ? 1 : $paged;
        $posts_per_page = get_option('posts_per_page', 10);
        $offset = $posts_per_page * ($paged - 1);

        $courses = $this->getFilteredCourses();
        $this->course_count = count($courses->all());
        // Then, only load those within our offset
        $courses = $courses->segment($posts_per_page, $offset, 'object');


        $settings = get_option('zippy_customizer_dashboard_options', array());
        $sort = isset($settings['course_order']) ? $settings['course_order'] : false;

        switch ($sort) {
            case 'publish_desc':
                $sort_by = 'published';
                $desc = true;
                break;
            case 'publish_asc':
                $sort_by = 'published';
                $desc = false;
                break;
            case 'alphanumeric_desc':
                $sort_by = 'title';
                $desc = true;
                break;
            case 'alphanumeric_asc':
            default:
                $sort_by = 'title';
                $desc = false;
                break;
        }

        $courses = $courses->sortBy($sort_by, $desc);
        


        return $courses;
    }

    private function renderCourses()
    {
        $courses = $this->getCourses();
        $output = '';

        foreach ($courses as $course) {
            $output .= $this->renderCourse($course);
        }

        $output = '<div class="zippy-courses zippy-courses-dashboard">' . $output . '</div>';

        return apply_filters('zippy_dashboard_html', $output, $courses);
    }

    private function renderCourse(Zippy_Course $course)
    {
        $zippy = Zippy::instance();

        if (!$course->entries->count() && !$course->additional_content->count()) {
            return '';
        }
        
        $settings = get_option('zippy_customizer_dashboard_options', array());

        $classes = array('zippy-course');
        $featured_image_html = $this->renderCourseFeaturedImage($course);


        if (!($zippy->utilities->post->hasFeaturedMedia($course->getId()))
            || !$zippy->utilities->dashboard->featuredMediaVisible()
            || empty($featured_image_html)
            ) {
            $classes[] = 'no-featured-image';
        }
        
        $output = '<div class="' . implode(' ', $classes) . '">';
        $output .= '<div class="zippy-course-header">' . $this->renderCourseHeader($course) . '</div>';


        if ($zippy->utilities->post->hasFeaturedMedia($course->getId()) 
            && $zippy->utilities->dashboard->featuredMediaVisible()
            && !empty($featured_image_html)
            ) {
            $output .= '<div class="zippy-featured-image">' . $featured_image_html . '</div>';
        }
            
        $output .= '<div class="zippy-course-description">';
        if ($zippy->utilities->dashboard->excerptsVisible()) {
            $output .= $this->renderCourseExcerpt($course);
        }
        $output .= '</div>';
        $output .= '<div class="zippy-course-footer">' . $this->renderCourseFooter($course) . '</div>';
        $output .= '</div>';

        /**
         * Filter the output of a single course in the Course Directory
         *
         * @since 1.0.0
         *
         * @param string                $output The HTML that will be output
         * @param Zippy_Course   $course The Course object
         */
        return apply_filters('zippy_dashboard_course_html', $output, $course);
    }

    private function renderCourseExcerpt(Zippy_Course $course)
    {
        $output     = '';
        $excerpt    = $course->getExcerpt();

        if (!empty($excerpt)) {
            $output .= wpautop($excerpt);
        }

        /**
         * Filter the output of the excerpt for a single course in the Course Directory
         *
         * @since 1.0.0
         *
         * @param string                $output The HTML that will be output
         * @param Zippy_Course   $course The Course object
         */
        return apply_filters('zippy_dashboard_course_excerpt_html', $output, $course);
    }

    private function renderCourseHeader(Zippy_Course $course)
    {
        $output = '<h3 class="zippy-course-title">';
            $output .= '<a href="' . get_permalink($course->id) . '">' . get_the_title($course->id) . '</a>';
        $output .= '</h3>';

        /**
         * Filter the output of the header for a single course in the Course Directory
         *
         * @since 1.0.0
         *
         * @param string                $output The HTML that will be output
         * @param Zippy_Course   $course The Course object
         */
        return apply_filters('zippy_dashboard_course_header_html', $output, $course);
    }

    private function renderCourseFooter(Zippy_Course $course)
    {
        $output = '<a href="' . get_permalink($course->getId()) . '" class="zippy-button zippy-course-continue ">' . __('Continue', ZippyCourses::TEXTDOMAIN) . ' &raquo;</a>';

        /**
         * Filter the output of the footer for a single course in the Course Directory
         *
         * @since 1.0.0
         *
         * @param string                $output The HTML that will be output
         * @param Zippy_Course   $course The Course object
         */
        return apply_filters('zippy_dashboard_course_footer_html', $output, $course);
    }

    private function renderCourseFeaturedImage(Zippy_Course $course)
    {
        $output         = '';
        $featured_image = $course->getPublicFeaturedImage('large');

        if ($featured_image) {
            $output .= '<img src="' . $featured_image[0] . '" width="' . $featured_image[1] . '" height="' . $featured_image[2] . '" />';
        }

        /**
         * Filter the output of the featured image for a single course in the Course Directory
         *
         * @since 1.0.0
         *
         * @param string                $output The HTML that will be output
         * @param Zippy_Course   $course The Course object
         */
        return apply_filters('zippy_dashboard_course_featured_image_html', $output, $course);
    }

    private function renderFooter()
    {
        $output = '<div class="zippy-course-directory-footer">';
            $output .= $this->renderPagination();
        $output .= '</div>';

        return $output;
    }

    private function renderPagination()
    {
        global $wpdb, $paged;

        $zippy = Zippy::instance();

        $paged = $paged <= 1 ? 1 : $paged;
        $num_pages = $this->getNumPages();

        if ($num_pages < 2) {
            return;
        }

        $output = '';

        $output .= '<div class="zippy-pagination zippy-dashboard-pagination">';
        $output .= $this->getPreviousPage($paged, $num_pages);
        for ($i=1; $i <= $num_pages; $i++) {
            $output .= $this->getPagedPermalink($i);
        }
        $output .= $this->getNextPage($paged, $num_pages);
        $output .= '</div>';

        return apply_filters('zippy_dashboard_pagination', $output, $paged, $num_pages);
    }

    private function getNextPage($page, $num_pages)
    {
        if ($page >= $num_pages) {
            return '<span>&raquo;</span>';
        }

        $next = $page + 1;

        return $this->getPagedPermalink($next, '&raquo;');
    }

    private function getPreviousPage($page, $num_pages)
    {
        if ($num_pages == 1 || $page == 1) {
            return '<span>&laquo;</span>';
        }

        $prev = $page - 1;

        return $this->getPagedPermalink($prev, '&laquo;');
    }

    private function getPagedPermalink($page, $text = '')
    {
        global $post;

        if ($page < 2) {
            $url = get_permalink();
        }

        $url = get_permalink() . 'page/' . $page . '/';

        $text = empty($text) ? $page : $text;

        return '<a href="' . $url . '">' . $text . '</a>';
    }

    private function getNumPages()
    {
        global $wpdb;

        $posts_per_page = get_option('posts_per_page', 10);
        $courses = $this->getFilteredCourses();
        $num_results    = isset($this->course_count) ? $this->course_count : count($this->getFilteredCourses());
        return ceil($num_results / $posts_per_page);
    }
}
