<?php

class ZippyCourses_Button_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        $zippy = Zippy::instance();

        extract(shortcode_atts(array(
            'type' => false,
            'text' => '',
            'style' => '',
            'href' => ''
        ), $atts));

        $classes = array( 'zippy-button' );

        switch ($style) {
            case 'primary':
                $classes[] = 'zippy-button-primary';
                break;
            default:
                break;
        }

        if ($type == 'register-login') {
            $text = !isset($atts['text']) ? __('Login', ZippyCourses::TEXTDOMAIN) : $text;
            $href = $zippy->utilities->cart->getLoginClaimUrl();
        }

        if ($type == 'register') {
            $text = !isset( $atts['text'] ) ? __('Register', ZippyCourses::TEXTDOMAIN) : $text;
            $href = $zippy->utilities->cart->getRegistrationClaimUrl();
        }

        $output = '<a href="' . $href . '" class="' . implode(' ', $classes) . '">' . $text . '</a>';

        return $output;
    }

    public function parseAttributes($atts)
    {
        return shortcode_atts(
            array(
            ),
            $atts,
            $this->shortcode->getId()
        );
    }
}
