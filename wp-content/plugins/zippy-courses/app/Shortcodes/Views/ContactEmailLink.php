<?php

class ZippyCourses_ContactEmailLink_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        $zippy = Zippy::instance();

        $email = $zippy->utilities->email->getSystemEmailAddress();

        return '<a href="mailto:' . $email . '">' . $email . '</a>';
    }

    public function parseAttributes($atts)
    {
        return shortcode_atts(
            array(
            ),
            $atts,
            $this->shortcode->getId()
        );
    }
}
