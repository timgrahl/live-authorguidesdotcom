<?php

class ZippyCourses_CourseDirectory_ShortcodeView extends Zippy_ShortcodeView
{
    public function render($atts, $content = "")
    {
        $zippy = Zippy::instance();

        $attributes = $this->parseAttributes($atts);
        $output  = $this->renderHeader();
        $output .= $this->renderCourses();
        $output .= $this->renderFooter();

        return $output;
    }

    public function parseAttributes($atts)
    {
        return shortcode_atts(
            array(
            ),
            $atts,
            $this->shortcode->getId()
        );
    }

    private function renderHeader()
    {
    }

    private function getDirectoryCourses()
    {
        global $wpdb, $paged;

        $zippy = Zippy::instance();

        $paged = $paged <= 1 ? 1 : $paged;
        $posts_per_page = get_option('posts_per_page', 10);
        $offset = $posts_per_page * ($paged - 1);

        $courses = $zippy->make('courses');

        $sql = $wpdb->prepare(
            "SELECT
                p.ID,
                pm.meta_value AS directory_order
             FROM
                $wpdb->posts AS p
             LEFT JOIN 
                $wpdb->postmeta AS pm
             ON
                (p.ID = pm.post_id)
             LEFT JOIN
                $wpdb->postmeta AS pm2
             ON
                (p.ID = pm2.post_id)
             WHERE 
                p.post_type = %s AND 
                p.post_status = %s AND
                pm.meta_key = %s AND
                pm2.meta_key = %s AND
                pm2.meta_value = %s
             ORDER BY 
                directory_order
             ASC 
             LIMIT
                %d 
             OFFSET
                %d",
            'course',
            'publish',
            'directory_order',
            'public',
            true,
            $posts_per_page,
            $offset
        );

        $course_ids = $wpdb->get_col($sql);

        foreach ($course_ids as $key => $course_id) {
            $course = $zippy->make('course', array('id' => $course_id, 'fetch' => true));
            $courses->add($course);
        }

        return $courses;
    }

    private function renderCourses()
    {
        $courses = $this->getDirectoryCourses();
        $output = '';

        foreach ($courses->all() as $course) {
            $output .= $this->renderCourse($course);
        }

        $output = '<div class="zippy-courses-directory">' . $output . '</div>';

        return apply_filters('zippy_course_directory_html', $output, $courses);
    }

    private function renderCourse(Zippy_Course $course)
    {
        $zippy = Zippy::instance();

        $settings = get_option('zippy_customizer_directory_options', array());

        $classes = array('zippy-course');
        $featured_image_html = $this->renderCourseFeaturedImage($course);

        if (!(
                $zippy->utilities->post->hasPublicFeaturedMedia($course->getId())
                || $zippy->utilities->post->hasFeaturedMedia($course->getId())
            ) ||
            !$zippy->utilities->directory->featuredMediaVisible()
            || empty($featured_image_html) 
        ) {
            $classes[] = 'no-featured-image';
        }

        $output = '<div class="' . implode(' ', $classes) . '">';
        if ((
                $zippy->utilities->post->hasPublicFeaturedMedia($course->getId())
                || $zippy->utilities->post->hasFeaturedMedia($course->getId())
            ) &&
            $zippy->utilities->directory->featuredMediaVisible()
            && !empty($featured_image_html)
        ) {
            $output .= '<div class="zippy-featured-image">' . $this->renderCourseFeaturedImage($course) . '</div>';
        }
        $output .= '<div class="zippy-course-details">';
        $output .= '<div class="zippy-course-header">' . $this->renderCourseHeader($course) . '</div>';
        
        $output .= '<div class="zippy-course-description">';
        if ($zippy->utilities->directory->excerptsVisible()) {
            $output .= $this->renderCourseExcerpt($course);
        }
        $output .= '</div>';
        
        $output .= '<div class="zippy-pricing-options">' . $this->renderCoursePricingOptions($course) . '</div>';
        $output .= '</div>';
        $output .= '</div>';

        /**
         * Filter the output of a single course in the Course Directory
         *
         * @since 1.0.0
         *
         * @param string                $output The HTML that will be output
         * @param Zippy_Course   $course The Course object
         */
        return apply_filters('zippy_course_directory_course_html', $output, $course);
    }

    private function renderCoursePricingOptions(Zippy_Course $course)
    {
        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');

        $output = '';

        $course_ids = $student !== null ? $student->getCourseIds() : array();

        if (in_array($course->getId(), $course_ids)) {
            return '<div class="zippy-course-already-owned">' . __('You own this course!', ZippyCourses::TEXTDOMAIN) . '</div>';
        }

        if (!$course->products->count()) {
            $course->fetchDeep();
        }

        $products = array();
        foreach ($course->products->all() as $course_product) {
            if (!$zippy->utilities->product->inLaunchWindow($course_product)) {
                continue;
            }

            if (get_post_status($course_product->getId()) !== 'publish') {
                continue;
            }

            $product = new stdClass;
            $product->id = $course_product->getId();
            $product->title = $course_product->getTitle();
            $product->price_string = $course_product->getPriceString();

            $products[] = $product;
        }

        if (count($products)) {
            $output .= '<script type="text/data" class="zippy-course-pricing-data">' . json_encode($products) . '</script>';
        }

        return $output;
    }

    private function renderCourseExcerpt(Zippy_Course $course)
    {
        $output     = '';
        $excerpt    = $course->getPublicExcerpt();

        if (!empty($excerpt)) {
            $output .= do_shortcode(wpautop($excerpt));
        }

        /**
         * Filter the output of the excerpt for a single course in the Course Directory
         *
         * @since 1.0.0
         *
         * @param string                $output The HTML that will be output
         * @param Zippy_Course   $course The Course object
         */
        return apply_filters('zippy_course_directory_course_excerpt_html', $output, $course);
    }

    private function renderCourseHeader(Zippy_Course $course)
    {
        $output = '<h3 class="zippy-course-title">';
            $output .= '<a href="' . get_permalink($course->id) . '">' . get_the_title($course->id) . '</a>';
        $output .= '</h3>';

        /**
         * Filter the output of the header for a single course in the Course Directory
         *
         * @since 1.0.0
         *
         * @param string                $output The HTML that will be output
         * @param Zippy_Course   $course The Course object
         */
        return apply_filters('zippy_course_directory_course_header', $output, $course);
    }

    private function renderCourseFeaturedImage(Zippy_Course $course)
    {
        $output         = '';
        $featured_image = $course->getPublicFeaturedImage('large');

        if ($featured_image) {
            $output .= '<img src="' . $featured_image[0] . '" width="' . $featured_image[1] . '" height="' . $featured_image[2] . '" />';
        }

        /**
         * Filter the output of the featured image for a single course in the Course Directory
         *
         * @since 1.0.0
         *
         * @param string                $output The HTML that will be output
         * @param Zippy_Course   $course The Course object
         */
        return apply_filters('zippy_course_directory_course_featured_image_html', $output, $course);
    }

    private function renderFooter()
    {
        $output = '<div class="zippy-course-directory-footer">';
            $output .= $this->renderPagination();
        $output .= '</div>';

        return $output;
    }

    private function renderPagination()
    {
        global $wpdb, $paged;

        $zippy = Zippy::instance();

        $paged = $paged <= 1 ? 1 : $paged;
        $num_pages = $this->getNumPages();

        if ($num_pages < 2) {
            return;
        }
        
        $output = '';

        $output .= '<div class="zippy-pagination zippy-course-directory-pagination">';
        $output .= $this->getPreviousPage($paged, $num_pages);
        for ($i=1; $i <= $num_pages; $i++) {
            $output .= $this->getPagedPermalink($i);
        }
        $output .= $this->getNextPage($paged, $num_pages);
        $output .= '</div>';

        return apply_filters('zippy_course_directory_pagination_html', $output, $paged, $num_pages);
    }

    private function getNextPage($page, $num_pages)
    {
        if ($page >= $num_pages) {
            return '<span>&raquo;</span>';
        }

        $next = $page + 1;

        return $this->getPagedPermalink($next, '&raquo;');
    }

    private function getPreviousPage($page, $num_pages)
    {
        if ($num_pages == 1 || $page == 1) {
            return '<span>&laquo;</span>';
        }

        $prev = $page - 1;

        return $this->getPagedPermalink($prev, '&laquo;');
    }

    private function getPagedPermalink($page, $text = '')
    {
        global $post;

        if ($page < 2) {
            $url = get_permalink();
        }

        $url = get_permalink() . 'page/' . $page . '/';

        $text = empty($text) ? $page : $text;

        return '<a href="' . $url . '">' . $text . '</a>';
    }

    private function getNumPages()
    {
        global $wpdb;

        $posts_per_page = get_option('posts_per_page', 10);
        $sql = $wpdb->prepare(
            "SELECT 
                COUNT(*)
             FROM
                $wpdb->posts AS p
             LEFT JOIN 
                $wpdb->postmeta AS pm
             ON
                (p.ID = pm.post_id)
             WHERE 
                p.post_type = %s AND 
                p.post_status = %s AND
                pm.meta_key = %s AND
                pm.meta_value = %d
            ",
            'course',
            'publish',
            'public',
            1
        );

        $num_results    = $wpdb->get_var($sql);

        return ceil($num_results / $posts_per_page);
    }
}
