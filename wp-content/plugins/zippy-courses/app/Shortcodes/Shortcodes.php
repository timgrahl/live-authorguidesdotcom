<?php

class ZippyCourses_Shortcodes extends Zippy_Repository
{
    public function __construct()
    {
        $this->addValidType('Zippy_Shortcode');
        $this->createDefaults();

        $this->hooks();
    }

    public function hooks()
    {
        add_action('init', array($this, 'register'));
    }
    
    private function createDefaults()
    {
        $defaults = array(
            'zippy_registration_form'   => 'registration_form_shortcode_view',
            'zippy_order_form'          => 'order_form_shortcode_view',
            'zippy_course_directory'    => 'course_directory_shortcode_view',
            'zippy_dashboard'           => 'dashboard_shortcode_view',
            'zippy_login_form'          => 'login_shortcode_view',
            'zippy_order_history'       => 'order_history_shortcode_view',
            'zippy_forgot_password'     => 'forgot_password_shortcode_view',
            'zippy_reset_password'      => 'reset_password_shortcode_view',
            'zippy_edit_account'        => 'edit_account_shortcode_view',
            'zippy_account'             => 'account_shortcode_view',
            'zippy_complete_lesson'     => 'complete_lesson_shortcode_view',
            'first_name'                => 'first_name_shortcode_view',
            'last_name'                 => 'last_name_shortcode_view',
            'username'                  => 'username_shortcode_view',
            'zippy_button'              => 'button_shortcode_view',
            'zippy_buy_now'             => 'buy_now_shortcode_view',
            'zippy_contact_email_link'  => 'contact_email_link_shortcode_view',
            'zippy_progress_bar'        => 'progress_bar_shortcode_view'
        );

        $this->registerDefaults($defaults);
    }

    private function registerDefaults($defaults)
    {
        $zippy = Zippy::instance();

        foreach ($defaults as $shortcode_id => $view) {
            $view = $zippy->make($view, array($shortcode_id));

            if (!($view instanceof Zippy_ShortcodeView)) {
            } else {
                $shortcode = $zippy->make('shortcode', array('id' => $shortcode_id));
                $shortcode->setView($view);

                $this->add($shortcode);
            }
        }
    }

    public function register()
    {
        foreach ($this->all() as $shortcode) {
            $shortcode->register();
        }
    }
}
