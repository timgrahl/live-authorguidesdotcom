<?php

class ZippyCourses_Students extends Zippy_Users
{
    /**
     * Prepares all course data for loaded courses at once
     * Loads the students' Course access data all at once to minimize queries to the db
     */
    public function prepareStudentTableData()
    {
        $this->setListTableUserdata();
        $this->setListTableCoursesOwned();
        $this->setListTableLastActiveDate();
    }
    protected function setListTableCoursesOwned()
    {
        global $wpdb;
        $zippy = Zippy::instance();
        $product_ids = $zippy->utilities->product->getAllProductIds();
        $users_products = $wpdb->get_results("SELECT user_id, meta_value from $wpdb->usermeta WHERE meta_key = 'products'");

        $product_courses = $zippy->utilities->product->getAllProductCourseIds();

        foreach ($users_products as $user_products) {
            $all_products[$user_products->user_id]['products'] = unserialize($user_products->meta_value);
        }
        
        foreach ($this->all() as $student) {
            // Get a specific student's products
            $products = isset($all_products[$student->getId()]) ? $all_products[$student->getId()]['products'] : null;
            if (empty($products)) {
                continue;
            }

            $product_course_ids = array();
            // For each of these products, insert Course IDs
            foreach ($products as $product_id) {
                if (isset($product_courses[$product_id])) {
                    $product_course_ids = array_merge($product_courses[$product_id], $product_course_ids);
                }
            }

            // Filter out any duplicates
            $product_course_ids = array_filter(array_unique($product_course_ids));

            // We do this so that it matches up with self::getCourseData's merging functionality
            $course_ids = array();
            foreach ($product_course_ids as $course_id) {
                $course_ids[$course_id] = $course_id;
            }

            if (!empty($course_ids)) {
                $student->setCache('course_ids', $course_ids);
            }

            
        }
    }
    protected function setListTableUserdata()
    {
        global $wpdb;
        $zippy = Zippy::instance();

        $student_ids = implode($this->getAllStudentIds(), ',');
        $results = $wpdb->get_results("
            SELECT ID, user_login, user_registered 
            FROM $wpdb->users
            WHERE ID IN ($student_ids)
            ");

        foreach ($results as $user_data) {
            $student = $this->get($user_data->ID);
            $student->username = $user_data->user_login;
            $student->joined = $zippy->utilities->datetime->getDate($user_data->user_registered);
        }
    }

    protected function setListTableLastActiveDate()
    {
        global $wpdb;
        $zippy = Zippy::instance();
        $student_ids = implode($this->getAllStudentIds(), ',');

        $results = $wpdb->get_results("
            SELECT user_id, meta_value
            FROM $wpdb->usermeta 
            WHERE meta_key = '_last_active'
            AND user_id IN ($student_ids)
            ");

        foreach ($results as $user_last_active) {
            $last_active = !empty($user_last_active->meta_value)
            ? $zippy->utilities->datetime->getDateFromTimestamp($user_last_active->meta_value)
            : null;
            
            if ($last_active instanceof DateTime) {
                $student = $this->get($user_last_active->user_id);
                $student->last_active = $last_active;
            }
        }
    }

    public function getAllStudentIDs()
    {
        $student_ids = array();
        foreach ($this->all() as $student) {
            $student_ids[] = $student->getId();
        }
        
        return $student_ids;
    }
}
