<?php

class ZippyCourses_Customer extends Zippy_User
{
    public function importData(array $data = array())
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }

        if ($this->full_name !== null && !$this->first_name && !$this->last_name) {
            $this->analyzeName($this->full_name);
        }
    }
}
