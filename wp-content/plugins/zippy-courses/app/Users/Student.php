<?php

class ZippyCourses_Student extends Zippy_User
{
    protected $products_owned;
    protected $quiz_results;
    protected $orders;

    protected $permissions;
    protected $completed;

    private $cache;

    /**
     * The time the student was last active
     *
     * @since 1.0.0
     *
     * @var DateTime
     */
    public $last_active;

    public function __construct($ID)
    {
        $zippy = Zippy::instance();

        parent::__construct($ID);

        $this->orders = $zippy->make('orders');
        $this->cache = $zippy->make('cache');
        
        $this->setupMiddlewareCache();
        $this->setupMiddlewareRulesCache();
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $user_id = wp_insert_user($this->getUserData());

        if (!is_wp_error($user_id)) {
            $this->ID = $user_id;
            $zippy->events->fire(new ZippyCourses_Registration_Event($this));
        }

        return $user_id;
    }

    public function getUserData()
    {
        return array(
            'user_login'    => $this->username,
            'user_email'    => $this->email,
            'user_pass'     => $this->password,
            'first_name'    => $this->first_name,
            'last_name'     => $this->last_name,
            'display_name'  => $this->getFullName(),
            'role'          => $this->role
        );
    }

    public function auth()
    {
        $zippy = Zippy::instance();

        if ($this->ID) {
            wp_set_auth_cookie($this->ID, true, is_ssl());
        }

        return $this;
    }

    public function saveMeta($key, $value)
    {
        if ($this->ID) {
            update_user_meta($this->ID, $key, $value);
        }

        return $this;
    }

    public function setCache($cache_name, $cache_value)
    {
        $this->cache->set($cache_name, $cache_value);
    }

    /**
     * Manuall build products owned
     * @return [type] [description]
     */
    public function getProductsOwned()
    {
        if ($this->products_owned === null || !is_array($this->products_owned)) {
            $this->products_owned = $this->calculateProductsOwned();
        }

        return $this->products_owned;
    }

    public function calculateProductsOwned()
    {
        $zippy = Zippy::instance();

        $orders = clone($this->getOrders());
        $products_owned = array();

        $middleware = $zippy->middleware->get('order-grants-access');

        if ($middleware !== null) {
            foreach ($orders->all() as $key => $order) {
                if ($middleware->run($order)) {
                    $product = $order->getProduct();
                    if ($product !== null) {
                        $products_owned[] = $product->getId();
                    }
                } else {
                }
            }
        } else {
        }

        return $products_owned;
    }

    public function getOrders()
    {
        global $wpdb;

        $zippy = Zippy::instance();
    
        if ($this->orders->count() < 1) {
            $this->orders->fetchByOwner($this->ID);
        }

        return $this->orders;
    }

    public function getCourse($course_id)
    {
        $courses = $this->getCourses();

        return $courses ? $courses->get($course_id) : null;
    }

    public function getCourses()
    {
        if (($courses = $this->cache->get('courses')) !== null) {
            return $courses;
        }

        $zippy          = Zippy::instance();
        
        $data    = $this->getCourseData();

        $courses = $zippy->make('courses');
        
        foreach ($data as $course_id => $course_data) {
            if (!current_user_can('edit_others_posts') && get_post_status($course_id) !== 'publish') {
                continue;
            }
            
            $course = $zippy->make('course', array('id' => $course_id));
            $start_date = $course_data['start_date'] instanceof DateTime ? $course_data['start_date'] : $zippy->utilities->datetime->getNow();
            
            if (!user_can($this->id, 'edit_others_posts')) {
                $course_products = $zippy->utilities->course->getAllCourseProducts($this, $course);
                if ($course_products !== null) {
                    foreach ($course_products as $course_product) {
                        $override = get_post_meta($course_product->getId(), 'scheduling_override', true);

                        if ($override) {
                            $course->config->scheduling->type = 'all';
                        }
                    }

                }

                $course->prepareForStudent($start_date, $course_data['tiers']);
            }

            $courses->add($course);
        }

        $this->cache->set('courses', $courses);

        return $courses;
    }

    public function getCourseStartDates()
    {
        if (($courses = $this->cache->get('course_start_dates')) !== null) {
            return $courses;
        }

        $zippy = Zippy::instance();

        $output = $this->getCourseIds();

        if (user_can($this->getId(), 'edit_others_posts')) {
            foreach ($output as &$start_date) {
                $start_date = $zippy->utilities->datetime->getToday();

                $this->cache->set('course_start_dates', $output);
            }

            return $output;
        }

        // Set a default value for each start date
        $empty_date = new DateTime('@0');
        foreach ($output as &$start_date) {
            $start_date = $empty_date;
        }

        $orders = $this->getOrders();

        foreach ($orders->all() as $order) {
            $product = $order->getProduct();
            $course_ids = $product ? $product->getCourses() : array();

            // Get the earliest start date available
            // Usually, this will be the only start date
            // But it is possible the student owns the course via multiple products
            foreach ($course_ids as $course_id) {
                if (!isset($output[$course_id])) {
                    continue;
                }
                
                $order_start_date =  $order->getStartDate($course_id);
                if ($output[$course_id] > $order_start_date
                    || $output[$course_id] == $empty_date
                    ) {
                    $output[$course_id] = $order->getStartDate($course_id);
                }
            }
        }
        
        $permissions = $this->getPermissions();
        foreach ($permissions['granted'] as $granted) {
            $course = $zippy->make('course', array($granted->course));
            $course->fetch();

            $output[(int) $granted->course] = $zippy->utilities->datetime->getDate($granted->time);
           
            $fixed_start_date = $course->config->scheduling->getStartDate();
            if (!empty($fixed_start_date)) {
                $output[(int) $granted->course] = $zippy->utilities->datetime->getDate($fixed_start_date);
            }
        }

        $start_dates = (array) $zippy->utilities->maybeJsonDecode(get_user_meta($this->getId(), 'zippy_courses_start_dates', true));
        $start_dates = array_filter($start_dates);

        foreach ($start_dates as $course_id => $date_string) {
            if ((bool) strtotime($date_string)) {
                $output[(int) $course_id] = $zippy->utilities->datetime->getDate($date_string);
            }
        }

        $this->cache->set('course_start_dates', $output);
        return $output;
    }

    public function getCourseExpirationDates()
    {
        if (($courses = $this->cache->get('course_expiration_dates')) !== null) {
            return $courses;
        }

        $zippy = Zippy::instance();

        $output = $this->getCourseIds();

        if (user_can($this->getId(), 'edit_others_posts')) {
            foreach ($output as &$start_date) {
                $start_date = $zippy->utilities->datetime->getToday();

                $this->cache->set('course_expiration_dates', $output);
            }

            return $output;
        }

        foreach ($output as &$start_date) {
            $start_date = $zippy->utilities->datetime->getToday();
        }

        $orders = $this->getOrders();

        foreach ($orders->all() as $order) {
            $product = $order->getProduct();
            $course_ids = $product ? $product->getCourses() : array();

            foreach ($course_ids as $course_id) {
                $output[$course_id] = $order->getStartDate($course_id);
            }
        }
        
        $permissions = $this->getPermissions();
        foreach ($permissions['granted'] as $granted) {
            $course = $zippy->make('course', array($granted->course));
            $course->fetch();

            $output[(int) $granted->course] = $zippy->utilities->datetime->getDate($granted->time);
           
            $fixed_start_date = $course->config->scheduling->getStartDate();
            if (!empty($fixed_start_date)) {
                $output[(int) $granted->course] = $zippy->utilities->datetime->getDate($fixed_start_date);
            }
        }

        $start_dates = (array) $zippy->utilities->maybeJsonDecode(get_user_meta($this->getId(), 'zippy_courses_start_dates', true));
        $start_dates = array_filter($start_dates);

        foreach ($start_dates as $course_id => $date_string) {
            $output[(int) $course_id] = $zippy->utilities->datetime->getDate($date_string);
        }

        $this->cache->set('course_expiration_dates', $output);

        return $output;
    }

    public function getCourseIds()
    {
        if (($course_ids = $this->cache->get('course_ids')) !== null) {
            return $course_ids;
        }

        $zippy = Zippy::instance();

        if (user_can($this->ID, 'edit_others_posts')) {
            global $wpdb;
            $raw_course_ids = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE post_type = 'course'");

            $course_ids = array();

            foreach ($raw_course_ids as $course_id) {
                $course_ids[$course_id] = $course_id;
            }

            $this->cache->set('course_ids', $course_ids);

            return $course_ids;
        }

        $products   = $this->getProductsOwned();

        $product_course_ids = array();

        foreach ($products as $product_id) {
            $product_course_ids = array_merge($zippy->utilities->product->getCourses($product_id), $product_course_ids);
        }

        $product_course_ids = array_filter(array_unique($product_course_ids));

        // We do this so that it matches up with self::getCourseData's merging functionality
        $course_ids = array();
        foreach ($product_course_ids as $course_id) {
            $course_ids[$course_id] = $course_id;
        }

        $permissions = $this->getPermissions();

        foreach ($permissions['granted'] as $granted) {
            if (!in_array($granted->course, $course_ids)) {
                $course_ids[$granted->course] = $granted->course;
            }
        }

        $this->cache->set('course_ids', $course_ids);

        return $course_ids;
    }

    public function getCourseData($id = null)
    {
        $zippy = Zippy::instance();

        if (($course_data = $this->cache->get('course_data')) !== null) {
            if ($id !== null) {
                return isset($course_data[$id]) ? $course_data[$id] : null;
            }

            return $course_data;
        }

        $raw = array(
            'id' => $this->getCourseIds(),
            'tiers' => $this->getCourseTiers(),
            'start_date' => $this->getCourseStartDates()
        );

        $keys = array_keys($raw);

        $course_data = array();
        foreach ($raw['id'] as $course_id) {
            $course_data[$course_id] = array();

            foreach ($keys as $key) {
                $course_data[$course_id][$key] = $zippy->utilities->array->pluckByKey($raw[$key], $course_id);
            }
        }

        if ($id !== null) {
            return isset($course_data[$id]) ? $course_data[$id] : null;
        }

        return $course_data;
    }

    public function getCourseTiers()
    {
        if (($course_tiers = $this->cache->get('course_tiers')) !== null) {
            return $course_tiers;
        }

        $zippy = Zippy::instance();

        $products   = $this->getProductsOwned();
        
        $course_tiers = $this->getCourseIds();

        foreach ($course_tiers as &$tier) {
            $tier = array();
        }

        foreach ($products as $product_id) {
            $courses_and_tiers = $zippy->utilities->product->getCoursesAndTiers($product_id);

            if (!empty($courses_and_tiers)) {
                foreach ($courses_and_tiers as $c) {
                    $course = $zippy->make('course', array($c['id']));

                    if (!isset($course_tiers[$c['id']])) {
                        $course_tiers[$c['id']] = array();
                    }
                        
                    foreach ($c['tiers'] as $tier_id) {
                        if (!array_key_exists($tier_id, $course_tiers[$c['id']])) {
                            $course_tiers[$c['id']][$tier_id] = array(
                                'id'     => $tier_id,
                                'title'  => $zippy->utilities->course->getTierTitle($course, $tier_id),
                                'status' => true
                            );
                        }
                    }
                }
            }
        }

        $permissions = $this->getPermissions();

        foreach ($permissions['granted'] as $granted) {
            if (!isset($course_tiers[$granted->course][$granted->tier])) {
                $course_tiers[$granted->course][$granted->tier]['status'] = true;
            }
        }

        foreach ($permissions['revoked'] as $revoked) {
            if (isset($course_tiers[$revoked->course][$revoked->tier])) {
                $course_tiers[$revoked->course][$revoked->tier]['status'] = false;
            }
        }

        $this->cache->set('course_tiers', $course_tiers);

        return $course_tiers;
    }

    /**
     *  Checks if a student has access to a specific Course and Tier
     *  @param  $course_id int Course ID Number
     *  @param  $tier_id int Tier ID Number
     *  @return  bool
     */
    public function hasTierAccess($course_id, $tier_id)
    {
        $tiers = $this->getCourseTiers();
        if (isset($tiers[$course_id])
            && isset($tiers[$course_id][$tier_id])
            && $tiers[$course_id][$tier_id]['status']) {
            return true;
        }
        return false;
    }

    /**
     * Sets the The time the student was last active.
     *
     * @param DateTime $last_active the last active
     *
     * @return self
     */
    public function setLastActive(DateTime $last_active)
    {
        $this->last_active = $last_active;

        return $this;
    }

    public function fetch()
    {
        parent::fetch();
        
        if ($this->ID) {
            $this->getLastActive();
            $this->getProductsOwned();
            $this->getOrders();
            $this->getPermissions();
        }
    }
    
    /**
     * Gets the The time the student was last active.
     *
     * @return DateTime
     */

    public function getLastActive($format = '')
    {
        $zippy = Zippy::instance();
        
        if ($this->last_active instanceof DateTime) {
            return !empty($format) ? date_i18n($format, $this->last_active->format('U')) : $this->last_active->format('U');
        } else {
            $last_active = get_user_meta($this->ID, '_last_active', true);
            $last_active = !empty($last_active) ? $zippy->utilities->datetime->getDateFromTimestamp($last_active) : null;

            if ($last_active instanceof DateTime) {
                $this->last_active = $last_active;
            }
        }

        return $this->last_active;
    }

    public function getPermissions()
    {
        $zippy = Zippy::instance();

        if ($this->permissions === null) {
            $defaults = array('granted' => array(), 'revoked' => array());
            $permissions = (array) $zippy->utilities->maybeJsonDecode(get_user_meta($this->ID, 'zippy_permissions', true));
            $this->permissions = (array) array_merge($defaults, array_filter($permissions));
            //Make sure that everything is an array, and that the array's values are set properly
            $this->permissions['granted'] = (array) $this->permissions['granted'];
            $this->permissions['revoked'] = (array) $this->permissions['revoked'];
            $this->permissions['granted'] = !empty($this->permissions['granted']) ? array_values($this->permissions['granted']) : array();
            $this->permissions['revoked'] = !empty($this->permissions['revoked']) ? array_values($this->permissions['revoked']) : array();
        }

        return $this->permissions;
    }

    public function getCompleted()
    {
        $zippy = Zippy::instance();

        if ($this->completed === null || !is_array($this->completed)) {
            $completed = (array) get_user_meta($this->ID, 'completed_entries', true);
            $this->completed = array_filter($completed);
        }

        return $this->completed;
    }

    public function getQuizResults($quiz_id = null)
    {
        if ($this->quiz_results === null) {
            $this->fetchQuizResults();
        }

        if ($quiz_id !== null) {
            $results = array();

            foreach ($this->quiz_results as $result) {
                if (is_object($result) && $result->quiz == $quiz_id) {
                    $results[] = $result;
                }
            }

            return $results;
        }

        return $this->quiz_results;
    }

    public function fetchQuizResults()
    {
        $quiz_results = get_user_meta($this->getId(), 'quiz_results');
        if (is_array($quiz_results)) {
            foreach ($quiz_results as $key => $quiz) {
                if (!is_array($quiz)) {
                    $quiz_results[$key] = json_decode($quiz);
                }
            }
        } else {
            $quiz_results = array();
        }

        $this->quiz_results = $quiz_results;
    }

    public function getPasswordResetKeys()
    {
        global $wpdb;

        if (($keys = $this->cache->get('password_reset_keys')) !== null) {
            return $keys;
        }

        $zippy = Zippy::instance();

        $keys = get_user_meta($this->getId(), 'password_reset_keys', true);
        $keys   = is_array($keys) && !empty($keys) ? $keys : array();

        $now = $zippy->utilities->datetime->getNow();

        foreach ($keys as $key => $data) {
            $expires = $zippy->utilities->datetime->getDateFromTimestamp($data['expires']);

            if ($expires->format('U') < $now->format('U')) {
                $zippy->utilities->user->deleteExpiredPasswordResetKey($data['umeta_id']);
                unset($keys[$key]);
            } else {
                $keys[$key]['key'] = $wpdb->get_var("SELECT meta_value FROM $wpdb->usermeta WHERE umeta_id = {$data['umeta_id']}");
            }
        }

        update_user_meta($this->getId(), 'password_reset_keys', $keys);

        $this->cache->set('password_reset_keys', $keys);

        return $keys;
    }

    public function addPasswordResetKey($key)
    {
        if (($keys = $this->cache->get('password_reset_keys')) === null) {
            $keys = $this->getPasswordResetKeys();
        }

        $zippy = Zippy::instance();

        $expires = $zippy->utilities->datetime->getNow();
        $expires->modify('+2 days');

        if (($insert_id = add_user_meta($this->getId(), 'password_reset_key', $key, false)) !== false) {
            $keys[] = array(
                'umeta_id' => $insert_id,
                'expires'  => $expires->format('U')
            );

            update_user_meta($this->getId(), 'password_reset_keys', $keys);
        }

        $this->cache->set('password_reset_keys', $keys);

        return $this;
    }

    public function removeTierAccess($course_id, $tier_id)
    {
        $zippy = Zippy::instance();

        $course_tiers = $this->getCourseTiers();
        $permissions = $this->getPermissions();

        $permission = new stdClass;
            $permission->course = $course_id;
            $permission->tier   = $tier_id;
            $permission->time   = $zippy->utilities->datetime->getNow()->format('Y-m-d H:i:s');
        
        if (isset($course_tiers[$course_id]) && !in_array($tier_id, $course_tiers[$course_id])) {
            $permissions['revoked'][] = $permission;
        }
        foreach ($permissions['granted'] as $key => $entry) {
            if ($entry->course == $course_id && $entry->tier == $tier_id) {
                unset($permissions['granted'][$key]);
            }
        }

        $this->permissions = $permissions;

        return (bool) update_user_meta($this->getId(), 'zippy_permissions', json_encode($permissions));
    }

    public function restoreTierAccess($course_id, $tier_id)
    {
        $zippy = Zippy::instance();

        $course_tiers = $this->getCourseTiers();

        $permissions = $this->getPermissions();
        $permission = new stdClass;
            $permission->course = $course_id;
            $permission->tier   = $tier_id;
            $permission->time   = $zippy->utilities->datetime->getNow()->format('Y-m-d H:i:s');
        if (isset($course_tiers[$course_id]) && !in_array($tier_id, $course_tiers[$course_id])) {
            $permissions['granted'][] = $permission;
        }

        foreach ($permissions['revoked'] as $key => $entry) {
            if ($entry->course == $course_id && $entry->tier == $tier_id) {
                unset($permissions['revoked'][$key]);
            }
        }

        $this->permissions = $permissions;

        return (bool) update_user_meta($this->getId(), 'zippy_permissions', json_encode($permissions));
    }

    public function isCompleted($entry_id)
    {
        $zippy = Zippy::instance();

        $completed          = false;
        $completed_entries  = $this->getCompleted();
        $course_id          = $zippy->utilities->entry->getCourseId($entry_id);

        if ($course_id !== null) {
            $course_completions = isset($this->completed[$course_id]) ? $this->completed[$course_id] : array();
            $completed = in_array($entry_id, $course_completions);
        }

        return $completed;
    }

    public function toggleEntryComplete($entry_id)
    {
        $zippy = Zippy::instance();

        $course_id = $zippy->utilities->entry->getCourseId($entry_id);
        $this->getCompleted();

        if ($course_id !== null) {
            if (isset($this->completed[$course_id]) && is_array($this->completed[$course_id])) {
                $key = array_search($entry_id, $this->completed[$course_id]);
                if ($key !== false) {
                    unset($this->completed[$course_id][$key]);
                } else {
                    $this->completed[$course_id][] = $entry_id;
                }
            } else {
                $this->completed[$course_id] = array($entry_id);
            }
        }

        update_user_meta($this->getId(), 'completed_entries', $this->getCompleted());
    }

    public function getCourseProgress(Zippy_Course $course)
    {
        $progress = 0;

        $completed = $this->getCompleted();
        $completions = isset($completed[$course->getId()]) ? $completed[$course->getId()] : array();

        $settings       = get_option('zippy_customizer_course_options', array());

        // Determines wheither units or lessons or both should be used when caluclating percentages. 
        // 0 = Both, 1 = Only Units, 2 = Only Lessons
        $units_lessons_completion  = isset($settings['units_lessons_completion_percentage'])
            ? $settings['units_lessons_completion_percentage']: 0;

        $course_entry_ids = $course->getEntryIds();

        foreach ($course_entry_ids as $key => $entry_id) {
            switch ($units_lessons_completion) {
                case 2:
                    if (get_post_type($entry_id) != 'lesson') {
                        unset($course_entry_ids[$key]);
                    }
                    break;
                case 1:
                    if (get_post_type($entry_id) != 'unit') {
                        unset($course_entry_ids[$key]);
                    }
                    break;
                case 0:
                default:
                    // All Good, do nothing
                    break;
            }
        }
        foreach ($completions as $key => $value) {
            if (!in_array($value, $course_entry_ids)) {
                unset($completions[$key]);
            }
        }

        if (count($course_entry_ids)) {
            $progress = number_format(((count($completions) / count($course_entry_ids)) * 100), 2, '.', ',');
        }

        return $progress;
    }

    public function setupMiddlewareCache()
    {
        $zippy = Zippy::instance();

        $this->cache->set('middleware', $zippy->make('cache'));
    }

    public function getMiddlewareCache($middleware_id)
    {
        $zippy = Zippy::instance();

        $cache = $this->cache->get('middleware');

        if (!isset($cache[$middleware_id])) {
            $cache[$middleware_id] = array();
        }

        $this->cache->set('middleware', $cache);

        return $cache[$middleware_id];
    }

    public function setMiddlewareCacheData($middleware_id, $data)
    {
        $zippy = Zippy::instance();

        $cache = array_filter((array) $this->cache->get('middleware'));

        if (!isset($cache[$middleware_id])) {
            $cache[$middleware_id] = array();
        }
        foreach ($data as $key => $value) {
            $cache[$middleware_id][$key] = $value;
        }
        
        return $this->cache->set('middleware', $cache);
    }

    public function setupMiddlewareRulesCache()
    {
        $zippy = Zippy::instance();

        $cache = array_filter((array) get_user_meta($this->getId(), "zippy_middleware_rules_cache", true));
        $timestamp = get_user_meta($this->getId(), "zippy_middleware_rules_cache_timestamp", true);
    
        // Validate that the cache has been set since midnight, and if not, flush it
        $timestamp = $timestamp ? $zippy->utilities->datetime->getDateFromTimestamp($timestamp) : null;
        $today = $zippy->utilities->datetime->getToday();
        
        if ($timestamp instanceof DateTime && $timestamp->format('U') < $today->format('U')) {
            $cache = array();
        }

        $this->cache->set('middleware_rules', $cache);
    }

    public function getMiddlewareRulesCache($rule_id)
    {
        $zippy = Zippy::instance();

        $cache = $this->cache->get('middleware_rules');

        if (isset($cache[$rule_id])) {
            $now = $zippy->utilities->datetime->getNow();

            foreach ($cache[$rule_id] as $key => $item) {
                $expired = isset($item['expires'])
                    ? $item['expires'] < $now->format('U')
                    : true;

                if ($expired) {
                    unset($cache[$rule_id][$key]);
                }
            }
        } else {
            $cache[$rule_id] = array();
        }

        $this->cache->set('middleware_rules', $cache);

        return $cache[$rule_id];
    }

    public function setMiddlewareRulesCacheData($rule_id, $data)
    {
        $zippy = Zippy::instance();

        $cache = array_filter((array) $this->cache->get('middleware_rules'));

        if (!isset($cache[$rule_id])) {
            $cache[$rule_id] = array();
        }
        foreach ($data as $key => $value) {
            if (!isset($cache[$rule_id][$key])) {
                $expires = $zippy->utilities->datetime->getNow();
                $expires->modify('+2 hours');

                $cache[$rule_id][$key] = array(
                    'value'     => $value['value'],
                    'expires'   => $expires->format('U')
                );
            } else {
                $cache[$rule_id][$key]['value'] = $value['value'];
            }
        }

        return $this->cache->set('middleware_rules', $cache);
    }

    public function persistMiddlewareRulesCache()
    {
        update_user_meta($this->getId(), 'zippy_middleware_rules_cache', $this->cache->get('middleware_rules'));
        update_user_meta($this->getId(), 'zippy_middleware_rules_cache_timestamp', time());
    }

    public function flushItemFromMiddlewareRulesCache($objectId)
    {
        $cache = array_filter((array) $this->cache->get('middleware_rules'));

        foreach ($cache as $middleware) {
            foreach ($middleware as $key => $value) {
                if ($key == $objectId) {
                    unset($cache[$middleware][$key]);
                }
            }
        }

        return $this->cache->set('middleware_rules', $cache);
    }

    /**
     * Check to see if a course has been completed by current student
     * If no completion is found, check to see if the course is now complete
     * @param  Zippy_Course $course
     * @return boolean
     */
    public function hasCompletedCourse(Zippy_Course $course)
    {
        $zippy = Zippy::instance();
        $course_id = $course->getId();

        $student_completion = json_decode(get_user_meta($this->getId(), "courses_completed", true));
        if ($student_completion && in_array($course_id, $student_completion)) {
            return true;
        }

        $middleware = $zippy->middleware->get('student-course-completion');
        if ($middleware->run($course)) {
            $this->completeCourse($course);
            return true;
        }

        return false;
    }

    /**
     * Marks a course as complete for the current student
     * @param  Zippy_Course $course
     * @return void
     */
    public function completeCourse(Zippy_Course $course)
    {
        $zippy = Zippy::instance();
        $course_id = $course->getId();
        $student_completion = json_decode(get_user_meta($this->getId(), "courses_completed", true));
        $student_completion = !empty($student_completion) ? $student_completion : array();

        $completion_date = $zippy->utilities->datetime->getToday()->format('U');
        array_push($student_completion, $course->getId());

        update_user_meta($this->getId(), 'courses_completed', json_encode($student_completion));
        update_user_meta($this->getId(), "course_{$course_id}_completion_date", $completion_date);

        $zippy->events->fire(new ZippyCourses_CourseCompletion_Event($this, $course));
    }
}
