<?php

class ZippyCourses_Student_Importer
{
    protected $data;
    protected $imported = array();
    protected $errors = array();
    protected $existing_users = array();

    public function __construct($file)
    {
        $this->data = $this->read($file);
    }

    public function import()
    {
        global $wpdb;
        $zippy = Zippy::instance();

        foreach ($this->data as $student) {
            $new_user_details = array(
                'user_login'    =>  $student['Username'],
                'user_email'    =>  $student['Email'],
                'user_pass'     =>  wp_generate_password(),
                'first_name'    =>  $student['FirstName'],
                'last_name'     =>  $student['LastName'],
                'role'          => 'subscriber'
            );

            $new_user_id = wp_insert_user($new_user_details);

            if (is_wp_error($new_user_id)) {
                $error = $new_user_id->get_error_message();
                $code = $new_user_id->get_error_code();

                switch($code) {

                    case 'existing_user_email':
                        $this->errors[] = $new_user_id->get_error_message() . ' (' . $new_user_details['user_email'] . ')';
                        $existing_user = get_user_by('email', $new_user_details['user_email']);
                        $this->existing_users[] = $existing_user->ID;
                        break;
                    
                    case 'existing_user_login':
                        $this->errors[] = $new_user_id->get_error_message() . ' (' . $new_user_details['user_login'] . ')';
                        $existing_user = get_user_by('login', $new_user_details['user_login']);
                        $this->existing_users[] = $existing_user->ID;
                        break;


                    default:
                        break;
                }
                
            } else {
                $this->imported[] = $new_user_id;
                $student = $zippy->make('student', array($new_user_id));
                $student->fetch();
                $student->password = $new_user_details['user_pass'];
                $zippy->events->fire(new ZippyCourses_Registration_Event($student));
            }
        }
    }

    public function read($file)
    {
        $keys = array();
        $data = array();
        
        $i = 0;

        $fh = fopen($file, "r");

        while (($line = fgetcsv($fh)) !== false) {
            //$line is an array of the csv elements
            if ($i == 0) {
                foreach ($line as $key) {
                    $keys[] = $key;
                }
            } else {
                $item = array();
                foreach ($line as $key => $value) {
                    $item[$keys[$key]] = $value;
                }
                $data[] = $item;
            }

            $i++;
        }
        fclose($fh);

        return $data;

    }

    public function getImported()
    {
        return $this->imported;
    }

    public function getErrors()
    {
        return $this->errors;
    }
    public function getExistingUsers()
    {
        return $this->existing_users;
    }
    public function getAllValidStudents()
    {
        return array_merge($this->imported, $this->existing_users);
    }

    public function getData()
    {
        return $this->data;
    }

    public function validate()
    {
        return true;
    }
}
