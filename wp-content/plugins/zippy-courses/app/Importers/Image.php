<?php

class ZippyCourses_Image_Importer
{
    protected $data;
    protected $imported = array();

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function import()
    {
        global $wpdb;

        foreach ($this->data as $attachment_id) {
            $files      = (array) get_post_meta($attachment_id, 'import_files', true);
            $file       = isset($files[0]) && !empty($files[0]) ? $files[0] : '';
            $id         = $this->fetch($file, $attachment_id);

            if (!is_wp_error($id)) {
                $this->searchAndReplace($id);
                $this->imported[] = $id;
            }
        }
    }

    public function fetch($file, $post_id, $desc = null)
    {
        if (!empty($file)) {
            global $wpdb;

            $meta_keys = array('import_id', 'import_files', 'import_author', 'import_parent_id');
            $meta = array();

            foreach ($meta_keys as $meta_key) {
                $meta[$meta_key] = get_post_meta($post_id, $meta_key, true);
            }

            // Set variables for storage, fix file filename for query strings.
            preg_match('/[^\?]+\.(jpe?g|jpe|gif|png)\b/i', $file, $matches);
            
            $file_array = array();
            $file_array['name'] = basename($matches[0]);
     
            // Download file to temp location.
            $file_array['tmp_name'] = download_url($file);
     
            // If error storing temporarily, return the error.
            if (is_wp_error($file_array['tmp_name'])) {
                return $file_array['tmp_name'];
            }

            // Do the validation and storage stuff.
            $id = media_handle_sideload($file_array, $post_id, $desc);
            
            foreach ($meta as $key => $value) {
                update_post_meta($id, $key, $value);
            }

            // If error storing permanently, unlink.
            if (!is_wp_error($id)) {
                $wpdb->query("DELETE FROM $wpdb->posts WHERE ID = $post_id");
            } else {
                @unlink($file_array['tmp_name']);
            }
            
            return $id;
        }

        return new WP_Error('file_missing', __("No URL or file path was provided.", "zippy-courses"));
    }

    public function searchAndReplace($id)
    {
        global $wpdb;

        $upload_dir = wp_upload_dir();
        
        $meta       = wp_get_attachment_metadata($id, true);

        $base_url   = $upload_dir['url'] . DIRECTORY_SEPARATOR;

        $old_images = get_post_meta($id, 'import_files', true);
        $new_images = array( $base_url . $meta['file'] );

        foreach ($meta['sizes'] as $k => $v) {
            $new_images[] = $base_url . $v['file'];
        }

        $matches = array();

        foreach ($new_images as $nimage) {
            $new_basename = basename($nimage);
            foreach ($old_images as $k => $oimage) {
                $old_basename = basename($oimage);
                if ($new_basename == $old_basename) {
                    $matches[] = array(
                        'old' => $oimage,
                        'new' => $nimage
                    );
                    break;
                }
            }
        }

        foreach ($matches as $match) {
            // Replace URLs in post content
            $sql = $wpdb->prepare("UPDATE $wpdb->posts SET post_content = REPLACE(post_content, %s, %s)", $match['old'], $match['new']);
            $wpdb->query($sql);

            // Replace URLs in Public data and featured content
            $sql = $wpdb->prepare("UPDATE $wpdb->postmeta SET meta_value = REPLACE(meta_value, %s, %s) WHERE meta_key IN (%s, %s, %s, %s)", $match['old'], $match['new'], 'featured_media', 'public_content', 'public_excerpt', 'public_featured_media');
            $wpdb->query($sql);
        }
    }

    public function getImported()
    {
        return $this->imported;
    }
}
