<?php

class ZippyCourses_WishlistMemberStudents_Importer
{
    protected $data;
    protected $imported = array( 'users' => array() );
    protected $map;
    protected $count = array('user' => 0);
    protected $errors = array();
    protected $valid = 'wishlist-students';

    protected $courses;

    public function __construct($file = null)
    {
        if ($file !== null) {
            $this->data = json_decode(file_get_contents($file));            
        }
    }

    public function getData()
    {
        return $this->data;
    }

    public function importStudents()
    {
        $new_users = array();
    
        foreach ($this->data->users as $user) {
            $new_user_id = $this->importUser($user);

            if (is_wp_error($new_user_id)) {
                $error = $new_user_id->get_error_message();
                $code = $new_user_id->get_error_code();

                switch($code) {
                    case 'existing_user_login':
                        $this->errors[] = $new_user_id->get_error_message() . ' (' . $user->user_login . ')';
                        break;

                    case 'existing_user_email':
                        $this->errors[] = $new_user_id->get_error_message() . ' (' . $user->user_email . ')';
                        break;

                    default:
                        break;
                }
                
            } else {
                $new_users[] = $new_user_id;
                $this->incrementCount();
            }
        }

        $this->imported['users'] = $new_users;

        return count($new_users);
    }

    /**
     * Import a single User
     *
     * @todo    Do a check to see if the username exists, and if it does, process it in another way
     * @since   0.9.19
     * @param   stdClass        $user     The WP_User object with it's meta that was exported
     * @return  int|WP_Error    $user_id  The ID of the new User or a WP_Error
     */
    public function importUser(stdClass $user)
    {
        global $wpdb;

        // Grab our meta for later and clear it out of the object.
        $meta = (array) $user->meta;
        unset( $user->meta );

        // Unset the ID, we don't need it
        unset( $user->ID );

        // Generate a new password
        $user->user_pass  = wp_generate_password(16);
        $user->role = 'subscriber';

        $new_user_id = wp_insert_user($user);

        if (!is_wp_error($new_user_id)) {
            foreach ($meta as $key => $value) {
                if ($key == 'levels') {
                    $lvls = array();
                    foreach ($value as $lvl_key => $lvl_value) {
                        $lvls[] = $lvl_value->id;
                    }
                    $wpdb->insert($wpdb->usermeta, array( 'user_id' => $new_user_id, 'meta_key' => 'wishlist_import_levels', 'meta_value' => implode(',', $lvls) ));
                    continue;
                }

                if (is_array($value)) {
                    foreach ($value as $v) {
                        $wpdb->insert($wpdb->usermeta, array( 'user_id' => $new_user_id, 'meta_key' => $key, 'meta_value' => $v ));
                    }
                } else {
                    // We only exported with one value for this key, so we need to see if the new object has a value set
                    $umeta_id = $wpdb->get_var($wpdb->prepare("SELECT umeta_id FROM $wpdb->usermeta WHERE user_id = %s AND meta_key = %s", $new_user_id, $key));

                    // If it exists, update; if not, insert
                    if ($umeta_id !== null) {
                        $wpdb->update($wpdb->usermeta, array( 'meta_value' => $value ), array( 'umeta_id' => $umeta_id ));
                    } else {
                        $wpdb->insert($wpdb->usermeta, array( 'user_id' => $new_user_id, 'meta_key' => $key, 'meta_value' => maybe_serialize($value)));
                    }
                    
                }
            }
        }

        return $new_user_id;
    }

    public function grantAccessToStudents($users = array())
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $users  = empty($users) ? $this->imported['users'] : $users;
        
        foreach ($users as $user_id) {
            $levels = array_filter(explode(',', get_user_meta($user_id, 'wishlist_import_levels', true)));
            foreach ($levels as $level_id)
            {
                $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_level', $level_id);
                $product_ids = $wpdb->get_col($sql);

                foreach ($product_ids as $product_id) {
                    $zippy->access->grant($user_id, $product_id);
                }
            }
        }
    }

    public function migrateStudentAccess()
    {
        global $wpdb;

        $this->addTablesToWpdb();

        $users  = $wpdb->get_col("SELECT ID FROM $wpdb->users");
        $this->grantAccessToStudents($users);

        return true;
    }

    public function import()
    {
        if ($this->data === null) {
            return false;
        }

        $success = $this->importStudents();
        if ($success) {
            $this->grantAccessToStudents();
        }

        return $success;
    }

    public function migrate()
    {
        return $this->migrateStudentAccess();
    }

    public function incrementCount($type = 'user')
    {
        $this->count[$type]++;
    }

    public function getCount($type = 'user')
    {
        return $this->count[$type];
    }

    public function getErrors()
    {
        return $this->errors;
    }
    public function getImported()
    {
        return $this->imported;
    }

    public function validateFile()
    {
        $type = false;

        if (isset($this->data->export_meta)) {
            $type = isset($this->data->export_meta->type) ? $this->data->export_meta->type : false;
        }

        return $type == $this->valid;
    }

    public function addTablesToWpdb()
    {
        global $wpdb;

        $wlm_prefix = $wpdb->prefix . 'wlm_';

        $wpdb->wlm = new stdClass;
        $wpdb->wlm->api_queue = $wlm_prefix . 'api_queue';
        $wpdb->wlm->contentlevels = $wlm_prefix . 'contentlevels';
        $wpdb->wlm->contentlevel_options = $wlm_prefix . 'contentlevel_options';
        $wpdb->wlm->emailbroadcast = $wlm_prefix . 'emailbroadcast';
        $wpdb->wlm->email_queue = $wlm_prefix . 'email_queue';
        $wpdb->wlm->options = $wlm_prefix . 'options';
        $wpdb->wlm->userlevels = $wlm_prefix . 'userlevels';
        $wpdb->wlm->userlevel_options = $wlm_prefix . 'userlevel_options';
        $wpdb->wlm->user_options = $wlm_prefix . 'user_options';
    }
}
