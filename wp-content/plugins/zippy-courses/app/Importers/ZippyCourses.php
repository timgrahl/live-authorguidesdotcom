<?php
/**
 * This class is used to import Zippy Courses JSON data of posts and users.
 *
 * The encoded data should follow a particular format:
 *
 * stdClass $data {
 *     array $posts = [
 *         array 'course' = [
 *             stdClass $course1 {},
 *             stdClass $course2 {},
 *             ...
 *         ],
 *         array 'unit' = [...]
 *         ...
 *     ],
 *     array $users = [
 *         stdClass $user1
 *     ]
 * }
 *
 * Users should exported with $wpdb->get_row and post can use get_post or $wpdb->get_row.
 *
 * When used in this fashion, the importer will properly import posts and users.
 *
 * After import, it is possible to use the newly imported objects and remap certain values,
 * such as ParentIDs (useful for Units, Lessons, Pricing Options, and Questions in Zippy Courses),
 * post_author and CSV meta values.
 *
 * @package    ZippyCourses\Importer
 * @author     Jonathan Wondrusch <jonathan@zippycourses.com>
 * @copyright  2014-2015    DHJW Software LLC
 * @link       http://zippycourses.com
 * @since      0.9.19
 */

class ZippyCourses_ZippyCourses_Importer
{
    
    protected $data;
    protected $imported = array();
    protected $valid = 'zippy-courses';

    public function __construct($file)
    {
        $this->data = json_decode(file_get_contents($file));
    }

    public function import()
    {
        foreach ($this->data->posts as $type => $posts) {
            $this->importEntries($type, $posts);
        }

        $this->importAttachments();
        $this->importComments();
        $this->importUsers();

        $this->remap();
    }

    public function remap()
    {
        $imported       = $this->getImported();
        $users          = $imported['users'];
        $comments       = $imported['comments'];

        foreach ($imported as $type => $posts) {
            if ($type == 'users' || $type == 'comments') {
                continue;
            }

            if ($type == 'course') {
                $this->remapEntries($posts, 'entries');
            }

            if ($type == 'course') {
                $this->remapEntries($posts, 'additional_content');
                $this->remapCourseProducts($posts);
            }

            if ($type == 'quiz') {
                $this->remapJsonData($posts, 'post', 'questions', 'ID');
            }

            if ($type == 'transaction') {
                $this->remapAuthor($posts);
                $this->remapKey($posts, 'post', 'order_id');
                $this->remapSerializedData($posts, 'post', 'details', 'product');
            }

            if ($type == 'zippy_order') {
                $this->remapAuthor($posts);
                $this->remapKey($posts, 'post', 'product');
            }

            $this->remapSlugs($posts);
        }

        $this->remapComments($comments);
        
        $this->remapQuizResults();
        $this->remapUserProducts();

        $attachments = $this->getSegment('attachments');
    }

    public function remapAuthor(array $list)
    {
        global $wpdb;

        foreach ($list as $item_id) {
            $old_author_id = get_post_meta($item_id, 'import_author', true);
            $new_author_id = $wpdb->get_var($wpdb->prepare("SELECT user_id FROM $wpdb->usermeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $old_author_id));

            if ($new_author_id && $old_author_id) {
                $wpdb->update($wpdb->posts, array('post_author' => $new_author_id), array('ID' => $item_id));
            }
        }
    }

    public function remapQuizResults()
    {
        global $wpdb;

        $results = $wpdb->get_results("SELECT * FROM $wpdb->usermeta WHERE meta_key = 'quiz_results'");
        
        foreach ($results as $key => $record) {
            $result = json_decode($record->meta_value);

            if (isset($result->quiz)) {
                $new_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $result->quiz));

                if ($new_id) {
                    $result->quiz = $new_id;
                }
            }

            if (isset($result->questions) && is_array($result->questions)) {
                foreach ($result->questions as &$question) {
                    $new_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $question->id));

                    if ($new_id) {
                        $question->id = $new_id;
                    }
                }
            }

            $wpdb->update(
                $wpdb->usermeta,
                array(
                    'meta_value' => json_encode($result)
                ),
                array(
                    'umeta_id' => $record->umeta_id
                )
            );
        }
    }

    public function remapSerializedData(array $list, $type, $meta_key, $key)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $type . 'meta';
        $type_id = $type . '_id';
        $id_col = $type == 'user' ? 'umeta_id' : 'meta_id';

        if (isset($list['error'])) {
            unset($list['error']);
        }

        foreach ($list as $item_id) {
            $sql = $wpdb->prepare("SELECT * FROM $table WHERE $type_id = %s AND meta_key = %s", $item_id, $meta_key);
            $data = array_filter((array) $wpdb->get_results($sql));

            foreach ($data as $row) {
                $meta_value = maybe_unserialize($row->meta_value);

                if (array_key_exists($key, $meta_value)) {
                    if (is_array($meta_value[$key])) {
                        foreach ($meta_value[$key] as $k => $v) {
                            $new_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $v));
                            if ($new_id) {
                                $meta_value[$key][$k] = $new_id;
                            }
                        }
                    } else {
                        $new_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $meta_value[$key]));
                        if ($new_id) {
                            $meta_value[$key] = $new_id;
                        }
                    }
                } else {
                    foreach ($meta_value as $k => $v) {
                        if (array_key_exists($key, $v)) {
                            $new_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $v[$key]));
                            if ($new_id) {
                                $meta_value[$k][$key] = $new_id;
                            }
                        }
                    }
                }

                $row->meta_value = serialize($meta_value);

                $wpdb->update($table, array('meta_value' => $row->meta_value), array($id_col => $row->$id_col));
            }
        }
    }

    public function remapUserProducts()
    {
        global $wpdb;
        
        if (isset($list['error'])) {
            unset($list['error']);
        }

        $sql = $wpdb->prepare("SELECT * FROM $wpdb->usermeta WHERE meta_key = %s", 'products');
        $data = $wpdb->get_results($sql);

        foreach ($data as $row) {
            $row->meta_value = maybe_unserialize($row->meta_value);
            $meta_value = array_filter((array) $row->meta_value);

            foreach ($meta_value as $k => &$v) {
                $new_id = $wpdb->get_var(
                    $wpdb->prepare(
                        "SELECT 
                            post_id 
                         FROM 
                            $wpdb->postmeta
                         WHERE
                            meta_key = %s AND
                            meta_value = %s
                        ",
                        'import_id',
                        $v
                    )
                );

                if ($new_id) {
                    $meta_value[$k] = $new_id;
                } else {
                    unset($meta_value[$k]);
                }
            }

            $wpdb->update(
                $wpdb->usermeta,
                array(
                    'meta_value' => serialize($meta_value)
                ),
                array(
                    'umeta_id' => $row->umeta_id
                )
            );
        }
    }

    public function remapCourseProducts(array $list)
    {
        global $wpdb;
        
        if (isset($list['error'])) {
            unset($list['error']);
        }

        foreach ($list as $item_id) {
            $sql = $wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE post_id = %s AND meta_key = %s", $item_id, 'product');
            $data = array_filter((array) $wpdb->get_results($sql));

            foreach ($data as $row) {
                $row->meta_value = $zippy->utilities->maybeJsonDecode($row->meta_value);
                
                $meta_value = array_filter((array) $row->meta_value);

                foreach ($meta_value as $k => &$v) {
                    $new_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $v));
                    if ($new_id) {
                        $meta_value[$k] = $new_id;
                    } else {
                        unset($meta_value[$k]);
                    }
                }

                $row->meta_value = json_encode($meta_value);

                $wpdb->update($table, array('meta_value' => $row->meta_value), array('meta_id' => $row->meta_id));
            }
        }
    }

    public function remapKey(array $list, $type, $meta_key)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $type . 'meta';
        $type_id = $type . '_id';
        $id_col = $type == 'user' ? 'umeta_id' : 'meta_id';

        if (isset($list['error'])) {
            unset($list['error']);
        }

        foreach ($list as $item_id) {
            $sql = $wpdb->prepare("SELECT * FROM $table WHERE $type_id = %s AND meta_key = %s", $item_id, $meta_key);
            
            $data = array_filter((array) $wpdb->get_results($sql));

            foreach ($data as $row) {
                $new_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $row->meta_value));
                if ($new_id) {
                    $row->meta_value = $new_id;
                }

                $wpdb->update($table, array('meta_value' => $row->meta_value), array($id_col => $row->{$id_col}));
            }
        }
    }

    public function remapJsonData(array $list, $type, $meta_key, $key)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $type . 'meta';
        $type_id = $type . '_id';
        $id_col = $type == 'user' ? 'umeta_id' : 'meta_id';

        if (isset($list['error'])) {
            unset($list['error']);
        }

        $zippy = Zippy::instance();
        
        foreach ($list as $item_id) {
            $sql = $wpdb->prepare("SELECT * FROM $table WHERE $type_id = %s AND meta_key = %s", $item_id, $meta_key);
            $data = array_filter((array) $wpdb->get_results($sql));

            foreach ($data as $row) {
                $row->meta_value = $zippy->utilities->maybeJsonDecode($row->meta_value);

                if (is_array($row->meta_value)) {
                    foreach ($row->meta_value as $k => $v) {
                        $new_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $v->ID));
                        if ($new_id) {
                            $row->meta_value[$k]->{$key} = $new_id;
                        }
                    }
                } else {
                    if (isset($row->meta_value->{$key})) {
                        $new_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $row->meta_value->{$key}));
                        if ($new_id) {
                            $row->meta_value->{$key} = $new_id;
                        }
                    }
                }

                $row->meta_value = json_encode($row->meta_value);

                $wpdb->update($table, array('meta_value' => $row->meta_value), array($id_col => $row->{$id_col}));
            }
        }
    }

    public function remapEntries(array $list, $key)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        foreach ($list as $item_id) {
            $entries = array_filter((array) $zippy->utilities->getJsonMeta($item_id, $key, true));
            
            $new_entries = array();

            foreach ($entries as &$entry) {
                $new_entry_id = $wpdb->get_var(
                    $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s ORDER BY post_id DESC LIMIT 1",
                        'import_id',
                        $entry->ID
                    )
                );
                

                $new_subentries = array();
                foreach ($entry->entries as &$subentry) {
                    $new_subentry_id = $wpdb->get_var(
                        $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s ORDER BY post_id DESC LIMIT 1",
                            'import_id',
                            $subentry->ID
                        )
                    );
                    
                    if ($new_subentry_id) {
                        $new_subentry = clone($subentry);
                        $new_subentry->ID = $new_subentry_id;
                        $new_subentries[] = $new_subentry;
                    }
                }

                $entry->entries = $new_subentries;

                if ($new_entry_id) {
                    $new_entry = clone($entry);
                    $new_entry->ID = $new_entry_id;
                    $new_entries[] = $new_entry;
                }
            }

            update_post_meta($item_id, $key, json_encode($new_entries));
        }
    }

    public function importEntries($type, $data)
    {
        global $wpdb, $current_user;

        $new_entries = array();

        foreach ($data as $item) {
            $item->post_author = $current_user->ID;
            $meta = (array) $item->meta;
            
            unset($item->meta);

            $args = (array) $item;
            unset($args['filter']);

            $wpdb->insert(
                $wpdb->posts,
                $args
            );

            $new_item_id = $wpdb->insert_id;
            
            if ($new_item_id) {
                $new_entries[] = $new_item_id;
                foreach ($meta as $key => $value) {
                    $value = is_array($value) || is_object($value) ? serialize($value) : $value;
                    $wpdb->insert($wpdb->postmeta, array('post_id' => $new_item_id, 'meta_key' => $key, 'meta_value' => $value));
                }
            }
        }

        // Store it here for future use...
        $this->imported[$type] = $new_entries;

        return $new_entries;
    }

    public function importComments()
    {

        global $wpdb, $current_user;

        $new_entries = array();

        $data = isset($this->data->comments) ? $this->data->comments : false;
        
        if (!$data) {
            $this->data->comments = array();
            $this->imported['comments'] = array();
            return $this->imported['comments'];
        }

        foreach ($data as $item) {
            $meta = (array) $item->meta;
            
            unset($item->meta);
            $args = (array) $item;

            $wpdb->insert(
                $wpdb->comments,
                $args
            );

            $new_item_id = $wpdb->insert_id;
            
            if ($new_item_id) {
                $new_entries[] = $new_item_id;
                foreach ($meta as $key => $value) {
                    $value = is_array($value) || is_object($value) ? serialize($value) : $value;                    
                    $wpdb->insert($wpdb->commentmeta, array('comment_id' => $new_item_id, 'meta_key' => $key, 'meta_value' => $value), array('%d', '%s', '%s'));
                }
            }
            
        }

        // Store it here for future use...
        $this->imported['comments'] = $new_entries;

        return $new_entries;

    }

    public function importAttachments()
    {
        $attachments = $this->data->attachments;

        foreach ($attachments as $key => $attachment) {
            unset($attachment->ID);
            $attachments[$key] = $attachment;
        }

        $this->importEntries('attachments', $attachments);
    }

    public function remapComments(array $list)
    {
        global $wpdb;

        // Remap author
        foreach ($list as $item_id) {
            $old_author_id = get_comment_meta($item_id, 'import_author', true);
            
            if (!$old_author_id) {
                continue;
            }

            $new_author_id = $wpdb->get_var($wpdb->prepare("SELECT user_id FROM $wpdb->usermeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $old_author_id));

            if ($new_author_id && $old_author_id) {
                $wpdb->update($wpdb->comments, array('user_id' => $new_author_id), array('comment_ID' => $item_id));
            }

        }

        // Remap posts
        foreach ($list as $item_id) {
            $old_post_id = $wpdb->get_var($wpdb->prepare("SELECT comment_post_ID FROM $wpdb->comments WHERE comment_ID = %s", $item_id));
            $new_post_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $old_post_id));

            if ($new_post_id) {
                $wpdb->update($wpdb->comments, array('comment_post_ID' => $new_post_id), array('comment_ID' => $item_id));
            }
            
        }

        // Remap comment parents
        foreach ($list as $item_id) {
            $old_parent_id = $wpdb->get_var($wpdb->prepare("SELECT comment_parent FROM $wpdb->comments WHERE comment_ID = %s", $item_id));
            $new_parent_id = $wpdb->get_var($wpdb->prepare("SELECT comment_id FROM $wpdb->commentmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $old_parent_id));

            if ($new_parent_id) {
                $wpdb->update($wpdb->comments, array('comment_parent' => $new_parent_id), array('comment_ID' => $item_id));
            }
            
        }
    }

    /**
     * Import the users in our data
     * @since   0.9.19
     * @param   array   $users is an array of WP_User objects
     * @return  array   $user_ids  A list of new User IDs
     */
    public function importUsers()
    {
        $new_users = array( 'error' => array() );
        
        $data = isset($this->data->users) ? $this->data->users : false;
        
        if (!$data) {
            $this->data->users = array();
            $this->imported['users'] = array();
            return $this->imported['users'];
        }

        foreach ($this->data->users as $user) {
            $new_user_id = $this->importUser($user);

            if (is_wp_error($new_user_id)) {
                $error = $new_user_id->get_error_message();
                $code = $new_user_id->get_error_code();

                switch($code) {
                    case 'existing_user_login':
                        $new_users['error'][] = $new_user_id->get_error_message() . ' (' . $user->user_login . ')';
                        break;

                    case 'existing_user_email':
                        $new_users['error'][] = $new_user_id->get_error_message() . ' (' . $user->user_email . ')';
                        break;

                    default:
                        break;
                }

            } else {
                $new_users[] = $new_user_id;
            }
        }

        // Store it here for future use...
        $this->imported['users'] = $new_users;

        return $new_users;
    }

    /**
     * Import a single User
     *
     * @todo    Do a check to see if the username exists, and if it does, process it in another way
     * @since   0.9.19
     * @param   stdClass        $user     The WP_User object with it's meta that was exported
     * @return  int|WP_Error    $user_id  The ID of the new User or a WP_Error
     */
    public function importUser(stdClass $user)
    {
        global $wpdb;

        // Grab our meta for later and clear it out of the object.
        $meta = (array) $user->meta;
        unset( $user->meta );

        // Unset the ID, we don't need it
        unset( $user->ID );

        // Generate a new password
        $user->user_pass  = wp_generate_password(16);
        $user->role = 'subscriber';
        
        $new_user_id = wp_insert_user($user);

        if (!is_wp_error($new_user_id)) {
            foreach ($meta as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $v) {
                        $wpdb->insert($wpdb->usermeta, array( 'user_id' => $new_user_id, 'meta_key' => $key, 'meta_value' => $v ));
                    }
                } else {
                    // We only exported with one value for this key, so we need to see if the new object has a value set
                    $umeta_id = $wpdb->get_var($wpdb->prepare("SELECT umeta_id FROM $wpdb->usermeta WHERE user_id = %s AND meta_key = %s", $new_user_id, $key));

                    // If it exists, update; if not, insert
                    if ($umeta_id !== null) {
                        $wpdb->update($wpdb->usermeta, array( 'meta_value' => $value ), array( 'umeta_id' => $umeta_id ));
                    } else {
                        $wpdb->insert($wpdb->usermeta, array( 'user_id' => $new_user_id, 'meta_key' => $key, 'meta_value' => $value ));
                    }
                    
                }
            }
        }

        return $new_user_id;
    }

    public function remapSlugs(array $list)
    {
        global $wpdb;
        foreach ($list as $item_id) {
            $title = get_the_title($item_id);

            $slug = wp_unique_post_slug(sanitize_title($title), $item_id, get_post_status($item_id), get_post_type($item_id), wp_get_post_parent_id($item_id));

            $wpdb->update($wpdb->posts, array('post_name' => $slug), array('ID' => $item_id));
        }
    }

    public function getImported()
    {
        return $this->imported;
    }

    public function validate()
    {
        
        return true;

        $valid = true;

        if (!is_object($this->data)) {
            $valid = false;
        }

        if (!isset($this->data->users) && !isset($this->data->posts)) {
            $valid = false;
        }

        if (isset($this->data->posts)) {
            if (!is_object($this->data->posts)) {
                $valid = false;
            }

            foreach ($this->data->posts as $p) {
                if (!is_array($p)) {
                    $valid = false;
                }

            }
        }

        if (!isset($this->data->export_meta) || !isset($this->data->export_meta->type) || $this->data->export_meta->type != 'zippy-courses') {
            $valid = false;
        }

        return $valid;

    }

    public function getCount($post_type)
    {
        $count = isset($this->imported[$post_type]) ? count($this->imported[$post_type]) : 0;

        if (isset($this->imported[$post_type]) && isset($this->imported[$post_type]['error'])) {
            $count--;
        }

        return $count;
    }

    public function getSegment($key)
    {
        return $this->data->{$key};
    }

    public function clearImportIds()
    {
        foreach ($this->imported as $type => $entries) {
            if ($type == 'users') {
                foreach ($entries as $user) {
                    delete_user_meta($user, 'import_id');
                }
            } elseif ($type == 'comments') {
                foreach ($entries as $comment) {
                    delete_comment_meta($comment, 'import_id');
                }
            } else {
                foreach ($entries as $post) {
                    delete_post_meta($post, 'import_id');
                }
            }
        }
    }
}
