<?php
/**
 * This class is used to import Zippy Courses JSON data of posts and users.
 *
 * The encoded data should follow a particular format:
 *
 * stdClass $data {
 *     array $posts = [
 *         array 'course' = [
 *             stdClass $course1 {},
 *             stdClass $course2 {},
 *             ...
 *         ],
 *         array 'unit' = [...]
 *         ...
 *     ],
 *     array $users = [
 *         stdClass $user1
 *     ]
 * }
 *
 * Users should exported with $wpdb->get_row and post can use get_post or $wpdb->get_row.
 *
 * When used in this fashion, the importer will properly import posts and users.
 *
 * After import, it is possible to use the newly imported objects and remap certain values,
 * such as ParentIDs (useful for Units, Lessons, Pricing Options, and Questions in Zippy Courses),
 * post_author and CSV meta values.
 *
 * @package    ZippyCourses\Importer
 * @author     Jonathan Wondrusch <jonathan@zippycourses.com>
 * @copyright  2014-2015    DHJW Software LLC
 * @link       http://zippycourses.com
 * @since      0.9.19
 */

class ZippyCourses_WishlistMember_Importer
{
    protected $data;
    protected $tiers;
    protected $courses;
    protected $products;
    protected $structure_courses;
    protected $structure_levels;
    protected $count = array('course' => 0, 'unit' => 0, 'lesson' => 0);
    protected $imported = array();
    protected $valid = 'wishlist-content';
    protected $levels = array();
    protected $imported_levels = array();

    protected $entries = array();

    public function __construct($file)
    {
        $this->data = json_decode(file_get_contents($file));
    }

    public function getData()
    {
        return $this->data;
    }
    public function getImported()
    {
        return $this->imported;
    }

    public function analyze()
    {
        $levels = (array) $this->data->levels;
        foreach ($levels as $key => $lvl) {
            $level = new stdClass;
            $level->id      = $key;
            $level->title   = $lvl->name;
            $level->courses = array();
            
            $this->levels[] = $level;
        }
    }

    public function createProducts()
    {
        global $wpdb, $current_user;

        $zippy = Zippy::instance();

        foreach ($this->imported_levels as $wlm_id => $level) {
            $course = $zippy->make('course', array($level['course']));

            $course_title = get_the_title($level['course']);
            $tier_title   = $zippy->utilities->course->getTierTitle($course, $level['tier']);

            $product_id = wp_insert_post(
                array(
                    'post_author'   => $current_user->ID,
                    'post_title'    => "$course_title - $tier_title",
                    'post_content'  => ' ',
                    'post_status'   => 'publish',
                    'post_type'     => 'product'
                ),
                true
            );

            if (!is_wp_error($product_id)) {
                $this->products[$wlm_id] = $product_id;

                $pricing = new stdClass;
                $pricing->type = 'free';
                $pricing->price = new stdClass;
                $pricing->price->free = new stdClass;
                $pricing->price->single = new stdClass;
                $pricing->price->single->amount = '0.00';
                $pricing->price->subscription = new stdClass;
                $pricing->price->subscription->amount = '0.00';
                $pricing->price->subscription->frequency = 'month';
                $pricing->price->payment_plan = new stdClass;
                $pricing->price->payment_plan->amount = '0.00';
                $pricing->price->payment_plan->frequency = 'month';
                $pricing->price->payment_plan->num_payments = '0';

                $access = new stdClass;
                $access->mode = 'product';
                $access->course = $level['course'];
                $access->tier = $level['tier'];
                $access->bundle = array();

                $launch_windows = new stdClass;
                $launch_windows->launch_windows = array();
                $launch_windows->enabled = false;

                // Update defaults
                update_post_meta($product_id, 'access', json_encode($access));
                update_post_meta($product_id, 'pricing', json_encode($pricing));
                update_post_meta($product_id, 'launch_windows', json_encode($launch_windows));
                update_post_meta($product_id, 'import_level', $wlm_id);

                // Add new product ID to course
                $course_products = array_filter((array) $zippy->utilities->getJsonMeta($level['course'], 'products', true));
                $course_products[] = "$product_id";

                update_post_meta($level['course'], 'products', json_encode($course_products));
            }
        }
    }

    public function setStructureCourses($courses)
    {
        $this->structure_courses = $courses;
    }

    public function setStructureLevels($levels)
    {
        $this->structure_levels = $levels;
    }

    public function createNewCourses()
    {
        global $wpdb, $current_user;

        foreach ($this->structure_courses as &$course) {
            $course->import_id = $course->id;

            $course_id = wp_insert_post(
                array(
                    'post_author' => $current_user->ID,
                    'post_title' => $course->title,
                    'post_status' => 'publish',
                    'post_content' => ' ',
                    'post_type' => 'course'
                ),
                true
            );

            if (!is_wp_error($course_id)) {
                $this->courses[$course->id] = $course_id;
            }
        }
    }

    public function createNewCourseTiers()
    {
        global $wpdb, $current_user;

        $tiers = array();

        foreach ($this->structure_levels as &$level) {
            if (isset($this->courses[$level->course])) {
                $course_id = $this->courses[$level->course];

                if (!isset($tiers[$course_id])) {
                    $tiers[$course_id] = array();
                }

                end($tiers[$course_id]);

                $next_id = key($tiers[$course_id]) !== null ? key($tiers[$course_id]) + 1 : 0;
                
                $tier = new stdClass;
                    $tier->ID = $next_id;
                    $tier->title = stripslashes($level->title);

                $tiers[$course_id][$next_id] = $tier;
                    
                $this->imported_levels[$level->id] = array(
                    'course'    => $course_id,
                    'tier'      => $next_id
                );

                $level->course_id = $course_id;
                $level->tier_id   = $next_id;

                $this->tiers[$level->course_id] = $next_id;
            }
        }

        foreach ($tiers as $course_id => $course_tiers) {
            update_post_meta($course_id, 'tiers', json_encode($course_tiers));
        }
    }

    public function getNewCourseEntry()
    {
        $entry = new stdClass;
        
        $entry->ID = 0;
        $entry->title = '';
        $entry->post_type = '';
        $entry->entries = array();
        $entry->tiers = array();
        $entry->scheduling = new stdClass;
            $entry->scheduling->day_of_week = null;
            $entry->scheduling->num_days = '1';
            $entry->scheduling->required = false;
        $entry->quiz = new stdClass;
            $entry->quiz->ID = 0;
            $entry->quiz->pass = 0;
            $entry->quiz->required = new stdClass;
                $entry->quiz->required->pass = false;
                $entry->quiz->required->completion = false;

        return $entry;
    }

    public function import()
    {
        if ($this->data === null) {
            return false;
        }

        $success = true;

        $this->createNewCourses();
        $this->createNewCourseTiers();
        $this->createProducts();
        $this->importEntries($this->data->content);

        $this->remapLevels($this->imported);

        $this->importEntries($this->data->attachments);
        $this->fetchAttachments();

        $this->importComments();
        
        $this->remapFeaturedImages($attachments);

        return $success;
    }

    public function remapLevels($entries)
    {
        $entries = array_merge($entries['post'], $entries['page']);
        foreach ($entries as $entry_id) {

            $levels = (array) get_post_meta($entry_id, 'levels', true);

            foreach ($levels as $level_id => $title) {
                if (isset($this->imported_levels[$level_id])) {
                    $course_id = $this->imported_levels[$level_id]['course'];
                    if (!isset($this->entries[$course_id])) {
                        $this->entries[$course_id] = array();
                    }

                    $exists = false;
                    foreach ($this->entries[$course_id] as $key => $entry) {
                        if ($entry->ID == $entry_id) {
                            $exists = $key;
                        }

                        if ($exists) {
                            break;
                        }
                    }

                    if ($exists === false) {
                        $entry = $this->getNewCourseEntry();

                        $entry->ID          = $entry_id;
                        $entry->title       = get_the_title($entry_id);
                        $entry->post_type   = get_post_type($entry_id);
                        $entry->tiers       = array($this->imported_levels[$level_id]['tier']);
                        
                        $this->entries[$course_id][] = $entry;
                    } else {
                        if (!in_array($this->imported_levels[$level_id]['tier'], $this->entries[$course_id][$exists]->tiers)) {
                            $this->entries[$course_id][$exists]->tiers[] = $this->imported_levels[$level_id]['tier'];
                        }
                    }
                }
            }
        }
        foreach ($this->entries as $course_id => $entries) {
            update_post_meta($course_id, 'entries', json_encode($entries));
        }
    }

    public function createCourse(stdClass $course)
    {
        global $current_user;

        $course_args = array(
            'post_type' => 'course',
            'post_status' => 'publish',
            'post_author' => $current_user->ID,
            'post_title' => $course->title,
            'post_content' => ''
        );

        $course_id = wp_insert_post($course_args);

        if (!is_wp_error($course_id)) {
            $this->incrementCount();

            $entries = $this->createEntries($course->entries, $course_id);
            
            update_post_meta($course_id, '_lessons-and-units_order', implode(',', $entries));

        }

        return $course_id;
    }

    public function importEntries(array $list)
    {
        global $wpdb, $current_user;

        $new_entries = array();

        foreach ($list as $item) {
            $item->post_author = $current_user->ID;
            $meta = (array) $item->meta;
            

            $args = (array) $item;
            
            unset($args['filter']);
            unset($args['ID']);
            unset($args['meta']);

            $wpdb->insert(
                $wpdb->posts,
                $args
            );

            $new_item_id = $wpdb->insert_id;
            
            if ($new_item_id) {
                if (isset($meta['import_id'])) {
                    unset($meta['import_id']);
                }

                foreach ($meta as $key => $value) {
                    $wpdb->insert($wpdb->postmeta, array('post_id' => $new_item_id, 'meta_key' => $key, 'meta_value' => maybe_serialize($value)), array('%d', '%s', '%s'));
                }
                    
                $new_entries[] = $new_item_id;

                $this->imported[$args['post_type']][] = $new_item_id;
            }
        }

        // Store it here for future use...

        return $this->imported;
    }

    public function importComments()
    {

        global $wpdb, $current_user;

        $new_entries = array();

        $data = $this->data->comments;
        foreach ($data as $item) {
            $args = (array) $item;
            unset($args['meta']);

            $wpdb->insert(
                $wpdb->comments,
                $args
            );

            $new_item_id = $wpdb->insert_id;
            
            if ($new_item_id) {
                $new_entries[] = $new_item_id;
            }
            
        }

        // Store it here for future use...
        $this->remapComments($new_entries);

        return $new_entries;

    }

    public function remapComments(array $list)
    {
        global $wpdb;

        // Remap posts
        foreach ($list as $item_id) {
            $old_post_id = $wpdb->get_var($wpdb->prepare("SELECT comment_post_ID FROM $wpdb->comments WHERE comment_ID = %s", $item_id));
            $new_post_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $old_post_id));

            if ($new_post_id) {
                $wpdb->update($wpdb->comments, array('comment_post_ID' => $new_post_id), array('comment_ID' => $item_id));
            }
        }

        // Remap comment parents
        foreach ($list as $item_id) {
            $old_parent_id = $wpdb->get_var($wpdb->prepare("SELECT comment_parent FROM $wpdb->comments WHERE comment_ID = %s", $item_id));
            $new_parent_id = $wpdb->get_var($wpdb->prepare("SELECT comment_id FROM $wpdb->commentmeta WHERE meta_key = %s AND meta_value = %s", 'import_id', $old_parent_id));

            if ($new_parent_id) {
                $wpdb->update($wpdb->comments, array('comment_parent' => $new_parent_id), array('comment_ID' => $item_id));
            }
        }
    }

    public function fetchAttachments()
    {

        global $wpdb, $current_user;

        $attachments = (array) $this->imported['attachment'];
        
        $importer = new ZippyCourses_Image_Importer($attachments);
        $importer->import();

        return;
    }

    public function remapFeaturedImages()
    {
        global $wpdb;
        $attachments = (array) $this->imported['attachment'];
        foreach ($attachments as $image_id) {
            $import_id = get_post_meta($image_id, 'import_id', true);

            $old = $wpdb->get_col($wpdb->prepare("SELECT pm1.post_id FROM $wpdb->postmeta AS pm1 LEFT JOIN $wpdb->postmeta AS pm2 ON (pm1.post_id = pm2.post_id) WHERE pm1.meta_key = %s AND pm1.meta_value = %s AND pm2.meta_key = %s AND pm2.meta_value != ''", '_thumbnail_id', $import_id, 'import_id'));

            foreach ($old as $post_id) {
                update_post_meta($post_id, '_thumbnail_id', $image_id);
            }
        }
    }

    public function incrementCount($type = 'course')
    {
        $this->count[$type]++;
    }

    public function getCount($type = 'course')
    {
        return $this->count[$type];
    }

    public function validate()
    {
        return true;

        $type = false;

        if (isset($this->data->export_meta)) {
            $type = isset($this->data->export_meta->type) ? $this->data->export_meta->type : false;
        }

        return $type == $this->valid;
    }

    public function getLevels()
    {
        return $this->levels;
    }
}
