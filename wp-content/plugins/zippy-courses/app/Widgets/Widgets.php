<?php

class ZippyCourses_Widgets
{
    public function __construct()
    {
        add_action('admin_print_scripts', array($this, 'template'));
        add_action('widgets_init', array($this, 'register'));

        add_filter('in_widget_form', array($this, 'fields'), 10, 3);
        add_filter('widget_update_callback', array($this, 'update'), 9, 3);
        add_action('sidebars_widgets', array($this, 'visibility'), 10, 1);

        add_filter('widget_text', 'do_shortcode');
        add_filter('widget_title', 'do_shortcode');

    }

    /**
     * Add a custom field to every widget
     *
     * @since 1.0.0
     * @param WP_Widget $this     The widget instance, passed by reference.
     * @param null      $return   Return null if new fields are added.
     */
    public function fields($instance, $new_instance)
    {
        $default_settings_object = new stdClass;
        $default_settings_object->settings = array();

        $settings = $instance->get_settings();
        $student_only = isset($settings[$instance->number]['zippy_student_only']) ? $settings[$instance->number]['zippy_student_only'] : 0;
        $widget_settings = isset($settings[$instance->number]['zippy_widget_settings']) ? $settings[$instance->number]['zippy_widget_settings'] : json_encode($default_settings_object);
        
        echo '<div class="zippy-widget-settings-region"></div>';
        echo '<input type="hidden" id="' . $instance->get_field_id('zippy_student_only') . '" name="' . $instance->get_field_name('zippy_student_only') . '" />';
        echo '<input type="hidden" id="' . $instance->get_field_id('zippy_widget_settings') . '" name="' . $instance->get_field_name('zippy_widget_settings') . '" class="zippy-widget-settings" value=\'' . $widget_settings . '\' />';

        return $instance;
    }

    /**
     * Update the Zippy Widget settings
     *
     * @since  1.0.0.0
     * @param array     $instance     The current widget instance's settings.
     * @param array     $new_instance Array of new widget settings.
     * @param array     $old_instance Array of old widget settings.
     */
    public function update($instance, $new_instance, $widget)
    {
        $instance['zippy_student_only'] = $new_instance['zippy_student_only'];
        $instance['zippy_widget_settings'] = $new_instance['zippy_widget_settings'];
        return $instance;
    }

    /**
     * Register the default ZippyCourses Widgets
     * @return void
     */
    public function register()
    {
        register_widget('ZippyCourses_CompleteEntry_Widget');
        register_widget('ZippyCourses_LoginAccount_Widget');
        register_widget('ZippyCourses_YourCourses_Widget');
        register_widget('ZippyCourses_CourseNavigation_Widget');
        register_widget('ZippyCourses_ProgressBar_Widget');
    }

    public function template()
    {
        $zippy = Zippy::instance();

        include(ZippyCourses::$path . 'assets/views/templates/admin/widget-visibility.php');

        $data = new stdClass;
            $data->courses = array_values($zippy->utilities->course->getAllCoursesAndTiers());
            
        echo '<script type="text/template" id="widget-settings-course-tier-data">' . json_encode($data) . '</script>';
    }

    /**
     * Zippy Widget Visibility
     *
     * Control the visibility of the widgets based on
     *
     * @param  array $widgets List of all of the widgets visible
     * @return array $widgets Modified list of all widgets
     */
    public function visibility($widgets)
    {
        global $current_user;

        $zippy = Zippy::instance();

        if (current_user_can('edit_others_posts')) {
            return $widgets;
        }

        $student = $zippy->make('student', array($current_user->ID));
        $student->fetch();

        // Filter out the unused and inactive widgets
        $zippy_widgets = $this->_filterInactiveWidgets($widgets);

        // Check to see if the widgets are publicly available or not
        foreach ($zippy_widgets as $key => $ws) {
            $values = get_option('widget_' . $key);

            foreach ($ws as $num => $k) {
                if (isset($values[$num])) {
                    if (isset($values[$num]['zippy_widget_settings'])) {
                        $settings = $zippy->utilities->maybeJsonDecode($values[$num]['zippy_widget_settings']);

                        if (isset($settings->type)) {
                            switch ($settings->type) {
                                case '1':
                                case 1:
                                    if (!is_user_logged_in()) {
                                        unset($widgets[$k['region']][$k['key']]);
                                    }
                                    break;
                                case '2':
                                case 2:
                                    if (is_user_logged_in()) {
                                        unset($widgets[$k['region']][$k['key']]);
                                    }
                                    break;
                                case '3':
                                case 3:
                                    if ($student === null || !$student->getId() || !isset($settings->selected) || !is_array($settings->selected)) {
                                        unset($widgets[$k['region']][$k['key']]);
                                    }

                                    if (!$this->_studentHasWidgetAccess($student, $settings->selected)) {
                                        unset($widgets[$k['region']][$k['key']]);
                                    }

                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }

        // Filter student only widgets for non students
        foreach ($zippy_widgets as $key => $ws) {
            $values = get_option('widget_' . $key);

            $student_only   = array('zippy-your-courses-widget', 'zippy-course-navigation-widget', 'zippy-complete-entry-widget');
            $purge          = in_array($key, $student_only);
            
            foreach ($ws as $num => $k) {
                if (($student === null || !$student->getId()) && $purge) {
                    unset($widgets[$k['region']][$k['key']]);
                }
            }
        }

        return $widgets;
    }

    /**
     * Check to see if a widget is accessible by a student
     *
     * @todo   Convert this to a Middleware for consistency
     * @since  1.0.0
     * @param  ZippyCourses_Student $student
     * @param  array                $widget_courses
     * @return bool
     */
    private function _studentHasWidgetAccess(ZippyCourses_Student $student, array $widget_courses)
    {
        $access  = false;

        foreach ($widget_courses as $course) {
            if (($student_course = $student->getCourses()->get($course->id)) !== null) {
                $student_course_tiers = $student->getCourseTiers();
                
                if (array_key_exists($course->id, $student_course_tiers)) {
                    foreach ($student_course_tiers[$course->id] as $tier) {
                        if (in_array($tier['id'], $course->tiers)) {
                            $access = true;
                        }

                        if ($access) {
                            break;
                        }
                    }
                } else {
                }
            } else {
            }

            if ($access) {
                break;
            }
        }

        return $access;
    }

    /**
     * Filter out inactive widgets
     *
     * @since  1.0.0
     * @param  array $widgets
     * @return array $widgets
     */
    private function _filterInactiveWidgets($widgets)
    {
        $output = array();

        foreach ($widgets as $key => $region) {
            if ($key != 'wp_inactive_widgets') {
                // Build out $output with a list of all of the regions
                if (is_array($region)) {
                    foreach ($region as $k => $widget) {
                        $pieces = explode('-', $widget);
                        $number = array_pop($pieces);
                        $widget_base = str_replace('-' . $number, '', $widget);

                        if (!isset( $output[$widget_base ])) {
                            $output[$widget_base] = array();
                        }

                        $output[$widget_base][$number] =  array( 'region' => $key, 'key' => $k );

                    }
                }
            }
        }

        return $output;
    }
}
