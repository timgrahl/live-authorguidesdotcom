<?php

class ZippyCourses_CourseNavigation_Widget extends Zippy_Widget_Model
{
    public function __construct()
    {
        $this->id               = 'zippy-course-navigation-widget';
        $this->title            = '[Zippy Courses] ' . __('Course Navigation', ZippyCourses::TEXTDOMAIN);
        $this->description      = __(
            'Allow quick and easy navigation of a course.',
            ZippyCourses::TEXTDOMAIN
        );

        $this->public_view_path = ZippyCourses::$path . 'assets/views/widgets/CourseNavigation/public.php';
        $this->admin_view_path  = ZippyCourses::$path . 'assets/views/widgets/CourseNavigation/admin.php';

        parent::__construct();
    }

    public function getDefaultValues()
    {
        return array(
            'course_title' => '',
            'unit_title'   => '',
            'entry_title'  => ''
        );
    }

    public function isVisible()
    {
        global $post;

        $zippy = Zippy::instance();

        $entry_ids = $zippy->utilities->entry->getAllEntryIds();

        return is_object($post) && ($post->post_type == 'course' || in_array($post->ID, $entry_ids));
    }
}
