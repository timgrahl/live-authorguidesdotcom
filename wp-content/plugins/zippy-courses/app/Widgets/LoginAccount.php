<?php

class ZippyCourses_LoginAccount_Widget extends Zippy_Widget_Model
{
    public function __construct()
    {
        $this->id               = 'zippy-login-account-widget';
        $this->title            = '[Zippy Courses] ' . __('Login/Account', ZippyCourses::TEXTDOMAIN);
        $this->description      = __(
            "Show a student's progress on the Course, when they are looking at a Course, Unit or Entry.",
            ZippyCourses::TEXTDOMAIN
        );

        $this->public_view_path = ZippyCourses::$path . 'assets/views/widgets/LoginAccount/public.php';
        $this->admin_view_path  = ZippyCourses::$path . 'assets/views/widgets/LoginAccount/admin.php';

        parent::__construct();
    }

    public function getDefaultValues()
    {
        return array(
            'login_title' => '',
            'account_title'   => ''
        );
    }
}
