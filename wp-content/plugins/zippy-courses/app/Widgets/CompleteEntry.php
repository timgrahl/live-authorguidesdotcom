<?php

class ZippyCourses_CompleteEntry_Widget extends Zippy_Widget_Model
{
    public function __construct()
    {
        $this->id               = 'zippy-complete-entry-widget';
        $this->title            = '[Zippy Courses] ' . __('Complete Entry', ZippyCourses::TEXTDOMAIN);
        $this->description      = __(
            'Allow students to complete course entries. This widget is only visible when viewing an entry.',
            ZippyCourses::TEXTDOMAIN
        );

        $this->public_view_path = ZippyCourses::$path . 'assets/views/widgets/CompleteEntry/public.php';
        $this->admin_view_path  = ZippyCourses::$path . 'assets/views/widgets/CompleteEntry/admin.php';

        parent::__construct();
    }

    public function getDefaultValues()
    {
        return array(
            'title' => ''
        );
    }
    
    public function isVisible()
    {
        global $post;

        $zippy = Zippy::instance();

        $entry_ids = $zippy->utilities->entry->getAllEntryIds();

        return is_object($post) && in_array($post->ID, $entry_ids);
    }
}
