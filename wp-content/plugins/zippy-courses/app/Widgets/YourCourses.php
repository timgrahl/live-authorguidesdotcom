<?php

class ZippyCourses_YourCourses_Widget extends Zippy_Widget_Model
{
    public function __construct()
    {
        $this->id               = 'zippy-your-courses-widget';
        $this->title            = '[Zippy Courses] ' . __('Your Courses', ZippyCourses::TEXTDOMAIN);
        $this->description      = __(
            "Show a student's progress on the Course, when they are looking at a Course, Unit or Entry.",
            ZippyCourses::TEXTDOMAIN
        );

        $this->public_view_path = ZippyCourses::$path . 'assets/views/widgets/YourCourses/public.php';
        $this->admin_view_path  = ZippyCourses::$path . 'assets/views/widgets/YourCourses/admin.php';

        parent::__construct();
    }

    public function getDefaultValues()
    {
        return array(
            'title' => ''
        );
    }
}
