<?php

class ZippyCourses_ProgressBar_Widget extends Zippy_Widget_Model
{
    public function __construct()
    {
        $this->id               = 'zippy-progress-bar-widget';
        $this->title            = '[Zippy Courses] ' . __('Course Progress Bar', ZippyCourses::TEXTDOMAIN);
        $this->description      = __(
            "Show progress bars indicating the completion percentage for all of a student's courses.",
            ZippyCourses::TEXTDOMAIN
        );

        $this->public_view_path = ZippyCourses::$path . 'assets/views/widgets/ProgressBar/public.php';
        $this->admin_view_path  = ZippyCourses::$path . 'assets/views/widgets/ProgressBar/admin.php';

        parent::__construct();
    }

    public function getDefaultValues()
    {
        return array(
            'title' => ''
        );
    }
}
