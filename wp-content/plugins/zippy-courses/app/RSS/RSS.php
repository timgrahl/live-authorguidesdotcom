<?php

class ZippyCourses_RSS
{
    public function getNewsFeed()
    {
        $zippy = Zippy::instance();
        $url = 'https://zippycourses.com/category/zippy-updates/feed';

        $last_failure = get_option('zippy_update_feed_failure') ? get_option('zippy_update_feed_failure') : false;

        if ($last_failure) {
            $d = date('Y-m-d H:i:s', $last_failure);

            $d = $zippy->utilities->datetime->getDate($d);
            $d->modify('+1 hour');

            $now = $zippy->utilities->datetime->getNow();
            
            if ($d->format('U') < $now->format('U')) {
                return;
            }
        }

        $rss = fetch_feed($url);

        if (is_wp_error($rss)) {
            update_option('zippy_update_feed_failure', time());
        }

        return $rss;
    }

    public function getItems($count = 5)
    {
        $rss = $this->getNewsFeed();

        if ($rss && !is_wp_error($rss)) {
            // Figure out how many total items there are, but limit it to 5.
            $maxitems = $rss->get_item_quantity($count);

            // Build an array of all the items, starting with element 0 (first element).
            $items = $rss->get_items(0, $maxitems);

            return $maxitems == 0 ? array() : $items;
        }

        return array();
    }
}
