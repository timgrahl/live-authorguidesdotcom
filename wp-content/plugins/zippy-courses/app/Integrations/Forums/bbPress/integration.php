<?php
/**
 * Id: bbpress
 * File Type: forum-integration
 * Class: ZippyCourses_bbPress_ForumIntegration
 *
 * @since 1.0.0
 */
class ZippyCourses_bbPress_ForumIntegration extends Zippy_ForumIntegration
{
    public $id = 'bbpress';

    public function __construct()
    {
        if (function_exists('bbpress')) {
            $this->hooks();
        }
    }

    public function hooks()
    {
        add_filter('wp_get_nav_menu_items', array($this, 'excludeMenuItems'), 10, 3);

        add_filter('get_template_part_content', array($this, 'templatePart'), 10, 2);
        add_filter('get_template_part_content', array($this, 'userTemplatePart'), 11, 2);

        add_filter('bbp_get_template_part', array($this, 'templates'), 10, 3);
        add_filter('bbp_get_template_part', array($this, 'userTemplates'), 10, 3);

        add_action('pre_get_posts', array($this, 'filterForums'), 10, 1);

        add_filter('the_title', array($this, 'title'), 10, 2);
    }

    public function templatePart($slug, $name)
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        $valid = array('single-forum', 'single-topic', 'replies', 'single-reply', 'replies', 'reply', 'anonymous', 'archive-forum', 'search', 'forums');
        $forum_id = bbp_get_forum_id();

        if (!$forum_id) {
            // We need to see how many forums we have access to
            if ($name == 'archive-forum') {
                global $wp_query;

                $access = false;

                foreach ($wp_query->posts as $forum) {
                    if (!$access) {
                        $access = $this->studentHasAccess($forum->ID);
                    }
                }

                if (!$access) {
                    echo '<div class="zippy-alert zippy-alert-error">';

                    echo '<p>' . __('You do not have access to any forums.', ZippyCourses::TEXTDOMAIN) . '</p>';
                    if (is_user_logged_in()) {
                        echo '<p><a href="' . $zippy->core_pages->getUrl('dashboard') . '">' . __('Click here', ZippyCourses::TEXTDOMAIN) . '</a> ' . __('to return to your Dashboard.', ZippyCourses::TEXTDOMAIN) . '</p>';
                    } else {
                        echo '<p><a href="' . home_url(). '">' . __('Click here', ZippyCourses::TEXTDOMAIN) . '</a> ' . __('to return to the Home page.', ZippyCourses::TEXTDOMAIN) . '</p>';
                    }

                    echo '</div>';
                }
            }
        } else {
            if (!$this->studentHasAccess($forum_id)) {
                echo '<div class="zippy-alert zippy-alert-error">';

                echo '<p>' . __('You do not have access to any forums.', ZippyCourses::TEXTDOMAIN) . '</p>';
                if (is_user_logged_in()) {
                    echo '<p><a href="' . $zippy->core_pages->getUrl('dashboard') . '">' . __('Click here', ZippyCourses::TEXTDOMAIN) . '</a> ' . __('to return to your Dashboard.', ZippyCourses::TEXTDOMAIN) . '</p>';
                } else {
                    echo '<p><a href="' . home_url(). '">' . __('Click here', ZippyCourses::TEXTDOMAIN) . '</a> ' . __('to return to the Home page.', ZippyCourses::TEXTDOMAIN) . '</p>';
                }

                echo '</div>';
            }
        }
    }

    public function userTemplatePart($slug, $name)
    {
        if ($slug == 'content' && $name == 'single-user') {
            if (!is_user_logged_in()) {
                echo '<p class="zippy-alert zippy-error error">';
                printf(__('You do not have permission to view this User\'s profile. <a href="%s">Click here</a> to return to the home page.', ZippyCourses::TEXTDOMAIN), home_url());
                echo '</p>';
            }
        }

    }

    public function userTemplates($templates, $slug, $name)
    {
        if ($slug == 'content' && $name == 'single-user' && !is_user_logged_in()) {
            return false;
        }

        return $templates;

    }

    public function templates($templates, $slug, $name)
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        $valid = array('single-forum', 'single-topic', 'replies', 'single-reply', 'replies', 'reply', 'anonymous', 'archive-forum', 'search', 'forums');
        $forum_id = bbp_get_forum_id();

        if (!$forum_id) {
            // We need to see how many forums we have access to
            if ($name == 'archive-forum') {
                global $wpdb;

                $forums = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type = 'forum' AND post_status = 'publish' ORDER BY post_date DESC");

                $access = false;

                foreach ($forums as $forum) {
                    if (!$access) {
                        $access = $this->studentHasAccess($forum->ID);
                    }
                }

                if (!$access) {
                    return false;
                }

            }

            return $templates;
        }

        if ($slug == 'content' && in_array($name, $valid)) {
            return $this->studentHasAccess($forum_id) ? $templates : false;
        }

        return $templates;
    }

    public function title($title, $post_id)
    {

        $forum_id = bbp_get_forum_id();

        if ($forum_id) {
            if (!$this->studentHasAccess($forum_id)) {
                $title = 'Forums';
            }
        } else {
            global $wp_query;
            if (isset($wp_query->query['bbp_user']) && !is_user_logged_in()) {
                $title = "User Profile";
            }
        }

        return $title;

    }

    public function getForums()
    {
        global $wpdb;

        return $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_type = %s AND post_status = %s", 'forum', 'publish'));
    }

    public function getForumTopicIds($forum_id)
    {
        global $wpdb;

        return $wpdb->get_results($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s AND post_status = %s AND post_parent = %s", 'topic', 'publish', $forum_id));
   
    }

    public function getTopicReplyIds($topic_id)
    {
        global $wpdb;

        return $wpdb->get_results($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s AND post_status = %s AND post_parent = %s", 'reply', 'publish', $topic_id));
    }

    public function filterForums($query)
    {
        if (is_admin()) {
            return $query;
        }

        global $wpdb;

        if (bbp_is_forum_archive()) {
            $forums = $this->getForums();
            $restricted_forums = array();

            foreach ($forums as $forum) {
                if (!$this->studentHasAccess($forum->ID)) {
                    $restricted_forums[] = $forum->ID;
                }
            }

            $query->set('post__not_in', $restricted_forums);
        }

        if (bbp_is_search()) {
            $forums = $this->getForums();
            $restricted_forums = array();

            foreach ($forums as $forum) {
                if (!$this->studentHasAccess($forum->ID)) {
                    $restricted_forums[] = $forum->ID;
                }
            }

            $restricted_items = $restricted_forums;
            foreach ($restricted_forums as $forum_id) {
                $topics = $this->getForumTopicIds($forum_id);
                
                foreach ($topics as $topic) {
                    $restricted_items[] = $topic->ID;

                    $replies = $this->getTopicReplyIds($topic->ID);
                    foreach ($replies as $reply) {
                        $restricted_items[] = $reply->ID;
                    }
                }

            }

            $query->set('post__not_in', $restricted_items);
        }
    }

    public function studentHasAccess($post_id)
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        if ($student === null) {
            return false;
        }

        if (user_can($student->getId(), 'edit_others_posts')) {
            return true;
        }
        
        $access = false;

        $forum_id   = bbp_get_forum_id($post_id);
        $forum      = $zippy->make('forum', array($forum_id));

        if (!$forum->isProtected()) {
            return true;
        }
        $course_tiers = $student->getCourseTiers();
        foreach ($course_tiers as $course_id => $tiers) {
            if (!empty($tiers)) {
                foreach ($tiers as $tier) {
                    if (!$access) {
                        $access = $forum->hasCourseTier($course_id, $tier['id']);

                        if ($access) {
                            break;
                        }
                    }
                }
            }

            if ($access) {
                break;
            }
        }

        return $access;

    }

    public function excludeMenuItems($items, $menu, $args)
    {
        global $wpdb;

        if (is_admin()) {
            return $items;
        }

        $zippy = Zippy::instance();

        $student                = $zippy->cache->get('student');
        
        $forums = $this->getForums();
        $restricted_forums = array();

        foreach ($forums as $forum) {
            if (!$this->studentHasAccess($forum->ID)) {
                $restricted_forums[] = $forum->ID;
            }
        }

        foreach ($items as $key => $item) {
            if (in_array($item->object_id, $restricted_forums)) {
                unset($items[$key]);
            }
        }

        return $items;
    }
}
