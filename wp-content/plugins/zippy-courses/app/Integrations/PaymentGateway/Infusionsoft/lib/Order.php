<?php
/**
 * This is an adapter for our Infusionsoft adapter.  Infusionsoft stores requisite information across a variety of API
 * calls, so we create this object to fetch and normalize all of the data
*/
class ZippyCourses_Infusionsoft_Order
{
    public $id;
    public $invoice;
    public $payments;
    public $payment;
    public $contact;
    public $contact_id;
    public $order_items;
    public $subscription_plan_id;
    public $recurring_order;
    public $pay_plan;
    public $pay_plan_items;

    public function __construct($id)
    {
        $this->id   = $id;
        $this->api  = new ZippyCourses_Infusionsoft_PaymentGatewayAPI;
    }

    public function fetchAll()
    {
        $this->invoice              = $this->fetchInvoice();
        
        $this->payments             = $this->fetchPayments();

        if (count($this->payments)) {
            $this->payment              = reset($this->payments);
        }

        $this->contact              = $this->fetchContact();
        $this->contact_id           = $this->fetchContactId();
        $this->order_items          = $this->fetchOrderItems();
        $this->pay_plan             = $this->fetchPayPlan();

        if (isset($this->pay_plan['Id'])) {
            $this->pay_plan_items   = $this->fetchPayPlanItems($this->pay_plan['Id']);
        }

        if (count($this->order_items) > 0) {
            $this->subscription_plan_id = $this->getSubscriptionPlanId();

            if ($this->subscription_plan_id) {
                $this->recurring_order  = $this->fetchRecurringOrder($this->subscription_plan_id, $this->contact_id);
            }
        }
    }

    public function getInvoice()
    {
        if ($this->invoice === null) {
            $this->invoice = $this->fetchInvoice();
        }

        return $this->invoice;
    }

    public function fetchInvoice()
    {
        return $this->api->getInvoice($this->id);
    }

    public function fetchPayments()
    {
        return $this->api->getPaymentsForInvoice($this->invoice['Id']);
    }

    public function fetchOrderItems()
    {
        return $this->api->getOrderItems($this->id);
    }

    public function fetchPayPlan()
    {
        return $this->api->getPayPlan($this->id);
    }

    public function fetchPayPlanItems($pay_plan_id)
    {
        return $this->api->getPayPlanItems($pay_plan_id);
    }

    public function getPayPlanItems()
    {
        if ($this->pay_plan_items === null && $this->pay_plan !== null) {
            $this->pay_plan_items = $this->fetchPayPlanItems($this->pay_plan['Id']);
        }

        return $this->pay_plan_items !== null ? $this->pay_plan_items : array();
    }

    public function fetchRecurringOrder($plan_id, $contact_id)
    {
        return $this->api->getRecurringOrder($plan_id, $contact_id);
    }

    public function getSubscriptionPlanId()
    {
        return isset($this->order_items[0]) ? $this->order_items[0]['SubscriptionPlanId'] : null;
    }

    public function fetchContactId()
    {
        if ($this->invoice !== null) {
            return $this->invoice['ContactId'];
        }

        if ($this->payment !== null) {
            return $this->payment['ContactId'];
        }

        return null;
    }

    public function fetchContact()
    {
        $contact    = null;
        $contact_id = null;

        if ($this->invoice !== null) {
            $contact_id = $this->invoice['ContactId'];
        }

        if ($this->payment !== null) {
            $contact_id = $this->payment['ContactId'];
        }

        if ($contact_id !== null) {
            $contact = $this->api->getContact('Id', $contact_id);
        }

        return $contact ? $contact : null;
    }

    /**
     * Gets the value of recurring_order.
     *
     * @return mixed
     */
    public function getRecurringOrder()
    {
        return $this->recurring_order;
    }

    /**
     * Sets the value of recurring_order.
     *
     * @param mixed $recurring_order the recurring order
     *
     * @return self
     */
    public function setRecurringOrder($recurring_order)
    {
        $this->recurring_order = $recurring_order;

        return $this;
    }
}
