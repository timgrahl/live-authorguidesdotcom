<?php

class ZippyCourses_InfusionsoftGateway_Utilities
{
    public function getProductsList()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = %s", 'infusionsoft_product_id');
        $used_ids = $wpdb->get_col($sql);

        $api = new ZippyCourses_Infusionsoft_PaymentGatewayAPI;

        if (($products = get_transient('_zippy_infusionsoft_products')) === false) {
            $products = $api->getProducts();

            set_transient('_zippy_infusionsoft_products', $products, HOUR_IN_SECONDS);
        }

        $output = array();

        foreach ($products as $product) {
            $output[$product['Id']] = array(
                'id'    => $product['Id'],
                'name'  => isset($product['ProductName']) ? $product['ProductName'] : '',
                'used'  => in_array($product['Id'], $used_ids)
            );
        }

        return $output;
    }
}
