<?php

class ZippyCourses_Infusionsoft_OrderFormProcessor extends Zippy_FormProcessor
{
    public function fail()
    {
        $zippy = Zippy::instance();
        $zippy->session->flashMessage('There was an error processing your request', 'error');
    }

    public function execute()
    {
        $zippy = Zippy::instance();

        $product_id = $_POST['product_id'];
        $product    = $zippy->make('product', array('id' => $_POST['product_id']));

        $order_form_url = get_post_meta($_POST['product_id'], 'infusionsoft_order_form_url', true);

        if (!empty($order_form_url)) {
            wp_redirect($order_form_url);
            exit;
        } else {
            $zippy->log('Product #' . $product_id . ' does not have an Infusionsoft Order Form URL.', 'INFUSIONSOFT_ERROR');

            $error_message  = sprintf(__('We could not find an Order Form for this product. If this problem persists, please <a href="mailto:%s">contact us</a>.', ZippyCourses::TEXTDOMAIN), $zippy->utilities->email->getSystemEmailAddress());

            $zippy->sessions->flashMessage($error_message, 'error', 0);
        }
    }
}
