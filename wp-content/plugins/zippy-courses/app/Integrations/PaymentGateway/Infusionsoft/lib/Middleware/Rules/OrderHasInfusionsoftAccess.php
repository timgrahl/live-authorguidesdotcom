<?php

class Zippy_OrderHasInfusionsoftAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You do not own access to this content.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'order-has-infusionsoft-access';
    }

    public function handle($object)
    {
        if (!($object instanceof Zippy_Order)) {
            $this->exitEarly(true);
        }

        if ($object->getGateway() !== 'infusionsoft') {
            return $this->pass($object);
        }

        $invoice_id = get_post_meta($object->getId(), 'infusionsoft_invoice_id', true);

        if (empty($invoice_id)) {
            $this->exitEarly(true);
        }

        $zippy = Zippy::instance();

        switch ($object->getType()) {
            case 'subscription':
                $access = $this->_handleRecurringAccess($object);
                break;
            case 'payment-plan':
                $access = $this->_handleRecurringAccess($object);
                break;
            case 'single':
            default:
                $access = $this->_handleSinglePaymentAccess($object);
                break;
        }

        if ($this->_transientExpired($object)) {
            $expire = $zippy->utilities->datetime->getNow();
            $expire->modify('+2 hours');

            $status = array(
                'active' => ($access ? 'yes' : 'no'),
                'expire' => $expire->format('U')
            );

            update_post_meta($object->getId(), 'infusionsoft_order_status', $status);
        }
        $access = apply_filters('zippy_courses_infusionsoft_order_access', $access, $object);
        return $access ? $this->pass($object) : $this->fail($object);
    }

    private function _transientExpired($object)
    {
        $zippy = Zippy::instance();

        $status = get_post_meta($object->getId(), 'infusionsoft_order_status', true);
        $now    = $zippy->utilities->datetime->getNow();

        return empty($status) || !isset($status['expire']) || $now->format('U') > $status['expire'];
    }

    private function _checkTransientStatus($object)
    {
        $zippy = Zippy::instance();
        
        $status = get_post_meta($object->getId(), 'infusionsoft_order_status', true);
        $now    = $zippy->utilities->datetime->getNow();

        if (!empty($status) &&
            isset($status['expire']) &&
            $now->format('U') < $status['expire'] &&
            isset($status['active']) && $status['active'] == 'yes'
        ) {
            return true;
        }

        return false;
    }

    private function _handleSinglePaymentAccess($object)
    {
        if (($status = $this->_checkTransientStatus($object)) === true) {
            return true;
        }

        $invoice_id = get_post_meta($object->getId(), 'infusionsoft_invoice_id', true);
        $order = new ZippyCourses_Infusionsoft_Order($invoice_id);

        $invoice_id = get_post_meta($object->getId(), 'infusionsoft_invoice_id', true);
        $invoice = $order->fetchInvoice();

        return $invoice['RefundStatus'] < 1;
    }

    private function _handleRecurringAccess($object)
    {
        if (($status = $this->_checkTransientStatus($object)) === true) {
            return true;
        }
        
        $invoice_id = get_post_meta($object->getId(), 'infusionsoft_invoice_id', true);
        $order = new ZippyCourses_Infusionsoft_Order($invoice_id);

        $order->fetchAll();

        if ($order->subscription_plan_id !== null) {
            return $this->_handleSubscriptionPlanAccess($order);
        }

        if ($order->pay_plan !== null) {
            return $this->_handlePayPlanAccess($order);
        }

        return false;
    }

    private function _handleSubscriptionPlanAccess(ZippyCourses_Infusionsoft_Order $order)
    {
        $invoice         = $order->getInvoice();
        $recurring_order = $order->getRecurringOrder();

        $access = true;

        if (!isset($recurring_order['Status']) && $recurring_order['Status'] != 'Active') {
            $access = false;
        }

        if ($invoice['RefundStatus'] == 1) {
            $access = false;
        }

        return $access;
    }

    private function _handlePayPlanAccess(ZippyCourses_Infusionsoft_Order $order)
    {
        $invoice = $order->getInvoice();
        $pay_plan_items = $order->getPayPlanItems();

        $access = true;

        foreach ($payment_plan_items as $ppi) {
            if ($ppi['Status'] != 0 &&
                $ppi['Status'] != 1 &&
                $ppi['Status'] != 2
            ) {
                $access = false;
            }
        }

        if ($invoice['RefundStatus'] == 1) {
            $access = false;
        }

        return $access;
    }
}
