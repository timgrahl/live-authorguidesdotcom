<?php

class ZippyCourses_InfusionsoftInvoice_PaymentGatewayTransactionAdapter extends Zippy_PaymentGatewayTransactionAdapter
{
    public $gateway = 'infusionsoft';
    
    public function normalize($data)
    {
        if ($this->normalized === null) {
            $zippy = Zippy::instance();

            $transaction        = $zippy->make('transaction');
            $transaction_key    = $this->normalizeKey($data);
            $transaction->buildByTransactionKey($transaction_key);

            $vendor_id          = $this->normalizeVendorId($data);
            $id                 = $this->normalizeId($transaction_key, $vendor_id);

            $product            = $transaction->getProduct();
            $order              = $this->normalizeOrder($id);
           
            $this->normalized = array(
                'id'                    => $id,
                'key'                   => $transaction_key,
                'title'                 => get_the_title($id),
                'vendor_id'             => $vendor_id,
                'total'                 => $this->normalizeTotal($data),
                'fee'                   => $this->normalizeFee($data),
                'tax'                   => $this->normalizeTax($data),
                'currency'              => $this->normalizeCurrency($data),
                'customer'              => $transaction->getCustomer(),
                'type'                  => $this->normalizeType($transaction),
                'timestamp'             => $this->normalizeTimestamp($data),
                'status'                => $this->normalizeStatus($data),
                'gateway'               => $this->gateway,
                'mode'                  => $this->normalizeMode($data),
                'recurring'             => $this->normalizeRecurring($data),
                'recurring_id'          => $this->normalizeRecurringId($data),
                'order'                 => $order,
                'owner'                 => $transaction->getOwner(),
                'method'                => $this->normalizeMethod($data),
                'product'               => $product,
                'raw'                   => json_encode($data)
            );
        }

        return $this->normalized;
    }


    protected function normalizeId($transaction_key, $vendor_id)
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'transaction_key', $transaction_key);
        $transaction_id = (int) $wpdb->get_var($sql);

        // If we've never seen this transaction key, it's a new transaction, so return right away.
        if ($transaction_id === 0) {
            return $transaction_id;
        }

        // If $vendor_id has not been passed in as 0 (like in the event of subscr_signup, etc),
        // then we can use it to see if this vendor_id has been used before.  Otherwise, it should be 0.
        if ($vendor_id) {
            $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'vendor_id', $vendor_id);
            $vendor_id_exists = $vendor_id ? $wpdb->get_var($sql) : null;

            if ($vendor_id_exists) {
                return $vendor_id_exists;
            }
        }

        // If an item has this transaction key and an empty vendor ID, use that record.  If an item has this transaction key, but has a vendor ID, create a new one...
        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE post_id = %s AND meta_key = %s", $transaction_id, 'vendor_id');
        $has_vendor_id = $wpdb->get_var($sql);

        if (empty($has_vendor_id)) {
            return $transaction_id;
        }

        return 0;
    }

    /**
     * Get the transaction key of this notification. If the Post ID given is not empty,
     * it means that we're getting a payment in a recurring cycle that has not been recorded yet.
     *
     * @return string|null
     */
    protected function normalizeKey($input)
    {
        $key = null;

        if ($input->object == 'charge' || $input->object == 'subscription') {
            $key = isset($input->metadata->zippy_transaction_key)
                ? $input->metadata->zippy_transaction_key
                : null;
        }

        if ($input->object == 'invoice') {
            $key = isset($input->lines->data[0]->metadata->zippy_transaction_key)
                ? $input->lines->data[0]->metadata->zippy_transaction_key
                : null;
        }

        return $key;
    }

    protected function normalizeType($transaction)
    {
        if ($transaction instanceof Zippy_Transaction) {
            return $transaction->getType();
        }

        return 'single';
    }

    protected function normalizeTotal($input)
    {
        $total = 0;

        if ($input->object == 'charge') {
            $total = isset($input->amount) ? $input->amount / 100 : 0;
        }

        if ($input->object == 'invoice') {
            $total = isset($input->total) ? $input->total / 100 : 0;
        }

        return $total;
    }

    protected function normalizeFee($input)
    {
        return 0;
    }

    protected function normalizeCurrency($input)
    {
        return 'USD';
    }

    protected function normalizeVendorId($input)
    {
        $vendor_id = 0;

        if ($input->object == 'charge') {
            $vendor_id = $input->id ? $input->id : 0;
        }

        if ($input->object == 'invoice') {
            $vendor_id = $input->charge ? $input->charge : 0;
        }

        return $vendor_id;
    }

    protected function normalizeProduct($input)
    {
    }

    protected function normalizeTax($input)
    {
    }

    protected function normalizeMode($input)
    {
        return 'live';
    }

    protected function normalizeMethod($input)
    {
        return 'credit_card';
    }

    protected function normalizeRecurringId($input)
    {
        if ($input->object == 'invoice') {
            return $input->subscription;
        }

        if ($input->object == 'subscription') {
            return $input->id;
        }

        return;
    }

    protected function normalizeRecurring($input)
    {
        if ($input->object == 'invoice' || $input->object == 'subscription') {
            return true;
        }

        if ($input->object == 'charge') {
            return isset($input->invoice);
        }
    }

    /**
     * Map status to valid transaction status: complete|pending|refunded|failed|revoked|cancelled
     * @param  string $input The status from gateway
     * @return string
     */
    protected function normalizeStatus($input)
    {
        $status = 'pending';

        if ($input->object == 'invoice' || $input->object == 'charge') {
            $status = $input->paid ? 'complete' : 'failed';
        }

        return $status;
    }

    protected function normalizeTimestamp($input)
    {
        $zippy = Zippy::instance();

        if ($input->object == 'invoice') {
            return $zippy->utilities->datetime->getDateFromTimestamp($input->date);
        }

        if ($input->object == 'charge') {
            return $zippy->utilities->datetime->getDateFromTimestamp($input->created);
        }

        return $zippy->utilities->datetime->getNow();
    }

    protected function normalizeOrder($id)
    {
        $zippy = Zippy::instance();

        if ($id) {
            $order_id = get_post_meta($id, 'order_id', true);

            if (!empty($order_id)) {
                $order = $zippy->make('order');
                $order->build($order_id);

                return $order;
            }
        }

        return;
    }
}
