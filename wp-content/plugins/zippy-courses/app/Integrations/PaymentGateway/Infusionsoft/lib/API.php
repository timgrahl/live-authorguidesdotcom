<?php

/**
 * Infusionsoft Gateway for Zippy Courses.  Using this API is useful because it does not require CURL as the official
 * library does, and it also uses wp_remote_post to make remote requests.
 *
 * The primary downside is that not everything is a particular type of Infusionsoft Object as the official SDK is,
 * but the functionality is leaner.
 */
if (!class_exists('iSDK')) {
    require_once(ZippyCourses::$path . '/vendor/infusionsoft/isdk.php');
}

class ZippyCourses_Infusionsoft_PaymentGatewayAPI extends Zippy_PaymentGatewayAPI
{
    public $ID;

    protected $_user_id;
    protected $_product_id;
    protected $_type;
    protected $_details;
    protected $_data;
    protected $_payment;
    protected $_invoice;
    protected $_pay_plan;
    protected $_pay_plan_items;
    protected $_recurring_order;
    protected $_product;
    protected $_items;
    protected $_contact_id;
    protected $_order;

    private $_app;

    public function __construct()
    {
        $options = array_filter((array) get_option('zippy_infusionsoft_payment_gateway_settings', array()));
        
        $this->api_key = isset($options['api_key']) ? $options['api_key'] : '';
        $this->app_id  = isset($options['app_id']) ? $options['app_id'] : '';
    }

    public function app()
    {
        $app = new iSDK($this->api_key, $this->app_id);
        if (!$app->cfgCon("connectionName")) {
            throw new Exception('Could not connect to InfusionSoft App');
        }

        return $app;
    }

    public function getProducts()
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $product_id = '%';

        $results = $app->dsQuery('Product', 1000, 0, array('Id' => $product_id), array(
            'BottomHTML',
            'CityTaxable',
            'CountryTaxable',
            'Description',
            'HideInStore',
            'Id',
            'InventoryLimit',
            'InventoryNotifiee',
            'IsPackage',
            'LargeImage',
            'NeedsDigitalDelivery',
            'ProductName',
            'ProductPrice',
            'Shippable',
            'ShippingTime',
            'ShortDescription',
            'Sku',
            'StateTaxable',
            'Status',
            'Taxable',
            'TopHTML',
            'Weight'
        ));

        return $results;
    }

    public function getType()
    {
        $type = 'single';

        if (!empty($this->_recurring_order)) {
            $type = 'subscription';
        }

        if (count($this->pay_plan_items) > 1) {
            $type = 'payment-plan';
        }

        return $type;

    }

    public function getPaymentsForInvoice($id)
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $result = $app->dsQueryOrderBy(
            'Payment',
            1000,
            0,
            array(
                'InvoiceId' => $id
            ),
            array(
                'ChargeId',
                'Commission',
                'ContactId',
                'Id',
                'InvoiceId',
                'PayAmt',
                'PayDate',
                'PayNote',
                'PayType',
                'RefundId',
                'Synced',
                'UserId'
            ),
            'PayDate',
            false
        );

        return !empty($result) ? $result : array();
    }

    public function getPaymentsForContact($id)
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $result = $app->dsQueryOrderBy(
            'Payment',
            1000,
            0,
            array(
                'ContactId' => $id
            ),
            array(
                'ChargeId',
                'Commission',
                'ContactId',
                'Id',
                'InvoiceId',
                'PayAmt',
                'PayDate',
                'PayNote',
                'PayType',
                'RefundId',
                'Synced',
                'UserId'
            ),
            'PayDate',
            false
        );

        return !empty($result) ? $result : array();
    }

    public function getRecurringOrder($plan_id, $contact_id)
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $result = $app->dsQuery('RecurringOrder', 1000, 0, array( 'SubscriptionPlanId' => $plan_id, 'ContactId' => $contact_id ), array(
            'AffiliateId',
            'AutoCharge',
            'BillingAmt',
            'BillingCycle',
            'CC1',
            'CC2',
            'ContactId',
            'EndDate',
            'Frequency',
            'Id',
            'LastBillDate',
            'LeadAffiliateId',
            'MaxRetry',
            'MerchantAccountId',
            'NextBillDate',
            'NumDaysBetweenRetry',
            'OriginatingOrderId',
            'PaidThruDate',
            'ProductId',
            'ProgramId',
            'PromoCode',
            'Qty',
            'ReasonStopped',
            'ShippingOptionId',
            'StartDate',
            'Status',
            'SubscriptionPlanId'
        ));

        return !empty( $result ) ? $result[0] : array();
    }


    public function getPayPlan($id = '%')
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $result = $app->dsQuery('PayPlan', 1000, 0, array( 'InvoiceId' => $id ), array(
            'AmtDue',
            'DateDue',
            'FirstPayAmt',
            'Id',
            'InitDate',
            'InvoiceId',
            'StartDate',
            'Type'
        ));

        return !empty( $result ) ? $result[0] : array();
    }

    public function getPayPlanItems($id)
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $result = $app->dsQuery('PayPlanItem', 1000, 0, array( 'PayPlanId' => $id ), array(
            'AmtDue',
            'AmtPaid',
            'DateDue',
            'Id',
            'PayPlanId',
            'Status'
        ));

        return $result;
    }


    public function getOrderItems($id)
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $results = $app->dsQuery('OrderItem', 1000, 0, array('OrderId' => $id), array(
            'CPU',
            'Id',
            'ItemDescription',
            'ItemName',
            'ItemType',
            'Notes',
            'OrderId',
            'PPU',
            'ProductId',
            'Qty',
            'SubscriptionPlanId'
        ));

        return $results;
    }

    public function getContactGroup($id = '%')
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }
        
        $page = 0;
        $completed = false;
        $all_results = array();
        
        while (!$completed) {
            $results = $app->dsQuery('ContactGroup', 1000, $page, array('Id' => $id ), array(
                'GroupCategoryId',
                'GroupDescription',
                'GroupName',
                'Id'
            ));

            $all_results = array_merge($all_results, $results);

            if (count($results) < 1000) {
                $completed = true;
            } else {
                $page++;
            }
        }

        return !empty($all_results)
            ? $id == '%'
                ? $all_results
                : $all_results[0]
            : false;
    }

    public function getInvoice($id)
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $results = $app->dsQuery('Invoice', 1000, 0, array('JobId' => $id ), array(
            'AffiliateId',
            'ContactId',
            'CreditStatus',
            'DateCreated',
            'Description',
            'Id',
            'InvoiceTotal',
            'InvoiceType',
            'JobId',
            'LeadAffiliateId',
            'PayPlanStatus',
            'PayStatus',
            'ProductSold',
            'PromoCode',
            'RefundStatus',
            'Synced',
            'TotalDue',
            'TotalPaid'
        ));

        return !empty($results) ? ( $id == '%' ? $results : $results[0]) : array();
    }

    public function getJob($id)
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }
        
        $results = $app->dsQuery('Job', 1000, 0, array('Id' => $id ), array(
            'ContactId',
            'DateCreated',
            'DueDate',
            'Id',
            'JobNotes',
            'JobRecurringId',
            'JobStatus',
            'JobTitle',
            'OrderStatus',
            'OrderType',
            'ProductId',
            'ShipCity',
            'ShipCompany',
            'ShipCountry',
            'ShipFirstName',
            'ShipLastName',
            'ShipMiddleName',
            'ShipPhone',
            'ShipState',
            'ShipStreet1',
            'ShipStreet2',
            'ShipZip',
            'StartDate'
        ));

        return !empty($results) ? ($id == '%' ? $results : $results[0]) : array();
    }

    public function getData()
    {
        return array(
            'vendor_id'         => $this->ID,
            'contact_id'        => $this->_contact_id,
            'invoice'           => $this->_invoice,
            'payment'           => $this->_payment,
            'items'             => $this->_items,
            'pay_plan'          => $this->_pay_plan,
            'pay_plan_items'    => $this->_pay_plan_items,
            'recurring_order'   => $this->_recurring_order,
        );

    }

    public function getTotal()
    {
        return $this->_invoice['TotalPaid'];
    }

    private function _getContactId()
    {
        return $this->_invoice['ContactId'];
    }


    public function getContact($field = 'Id', $value = '%')
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $results = $app->dsQuery('Contact', 1000, 0, array($field => $value), array(
            'AccountId',
            'Address1Type',
            'Address2Street1',
            'Address2Street2',
            'Address2Type',
            'Address3Street1',
            'Address3Street2',
            'Address3Type',
            'Anniversary',
            'AssistantName',
            'AssistantPhone',
            'BillingInformation',
            'Birthday',
            'City',
            'City2',
            'City3',
            'Company',
            'CompanyID',
            'ContactNotes',
            'ContactType',
            'Country',
            'Country2',
            'Country3',
            'CreatedBy',
            'DateCreated',
            'Email',
            'EmailAddress2',
            'EmailAddress3',
            'Fax1',
            'Fax1Type',
            'Fax2',
            'Fax2Type',
            'FirstName',
            'Groups',
            'Id',
            'JobTitle',
            'LastName',
            'LastUpdated',
            'LastUpdatedBy',
            'LeadSourceId',
            'Leadsource',
            'MiddleName',
            'Nickname',
            'OwnerID',
            'Password',
            'Phone1',
            'Phone1Ext',
            'Phone1Type',
            'Phone2',
            'Phone2Ext',
            'Phone2Type',
            'Phone3',
            'Phone3Ext',
            'Phone3Type',
            'Phone4',
            'Phone4Ext',
            'Phone4Type',
            'Phone5',
            'Phone5Ext',
            'Phone5Type',
            'PostalCode',
            'PostalCode2',
            'PostalCode3',
            'ReferralCode',
            'SpouseName',
            'State',
            'State2',
            'State3',
            'StreetAddress1',
            'StreetAddress2',
            'Suffix',
            'Title',
            'Username',
            'Validated',
            'Website',
            'ZipFour1',
            'ZipFour2',
            'ZipFour3'
        ));

        return !empty( $results ) ? $results[0] : false;
    }
}
