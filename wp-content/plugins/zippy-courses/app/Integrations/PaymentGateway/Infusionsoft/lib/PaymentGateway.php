<?php

class ZippyCourses_Infusionsoft_PaymentGateway extends Zippy_PaymentGateway
{
    public $id      = 'infusionsoft';
    public $name    = 'Infusionsoft';

    public function __construct()
    {
        parent::__construct();
    }

    public function setup()
    {
    }

    public function register()
    {
    }

    public function registerListener()
    {
        $this->listener = new ZippyCourses_Infusionsoft_PaymentGatewayListener($this->id);
        $this->listener->register();
    }

    public function getId()
    {
        return $this->id;
    }
}
