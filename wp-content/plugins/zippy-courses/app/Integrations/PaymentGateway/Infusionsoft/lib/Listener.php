<?php

class ZippyCourses_Infusionsoft_PaymentGatewayListener extends Zippy_PaymentGatewayListener
{
    public function register()
    {
        parent::register();

        $zippy = Zippy::instance();

        $zippy->events->register('OrderStatusChange', $this);
        $zippy->events->register('ApiRequest', $this);
        $zippy->events->register('OrderOwnerChange', $this);
        $zippy->events->register('SaveTransaction', $this);
        $zippy->events->register('OrderSave', $this);
    }

    public function handle(Zippy_Event $event)
    {
        switch ($event->getEventName()) {
            case 'TransactionNotification':
                if ($this->validateGateway($event)) {
                    $this->process($event);
                }
                break;
            case 'ApiRequest':
                if ($event->endpoint == 'infusionsoft') {
                    $this->_handleApiRequest($event);
                }
                break;
            case 'OrderSave':
                $this->_handleOrderSave($event);
                break;
            case 'OrderOwnerChange':
                $this->_handleOrderOwnerChange($event);
                break;
            default:
                break;
        }
    }

    private function _handleOrderOwnerChange(Zippy_Event $event)
    {
        $order = $event->order;

        if (!$order->transactions->count()) {
            $order->fetchTransactions();
        }

        $transaction = $order->transactions->first();

        if (!$transaction) {
            return;
        }

        $vendor     = get_post_meta($transaction->getId(), 'vendor', true);
        $details    = get_post_meta($transaction->getId(), 'details', true);

        if ($vendor !== 'infusionsoft' || !is_array($details) || !isset($details['raw'])) {
            return;
        }

        $json = json_decode($details['raw']);
        $infusionsoft_invoice_id = false;

        if (isset($json->contact_id)) {
            $infusionsoft_contact_id = $json->contact_id;
        }

        if ($infusionsoft_contact_id) {
            $old_user_contact_id = get_user_meta($event->old_owner, 'infusionsoft_contact_id', true);
            if ($old_user_contact_id == $infusionsoft_contact_id) {
                delete_user_meta($event->old_owner, 'infusionsoft_contact_id');
            }

            update_user_meta($event->new_owner, 'infusionsoft_contact_id', $infusionsoft_contact_id);
        }
    }

    private function _handleOrderSave(Zippy_Event $event)
    {
        $order = $event->order;

        if (!$order->transactions->count()) {
            $order->fetchTransactions();
        }

        $transaction = $order->transactions->first();

        if (!$transaction) {
            return;
        }

        $vendor     = get_post_meta($transaction->getId(), 'vendor', true);
        $details    = get_post_meta($transaction->getId(), 'details', true);

        if ($vendor !== 'infusionsoft' || !is_array($details) || !isset($details['raw'])) {
            return;
        }

        $json = json_decode($details['raw']);
        $infusionsoft_invoice_id = false;

        if (isset($json->id)) {
            $infusionsoft_invoice_id = $json->id;
        }

        if ($infusionsoft_invoice_id) {
            update_post_meta($order->getId(), 'infusionsoft_invoice_id', $infusionsoft_invoice_id);
        }
    }

    protected function process(Zippy_Event $event)
    {
        global $zippy_transaction_key;

        $zippy = Zippy::instance();

        $type       = isset($event->get['type']) ? $event->get['type'] : 'order';

        switch ($type) {
            case 'order':
                $transaction = $this->_processOrder($event);
                break;
            default:
                $transaction = null;
                break;
        }

        if ($transaction) {
            // Set our global for use in filters, etc.
            $zippy_transaction_key = $transaction->getKey();
        }
    }

    private function _processOrder(Zippy_event $event)
    {
        $vendor_order_id   = $event->get['orderId'];
        $vendor_order = new ZippyCourses_Infusionsoft_Order($vendor_order_id);
        $vendor_order->fetchAll();

        $adapter    = new ZippyCourses_InfusionsoftOrder_PaymentGatewayTransactionAdapter($vendor_order);
        return $this->_processTransactionData($adapter->getNormalizedData());
    }

    private function _processTransactionData($data)
    {
        $zippy = Zippy::instance();

        // Prepare a fresh transaction object
        $transaction = $zippy->make('transaction');

        // If we've got a transaction key from the adapter, then use it to fetch the
        // existing transaction.
        if ($data['key']) {
            $transaction->buildByTransactionKey($data['key']);
        }
    
        // Import the IPN data into the transaction and save.
        $transaction->importData($data);

        $transaction->save();

        return $transaction;
    }

    private function _processChargeNotification(Zippy_Event $event)
    {
    }

    private function _processInvoiceNotification(Zippy_Event $event)
    {
    }

    private function _processSubscriptionNotification(Zippy_Event $event)
    {
        $event_data = $this->parseRawPostdata($event->raw_post);

        // We are only handling charges with a Transaction Key from Zippy Courses attached to it.
        if (!isset($event_data->data->object->metadata->zippy_transaction_key)) {
            return null;
        }

        $adapter    = new ZippyCourses_InfusionsoftSubscription_PaymentGatewayTransactionAdapter($event_data->data->object);
        return $this->_processTransactionData($adapter->getNormalizedData());
    }


    /**
     * Receive raw post data from Infusionsoft and decode the JSON
     *
     * @since   1.0.0
     *
     * @param   JSON $input php://input stream
     *
     * @return  stdClass
     */
    private function parseRawPostdata($input)
    {
        return json_decode($input);
    }

    /**
     * Validate that the IPN is for Infusionsoft
     *
     * @param  ZippyCourses_TransactionNotification_Event $event
     *
     * @return bool
     */
    protected function validateGateway(Zippy_Event $event)
    {
        $email      = urldecode(filter_input(INPUT_GET, 'inf_field_Email'));
        $fname      = filter_input(INPUT_GET, 'inf_field_FirstName');
        $lname      = filter_input(INPUT_GET, 'inf_field_LastName');
        $order_id   = filter_input(INPUT_GET, 'orderId');

        return $event->gateway == 'infusionsoft' || (!empty($email) && !empty($fname) && !empty($lname) && !empty($lname));
    }

    private function _handleApiRequest(Zippy_Event $event)
    {
        $actions = array('grant_access');

        $parameters = $event->parameters;
        $action = isset($parameters[0]) ? $parameters[0] : null;

        if ($action && in_array($action, $actions)) {
            switch ($action) {
                case 'grant_access':
                    return $this->_handleGrantAccessApiRequest($event);
                    break;
                default:
                    break;
            }
        }

        return $event;
    }

    private function _handleGrantAccessApiRequest($event)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $data = $event->data;

        $remote_contact_id = isset($data['contact_id']) ? $data['contact_id'] : null;
        $remote_product_id = isset($data['product_id']) ? $data['product_id'] : null;
        
        $user_id           = 0;
        $product_id        = 0;

        $errors = array();

        if ($remote_contact_id !== null) {
            $user_id_sql = $wpdb->prepare(
                "SELECT user_id FROM $wpdb->usermeta WHERE meta_key = %s AND meta_value = %s",
                'infusionsoft_contact_id',
                $remote_contact_id
            );

            $user_id = $wpdb->get_var($user_id_sql);

            if ($user_id === null) {
                $errors[] = 'IFS001: Could not find a record with a matching Infusionsoft Contact ID.';
            }
        }

        if ($remote_product_id !== null) {
            $product_id_sql = $wpdb->prepare(
                "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s",
                'infusionsoft_product_id',
                $remote_product_id
            );

            $product_id = $wpdb->get_var($product_id_sql);

            if ($product_id === null) {
                $errors[] = 'IFS002: Could not find a record with a matching Infusionsoft Product ID.';
            }
        }
        
        if ($user_id && $product_id) {
            $zippy->access->grant($user_id, $product_id);
        }

        if (count($errors)) {
            $event->response = new stdClass;
            $event->response->errors = $errors;
        } else {
            $event->response = new stdClass;
            $event->response->success = 'Your request was successful.';
        }

        return $event;
    }
}
