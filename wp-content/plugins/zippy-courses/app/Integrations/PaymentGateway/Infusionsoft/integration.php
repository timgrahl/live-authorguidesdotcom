<?php
/**
 * Id: infusionsoft
 * File Type: payment-gateway-integration
 * Class: ZippyCourses_Infusionsoft_PaymentGatewayIntegration
 *
 * @since 1.0.0
 */

class ZippyCourses_Infusionsoft_PaymentGatewayIntegration extends Zippy_PaymentGatewayIntegration
{
    public $id = 'infusionsoft';
    public $service = 'infusionsoft';
    public $name = 'Infusionsoft';
    public $settings = array();
    
    protected $api;

    private $path;
    private $url;

    public function __construct()
    {
        $this->path = plugin_dir_path(__FILE__);
        $this->url  = plugin_dir_url(__FILE__);
        
        parent::__construct();
        
        $this->api  = new ZippyCourses_Infusionsoft_PaymentGatewayAPI;

        add_filter('zippy_middleware_rules', array($this, 'middleware'), 10, 2);
        add_filter('zippy_infusionsoft_connection_data', array($this, 'connectionInfo'));
    }

    public function setup()
    {
        $this->settings();
        $this->actions();
        $this->filters();
        $this->utilities();
        $this->endpoints();

        add_filter('zippy_metaboxes', array($this, 'metaboxes'));
    }

    protected function actions()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_action('init', array($this, 'orderForm'));
            add_action('template_redirect', array($this, 'buyNow'), 11);
        }

    }

    public function connectionInfo($conn)
    {
        return array('connectionName:' . $this->api->app_id . ':i:' . $this->api->api_key . ':This is the connection for ' . $this->api->app_id . '.infusionsoft.com');
    }
    
    protected function filters()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_filter('zippy_active_gateway_configured', array($this, 'checkConfiguration'));
        }
    }

    public function map($classes)
    {
        $classes['ZippyCourses_Infusionsoft_PaymentGateway']            = $this->path . 'lib/PaymentGateway.php';
        $classes['ZippyCourses_Infusionsoft_PaymentGatewayAPI']         = $this->path . 'lib/API.php';
        $classes['ZippyCourses_Infusionsoft_PaymentGatewayListener']    = $this->path . 'lib/Listener.php';
        $classes['ZippyCourses_InfusionsoftGateway_Utilities']          = $this->path . 'lib/Utilities.php';
        $classes['ZippyCourses_Infusionsoft_OrderForm']                 = $this->path . 'lib/OrderForm.php';
        $classes['ZippyCourses_Infusionsoft_OrderFormProcessor']        = $this->path . 'lib/OrderFormProcessor.php';
        $classes['ZippyCourses_Infusionsoft_Order']                     = $this->path . 'lib/Order.php';
        $classes['ZippyCourses_InfusionsoftOrder_PaymentGatewayTransactionAdapter']    = $this->path . 'lib/OrderAdapter.php';
        $classes['Zippy_OrderHasInfusionsoftAccess_MiddlewareRule']    = $this->path . 'lib/Middleware/Rules/OrderHasInfusionsoftAccess.php';
       
        return $classes;
    }

    public function utilities()
    {
        $zippy = Zippy::instance();

        if (!isset($zippy->utilities->infusionsoft)) {
            $zippy->utilities->infusionsoft = new stdClass;
        }

        $zippy->utilities->infusionsoft->gateway = new ZippyCourses_InfusionsoftGateway_Utilities;
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $integrations = $zippy->make('payment_gateway_integrations');
        $integrations->add($this);

        $gateway    = new ZippyCourses_Infusionsoft_PaymentGateway;
        $gateways   = $zippy->make('payment');
        
        $gateways->add($gateway);

        $this->registerForm();
    }

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    public function settings()
    {
        $zippy = Zippy::instance();

        $settings_pages = $zippy->make('settings_pages_repository');
        $page = $settings_pages->fetch('zippy_settings_payment');

        $section = $page->createSection($this->getSettingsName(), 'Infusionsoft');
            $section->createField('app_id', __('App ID', ZippyCourses::TEXTDOMAIN));
            $section->createField('api_key', __('API Key', ZippyCourses::TEXTDOMAIN));
    }

    public function orderForm()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            $zippy = Zippy::instance();

            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_Infusionsoft_OrderForm');
        }
    }

    public static function path()
    {
        return plugin_dir_path(__FILE__);
    }

    public static function url()
    {
        return plugin_dir_url(__FILE__);
    }

    public function assets()
    {
    }

    /**
     * Integrate with the correct metaboxes
     * @return void
     */
    public function metaboxes($metaboxes)
    {
        $payment_settings = get_option('zippy_payment_general_settings', array());
        $gateway          = isset($payment_settings['method']) ? $payment_settings['method'] : null;

        if ($gateway != $this->service) {
            return $metaboxes;
        }

        $dir = $this->path . 'assets/views/metaboxes/';
        $files = array_diff(scandir($dir), array('..', '.'));

        foreach ($files as $key => &$file) {
            if (!is_dir($dir . $file)) {
                $file = $dir . $file;
            } else {
                unset($files[$key]);
            }
        }

        return array_merge($metaboxes, $files);
    }

    public function getId()
    {
        return $this->id;
    }

    public function checkConfiguration($configured)
    {
        $settings = get_option('zippy_infusionsoft_payment_gateway_settings');
        $app_id    = isset($settings['app_id']) ? $settings['app_id'] : '';
        $api_key    = isset($settings['api_key']) ? $settings['api_key'] : '';

        $configured = !empty($app_id) && !empty($api_key);

        return $configured;
    }

    public function endpoints()
    {
        $zippy = Zippy::instance();

        $zippy->api->addEndpoint('infusionsoft');
    }

    public function middleware($rules, $id)
    {
        $zippy = Zippy::instance();

        if ($id == 'order-grants-access') {
            $rules->add(new Zippy_OrderHasInfusionsoftAccess_MiddlewareRule);
        }

        return $rules;
    }

    public function registerForm()
    {
    }

    public function buyNow()
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || ($post->post_type !== 'product' && !$zippy->core_pages->is($post->ID, 'buy')) || !isset($_GET['buy-now'])) {
            return;
        }

        $product = $post->post_type == 'product'
            ? $zippy->make('product', array('id' => $post->ID))
            : $zippy->utilities->product->getProductByBuyQueryVar();

        if ($product) {
            $is_active = get_post_meta($product->id, 'status', true) == 'active';
            if (!$zippy->utilities->product->inLaunchWindow($product) || !$is_active) {
                return;
            }
            $order_form_url = get_post_meta($product->getId(), 'infusionsoft_order_form_url', true);
            if (!empty($order_form_url)) {
                wp_redirect($order_form_url);
                exit;
            } else {
                $zippy->log('Product #' . $product_id . ' does not have an Infusionsoft Order Form URL.', 'INFUSIONSOFT_ERROR');

                $error_message  = sprintf(__('We could not find an Order Form for this product. If this problem persists, please <a href="mailto:%s">contact us</a>.', ZippyCourses::TEXTDOMAIN), $zippy->utilities->email->getSystemEmailAddress());

                $zippy->sessions->flashMessage($error_message, 'error', 0);
            }
        }
    }
}
