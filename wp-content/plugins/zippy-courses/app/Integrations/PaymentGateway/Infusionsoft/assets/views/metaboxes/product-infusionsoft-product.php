<?php
/*
Id: zippy-infusionsoft-product
Name: Infusionsoft Product
Fields: infusionsoft_product_id, infusionsoft_order_form_url
Post Types: product
Context: normal
Priority: default
Version: 1.0.0
*/
global $post;

$list = $zippy->utilities->infusionsoft->gateway->getProductsList();
?>

<p>
<label for="infusionsoft_product_id"><?php _e('Infusionsoft Product:', ZippyCourses::TEXTDOMAIN); ?></label>
<select name="infusionsoft_product_id" id="infusionsoft_product_id">
    <option value=""></option>
    <?php foreach ($list as $item):
        $disabled = $item['used'] && $item['id'] != $infusionsoft_product_id ? ' disabled="disabled" ' : '';
        $selected = selected($infusionsoft_product_id, $item['id'], false);
        ?>
        <option value="<?php echo $item['id']; ?>" <?php echo $selected; ?> <?php echo $disabled; ?>><?php echo $item['name']; ?></option>
    <?php endforeach; ?>
</select>
</p>

<p>
<label for="infusionsoft_order_form_url"><?php _e('Order Form URL:', ZippyCourses::TEXTDOMAIN); ?></label>
<input name="infusionsoft_order_form_url" value="<?php echo $infusionsoft_order_form_url; ?>" />
</p>
