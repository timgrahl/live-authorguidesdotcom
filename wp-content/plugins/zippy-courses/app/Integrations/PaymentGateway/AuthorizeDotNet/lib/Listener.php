<?php

class ZippyCourses_AuthorizeDotNet_PaymentGatewayListener extends Zippy_PaymentGatewayListener
{
    public function register()
    {
        parent::register();

        $zippy = Zippy::instance();

        $zippy->events->register('OrderStatusChange', $this);
        $zippy->events->register('SaveTransaction', $this);
        $zippy->events->register('OrderSave', $this);
    }

    public function handle(Zippy_Event $event)
    {
        switch ($event->getEventName()) {
            case 'TransactionNotification':
                if ($this->validateGateway($event)) {
                    $this->process($event);
                }
                break;
            case 'OrderStatusChange':
                $this->_handleOrderCompletion($event);
                break;
            case 'OrderSave':
                break;
            default:
                break;
        }
    }

    private function _handleOrderCompletion(Zippy_Event $event)
    {
        $order          = $event->order;
        $status         = $event->new_status;

        if ($order->getType() == 'payment-plan' && $status == 'complete') {
            if (!$order->transactions->count()) {
                $order->fetchTransactions();
            }

            $transaction        = $order->transactions->first();
            $customer_id        = get_post_meta($transaction->getId(), 'authorizedotnet_customer_id', true);
            $subscription_id    = get_post_meta($transaction->getId(), 'recurring_id', true);

            if (!empty($customer_id) && !empty($subscription_id)) {
                $api = new ZippyCourses_AuthorizeDotNet_PaymentGatewayAPI;
                $cancel = $api->cancelSubscription($customer_id, $subscription_id);
            }
        }
    }

    protected function process(Zippy_Event $event)
    {
        global $zippy_transaction_key;

        $zippy = Zippy::instance();

        $type = isset($event->type) ? $event->type : 'notification';

        switch ($type) {
            case 'payment':
                $transaction = $this->_processPayment($event);
                break;
            default:
                $transaction = null;
                break;
        }
        
        if ($transaction) {
            // Set our global for use in filters, etc.
            $zippy_transaction_key = $transaction->getKey();
            $zippy->sessions->store('zippy_transaction_key', $zippy_transaction_key);
        } else {
            $zippy->sessions->delete('zippy_transaction_key');
        }
    }

    private function _processPayment(Zippy_Event $event)
    {
        global $wpdb;

        $adapter    = new ZippyCourses_AuthorizeDotNet_PaymentGatewayTransactionAdapter(
            json_decode(
                $event->raw_post
            )
        );

        return $this->_processTransactionData($adapter->getNormalizedData());
    }

    private function _processTransactionData($data)
    {
        $zippy = Zippy::instance();

        // Prepare a fresh transaction object
        $transaction = $zippy->make('transaction');

        // If we've got a transaction key from the adapter, then use it to fetch the
        // existing transaction.
        if ($data['key']) {
            $transaction->buildByTransactionKey($data['key']);
        }
    
        // Import the IPN data into the transaction and save.
        $transaction->importData($data);
        $transaction->save();

        return $transaction;
    }

    /**
     * Validate that the IPN is for AuthorizeDotNet
     *
     * @param  ZippyCourses_TransactionNotification_Event $event
     *
     * @return bool
     */
    protected function validateGateway(Zippy_Event $event)
    {
        return $event->gateway == 'authorizedotnet';
    }
}
