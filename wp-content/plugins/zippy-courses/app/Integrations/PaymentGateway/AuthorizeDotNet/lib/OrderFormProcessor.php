<?php

class ZippyCourses_AuthorizeDotNet_OrderFormProcessor extends Zippy_FormProcessor
{
    /**
     * Transaction
     * @var Zippy_Transaction
     */
    protected $transaction;

    public function fail()
    {
        $zippy = Zippy::instance();
        $zippy->session->flashMessage('There was an error processing your request', 'error');
    }

    public function execute()
    {
        $zippy = Zippy::instance();

        $product = $zippy->make('product', array('id' => $_POST['product_id']));

        $customer = $zippy->make('customer');
        $customer->importData(array(
            'first_name' => filter_input(INPUT_POST, 'first_name'),
            'last_name' => filter_input(INPUT_POST, 'last_name'),
            'email'     => filter_input(INPUT_POST, 'email'),
        ));

        $card = new ZippyCourses_AuthorizeDotNet_Card(
            $customer,
            filter_input(INPUT_POST, 'cc'),
            filter_input(INPUT_POST, 'cvc'),
            array(
                'month' => filter_input(INPUT_POST, 'exp_month'),
                'year'  => filter_input(INPUT_POST, 'exp_year')
            )
        );
        
        $transaction = $zippy->make('transaction');

        $transaction->setProduct($product);
        $transaction->setType($product->getType());
        $transaction->setStatus('pending');
        $transaction->setGateway('authorizedotnet');
        $transaction->setCustomer($customer);
        $transaction->save();

        $this->chargeTransaction($transaction, $customer, $card);
    }

    private function chargeTransaction(
        Zippy_Transaction $transaction,
        Zippy_Customer $customer,
        ZippyCourses_AuthorizeDotNet_Card $card
    ) {
        switch ($transaction->getType()) {
            case 'single':
                $payment = $this->chargeSinglePayment($transaction, $customer, $card);
                break;
            case 'subscription':
            case 'payment-plan':
                $payment = $this->chargeRecurring($transaction, $customer, $card);
                break;
            default:
                break;
        }
    }

    private function chargeSinglePayment(
        Zippy_Transaction $transaction,
        Zippy_Customer $customer,
        ZippyCourses_AuthorizeDotNet_Card $card
    ) {
        $zippy = Zippy::instance();

        $api = new ZippyCourses_AuthorizeDotNet_PaymentGatewayAPI;

        $product = $transaction->getProduct();

        $payment  = $api->makeSinglePayment($product, $customer, $card);
        $payment->invoice_num = $transaction->getKey();

        $response = $payment->authorizeAndCapture();

        if ($response->response_code == '1' && $response->response_reason_code == '1') {
            $vendor_transaction = $api->getTransaction($response->transaction_id);

            $event = new ZippyCourses_TransactionNotification_Event('authorizedotnet');

            $event->type        = 'payment';
            $event->raw_post    = json_encode($vendor_transaction);

            $zippy->events->fire($event);

            wp_redirect($zippy->core_pages->getUrl('thank_you') . '?transaction_key=' . $transaction->getKey());
            exit;
        } else {
            $error_message  = sprintf(__('Your payment could not be completed at this time. You have not been charged. If this problem persists, please <a href="mailto:%s">contact us</a>.', ZippyCourses::TEXTDOMAIN), $zippy->utilities->email->getSystemEmailAddress());

            $error = isset($response->response_reason_text) ? $response->response_reason_text : '';

            $zippy->log('Could not process Transaction #' . $transaction->getId() . '. Reason: ' . $error, 'AUTHORIZEDOTNET_ERROR');
            $zippy->sessions->flashMessage($error_message, 'error', 0);

            $transaction->setStatus('fail');
            $transaction->save();
        }
    }

    private function chargeRecurring(
        Zippy_Transaction $transaction,
        Zippy_Customer $customer,
        ZippyCourses_AuthorizeDotNet_Card $card
    ) {
        $zippy = Zippy::instance();

        $api = new ZippyCourses_AuthorizeDotNet_PaymentGatewayAPI;

        $product        = $transaction->getProduct();

        $subscription   = $api->makeSubscription($product, $customer, $card);
        $request        = $api->makeRecurringBillingRequest();
        
        $response       = $api->xmlToObject($request->createSubscription($subscription));

        $error_message  = sprintf(__('Your payment could not be completed at this time. You have not been charged. If this problem persists, please <a href="mailto:%s">contact us</a>.', ZippyCourses::TEXTDOMAIN), $zippy->utilities->email->getSystemEmailAddress());

        if ($response->xml->messages->resultCode == 'Ok') {
            $recurring_id = $response->xml->subscriptionId;
            
            $payment  = $api->makeSinglePayment($product, $customer, $card);
            $payment->invoice_num = $transaction->getKey();

            $response2 = $payment->authorizeAndCapture();


            if ($response2->response_code == '1' && $response2->response_reason_code == '1') {
                $vendor_transaction = $api->getTransaction($response2->transaction_id);
                $vendor_transaction->subscription = new stdClass;
                $vendor_transaction->subscription->id = $recurring_id;
                $vendor_transaction->subscription->payNum = 0;

                $event = new ZippyCourses_TransactionNotification_Event('authorizedotnet');
                $event->type = 'payment';
                $event->raw_post = json_encode($vendor_transaction);

                $zippy->events->fire($event);

                wp_redirect($zippy->core_pages->getUrl('thank_you') . '?transaction_key=' . $transaction->getKey());
                exit;
            } else {
                $api->cancelRecurringBilling($recurring_id);
                
                $error = isset($response2->response_reason_text) ? $response2->response_reason_text : '';

                if (empty($error) && isset($response2->xml->messages->message->text)) {
                    $error = $response2->xml->messages->message->text;
                }
            
                $zippy->log('Could not process Transaction #' . $transaction->getId() . '. Reason: ' . $error, 'AUTHORIZEDOTNET_ERROR');
                $zippy->sessions->flashMessage($error_message, 'error', 0);

                $transaction->setStatus('fail');
                $transaction->save();
            }
        } else {
            // Trigger some errors
            $error = isset($response->response_reason_text) ? $response->response_reason_text : '';
            
            if (empty($error) && isset($response->xml->messages->message->text)) {
                $error = $response->xml->messages->message->text;
            }

            $zippy->log('Could not process Transaction #' . $transaction->getId() . '. Reason: ' . $error, 'AUTHORIZEDOTNET_ERROR');
            $zippy->sessions->flashMessage($error_message, 'error', 0);

            $transaction->setStatus('fail');
            $transaction->save();
        }
    }

    private function redirect($url)
    {
        wp_redirect($url);
        exit;
    }
}
