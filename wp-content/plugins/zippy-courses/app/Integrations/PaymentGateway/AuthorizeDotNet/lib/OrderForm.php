<?php

class ZippyCourses_AuthorizeDotNet_OrderForm extends Zippy_OrderForm
{
    public function defaultFields()
    {
        parent::defaultFields();

        $this->addTextField('first_name', __('First Name', ZippyCourses::TEXTDOMAIN));
        
        $this->addTextField('last_name', __('Last Name', ZippyCourses::TEXTDOMAIN));

        $this->addTextField('email', __('Email', ZippyCourses::TEXTDOMAIN));

        $this->addTextField('cc', __('Card Number', ZippyCourses::TEXTDOMAIN));

        $this->addTextField('cvc', __('CVC', ZippyCourses::TEXTDOMAIN))
            ->addContainerClass('zippy-input-small zippy-input-left zippy-input-clear');

        $this->addTextField('exp_month', __('Expiration Month', ZippyCourses::TEXTDOMAIN))
            ->setPlaceholder('MM')
            ->addContainerClass('zippy-input-small zippy-input-left zippy-input-clear');

        $this->addTextField('exp_year', __('Expiration Year', ZippyCourses::TEXTDOMAIN))
            ->setPlaceholder('YYYY')
            ->addContainerClass('zippy-input-small zippy-input-left zippy-input-clear');

        $this->addHiddenField('method')->setValue('authorizedotnet');
    }
    
    public function afterSetup()
    {
        $zippy = Zippy::instance();

        $settings = get_option('zippy_stripe_payment_gateway_settings');
        $paypal   = isset($settings['paypal']) ? (bool) $settings['paypal'] : false;

        if ($paypal) {
            $product = $this->getProduct();

            $url = get_permalink($product->getId()) . '?buy-with=paypal';

            $msg = '<a href="' . $url . '" class="zippy-button">' . __('Pay with PayPal', ZippyCourses::TEXTDOMAIN) . '</a>';

            $this->setAfter($msg);
        }
    }
}
