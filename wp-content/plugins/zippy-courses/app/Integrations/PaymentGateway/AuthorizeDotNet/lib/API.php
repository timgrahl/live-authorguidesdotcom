<?php

/**
 * AuthorizeDotNet Gateway for Zippy Courses.  Using this API is useful because it does not require CURL as the official
 * library does, and it also uses wp_remote_post to make remote requests.
 *
 * The primary downside is that not everything is a particular type of AuthorizeDotNet Object as the official SDK is,
 * but the functionality is leaner.
 */

class ZippyCourses_AuthorizeDotNet_PaymentGatewayAPI extends Zippy_PaymentGatewayAPI
{
    public $api_login;
    public $transaction_key;
    private $mode;

    private $base_url = '';

    public function __construct()
    {
        $options                = get_option('zippy_authorizedotnet_payment_gateway_settings');
        $this->api_login        = isset($options['api_login']) ? $options['api_login'] : '';
        $this->transaction_key  = isset($options['transaction_key']) ? $options['transaction_key'] : '';
        $this->mode             = isset($options['mode']) ? $options['mode'] : 'live';

        $this->sdk();
    }

    public function makeSinglePayment(Zippy_Product $product, Zippy_Customer $customer, ZippyCourses_AuthorizeDotNet_Card $card)
    {
        $payment = new AuthorizeNetAIM($this->api_login, $this->transaction_key);

        $payment->amount   = $product->getAmount();
        $payment->card_num = $card->number;
        $payment->exp_date = $card->getExpDateString();
        $payment->first_name          = $customer->getFirstName();
        $payment->last_name           = $customer->getLastName();
        $payment->setSandbox($this->isSandbox());

        return $payment;
    }

    public function makeRecurringBillingRequest()
    {
        $payment = new AuthorizeNetARB($this->api_login, $this->transaction_key);
        $payment->setSandbox($this->isSandbox());
        return $payment;
    }

    public function makeSubscription(Zippy_Product $product, Zippy_Customer $customer, ZippyCourses_AuthorizeDotNet_Card $card)
    {
        $zippy = Zippy::instance();
        
        $subscription = new AuthorizeNet_Subscription;

        $num_payments = $product->getNumPayments() === 0 ? '9999' : $product->getNumPayments();

        $frequency = $product->getFrequency();

        switch ($frequency) {
            case 'year':
                $interval_unit   = 'months';
                $interval_length = '12';
                break;

            case 'day':
            case 'month':
                $interval_unit = 'months';
                $interval_length = '1';
                break;

            case 'week':
                $interval_unit = 'days';
                $interval_length = '7';
                break;
            
            default:
                break;
        }

        $start_date = $zippy->utilities->datetime->getNow();
        $start_date->modify('+1 ' . $frequency);

        $subscription->name                     = $product->getTitle();
        $subscription->intervalLength           = $interval_length;
        $subscription->intervalUnit             = $interval_unit;
        $subscription->startDate                = $start_date->format('Y-m-d');
        $subscription->totalOccurrences         = $num_payments;
        $subscription->amount                   = $product->getAmount();
        $subscription->creditCardCardNumber     = $card->number;
        $subscription->creditCardExpirationDate = $card->getExpDateString();
        $subscription->creditCardCardCode       = $card->cvc;
        $subscription->billToFirstName          = $customer->getFirstName();
        $subscription->billToLastName           = $customer->getLastName();

        return $subscription;
    }

    public function getTransaction($transaction_id)
    {
        $details = new AuthorizeNetTD($this->api_login, $this->transaction_key);
        $details->setSandbox($this->isSandbox());

        $transaction = $this->xmlToObject($details->getTransactionDetails($transaction_id));

        $transaction = $transaction->xml->messages->resultCode == 'Ok'
            ? $transaction->xml->transaction
            : null;

        return $transaction;

    }

    public function cancelRecurringBilling($recurring_id)
    {
        $request = $this->makeRecurringBillingRequest();
        $request->cancelSubscription($recurring_id);
    }

    public function getRecurringStatus($recurring_id)
    {
        $request    = $this->makeRecurringBillingRequest();
        $response   = $this->xmlToObject($request->getSubscriptionStatus($recurring_id));

        return $response->xml->messages->resultCode == 'Ok' ? $response->xml->status : false;
    }

    public function isSandbox()
    {
        return $this->mode !== 'live';
    }

    public function sdk()
    {
        if (!class_exists('AuthorizeNetRequest') &&
            !class_exists('AuthorizeNetAIM') &&
            !class_exists('AuthorizeNetARB')
        ) {
            require_once(dirname(__FILE__) . '/../vendor/AuthorizeDotNet-sdk-php/autoload.php');
        }
    }

    public function xmlToObject($input)
    {
        $xml = is_object($input) ? $input : simplexml_load_string($input);
        return json_decode(json_encode($xml));
    }

    public function getSubscriptionList()
    {
        $list = new AuthorizeNetGetSubscriptionList;
        $list->searchType = 'subscriptionActive';

        $request    = $this->makeRecurringBillingRequest();
        $response   = $request->getSubscriptionList($list);

        return $response;
    }
}
