<?php

class ZippyCourses_AuthorizeDotNet_Card
{
    public $first_name;
    public $last_name;
    public $number;
    public $cvc;

    /**
     * Expects array('month' => '00', 'year' => '0000') format
     * @var array
     */
    public $expiration;

    public function __construct(Zippy_Customer $customer, $number, $cvc, array $exp)
    {
        $this->first_name   = $customer->getFirstName();
        $this->last_name    = $customer->getLastName();
        $this->number       = $number;
        $this->cvc          = $cvc;
        $this->exp          = $exp;
    }

    public function getExpDateString()
    {
        $month = $this->exp['month'];
        $year  = $this->exp['year'];

        return "{$month}/{$year}";
    }
}
