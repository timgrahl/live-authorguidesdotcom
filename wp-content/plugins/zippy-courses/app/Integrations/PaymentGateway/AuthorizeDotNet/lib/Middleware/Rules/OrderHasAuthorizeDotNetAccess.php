<?php

class Zippy_OrderHasAuthorizeDotNetAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You do not own access to this content.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'order-has-authorizedotnet-access';
    }

    public function handle($object)
    {
        if (!($object instanceof Zippy_Order) || is_admin()) {
            return $this->exitEarly();
        }

        if ($object->getGateway() !== 'authorizedotnet') {
            return $this->pass($object);
        }

        $order_id = get_post_meta($object->getId(), 'authorizedotnet_order_id', true);

        if (empty($order_id) || $order_id === '') {
            return $this->exitEarly();
        }

        $zippy = Zippy::instance();

        $access = $this->_handleOrderAccess($object);

        if ($this->_transientExpired($object)) {
            $now = $zippy->utilities->datetime->getNow();
            $now->modify('+1 hour');

            $status = array(
                'active' => ($access ? 'yes' : 'no'),
                'expire' => $now->format('U')
            );

            update_post_meta($object->getId(), 'authorizedotnet_order_status', $status);
        }

        return $access ? $this->pass($object) : $this->fail($object);
    }

    private function _transientExpired($object)
    {
        $zippy = Zippy::instance();
        
        $status = get_post_meta($object->getId(), 'authorizedotnet_order_status', true);
        $now    = $zippy->utilities->datetime->getNow();

        return empty($status) || (isset($status['expire']) && $now->format('U') > $status['expire']);
    }

    private function _checkTransientStatus($object)
    {
        $zippy = Zippy::instance();
        
        $status = get_post_meta($object->getId(), 'authorizedotnet_order_status', true);
        $now    = $zippy->utilities->datetime->getNow();
        
        if (!empty($status) &&
            isset($status['expire']) &&
            $now->format('U') < $status['expire'] &&
            isset($status['active']) && $status['active'] == 'yes'
        ) {
            return true;
        }

        return false;
    }

    private function _handleOrderAccess($object)
    {
        if (($status = $this->_checkTransientStatus($object)) === true || $object->getType() == 'single') {
            return true;
        }

        $zippy = Zippy::instance();

        $access = true;

        $valid_statuses = array('active', 'expired');

        $recurring_id = $zippy->utilities->orders->getVendorRecurringId($object->getId());

        $api = new ZippyCourses_AuthorizeDotNet_PaymentGatewayAPI;

        if (!empty($recurring_id)) {
            $status = $api->getRecurringStatus($recurring_id);
            $access = in_array($status, $valid_statuses);
        } else {
            $access = false;
        }

        return $access;
    }
}
