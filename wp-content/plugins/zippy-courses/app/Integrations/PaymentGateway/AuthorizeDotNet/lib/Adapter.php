<?php

class ZippyCourses_AuthorizeDotNet_PaymentGatewayTransactionAdapter extends Zippy_PaymentGatewayTransactionAdapter
{
    public $gateway = 'authorizedotnet';
    
    public function normalize($data)
    {
        if ($this->normalized === null) {
            $zippy = Zippy::instance();

            $vendor_id          = $data->transId;
            
            $transaction        = $zippy->make('transaction');
            $transaction_key    = $this->normalizeKey($vendor_id, $data);

            $transaction->buildByTransactionKey($transaction_key);

            $id                 = $this->normalizeId($transaction_key, $vendor_id);

            $product            = $transaction->getProduct()->getId();
            $order              = $this->normalizeOrder($id, $vendor_id, $data);
           
            $this->normalized = array(
                'id'                    => $id,
                'key'                   => $transaction_key,
                'title'                 => ($id ? get_the_title($id) : ''),
                'vendor_id'             => $vendor_id,
                'total'                 => $this->normalizeTotal($data),
                'fee'                   => $this->normalizeFee($data),
                'tax'                   => $this->normalizeTax($data),
                'currency'              => $this->normalizeCurrency($data),
                'customer'              => $transaction->getCustomer(),
                'type'                  => $this->normalizeType($transaction),
                'timestamp'             => $this->normalizeTimestamp($data),
                'status'                => $this->normalizeStatus($data),
                'gateway'               => $this->gateway,
                'mode'                  => $this->normalizeMode($data),
                'recurring'             => $this->normalizeRecurring($data),
                'recurring_id'          => $this->normalizeRecurringId($data),
                'order'                 => $order,
                'owner'                 => $transaction->getOwner(),
                'method'                => $this->normalizeMethod($data),
                'product'               => $product,
                'raw'                   => json_encode($data)
            );
        }

        return $this->normalized;
    }

    protected function normalizeId($transaction_key, $vendor_id)
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'transaction_key', $transaction_key);
        $transaction_id = (int) $wpdb->get_var($sql);

        // If we've never seen this transaction key, it's a new transaction, so return right away.
        if ($transaction_id === 0) {
            return $transaction_id;
        }

        // If $vendor_id has not been passed in as 0 (like in the event of subscr_signup, etc),
        // then we can use it to see if this vendor_id has been used before.  Otherwise, it should be 0.
        if ($vendor_id) {
            $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'vendor_id', $vendor_id);
            $vendor_id_exists = $vendor_id ? $wpdb->get_var($sql) : null;

            if ($vendor_id_exists) {
                return $vendor_id_exists;
            }
        }

        // If an item has this transaction key and an empty vendor ID, use that record.  If an item has this transaction key, but has a vendor ID, create a new one...
        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE post_id = %s AND meta_key = %s", $transaction_id, 'vendor_id');
        $has_vendor_id = $wpdb->get_var($sql);

        if (empty($has_vendor_id)) {
            return $transaction_id;
        }

        return 0;
    }

    /**
     * Get the transaction key of this notification. If the Post ID given is not empty,
     * it means that we're getting a payment in a recurring cycle that has not been recorded yet.
     *
     * @return string|null
     */
    protected function normalizeKey($vendor_id, $input)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $key = isset($input->order->invoiceNumber) ? $input->order->invoiceNumber : null;

        // If there is an existing transaction key, then we need to double check if a transaction already has it,
        // and if so, double check that the vendor ids match.  If they don't, then return null.
        if ($key !== null) {
            $sql = $wpdb->prepare(
                "SELECT
                    post_id
                 FROM
                    $wpdb->postmeta
                 WHERE
                    meta_key = %s AND
                    meta_value = %s",
                'transaction_key',
                $key
            );

            $post_id = $wpdb->get_var($sql);

            if ($post_id) {
                $recurring = get_post_meta($post_id, 'recurring', true);
                $is_recurring = empty($recurring);
                $order_id = get_post_meta($post_id, 'order_id', true);
                
                if ($is_recurring && !empty($order_id)) {
                    $key = null;
                }
            }
        }

        return $key;
    }

    protected function normalizeType($transaction)
    {
        if ($transaction instanceof Zippy_Transaction) {
            return $transaction->getType();
        }

        return 'single';
    }

    protected function normalizeTotal($input)
    {
        return $input->authAmount;
    }

    protected function normalizeFee($input)
    {
        return 0;
    }

    protected function normalizeCurrency($input)
    {
        $zippy = Zippy::instance();
        return $zippy->utilities->cart->getCurrency();
    }

    protected function normalizeTax($input)
    {
        return 0;
    }

    protected function normalizeMode($input)
    {
        return 'live';
    }

    protected function normalizeMethod($input)
    {
        return 'credit_card';
    }

    protected function normalizeRecurringId($input)
    {
        return isset($input->subscription) ? $input->subscription->id : 0;
    }

    protected function normalizeRecurring($input)
    {
        return isset($input->subscription);
    }

    /**
     * Map status to valid transaction status: complete|pending|refunded|failed|revoked|cancelled
     * @param  string $input The status from gateway
     * @return string
     */
    protected function normalizeStatus($input)
    {
        return $input->responseCode == '1' ? 'complete' : 'pending';
    }

    protected function normalizeTimestamp($input)
    {
        $zippy = Zippy::instance();
        return $zippy->utilities->datetime->getDate($input->submitTimeUTC);
    }

    protected function normalizeOrder($id, $vendor_id, $data)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        if ($id) {
            $order_id = get_post_meta($id, 'order_id', true);
        } else {
            if (isset($data->subscription)) {
                $order_id = $this->_getSubscriptionTransactions($data->subscription->id);
            }
        }

        if (!empty($order_id)) {
            $order = $zippy->make('order');
            $order->build($order_id);

            return $order;
        }

        return;
    }

    public function _getSubscriptionTransactions($recurring_id)
    {
        global $wpdb;

        $order_id = 0;

        $sql = $wpdb->prepare(
            "SELECT
                post_id
             FROM
                $wpdb->postmeta
             WHERE
                meta_key = %s AND
                meta_value = %s",
            'recurring_id',
            $recurring_id
        );

        $transaction_id = $wpdb->get_var($sql);

        if ($transaction_id) {
            $order_id = get_post_meta($transaction_id, 'order_id', true);
        }

        return $order_id;
    }
}