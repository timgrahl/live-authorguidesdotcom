<?php

class ZippyCourses_AuthorizeDotNet_PaymentGateway extends Zippy_PaymentGateway
{
    public $id      = 'authorizedotnet';
    public $name    = 'AuthorizeDotNet';

    public function __construct()
    {
        parent::__construct();
    }

    public function setup()
    {
    }

    public function register()
    {
    }

    public function registerListener()
    {
        $this->listener = new ZippyCourses_AuthorizeDotNet_PaymentGatewayListener($this->id);
        $this->listener->register();
    }

    public function getId()
    {
        return $this->id;
    }
}
