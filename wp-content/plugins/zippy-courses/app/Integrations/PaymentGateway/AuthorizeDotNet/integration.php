<?php
/**
 * Id: authorizedotnet
 * File Type: payment-gateway-integration
 * Class: ZippyCourses_AuthorizeDotNet_PaymentGatewayIntegration
 *
 * @since 1.0.0
 */

class ZippyCourses_AuthorizeDotNet_PaymentGatewayIntegration extends Zippy_PaymentGatewayIntegration
{
    public $id = 'authorizedotnet';
    public $service = 'authorizedotnet';
    public $name = 'Authorize.Net';
    public $settings = array();
    
    protected $api;

    private $path;
    private $url;

    public function __construct()
    {
        $this->path = plugin_dir_path(__FILE__);
        $this->url  = plugin_dir_url(__FILE__);

        add_filter('zippy_middleware_rules', array($this, 'middleware'), 10, 2);

        parent::__construct();
    }

    public function setup()
    {
        $this->settings();
        $this->actions();
        $this->filters();
    }

    protected function actions()
    {
        add_action('init', array($this, 'orderForm'));
        add_action('wp_enqueue_scripts', array($this, 'assets'));
    }

    protected function filters()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_filter('zippy_active_gateway_configured', array($this, 'checkConfiguration'));
            add_filter('zippy_filter_get_transaction_key', array($this, 'thankYou'));
            add_filter('zippy_courses_payment_method_requires_register_first_redirect', '__return_true');
        }
    }

    public function map($classes)
    {
        $classes['ZippyCourses_AuthorizeDotNet_OrderForm']                           = $this->path . 'lib/OrderForm.php';
        $classes['ZippyCourses_AuthorizeDotNet_OrderFormProcessor']                  = $this->path . 'lib/OrderFormProcessor.php';
        $classes['ZippyCourses_AuthorizeDotNet_PaymentGatewayListener']              = $this->path . 'lib/Listener.php';
        $classes['ZippyCourses_AuthorizeDotNet_PaymentGatewayTransactionAdapter']    = $this->path . 'lib/Adapter.php';
        $classes['ZippyCourses_AuthorizeDotNet_PaymentGatewayAPI']                   = $this->path . 'lib/API.php';
        $classes['ZippyCourses_AuthorizeDotNet_PaymentGateway']                      = $this->path . 'lib/PaymentGateway.php';
        $classes['ZippyCourses_AuthorizeDotNet_Card']                                = $this->path . 'lib/Card.php';
        $classes['Zippy_OrderHasAuthorizeDotNetAccess_MiddlewareRule']               = $this->path . 'lib/Middleware/Rules/OrderHasAuthorizeDotNetAccess.php';

        return $classes;
    }

    public function registerForm()
    {
        $zippy = Zippy::instance();

        $payment_settings = get_option('zippy_payment_general_settings', array());
        $gateway          = isset($payment_settings['method']) ? $payment_settings['method'] : null;

        if ($this->id == $gateway) {
            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_AuthorizeDotNet_OrderForm');
        }
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $integrations = $zippy->make('payment_gateway_integrations');
        $integrations->add($this);

        $gateway    = new ZippyCourses_AuthorizeDotNet_PaymentGateway;
        $gateways   = $zippy->make('payment');
        
        $gateways->add($gateway);

        $this->registerForm();
    }

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    public function settings()
    {
        $zippy = Zippy::instance();

        $settings_pages = $zippy->make('settings_pages_repository');
        $page = $settings_pages->fetch('zippy_settings_payment');

        $section = $page->createSection($this->getSettingsName(), 'Authorize.Net');
            $section->createField('api_login', __('API Login ID', ZippyCourses::TEXTDOMAIN));
            $section->createField('transaction_key', __('Transaction Key', ZippyCourses::TEXTDOMAIN));
            $section->createField('mode', __('Mode', ZippyCourses::TEXTDOMAIN), 'select', array(
                'live' => __('Live', ZippyCourses::TEXTDOMAIN),
                'test' => __('Test', ZippyCourses::TEXTDOMAIN)
            ));

    }

    public function orderForm()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            $zippy = Zippy::instance();

            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_AuthorizeDotNet_OrderForm');
        }
    }

    public function thankYou($key)
    {
        global $zippy_transaction_key;

        $transaction_key = filter_input(INPUT_GET, 'transaction_key');
        if ($transaction_key) {
            $zippy_transaction_key = $transaction_key;
        }

        return $zippy_transaction_key;
    }

    public static function path()
    {
        return plugin_dir_path(__FILE__);
    }

    public static function url()
    {
        return plugin_dir_url(__FILE__);
    }

    public function assets()
    {
        $payment_settings = get_option('zippy_payment_general_settings', array());
        $gateway          = isset($payment_settings['method']) ? $payment_settings['method'] : null;

        if ($this->id == $gateway) {
        }
    }

    public function middleware($rules, $id)
    {
        if ($id !== 'order-grants-access') {
            return $rules;
        }

        $rules->add(new Zippy_OrderHasAuthorizeDotNetAccess_MiddlewareRule);

        return $rules;
    }

    public function getId()
    {
        return $this->id;
    }

    public function checkConfiguration($configured)
    {
        $settings = get_option('zippy_authorizedotnet_payment_gateway_settings');

        $api_login = isset($settings['api_login']) ? $settings['api_login'] : '';
        $transaction_key = isset($settings['transaction_key']) ? $settings['transaction_key'] : '';
        $mode = isset($settings['mode']) ? $settings['mode'] : '';
        $secret_key = isset($settings['secret_key']) ? $settings['secret_key'] : '';
        
        $configured = !empty($api_login) && !empty($transaction_key) && !empty($mode);

        return $configured;
    }
}
