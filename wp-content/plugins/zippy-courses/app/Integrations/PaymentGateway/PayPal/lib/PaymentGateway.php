<?php

class ZippyCourses_PayPal_PaymentGateway extends Zippy_PaymentGateway
{
    public $id      = 'paypal';
    public $name    = 'PayPal';

    public function __construct()
    {
        parent::__construct();
    }

    public function setup()
    {
    }

    public function register()
    {
    }

    public function registerListener()
    {
        $this->listener = new ZippyCourses_PayPal_PaymentGatewayListener($this->id);
        $this->listener->register();
    }

    public function getId()
    {
        return $this->id;
    }
}
