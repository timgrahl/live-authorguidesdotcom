<?php

class ZippyCourses_PayPal_PaymentGatewayListener extends Zippy_PaymentGatewayListener
{
    public function __construct($gateway)
    {
        $settings = get_option('zippy_paypal_payment_gateway_settings', array());

        $this->mode     = isset($settings['test_mode']) && $settings['test_mode'] == '1' ? 'test' : 'live';

        $this->gateway = $gateway;
    }

    protected function process(Zippy_Event $event)
    {
        global $zippy_transaction_key;

        $zippy = Zippy::instance();
            
        // Determine what to process
        $notification_method = $this->_getNotificationMethod($event);

        switch ($notification_method) {
            case 'ipn':
                $transaction = $this->_processIpnTransaction($event);
                break;
            case 'pdt':
                $transaction = $this->_processPdtTransaction($event);
                break;
            default:
                $transaction = null;
                break;
        }

        if ($transaction) {
            // Set our global for use in filters, etc.
            $zippy_transaction_key = $transaction->getKey();
        }
    }

    private function _processIpnTransaction(Zippy_Event $event)
    {
        // Bail if this is not a validate IPN message from PayPal
        /**
         * @todo Implement a separate PayPal IPN validation when the data is coming from the Thank You page
         */
        /*
        if (!$this->validateIPN($event->raw_post)) {
            return;
        }
        */

        // Get an adapter for this gateway from the post data.
        $event_data = $this->parseRawPostdata($event->raw_post);

        // If this is a subscription cancellation, cancel the subscription
        if ($event_data['txn_type'] == 'subscr_cancel') {
            $this->_processSubscriptionCancellation($event_data['subscr_id']);
            return;
        }

        $adapter    = new ZippyCourses_PayPal_PaymentGatewayTransactionAdapter($event_data);

        return $this->_processTransactionData($adapter->getNormalizedData());
    }

    private function _processTransactionData($data)
    {
        $zippy = Zippy::instance();

        // If we've got a transaction key from the adapter, then use it to fetch the
        // existing transaction.
        
        if ($data['key'] && $this->validateTransactionKey($data['key'])) {
            // Prepare a fresh transaction object
            $transaction = $zippy->make('transaction');
            $transaction->buildByTransactionKey($data['key']);

            // Import the IPN data into the transaction and save.
            $transaction->importData($data);
            $transaction->save();
        } else {
            $transaction = null;
        }

        return $transaction;
    }

    private function _processSubscriptionCancellation($subscription_id)
    {
        global $wpdb;
        $zippy = Zippy::instance();

        $transaction_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'recurring_id', $subscription_id));
        $order_id = get_post_meta($transaction_id, 'order_id', true);

        if ($order_id) {
            $order = $zippy->make('order');
            $order->build($order_id);
            $order->setStatus('cancel');
            $order->save();
        }
    }

    private function _processPdtTransaction(Zippy_Event $event)
    {
        $event_data = $this->_getPdtData($event);
        
        if ($event_data) {
            $adapter    = new ZippyCourses_PayPal_PaymentGatewayTransactionAdapter($event_data);
            return $this->_processTransactionData($adapter->getNormalizedData());
        }

        return $this->_processBasicTransaction($event);
    }

    private function _validateBasicData(array $data)
    {
        if (!isset($data['key']) || !$data['key']) {
            return false;
        }

        $zippy = Zippy::instance();

        $valid = true;

        // Prepare a fresh transaction object
        $transaction = $zippy->make('transaction');
        $transaction->buildByTransactionKey($data['key']);

        if ($transaction->getTotal() != $data['total']) {
            $valid = false;
        }

        if ($transaction->getCurrency() != $data['currency']) {
            $valid = false;
        }

        return $valid;
    }

    private function _processBasicTransaction(Zippy_Event $event)
    {
        $adapter    = new ZippyCourses_PayPalBasic_PaymentGatewayTransactionAdapter($event->get);
        $data       = $adapter->getNormalizedData();

        return $this->_validateBasicData($data) ? $this->_processTransactionData($data) : false;
    }

    private function _getPdtData(Zippy_Event $event)
    {
        $settings       = get_option('zippy_paypal_payment_gateway_settings', array());
        $auth_token     = isset($settings['pdt_token']) && !empty($settings['pdt_token']) ? $settings['pdt_token'] : false;

        if (!$auth_token || empty($auth_token)) {
            return false;
        }

        $tx_token       = $event->get['tx'];

        $pdt = array(
            'body'          => array(
                'cmd' => '_notify-synch',
                'tx'  => $tx_token,
                'at'  => $auth_token
            ),
            'timeout'       => 60,
            'httpversion'   => '1.1',
            'user-agent'    => 'ZippyCourses/' . ZippyCourses::VERSION
        );

        $url = $this->getUrl() . 'cgi-bin/webscr';

        $request = wp_remote_post($url, $pdt);

        if (!is_wp_error($request) && strpos($request['body'], "SUCCESS") === 0) {
            $lines = explode(PHP_EOL, $request['body']);
            $lines = array_slice($lines, 1, -1);

            $output = array();

            foreach ($lines as $line) {
                $data = explode('=', $line);
                $output[$data[0]] = urldecode($data[1]);
            }

            if (!empty($output)) {
                return $output;
            }
        }

        return null;
    }

    private function _getNotificationMethod(Zippy_Event $event)
    {
        if (!empty($event->get) && isset($event->get['tx'])) {
            return 'pdt';
        }

        if (!empty($event->raw_post)) {
            $data = $this->parseRawPostdata($event->raw_post);
            
            if (isset($data['txn_type']) || isset($data['payment_status'])) {
                return 'ipn';
            }
        }
    }

    /**
     * Receipte php://input data and return an array of decoded items
     * @param  string $input php://input stream
     * @return array
     */
    private function parseRawPostdata($input)
    {
        if (!is_string($input)) {
            return array();
        }

        parse_str($input, $data);

        foreach ($data as $key => $value) {
            if (is_string($value)) {
                $data[$key] = urldecode($value);
            }
        }

        return $data;
    }

    /**
     * Validate that the IPN is for PayPal
     *
     * @param  ZippyCourses_TransactionNotification_Event $event
     *
     * @return bool
     */
    protected function validateGateway(Zippy_Event $event)
    {
        $data = !empty($event->raw_post) && is_string($event->raw_post) ? $this->parseRawPostdata($event->raw_post) : array();

        $valid = isset($data['txn_type']) || isset($data['payment_status']) || isset($event->get['tx']);

        return $valid;
    }

    /**
     *  Validate the IPN using wp_remote_post, so that it works with and without cURL enabled.
     *
     *  @param string
     *
     *  @return boolean
     */
    public function validateIPN($data)
    {
        $data = 'cmd=_notify-validate&' . urldecode($data);
        
        $url = $this->getUrl() . 'cgi-bin/webscr';
        $host = 'www.paypal.com';

        $args      = array(
            'method'           => 'POST',
            'sslverify'        => false,
            'timeout'          => 20,
            'redirection'      => 5,
            'httpversion'      => '1.1',
            'blocking'         => true,
            'headers'          => array(
                'host'         => $host,
                'connection'   => 'close',
                'content-type' => 'application/x-www-form-urlencoded',
                'post'         => '/cgi-bin/webscr HTTP/1.1',
            ),
            'body'             => $data
        );

        $response = wp_remote_post($url, $args);

        if (!is_wp_error($response)) {
            return strpos($response['body'], 'VERIFIED') !== false;
        } else {
        }

        // False by default
        return false;
    }

    public function getUrl()
    {
        return $this->mode == 'live' ? 'https://www.paypal.com/' : 'https://www.sandbox.paypal.com/';
    }

    public function validateTransactionKey($key)
    {
        global $wpdb;

        $post_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'transaction_key', $key));
        $vendor  = $post_id ? get_post_meta($post_id, 'vendor', true) : false;

        return $vendor && $vendor == 'paypal';
    }
}
