<?php

class ZippyCourses_PayPal_PaymentGatewayTransactionAdapter extends Zippy_PaymentGatewayTransactionAdapter
{
    public $gateway = 'paypal';
    
    public function normalize($data)
    {
        if ($this->normalized === null) {
            $zippy = Zippy::instance();
            
            $transaction_key    = $data['custom'];
            $transaction        = $zippy->make('transaction');
            $transaction->buildByTransactionKey($transaction_key);

            $vendor_id          = $this->normalizeVendorId($data);
            $id                 = $this->normalizeId($transaction_key, $vendor_id);

            $product            = $transaction->getProduct();
            $order              = $this->normalizeOrder($id);

            $this->normalized = array(
                'id'                    => $id,
                'key'                   => $this->normalizeKey($id, $transaction_key),
                'title'                 => get_the_title($id),
                'vendor_id'             => $vendor_id,
                'total'                 => $this->normalizeTotal($data),
                'fee'                   => $this->normalizeFee($data),
                'tax'                   => $this->normalizeTax($data),
                'currency'              => $this->normalizeCurrency($data),
                'customer'              => $this->normalizeCustomer($data),
                'type'                  => $this->normalizeType($transaction),
                'timestamp'             => $this->normalizeTimestamp($data),
                'status'                => $this->normalizeStatus($data),
                'gateway'               => $this->gateway,
                'mode'                  => $this->normalizeMode($data),
                'recurring'             => $this->normalizeRecurring($data),
                'recurring_id'          => $this->normalizeRecurringId($data),
                'order'                 => $order,
                'owner'                 => $transaction->getOwner(),
                'method'                => $this->normalizeMethod($data),
                'product'               => $product,
                'raw'                   => json_encode($data)
            );
        }

        return $this->normalized;
    }

    protected function normalizeOrder($id)
    {
        $zippy = Zippy::instance();

        if ($id) {
            $order_id = get_post_meta($id, 'order_id', true);

            if (!empty($order_id)) {
                $order = $zippy->make('order');
                $order->build($order_id);

                return $order;
            }
        }

        return;
    }

    protected function normalizeId($transaction_key, $vendor_id)
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'transaction_key', $transaction_key);
        $transaction_id = (int) $wpdb->get_var($sql);

        // If we've never seen this transaction key, it's a new transaction, so return right away.
        if ($transaction_id === 0) {
            return $transaction_id;
        }

        // If $vendor_id has not been passed in as 0 (like in the event of subscr_signup, etc),
        // then we can use it to see if this vendor_id has been used before.  Otherwise, it should be 0.
        if ($vendor_id) {
            $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'vendor_id', $vendor_id);
            $vendor_id_exists = $vendor_id ? $wpdb->get_var($sql) : null;

            if ($vendor_id_exists) {
                return $vendor_id_exists;
            }
        }

        // If an item has this transaction key and an empty vendor ID, use that record.  If an item has this transaction key, but has a vendor ID, create a new one...
        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE post_id = %s AND meta_key = %s", $transaction_id, 'vendor_id');
        $has_vendor_id = $wpdb->get_var($sql);

        if (empty($has_vendor_id)) {
            return $transaction_id;
        }

        return 0;
    }

    /**
     * Get the transaction key of this notification. If the Post ID given is not empty,
     * it means that we're getting a payment in a recurring cycle that has not been recorded yet.
     *
     * @param  string $transaction_id
     * @param  string $transaction_key
     * @return string|null
     */
    protected function normalizeKey($transaction_id, $transaction_key)
    {
        return $transaction_id !== 0 ? $transaction_key : null;
    }

    protected function normalizeType($transaction)
    {
        if ($transaction instanceof Zippy_Transaction) {
            return $transaction->getType();
        }

        return 'single';
    }

    protected function normalizeTotal($input)
    {
        return isset($input['mc_gross']) ? $input['mc_gross'] : 0;
    }

    protected function normalizeFee($input)
    {
        return isset($input['mc_fee']) ? $input['mc_fee'] : 0;
    }

    protected function normalizeCurrency($input)
    {
        // TODO: Change default to system currency
        return isset($input['mc_currency']) ? $input['mc_currency'] : 'USD';
    }

    protected function normalizeVendorId($input)
    {
        $vendor_id = isset($input['txn_id']) ? $input['txn_id'] : 0;
        $vendor_id = isset($input['parent_txn_id']) ? $input['parent_txn_id'] : $vendor_id;

        return $vendor_id;
    }

    protected function normalizeProduct($input)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $txn_details = get_post_meta($input, 'details', true);

        $product_id = isset($txn_details['product']) ? $txn_details['product'] : 0;

        return $product_id !== 0 ? $zippy->make('product', array('id' => $product_id)) : $zippy->make('product');
    }

    protected function normalizeTax($input)
    {
        return isset($input['tax']) ? $input['tax'] : 0;
    }

    protected function normalizeMode($input)
    {
        return isset($input['test_ipn']) && $input['test_ipn'] == '1' ? 'test' : 'live';
    }

    protected function normalizeMethod($input)
    {
        return 'gateway';
    }

    protected function normalizeRecurringId($input)
    {
        return isset($input['subscr_id']) ? $input['subscr_id'] : 0;
    }

    protected function normalizeRecurring($input)
    {
        return isset($input['subscr_id']);
    }

    /**
     * Map status to valid transaction status: complete|pending|refunded|failed|revoked|cancelled
     * @param  string $input The status from gateway
     * @return string
     */
    protected function normalizeStatus($input)
    {
        $status = 'pending';

        if (isset($input['payment_status'])) {
            switch ($input['payment_status']) {
                case 'Completed':
                    $status = 'complete';
                    break;
                case 'Refunded':
                    $status = 'refunded';
                    break;
                default:
                    break;
            }
        }

        if (isset($input['txn_type']) && $input['txn_type'] == 'subscr_cancel') {
            $status = 'cancelled';
        }

        return $status;
    }

    protected function normalizeTimestamp($input)
    {
        $zippy = Zippy::instance();
        
        $date_fields = array('payment_date', 'subscr_date');
        $date = null;

        foreach ($date_fields as $field) {
            if ($date === null && isset($input[$field])) {
                $date = $zippy->utilities->datetime->getDate($input[$field]);
            }
        }

        return $date;
    }

    protected function normalizeCustomer($input)
    {
        $zippy = Zippy::instance();

        $customer = $zippy->make('customer');
            $customer->first_name   = $input['first_name'];
            $customer->last_name    = $input['last_name'];
            $customer->email        = $input['payer_email'];

        return $customer;
    }
}
