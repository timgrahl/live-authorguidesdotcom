<?php

class ZippyCourses_PayPal_HttpQuery
{
    public $transaction;
    public $product;
    public $mode;
    public $business;

    public function __construct(Zippy_Transaction $transaction)
    {
        $settings = get_option('zippy_paypal_payment_gateway_settings', array());

        $this->business = isset($settings['email']) && !empty($settings['email']) ? $settings['email'] : '';
        $this->mode     = isset($settings['test_mode']) && $settings['test_mode'] == '1' ? 'test' : 'live';

        $this->transaction  = $transaction;
        $this->product      = $this->transaction->getProduct();
        $this->business = apply_filters('zippy_paypal_email_address', $this->business, $this->product);

    }

    public function getUrl()
    {
        $base_url = $this->mode == 'live' ? 'https://www.paypal.com/' : 'https://www.sandbox.paypal.com/';
        
        return $base_url . 'cgi-bin/webscr/?' . http_build_query($this->getPaymentArgs());
    }

    private function getSinglePaymentPaymentArgs()
    {
        $zippy = Zippy::instance();

        return apply_filters(
            'zippy_paypal_single_payment_args',
            array(
                'cmd'           => '_xclick',
                'amount'        => $this->product->getAmount(),
                'business'      => $this->business,
                'item_name'     => $this->product->getTitle(),
                'no_shipping'   => '1',
                'no_note'       => '1',
                'currency_code' => $zippy->utilities->cart->getCurrency(),
                'charset'       => 'UTF-8',
                'custom'        => $this->transaction->getKey(),
                'rm'            => '2',
                'return'        => $zippy->core_pages->getUrl('thank_you'),
                'notify_url'    => home_url('payment/notification/paypal')
            ),
            $this->product
        );
    }

    private function getSubscriptionArgs()
    {
        $zippy = Zippy::instance();
        
        return apply_filters(
            'zippy_paypal_subscription_args',
            array(
                'cmd' => '_xclick-subscriptions',
                'amount'        => $this->product->getAmount(),
                'business'      => $this->business,
                'item_name'     => $this->product->getTitle(),
                'no_shipping'   => '1',
                'no_note'       => '1',
                'currency_code' => $zippy->utilities->cart->getCurrency(),
                'charset'       => 'UTF-8',
                'custom'        => $this->transaction->getKey(),
                'rm'            => '2',
                'return'        => $zippy->core_pages->getUrl('thank_you'),
                'notify_url'    => home_url('payment/notification/paypal'),
                'src'           => '1',
                'a3'            => $this->product->getAmount(),
                'p3'            => '1',
                't3'            => $this->getFrequencyCode($this->product)
            ),
            $this->product
        );
    }

    private function getPaymentPlanArgs()
    {
        return apply_filters(
            'zippy_paypal_payment_plan_args',
            array_merge($this->getSubscriptionArgs($this->product), array('srt' => $this->product->getNumPayments())),
            $this->product
        );
    }

    public function getPaymentArgs()
    {
        switch ($this->product->getType()) {
            case 'subscription':
                return $this->getSubscriptionArgs($this->product);
                break;
            case 'payment-plan':
                return $this->getPaymentPlanArgs($this->product);
                break;
            case 'single':
            default:
                return $this->getSinglePaymentPaymentArgs($this->product);
                break;
        }
    }

    private function getFrequencyCode()
    {
        switch ($this->product->getFrequency()) {
            case 'day':
                return 'D';
                break;
            case 'week':
                return 'W';
                break;
            case 'month':
            default:
                return 'M';
                break;
            case 'year':
                return 'Y';
                break;
        }
    }
}