<?php

class ZippyCourses_PayPal_OrderFormProcessor extends ZippyCourses_Order_FormProcessor
{
    /**
     * Transaction
     * @var Zippy_Transaction
     */
    protected $txn;

    public $business;

    public function fail()
    {
        $zippy = Zippy::instance();
        $zippy->sessions->flashMessage('There was an error processing your request', 'error', 0);
    }

    public function execute()
    {
        $zippy = Zippy::instance();

        $product = $zippy->make('product', array('id' => $_POST['product_id']));

        if ($product->getType() == 'free' || $product->getAmount() == 0) {
            wp_redirect($zippy->core_pages->getUrl('register') . '?claim=' . $product->getUid());
            exit;
        }

        $this->txn     = $zippy->make('transaction');
            $this->txn->setProduct($product);
            $this->txn->setGateway('paypal');
            $this->txn->save();

        $query = new ZippyCourses_PayPal_HttpQuery($this->txn);

        if ($product) {
            wp_redirect($query->getUrl());
            exit;
        }
    }
}
