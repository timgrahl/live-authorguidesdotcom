<?php
/**
 * Id: paypal
 * File Type: payment-gateway-integration
 * Class: ZippyCourses_PayPal_PaymentGatewayIntegration
 *
 * @since 1.0.0
 */

class ZippyCourses_PayPal_PaymentGatewayIntegration extends Zippy_PaymentGatewayIntegration
{
    public $id = 'paypal';
    public $service = 'paypal';
    public $name = 'PayPal';
    public $settings = array();
    
    protected $api;

    private $path;

    public function __construct()
    {
        $this->path = plugin_dir_path(__FILE__);
        $this->url  = plugin_dir_url(__FILE__);

        parent::__construct();
    }

    public function setup()
    {
        $this->settings();
        $this->actions();
        $this->filters();
    }

    protected function actions()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_action('init', array($this, 'orderForm'));
            add_action('template_redirect', array($this, 'buyNow'), 11);
        }
        
        add_action('template_redirect', array($this, 'buyViaPayPal'));
    }

    protected function filters()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_filter('zippy_active_gateway_configured', array($this, 'checkConfiguration'));
        }
    }

    public function map($classes)
    {
        $classes['ZippyCourses_PayPal_OrderForm']                           = $this->path . 'lib/OrderForm.php';
        $classes['ZippyCourses_PayPal_OrderFormProcessor']                  = $this->path . 'lib/OrderFormProcessor.php';
        $classes['ZippyCourses_PayPal_PaymentGatewayListener']              = $this->path . 'lib/Listener.php';
        $classes['ZippyCourses_PayPal_PaymentGatewayTransactionAdapter']    = $this->path . 'lib/Adapter.php';
        $classes['ZippyCourses_PayPalBasic_PaymentGatewayTransactionAdapter']    = $this->path . 'lib/BasicAdapter.php';
        $classes['ZippyCourses_PayPal_PaymentGateway']                      = $this->path . 'lib/PaymentGateway.php';
        $classes['ZippyCourses_PayPal_HttpQuery']                      = $this->path . 'lib/HttpQuery.php';

        return $classes;
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $integrations = $zippy->make('payment_gateway_integrations');
        $integrations->add($this);

        $gateway    = new ZippyCourses_PayPal_PaymentGateway;
        $gateways   = $zippy->make('payment');
        $gateways->add($gateway);
    }

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    public function settings()
    {
        $zippy = Zippy::instance();

        $settings_pages = $zippy->make('settings_pages_repository');
        $page = $settings_pages->fetch('zippy_settings_payment');


        $section = $page->createSection($this->getSettingsName(), 'PayPal');
            $section->createField('email', __('Email', ZippyCourses::TEXTDOMAIN));
            $section->createField('pdt_token', __('PDT Token', ZippyCourses::TEXTDOMAIN));
            $section->createField('test_mode', __('Test Mode', ZippyCourses::TEXTDOMAIN), 'select', array(
                '0' => __('Off', ZippyCourses::TEXTDOMAIN),
                '1' => __('On', ZippyCourses::TEXTDOMAIN),
            ));

    }

    public function orderForm()
    {
        $zippy = Zippy::instance();

        $settings   = get_option('zippy_payment_general_settings', null);
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_PayPal_OrderForm');
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function checkConfiguration($configured)
    {
        $settings = get_option('zippy_paypal_payment_gateway_settings');
        $email    = isset($settings['email']) ? $settings['email'] : '';

        $configured = !empty($email);

        return $configured;
    }

    public function buyNow()
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) ||
            (
                $post->post_type !== 'product' &&
                !$zippy->core_pages->is($post->ID, 'buy')
            ) ||
            !isset($_GET['buy-now']) ||
            current_user_can('edit_others_posts')
        ) {
            return;
        }

        $product = $post->post_type == 'product'
            ? $zippy->make('product', array('id' => $post->ID))
            : $zippy->utilities->product->getProductByBuyQueryVar();

        if ($product !== null) {
            if (!$zippy->utilities->product->inLaunchWindow($product)) {
                $message = __('This product is not currently available for sale.', ZippyCourses::TEXTDOMAIN);

                $content = '<div class="zippy-alert zippy-alert-error">' . $message . '</div>';
                $product = null;
            }
        }

        if ($product) {
            $transaction = $zippy->make('transaction');
            $transaction->setProduct($product);
            $transaction->setGateway('paypal');
            $transaction->save();

            $query = new ZippyCourses_PayPal_HttpQuery($transaction);

            wp_redirect($query->getUrl());
            exit;
        }
    }

    public function buyViaPayPal()
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || $post->post_type !== 'product' || !isset($_GET['buy-with']) || $_GET['buy-with'] !== $this->service) {
            return;
        }

        $product = $zippy->make('product', array('id' => $post->ID));

        if ($product) {
            $transaction = $zippy->make('transaction');
            $transaction->setProduct($product);
            $transaction->setGateway('paypal');
            $transaction->save();

            $query = new ZippyCourses_PayPal_HttpQuery($transaction);

            wp_redirect($query->getUrl());
            exit;
        }
    }
}
