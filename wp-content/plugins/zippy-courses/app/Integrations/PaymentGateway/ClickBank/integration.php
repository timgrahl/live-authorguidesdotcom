<?php
/**
 * Id: clickbank
 * File Type: payment-gateway-integration
 * Class: ZippyCourses_ClickBank_PaymentGatewayIntegration
 *
 * @since 1.0.0
 */

class ZippyCourses_ClickBank_PaymentGatewayIntegration extends Zippy_PaymentGatewayIntegration
{
    public $id = 'clickbank';
    public $service = 'clickbank';
    public $name = 'ClickBank';
    public $settings = array();
    
    protected $api;

    private $path;
    private $url;

    public function __construct()
    {
        $this->path = plugin_dir_path(__FILE__);
        $this->url  = plugin_dir_url(__FILE__);

        add_filter('zippy_middleware_rules', array($this, 'middleware'), 10, 2);

        parent::__construct();
    }

    public function setup()
    {
        $this->settings();
        $this->actions();
        $this->filters();
        $this->utilities();

        $this->api = new ZippyCourses_ClickBank_PaymentGatewayAPI;

        add_filter('zippy_metaboxes', array($this, 'metaboxes'));
    }

    protected function actions()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        add_action('wp_enqueue_scripts', array($this, 'assets'));

        if ($method == $this->service) {
            add_action('init', array($this, 'orderForm'));
            add_action('template_redirect', array($this, 'buyNow'), 11);
        }
    }

    protected function filters()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_filter('zippy_active_gateway_configured', array($this, 'checkConfiguration'));
        }
    }

    public function map($classes)
    {
        $classes['ZippyCourses_ClickBank_PaymentGateway']            = $this->path . 'lib/PaymentGateway.php';
        $classes['ZippyCourses_ClickBank_PaymentGatewayAPI']         = $this->path . 'lib/API.php';
        $classes['ZippyCourses_ClickBank_PaymentGatewayListener']    = $this->path . 'lib/Listener.php';
        $classes['ZippyCourses_ClickBankGateway_Utilities']          = $this->path . 'lib/Utilities.php';
        $classes['ZippyCourses_ClickBank_OrderForm']                 = $this->path . 'lib/OrderForm.php';
        $classes['ZippyCourses_ClickBank_OrderFormProcessor']        = $this->path . 'lib/OrderFormProcessor.php';
        $classes['ZippyCourses_ClickBank_Order']                     = $this->path . 'lib/Order.php';
        $classes['Zippy_OrderHasClickBankAccess_MiddlewareRule']     = $this->path . 'lib/Middleware/Rules/OrderHasClickBankAccess.php';
        $classes['ZippyCourses_ClickBank_PaymentGatewayTransactionAdapter']    = $this->path . 'lib/Adapter.php';
       
        return $classes;
    }

    public function utilities()
    {
        $zippy = Zippy::instance();

        if (!isset($zippy->utilities->clickbank)) {
            $zippy->utilities->clickbank = new stdClass;
        }

        $zippy->utilities->clickbank->gateway = new ZippyCourses_ClickBankGateway_Utilities;
    }

    public function registerForm()
    {
        $zippy = Zippy::instance();

        $payment_settings = get_option('zippy_payment_general_settings', array());
        $gateway          = isset($payment_settings['method']) ? $payment_settings['method'] : null;

        if ($this->id == $gateway) {
            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_ClickBank_OrderForm');
        }
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $integrations = $zippy->make('payment_gateway_integrations');
        $integrations->add($this);

        $gateway    = new ZippyCourses_ClickBank_PaymentGateway;
        $gateways   = $zippy->make('payment');
        
        $gateways->add($gateway);

        $this->registerForm();
    }

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    public function settings()
    {
        $zippy = Zippy::instance();

        $settings_pages = $zippy->make('settings_pages_repository');
        $page = $settings_pages->fetch('zippy_settings_payment');

        $section = $page->createSection($this->getSettingsName(), 'ClickBank');
            $section->createField('nickname', __('Nickname', ZippyCourses::TEXTDOMAIN));
            $section->createField('clerk_key', __('Clerk API Key', ZippyCourses::TEXTDOMAIN));
            $section->createField('developer_key', __('Developer Key', ZippyCourses::TEXTDOMAIN));
            $section->createField('secret_key', __('Secret Key', ZippyCourses::TEXTDOMAIN));

    }

    public function orderForm()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            $zippy = Zippy::instance();

            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_ClickBank_OrderForm');
        }
    }

    public static function path()
    {
        return plugin_dir_path(__FILE__);
    }

    public static function url()
    {
        return plugin_dir_url(__FILE__);
    }

    public function assets()
    {
    }

    /**
     * Integrate with the correct metaboxes
     * @return void
     */
    public function metaboxes($metaboxes)
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method = isset($settings['method']) ? $settings['method'] : false;

        if ($method != $this->service) {
            return $metaboxes;
        }

        $dir = $this->path . 'assets/views/metaboxes/';
        $files = array_diff(scandir($dir), array('..', '.'));

        foreach ($files as $key => &$file) {
            if (!is_dir($dir . $file)) {
                $file = $dir . $file;
            } else {
                unset($files[$key]);
            }
        }

        return array_merge($metaboxes, $files);
    }

    public function middleware($rules, $id)
    {
        if ($id !== 'order-grants-access') {
            return $rules;
        }

        $rules->add(new Zippy_OrderHasClickBankAccess_MiddlewareRule);

        return $rules;
    }

    public function getId()
    {
        return $this->id;
    }

    public function checkConfiguration($configured)
    {
        $settings = get_option('zippy_clickbank_payment_gateway_settings');

        $nickname = isset($settings['nickname']) ? $settings['nickname'] : '';
        $clerk_key = isset($settings['clerk_key']) ? $settings['clerk_key'] : '';
        $developer_key = isset($settings['developer_key']) ? $settings['developer_key'] : '';
        $secret_key = isset($settings['secret_key']) ? $settings['secret_key'] : '';
        
        $configured = !empty($nickname) && !empty($clerk_key) && !empty($developer_key) && !empty($secret_key);

        return $configured;
    }

    public function buyNow()
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || ($post->post_type !== 'product' && !$zippy->core_pages->is($post->ID, 'buy')) || !isset($_GET['buy-now'])) {
            return;
        }

        $product = $post->post_type == 'product'
            ? $zippy->make('product', array('id' => $post->ID))
            : $zippy->utilities->product->getProductByBuyQueryVar();

        if ($product) {
            $options                = get_option('zippy_clickbank_payment_gateway_settings');
            $nickname               = isset($options['nickname']) ? $options['nickname'] : '';
            $clickbank_product_id   = get_post_meta($product->getId(), 'clickbank_product_id', true);

            $url = 'http://' . $clickbank_product_id . '.' . $nickname . '.pay.clickbank.net';

            wp_redirect($url);
            exit;
        }
    }
}
