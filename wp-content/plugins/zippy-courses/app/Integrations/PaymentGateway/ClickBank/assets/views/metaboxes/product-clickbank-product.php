<?php
/*
Id: zippy-clickbank-product
Name: ClickBank Product
Fields: clickbank_product_id, clickbank_order_form_url
Post Types: product
Context: normal
Priority: default
Version: 1.0.0
*/
global $post;

$list = $zippy->utilities->clickbank->gateway->getProductsList();
?>

<p>
<label for="clickbank_product_id"><?php _e('ClickBank Product:', ZippyCourses::TEXTDOMAIN); ?></label>
<select name="clickbank_product_id" id="clickbank_product_id">
    <option value=""></option>
    <?php foreach ($list as $item):
        $disabled = $item['used'] && $item['id'] != $clickbank_product_id ? ' disabled="disabled" ' : '';
        $selected = selected($clickbank_product_id, $item['id'], false);
        ?>
        <option value="<?php echo $item['id']; ?>" <?php echo $selected; ?> <?php echo $disabled; ?>><?php echo $item['name']; ?></option>
    <?php endforeach; ?>
</select>
</p>
