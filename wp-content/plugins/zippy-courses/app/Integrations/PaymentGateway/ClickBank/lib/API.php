<?php

/**
 * ClickBank Gateway for Zippy Courses.  Using this API is useful because it does not require CURL as the official
 * library does, and it also uses wp_remote_post to make remote requests.
 *
 * The primary downside is that not everything is a particular type of ClickBank Object as the official SDK is,
 * but the functionality is leaner.
 */
class ZippyCourses_ClickBank_PaymentGatewayAPI extends Zippy_PaymentGatewayAPI
{
    public $nickname;
    public $clerk_key;
    public $developer_key;
    public $secret_key;
    public $mode = 'live';

    public function __construct()
    {
        $options               = get_option('zippy_clickbank_payment_gateway_settings');
        
        $this->nickname        = isset($options['nickname']) ? $options['nickname'] : '';
        $this->secret_key      = isset($options['secret_key']) ? $options['secret_key'] : '';
        $this->developer_key   = isset($options['developer_key']) ? $options['developer_key'] : '';
        $this->clerk_key       = isset($options['clerk_key']) ? $options['clerk_key'] : '';
    }

    public function getSecretKey()
    {
        return $this->secret_key;
    }

    public function getDeveloperKey()
    {
        return $this->developer_key;
    }

    public function getClerkKey()
    {
        return $this->clerk_key;
    }

    /**
     * Handle a wp_remote_post call, either returning a json_decoded version of the body,
     * or WP_Error in the event of a non-200 status code.
     *
     * @since   1.0.0
     *
     * @param   array   $response   The results form a wp_remote_post request
     *
     * @return  JSON|WP_Error
     */
    public function handleResponse($response)
    {
        if (!is_wp_error($response)) {
            if ($response['response']['code'] == 200) {
                $body = json_decode($response['body']);
                return $body;
            } else {
                $body = json_decode($response['body']);
                $error = new WP_Error($body->error->type, $body->error->message, $response);
            }
        } else {
        }
    }

    /**
     * Build the parameters from a default, plus the data sent by the calling method
     *
     * @since   1.0.0
     *
     * @param   array   $body       The parameters being sent in the request body
     * @param   string  $method     The HTTP method
     *
     * @return  JSON|WP_Error
     */
    private function buildParams(array $body = array(), $method = 'GET')
    {
        return array(
            'method'        => $method,
            'timeout'       => 10,
            'redirection'   => 5,
            'httpversion'   => '1.1',
            'blocking'      => true,
            'headers'       => array(
                'Accept'        => 'application/json',
                'Authorization' => $this->_getAuthKey()
            )
        );
    }

    private function _getAuthKey()
    {
        return "{$this->developer_key}:{$this->clerk_key}";
    }

    /**
     * Basic endpoint builder
     *
     * @since   1.0.0
     *
     * @param   string $endpoint
     *
     * @return  string
     */
    private function getUrl($endpoint)
    {
        return $this->getBaseUrl() . $endpoint;
    }

    /**
     * Basic endpoint builder
     *
     * @since   1.0.0
     *
     * @param   string $endpoint
     *
     * @return  string
     */
    private function buildUrl($endpoint, array $args = array())
    {
        return $this->getUrl($endpoint) . '?' . http_build_query($args);
    }

    private function getBaseUrl()
    {
        return $this->mode == 'live' ? 'https://api.clickbank.com/rest/1.3/' : 'https://sandbox.clickbank.com/rest/1.3/';
    }

    public function getProducts()
    {
        $url    = $this->buildUrl('products/list', array('site' => $this->nickname));
        $params = $this->buildParams();

        $response = $this->handleResponse(wp_remote_post($url, $params));

        if (!is_wp_error($response)) {
            return is_array($response->products->product)
                ? $response->products->product
                : array($response->products->product);
        }

        return $response;
    }

    public function getOrder($remote_order_id)
    {
        $url    = $this->buildUrl('orders/' . $remote_order_id);
        $params = $this->buildParams();

        $response = $this->handleResponse(wp_remote_post($url, $params));

        if (!is_wp_error($response)) {
            return is_array($response->orderData) ? reset($response->orderData) : $response->orderData;
        }

        return $response;
    }

    public function getOrders()
    {
        $url    = $this->buildUrl('orders/list');
        $params = $this->buildParams();

        $response = $this->handleResponse(wp_remote_post($url, $params));

        if (!is_wp_error($response)) {
            return $response;
        }

        return $response;
    }
}

/*
class ZippyPowerUp_ClickBank_Api {

    protected $developer_key, $clerk_key, $nickname;
    public $mode = 'live';

    public function __construct( $nickname, $developer_key, $clerk_key ) {
        $this->developer_key = $developer_key;
        $this->clerk_key = $clerk_key;
        $this->nickname = $nickname;
    }

    public function get_url_base() {
        return $this->mode == 'live' ? 'https://api.clickbank.com/rest/1.3/' : 'https://sandbox.clickbank.com/rest/1.3/';
    }

    public function make_request( $endpoint, $method = 'GET', $format = 'json', $args = array() ) {

        $url = $this->get_url_base() . $this->filter_endpoint( $endpoint );

        if( !empty( $args )) {
             $url .= '?' . $this->get_params( $args );
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method );
        curl_setopt($ch, CURLOPT_HEADER, false );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( "Accept: application/$format", "Authorization: $this->developer_key:$this->clerk_key" ) );
        
        $result = curl_exec($ch);

        $code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

        curl_close($ch);

        return $format == 'json' ? json_decode( $result ) : simplexml_load_string( $result );

    }

    public function get_params( array $args )  {
        return http_build_query( $args );
    }

    public function get_products() {
        
        $args = $this->mode == 'live' ? array( 'site' => $this->nickname ) : array();

        $products = $this->make_request( 'products/list', 'GET', 'json', $args );

        return is_object( $products ) && $products->total_record_count > 0 ? ( is_array($products->products->product) ? $products->products->product : array($products->products->product)) : array();

    }

    public function get_product( $sku ) {
        
        $product = $this->make_request( 'product/' . $sku );

        return $product;

    }

    public function get_orders() {
        
        $orders = $this->make_request( 'orders/list' );

        return is_object( $orders ) || is_array( $orders ) ? current( $orders ) : array();

    }

    public function get_order( $receipt ) {
        
        $order = $this->make_request( 'orders/' . $receipt );

        $order = is_array( $order ) || is_object( $order ) ? current( $order ) : false;

        // For recurring, only get the subscription/payment-plan creating receipt instead of payments
        $order = is_array( $order ) ? end( $order ) : $order; 

        return $order;

    }

    public function get_recurring_order( $receipt ) {
        
        $order = $this->make_request( 'orders/' . $receipt );

        $order = is_array( $order ) || is_object( $order ) ? current( $order ) : false;

        return $order;

    }

    private function filter_endpoint( $endpoint ) {


        if( $this->mode == 'live' ) {
            $endpoint = str_replace(
                array( 'product/' ),
                array( 'products/' ),
                $endpoint
            );  
        } else {
            $endpoint = str_replace(
                array( 'products/' ),
                array( 'sandbox/product/' ),
                $endpoint
            );
        }

        return $endpoint;

    }

    public function create_sandbox_txn( $product_id ) {
        $txns = $this->make_request( 'sandbox/create-txn', 'POST', 'json', array( 'count' => 5, 'item' => $product_id, 'zip' => '66223' ) );
        return $txns;
    }

    public function set_mode( $mode ) {
        $this->mode = $mode;
    }

    public function debug() {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.clickbank.com/rest/1.3/debug");
        curl_setopt($ch, CURLOPT_HEADER, false); 
        curl_setopt($ch, CURLOPT_HTTPGET, true); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( "Accept: application/json", "Authorization: $this->developer_key:$this->clerk_key" ) );
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;

    }

    public function auth() {
        
        $url = $this->mode == 'live' ? "https://api.clickbank.com/rest/1.3/debug" : "https://sandbox.clickbank.com/rest/1.3/debug";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_HEADER, false); 
        curl_setopt($ch, CURLOPT_HTTPGET, true); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( "Accept: application/json", "Authorization: $this->developer_key:$this->clerk_key" ) );
        $result = curl_exec($ch);
        
        $code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

        curl_close($ch);        

        return $code != 403;

    }

}
*/