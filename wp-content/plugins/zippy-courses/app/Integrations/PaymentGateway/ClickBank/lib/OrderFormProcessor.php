<?php

class ZippyCourses_ClickBank_OrderFormProcessor extends Zippy_FormProcessor
{
    public function fail()
    {
        $zippy = Zippy::instance();
        $zippy->session->flashMessage('There was an error processing your request', 'error');
    }

    public function execute()
    {
        $zippy = Zippy::instance();

        $options                = get_option('zippy_clickbank_payment_gateway_settings');
        $nickname               = isset($options['nickname']) ? $options['nickname'] : '';
        $product                = $zippy->make('product', array('id' => $_POST['product_id']));
        $clickbank_product_id   = get_post_meta($product->getId(), 'clickbank_product_id', true);
        
        $url = 'http://' . $clickbank_product_id . '.' . $nickname . '.pay.clickbank.net';

        wp_redirect($url);
        exit;
    }
}
