<?php

class ZippyCourses_ClickBank_PaymentGatewayListener extends Zippy_PaymentGatewayListener
{
    public function register()
    {
        parent::register();

        $zippy = Zippy::instance();

        $zippy->events->register('OrderStatusChange', $this);
        $zippy->events->register('OrderOwnerChange', $this);
        $zippy->events->register('SaveTransaction', $this);
        $zippy->events->register('OrderSave', $this);
    }

    public function handle(Zippy_Event $event)
    {
        switch ($event->getEventName()) {
            case 'TransactionNotification':
                if ($this->validateGateway($event)) {
                    $this->process($event);
                }
                break;
            case 'OrderSave':
                $this->_handleOrderSave($event);
                break;
            default:
                break;
        }
    }

    private function _handleOrderSave(Zippy_Event $event)
    {
        $order = $event->order;

        if (!$order->transactions->count()) {
            $order->fetchTransactions();
        }

        $transaction = $order->transactions->first();

        if (!$transaction) {
            return;
        }

        $vendor     = get_post_meta($transaction->getId(), 'vendor', true);
        $details    = get_post_meta($transaction->getId(), 'details', true);

        if ($vendor !== 'clickbank' || !is_array($details) || !isset($details['raw'])) {
            return;
        }

        $json = json_decode($details['raw']);
        $clickbank_order_id = false;

        if (isset($json->receipt)) {
            $clickbank_order_id = $json->receipt;
        }

        if ($clickbank_order_id) {
            update_post_meta($order->getId(), 'clickbank_order_id', $clickbank_order_id);
        }
    }

    protected function process(Zippy_Event $event)
    {
        global $zippy_transaction_key;

        $zippy = Zippy::instance();

        $type = isset($event->get['cbreceipt']) ? 'thank-you' : 'notification';

        switch ($type) {
            case 'notification':
                $transaction = $this->_processNotification($event);
                break;
            case 'thank-you':
                $transaction = $this->_processThankyou($event);
                break;
            default:
                $transaction = null;
                break;
        }

        if ($transaction) {
            // Set our global for use in filters, etc.
            $zippy_transaction_key = $transaction->getKey();
        }
    }

    private function _processNotification(Zippy_Event $event)
    {
        $api = new ZippyCourses_ClickBank_PaymentGatewayAPI;

        $message = json_decode($event->raw_post);
        
        $encrypted = $message->{'notification'};
        $iv = $message->{'iv'};
        
        // decrypt the body...
        $decrypted = trim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128,
                substr(sha1($api->getSecretKey()), 0, 32),
                base64_decode($encrypted),
                MCRYPT_MODE_CBC,
                base64_decode($iv)
            ),
            "\0..\32"
        );

        $data = json_decode($decrypted);

        $vendor_order_id = $data->receipt;

        return $this->_processOrder($vendor_order_id);
    }

    private function _processThankYou(Zippy_Event $event)
    {
        $vendor_order_id   = $event->get['cbreceipt'];

        return $this->_processOrder($vendor_order_id);
    }

    private function _processOrder($vendor_order_id)
    {
        $api = new ZippyCourses_ClickBank_PaymentGatewayAPI;
        $vendor_order = $api->getOrder($vendor_order_id);

        $adapter    = new ZippyCourses_ClickBank_PaymentGatewayTransactionAdapter($vendor_order);
        return $this->_processTransactionData($adapter->getNormalizedData());
    }

    private function _processTransactionData($data)
    {
        $zippy = Zippy::instance();

        // Prepare a fresh transaction object
        $transaction = $zippy->make('transaction');

        // If we've got a transaction key from the adapter, then use it to fetch the
        // existing transaction.
        if ($data['key']) {
            $transaction->buildByTransactionKey($data['key']);
        }
    
        // Import the IPN data into the transaction and save.
        $transaction->importData($data);

        $transaction->save();

        return $transaction;
    }

    /**
     * Validate that the IPN is for ClickBank
     *
     * @param  ZippyCourses_TransactionNotification_Event $event
     *
     * @return bool
     */
    protected function validateGateway(Zippy_Event $event)
    {
        $receipt    = filter_input(INPUT_GET, 'cbreceipt');
        $email      = urldecode(filter_input(INPUT_GET, 'cemail'));

        return $event->gateway == 'clickbank' || (!empty($email) && !empty($receipt));
    }
}
