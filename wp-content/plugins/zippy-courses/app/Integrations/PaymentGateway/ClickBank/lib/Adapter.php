<?php

class ZippyCourses_ClickBank_PaymentGatewayTransactionAdapter extends Zippy_PaymentGatewayTransactionAdapter
{
    public $gateway = 'clickbank';
    
    public function normalize($data)
    {
        if ($this->normalized === null) {
            $zippy = Zippy::instance();

            $transaction        = $zippy->make('transaction');
            $transaction_key    = $this->normalizeKey($data);

            $transaction->buildByTransactionKey($transaction_key);

            $vendor_id          = $data->receipt;

            $id                 = $this->normalizeId($transaction_key, $vendor_id);
            $order              = $this->normalizeOrder($id, $vendor_id, $data);
           
            $this->normalized = array(
                'id'                    => $id,
                'key'                   => $transaction_key,
                'title'                 => ($id ? get_the_title($id) : ''),
                'vendor_id'             => $vendor_id,
                'total'                 => $this->normalizeTotal($data),
                'fee'                   => $this->normalizeFee($data),
                'tax'                   => $this->normalizeTax($data),
                'currency'              => $this->normalizeCurrency($data),
                'customer'              => $this->normalizeCustomer($data),
                'type'                  => $this->normalizeType($transaction),
                'timestamp'             => $this->normalizeTimestamp($data),
                'status'                => $this->normalizeStatus($data),
                'gateway'               => $this->gateway,
                'mode'                  => $this->normalizeMode($data),
                'recurring'             => $this->normalizeRecurring($data),
                'recurring_id'          => $this->normalizeRecurringId($data),
                'order'                 => $order,
                'owner'                 => $transaction->getOwner(),
                'method'                => $this->normalizeMethod($data),
                'product'               => $this->normalizeProduct($transaction, $data),
                'raw'                   => json_encode($data)
            );
        }

        return $this->normalized;
    }

    protected function normalizeId($transaction_key, $vendor_id)
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'transaction_key', $transaction_key);
        $transaction_id = (int) $wpdb->get_var($sql);

        // If we've never seen this transaction key, it's a new transaction, so return right away.
        if ($transaction_id === 0) {
            return $transaction_id;
        }

        // If $vendor_id has not been passed in as 0 (like in the event of subscr_signup, etc),
        // then we can use it to see if this vendor_id has been used before.  Otherwise, it should be 0.
        if ($vendor_id) {
            $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'vendor_id', $vendor_id);
            $vendor_id_exists = $vendor_id ? $wpdb->get_var($sql) : null;

            if ($vendor_id_exists) {
                return $vendor_id_exists;
            }
        }

        // If an item has this transaction key and an empty vendor ID, use that record.  If an item has this transaction key, but has a vendor ID, create a new one...
        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE post_id = %s AND meta_key = %s", $transaction_id, 'vendor_id');
        $has_vendor_id = $wpdb->get_var($sql);

        if (empty($has_vendor_id)) {
            return $transaction_id;
        }

        return 0;
    }

    /**
     * Get the transaction key of this notification. If the Post ID given is not empty,
     * it means that we're getting a payment in a recurring cycle that has not been recorded yet.
     *
     * @return string|null
     */
    protected function normalizeKey($input)
    {
        global $wpdb;

        $key = null;

        $sql = $wpdb->prepare(
            "SELECT
                pm1.post_id,
                pm1.meta_value 
             FROM 
                $wpdb->postmeta AS pm1 
             LEFT JOIN 
                $wpdb->postmeta AS pm2 
             ON
                (pm1.post_id = pm2.post_id) 
             WHERE 
                pm1.meta_key = %s AND
                pm2.meta_key = %s AND
                pm2.meta_value = %s",
            'vendor_id',
            'vendor',
            'clickbank'
        );

        $results = $wpdb->get_results($sql);
        
        $transactions = array();
        foreach ($results as $r) {
            $transactions[$r->post_id] = $r->meta_value;
        }

        $post_id = array_search($input->receipt, $transactions);

        if ($post_id !== false) {
            $key = get_post_meta($post_id, 'transaction_key', true);
        }

        return $key;
    }

    protected function normalizeType($transaction)
    {
        if ($transaction instanceof Zippy_Transaction) {
            return $transaction->getType();
        }

        return 'single';
    }

    protected function normalizeTotal($input)
    {
        return $input->amount;
    }

    protected function normalizeFee($input)
    {
        return 0;
    }

    protected function normalizeCurrency($input)
    {
        if (isset($input->currency)) {
            return $input->currency;
        }

        $zippy = Zippy::instance();

        return $zippy->utilities->cart->getCurrency();
    }

    protected function normalizeProduct(Zippy_Transaction $transaction, $data)
    {
        if (($product = $transaction->getProduct()) instanceof Zippy_Product) {
            return $product->getId();
        }

        global $wpdb;

        $zippy = Zippy::instance();
        $remote_product_id = $data->item;

        $sql = $wpdb->prepare(
            "SELECT
                post_id
             FROM
                $wpdb->postmeta
             WHERE
                meta_key = %s AND
                meta_value = %s",
            'clickbank_product_id',
            $remote_product_id
        );

        return (int) $wpdb->get_var($sql);
    }

    protected function normalizeTax($input)
    {
        return 0;
    }

    protected function normalizeMode($input)
    {
        return 'live';
    }

    protected function normalizeMethod($input)
    {
        return 'credit_card';
    }

    protected function normalizeRecurringId($input)
    {
        if ($input->recurring == 'true') {
            $id = $input->receipt;
            $parts = explode('-', $id);

            return $parts[0];
        }
        
        return 0;
    }

    protected function normalizeRecurring($input)
    {
        return $input->recurring == 'true';
    }

    /**
     * Map status to valid transaction status: complete|pending|refund|fail|revoke|cancel
     *
     * Available status: ACTIVE | COMPLETED | CANCELED | RETRY_PAYMENT | REQUEST_NEW_CARD
     * Available types: SALE | RFND |CGBK | FEE | BILL | TEST_SALE | TEST_BILL | TEST_RFND | TEST_FEE
     *
     * @param  string $input The status from gateway
     * @return string
     */
    protected function normalizeStatus($input)
    {
        $status         = $input->recurring == 'false' ? $input->txnType : $input->status;
        $valid_statuses = array('ACTIVE', 'COMPLETED', 'SALE', 'BILL', 'TEST_SALE', 'TEST_BILL');

        return in_array($status, $valid_statuses) ? 'complete' : 'refund';
    }

    protected function normalizeTimestamp($input)
    {
        $zippy = Zippy::instance();
        
        if (!empty($input->date)) {
            return $zippy->utilities->datetime->getDate($input->date);
        }
        
        return $zippy->utilities->datetime->getNow();
    }

    protected function normalizeOrder($id, $vendor_id, $data)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        if ($id) {
            $order_id = get_post_meta($id, 'order_id', true);
        } else {
            if ($data->recurring == 'true') {
                $order_id = $this->_getSubscriptionTransactions($vendor_id);
            }
        }

        if (!empty($order_id)) {
            $order = $zippy->make('order');
            $order->build($order_id);

            return $order;
        }

        return;
    }

    public function _getSubscriptionTransactions($vendor_id)
    {
        global $wpdb;

        $order_id = 0;

        $parts = explode('-', $vendor_id);

        $sql = $wpdb->prepare(
            "SELECT
                post_id
             FROM
                $wpdb->postmeta
             WHERE
                meta_key = %s AND
                meta_value = %s",
            'recurring_id',
            $parts[0]
        );

        $transaction_id = $wpdb->get_var($sql);


        if ($transaction_id) {
            $order_id = get_post_meta($transaction_id, 'order_id', true);
        }

        return $order_id;
    }

    protected function normalizeCustomer($input)
    {
        $zippy = Zippy::instance();

        $customer = $zippy->make('customer');

        $customer->importData(array(
            'email'         => $input->email,
            'first_name'    => ucwords(strtolower($input->firstName)),
            'last_name'     => ucwords(strtolower($input->lastName))
        ));

        return $customer;
    }
}
