<?php

class Zippy_OrderHasClickBankAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You do not own access to this content.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'order-has-clickbank-access';
    }

    public function handle($object)
    {
        if (!($object instanceof Zippy_Order) || is_admin()) {
            $this->exitEarly(true);
        }

        if ($object->getGateway() !== 'clickbank') {
            return $this->pass($object);
        }

        $order_id = get_post_meta($object->getId(), 'clickbank_order_id', true);

        if (empty($order_id)) {
            $this->exitEarly(true);
        }

        $zippy = Zippy::instance();

        $access = $this->_handleOrderAccess($object);

        if ($this->_transientExpired($object)) {
            $now = $zippy->utilities->datetime->getNow();

            $status = array(
                'active' => ($access ? 'yes' : 'no'),
                'expire' => $now->format('U')
            );

            update_post_meta($object->getId(), 'clickbank_order_status', $status);
        }

        $access = apply_filters('zippy_courses_clickbank_order_access', $access, $object);

        return $access ? $this->pass($object) : $this->fail($object);
    }

    private function _transientExpired($object)
    {
        $zippy = Zippy::instance();
        
        $status = get_post_meta($object->getId(), 'clickbank_order_status', true);
        $now    = $zippy->utilities->datetime->getNow();

        return empty($status) || (isset($status['expire']) && $now->format('U') > $status['expire']);
    }

    private function _checkTransientStatus($object)
    {
        $zippy = Zippy::instance();
        
        $status = get_post_meta($object->getId(), 'clickbank_order_status', true);
        $now    = $zippy->utilities->datetime->getNow();

        if (!empty($status) &&
            isset($status['expire']) &&
            $now->format('U') < $status['expire'] &&
            isset($status['active']) && $status['active'] == 'yes'
        ) {
            return true;
        }

        return false;
    }

    private function _handleOrderAccess($object)
    {
        if (($status = $this->_checkTransientStatus($object)) === true) {
            return true;
        }

        $access = true;

        $order_id = get_post_meta($object->getId(), 'clickbank_order_id', true);

        $api = new ZippyCourses_ClickBank_PaymentGatewayAPI;
        $vendor_order = $api->getOrder($order_id);

        if (!is_wp_error($access)) {
            if (is_array($vendor_order)) {
                foreach ($vendor_order as $vo) {
                    $access = $this->_checkOrderAccess($vo);

                    if (!$access) {
                        break;
                    }
                }
            } else {
                $access = $this->_checkOrderAccess($vendor_order);
            }
        }

        return $access;
    }

    private function _checkOrderAccess($vendor_order)
    {
        $status         = $vendor_order->recurring == 'false' ? $vendor_order->txnType : $vendor_order->status;
        $valid_statuses = array('ACTIVE', 'COMPLETED', 'SALE', 'BILL', 'TEST_SALE', 'TEST_BILL');

        return in_array($status, $valid_statuses);
    }

    private function _handleRecurringAccess($object)
    {
        if (($status = $this->_checkTransientStatus($object)) === true) {
            return true;
        }

        $access = true;

        if (!$object->transactions->count()) {
            $object->fetchTransactions();
        }

        $max_checks = $object->transactions->count() > 3 ? 3 : $object->transactions->count();

        $i = 0;
        foreach ($object->transactions->all() as $transaction) {
            $access         = $this->_handleTransactionAccess($transaction);

            if (!$access || $i >= $max_checks) {
                break;
            }

            $i++;
        }

        return $access;
    }

    private function _handleSinglePaymentAccess($object)
    {
        if (($status = $this->_checkTransientStatus($object)) === true) {
            return true;
        }

        if (!$object->transactions->count()) {
            $object->fetchTransactions();
        }

        $transaction = $object->transactions->first();

        return $transaction !== null ? $this->_handleTransactionAccess($transaction) : false;
    }
}
