<?php

class ZippyCourses_ClickBankGateway_Utilities
{
    public function getProductsList()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = %s", 'clickbank_product_id');
        $used_ids = $wpdb->get_col($sql);

        $api = new ZippyCourses_ClickBank_PaymentGatewayAPI;

        $output = array();

        $products = $api->getProducts();

        foreach ($products as $product) {
            $output[$product->{'@sku'}] = array(
                'id'    => $product->{'@sku'},
                'name'  => $product->title,
                'used'  => in_array($product->{'@sku'}, $used_ids)
            );
        }

        return $output;
    }
}
