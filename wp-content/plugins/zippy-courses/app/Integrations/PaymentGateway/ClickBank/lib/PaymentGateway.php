<?php

class ZippyCourses_ClickBank_PaymentGateway extends Zippy_PaymentGateway
{
    public $id      = 'clickbank';
    public $name    = 'ClickBank';

    public function __construct()
    {
        parent::__construct();
    }

    public function setup()
    {
    }

    public function register()
    {
    }

    public function registerListener()
    {
        $this->listener = new ZippyCourses_ClickBank_PaymentGatewayListener($this->id);
        $this->listener->register();
    }

    public function getId()
    {
        return $this->id;
    }
}
