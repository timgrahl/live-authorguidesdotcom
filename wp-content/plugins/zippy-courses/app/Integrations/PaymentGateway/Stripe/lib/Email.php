<?php

class Zippy_Stripe_Email
{
    protected $title;
    protected $content;
    protected $headers;
    protected $recipient;

    public function __construct()
    {
        $this->setup();
    }
    public function sendPaymentFailureNotification($data)
    {
        $this->title = __('Notice of Stripe Payment Failure in your Zippy Courses', ZippyCourses::TEXTDOMAIN);
        $this->content = $this->getPaymentFailureNotificationContent($data);
        $this->recipient = get_bloginfo('admin_email');
        $this->send();

    }
    public function send()
    {
        $zippy = Zippy::instance();
        $sent = wp_mail(
            $this->recipient,
            $this->title,
            $this->content,
            $this->headers
        );

        if ($sent) {
            $zippy->log('Sent "' . $this->title . '" to ' . get_bloginfo('admin_email'), 'EMAIL_SENT');
            // $zippy->log('Sent "' . $this->content . '" to ' . get_bloginfo('admin_email'), 'EMAIL_SENT');
        } else {
            $zippy->log('Failed sending "' . $this->title . '" to ' . get_bloginfo('admin_email'), 'EMAIL_ERROR');
        }
    }
    protected function setup()
    {
        $this->setupHeaders();
    }

    private function setupHeaders()
    {
        $option = get_option('zippy_system_emails_general_settings', array());

        $email        = isset($option['sender_email']) && !empty($option['sender_email'])
                            ? $option['sender_email']
                            : get_bloginfo('admin_email');

        $name         = isset($option['sender_name']) && !empty($option['sender_name'])
                            ? $option['sender_name']
                            : get_bloginfo('blogname');

        $this->headers = array('email' => $email, 'name' => $name);
    }

    protected function getPaymentFailureNotificationContent($data)
    {
        $zippy = Zippy::instance();
        $order = $data['order'];
        $student_id = $order->getOwner();
        $student = $zippy->make('student', array($student_id));
        $student->fetch();
        $order_owner_id = $student->getId();
        $product_id = $order->getProduct()->getId();
        $timestamp = $data['timestamp'];

        $name = $student->getFullName();
        $email = $student->getEmail();
        $product_title = get_the_title($product_id);
        $student_profile_link = get_edit_post_link($student->getId());
        $stripe_payment_link = get_admin_url(null, 'admin.php?page=zippy-settings&tab=zippy_settings_payment');
        $payment_date = $timestamp->format('m-d-Y');

        $content = __('This message is to notify you that a payment in Stripe for your Zippy Course has failed.') . "\n";
        $content .= __('The details of this failed payment are below:', ZippyCourses::TEXTDOMAIN) . "\n";
        $content .= __('Name: ', ZippyCourses::TEXTDOMAIN) . $name . "\n";
        $content .= __('Product: ', ZippyCourses::TEXTDOMAIN) . $product_title . "\n";
        $content .= __('Date of Failed Payment: ', ZippyCourses::TEXTDOMAIN) . $payment_date . "\n";

        $content  .= __('This students access to your course has <em>not</em> been revoked. To view this student\'s profile in your site, click the link below:', ZippyCourses::TEXTDOMAIN) . "\n";

        $content .= get_admin_url(null, 'admin.php?page=zippy-student&ID=' . $order_owner_id) . "\n";
        
        $content .= __('Regards,', ZippyCourses::TEXTDOMAIN) . "\n";
        $content .= __('Zippy Courses', ZippyCourses::TEXTDOMAIN) . "\n" . "\n";

        $content .= __('P.S. Don\'t want to receive these notifications? You can disable them at any time in your', ZippyCourses::TEXTDOMAIN);
        $content .= '<a href="' . $stripe_payment_link . '">' . __('Zippy Courses Stripe Payment Settings', ZippyCourses::TEXTDOMAIN) . '</a>';


        return $content;
    }
}
