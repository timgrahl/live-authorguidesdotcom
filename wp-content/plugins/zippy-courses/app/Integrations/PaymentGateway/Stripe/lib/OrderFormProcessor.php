<?php
/*
if (!class_exists('Stripe')) {
    $stripe_path = ZippyCourses_Stripe_PaymentGatewayIntegration::path() . 'vendor/stripe-php-1.18.0/lib/Stripe.php';
    require_once($stripe_path);
}
*/

class ZippyCourses_Stripe_OrderFormProcessor extends ZippyCourses_Order_FormProcessor
{
    /**
     * Transaction
     * @var Zippy_Transaction
     */
    protected $transaction;

    public function fail()
    {
        $zippy = Zippy::instance();
        $zippy->session->flashMessage('There was an error processing your request', 'error');
    }

    public function execute()
    {
        $zippy = Zippy::instance();

        $token = $_POST['stripeToken'];
        $product = $zippy->make('product', array('id' => $_POST['product_id']));

        $full_name = filter_input(INPUT_POST, 'name');
        $email = filter_input(INPUT_POST, 'email');

        $customer = $zippy->make('customer');
        $customer->importData(array(
            'full_name' => trim($full_name),
            'email'     => trim($email),
        ));


        
        $transaction = $zippy->make('transaction');

        $transaction->setProduct($product);
        $transaction->setType($product->getType());
        $transaction->setStatus('pending');
        $transaction->setGateway('stripe');
        $transaction->setCurrency($zippy->utilities->cart->getCurrency());
        $transaction->setCustomer($customer);
        $transaction->save();

        if (!empty($_POST['billing_address_line_1'])) {
            $address_data = array(
                'billing_address_line_1' => filter_input(INPUT_POST, 'billing_address_line_1'),
                'billing_address_line_2' => filter_input(INPUT_POST, 'billing_address_line_2'),
                'billing_address_city' => filter_input(INPUT_POST, 'billing_address_city'),
                'billing_address_state' => filter_input(INPUT_POST, 'billing_address_state'),
                'billing_address_zip' => filter_input(INPUT_POST, 'billing_address_zip'),
                'billing_address_country' => filter_input(INPUT_POST, 'billing_address_country')

                );
            update_post_meta($transaction->getId(), 'stripe_customer_billing_address', $address_data);
        }

        $this->chargeTransaction($transaction, $token);
    }

    private function chargeTransaction(Zippy_Transaction $transaction, $token = '')
    {
        switch ($transaction->getType()) {
            case 'single':
                $this->chargeSinglePayment($transaction, $token);
                break;
            case 'subscription':
            case 'payment-plan':
                $this->chargeSubscription($transaction, $token);
                break;
            default:
                break;
        }
    }

    private function chargeSinglePayment(Zippy_Transaction $transaction, $token = '')
    {
        $zippy = Zippy::instance();

        $api = new ZippyCourses_Stripe_PaymentGatewayAPI;

        $customer       = $api->createCustomer($transaction->getCustomer());
        if (is_wp_error($customer)) {
            $zippy->log('Could not create customer while processing Transaction #' . $transaction->getId() . '. Reason: ' . $customer->get_error_message(), 'STRIPE_ERROR');

            $error_message  = sprintf(__('%s You have not been charged. If this problem persists, please <a href="mailto:%s">contact us</a>.', ZippyCourses::TEXTDOMAIN), $customer->get_error_message(), $zippy->utilities->email->getSystemEmailAddress());

            $zippy->sessions->flashMessage($error_message, 'error', 0);
            return;
        }

        $card           = $api->addCardToCustomer($token, $customer->id);
        if (is_wp_error($card)) {
            $zippy->log('Could add card to customer while processing Transaction #' . $transaction->getId() . '. Reason: ' . $card->get_error_message(), 'STRIPE_ERROR');
            
            $error_message  = sprintf(__('%s You have not been charged. If this problem persists, please <a href="mailto:%s">contact us</a>.', ZippyCourses::TEXTDOMAIN), $card->get_error_message(), $zippy->utilities->email->getSystemEmailAddress());
            $zippy->sessions->flashMessage($error_message, 'error', 0);
            return;
        }

        $charge = $api->charge($transaction, $customer, $card, $token);

        if (!is_wp_error($charge)) {
            $event = new ZippyCourses_TransactionNotification_Event('stripe');

            $raw = new stdClass;
            $raw->type = $charge->object . '.' . $charge->status;
            $raw->data = new stdClass;
            $raw->data->object = $charge;

            $event->raw_post = json_encode($raw);

            $zippy->events->fire($event);

            $this->redirect($transaction);
        } else {
            $zippy->log('Could not charge card while processing Transaction #' . $transaction->getId() . '. Reason: ' . $charge->get_error_message(), 'STRIPE_ERROR');
            
            $error_message  = sprintf(__('%s You have not been charged. If this problem persists, please <a href="mailto:%s">contact us</a>.', ZippyCourses::TEXTDOMAIN), $charge->get_error_message(), $zippy->utilities->email->getSystemEmailAddress());
            $zippy->sessions->flashMessage($error_message, 'error', 0);
            return;
        }
    }

    private function chargeSubscription(Zippy_Transaction $transaction, $token = '')
    {
        $zippy = Zippy::instance();

        $api            = new ZippyCourses_Stripe_PaymentGatewayAPI;

        $settings       = get_option('zippy_stripe_payment_gateway_settings', array());
        $mode           = isset($settings['test_mode']) && $settings['test_mode'] == '0' ? true : false;
        $product        = $transaction->getProduct();
        $plans          = $zippy->utilities->getJsonMeta($product->getId(), 'stripe_plans', true);
        $plan_type      = $transaction->getType() == 'subscription' ? 'subscription' : 'payment_plan';

        if (!is_object($plans) || !isset($plans->{$plan_type}) || (isset($plans->{$plan_type}->livemode) && $plans->{$plan_type}->livemode !== $mode)) {
            $product->fetchPricing();
            $plans = $this->createPlans($product);
        }

        $plan           = is_object($plans) && isset($plans->{$plan_type}) ? $plans->{$plan_type} : false;

        $error_message  = sprintf(__('Your payment could not be completed at this time. You have not been charged. If this problem persists, please <a href="mailto:%s">contact us</a>.', ZippyCourses::TEXTDOMAIN), $zippy->utilities->email->getSystemEmailAddress());

        if ($plan === false) {
            $zippy->log('Could not create customer while processing Transaction #' . $transaction->getId() . '. Reason: Missing Stripe Plan', 'STRIPE_ERROR');
            $zippy->sessions->flashMessage($error_message, 'error', 0);
        }

        $customer       = $api->createCustomer($transaction->getCustomer());
        if (is_wp_error($customer)) {
            $zippy->log('Could not create customer while processing Transaction #' . $transaction->getId() . '. Reason: ' . $customer->get_error_message(), 'STRIPE_ERROR');
            $zippy->sessions->flashMessage($error_message, 'error', 0);
            return;
        }

        $card           = $api->addCardToCustomer($token, $customer->id);
        if (is_wp_error($card)) {
            $zippy->log('Could not create customer while processing Transaction #' . $transaction->getId() . '. Reason: ' . $card->get_error_message(), 'STRIPE_ERROR');
            $zippy->sessions->flashMessage($error_message, 'error', 0);
            return;
        }

        $subscription   = $api->createSubscription($transaction->getKey(), $customer->id, $plan->id);
        if (is_wp_error($subscription)) {
            $zippy->log('Could not create customer while processing Transaction #' . $transaction->getId() . '. Reason: ' . $subscription->get_error_message(), 'STRIPE_ERROR');
            $zippy->sessions->flashMessage($error_message, 'error', 0);
            return;
        }

        $raw = new stdClass;
        $raw->type = 'customer.subscription.created';
        $raw->data = new stdClass;
        $raw->data->object = $subscription;

        $event = new ZippyCourses_TransactionNotification_Event('stripe');
        $event->raw_post = json_encode($raw);

        $zippy->events->fire($event);

        $this->redirect($transaction);
    }

    private function getFrequencyCode($product)
    {
        switch ($product->getFrequency()) {
            case 'day':
                return 'D';
                break;
            case 'week':
                return 'W';
                break;
            case 'month':
            default:
                return 'M';
                break;
            case 'year':
                return 'Y';
                break;
        }
    }

    public function createPlans(Zippy_Product $product)
    {
        $zippy = Zippy::instance();
    
        $api            = $this->api = new ZippyCourses_Stripe_PaymentGatewayAPI;
        $plans          = $zippy->utilities->getJsonMeta($product->getId(), 'stripe_plans', true);
        $plans          = is_object($plans) ? $plans : new stdClass;

        $valid_types    = array('free', 'single', 'subscription', 'payment-plan');
        $type           = in_array($product->getType(), $valid_types) ? $product->getType() : false;
        $key            = in_array($product->getType(), $valid_types) ? $product->getType() : false;
        if ($key == 'payment-plan') {
            $key = 'payment_plan';
        }

        $settings       = get_option('zippy_stripe_payment_gateway_settings', array());
        $mode           = isset($settings['test_mode']) && $settings['test_mode'] == '0' ? true : false;

        if ($type != 'subscription' && $type != 'payment-plan') {
            return;
        }

        $needs_new_plan = false;
        if (!isset($plans->{$key}) ||
            (isset($plans->{$key}->livemode) && $plans->{$key}->livemode !== $mode)
        ) {
            $needs_new_plan = true;
        }

        if ($needs_new_plan) {
            $plan = $api->createPlan($product);
            $plans->{$key} = $plan;
            
            update_post_meta($product->getId(), 'stripe_plans', json_encode($plans));
        }

        return $plans;
    }

    public function redirect(Zippy_Transaction $transaction)
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');


        if ($student === null) {
            wp_redirect($zippy->core_pages->getUrl('thank_you') . '?transaction_key=' . $transaction->getKey());
            exit;
        } else {
            $zippy->access->claim($student, $transaction);

            wp_redirect($zippy->core_pages->getUrl('dashboard'));
            exit;
        }
    }
}
