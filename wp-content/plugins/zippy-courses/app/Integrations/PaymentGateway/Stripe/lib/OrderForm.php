<?php

class ZippyCourses_Stripe_OrderForm extends Zippy_OrderForm
{
    public function defaultFields()
    {
        parent::defaultFields();
        $settings = get_option('zippy_stripe_payment_gateway_settings');
        $billing_address   = isset($settings['billing_address']) ? (bool) $settings['billing_address'] : false;

        $this->addClass('zippy-stripe-payment-form');

        $this->addTextField('name', __('Name on Card', ZippyCourses::TEXTDOMAIN))
            ->addData('stripe', 'name');
            
        $this->addTextField('email', __('Email', ZippyCourses::TEXTDOMAIN));

        $this->addTextField('cc', __('Card Number', ZippyCourses::TEXTDOMAIN))
             ->addData('stripe', 'number')
             ->hideName();

        $this->addTextField('cvc', __('CVC', ZippyCourses::TEXTDOMAIN))
             ->addData('stripe', 'cvc')
             ->addContainerClass('zippy-input-small zippy-input-left')
             ->hideName();

        $this->addTextField('exp_month', __('Exp. Month', ZippyCourses::TEXTDOMAIN))
             ->addData('stripe', 'exp-month')
             ->setPlaceholder('MM')
             ->addContainerClass('zippy-input-small zippy-input-left zippy-input-clear')
             ->hideName();
             
        $this->addTextField('exp_year', __('Exp. Year', ZippyCourses::TEXTDOMAIN))
             ->addData('stripe', 'exp-year')
             ->setPlaceholder('YYYY')
             ->addContainerClass('zippy-input-small zippy-input-left')
             ->hideName();

        if ($billing_address) {
            $this->_addBillingAddressFields();
        }

        do_action('zippy_courses_stripe_payment_form_fields', $this);

        $this->addHiddenField('method')->setValue('stripe');
    }
    
    private function _addBillingAddressFields()
    {
        $zippy = Zippy::instance();
        $countries = $zippy->utilities->product->getCountryArray();

        $this->addHtml('billing_title', '<h3>' . __('Billing Address', ZippyCourses::TEXTDOMAIN) . '</h3>');
        $this->addTextField('billing_address_line_1', __('Address Line 1', ZippyCourses::TEXTDOMAIN))
             ->addData('stripe', 'address-line1');
        $this->addTextField('billing_address_line_2', __('Address Line 2', ZippyCourses::TEXTDOMAIN))
             ->addData('stripe', 'address-line2');

        $this->addTextField('billing_address_city', __('City', ZippyCourses::TEXTDOMAIN))
             ->addData('stripe', 'address-city');

        $this->addTextField('billing_address_state', __('State/Province', ZippyCourses::TEXTDOMAIN))
             ->addData('stripe', 'address-state')
             ->addContainerClass('zippy-input-small zippy-input-left zippy-input-clear');

        $this->addTextField('billing_address_zip', __('ZIP', ZippyCourses::TEXTDOMAIN))
             ->addData('stripe', 'address_zip')
             ->addContainerClass('zippy-input-small zippy-input-left');
        $this->addSelectField('billing_address_country', __('Billing Country', ZippyCourses::TEXTDOMAIN), $countries)
            ->addData('stripe', 'address-country');
    }

    public function afterSetup()
    {
        $zippy = Zippy::instance();

        $settings = get_option('zippy_stripe_payment_gateway_settings');
        $paypal   = isset($settings['paypal']) ? (bool) $settings['paypal'] : false;

        if ($paypal) {
            $product = $this->getProduct();

            $url = get_permalink($product->getId()) . '?buy-with=paypal';

            $msg = '<a href="' . $url . '" class="zippy-button">' . __('Pay with PayPal', ZippyCourses::TEXTDOMAIN) . '</a>';

            $this->setAfter($msg);
        }
    }
}
