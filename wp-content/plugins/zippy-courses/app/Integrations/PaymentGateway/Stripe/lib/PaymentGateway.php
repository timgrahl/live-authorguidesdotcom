<?php

class ZippyCourses_Stripe_PaymentGateway extends Zippy_PaymentGateway
{
    public $id      = 'stripe';
    public $name    = 'Stripe';

    public function __construct()
    {
        parent::__construct();
    }

    public function setup()
    {
    }

    public function register()
    {
    }

    public function registerListener()
    {
        $this->listener = new ZippyCourses_Stripe_PaymentGatewayListener($this->id);
        $this->listener->register();
    }

    public function getId()
    {
        return $this->id;
    }
}
