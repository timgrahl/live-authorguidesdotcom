<?php

class ZippyCourses_Stripe_PaymentGatewayListener extends Zippy_PaymentGatewayListener
{
    public function register()
    {
        parent::register();

        $zippy = Zippy::instance();

        $zippy->events->register('OrderStatusChange', $this);
        $zippy->events->register('SaveTransaction', $this);
        $zippy->events->register('OrderSave', $this);
        $zippy->events->register('NewOrder', $this);

    }

    public function handle(Zippy_Event $event)
    {

        switch ($event->getEventName()) {
            case 'TransactionNotification':
                if ($this->validateGateway($event)) {
                    $this->process($event);
                }
                break;
            case 'OrderStatusChange':
                $this->_handleOrderCompletion($event);
                break;
            case 'OrderSave':
                break;
            case 'SaveTransaction':
                $this->_handleTransactionSave($event);
                break;
            case 'NewOrder':
                $this->_handleNewOrder($event);
            default:
                break;
        }
    }

    private function _handleTransactionSave(Zippy_Event $event)
    {
        $transaction = $event->transaction;

        $vendor = get_post_meta($transaction->getId(), 'vendor', true);
        $details = get_post_meta($transaction->getId(), 'details', true);

        if ($vendor !== 'stripe' || !is_array($details) || !isset($details['raw'])) {
            return;
        }

        $json = json_decode($details['raw']);
        $stripe_customer_id = false;

        if (isset($json->customer)) {
            $stripe_customer_id = $json->customer;
        }

        if ($stripe_customer_id) {
            update_post_meta($transaction->getId(), 'stripe_customer_id', $stripe_customer_id);
        }
    }

    private function _handleOrderCompletion(Zippy_Event $event)
    {
        $order          = $event->order;
        $status         = $event->new_status;

        if ($order->getType() == 'payment-plan' && $status == 'complete') {
            if (!$order->transactions->count()) {
                $order->fetchTransactions();
            }

            $transaction        = $order->transactions->first();
            $customer_id        = get_post_meta($transaction->getId(), 'stripe_customer_id', true);
            $subscription_id    = get_post_meta($transaction->getId(), 'recurring_id', true);

            if (!empty($customer_id) && !empty($subscription_id)) {
                $api = new ZippyCourses_Stripe_PaymentGatewayAPI;
                $cancel = $api->cancelSubscription($customer_id, $subscription_id);

                if (is_wp_error($cancel)) {
                }
            }
        }
    }
    protected function _handleNewOrder(Zippy_Event $event)
    {

        $zippy = Zippy::instance();
        $order              = $event->order;
        $transaction        = $order->transactions->first();

        // Look for a Billing Address, and save the data as a note if it exists
        $billing_address    = get_post_meta($transaction->getId(), 'stripe_customer_billing_address', true);

        if (!empty($billing_address)) {
                $this->_saveBillingAddressNote($order, $billing_address);
        }
    }

    protected function _saveBillingAddressNote(Zippy_Order $order, $billing_address)
    {
            $billing_address_line_1 = !empty($billing_address['billing_address_line_1']) ? $billing_address['billing_address_line_1'] : null;
            $billing_address_line_2 = !empty($billing_address['billing_address_line_2']) ? $billing_address['billing_address_line_2'] : null;
            $billing_address_city = !empty($billing_address['billing_address_city']) ? $billing_address['billing_address_city'] : null;
            $billing_address_state = !empty($billing_address['billing_address_state']) ? $billing_address['billing_address_state'] : null;
            $billing_address_zip = !empty($billing_address['billing_address_zip']) ? $billing_address['billing_address_zip'] : null;
            
            $note = '<strong>Billing Address</strong> <br>';
            $note .= $billing_address_line_1 . '<br>';
            $note .= $billing_address_line_2 ? $billing_address_line_2 . '<br>' : null;
            $note .= $billing_address_city . ', ' . $billing_address_state . '<br>';
            $note .= $billing_address_zip;
            $order->addNote(array(
                'content'   => $note,
                'timestamp' => time()
                ) 
            ); 
            $order->saveNotes();
    }

    protected function process(Zippy_Event $event)
    {
        global $zippy_transaction_key;

        $zippy = Zippy::instance();

        $event_data = $this->parseRawPostdata($event->raw_post);
        switch ($event_data->type) {
            case 'charge.succeeded':
            case 'charge.paid':
            case 'charge.refunded':
                $transaction = $this->_processChargeNotification($event);
                break;
            case 'invoice.payment_succeeded':
                $transaction = $this->_processInvoiceNotification($event);
                break;
            case 'invoice.payment_failed':
                $transaction = null;
                $this->_sendPaymentFailureNotification($event);
                break;
            case 'customer.subscription.created':
            case 'customer.subscription.deleted':
                $transaction = $this->_processSubscriptionNotification($event);
                break;
            default:
                $transaction = null;
                break;
        }

        if ($transaction) {
            // Set our global for use in filters, etc.
            $zippy_transaction_key = $transaction->getKey();
            $zippy->sessions->store('zippy_transaction_key', $zippy_transaction_key);
        } else {
            $zippy->sessions->delete('zippy_transaction_key');
        }
    }

    private function _processTransactionData($data)
    {
        $zippy = Zippy::instance();

        // Prepare a fresh transaction object
        $transaction = $zippy->make('transaction');

        // If we've got a transaction key from the adapter, then use it to fetch the
        // existing transaction.
        if ($data['key']) {
            $transaction->buildByTransactionKey($data['key']);
        }
    
        // Import the IPN data into the transaction and save.
        $transaction->importData($data);

        $transaction->save();

        return $transaction;
    }

    private function _processChargeNotification(Zippy_Event $event)
    {
        global $wpdb;

        $event_data = $this->parseRawPostdata($event->raw_post);

        if (!isset($event_data->data->object->metadata->zippy_transaction_key)) {
            $id = $event_data->data->object->id;

            $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'vendor_id', $id);

            $transaction_id = $wpdb->get_var($sql);
            if (empty($transaction_id)) {
                return null;
            }

            $transaction_key = get_post_meta($transaction_id, 'transaction_key', true);
            if (empty($transaction_key)) {
                return null;
            }

            if (!isset($event_data->data->object->metadata) || !is_object($event_data->data->object->metadata)) {
                $event_data->data->object->metadata = new stdClass;
            }

            $event_data->data->object->metadata->zippy_transaction_key = $transaction_key;
        }

        $adapter    = new ZippyCourses_StripeCharge_PaymentGatewayTransactionAdapter($event_data->data->object);
        return $this->_processTransactionData($adapter->getNormalizedData());
    }

    private function _processInvoiceNotification(Zippy_Event $event)
    {
        $event_data = $this->parseRawPostdata($event->raw_post);
        $line_data  = $event_data->data->object->lines->data;
        $charge     = reset($line_data);

        // We are only handling charges with a Transaction Key from Zippy Courses attached to it.
        if (!isset($charge->metadata->zippy_transaction_key)) {
            return null;
        }

        $adapter    = new ZippyCourses_StripeInvoice_PaymentGatewayTransactionAdapter($event_data->data->object);


        return $this->_processTransactionData($adapter->getNormalizedData());
    }

    private function _sendPaymentFailureNotification(Zippy_Event $event)
    {
        $zippy = Zippy::instance();

        $settings = get_option('zippy_stripe_payment_gateway_settings');
        $send_notification   = isset($settings['payment_failure_notification']) ? (bool) $settings['payment_failure_notification'] : true;

        if (!$send_notification) {
            return null;
        }

        $event_data = $this->parseRawPostdata($event->raw_post);
        $line_data  = $event_data->data->object->lines->data;
        $charge     = reset($line_data);
        
        if (!isset($charge->metadata->zippy_transaction_key)) {
            return null;
        }
        

        $adapter    = new ZippyCourses_StripeInvoice_PaymentGatewayTransactionAdapter($event_data->data->object);
        $email = new Zippy_Stripe_Email;
        $email->sendPaymentFailureNotification($adapter->getNormalizedData());
    }


    private function _processSubscriptionNotification(Zippy_Event $event)
    {
        $event_data = $this->parseRawPostdata($event->raw_post);

        // We are only handling charges with a Transaction Key from Zippy Courses attached to it.
        if (!isset($event_data->data->object->metadata->zippy_transaction_key)) {
            return null;
        }

        $adapter    = new ZippyCourses_StripeSubscription_PaymentGatewayTransactionAdapter($event_data->data->object);
        return $this->_processTransactionData($adapter->getNormalizedData());
    }


    /**
     * Receive raw post data from Stripe and decode the JSON
     *
     * @since   1.0.0
     *
     * @param   JSON $input php://input stream
     *
     * @return  stdClass
     */
    private function parseRawPostdata($input)
    {
        return json_decode($input);
    }

    /**
     * Validate that the IPN is for Stripe
     *
     * @param  ZippyCourses_TransactionNotification_Event $event
     *
     * @return bool
     */
    protected function validateGateway(Zippy_Event $event)
    {
        return $event->gateway == 'stripe';
    }
}
