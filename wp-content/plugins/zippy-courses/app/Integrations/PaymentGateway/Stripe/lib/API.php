<?php

/**
 * Stripe Gateway for Zippy Courses.  Using this API is useful because it does not require CURL as the official
 * library does, and it also uses wp_remote_post to make remote requests.
 *
 * The primary downside is that not everything is a particular type of Stripe Object as the official SDK is,
 * but the functionality is leaner.
 */
class ZippyCourses_Stripe_PaymentGatewayAPI extends Zippy_PaymentGatewayAPI
{
    private $secret_key;
    private $publishable_key;
    private $base_url = 'https://api.stripe.com/v1/';

    public function __construct()
    {
        $options                = get_option('zippy_stripe_payment_gateway_settings');
        $this->secret_key       = isset($options['secret_key']) ? $options['secret_key'] : '';
        $this->publishable_key  = isset($options['publishable_key']) ? $options['publishable_key'] : '';
    }

    /**
     * Process a Stripe Charge
     *
     * @since   1.0.0
     *
     * @todo    Implement Idempotent handleResponses
     *
     * @param   Zippy_Transaction $transaction
     * @param   string            $token       Stripe Token as generated on front end
     *
     * @see     https://stripe.com/docs/api/curl#create_charge
     *
     * @return  void
     */
    public function charge(Zippy_Transaction $transaction, $customer, $card)
    {
        $url = $this->getUrl('charges');
        $product = $transaction->getProduct();

        $params = $this->buildParams(array(
            'amount'        => $transaction->getTotal() * 100,
            'currency'      => strtolower($transaction->getCurrency()),
            'customer'      => $customer->id,
            'source'        => $card->id,
            'description'   => 'Charge for Product: ' . $product->getTitle(),
            'metadata'      => array(
                'zippy_transaction_key' => $transaction->getKey(),
                'name'                  => $transaction->getCustomer()->getFullName(),
                'email'                 => $transaction->getCustomer()->getEmail(),
            )
        ), 'POST');

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Get the charge from Stripe
     *
     * @since   1.0.0
     *
     * @param   string $id
     *
     * @see     https://stripe.com/docs/api/curl#retrieve_charge
     *
     * @return  JSON|WP_Error
     */
    public function getCharge($id)
    {
        $url = $this->getUrl("charges/$id");
        $params = $this->buildParams(array());

        return $this->handleResponse(wp_remote_post($url, $params));
    }
    
    /**
     * Get the Event from Stripe, used for validating Webhooks
     *
     * @since   1.0.0
     *
     * @param   string $id
     *
     * @see     https://stripe.com/docs/api/curl#retrieve_event
     *
     * @return  JSON|WP_Error
     */
    public function getEvent($id)
    {
        $url = $this->getUrl("events/$id");
        $params = $this->buildParams(array());

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Create a new Stripe Customer
     *
     * @since   1.0.0
     *
     * @param   Zippy_Customer  $customer
     * @param   string          $source     Stripe Token or Card Data
     *
     * @see     https://stripe.com/docs/api/curl#create_customer
     *
     * @return  JSON|WP_Error
     */
    public function createCustomer(Zippy_Customer $customer, $source = '')
    {
        $url = $this->getUrl("customers");

        $params = $this->buildParams(array(
            'email'         => $customer->getEmail(),
            'description'   => $customer->getFullName(),
            'metadata'      => array()
        ), 'POST');

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    public function fetchCustomer(Zippy_Customer $customer)
    {

    }
    
    /**
     * Create a new Card source in Stripe for a customer
     *
     * @since   1.0.0
     *
     * @param   string  $customer_id    Stripe Customer ID
     * @param   string  $source         Stripe Source, such as a token or card info array
     *
     * @see     https://stripe.com/docs/api/curl#create_card
     *
     * @return  JSON|WP_Error
     */
    public function createCard($card)
    {
        $url = $this->getUrl("tokens");
        $params = $this->buildParams(array(
            'card' => $card
        ), 'POST');

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Add a token to an existing customer object
     *
     * @since   1.0.0
     *
     * @param   string  $token_id       Stripe Token ID
     * @param   string  $customer_id    Stripe Customer ID
     *
     * @see     https://stripe.com/docs/api/curl#create_card
     *
     * @return  JSON|WP_Error
     */
    public function addCardToCustomer($token_id, $customer_id)
    {
        $url = $this->getUrl("customers/{$customer_id}/sources");
        
        $params = $this->buildParams(array(
            'source' => $token_id
        ), 'POST');

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Create a Plan in Stripe that can later be attached as a Subscription
     * for a Product.
     *
     * NOTE: Stripe Plans are separate from the Zippy definition of Payment Plans.
     * Plans are basically a "template" for a Subscription or Payment Plan.
     *
     * @since   1.0.0
     *
     * @param   Zippy_Product $product
     *
     * @see     https://stripe.com/docs/api/curl#create_plan
     *
     * @return  JSON|WP_Error
     */
    public function createPlan(Zippy_Product $product)
    {
        $url = $this->getUrl("plans");

        $params = $this->buildParams(array(
            'amount'    => $product->getAmount() * 100,
            'interval'  => $product->getFrequency(),
            'name'      => $product->getTitle(),
            'id'        => $this->generatePlanId($product),
            'currency'  => strtolower($product->getCurrency())
        ), 'POST');

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Create a new Subscription for a customer in Stripe
     *
     * @since   1.0.0
     *
     * @param   string  $customer_id    Stripe Customer ID
     * @param   string  $plan_id        Stripe Plan ID
     *
     * @see     https://stripe.com/docs/api/curl#create_subscription
     *
     * @return  JSON|WP_Error
     */
    public function createSubscription($transaction_key, $customer_id, $plan_id)
    {
        $url = $this->getUrl("customers/$customer_id/subscriptions");
        $params = $this->buildParams(array(
            'plan' => $plan_id,
            'metadata' => array(
                'zippy_transaction_key' => $transaction_key
            )
        ), 'POST');

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Cancel an existing Stripe Subscription for a Customer
     *
     * @since   1.0.0
     *
     * @param   string  $customer_id        Stripe Customer ID
     * @param   string  $subscription_id    Stripe Subscription ID
     *
     * @see     https://stripe.com/docs/api/curl#cancel_subscription
     *
     * @return  JSON|WP_Error
     */
    public function cancelSubscription($customer_id, $subscription_id)
    {
        $url = $this->getUrl("customers/$customer_id/subscriptions/$subscription_id");
        $params = $this->buildParams(array(), 'DELETE');

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Handle a wp_remote_post call, either returning a json_decoded version of the body,
     * or WP_Error in the event of a non-200 status code.
     *
     * @since   1.0.0
     *
     * @param   array   $response   The results form a wp_remote_post request
     *
     * @return  JSON|WP_Error
     */
    public function handleResponse($response)
    {
        if (!is_wp_error($response)) {
            if ($response['response']['code'] == 200) {
                $body = json_decode($response['body']);
                return $body;
            } else {
                $body = json_decode($response['body']);
                $error = new WP_Error($body->error->type, $body->error->message, $response);
                return $error;
            }
        } else {
            return $response;
        }
    }

    /**
     * Build the parameters from a default, plus the data sent by the calling method
     *
     * @since   1.0.0
     *
     * @param   array   $body       The parameters being sent in the request body
     * @param   string  $method     The HTTP method
     *
     * @return  JSON|WP_Error
     */
    private function buildParams(array $body = array(), $method = 'GET')
    {
        return array(
            'method'        => $method,
            'timeout'       => 10,
            'redirection'   => 5,
            'httpversion'   => '1.1',
            'blocking'      => true,
            'headers'       => array(
                'Authorization' => "Bearer {$this->secret_key}"
            ),
            'body'          => $body
        );
    }

    /**
     * Basic endpoint builder
     *
     * @since   1.0.0
     *
     * @param   string $endpoint
     *
     * @return  string
     */
    private function getUrl($endpoint)
    {
        return $this->base_url . $endpoint;
    }

    public function generatePlanId(Zippy_Product $product)
    {
        $time = time();
        return "{$product->getId()}-{$product->getType()}-{$time}";
    }
}
