<?php
/**
 * Id: stripe
 * File Type: payment-gateway-integration
 * Class: ZippyCourses_Stripe_PaymentGatewayIntegration
 *
 * @since 1.0.0
 */

class ZippyCourses_Stripe_PaymentGatewayIntegration extends Zippy_PaymentGatewayIntegration
{
    public $id = 'stripe';
    public $service = 'stripe';
    public $name = 'Stripe';
    public $settings = array();
    
    protected $api;

    private $path;
    private $url;

    public function __construct()
    {
        $this->path = plugin_dir_path(__FILE__);
        $this->url  = plugin_dir_url(__FILE__);

        parent::__construct();
    }

    public function setup()
    {
        $this->settings();
        $this->actions();
        $this->filters();

        $this->api = new ZippyCourses_Stripe_PaymentGatewayAPI;
    }

    protected function actions()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        add_action('init', array($this, 'orderForm'));
        add_action('wp_enqueue_scripts', array($this, 'assets'));
        add_action('update_post_meta', array($this, 'createStripePlans'), 10, 4);
        add_action('added_post_meta', array($this, 'createStripePlans'), 10, 4);

        add_action('save_post', array($this, 'createStripePlansOnPostSave'));

        if ($method == $this->service) {
            add_action('admin_init', array($this, 'sslWarning'), 11);
        }
    }

    protected function filters()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_filter('the_content', array($this, 'alerts'));
            add_filter('zippy_active_gateway_configured', array($this, 'checkConfiguration'));
            add_filter('zippy_filter_get_transaction_key', array($this, 'thankYou'));

            add_filter('zippy_courses_payment_method_requires_register_first_redirect', '__return_true');
            add_filter('zippy_checkout_requires_ssl', array($this, 'requireSSL'));
        }
    }

    public function map($classes)
    {
        $classes['ZippyCourses_Stripe_OrderForm']                           = $this->path . 'lib/OrderForm.php';
        $classes['ZippyCourses_Stripe_OrderFormProcessor']                  = $this->path . 'lib/OrderFormProcessor.php';
        $classes['ZippyCourses_Stripe_PaymentGatewayListener']              = $this->path . 'lib/Listener.php';
        $classes['ZippyCourses_StripeCharge_PaymentGatewayTransactionAdapter']    = $this->path . 'lib/ChargeAdapter.php';
        $classes['ZippyCourses_StripeInvoice_PaymentGatewayTransactionAdapter']    = $this->path . 'lib/InvoiceAdapter.php';
        $classes['ZippyCourses_StripeSubscription_PaymentGatewayTransactionAdapter']    = $this->path . 'lib/SubscriptionAdapter.php';
        $classes['ZippyCourses_Stripe_PaymentGatewayAPI']                   = $this->path . 'lib/API.php';
        $classes['ZippyCourses_Stripe_PaymentGateway']                      = $this->path . 'lib/PaymentGateway.php';
        $classes['Zippy_Stripe_Email']                      = $this->path . 'lib/Email.php';

        return $classes;
    }

    public function registerForm()
    {
        $zippy = Zippy::instance();

        $payment_settings = get_option('zippy_payment_general_settings', array());
        $gateway          = isset($payment_settings['method']) ? $payment_settings['method'] : null;

        if ($this->id == $gateway) {
            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_Stripe_OrderForm');
        }
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $integrations = $zippy->make('payment_gateway_integrations');
        $integrations->add($this);

        $gateway    = new ZippyCourses_Stripe_PaymentGateway;
        $gateways   = $zippy->make('payment');
        
        $gateways->add($gateway);

        $this->registerForm();
    }

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    public function settings()
    {
        $zippy = Zippy::instance();

        $settings_pages = $zippy->make('settings_pages_repository');
        $page = $settings_pages->fetch('zippy_settings_payment');

        $section = $page->createSection($this->getSettingsName(), 'Stripe');
            $section->createField('secret_key', __('Secret Key', ZippyCourses::TEXTDOMAIN));
            $section->createField('publishable_key', __('Publishable Key', ZippyCourses::TEXTDOMAIN));

            $section->createField('test_mode', __('Test Mode?', ZippyCourses::TEXTDOMAIN), 'select', array('0' => __('No', ZippyCourses::TEXTDOMAIN), '1' => __('Yes', ZippyCourses::TEXTDOMAIN)));
            $section->createField('paypal', __('Include PayPal?', ZippyCourses::TEXTDOMAIN), 'select', array('0' => __('No', ZippyCourses::TEXTDOMAIN), '1' => __('Yes', ZippyCourses::TEXTDOMAIN)));
            $section->createField('billing_address', __('Require Billing Address?', ZippyCourses::TEXTDOMAIN), 'select', array('0' => __('No', ZippyCourses::TEXTDOMAIN), '1' => __('Yes', ZippyCourses::TEXTDOMAIN)));
            $field = $section->createField(
                'webhooks_description',
                __('Webhook URL: ', ZippyCourses::TEXTDOMAIN),
                'raw',
                $this->getWebhooksDescription()
            );
            $field->setValue($this->getWebhooksDescription());
            $section->createField('payment_failure_notification', __('Email me payment failure notifications?', ZippyCourses::TEXTDOMAIN), 'select', array('1' => __('Yes', ZippyCourses::TEXTDOMAIN), '0' => __('No', ZippyCourses::TEXTDOMAIN)));

    }

    public function getWebhooksDescription()
    {

        $html = null;
        $html.= get_site_url() . '/payment/notification/stripe<br>';
        $html.= '<p><strong>' . __('Note:', ZippyCourses::TEXTDOMAIN) . ' </strong>';
        $html.= __('Stripe requires that you enable Webhooks in order for automatic payments to process properly. ', ZippyCourses::TEXTDOMAIN) . '</a>';
        $html.= __('If you haven\'t set your webhooks, ', ZippyCourses::TEXTDOMAIN);
        $html.=  '<a href="http://docs.zippycourses.com/article/75-creating-your-stripe-webhooks" target="_blank">';
        $html.= __('click here', ZippyCourses::TEXTDOMAIN);
        $html .= '</a> ' . __('to learn how.', ZippyCourses::TEXTDOMAIN);
        
        return $html;
    }


    public function orderForm()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            $zippy = Zippy::instance();

            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_Stripe_OrderForm');
        }
    }

    public function thankYou($key)
    {
        global $zippy_transaction_key;

        $transaction_key = filter_input(INPUT_GET, 'transaction_key');
        if ($transaction_key) {
            $zippy_transaction_key = $transaction_key;
        }

        return $zippy_transaction_key;
    }

    public static function path()
    {
        return plugin_dir_path(__FILE__);
    }

    public static function url()
    {
        return plugin_dir_url(__FILE__);
    }

    public function assets()
    {
        $payment_settings = get_option('zippy_payment_general_settings', array());
        $gateway          = isset($payment_settings['method']) ? $payment_settings['method'] : null;

        if ($this->id == $gateway) {
            $options            = get_option('zippy_stripe_payment_gateway_settings');
            $publishable_key    = isset($options['publishable_key']) ? $options['publishable_key'] : '';
            
            wp_enqueue_script('zippy-stripe', 'https://js.stripe.com/v2/', array('jquery'), ZippyCourses::VERSION, true);
            wp_enqueue_script('zippy-stripe-processor', self::url() . 'assets/js/process-payment-form.js', array('zippy-stripe'), ZippyCourses::VERSION, true);

            wp_localize_script('zippy-stripe-processor', 'ZippyStripe', array(
                'publishable_key' => trim($publishable_key)
            ));
        }
    }

    public function createStripePlansOnPostSave($post_id)
    {
        if (get_post_type($post_id) !== 'product') {
            return;
        }

        $meta_key   = 'pricing';
        $meta_value = get_post_meta($post_id, $meta_key, true);

        $this->createStripePlans(0, $post_id, $meta_key, $meta_value);
    }

    public function createStripePlans($meta_id, $post_id, $meta_key, $_meta_value)
    {
        $zippy = Zippy::instance();
        $method = $zippy->utilities->cart->getActivePaymentMethod();

        if ($method != 'stripe' || $meta_key != 'pricing') {
            return;
        }
        
        $settings       = get_option('zippy_stripe_payment_gateway_settings', array());
        $mode           = isset($settings['test_mode']) && $settings['test_mode'] == '0' ? true : false;

        $plans          = $zippy->utilities->getJsonMeta($post_id, 'stripe_plans', true);
        $plans          = is_object($plans) ? $plans : new stdClass;

        $new_pricing    = $zippy->utilities->maybeJsonDecode($_meta_value);
        $old_pricing    = $zippy->utilities->getJsonMeta($post_id, 'pricing', true);

        $valid_types    = array('free', 'single', 'subscription', 'payment-plan');
        $type           = is_object($new_pricing) && in_array($new_pricing->type, $valid_types) ? $new_pricing->type : false;
        $key            = is_object($new_pricing) && in_array($new_pricing->type, $valid_types) ? $new_pricing->type : false;

        if ($key == 'payment-plan') {is_object($new_pricing) && 
            $key = 'payment_plan';
        }

        if ($type != 'subscription' && $type != 'payment-plan') {
            return;
        }

        $needs_new_plan = false;
        $stripe_plans = get_post_meta($post_id, 'stripe_plans', true);

        if (empty($stripe_plans) ||
            !isset($plans->{$key}) ||
            (isset($plans->{$key}->livemode) && $plans->{$key}->livemode !== $mode) ||
            ($meta_id && ($old_pricing->price->{$key}->amount != $new_pricing->price->{$key}->amount))
        ) {
            $needs_new_plan = true;
        }

        if ($needs_new_plan) {
            $data = $type == 'subscription'
                ? $new_pricing->price->subscription
                : $new_pricing->price->payment_plan;

            $data->num_payments = isset($data->num_payments) ? $data->num_payments : 0;

            $product = $zippy->make('product', array($post_id));
            $product->setFrequency($data->frequency);
            $product->setType($type);
            $product->setNumPayments($data->num_payments);
            $product->setAmount($data->amount);
            
            $plan = $this->api->createPlan($product);
            
            $plans->{$key} = $plan;
            
            update_post_meta($post_id, 'stripe_plans', json_encode($plans));
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function checkConfiguration($configured)
    {
        $settings = get_option('zippy_stripe_payment_gateway_settings');
        
        $publishable_key    = isset($settings['publishable_key']) ? $settings['publishable_key'] : '';
        $secret_key         = isset($settings['secret_key']) ? $settings['secret_key'] : '';

        $configured = !empty($publishable_key) && !empty($secret_key);

        return $configured;
    }

    public function alerts($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || ($post->post_type !== 'product' && !$zippy->core_pages->is($post->ID, 'buy'))) {
            return $content;
        }

        $settings = get_option('zippy_stripe_payment_gateway_settings', array());
        $mode     = isset($settings['test_mode']) && $settings['test_mode'] == '1' ? 'test' : 'live';

        $product = $post->post_type == 'product'
            ? $zippy->make('product', array($post->ID))
            : $zippy->utilities->product->getProductByBuyQueryVar();

        if ($product && $product->getAmount() < '0.50' && $product->getType() != 'free') {
            if (current_user_can('edit_others_posts')) {
                $alert = '<div class="zippy-alert zippy-alert-warning">';
                $alert .= __('This product must cost at least $0.50 to work with Stripe.', ZippyCourses::TEXTDOMAIN);
                $alert .= '</div>';

                return $alert;
            } else {
                $alert = '<div class="zippy-alert zippy-alert-error">';
                    $alert .= __('This product is not currently available for sale.', ZippyCourses::TEXTDOMAIN);
                $alert .= '</div>';

                return $alert;
            }
        }

        if ($mode == 'test') {
            if (current_user_can('edit_others_posts')) {
                $alert = '<div class="zippy-alert zippy-alert-warning">';
                $alert .= __('<strong>NOTICE:</strong> Stripe is currently in Test Mode and will only accept test Credit Card numbers.', ZippyCourses::TEXTDOMAIN);
                $alert .= '</div>';

                return $alert . $content;
            }
        }

        return $content;
    }

    public function sslWarning()
    {
        $zippy = Zippy::instance();
        $notices = $zippy->make('admin_notice_repository');
        $notice = $zippy->make(
            'admin_notice',
            array(
                'id' => 'ssl-not-enabled',
                'message' => __(
                    'You are using Stripe as your payment gateway without having SSL Enabled on your Site. You can use Stripe in Test most with SSL disabled, but live payments may not process correctly through Stripe if SSL is not enabled.',
                    ZippyCourses::TEXTDOMAIN
                ),
                'type' => 'warning'
            )
        );
        if (!is_ssl()) {
            $notices->add($notice);
        }
    }

    public function requireSSL()
    {
        if ($this->isTestModeEnabled()) {
            return false;
        }
        // @todo: We are allowing this to return false for now (but adding a warning)
        // In version 1.4 and later, this needs to be changed to forbid non-SSL connections
        // return true;
        return false;
    }
    public function isTestModeEnabled()
    {
        $settings = get_option('zippy_stripe_payment_gateway_settings', array());
        return isset($settings['test_mode']) && $settings['test_mode'] == '1' ? true : false;

    }
}
