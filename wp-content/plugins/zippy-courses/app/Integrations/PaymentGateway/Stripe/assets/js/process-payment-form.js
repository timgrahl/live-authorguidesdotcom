(function ($) {
    "use strict";

    Stripe.setPublishableKey(ZippyStripe.publishable_key);

    function stripeResponseHandler(status, response) {
        var $form = $('.zippy-order-form');

        if (response.error) {
            // Show the errors on the form
            var $messages = $('<div/>');
            
            $messages
                .addClass('zippy-alert')
                .addClass('zippy-alert-error')
                .text(response.error.message)
                .prependTo($form);

            $form.find('input[type="submit"]').prop('disabled', false);
        } else {
            // response contains id and card, which contains additional card details
            var token = response.id;
            // Insert the token into the form so it gets submitted to the server
            $form.append($('<input type="hidden" name="stripeToken" />').val(token));

            // and submit
            $form.get(0).submit();
        }
    };

    $('.zippy-order-form').submit(function(event) {
        var $form = $(this);
        if (!$form.valid()) {
            return false;
        }
        // Disable the submit button to prevent repeated clicks
        $form.find('input[type="submit"]').prop('disabled', true);
        $form.find('.zippy-alert').remove();
        
        Stripe.card.createToken($form, stripeResponseHandler);

        // Prevent the form from submitting with the default action
        return false;
    });

}(jQuery));