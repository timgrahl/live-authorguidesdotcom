<?php
/**
 * Id: samcart
 * File Type: payment-gateway-integration
 * Class: ZippyCourses_Samcart_PaymentGatewayIntegration
 *
 * @since 1.0.0
 */

class ZippyCourses_Samcart_PaymentGatewayIntegration extends Zippy_PaymentGatewayIntegration
{
    public $id = 'samcart';
    public $service = 'samcart';
    public $name = 'Samcart';
    public $settings = array();
    
    protected $api;

    private $path;

    public function __construct()
    {
        $this->path = plugin_dir_path(__FILE__);
        $this->url  = plugin_dir_url(__FILE__);
        add_filter('zippy_metaboxes', array($this, 'metaboxes'));

        parent::__construct();
    }

    public function setup()
    {
        $this->settings();
        $this->actions();
        $this->filters();
    }

    protected function actions()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_action('init', array($this, 'orderForm'));
            add_action('template_redirect', array($this, 'buyNow'), 11);
        }
        
    }

    protected function filters()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_filter('zippy_active_gateway_configured', array($this, 'checkConfiguration'));
            add_filter('the_content', array($this, 'thankYou'), 11);
            add_filter('zippy_filter_get_transaction_key', array($this, 'setDummyTransactionKey'));
        }
    }

    public function map($classes)
    {
        $classes['ZippyCourses_Samcart_OrderForm']                           = $this->path . 'lib/OrderForm.php';
        $classes['ZippyCourses_Samcart_OrderFormProcessor']                  = $this->path . 'lib/OrderFormProcessor.php';
        $classes['ZippyCourses_Samcart_PaymentGatewayListener']              = $this->path . 'lib/Listener.php';
        $classes['ZippyCourses_Samcart_PaymentGatewayTransactionAdapter']    = $this->path . 'lib/Adapter.php';
        $classes['ZippyCourses_Samcart_PaymentGateway']                      = $this->path . 'lib/PaymentGateway.php';

        return $classes;
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $integrations = $zippy->make('payment_gateway_integrations');
        $integrations->add($this);

        $gateway    = new ZippyCourses_Samcart_PaymentGateway;
        $gateways   = $zippy->make('payment');
        $gateways->add($gateway);
    }

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    public function settings()
    {
        $zippy = Zippy::instance();

        $settings_pages = $zippy->make('settings_pages_repository');
        $page = $settings_pages->fetch('zippy_settings_payment');

        $section = $page->createSection($this->getSettingsName(), 'Samcart');
        $field = $section->createField('api_key', __('API Key', ZippyCourses::TEXTDOMAIN), 'text', array('readonly'));
        $field->setValue($this->getAPIKey());

        $button_text = $this->getResetButton();
        $field = $section->createField('api_reset_button', '', 'raw');
        $field->setValue($button_text);

        $instructions_text = $this->getInstructions();
        $instructions = $section->createField('samcart-instructions', '', 'raw');
        $instructions->setValue($instructions_text);
    }

    private function getInstructions()
    {
        $html = '<div class="zippy-settings-warning">' .
                    '<p><strong>Note on Samcart Integration:</strong></p>' .
                    '<p>When using Samcart with Zippy Courses, it is important to be aware of a few functional differences 
                    that Samcart has when compared to other Payment Gateway integrations.  These differences will
                    impact how you use Samcart with Zippy Courses for a few common functions:</p>' .
                    '<ul>' .
                        '<li>When refunding an order in Samcart, you will also need to manually cancel or mark an order 
                        as refunded in your Zippy Courses site.</li>' .
                        '<li>Purchases of Subscriptions or Payment Plans via Samcart will only send a single notification 
                        at the time of the first payment, so Zippy Courses will be unaware of recurring payment.  For
                        accurate accounting, please visit the Samcart website.</li>' .
                    '</ul>' .
                    '<p>With these considerations in mind, you are ready to use Zippy Courses with Samcart!</p>' .
                '</div>';
        
        return $html;
    }
    private function getResetButton()
    {
        $html = '<button id="zippy-courses-samcart-key-reset-button" class="button" onClick="">' . __('Reset API Key', ZippyCourses::TEXTDOMAIN) . '</button>';
        return $html;
    }


    public function orderForm()
    {
        $zippy = Zippy::instance();

        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_Samcart_OrderForm');
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function checkConfiguration($configured)
    {
        $settings = get_option('zippy_samcart_payment_gateway_settings');
        $api_key    = isset($settings['api_key']) ? $settings['api_key'] : '';

        $configured = !empty($api_key);

        return $configured;
    }

    /**
     * Integrate with the correct metaboxes
     * @return void
     */
    public function metaboxes($metaboxes)
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method = isset($settings['method']) ? $settings['method'] : false;

        if ($method != $this->service) {
            return $metaboxes;
        }

        $dir = $this->path . 'assets/views/metaboxes/';
        $files = array_diff(scandir($dir), array('..', '.'));

        foreach ($files as $key => &$file) {
            if (!is_dir($dir . $file)) {
                $file = $dir . $file;
            } else {
                unset($files[$key]);
            }
        }

        return array_merge($metaboxes, $files);
    }

    public function buyNow()
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) ||
            (
                $post->post_type !== 'product' &&
                !$zippy->core_pages->is($post->ID, 'buy')
            ) ||
            !isset($_GET['buy-now']) ||
            current_user_can('edit_others_posts')
        ) {
            return;
        }
        
        $product = $post->post_type == 'product'
            ? $zippy->make('product', array('id' => $post->ID))
            : $zippy->utilities->product->getProductByBuyQueryVar();

        if ($product) {
            $order_form_url = get_post_meta($product->getId(), 'samcart_product_url', true);
            
            if (!empty($order_form_url)) {
                wp_redirect($order_form_url);
                exit;
            } else {
                $zippy->log('Product #' . $product->getId() . ' does not have a Samcart Order Form URL.', 'SAMCART_ERROR');

                $error_message  = sprintf(__('We could not find an Order Form for this product. If this problem persists, please <a href="mailto:%s">contact us</a>.', ZippyCourses::TEXTDOMAIN), $zippy->utilities->email->getSystemEmailAddress());

                $zippy->sessions->flashMessage($error_message, 'error', 0);
            }
        }

        return;
    }

    public function thankYou($content)
    {
        global $post;

        $zippy = Zippy::instance();


        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'thank_you')) {
            return $content;
        }

        if (empty($content) || (isset($_GET['preview']) && $_GET['preview'])) {
            $content =  '<p>' . __('Thank you for your purchase!', ZippyCourses::TEXTDOMAIN) . '</p>' .
                        '<p>' . __('An email will be delivered to your inbox shortly with a link to claim your course. If you run into any difficulties, please contact [zippy_contact_email_link] and we will help resolve any issues.', ZippyCourses::TEXTDOMAIN) . '</p>';

            $content = do_shortcode($content);
            
        }

        return $content;
    }

    /**
     * Since we always want it to appear like an order was placed on the Thank-You page with Samcart
     * We set a dummy transaction key, so that we don't have to worry about transaction key checks
     * outside the integration
     * @since  1.2.1
     */
    public function setDummyTransactionKey($output)
    {
        global $zippy_transaction_key;
        $zippy_transaction_key = 'samcart';
        return $zippy_transaction_key;
    }

    public function getAPIKey()
    {
        $settings = get_option('zippy_samcart_payment_gateway_settings');

        $api_key    = isset($settings['api_key']) ? $settings['api_key'] : '';
        
        if (empty($api_key)) {
            //If an API Key Hasn't been set, create one
            $settings['api_key'] = uniqid('zc_', true);
            update_option('zippy_samcart_payment_gateway_settings', $settings);
        }

        return  $settings['api_key'];
    }
}
