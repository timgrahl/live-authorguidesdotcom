<?php

class ZippyCourses_Samcart_PaymentGatewayListener extends Zippy_PaymentGatewayListener
{
    public function __construct($gateway)
    {
        $settings = get_option('zippy_samcart_payment_gateway_settings', array());

        $this->gateway = $gateway;
    }

    protected function process(Zippy_Event $event)
    {
        global $zippy_transaction_key;

        $zippy = Zippy::instance();
        if ($this->validateGateway($event)) {
            $transaction = $this->_processTransaction($event);
        }
    }

    public function _processTransaction(Zippy_Event $event)
    {
        $zippy = Zippy::instance();
        $settings = get_option('zippy_samcart_payment_gateway_settings');
        $api_key    = isset($settings['api_key']) ? $settings['api_key'] : '';

        $event_data = $this->parseRawPostdata($event->raw_post);
        if ($event_data->api_key == $api_key) {
                $adapter    = new ZippyCourses_Samcart_PaymentGatewayTransactionAdapter($event_data);
                return $this->_processTransactionData($adapter->getNormalizedData());
        } else {
            $zippy->log->log('SAMCART: Invalid API Key Provided by Payment Notification.');
        }
    }

    /**
     * Receipte php://input data and return an array of decoded items
     * @param  string $input php://input stream
     * @return array
     */
    private function parseRawPostdata($input)
    {
        return json_decode($input);
    }
    
    private function _processTransactionData($data)
    {
        $zippy = Zippy::instance();

        // Prepare a fresh transaction object
        $transaction = $zippy->make('transaction');

        // If we've got a transaction key from the adapter, then use it to fetch the
        // existing transaction.
        if ($data['key']) {
            $transaction->buildByTransactionKey($data['key']);
        }
        
        $transaction->importData($data);

        $transaction->save();

        return $transaction;
    }

    /**
     * Validate that the IPN is for Samcart
     *
     * @param  ZippyCourses_TransactionNotification_Event $event
     *
     * @return bool
     */
    protected function validateGateway(Zippy_Event $event)
    {
        return $event->gateway == 'samcart';
    }
}
