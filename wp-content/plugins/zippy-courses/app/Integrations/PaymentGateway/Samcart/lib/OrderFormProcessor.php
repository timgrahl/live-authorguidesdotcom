<?php

class ZippyCourses_Samcart_OrderFormProcessor extends ZippyCourses_Order_FormProcessor
{
    /**
     * Transaction
     * @var Zippy_Transaction
     */
    protected $txn;

    public $business;

    public function fail()
    {
        $zippy = Zippy::instance();
        $zippy->sessions->flashMessage('There was an error processing your request', 'error', 0);
    }

    public function execute()
    {
        global $post;
        $zippy = Zippy::instance();
        $product = $post->post_type == 'product'
            ? $zippy->make('product', array('id' => $post->ID))
            : $zippy->utilities->product->getProductByBuyQueryVar();

        if ($product) {
            $order_form_url = get_post_meta($product->getId(), 'samcart_product_url', true);
            if (!empty($order_form_url)) {
                wp_redirect($order_form_url);
                exit;
            } else {
                $zippy->log('Product #' . $product->getId() . ' does not have a Samcart Order Form URL.', 'SAMCART_ERROR');

                $error_message  = sprintf(__('We could not find an Order Form for this product. If this problem persists, please <a href="mailto:%s">contact us</a>.', ZippyCourses::TEXTDOMAIN), $zippy->utilities->email->getSystemEmailAddress());

                $zippy->sessions->flashMessage($error_message, 'error', 0);
            }
        }
    }
}
