<?php

class ZippyCourses_Samcart_PaymentGateway extends Zippy_PaymentGateway
{
    public $id      = 'samcart';
    public $name    = 'SamCart';

    public function __construct()
    {
        parent::__construct();
    }

    public function setup()
    {
    }

    public function register()
    {
    }

    public function registerListener()
    {
        $this->listener = new ZippyCourses_Samcart_PaymentGatewayListener($this->id);
        $this->listener->register();
    }

    public function getId()
    {
        return $this->id;
    }
}
