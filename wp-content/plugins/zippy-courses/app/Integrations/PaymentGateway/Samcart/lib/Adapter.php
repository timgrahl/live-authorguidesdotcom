<?php

class ZippyCourses_Samcart_PaymentGatewayTransactionAdapter extends Zippy_PaymentGatewayTransactionAdapter
{
    public $gateway = 'samcart';
    
    public function normalize($data)
    {
        if ($this->normalized === null) {
            $zippy = Zippy::instance();
        
            $transaction        = $zippy->make('transaction');
            $transaction_key    = null;
            $transaction->buildByTransactionKey($transaction_key);

            $vendor_id          = $data->order->id;
            $id                 = $this->normalizeId($transaction_key, $vendor_id);

            $product            = $this->getProduct($data);

            $order              = $this->normalizeOrder($id, $vendor_id, $data);

            $this->normalized = array(
                'id'                    => $id,
                'key'                   => $this->normalizeKey($id),
                'title'                 => get_the_title($id),
                'vendor_id'             => $vendor_id,
                'total'                 => $this->normalizeTotal($data),
                'fee'                   => $this->normalizeFee($data),
                'tax'                   => $this->normalizeTax($data),
                'currency'              => $this->normalizeCurrency($data),
                'customer'              => $this->normalizeCustomer($data->customer),
                'type'                  => $this->normalizeType($transaction),
                'timestamp'             => $this->normalizeTimestamp($data),
                'status'                => $this->normalizeStatus($data),
                'gateway'               => $this->gateway,
                'mode'                  => $this->normalizeMode($data),
                'recurring'             => $this->normalizeRecurring($data),
                'recurring_id'          => $this->normalizeRecurringId($data),
                'order'                 => $order,
                'owner'                 => $transaction->getOwner(),
                'method'                => $this->normalizeMethod($data),
                'product'               => $product,
                'raw'                   => json_encode($data)
            );
        }
        return $this->normalized;
    }
    protected function normalizeKey($input)
    {
        global $wpdb;

        $key = null;

        $sql = $wpdb->prepare(
            "SELECT
                pm1.post_id,
                pm1.meta_value 
             FROM 
                $wpdb->postmeta AS pm1 
             LEFT JOIN 
                $wpdb->postmeta AS pm2 
             ON
                (pm1.post_id = pm2.post_id) 
             WHERE 
                pm1.meta_key = %s AND
                pm2.meta_key = %s AND
                pm2.meta_value = %s",
            'vendor_id',
            'vendor',
            'samcart'
        );

        $results = $wpdb->get_results($sql);
        
        $transactions = array();
        foreach ($results as $r) {
            $transactions[$r->post_id] = $r->meta_value;
        }

        $post_id = array_search($input['Id'], $transactions);

        if ($post_id !== false) {
            $key = get_post_meta($post_id, 'transaction_key', true);
        }

        return $key;
    }
    protected function normalizeType($transaction)
    {
        if ($transaction instanceof Zippy_Transaction) {
            return $transaction->getType();
        }

        return 'single';
    }

    protected function normalizeId($transaction_key, $vendor_id)
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'transaction_key', $transaction_key);
        $transaction_id = (int) $wpdb->get_var($sql);

        // If we've never seen this transaction key, it's a new transaction, so return right away.
        if ($transaction_id === 0) {
            return $transaction_id;
        }

        // If $vendor_id has not been passed in as 0 (like in the event of subscr_signup, etc),
        // then we can use it to see if this vendor_id has been used before.  Otherwise, it should be 0.
        if ($vendor_id) {
            $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'vendor_id', $vendor_id);
            $vendor_id_exists = $vendor_id ? $wpdb->get_var($sql) : null;

            if ($vendor_id_exists) {
                return $vendor_id_exists;
            }
        }

        // If an item has this transaction key and an empty vendor ID, use that record.  If an item has this transaction key, but has a vendor ID, create a new one...
        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE post_id = %s AND meta_key = %s", $transaction_id, 'vendor_id');
        $has_vendor_id = $wpdb->get_var($sql);

        if (empty($has_vendor_id)) {
            return $transaction_id;
        }

        return 0;
    }


    protected function normalizeTotal($input)
    {
        return $input->product->price;
    }

    protected function normalizeFee($input)
    {
        return 0;
    }

    protected function normalizeCurrency($input)
    {
        if (isset($input->currency)) {
            return $input->currency;
        }

        $zippy = Zippy::instance();

        return $zippy->utilities->cart->getCurrency();
    }

    protected function normalizeTax($input)
    {
        return 0;
    }

    protected function normalizeMode($input)
    {
        return 'live';
    }

    protected function normalizeMethod($input)
    {
        return 'credit_card';
    }

    protected function normalizeRecurringId($input)
    {
        return 0;
    }


    protected function normalizeRecurring($input)
    {
        return false;
    }

    /**
     * Map status to valid transaction status: complete|pending|refund|fail|revoke|cancel
     *
     *
     * @param  string $input The status from gateway
     * @return string
     */
    protected function normalizeStatus($input)
    {
        $status         = $input->type;
        $valid_statuses = array('Order');

        return in_array($status, $valid_statuses) ? 'complete' : 'refund';
    }

    protected function normalizeTimestamp($input)
    {
        $zippy = Zippy::instance();

        
        return $zippy->utilities->datetime->getNow();
    }

    protected function normalizeOrder($id, $vendor_id, $data)
    {
        return null;
    }

    /**
     * Search for a product with a matching 'Samcart ID'
     * and return that product
     */
    public function getProduct($input)
    {
        global $wpdb;
        $zippy = Zippy::instance();
        $samcart_id = $input->product->id;
        $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'samcart_product_id', $samcart_id);
        $product_id = $wpdb->get_var($sql);
        $product = $zippy->make('product', array($product_id));
        return $product;


    }

    protected function normalizeCustomer($input)
    {
        $zippy = Zippy::instance();

        $customer = $zippy->make('customer');

        $customer->importData(array(
            'email'         => $input->email,
            'first_name'    => ucwords(strtolower($input->first_name)),
            'last_name'     => ucwords(strtolower($input->last_name))
        ));

        return $customer;
    }
}
