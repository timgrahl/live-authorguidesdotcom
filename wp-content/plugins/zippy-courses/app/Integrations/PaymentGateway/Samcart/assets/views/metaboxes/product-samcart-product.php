<?php
/*
Id: zippy-samcart-product
Name: Samcart Product
Fields: samcart_product_id, samcart_product_url
Post Types: product
Context: normal
Priority: default
Version: 1.0.0
*/
global $post;

?>

<p>
<label for="samcart_product_id"><?php _e('Samcart Product ID:', ZippyCourses::TEXTDOMAIN); ?></label>
<input type="text" name="samcart_product_id" id="samcart_product_id" value="<?php echo $samcart_product_id ?>">
</p>

<p>
<label for="samcart_product_url"><?php _e('Samcart Product URL:', ZippyCourses::TEXTDOMAIN); ?></label>
<input name="samcart_product_url" value="<?php echo $samcart_product_url; ?>" />
</p>
