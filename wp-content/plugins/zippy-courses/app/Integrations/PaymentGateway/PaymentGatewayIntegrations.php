<?php

class ZippyCourses_PaymentGatewayIntegrations
{
    public function __construct()
    {
        $this->prepare();
    }

    public function prepare()
    {
        $zippy = Zippy::instance();

        $files = $zippy->utilities->file->getIntegrationFiles(
            'payment-gateway-integration',
            'PaymentGatewayIntegration',
            ZippyCourses::$path . 'app/Integrations/PaymentGateway/'
        );

        foreach ($files as $file) {
            $zippy->makeFromFile($file);
        }
    }
}
