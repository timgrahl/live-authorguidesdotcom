<?php
/*
Id: zippy-oneshoppingcart-product
Name: 1ShoppingCart Product
Fields: oneshoppingcart_product_id, oneshoppingcart_order_form_url
Post Types: product
Context: normal
Priority: default
Version: 1.0.0
*/
global $post;

$list = $zippy->utilities->oneshoppingcart->gateway->getProductsList();
?>

<p>
<label for="oneshoppingcart_product_id"><?php _e('1ShoppingCart Product:', ZippyCourses::TEXTDOMAIN); ?></label>
<select name="oneshoppingcart_product_id" id="oneshoppingcart_product_id">
    <option value=""></option>
    <?php foreach ($list as $item):
        $disabled = $item['used'] && $item['id'] != $oneshoppingcart_product_id ? ' disabled="disabled" ' : '';
        $selected = selected($oneshoppingcart_product_id, $item['id'], false);
        ?>
        <option value="<?php echo $item['id']; ?>" <?php echo $selected; ?> <?php echo $disabled; ?>><?php echo $item['name']; ?></option>
    <?php endforeach; ?>
</select>
</p>

<p>
<label for="oneshoppingcart_order_form_url"><?php _e('Order Form URL:', ZippyCourses::TEXTDOMAIN); ?></label>
<input name="oneshoppingcart_order_form_url" value="<?php echo $oneshoppingcart_order_form_url; ?>" />
</p>
