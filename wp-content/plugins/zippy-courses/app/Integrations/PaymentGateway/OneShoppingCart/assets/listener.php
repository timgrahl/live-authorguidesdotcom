<?php

require_once(dirname(dirname(__FILE__)) . '/wp-load.php');

$gateway = 'oneshoppingcart';

$zippy = Zippy::instance();
$event = new ZippyCourses_TransactionNotification_Event($gateway);

$zippy->events->fire($event);
