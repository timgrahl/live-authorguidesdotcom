<?php
/**
 * Id: oneshoppingcart
 * File Type: payment-gateway-integration
 * Class: ZippyCourses_1ShoppingCart_PaymentGatewayIntegration
 *
 * @since 1.0.0
 */

class ZippyCourses_1ShoppingCart_PaymentGatewayIntegration extends Zippy_PaymentGatewayIntegration
{
    public $id = 'oneshoppingcart';
    public $service = 'oneshoppingcart';
    public $name = '1ShoppingCart';
    public $settings = array();
    
    protected $api;

    private $path;
    private $url;

    public function __construct()
    {
        $this->path = plugin_dir_path(__FILE__);
        $this->url  = plugin_dir_url(__FILE__);

        add_filter('zippy_middleware_rules', array($this, 'middleware'), 10, 2);

        parent::__construct();
    }

    public function setup()
    {
        $this->settings();
        $this->actions();
        $this->filters();
        $this->utilities();

        add_filter('zippy_metaboxes', array($this, 'metaboxes'));
    }

    protected function actions()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_action('init', array($this, 'installListener'));
            add_action('init', array($this, 'orderForm'));
            add_action('template_redirect', array($this, 'buyNow'), 11);
        }
    }

    protected function filters()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_filter('zippy_active_gateway_configured', array($this, 'checkConfiguration'));
            add_filter('zippy_filter_get_transaction_key', array($this, 'thankYou'));
        }
    }

    public function map($classes)
    {
        $classes['ZippyCourses_1ShoppingCart_PaymentGateway']            = $this->path . 'lib/PaymentGateway.php';
        $classes['ZippyCourses_1ShoppingCart_PaymentGatewayAPI']         = $this->path . 'lib/API.php';
        $classes['ZippyCourses_1ShoppingCart_PaymentGatewayListener']    = $this->path . 'lib/Listener.php';
        $classes['ZippyCourses_1ShoppingCartGateway_Utilities']          = $this->path . 'lib/Utilities.php';
        $classes['ZippyCourses_1ShoppingCart_OrderForm']                 = $this->path . 'lib/OrderForm.php';
        $classes['ZippyCourses_1ShoppingCart_OrderFormProcessor']        = $this->path . 'lib/OrderFormProcessor.php';
        $classes['ZippyCourses_1ShoppingCart_PaymentGatewayTransactionAdapter']    = $this->path . 'lib/Adapter.php';
        
        $classes['Zippy_OrderHas1ShoppingCartAccess_MiddlewareRule']     = $this->path . 'lib/Middleware/Rules/OrderHas1ShoppingCartAccess.php';

        return $classes;
    }

    public function utilities()
    {
        $zippy = Zippy::instance();

        if (!isset($zippy->utilities->oneshoppingcart)) {
            $zippy->utilities->oneshoppingcart = new stdClass;
        }

        $zippy->utilities->oneshoppingcart->gateway = new ZippyCourses_1ShoppingCartGateway_Utilities;
    }

    public function registerForm()
    {
        $zippy = Zippy::instance();

        $payment_settings = get_option('zippy_payment_general_settings', array());
        $gateway          = isset($payment_settings['method']) ? $payment_settings['method'] : null;

        if ($this->id == $gateway) {
            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_1ShoppingCart_OrderForm');
        }
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $integrations = $zippy->make('payment_gateway_integrations');
        $integrations->add($this);

        $gateway    = new ZippyCourses_1ShoppingCart_PaymentGateway;
        $gateways   = $zippy->make('payment');
        
        $gateways->add($gateway);

        $this->registerForm();
    }

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    public function settings()
    {
        $zippy = Zippy::instance();

        $settings_pages = $zippy->make('settings_pages_repository');
        $page = $settings_pages->fetch('zippy_settings_payment');

        $section = $page->createSection($this->getSettingsName(), '1ShoppingCart');
            $section->createField('merchant_id', __('Merchant ID', ZippyCourses::TEXTDOMAIN));
            $section->createField('api_key', __('API Key', ZippyCourses::TEXTDOMAIN));
            $section->createField('api_key', __('API Key', ZippyCourses::TEXTDOMAIN));

            $field = $section->createField('notification-url', __('Notification URL', ZippyCourses::TEXTDOMAIN), 'raw');
            $field->setValue($this->getListenerURL());

            $field = $section->createField('thank-you-url', __('Thank You URL', ZippyCourses::TEXTDOMAIN), 'raw');
            $field->setValue($this->getListenerURL());

    }

    public function orderForm()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            $zippy = Zippy::instance();

            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_1ShoppingCart_OrderForm');
        }
    }

    public function thankYou($key)
    {
        global $zippy_transaction_key;

        $zippy = Zippy::instance();
        
        $zippy_transaction_key = $zippy->sessions->retrieve('zippy_transaction_key');

        return $key;
    }

    public static function path()
    {
        return plugin_dir_path(__FILE__);
    }

    public static function url()
    {
        return plugin_dir_url(__FILE__);
    }

    public function assets()
    {
    }

    /**
     * Integrate with the correct metaboxes
     * @return void
     */
    public function metaboxes($metaboxes)
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method = isset($settings['method']) ? $settings['method'] : false;

        if ($method != $this->service) {
            return $metaboxes;
        }
        
        $dir = $this->path . 'assets/views/metaboxes/';
        $files = array_diff(scandir($dir), array('..', '.'));

        foreach ($files as $key => &$file) {
            if (!is_dir($dir . $file)) {
                $file = $dir . $file;
            } else {
                unset($files[$key]);
            }
        }

        return array_merge($metaboxes, $files);
    }

    public function middleware($rules, $id)
    {
        if ($id !== 'order-grants-access') {
            return $rules;
        }

        $rules->add(new Zippy_OrderHas1ShoppingCartAccess_MiddlewareRule);

        return $rules;
    }

    public function getId()
    {
        return $this->id;
    }

    public function checkConfiguration($configured)
    {
        $settings = get_option('zippy_oneshoppingcart_payment_gateway_settings');
        $merchant_id    = isset($settings['merchant_id']) ? $settings['merchant_id'] : '';
        $api_key    = isset($settings['api_key']) ? $settings['api_key'] : '';

        $configured = !empty($merchant_id) && !empty($api_key);

        return $configured;
    }

    /**
     * We install a listener file because 1ShoppingCart WILL NOT forward data to a Thank You page unless it ends in .php,
     * which means we cannot use the default endpoint for our listener.
     *
     * In 1.0, we add an else statement in order to UPDATE the file.
     *
     * @return void
     */
    public function installListener()
    {
        $dir = ABSPATH . 'zippy-listeners/';
        $file = $dir . $this->getListenerFileName();
        
        if (!file_exists($file)) {
            if (!file_exists($dir) && is_writable(ABSPATH)) {
                mkdir($dir);
            }

            copy(dirname(__FILE__) . '/assets/listener.php', $file);
        } else {
            $updated = get_option('zippy_oneshoppingcart_listener_updated', false);

            if (!$updated) {
                copy(dirname(__FILE__) . '/assets/listener.php', $file);
            }
        }
    }

    private function getListenerFileName()
    {
        $filename = get_option('zippy_oneshoppingcart_listener_filename');

        if ($filename === false) {
            $filename = uniqid() . '.php';
            update_option('zippy_oneshoppingcart_listener_filename', $filename);
        }

        return $filename;
    }

    private function getListenerURL()
    {
        $filename = get_option('zippy_oneshoppingcart_listener_filename');
        return home_url('/zippy-listeners/' . $filename);
    }

    public function buyNow()
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || ($post->post_type !== 'product' && !$zippy->core_pages->is($post->ID, 'buy')) || !isset($_GET['buy-now'])) {
            return;
        }

        $product = $post->post_type == 'product'
            ? $zippy->make('product', array('id' => $post->ID))
            : $zippy->utilities->product->getProductByBuyQueryVar();

        if ($product) {
            $order_form_url = get_post_meta($product->getId(), 'oneshoppingcart_order_form_url', true);
            wp_redirect($order_form_url);
            exit;
        }
    }
}
