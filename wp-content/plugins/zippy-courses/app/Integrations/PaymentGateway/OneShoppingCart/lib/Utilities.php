<?php

class ZippyCourses_1ShoppingCartGateway_Utilities
{
    public function getProductsList()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = %s", 'oneshoppingcart_product_id');
        $used_ids = $wpdb->get_col($sql);

        $api = new ZippyCourses_1ShoppingCart_PaymentGatewayAPI;

        $output = array();

        $products = $api->getProducts();

        foreach ($products as $product_id => $product) {
            $output[$product_id] = array(
                'id'    => $product_id,
                'name'  => $product->ProductName,
                'used'  => in_array($product_id, $used_ids)
            );
        }

        return $output;
    }
}
