<?php

class ZippyCourses_1ShoppingCart_OrderFormProcessor extends Zippy_FormProcessor
{
    public function fail()
    {
        $zippy = Zippy::instance();
        $zippy->session->flashMessage('There was an error processing your request', 'error');
    }

    public function execute()
    {
        $zippy = Zippy::instance();

        $product = $zippy->make('product', array('id' => $_POST['product_id']));

        $order_form_url = get_post_meta($_POST['product_id'], 'oneshoppingcart_order_form_url', true);

        wp_redirect($order_form_url);
        exit;
    }
}
