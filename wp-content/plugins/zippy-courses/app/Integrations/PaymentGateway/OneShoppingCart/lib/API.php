<?php

/**
 * 1ShoppingCart Gateway for Zippy Courses.  Using this API is useful because it does not require CURL as the official
 * library does, and it also uses wp_remote_post to make remote requests.
 *
 * The primary downside is that not everything is a particular type of 1ShoppingCart Object as the official SDK is,
 * but the functionality is leaner.
 */
class ZippyCourses_1ShoppingCart_PaymentGatewayAPI extends Zippy_PaymentGatewayAPI
{
    public $merchant_id;
    public $api_key;
    public $api_url = 'https://www.mcssl.com';

    public function __construct()
    {
        $options            = get_option('zippy_oneshoppingcart_payment_gateway_settings');
        $this->merchant_id  = isset($options['merchant_id']) ? $options['merchant_id'] : '';
        $this->api_key      = isset($options['api_key']) ? $options['api_key'] : '';
    }

    public function getUrl($endpoint)
    {
        return $this->api_url . "/API/" . $this->merchant_id . $endpoint;
    }

    /**
     * Build the parameters from a default, plus the data sent by the calling method
     *
     * @since   1.0.0
     *
     * @param   array   $body       The parameters being sent in the request body
     * @param   string  $method     The HTTP method
     *
     * @return  JSON|WP_Error
     */
    private function buildParams($body, $method = 'POST')
    {
        return array(
            'method'        => $method,
            'timeout'       => 10,
            'redirection'   => 5,
            'httpversion'   => '1.1',
            'blocking'      => true,
            'headers'       => array(
                'X-POST_DATA_FORMAT' => 'xml'
            ),
            'body'          => $body
        );
    }

    /**
     * Handle a wp_remote_post call, either returning a json_decoded version of the body,
     * or WP_Error in the event of a non-200 status code.
     *
     * @since   1.0.0
     *
     * @param   array   $response   The results form a wp_remote_post request
     *
     * @return  JSON|WP_Error
     */
    public function handleResponse($response)
    {
        $zippy = Zippy::instance();

        if (!is_wp_error($response)) {
            $body = $this->xmlToObject($response['body']);

            if (isset($body->Error)) {
                return new WP_Error('oneshoppingcart_api', $body->Error);
            }

            return $body;
        }

        return $response;
    }

    public function getProducts()
    {
        $product_ids = $this->getProductIds();
        $output = array();

        foreach ($product_ids as $product_id) {
            $product = $this->getProduct($product_id);

            if (!is_wp_error($product)) {
                $output[$product_id] = $product;
            }
        }

        return $output;
    }

    public function getProductIds()
    {
        $url = $this->getUrl('/PRODUCTS/LIST');

        $params = $this->buildParams($this->createRequestString(array()));

        $response = $this->handleResponse(
            wp_remote_post(
                $url,
                $params
            )
        );

        if (!is_wp_error($response)) {
            $output = array();

            $products = $response->Products->Product;
            foreach ($products as $id) {
                $output[] = (int) $id;
            }

            return $output;
        }

        return $response;
    }
    
    public function createRequestString($parameters = array())
    {
        $request_body = "<Request><Key>" . $this->api_key . "</Key>" . $this->parseApiParameters($parameters) . "</Request>";
        return $request_body;
    }
    
    public function parseApiParameters($parameters)
    {
        $request_payload = "";
        if ((!empty($parameters)) && (is_array($parameters))) {
            foreach ($parameters as $key => $value) {
                if (!is_array($value)) {
                    $request_payload .= ("<".$key.">".$value."</".$key.">\r\n");
                } else {
                    $request_payload .= "<".$key.">\r\n";
                    $request_payload .= $this->create_request($value);
                    $request_payload .= "</".$key.">\r\n";
                }
            }
        }
        return $request_payload;
    }
    
    public function getOrdersList()
    {
        $url        = $this->getUrl("/ORDERS/LIST");
        $params     = $this->buildParams($this->createRequestString(array()));

        $response   = $this->handleResponse(wp_remote_post($url, $params));

        if (!is_wp_error($response)) {
            return $response->OrderInfo;
        }

        return $response;
    }
        
    public function getOrder($order_id)
    {
        $url        = $this->getUrl('/ORDERS/' . $order_id . '/READ');
        $params     = $this->buildParams($this->createRequestString(array()));

        $response   = $this->handleResponse(wp_remote_post($url, $params));

        if (!is_wp_error($response)) {
            return $response->OrderInfo;
        }

        return $response;
    }

    public function getProductsList()
    {
        $url        = $this->getUrl("/PRODUCTS/LIST");
        $params     = $this->buildParams($this->createRequestString(array()));

        $response   = $this->handleResponse(wp_remote_post($url, $params));

        if (!is_wp_error($response)) {
            return (array) $response->Products->Product;
        }

        return $response;
    }
    
    public function getProduct($product_id)
    {
        $url        = $this->getUrl('/PRODUCTS/' . $product_id . '/READ');
        $params     = $this->buildParams($this->createRequestString(array()));

        $response   = $this->handleResponse(wp_remote_post($url, $params));

        if (!is_wp_error($response)) {
            return $response->ProductInfo;
        }

        return $response;
    }
    
    public function getClientsList()
    {
        $url        = $this->getUrl("/CLIENTS/LIST");
        $params     = $this->buildParams($this->createRequestString(array()));

        $response   = $this->handleResponse(wp_remote_post($url, $params));

        if (!is_wp_error($response)) {
            return (array) $response;
        }

        return $response;
    }
     
    public function getClientById($clientId)
    {
        $url        = $this->getUrl("/CLIENTS/". $clientId ."/READ");
        $params     = $this->buildParams($this->createRequestString(array()));

        $response   = $this->handleResponse(wp_remote_post($url, $params));

        if (!is_wp_error($response)) {
            return $response->ClientInfo;
        }

        return $response;
    }
    
    public function getErrorsList()
    {
        $url        = $this->getUrl("/ERRORS/LIST");
        $params     = $this->buildParams($this->createRequestString(array()));

        $response   = $this->handleResponse(wp_remote_post($url, $params));

        if (!is_wp_error($response)) {
            return $response;
        }

        return $response;
    }

    public function xmlToObject($input)
    {
        return json_decode(json_encode(simplexml_load_string($input)));
    }
}
