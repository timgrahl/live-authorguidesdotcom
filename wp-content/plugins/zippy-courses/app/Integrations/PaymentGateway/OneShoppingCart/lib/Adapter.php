<?php

class ZippyCourses_1ShoppingCart_PaymentGatewayTransactionAdapter extends Zippy_PaymentGatewayTransactionAdapter
{
    public $gateway = 'oneshoppingcart';
    
    public function normalize($data)
    {
        if ($this->normalized === null) {
            $zippy = Zippy::instance();

            $vendor_id          = $data->Id;

            $transaction        = $zippy->make('transaction');
            $transaction_key    = $this->normalizeKey($vendor_id);

            $transaction->buildByTransactionKey($transaction_key);

            $id = $this->normalizeId($transaction_key, $vendor_id);
            $recurring    = $this->normalizeRecurring($data);
            $recurring_id = $this->normalizeRecurringId($data);
            $product      = $this->normalizeProduct($transaction, $data);

            $this->normalized = array(
                'id'                    => $id,
                'key'                   => $transaction_key,
                'title'                 => ($id ? get_the_title($id) : ''),
                'vendor_id'             => $vendor_id,
                'total'                 => $this->normalizeTotal($data),
                'fee'                   => 0,
                'tax'                   => $this->normalizeTax($data),
                'currency'              => $this->normalizeCurrency($data),
                'customer'              => $this->normalizeCustomer($data),
                'type'                  => $this->normalizeType($transaction),
                'timestamp'             => $this->normalizeTimestamp($data),
                'status'                => $this->normalizeStatus($data),
                'gateway'               => $this->gateway,
                'mode'                  => $this->normalizeMode($data),
                'recurring'             => $recurring,
                'recurring_id'          => $recurring_id,
                'order'                 => $this->normalizeOrder($id, $recurring, $recurring_id, $product, $data),
                'owner'                 => $transaction->getOwner(),
                'method'                => $this->normalizeMethod($data),
                'product'               => $product,
                'raw'                   => json_encode($data)
            );
        }

        return $this->normalized;
    }

    protected function normalizeId($transaction_key, $vendor_id)
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'transaction_key', $transaction_key);
        $transaction_id = (int) $wpdb->get_var($sql);

        // If we've never seen this transaction key, it's a new transaction, so return right away.
        if ($transaction_id === 0) {
            return $transaction_id;
        }

        // If $vendor_id has not been passed in as 0 (like in the event of subscr_signup, etc),
        // then we can use it to see if this vendor_id has been used before.  Otherwise, it should be 0.
        if ($vendor_id) {
            $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'vendor_id', $vendor_id);
            $vendor_id_exists = $vendor_id ? $wpdb->get_var($sql) : null;

            if ($vendor_id_exists) {
                return $vendor_id_exists;
            }
        }

        // If an item has this transaction key and an empty vendor ID, use that record.  If an item has this transaction key, but has a vendor ID, create a new one...
        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE post_id = %s AND meta_key = %s", $transaction_id, 'vendor_id');
        $has_vendor_id = $wpdb->get_var($sql);

        if (empty($has_vendor_id)) {
            return $transaction_id;
        }

        return 0;
    }

    /**
     * Get the transaction key of this notification. If the Post ID given is not empty,
     * it means that we're getting a payment in a recurring cycle that has not been recorded yet.
     *
     * @return string|null
     */
    protected function normalizeKey($vendor_id)
    {
        global $wpdb;

        $key = null;

        $sql = $wpdb->prepare(
            "SELECT
                pm1.post_id,
                pm1.meta_value 
             FROM 
                $wpdb->postmeta AS pm1 
             LEFT JOIN 
                $wpdb->postmeta AS pm2 
             ON
                (pm1.post_id = pm2.post_id) 
             WHERE 
                pm1.meta_key = %s AND
                pm2.meta_key = %s AND
                pm2.meta_value = %s",
            'vendor_id',
            'vendor',
            'oneshoppingcart'
        );

        $results = $wpdb->get_results($sql);
        
        $transactions = array();
        foreach ($results as $r) {
            $transactions[$r->post_id] = $r->meta_value;
        }

        $post_id = array_search($vendor_id, $transactions);

        if ($post_id !== false) {
            $key = get_post_meta($post_id, 'transaction_key', true);
        }


        return $key;
    }

    protected function normalizeType($transaction)
    {
        if ($transaction instanceof Zippy_Transaction) {
            return $transaction->getType();
        }

        return 'single';
    }

    protected function normalizeTotal($input)
    {
        return $input->GrandTotal;
    }

    protected function normalizeFee($input)
    {
        return 0;
    }

    protected function normalizeCurrency($input)
    {
        $zippy = Zippy::instance();

        return $zippy->utilities->cart->getCurrency();
    }

    protected function normalizeVendorId($input)
    {
        $vendor_id = 0;

        if ($input->object == 'charge') {
            $vendor_id = $input->id ? $input->id : 0;
        }

        if ($input->object == 'invoice') {
            $vendor_id = $input->charge ? $input->charge : 0;
        }

        return $vendor_id;
    }

    protected function normalizeProduct(Zippy_Transaction $transaction, $input)
    {
        if (($product = $transaction->getProduct()) instanceof Zippy_Product) {
            return $product;
        }

        global $wpdb;

        $zippy = Zippy::instance();
        $remote_product_id = $input->LineItems->LineItemInfo->ProductId;

        $sql = $wpdb->prepare(
            "SELECT
                post_id
             FROM
                $wpdb->postmeta
             WHERE
                meta_key = %s AND
                meta_value = %s",
            'oneshoppingcart_product_id',
            $remote_product_id
        );

        $zippy = Zippy::instance();

        $product_id = (int) $wpdb->get_var($sql);
        $product    = $zippy->make('product', array($product_id));

        return $product;
    }

    protected function normalizeTax($input)
    {
        return is_object($input->LineItems->LineItemInfo->ProductTaxes) ? $input->LineItems->LineItemInfo->ProductTaxes : 0;
    }

    protected function normalizeMode($input)
    {
        return 'live';
    }

    protected function normalizeMethod($input)
    {
        return 'credit_card';
    }

    protected function normalizeRecurringId($input)
    {
        $recurring_id = is_object($input->RecurringOrderId) ? 0 : $input->RecurringOrderId;

        return $recurring_id;
    }

    protected function normalizeRecurring($input)
    {
        return $input->LineItems->LineItemInfo->IsRecurring === 'true' ? true : false;
    }

    /**
     * Map status to valid transaction status: complete|pending|refunded|failed|revoked|cancelled
     * @param  string $input The status from gateway
     * @return string
     */
    protected function normalizeStatus($input)
    {
        switch ($input->OrderStatusType) {
            case 'Refunded':
                return 'refund';
                break;
            case 'Accepted':
            default:
                return 'complete';
                break;
        }
    }

    protected function normalizeTimestamp($input)
    {
        $zippy = Zippy::instance();

        return $zippy->utilities->datetime->getDate($input->OrderDate);
    }

    /**
     * 1ShoppingCart has a weakness in their API where there is no ID based link between recurring transactiosn such as
     * a Subscription ID.  This only applies to the first order, which lacks one, and followup payments, which have one.
     *
     * We use the local Product ID and the remote Client ID (which we save to Orders) to intelligently determine if
     * there is an existing recurring transaction that is missing a recurring ID, for the same product, and if so,
     * we return that order, as it is very likely that it is in the same sequence.
     *
     * If it is not recurring, or a transaction with an existing order_id, we handle those very quickly.
     *
     * @since  1.0.0
     *
     * @param  [type] $id           [description]
     * @param  [type] $recurring    [description]
     * @param  [type] $recurring_id [description]
     * @param  [type] $product      [description]
     * @param  [type] $data         [description]
     * @return [type]               [description]
     */
    protected function normalizeOrder($id, $recurring, $recurring_id, $product, $data)
    {
        global $wpdb;

        $zippy = Zippy::instance();
        $order_id = 0;

        if ($id) {
            $order_id = get_post_meta($id, 'order_id', true);
        } else {
            if (!$recurring) {
                return;
            }

            if ($recurring_id !== null) {
                $order_id = $this->_checkForOrderWithMissingRecurringId($product, $data);

                if ($order_id) {
                    $this->_updateOrderTransactionsWithMissingRecurringId($recurring_id, $order_id);
                }

            } else {
                $order_id = $this->_getOrderIdByRecurringId($recurring_id);
            }
        }

        if (!empty($order_id)) {
            $order = $zippy->make('order');
            $order->build($order_id);

            return $order;
        }

        return;
    }

    private function _updateOrderTransactionsWithMissingRecurringId($recurring_id, $order_id)
    {
        $transaction_ids = $this->_getMatchingRecurringTransactions(array($order_id));

        foreach ($transaction_ids as $transaction_id) {
            update_post_meta($transaction_id, 'recurring_id', $recurring_id);
        }
    }

    /**
     * Get an Order ID based on the recurring ID of a recurring transaction
     *
     * @since  1.0.0
     *
     * @param  string  $recurring_id The vendor recurring ID
     * @return int
     */
    private function _getOrderIdByRecurringId($recurring_id)
    {
        global $wpdb;

        $sql = $wpdb->prepare(
            "SELECT
                pm1.post_id
            FROM
                $wpdb->postmeta AS pm1
            LEFT JOIN
                $wpdb->postmeta AS pm2
            ON
                (pm1.post_id = pm2.post_id)
            WHERE
                pm1.meta_key = %s AND
                pm2.meta_value = %s AND
                pm2.meta_key = %s AND
                pm2.meta_value = %s",
            'vendor',
            'oneshoppingcart',
            'recurring_id',
            $recurring_id
        );

        $transaction_id = $wpdb->get_var($sql);

        return $transaction_id ? (int) get_post_meta($transaction_id, 'order_id', true) : 0;
    }

    /**
     * Perform a variety of sql searches in order to determine if there is a recurring order with transactions missing
     * their recurring_ids.
     *
     * @see self::normalizeOrder
     *
     * @since  1.0.0
     * @param  Zippy_Product    $product
     * @param  Object $data     Data from request
     * @return int
     */
    private function _checkForOrderWithMissingRecurringId(Zippy_Product $product, $data)
    {
        $order_id = 0;

        $order_ids    = $this->_getOrdersWithContactAndProduct($data->ClientId, $product->getId());
        $transactions = $this->_getMatchingRecurringTransactions($order_ids);

        $missing_recurring_id = 0;
        foreach ($transactions as $transaction_id) {
            $recurring_id = get_post_meta($transaction_id, 'recurring_id', true);

            if (empty($recurring_id)) {
                $missing_recurring_id = $transaction_id;
                break;
            }
        }

        if ($missing_recurring_id) {
            $order_id = get_post_meta($missing_recurring_id, 'order_id', true);
        }

        return $order_id;
    }

    /**
     * Get a list of transactions that have a given Order ID
     *
     * @since  1.0.0
     * @param  array    $order_ids
     * @return array    List of transaction IDs
     */
    private function _getMatchingRecurringTransactions(array $order_ids)
    {
        global $wpdb;

        if (empty($order_ids)) {
            return array();
        }

        $order_id_list = implode(',', $order_ids);

        $sql = $wpdb->prepare(
            "SELECT
                pm3.post_id
             FROM
                $wpdb->postmeta AS pm3
             LEFT JOIN
                $wpdb->postmeta AS pm2
             ON
                (pm3.post_id = pm2.post_id)
             LEFT JOIN
                $wpdb->postmeta AS pm1
             ON
                (pm3.post_id = pm1.post_id)
             WHERE
                pm1.meta_key = %s AND
                pm1.meta_value IN ($order_id_list) AND
                pm2.meta_key = %s AND
                pm2.meta_value = %s AND
                pm3.meta_key = %s AND
                pm3.meta_value = %s",
            'order_id',
            'vendor',
            'oneshoppingcart',
            'recurring',
            '1'
        );

        return array_values(
            array_filter(
                (array) $wpdb->get_col($sql)
            )
        );
    }

    /**
     * Get a list of orders with a matching product and remote contact ID
     *
     * @since 1.0.0
     * @param  int $contact_id
     * @param  int $product_id
     * @return array    List of order IDs
     */
    private function _getOrdersWithContactAndProduct($contact_id, $product_id)
    {
        global $wpdb;

        $sql = $wpdb->prepare(
            "SELECT
                pm2.post_id
             FROM
                $wpdb->postmeta AS pm2
             LEFT JOIN
                $wpdb->postmeta AS pm1
             ON
                (pm1.post_id = pm2.post_id)
             WHERE
                pm1.meta_key = %s AND
                pm1.meta_value = %s AND
                pm2.meta_key = %s AND
                pm2.meta_value = %s",
            'product',
            $product_id,
            'oneshoppingcart_contact_id',
            $contact_id
        );

        return array_values(
            array_filter(
                (array) $wpdb->get_col($sql)
            )
        );
    }

    protected function normalizeCustomer($input)
    {
        $zippy = Zippy::instance();

        $customer = $zippy->make('customer');
        
        $api = new ZippyCourses_1ShoppingCart_PaymentGatewayAPI;
        $contact = $api->getClientById($input->ClientId);

        if (!is_wp_error($contact)) {
            $customer->importData(array(
                'email'         => $contact->Email,
                'first_name'    => $contact->FirstName,
                'last_name'     => $contact->LastName
            ));
        }

        return $customer;
    }
}
