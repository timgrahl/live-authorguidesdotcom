<?php

class ZippyCourses_1ShoppingCart_PaymentGateway extends Zippy_PaymentGateway
{
    public $id      = 'oneshoppingcart';
    public $name    = '1ShoppingCart';

    public function __construct()
    {
        parent::__construct();
    }

    public function setup()
    {
    }

    public function register()
    {
    }

    public function registerListener()
    {
        $this->listener = new ZippyCourses_1ShoppingCart_PaymentGatewayListener($this->id);
        $this->listener->register();
    }

    public function getId()
    {
        return $this->id;
    }
}
