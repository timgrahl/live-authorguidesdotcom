<?php

class Zippy_OrderHas1ShoppingCartAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You do not own access to this content.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'order-has-oneshoppingcart-access';
    }

    public function handle($object)
    {
        if (!($object instanceof Zippy_Order) || is_admin()) {
            $this->exitEarly(true);
        }

        if ($object->getGateway() !== 'oneshoppingcart') {
            return $this->pass($object);
        }

        $order_id = get_post_meta($object->getId(), 'oneshoppingcart_order_id', true);

        if (empty($order_id)) {
            $this->exitEarly(true);
        }

        $zippy = Zippy::instance();

        switch ($object->getType()) {
            case 'subscription':
            case 'payment-plan':
                $access = $this->_handleRecurringAccess($object);
                break;
            case 'single':
            default:
                $access = $this->_handleSinglePaymentAccess($object);
                break;
        }

        if ($this->_transientExpired($object)) {
            $expire = $zippy->utilities->datetime->getNow();
            $expire->modify('+2 hours');

            $status = array(
                'active' => ($access ? 'yes' : 'no'),
                'expire' => $expire->format('U')
            );

            update_post_meta($object->getId(), 'oneshoppingcart_order_status', $status);
        }

        return $access ? $this->pass($object) : $this->fail($object);
    }

    private function _transientExpired($object)
    {
        $zippy = Zippy::instance();
        
        $status = get_post_meta($object->getId(), 'oneshoppingcart_order_status', true);
        $now    = $zippy->utilities->datetime->getNow();

        return empty($status) || !isset($status['expire']) || $now->format('U') > $status['expire'];
    }

    private function _checkTransientStatus($object)
    {
        $zippy = Zippy::instance();
        
        $status = get_post_meta($object->getId(), 'oneshoppingcart_order_status', true);
        $now    = $zippy->utilities->datetime->getNow();

        if (!empty($status) &&
            isset($status['expire']) &&
            $now->format('U') < $status['expire'] &&
            isset($status['active']) && $status['active'] == 'yes'
        ) {
            return true;
        }

        return false;
    }

    private function _handleTransactionAccess(Zippy_Transaction $transaction)
    {
        $api = new ZippyCourses_1ShoppingCart_PaymentGatewayAPI;

        $vendor_order = $api->getOrder($transaction->getVendorId());

        if (!is_wp_error($vendor_order)) {
            return $vendor_order->OrderStatusType == 'Accepted';
        }

        return false;
    }

    private function _handleRecurringAccess($object)
    {
        if (($status = $this->_checkTransientStatus($object)) === true) {
            return true;
        }

        $access = true;

        if (!$object->transactions->count()) {
            $object->fetchTransactions();
        }

        $max_checks = $object->transactions->count() > 3 ? 3 : $object->transactions->count();

        $i = 0;
        foreach ($object->transactions->all() as $transaction) {
            $access         = $this->_handleTransactionAccess($transaction);

            if (!$access || $i >= $max_checks) {
                break;
            }

            $i++;
        }

        return $access;
    }

    private function _handleSinglePaymentAccess($object)
    {
        if (($status = $this->_checkTransientStatus($object)) === true) {
            return true;
        }

        if (!$object->transactions->count()) {
            $object->fetchTransactions();
        }

        $transaction = $object->transactions->first();

        return $transaction !== null ? $this->_handleTransactionAccess($transaction) : false;
    }
}
