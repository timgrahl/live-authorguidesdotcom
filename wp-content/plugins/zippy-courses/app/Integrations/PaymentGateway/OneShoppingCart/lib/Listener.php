<?php

class ZippyCourses_1ShoppingCart_PaymentGatewayListener extends Zippy_PaymentGatewayListener
{
    public function register()
    {
        parent::register();

        $zippy = Zippy::instance();

        $zippy->events->register('OrderStatusChange', $this);
        $zippy->events->register('OrderOwnerChange', $this);
        $zippy->events->register('SaveTransaction', $this);
        $zippy->events->register('OrderSave', $this);
    }

    public function handle(Zippy_Event $event)
    {
        switch ($event->getEventName()) {
            case 'TransactionNotification':
                if ($this->validateGateway($event)) {
                    $this->process($event);
                }
                break;
            case 'OrderSave':
                $this->_handleOrderSave($event);
                break;
            case 'OrderOwnerChange':
                $this->_handleOrderOwnerChange($event);
                break;
            default:
                break;
        }
    }

    private function _handleOrderOwnerChange(Zippy_Event $event)
    {
        $order = $event->order;

        if (!$order->transactions->count()) {
            $order->fetchTransactions();
        }

        $transaction = $order->transactions->first();

        if (!$transaction) {
            return;
        }

        $vendor     = get_post_meta($transaction->getId(), 'vendor', true);
        $details    = get_post_meta($transaction->getId(), 'details', true);

        if ($vendor !== 'oneshoppingcart' || !is_array($details) || !isset($details['raw'])) {
            return;
        }

        $json = json_decode($details['raw']);
        $oneshoppingcart_contact_id = false;

        if (isset($json->ClientId)) {
            $oneshoppingcart_contact_id = $json->ClientId;
        }

        if ($oneshoppingcart_contact_id) {
            $old_user_contact_id = get_user_meta($event->old_owner, 'oneshoppingcart_contact_id', true);
            if ($old_user_contact_id == $oneshoppingcart_contact_id) {
                delete_user_meta($event->old_owner, 'oneshoppingcart_contact_id');
            }

            update_user_meta($event->new_owner, 'oneshoppingcart_contact_id', $oneshoppingcart_contact_id);
        }
    }

    private function _handleOrderSave(Zippy_Event $event)
    {
        $order = $event->order;

        if (!$order->transactions->count()) {
            $order->fetchTransactions();
        }

        $transaction = $order->transactions->first();

        if (!$transaction) {
            return;
        }

        $vendor     = get_post_meta($transaction->getId(), 'vendor', true);
        $details    = get_post_meta($transaction->getId(), 'details', true);

        if ($vendor !== 'oneshoppingcart' || !is_array($details) || !isset($details['raw'])) {
            return;
        }

        $json = json_decode($details['raw']);
        $oneshoppingcart_order_id = false;
        $oneshoppingcart_contact_id = false;

        if (isset($json->Id)) {
            $oneshoppingcart_order_id = $json->Id;
        }

        if ($oneshoppingcart_order_id) {
            update_post_meta($order->getId(), 'oneshoppingcart_order_id', $oneshoppingcart_order_id);
        }

        if (isset($json->ClientId)) {
            $oneshoppingcart_contact_id = $json->ClientId;
        }

        if ($oneshoppingcart_contact_id) {
            update_post_meta($order->getId(), 'oneshoppingcart_contact_id', $oneshoppingcart_contact_id);
        }
    }

    protected function process(Zippy_Event $event)
    {
        global $zippy_transaction_key;

        $zippy = Zippy::instance();

        $type = isset($event->post['orderID']) ? 'thank-you' : 'notification';

        switch ($type) {
            case 'notification':
                $transaction = $this->_processNotification($event);
                break;
            case 'thank-you':
                $transaction = $this->_processThankYou($event);
                break;
            default:
                $transaction = null;
                break;
        }

        if ($transaction) {
            // Set our global for use in filters, etc.
            $zippy_transaction_key = $transaction->getKey();
            $zippy->sessions->store('zippy_transaction_key', $zippy_transaction_key);

            if ($type == 'thank-you') {
                wp_redirect($zippy->core_pages->getUrl('thank_you'));
                exit;
            }

        } else {
            $zippy->sessions->delete('zippy_transaction_key');
        }
    }

    private function _processThankYou(Zippy_event $event)
    {
        $api = new ZippyCourses_1ShoppingCart_PaymentGatewayAPI;

        $vendor_order_id   = $event->post['orderID'];
        $vendor_order = $api->getOrder($vendor_order_id);

        $adapter    = new ZippyCourses_1ShoppingCart_PaymentGatewayTransactionAdapter($vendor_order);

        return $this->_processTransactionData($adapter->getNormalizedData());
    }

    private function _processNotification(Zippy_event $event)
    {
        $api = new ZippyCourses_1ShoppingCart_PaymentGatewayAPI;

        $xml = $api->xmlToObject($event->raw_post);

        $vendor_order_id = $xml->Token;
        $vendor_order = $api->getOrder($vendor_order_id);

        $adapter    = new ZippyCourses_1ShoppingCart_PaymentGatewayTransactionAdapter($vendor_order);
        return $this->_processTransactionData($adapter->getNormalizedData());
    }

    private function _processTransactionData($data)
    {
        $zippy = Zippy::instance();

        // Prepare a fresh transaction object
        $transaction = $zippy->make('transaction');

        // If we've got a transaction key from the adapter, then use it to fetch the
        // existing transaction.
        if ($data['key']) {
            $transaction->buildByTransactionKey($data['key']);
        }
    
        // Import the IPN data into the transaction and save.
        $transaction->importData($data);

        $transaction->save();

        return $transaction;
    }

    /**
     * Validate that the IPN is for 1ShoppingCart
     *
     * @param  ZippyCourses_TransactionNotification_Event $event
     *11
     * @return bool
     */
    protected function validateGateway(Zippy_Event $event)
    {
        return $event->gateway == 'oneshoppingcart';
    }
}
