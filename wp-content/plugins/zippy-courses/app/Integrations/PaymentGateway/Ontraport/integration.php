<?php
/**
 * Id: ontraport
 * File Type: payment-gateway-integration
 * Class: ZippyCourses_Ontraport_PaymentGatewayIntegration
 *
 * @since 1.0.0
 */

class ZippyCourses_Ontraport_PaymentGatewayIntegration extends Zippy_PaymentGatewayIntegration
{
    public $id = 'ontraport';
    public $service = 'ontraport';
    public $name = 'Ontraport';
    public $settings = array();
    
    protected $api;

    private $path;
    private $url;

    public function __construct()
    {
        $this->path = plugin_dir_path(__FILE__);
        $this->url  = plugin_dir_url(__FILE__);
        
        add_filter('zippy_middleware_rules', array($this, 'middleware'), 10, 2);

        parent::__construct();
    }

    public function setup()
    {
        $this->settings();
        $this->actions();
        $this->filters();
        $this->utilities();

        $this->api = new ZippyCourses_Ontraport_PaymentGatewayAPI;

        add_filter('zippy_metaboxes', array($this, 'metaboxes'));
    }

    protected function actions()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_action('init', array($this, 'orderForm'));
            add_action('template_redirect', array($this, 'buyNow'), 11);
        }
    }

    protected function filters()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            add_filter('zippy_active_gateway_configured', array($this, 'checkConfiguration'));
        }
    }

    public function map($classes)
    {
        $classes['ZippyCourses_Ontraport_PaymentGateway']            = $this->path . 'lib/PaymentGateway.php';
        $classes['ZippyCourses_Ontraport_PaymentGatewayAPI']         = $this->path . 'lib/API.php';
        $classes['ZippyCourses_Ontraport_PaymentGatewayListener']    = $this->path . 'lib/Listener.php';
        $classes['ZippyCourses_OntraportGateway_Utilities']          = $this->path . 'lib/Utilities.php';
        $classes['ZippyCourses_Ontraport_OrderForm']                 = $this->path . 'lib/OrderForm.php';
        $classes['ZippyCourses_Ontraport_OrderFormProcessor']        = $this->path . 'lib/OrderFormProcessor.php';
        $classes['ZippyCourses_Ontraport_Contact']                   = $this->path . 'lib/Contact.php';
        $classes['ZippyCourses_Ontraport_PaymentGatewayTransactionAdapter']    = $this->path . 'lib/Adapter.php';
        $classes['ZippyCourses_Ontraport_PingURLTransactionAdapter']    = $this->path . 'lib/PingURLAdapter.php';

        $classes['Zippy_OrderHasOntraportAccess_MiddlewareRule']     = $this->path . 'lib/Middleware/Rules/OrderHasOntraportAccess.php';
       
        return $classes;
    }

    public function utilities()
    {
        $zippy = Zippy::instance();

        if (!isset($zippy->utilities->ontraport)) {
            $zippy->utilities->ontraport = new stdClass;
        }

        $zippy->utilities->ontraport->gateway = new ZippyCourses_OntraportGateway_Utilities;
    }

    public function registerForm()
    {

    }

    public function register()
    {
        $zippy = Zippy::instance();

        $integrations = $zippy->make('payment_gateway_integrations');
        $integrations->add($this);

        $gateway    = new ZippyCourses_Ontraport_PaymentGateway;
        $gateways   = $zippy->make('payment');
        
        $gateways->add($gateway);

        $this->registerForm();
    }

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    public function settings()
    {
        $zippy = Zippy::instance();

        $settings_pages = $zippy->make('settings_pages_repository');
        $page = $settings_pages->fetch('zippy_settings_payment');

        $section = $page->createSection($this->getSettingsName(), 'Ontraport');
            $section->createField('app_id', __('App ID', ZippyCourses::TEXTDOMAIN));
            $section->createField('api_key', __('API Key', ZippyCourses::TEXTDOMAIN));

    }

    public function orderForm()
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;

        if ($method == $this->service) {
            $zippy = Zippy::instance();

            $forms = $zippy->make('forms_repository');
            $forms->register('order', 'ZippyCourses_Ontraport_OrderForm');
        }
    }
    public static function path()
    {
        return plugin_dir_path(__FILE__);
    }

    public static function url()
    {
        return plugin_dir_url(__FILE__);
    }

    public function assets()
    {
    }

    /**
     * Integrate with the correct metaboxes
     * @return void
     */
    public function metaboxes($metaboxes)
    {
        $settings   = get_option('zippy_payment_general_settings');
        $method = isset($settings['method']) ? $settings['method'] : false;

        if ($method != $this->service) {
            return $metaboxes;
        }

        $dir = $this->path . 'assets/views/metaboxes/';
        $files = array_diff(scandir($dir), array('..', '.'));

        foreach ($files as $key => &$file) {
            if (!is_dir($dir . $file)) {
                $file = $dir . $file;
            } else {
                unset($files[$key]);
            }
        }

        return array_merge($metaboxes, $files);
    }

    public function middleware($rules, $id)
    {
        if ($id !== 'order-grants-access') {
            return $rules;
        }

        $rules->add(new Zippy_OrderHasOntraportAccess_MiddlewareRule);

        return $rules;
    }

    public function getId()
    {
        return $this->id;
    }

    public function checkConfiguration($configured)
    {
        $settings = get_option('zippy_ontraport_payment_gateway_settings');
        $app_id    = isset($settings['app_id']) ? $settings['app_id'] : '';
        $api_key    = isset($settings['api_key']) ? $settings['api_key'] : '';

        $configured = !empty($app_id) && !empty($api_key);

        return $configured;
    }

    public function buyNow()
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || ($post->post_type !== 'product' && !$zippy->core_pages->is($post->ID, 'buy')) || !isset($_GET['buy-now'])) {
            return;
        }

        $product = $post->post_type == 'product'
            ? $zippy->make('product', array('id' => $post->ID))
            : $zippy->utilities->product->getProductByBuyQueryVar();

        if ($product) {
            $order_form_url = get_post_meta($product->getId(), 'ontraport_order_form_url', true);

            wp_redirect($order_form_url);
            exit;
        }
    }
}
