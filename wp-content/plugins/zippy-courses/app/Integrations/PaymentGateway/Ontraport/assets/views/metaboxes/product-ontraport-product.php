<?php
/*
Id: zippy-ontraport-product
Name: Ontraport Product
Fields: ontraport_product_id, ontraport_tag_id, ontraport_order_form_url
Post Types: product
Context: normal
Priority: default
Version: 1.0.0
*/
global $post;

$products = $zippy->utilities->ontraport->gateway->getProductsList();
$tags = $zippy->utilities->ontraport->gateway->getTagsList();
?>

<p>
<label for="ontraport_product_id"><?php _e('Ontraport Product:', ZippyCourses::TEXTDOMAIN); ?></label>
<select name="ontraport_product_id" id="ontraport_product_id">
    <option value=""></option>
    <?php foreach ($products as $product):
        $disabled = $product['used'] && $product['id'] != $ontraport_product_id ? ' disabled="disabled" ' : '';
        $selected = selected($ontraport_product_id, $product['id'], false);
        ?>
        <option value="<?php echo $product['id']; ?>" <?php echo $selected; ?> <?php echo $disabled; ?>><?php echo $product['name']; ?></option>
    <?php endforeach; ?>
</select>
</p>

<p>
<label for="ontraport_tag_id"><?php _e('Ontraport Tag:', ZippyCourses::TEXTDOMAIN); ?></label>
<select name="ontraport_tag_id" id="ontraport_tag_id">
    <option value=""></option>
    <?php foreach ($tags as $tag):
        $disabled = $tag['used'] && $tag['id'] != $ontraport_tag_id ? ' disabled="disabled" ' : '';
        $selected = selected($ontraport_tag_id, $tag['id'], false);
        ?>
        <option value="<?php echo $tag['id']; ?>" <?php echo $selected; ?> <?php echo $disabled; ?>><?php echo $tag['name']; ?></option>
    <?php endforeach; ?>
</select>
</p>

<p>
<label for="ontraport_order_form_url"><?php _e('Order Form URL:', ZippyCourses::TEXTDOMAIN); ?></label>
<input name="ontraport_order_form_url" value="<?php echo $ontraport_order_form_url; ?>" />
</p>
