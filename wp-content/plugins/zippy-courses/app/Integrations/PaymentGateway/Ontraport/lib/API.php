<?php

/**
 * Ontraprot Gateway API for Zippy Courses.
 *
 * @since   1.0.0
 */
class ZippyCourses_Ontraport_PaymentGatewayAPI extends Zippy_PaymentGatewayAPI
{
    private $base_url = 'https://api.ontraport.com/1/';

    public function __construct()
    {
        $options        = get_option('zippy_ontraport_payment_gateway_settings');
        $this->app_id   = isset($options['app_id']) ? $options['app_id'] : '';
        $this->api_key  = isset($options['api_key']) ? $options['api_key'] : '';
    }

    /**
     * Get a list of products
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getProducts()
    {
        $url = $this->getUrl('objects');

        $products = array();

        $start = 0;
        $range = 50;
        $fetch = true;

        while ($fetch) {
            $params = $this->buildParams(array(
                'objectID' => 16,
                'performAll' => 'true',
                'sortDir' => 'asc',
                'searchNotes' => 'true',
                'range' => $range,
                'start' => $start
            ));

            $results = $this->handleResponse(wp_remote_post($url, $params));

            if (!is_wp_error($results)) {
                if (is_array($results)) {
                    $products = array_merge($products, $results);
                } else {
                    $fetch = false;
                }

                $start += $range;

                if (count($results) < $range) {
                    $fetch = false;
                }
            }
        }

        return $products;
    }

    /**
     * Get a list of contacts
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getContacts()
    {
        $url = $this->getUrl('objects');

        $params = $this->buildParams(array(
            'objectID' => 0
        ));

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Get Contact by ID
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getContact($id)
    {
        $url = $this->getUrl('object');

        $params = $this->buildParams(array(
            'objectID' => 0,
            'id' => $id
        ));

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Get Contact by Email
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getContactByEmail($email)
    {
        $url = $this->getUrl('objects');

        $params = $this->buildParams(array(
            'objectID' => 0,
            'search' => $email
        ));

        $response = $this->handleResponse(wp_remote_post($url, $params));

        if (is_wp_error($response)) {
            return $response;
        } else {
            return $response[0];
        }
        
    }

    /**
     * Get a list of purchases
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getPurchases()
    {
        $url = $this->getUrl('objects');

        $params = $this->buildParams(array(
            'objectID' => 17
        ));

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Get a purchase
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getPurchase($id)
    {
        $url = $this->getUrl('object');

        $params = $this->buildParams(array(
            'objectID' => 17,
            'id' => $id
        ));

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Get a list of tags
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getTags()
    {
        $url = $this->getUrl('objects');

        $tags = array();

        $start = 0;
        $range = 50;
        $fetch = true;

        while ($fetch) {
            $params = $this->buildParams(array(
                'objectID' => 14,
                'range' => $range,
                'start' => $start
            ));

            $results = $this->handleResponse(wp_remote_post($url, $params));

            if (!is_wp_error($results)) {
                if (is_array($results)) {
                    $tags = array_merge($tags, $results);
                } else {
                    $fetch = false;
                }

                $start += $range;

                if (count($results) < $range) {
                    $fetch = false;
                }
            }
        }

        return $tags;
    }

    /**
     * Get a purchase
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getTag($id)
    {
        $url = $this->getUrl('object');

        $params = $this->buildParams(array(
            'objectID' => 14,
            'id' => $id
        ));

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Get a list of contacts
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getObjectTypes()
    {
        $url = $this->getUrl('objects/meta');

        $params = $this->buildParams(array());

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Get transaction details
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getTransactions()
    {
        $url = $this->getUrl('objects');

        $params = $this->buildParams(array(
            'objectID' => 46
        ));

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Get transaction details
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getTransaction($id)
    {
        $url = $this->getUrl('object');

        $params = $this->buildParams(array(
            'objectID' => 46,
            'id' => $id
        ));

        return $this->handleResponse(wp_remote_post($url, $params));
    }

    /**
     * Get transaction details
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getTransactionByTransactionId($transaction_id)
    {

        $url = $this->getUrl('object');
        $object_id = $this->getObjectIdbyTransactionId($transaction_id);
        
        $params = $this->buildParams(array(
                'objectID'  => 46,
                'id'        => $object_id
            ));

        $response = $this->handleResponse(wp_remote_post($url, $params));

        return $response;

    }

    public function getObjectIdbyTransactionId($transaction_id)
    {
        $url = $this->getUrl('objects');

        $params = $this->buildParams(array(
            'objectID' => 46,
            'condition' => 'transaction_id="' . $transaction_id . '"'
        ));

        $response = $this->handleResponse(wp_remote_post($url, $params));

        if (is_wp_error($response)) {
            return $response;
        } else {
            return $response[0]->id;
        }
    }

    /**
     * Handle a wp_remote_post call, either returning a json_decoded version of the body,
     * or WP_Error in the event of a non-200 status code.
     *
     * @since   1.0.0
     *
     * @param   array   $response   The results form a wp_remote_post request
     *
     * @return  JSON|WP_Error
     */
    public function handleResponse($response)
    {
        $zippy = Zippy::instance();

        if (!is_wp_error($response)) {
            if ($response['response']['code'] == 200) {
                $body = json_decode($response['body']);

                return $zippy->utilities->deepJsonDecode($body->data);
            } else {
                $error = new WP_Error($response['body']);
            }
        } else {
        }
    }

    /**
     * Build the parameters from a default, plus the data sent by the calling method
     *
     * @since   1.0.0
     *
     * @param   array   $body       The parameters being sent in the request body
     * @param   string  $method     The HTTP method
     *
     * @return  JSON|WP_Error
     */
    private function buildParams(array $body = array(), $method = 'GET')
    {
        return array(
            'method'        => $method,
            'timeout'       => 10,
            'redirection'   => 5,
            'httpversion'   => '1.1',
            'blocking'      => true,
            'headers'       => array(
                'Api-Appid' => $this->app_id,
                'Api-Key'   => $this->api_key
            ),
            'body'          => $body
        );
    }

    /**
     * Basic endpoint builder
     *
     * @since   1.0.0
     *
     * @param   string $endpoint
     *
     * @return  string
     */
    private function getUrl($endpoint)
    {
        return $this->base_url . $endpoint;
    }

    public function generatePlanId(Zippy_Product $product)
    {
        $time = time();
        return "{$product->getId()}-{$product->getType()}-{$time}";
    }
}
