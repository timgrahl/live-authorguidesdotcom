<?php

class ZippyCourses_Ontraport_PaymentGateway extends Zippy_PaymentGateway
{
    public $id      = 'ontraport';
    public $name    = 'Ontraport';

    public function __construct()
    {
        parent::__construct();
    }

    public function setup()
    {
    }

    public function register()
    {
    }

    public function registerListener()
    {
        $this->listener = new ZippyCourses_Ontraport_PaymentGatewayListener($this->id);
        $this->listener->register();
    }

    public function getId()
    {
        return $this->id;
    }
}
