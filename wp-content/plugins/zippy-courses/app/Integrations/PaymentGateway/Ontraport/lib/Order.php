<?php
/**
 * This is an adapter for our Ontraport adapter.  Ontraport stores requisite information across a variety of API
 * calls, so we create this object to fetch and normalize all of the data
*/
class ZippyCourses_Ontraport_Order
{
    public $id;
    public $invoice;
    public $payments;
    public $payment;
    public $contact_id;
    public $order_items;
    public $subscription_plan_id;
    public $recurring_order;
    public $pay_plan;
    public $pay_plan_items;

    public function __construct($id)
    {
        $this->id   = $id;
        $this->api  = new ZippyCourses_Ontraport_PaymentGatewayAPI;
    }

    public function fetchAll()
    {
        $this->invoice              = $this->fetchInvoice();
        $this->payments             = $this->fetchPayments();

        if (count($this->payments)) {
            $this->payment              = reset($this->payments);
        }

        $this->contact_id           = $this->fetchContactId();
        $this->order_items          = $this->fetchOrderItems();
        $this->pay_plan             = $this->fetchPayPlan();

        if (isset($this->pay_plan['Id'])) {
            $this->pay_plan_items   = $this->fetchPayPlanItems();
        }

        if (count($this->order_items) > 0) {
            $this->subscription_plan_id = $this->getSubscriptionPlanId();

            if ($this->subscription_plan_id) {
                $this->recurring_order  = $this->fetchRecurringOrder($this->subscription_plan_id, $this->contact_id);
            }
        }
    }

    public function getInvoice()
    {
        if ($this->invoice === null) {
            $this->invoice = $this->fetchInvoice();
        }

        return $this->invoice;
    }

    public function fetchInvoice()
    {
        return $api->getInvoice($this->id);
    }

    public function fetchPayments()
    {
        return $this->api->getPaymentsForInvoice($this->id);
    }

    public function fetchOrderItems()
    {
        return $this->api->getOrderItems($this->id);
    }

    public function fetchPayPlan()
    {
        return $this->api->getPayPlan($this->id);
    }

    public function fetchPayPlanItems($pay_plan_id)
    {
        return $this->api->getPayPlanItems($pay_plan_id);
    }

    public function getPayPlanItems()
    {
        if ($this->pay_plan_items === null && $this->pay_plan !== null) {
            $this->pay_plan_items = $this->fetchPayPlanItems($this->pay_plan['Id']);
        }

        return $this->pay_plan_items !== null ? $this->pay_plan_items : array();
    }

    public function fetchRecurringOrder($plan_id, $contact_id)
    {
        return $this->api->getRecurringOrder($plan_id, $contact_id);
    }

    public function getSubscriptionPlanId()
    {
        return isset($this->order_items[0]) ? $this->order_items[0]['SubscriptionPlanId'] : null;
    }

    public function fetchContactId()
    {
        if ($this->invoice !== null) {
            return $this->invoice['ContactId'];
        }

        if ($this->payment !== null) {
            return $this->payment['ContactId'];
        }

        return null;
    }
}
