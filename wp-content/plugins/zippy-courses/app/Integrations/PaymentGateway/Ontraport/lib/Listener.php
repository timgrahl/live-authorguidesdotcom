<?php

class ZippyCourses_Ontraport_PaymentGatewayListener extends Zippy_PaymentGatewayListener
{
    public function register()
    {
        parent::register();

        $zippy = Zippy::instance();

        $zippy->events->register('OrderStatusChange', $this);
        $zippy->events->register('OrderOwnerChange', $this);
        $zippy->events->register('SaveTransaction', $this);
        $zippy->events->register('OrderSave', $this);
    }

    public function handle(Zippy_Event $event)
    {
        switch ($event->getEventName()) {
            case 'TransactionNotification':
                if ($this->validateGateway($event)) {
                    $this->process($event);
                }
                break;
            case 'OrderSave':
                $this->_handleOrderSave($event);
                break;
            case 'OrderOwnerChange':
                $this->_handleOrderOwnerChange($event);
                break;
            default:
                break;
        }
    }

    private function _handleOrderOwnerChange(Zippy_Event $event)
    {
        $order = $event->order;

        if (!$order->transactions->count()) {
            $order->fetchTransactions();
        }

        $transaction = $order->transactions->first();

        if (!$transaction) {
            return;
        }

        $vendor     = get_post_meta($transaction->getId(), 'vendor', true);
        $details    = get_post_meta($transaction->getId(), 'details', true);

        if ($vendor !== 'ontraport' || !is_array($details) || !isset($details['raw'])) {
            return;
        }

        $json = json_decode($details['raw']);
        $ontraport_contact_id = false;

        if (isset($json->contact_id)) {
            $ontraport_contact_id = $json->contact_id;
        }

        if ($ontraport_contact_id) {
            // Handle Old User Contact Ids
            $old_user_contact_ids = array_filter((array) get_user_meta($event->old_owner, 'ontraport_contact_id', true));
            foreach ($old_user_contact_ids as $key => $old_contact_id) {
                if ($ontraport_contact_id == $old_contact_id) {
                    unset($old_user_contact_ids[$key]);
                }
            }
            update_user_meta($event->old_owner, 'ontraport_contact_ids', $old_user_contact_ids);
            
            // Handle new user contact ids
            $new_user_contact_ids = array_filter((array) get_user_meta($event->new_owner, 'ontraport_contact_id', true));
            $new_user_contact_ids[] = $ontraport_contact_id;
            update_user_meta($event->new_owner, 'ontraport_contact_id', $new_user_contact_ids);
        }
    }

    private function _handleOrderSave(Zippy_Event $event)
    {
        $order = $event->order;

        if (!$order->transactions->count()) {
            $order->fetchTransactions();
        }

        $transaction = $order->transactions->first();

        if (!$transaction) {
            return;
        }

        $vendor     = get_post_meta($transaction->getId(), 'vendor', true);
        $details    = get_post_meta($transaction->getId(), 'details', true);

        if ($vendor !== 'ontraport' || !is_array($details) || !isset($details['raw'])) {
            return;
        }

        $json = json_decode($details['raw']);

        $ontraport_contact_id = false;

        if (isset($json->contact_id)) {
            $ontraport_contact_id = $json->contact_id;
        }

        if ($ontraport_contact_id) {
            update_post_meta($order->getId(), 'ontraport_contact_id', $ontraport_contact_id);
        }

        $ontraport_invoice_id = false;

        if (isset($json->id)) {
            $ontraport_invoice_id = $json->id;
        }

        if ($ontraport_invoice_id) {
            update_post_meta($order->getId(), 'ontraport_transaction_id', $ontraport_invoice_id);
        }
    }

    protected function process(Zippy_Event $event)
    {
        global $zippy_transaction_key;

        $zippy = Zippy::instance();

        $transaction = $this->_processTransaction($event);

        if ($transaction) {
            // Set our global for use in filters, etc.
            $zippy_transaction_key = $transaction->getKey();
        }
    }

    private function _processTransaction(Zippy_Event $event)
    {

        $api = new ZippyCourses_Ontraport_PaymentGatewayAPI;

        // Invoice ID is sent for Ping URLs, so check for that.
        if (!empty($event->get['invoice_id']) || !empty($event->request['invoice_id'])) {
            $transaction_id = !empty($event->get['invoice_id']) ? $event->get['invoice_id'] : $event->request['invoice_id'];
            $data           = $api->getTransaction($transaction_id);
            $adapter = new ZippyCourses_Ontraport_PaymentGatewayTransactionAdapter($data);

        } else {
            $transaction_id = $event->get['transaction_id'];
            $data           = $api->getTransactionByTransactionId($transaction_id);
            $adapter    = new ZippyCourses_Ontraport_PaymentGatewayTransactionAdapter($data);
        }
        return $this->_processTransactionData($adapter->getNormalizedData());
    }

    private function _processTransactionData($data)
    {
        $zippy = Zippy::instance();

        // Prepare a fresh transaction object
        $transaction = $zippy->make('transaction');

        // If we've got a transaction key from the adapter, then use it to fetch the
        // existing transaction.
        if ($data['key']) {
            $transaction->buildByTransactionKey($data['key']);
        }
    
        // Import the IPN data into the transaction and save.
        $transaction->importData($data);

        $transaction->save();

        return $transaction;
    }

    /**
     * Receive raw post data from Ontraport and decode the JSON
     *
     * @since   1.0.0
     *
     * @param   JSON $input php://input stream
     *
     * @return  stdClass
     */
    private function parseRawPostdata($input)
    {
        return json_decode($input);
    }

    /**
     * Validate that the IPN is for Ontraport
     *
     * @param  ZippyCourses_TransactionNotification_Event $event
     *
     * @return bool
     */
    protected function validateGateway(Zippy_Event $event)
    {
        $email      = urldecode(filter_input(INPUT_GET, 'email'));
        $fname      = filter_input(INPUT_GET, 'firstname');
        $lname      = filter_input(INPUT_GET, 'lastname');
        $order_id   = filter_input(INPUT_GET, 'transaction_id');
        $ip         = filter_input(INPUT_GET, 'ip_addy');

        return $event->gateway == 'ontraport' || (!empty($email) && !empty($fname) && !empty($lname) && !empty($lname) && !empty($ip));
    }
}
