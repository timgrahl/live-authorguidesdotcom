<?php

class Zippy_OrderHasOntraportAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You do not own access to this content.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'order-has-ontraport-access';
    }

    public function handle($object)
    {
        global $wpdb;

        if (!($object instanceof Zippy_Order)) {
            $this->exitEarly(true);
        }

        $invoice_id = get_post_meta($object->getId(), 'ontraport_transaction_id', true);
        if (empty($invoice_id)) {
            return $this->pass($object);
        }

        $zippy = Zippy::instance();

        $now = $zippy->utilities->datetime->getNow();
        $date = $wpdb->get_var($wpdb->prepare("SELECT post_date FROM $wpdb->posts WHERE ID = %s", $object->getId()));
        $date = $date ? $zippy->utilities->datetime->getDate($date) : null;

        if ($this->_transientExpired($object)) {
            $remote_order_access = $this->_handleOrderAccess($object);

            $access = $remote_order_access;

            $now = $zippy->utilities->datetime->getNow();
            if ($access) {
                $now->modify('+2 hours');
            } else {
                $now->modify('+10 minutes');
            }

            $status = array(
                'active' => ($access ? 'yes' : 'no'),
                'expire' => $now->format('U')
            );

            update_post_meta($object->getId(), 'ontraport_order_status', $status);
        } else {
            $status = get_post_meta($object->getId(), 'ontraport_order_status', true);
            $access = isset($status['active']) && $status['active'] == 'yes';
        }

        $access = apply_filters('zippy_courses_ontraport_order_access', $access, $object);


        return $access ? $this->pass($object) : $this->fail($object);
    }

    private function _transientExpired($object)
    {
        $zippy = Zippy::instance();
        
        $status = get_post_meta($object->getId(), 'ontraport_order_status', true);
        if (empty($status) || !isset($status['expire'])) {
            return true;
        }

        $now        = $zippy->utilities->datetime->getNow();
        $expires    = new DateTime('@' . $status['expire']);
        $expires->setTimezone($zippy->utilities->datetime->getTimezone());

        return  $now->format('U') > $expires->format('U');
    }

    private function _checkTransientStatus($object)
    {
        $zippy = Zippy::instance();
        
        $status = get_post_meta($object->getId(), 'ontraport_order_status', true);
        $now    = $zippy->utilities->datetime->getNow();

        if (!empty($status) &&
            isset($status['expire']) &&
            $now->format('U') < $status['expire'] &&
            isset($status['active']) && $status['active'] == 'yes'
        ) {
            return true;
        }

        return false;
    }

    private function _handleOrderAccess($object)
    {
        $api            = new ZippyCourses_Ontraport_PaymentGatewayAPI;
        $transaction_id = get_post_meta($object->getId(), 'ontraport_transaction_id', true);

        $transaction    = $api->getTransaction($transaction_id);

        $status = !is_wp_error($transaction) && isset($transaction->status) && $transaction->status == 1;

        return $status;
    }
}
