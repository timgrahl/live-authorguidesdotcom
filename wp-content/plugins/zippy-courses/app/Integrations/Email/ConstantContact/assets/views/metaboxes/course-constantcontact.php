<?php
/*
Id:         zippy-course-constantcontact
Name:       Constant Contact
Data:       email_lists, constantcontact_lists
Fields:     email_lists:json
Post Types: course
Context:    zippy_course_email_lists_main
Priority:   default
Type:       email_integration
Service:    constantcontact
Version:    1.0.0
*/
?>

<div class="vue" email-integration="constantcontact"></div>
<input type="hidden" name="email_lists" value="" />
