<?php
/**
 * Clear todos as you finish them
 *
 * @todo Make formatting PSR-2 compliant
 * @todo Make tabs 4 spaces
 * @todo self::request() needs to be fixed so that all args with defaults are at end and all required are at teh beginning
 * @todo Let's actually try to keep the request method similar to the others, where it's $url, $method, $args. Build the
 *       URL in the functions making the request.
 * @todo Define $this->base_url and then use $this->getURL() to automatically include api_key and api_output.  You can
 *       then use getUrl in a similar fashion to the MadMimi integration that basically takes an endpoint and appends it.
 *       That should shorten up the request method a bit.
 *
 * @todo Use self::getQueryArgs to build default arguments and then merge on others
 * @todo Let's add either a class comment using @example or function comments that explain the basic AC API.  I'll add
 *       those to MadMimi and MailChimp
 *
 * @todo (For Jonathan) - merge getList, getLists, getListsJSON, and getSettingsName into Zippy_EmailListIntegrationAPI
 *
 */
class ZippyCourses_ActiveCampaign_API extends Zippy_EmailListIntegrationAPI
{
    protected $service = 'activecampaign';

    public $api_key;
    public $api_url;

    public function __construct()
    {
        $settings = $this->getSettingValues();

        $this->api_key = $settings['api_key'];
        $this->api_url = $settings['api_url'];
    }

    public function validateSettings()
    {
        if (empty($this->api_key) ||
            empty($this->api_url)
        ) {
            return false;
        }

        return true;
    }

    public function getUrl()
    {
        return $this->api_url  . '/admin/api.php?';
    }

    public function request($method = 'GET', $action = '', $args = array())
    {
        if (!$this->validateSettings()) {
            return new WP_Error(__('Active Campaign settings are missing.', ZippyCourses::TEXTDOMAIN));
        }

        $params = array(
            'method'  => $method,
            'body'    => !empty($args['body']) ? $args['body'] : null
        );
        unset($args['body']);

        $conditions = array(
            'api_key'       => $this->api_key,
            'api_action'    => $action,
            'api_output'    => 'json'
        );

        $conditions = array_merge($conditions, $args);
        $queries    = http_build_query($conditions);
        $url        = $this->getUrl() . $queries;
        $response   = wp_remote_post($url, $params);

        if (is_wp_error($response)) {
            return $response;
        }

        return $response['response']['code'] == 200 ? $response : false;
    }

    private function getQueryArgs(array $args = array())
    {
    }

    public function fetchLists($id = 'all')
    {
        $zippy = Zippy::instance();

        $args = array(
            'full' =>  0,
            'ids'  =>  $id
        );

        $response = $this->request('GET', 'list_list', $args);
        $lists = $zippy->make('email_list_repository');
        
        if (!is_wp_error($response)) {
            if ($response) {
                $body = json_decode($response['body']);
                foreach ($body as $entry) {
                    if (is_object($entry)) {
                        $id = (int) $entry->id;
                        $name = (string) $entry->name;
                        $list = $zippy->make($this->getBinding('list'), array('id' => $id, 'name' => $name));
                        $lists->add($list);
                    }

                }
            }
        } else {
            $zippy->log->log($response->get_error_message());
        }

        return $lists;
    }

    public function getList($id)
    {
        $lists =  $this->getLists();
        $list = $lists->get($id);
        return $list;
    }
    
    public function getLists()
    {
        $zippy = Zippy::instance();

        if (false === ($lists = get_transient($this->getTransientName('lists')))) {
            $lists = $this->fetchLists();
            // It wasn't there, so regenerate the data and save the transient
            set_transient($this->getTransientName('lists'), $lists->toArray('name'), HOUR_IN_SECONDS);
        } else {
            $repository = $zippy->make('email_list_repository');

            foreach ($lists as $id => $name) {
                $list = $zippy->make($this->getBinding('list'), array('id' => $id, 'name' => $name));
                $repository->add($list);
            }
            $lists = $repository;
        }

        return $lists;
    }

    public function getListsJSON()
    {
        $output = array();

        $obj = new stdClass;
            $obj->text = '';
            $obj->value = 0;

        $output[] = $obj;
        
        foreach ($this->getLists()->all() as $list) {
            $obj = new stdClass;
            $obj->text = $list->getName();
            $obj->value = $list->getId();

            $output[] = $obj;
        }

        echo json_encode($output);
        die();
    }

    public function subscribe(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $zippy = Zippy::instance();

        $list_id = $list->id;

        $args = array(
            'body' => array(
                'email'                         => $student->email,
                'first_name'                    => $student->first_name,
                'last_name'                     => $student->last_name,
                // assign to lists:
                "p[$list_id]"                   => $list_id,
                "status[$list_id]"              => 1,
                "instantresponders[$list_id]"   => 1
            ),
        );

        $response = $this->request('POST', 'contact_add', $args);

        return $response && !is_wp_error($response) ? true : false;
    }

    public function getContact(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $zippy = Zippy::instance();
        
        $args = array(
            'filters[email]' => $student->email,
            'full' => 0
        );

        $response = $this->request('GET', 'contact_list', $args);

        if (!is_wp_error($response)) {
            $body = json_decode($response['body']);
            return isset($body->{0}) ? $body->{0} : false;
        } else {
            $zippy->log->log($response->get_error_message());
            return false;
        }
    }

    public function unsubscribe(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $contact = $this->getContact($student, $list);
        $list_id = $list->id;
        $response = '';

        if ($contact) {
            $args = array(
                'ids'               => $contact->id,
                "listids[$list_id]" => $list_id
            );

            $response = $this->request('GET', 'contact_delete_list', $args);
        }
        
        return !empty($response) && !is_wp_error($response)  ? true : false;
    }
    
    public function getSettingValues()
    {
        $default_keys = array('api_key', 'api_url');

        $settings = (array) get_option($this->getSettingsName());

        foreach ($default_keys as $key) {
            if (!isset($settings[$key])) {
                $settings[$key] = '';
            }
        }

        return $settings;
    }

    public function getSettingsName()
    {
        return 'zippy_' . $this->service . '_email_integration_settings';
    }
}
