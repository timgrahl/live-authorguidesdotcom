<?php
/*
Id: zippy-product-activecampaign
Name: Active Campaign
Data:       email_lists, activecampaign_lists
Fields:     email_lists:json
Post Types: product
Context: zippy_product_email_lists_main
Priority: default
Type: email_integration
Service: activecampaign
Version:     1.0.0
*/
?>

<div class="vue" email-integration="activecampaign"></div>
<input type="hidden" name="email_lists" value="" />
