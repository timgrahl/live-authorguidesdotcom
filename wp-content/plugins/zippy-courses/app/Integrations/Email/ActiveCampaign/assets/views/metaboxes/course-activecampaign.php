<?php
/*
Id:         zippy-course-activecampaign
Name:       ActiveCampaign
Data:       email_lists, activecampaign_lists
Fields:     email_lists:json
Post Types: course
Context:    zippy_course_email_lists_main
Priority:   default
Type:       email_integration
Service:    activecampaign
Version:    1.0.0
*/
?>

<div class="vue" email-integration="activecampaign"></div>
<input type="hidden" name="email_lists" value="" />
