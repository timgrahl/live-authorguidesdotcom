<?php
/**
 * Id: infusionsoft
 * File Type: email-integration
 * Class: ZippyCourses_Infusionsoft_EmailListIntegration
 *
 * @since 1.0.0
 */

class ZippyCourses_Infusionsoft_EmailListIntegration extends Zippy_EmailListIntegration
{
    public $settings = array();
    
    public $api;

    private $path;

    public function __construct()
    {
        $this->id       = 'infusionsoft';
        $this->service  = 'infusionsoft';
        $this->name     = 'Infusionsoft';
        
        $this->path     = plugin_dir_path(__FILE__);
        $this->url      = plugin_dir_url(__FILE__);

        $this->settings = (array) get_option('zippy_infusionsoft_email_integration_settings');
        $this->enabled  = isset($this->settings['enabled']) ? (bool) $this->settings['enabled'] : false;

        add_filter('zippy_classmap', array($this, 'map'));
        add_filter('zippy_fetch_meta_data', array($this, 'metaboxLists'), 10, 2);
        
        parent::__construct();
    }

    public function connectionInfo($conn)
    {
        return array('connectionName:' . $this->api->app_id . ':i:' . $this->api->api_key . ':This is the connection for ' . $this->api->app_id . '.infusionsoft.com');
    }

    public function map($classes)
    {
        $classes['ZippyCourses_Infusionsoft_EmailListIntegrationAPI'] = $this->path . 'lib/API.php';
        $classes['ZippyCourses_Infusionsoft_EmailList'] = $this->path . 'lib/EmailList.php';

        return $classes;
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $repository = $zippy->make('email_list_integration_repository');
        $repository->add($this);

        parent::register();

    }

    public function ajax()
    {
        add_action('wp_ajax_get_infusionsoft_lists', array($this->api, 'getListsJSON'));
    }

    /**
     * Setup the integration with the appropriate settings and hooks,
     * such as landing page integration list and settings
     * @return void
     */
    public function setup()
    {
        $zippy = Zippy::instance();
        
        $this->api = new ZippyCourses_Infusionsoft_EmailListIntegrationAPI;

        $this->ajax();

        $zippy->bind($this->api->getBinding('list'), 'ZippyCourses_Infusionsoft_EmailList');

        add_filter('zippy_metaboxes', array($this, 'metaboxes'));
        add_action('init', array($this, 'settings'));
        
        // add_filter('zippy_infusionsoft_connection_data', array($this, 'connectionInfo'));
    }

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    public function settings()
    {
        $zippy = Zippy::instance();

        $settings_pages = $zippy->make('settings_pages_repository');
        $settings_page = $settings_pages->fetch('zippy_settings_email_lists');

        $section = $settings_page->createSection($this->getSettingsName(), 'Infusionsoft');
            $section->createField(
                'enabled',
                __('Enabled?', ZippyCourses::TEXTDOMAIN),
                'select',
                array(
                    __('No', ZippyCourses::TEXTDOMAIN),
                    __('Yes', ZippyCourses::TEXTDOMAIN)
                )
            );
            $section->createField('app_id', __('App ID', ZippyCourses::TEXTDOMAIN));
            $section->createField('api_key', __('API Key', ZippyCourses::TEXTDOMAIN));
    }

    /**
     * Integrate with the correct metaboxes
     * @return void
     */
    public function metaboxes($metaboxes)
    {
        if (!$this->enabled) {
            return $metaboxes;
        }

        $dir = $this->path . 'assets/views/metaboxes/';
        $files = array_diff(scandir($dir), array('..', '.'));

        foreach ($files as $key => &$file) {
            if (!is_dir($dir . $file)) {
                $file = $dir . $file;
            } else {
                unset($files[$key]);
            }
        }

        return array_merge($metaboxes, $files);
    }

    /**
     * Get the Lists repository
     * @return Zippy_EmailListRepository
     */
    public function getLists()
    {
        return $this->lists;
    }

    /**
     * Make sure that tabs are set up where needed
     * @return void
     */
    protected function addTabs()
    {
        if (!$this->enabled) {
            return;
        }
        
        add_filter('zippy_enable_course_email_list_tab', '__return_true');
        add_filter('zippy_enable_product_email_list_tab', '__return_true');
    }
}
