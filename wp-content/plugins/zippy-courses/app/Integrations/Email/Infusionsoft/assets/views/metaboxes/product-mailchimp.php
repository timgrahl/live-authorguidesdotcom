<?php
/*
Id: zippy-product-infusionsoft
Name: Infusionsoft
Data: email_lists, infusionsoft_lists
Fields: email_lists:json
Post Types: product
Context: zippy_product_email_lists_main
Priority: default
Type: email_integration
Service: infusionsoft
Version:     1.0.0
*/
?>

<div class="vue" email-integration="infusionsoft"></div>

<input type="hidden" name="email_lists" value="" />
