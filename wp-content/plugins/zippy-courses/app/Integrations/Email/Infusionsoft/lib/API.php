<?php

if (!class_exists('iSDK')) {
    require_once(ZippyCourses::$path . '/vendor/infusionsoft/isdk.php');
}
class ZippyCourses_Infusionsoft_EmailListIntegrationAPI extends Zippy_EmailListIntegrationAPI
{
    protected $service = 'infusionsoft';

    public $api_key;
    public $app_id;
    private $app;

    public function __construct()
    {
        $settings = $this->getSettingValues();

        $this->api_key      = $settings['api_key'];
        $this->app_id       = $settings['app_id'];
    }

    public function app()
    {
        $app = new iSDK($this->api_key, $this->app_id);
        
        if (!$app->cfgCon("connectionName")) {
            throw new Exception('Could not connect to InfusionSoft App');
        }

        return $app;
    }

    /**
     * Fetches lists and converts them into an email list repository
     * @since  1.0.0
     * @param  string $id a single id number, or 'all' for all ids
     * @return object Zippy_EmailListRepository
     */
    public function fetchLists()
    {
        $zippy = Zippy::instance();

        $groups = $this->getContactGroup();

        $lists = $zippy->make('email_list_repository');

        if ($groups) {
            foreach ($groups as $group) {
                $list = $zippy->make($this->getBinding('list'), array('id' => $group['Id'], 'name' => $group['GroupName']));
                $lists->add($list);
            }
        }

        return $lists;
    }

    /**
     * Get a single list object
     * @since  1.0.0
     * @param  int $id the List's ID Number
     * @return object Zippy_EmailList
     */
    public function getList($id)
    {
        $lists = $this->getLists();
        foreach ($lists->items as $list) {
            if ($list->id == $id) {
                return $list;
            }
        }
    }

    /**
     * Get all Lists
     * @since  1.0.0
     * @return object Zippy_EmailListRepository
     */
    public function getLists()
    {
        $zippy = Zippy::instance();
        
        if (false === ($lists = get_transient($this->getTransientName('lists')))) {
            // It wasn't there, so regenerate the data and save the transient
            $lists = $this->fetchLists();
            set_transient($this->getTransientName('lists'), $lists->toArray('name'), HOUR_IN_SECONDS);
        } else {
            $repository = $zippy->make('email_list_repository');

            foreach ($lists as $id => $name) {
                $list = $zippy->make($this->getBinding('list'), array('id' => $id, 'name' => $name));
                $repository->add($list);
            }

            $lists = $repository;
        }

        return $lists;
    }

    /**
     * Get lists as a JSON Object
     * @since  1.0.0
     * @return json
     */
    public function getListsJSON()
    {
        $output = array();

        $obj = new stdClass;
            $obj->text = '';
            $obj->value = 0;

        $output[] = $obj;
        
        foreach ($this->getLists()->all() as $list) {
            $obj = new stdClass;
            $obj->text = $list->getName();
            $obj->value = $list->getId();

            $output[] = $obj;
        }

        echo json_encode($output);
        die();
    }

    /**
     * Add a student to a list
     * @since  1.0.0
     * @param  ZippyCourses_Student
     * @param  Zippy_EmailList
     * @return bool
     */
    public function subscribe(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $zippy = Zippy::instance();

        $tag = $this->getContactGroup($list->getId());
        $contact = $this->getContact($student, $list);

        if (!$tag || !$contact) {
            return false;
        }

        $app->optIn($student->getEmail(), "Joined a course on " . get_bloginfo('name'));

        return $app->grpAssign($contact['Id'], $tag['Id']);
    }

    /**
     * Get a single contact from the list
     * @since  1.0.0
     * @param  ZippyCourses_Student
     * @param  Zippy_EmailList
     * @return object Single Subscriber
     */
    public function getContact(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $contact = $this->_getContact('Email', $student->getEmail());

        if ($contact === false) {
            $contact_id = $this->addContact($student);
            $contact = $this->_getContact('Email', $student->getEmail());
        }

        return $contact;
    }

    /**
     * Remove a student from a list
     * @since  1.0.0
     * @param  ZippyCourses_Student
     * @param  Zippy_EmailList
     * @return bool
     */
    public function unsubscribe(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $zippy = Zippy::instance();

        $tag = $this->getContactGroup($list->getId());
        $contact = $this->getContact($student, $list);

        if (!$tag || !$contact) {
            return false;
        }

        return $app->grpRemove($contact['Id'], $tag['Id']);
    }

    /**
     * Get settings values for Integration
     * @since  1.0.0
     * @return array settings
     */
    public function getSettingValues()
    {
        $default_keys = array('api_key', 'app_id');

        $settings = (array) get_option($this->getSettingsName());

        foreach ($default_keys as $key) {
            if (!isset($settings[$key])) {
                $settings[$key] = '';
            }
        }

        return $settings;
    }

    /**
     * Get settings name
     * @since  1.0.0
     * @return string Settings Name
     */
    public function getSettingsName()
    {
        return 'zippy_' . $this->service . '_email_integration_settings';
    }

    public function getContactGroup($id = '%')
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }
        
        $page = 0;
        $completed = false;
        $all_results = array();
        
        while (!$completed) {
            $results = $app->dsQuery('ContactGroup', 1000, $page, array('Id' => $id ), array(
                'GroupCategoryId',
                'GroupDescription',
                'GroupName',
                'Id'
            ));

            $all_results = array_merge($all_results, $results);

            if (count($results) < 1000) {
                $completed = true;
            } else {
                $page++;
            }
        }

        return !empty($all_results)
            ? $id == '%'
                ? $all_results
                : $all_results[0]
            : false;
    }

    private function _getContact($field = 'Id', $value = '%')
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $results = $app->dsQuery('Contact', 5000, 0, array($field => $value), array(
            'AccountId',
            'Address1Type',
            'Address2Street1',
            'Address2Street2',
            'Address2Type',
            'Address3Street1',
            'Address3Street2',
            'Address3Type',
            'Anniversary',
            'AssistantName',
            'AssistantPhone',
            'BillingInformation',
            'Birthday',
            'City',
            'City2',
            'City3',
            'Company',
            'CompanyID',
            'ContactNotes',
            'ContactType',
            'Country',
            'Country2',
            'Country3',
            'CreatedBy',
            'DateCreated',
            'Email',
            'EmailAddress2',
            'EmailAddress3',
            'Fax1',
            'Fax1Type',
            'Fax2',
            'Fax2Type',
            'FirstName',
            'Groups',
            'Id',
            'JobTitle',
            'LastName',
            'LastUpdated',
            'LastUpdatedBy',
            'LeadSourceId',
            'Leadsource',
            'MiddleName',
            'Nickname',
            'OwnerID',
            'Password',
            'Phone1',
            'Phone1Ext',
            'Phone1Type',
            'Phone2',
            'Phone2Ext',
            'Phone2Type',
            'Phone3',
            'Phone3Ext',
            'Phone3Type',
            'Phone4',
            'Phone4Ext',
            'Phone4Type',
            'Phone5',
            'Phone5Ext',
            'Phone5Type',
            'PostalCode',
            'PostalCode2',
            'PostalCode3',
            'ReferralCode',
            'SpouseName',
            'State',
            'State2',
            'State3',
            'StreetAddress1',
            'StreetAddress2',
            'Suffix',
            'Title',
            'Username',
            'Validated',
            'Website',
            'ZipFour1',
            'ZipFour2',
            'ZipFour3'
        ));

        return !empty( $results ) ? $results[0] : false;
    }

    public function addContact(ZippyCourses_Student $student)
    {
        $app = null;

        try {
            $app = $this->app();
        } catch (Exception $e) {
        }

        if ($app == null) {
            return false;
        }

        $id = $app->dsAdd('Contact', array(
            'Email'            => $student->getEmail(),
            'FirstName'        => $student->getFirstName(),
            'LastName'         => $student->getLastName()
        ));

        return $id;
    }
}
