<?php
/*
Id: zippy-course-mailchimp
Name: MailChimp
Data: email_lists, mailchimp_lists
Fields: email_lists:json
Post Types: course
Context: zippy_course_email_lists_main
Priority: default
Type: email_integration
Service: mailchimp
Version:     1.0.0
*/
?>

<div class="vue" email-integration="mailchimp"></div>

<input type="hidden" name="email_lists" value="" />
