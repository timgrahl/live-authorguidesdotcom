<?php
/*
Id: zippy-product-getresponse
Name: GetResponse
Data:       email_lists, getresponse_lists
Fields:     email_lists:json
Post Types: product
Context: zippy_product_email_lists_main
Priority: default
Type: email_integration
Service: getresponse
Version:     1.0.0
*/
?>

<div class="vue" email-integration="getresponse"></div>
<input type="hidden" name="email_lists" value="" />
