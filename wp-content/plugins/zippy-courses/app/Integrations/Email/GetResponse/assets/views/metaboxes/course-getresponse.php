<?php
/*
Id:         zippy-course-getresponse
Name:       GetResponse
Data:       email_lists, getresponse_lists
Fields:     email_lists:json
Post Types: course
Context:    zippy_course_email_lists_main
Priority:   default
Type:       email_integration
Service:    getresponse
Version:    1.0.0
*/
?>

<div class="vue" email-integration="getresponse"></div>
<input type="hidden" name="email_lists" value="" />
