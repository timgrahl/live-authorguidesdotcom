<?php

class ZippyCourses_EmailListIntegrations
{
    public function __construct()
    {
        $this->prepare();
    }

    public function prepare()
    {
        $zippy = Zippy::instance();

        $files = $zippy->utilities->file->getIntegrationFiles(
            'email-integration',
            'EmailIntegration',
            ZippyCourses::$path . 'app/Integrations/Email/'
        );

        foreach ($files as $file) {
            $item = $zippy->makeFromFile($file);
        }
    }
}
