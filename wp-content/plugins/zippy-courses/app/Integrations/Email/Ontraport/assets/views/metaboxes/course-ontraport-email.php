<?php
/*
Id: zippy-course-ontraport-email
Name: Ontraport
Data: email_lists, ontraport_lists
Fields: email_lists:json
Post Types: course
Context: zippy_course_email_lists_main
Priority: default
Type: email_integration
Service: ontraport
Version:     1.0.0
*/
?>

<div class="vue" email-integration="ontraport"></div>

<input type="hidden" name="email_lists" value="" />
