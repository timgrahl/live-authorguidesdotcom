<?php
/*
Id: zippy-product-ontraport-email
Name: Ontraport
Data: email_lists, ontraport_lists
Fields: email_lists:json
Post Types: product
Context: zippy_product_email_lists_main
Priority: default
Type: email_integration
Service: ontraport
Version:     1.0.0
*/
?>

<div class="vue" email-integration="ontraport"></div>

<input type="hidden" name="email_lists" value="" />
