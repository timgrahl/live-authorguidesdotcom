<?php
/**
 * This is an adapter for our Ontraport adapter.  Ontraport stores requisite information across a variety of API
 * calls, so we create this object to fetch and normalize all of the data
*/
class ZippyCourses_Ontraport_Contact
{
    public $data;
    public $id;
    public $tags;

    public function __construct($data)
    {
        if (is_numeric($data)) {
            $this->id = $data;
            $this->fetch($data);
        } else {
            if (is_object($data)) {
                $this->data = $data;
            }
        }
    }

    public function getTags()
    {
        if ($this->tags === null && isset($this->data->contact_cat)) {
            $this->tags = array_values(array_filter(explode('*/*', $this->data->contact_cat)));
        }

        return $this->tags;
    }

    public function fetch($id)
    {
        $api = new ZippyCourses_Ontraport_PaymentGatewayAPI;

        $data = $api->getContact($id);

        if (!is_wp_error($data)) {
            $this->data = $data;
        }

        return $this;
    }
}
