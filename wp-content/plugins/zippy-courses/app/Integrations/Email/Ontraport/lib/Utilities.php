<?php

class ZippyCourses_OntraportGateway_Utilities
{
    public function getProductsList()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = %s", 'ontraport_product_id');
        $used_ids = $wpdb->get_col($sql);

        $api = new ZippyCourses_Ontraport_PaymentGatewayAPI;

        $output = array();

        $products = $api->getProducts();

        foreach ($products as $product) {
            $output[$product->id] = array(
                'id'    => $product->id,
                'name'  => $product->name,
                'used'  => in_array($product->id, $used_ids)
            );
        }

        return $output;
    }

    public function getTagsList()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = %s", 'ontraport_tag_id');
        $used_ids = $wpdb->get_col($sql);

        $api = new ZippyCourses_Ontraport_PaymentGatewayAPI;

        $output = array();

        $tags = $api->getTags();

        foreach ($tags as $tag) {
            $output[$tag->tag_id] = array(
                'id'    => $tag->tag_id,
                'name'  => $tag->tag_name,
                'used'  => in_array($tag->tag_id, $used_ids)
            );
        }

        return $output;
    }
}
