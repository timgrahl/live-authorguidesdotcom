<?php

class ZippyCourses_Ontraport_API extends Zippy_EmailListIntegrationAPI
{
    private $base_url = 'https://api.ontraport.com/1/';

    protected $service = 'ontraport';

    public $api_key;
    public $app_id;

    public function __construct()
    {
        $settings = $this->getSettingValues();

        $this->api_key      = $settings['api_key'];
        $this->app_id       = $settings['app_id'];
    }

    public function validateSettings()
    {
        if (empty($this->api_key) ||
            empty($this->app_id)
        ) {
            return false;
        }

        return true;
    }

    /**
     * Basic endpoint builder
     *
     * @since   1.0.0
     *
     * @param   string $endpoint
     *
     * @return  string
     */
    private function getUrl($endpoint)
    {
        return $this->base_url . $endpoint;
    }

    /**
     * Fetches lists and converts them into an email list repository
     * @since  1.0.0
     * @param  string $id a single id number, or 'all' for all ids
     * @return object Zippy_EmailListRepository
     */
    public function fetchLists()
    {
        $zippy = Zippy::instance();

        $url = $this->getUrl('objects');
        $lists = $zippy->make('email_list_repository');


        $tags = array();
        $start = 0;
        $range = 50;
        $fetch = true;

        if (!$this->validateSettings()) {
            return $lists;
        }

        while ($fetch) {
            $params = $this->buildParams(array(
                'objectID' => 14,
                'range' => $range,
                'start' => $start
            ));

            $results = $this->handleResponse(wp_remote_post($url, $params));
            if (!is_wp_error($results)) {
                if (is_array($results)) {
                    $tags = array_merge($tags, $results);
                } else {
                    $fetch = false;
                }

                $start += $range;

                if (count($results) < $range) {
                    $fetch = false;
                }
            }
        }
        foreach ($tags as $tag) {
            $list = $zippy->make($this->getBinding('list'), array('id' => $tag->tag_id, 'name' => $tag->tag_name));
            $lists->add($list);
        }

        
        return $lists;
    }

    /**
     * Get a single list object
     * @since  1.0.0
     * @param  int $id the List's ID Number
     * @return object Zippy_EmailList
     */
    public function getList($id)
    {
        $lists = $this->getLists();
        foreach ($lists->items as $list) {
            if ($list->id == $id) {
                return $list;
            }
        }
    }

    /**
     * Get all Lists
     * @since  1.0.0
     * @return object Zippy_EmailListRepository
     */
    public function getLists()
    {
        $zippy = Zippy::instance();
        
        if (false === ($lists = get_transient($this->getTransientName('lists')))) {
            // It wasn't there, so regenerate the data and save the transient
            $lists = $this->fetchLists();
            set_transient($this->getTransientName('lists'), $lists->toArray('name'), HOUR_IN_SECONDS);
        } else {
            $repository = $zippy->make('email_list_repository');

            foreach ($lists as $id => $name) {
                $list = $zippy->make($this->getBinding('list'), array('id' => $id, 'name' => $name));
                $repository->add($list);
            }

            $lists = $repository;
        }

        return $lists;
    }

    /**
     * Get lists as a JSON Object
     * @since  1.0.0
     * @return json
     */
    public function getListsJSON()
    {
        $output = array();

        $obj = new stdClass;
            $obj->text = '';
            $obj->value = 0;

        $output[] = $obj;
        
        foreach ($this->getLists()->all() as $list) {
            $obj = new stdClass;
            $obj->text = $list->getName();
            $obj->value = $list->getId();

            $output[] = $obj;
        }

        echo json_encode($output);
        die();
    }

    /**
     * Add a student to a list
     * @since  1.0.0
     * @param  ZippyCourses_Student
     * @param  Zippy_EmailList
     * @return bool
     */
    public function subscribe(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $zippy = Zippy::instance();

        if (!$this->validateSettings()) {
            return new WP_Error(__('Your Ontraport integration settings are missing.', ZippyCourses::TEXTDOMAIN));
        }

        $url = $this->getUrl('objects/tag');

        $contact = $this->getContact($student, $list);

        if (!is_wp_error($contact) && isset($contact->id)) {
            $params = $this->buildParams(array(
                'objectID'    => 0,
                'ids'         => $contact->id,
                'add_list'    => $list->getId()
            ), 'PUT');
            
            $response = $this->handleResponse(wp_remote_post($url, $params));

            if (is_wp_error($response)) {
                return false;
            }
        } else {
            return false;
        }
        
        return true;
    }

    /**
     * Add a contact to the list
     *
     * @since  1.0.0
     *
     * @param  ZippyCourses_Student
     * @param  Zippy_EmailList
     * @return json|null
     */
    public function addContact(ZippyCourses_Student $student)
    {
        if (!$this->validateSettings()) {
            return new WP_Error(__('Your Ontraport integration settings are missing.', ZippyCourses::TEXTDOMAIN));
        }

        $url = $this->getUrl('objects');

        $params = $this->buildParams(array(
            'objectID'  => 0,
            'email'     => $student->getEmail(),
            'firstname' => $student->getFirstName(),
            'lastname'  => $student->getLastName(),
        ), 'POST');

        $response = $this->handleResponse(wp_remote_post($url, $params));

        return is_wp_error($response) ? $response : $response;
    }

    /**
     * Get a single contact from the list
     * @since  1.0.0
     * @param  ZippyCourses_Student
     * @param  Zippy_EmailList
     * @return json|null
     */
    public function getContact(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        if (!$this->validateSettings()) {
            return new WP_Error(__('Your Ontraport integration settings are missing.', ZippyCourses::TEXTDOMAIN));
        }

        $url = $this->getUrl('objects');

        $params = $this->buildParams(array(
            'objectID' => 0,
            'search' => $student->getEmail()
        ));

        $response = $this->handleResponse(wp_remote_post($url, $params));

        return !is_wp_error($response)
            ? isset($response[0])
                ? $response[0]
                : $this->addContact($student)
            : $response;
    }

    /**
     * Remove a student from a list
     * @since  1.0.0
     * @param  ZippyCourses_Student
     * @param  Zippy_EmailList
     * @return bool
     */
    public function unsubscribe(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $zippy = Zippy::instance();

        if (!$this->validateSettings()) {
            return new WP_Error(__('Your Ontraport integration settings are missing.', ZippyCourses::TEXTDOMAIN));
        }

        $url = $this->getUrl('objects/tag');

        $contact = $this->getContact($student, $list);

        if (!is_wp_error($contact) && isset($contact->id)) {
            $params = $this->buildParams(array(
                'objectID'     => 0,
                'ids'          => $contact->id,
                'remove_list'  => $list->getId()
            ), 'DELETE');
            
            $response = $this->handleResponse(wp_remote_post($url, $params));

            if (is_wp_error($response)) {
                return false;
            }
        } else {
            return false;
        }
        
        return true;
    }

    /**
     * Get settings values for Integration
     * @since  1.0.0
     * @return array settings1
     */
    public function getSettingValues()
    {
        $default_keys = array('api_key', 'app_id');

        $settings = (array) get_option($this->getSettingsName());

        foreach ($default_keys as $key) {
            if (!isset($settings[$key])) {
                $settings[$key] = '';
            }
        }

        return $settings;
    }

    /**
     * Get settings name
     * @since  1.0.0
     * @return string Settings Name
     */
    public function getSettingsName()
    {
        return 'zippy_' . $this->service . '_email_integration_settings';
    }


    /**
     * Get a list of tags
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getTags()
    {
        if (!$this->validateSettings()) {
            return new WP_Error(__('Your Ontraport integration settings are missing.', ZippyCourses::TEXTDOMAIN));
        }

        $url = $this->getUrl('objects');

        $tags = array();

        $start = 0;
        $range = 50;
        $fetch = true;

        while ($fetch) {
            $params = $this->buildParams(array(
                'objectID' => 14,
                'range' => $range,
                'start' => $start
            ));

            $results = $this->handleResponse(wp_remote_post($url, $params));

            if (!is_wp_error($results)) {
                if (is_array($results)) {
                    $tags = array_merge($tags, $results);
                } else {
                    $fetch = false;
                }

                $start += $range;

                if (count($results) < $range) {
                    $fetch = false;
                }
            }
        }

        return $tags;
    }

    /**
     * Get a purchase
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function getTag($id)
    {
        $url = $this->getUrl('object');

        $params = $this->buildParams(array(
            'objectID' => 14,
            'id' => $id
        ));

        return $this->handleResponse(wp_remote_post($url, $params));
    }


    /**
     * Build the parameters from a default, plus the data sent by the calling method
     *
     * @since   1.0.0
     *
     * @param   array   $body       The parameters being sent in the request body
     * @param   string  $method     The HTTP method
     *
     * @return  JSON|WP_Error
     */
    private function buildParams(array $body = array(), $method = 'GET')
    {
        return array(
            'method'        => $method,
            'timeout'       => 10,
            'redirection'   => 5,
            'httpversion'   => '1.1',
            'blocking'      => true,
            'headers'       => array(
                'Api-Appid' => $this->app_id,
                'Api-Key'   => $this->api_key
            ),
            'body'          => $body
        );
    }

    /**
     * Handle a wp_remote_post call, either returning a json_decoded version of the body,
     * or WP_Error in the event of a non-200 status code.
     *
     * @since   1.0.0
     *
     * @param   array   $response   The results form a wp_remote_post request
     *
     * @return  JSON|WP_Error
     */
    public function handleResponse($response)
    {
        $zippy = Zippy::instance();

        if (!is_wp_error($response)) {
            if ($response['response']['code'] == 200) {
                $body = json_decode($response['body']);

                return $zippy->utilities->deepJsonDecode($body->data);
            } else {
                $error = new WP_Error($response['body']);
            }
        } else {
        }
    }
}
