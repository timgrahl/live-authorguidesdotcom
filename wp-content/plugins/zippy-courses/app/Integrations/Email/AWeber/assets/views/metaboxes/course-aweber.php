<?php
/*
Id:         zippy-course-aweber
Name:       AWeber
Data:       email_lists, aweber_lists
Fields: email_lists:json
Post Types: course
Context:    zippy_course_email_lists_main
Priority:   default
Type:       email_integration
Service:    aweber
Version:    1.0.0
*/
?>

<div class="vue" email-integration="aweber"></div>
<input type="hidden" name="email_lists" value="" />
