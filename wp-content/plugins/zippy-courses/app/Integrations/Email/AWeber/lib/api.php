<?php
class ZippyCourses_Aweber_API extends Zippy_EmailListIntegrationAPI
{
    protected $service = 'aweber';

    public $auth_code;
    public $aweber;

    public $consumer_key;
    public $consumer_secret;
    public $access_token;
    public $access_secret;

    public $account;
    public $settings_name;
    public $settings;

    public function __construct()
    {
        $this->settings = $this->getSettingValues();
        $this->auth_code = $this->settings['auth_code'];
        
        $this->settings_name = $this->getSettingsName();

        $this->mapKeys();
        $this->aweber = new Zippy_AWeberAPI($this->consumer_key, $this->consumer_secret);
        
        add_filter("pre_update_option_{$this->settings_name}", array($this, 'processAuth'), 10, 3);
    }

    public function validateSettings()
    {
        if (empty($this->consumer_key) ||
            empty($this->consumer_secret) ||
            empty($this->access_token) ||
            empty($this->access_secret) ||
            empty($this->auth_code)
        ) {
            return false;
        }

        return true;
    }

    /**
     * Prepare the Authorized User's account
     * @since  1.0.0
     * @return void
     */
    public function getAccount()
    {
        $zippy = Zippy::instance();
        
        if (!$this->validateSettings()) {
            return false;
        }

        if ($this->account !== null) {
            return $this->account;
        }

        try {
            $this->account = $this->aweber->getAccount($this->access_token, $this->access_secret);
        } catch (Exception $e) {
            $message = 'Caught Exception: '. $e->getMessage() . '\n';
            $zippy->log->log($message);
        }

        return $this->account;
    }

    /**
     * Map the Consumer and Access Keys
     * @return void
     */
    public function mapKeys()
    {
        $keys = get_option($this->settings_name);
        $this->consumer_key     = !empty($keys['consumer_key']) ? $keys['consumer_key'] : '';
        $this->consumer_secret  = !empty($keys['consumer_secret']) ? $keys['consumer_secret'] : '';
        $this->access_token     = !empty($keys['access_key']) ? $keys['access_key'] : '';
        $this->access_secret    = !empty($keys['access_secret']) ? $keys['access_secret'] : '';
    }

    /**
     * Process OAuth Authorization and save access/consumer keys to database
     * @since  1.0.0
     * @param  array $value
     * @param  array $old_value
     * @return void
     */
    public function processAuth($value, $old_value)
    {
        $zippy = Zippy::instance();

        $old_auth = isset($old_value['auth_code']) ? $old_value['auth_code'] : '';

        // If the values are the same, make no changes
        if (isset($value['auth_code']) && $old_auth !== $value['auth_code']) {
            // If the values are different, update via AWeber and save new tokens
            try {
                $returned_credentials = Zippy_AWeberAPI::getDataFromAweberID($value['auth_code']);
                list($consumerKey, $consumerSecret, $accessKey, $accessSecret) = $returned_credentials;
                $value['consumer_key'] = $consumerKey;
                $value['consumer_secret'] = $consumerSecret;
                $value['access_key'] = $accessKey;
                $value['access_secret'] = $accessSecret;
            } catch (Exception $e) {
                $message = 'Caught Exception: '. $e->getMessage() . '\n';
                $zippy->log->log($message);
            }
        } else {
            $value['consumer_key']      = isset($old_value['consumer_key']) ? $old_value['consumer_key'] : '';
            $value['consumer_secret']   = isset($old_value['consumer_secret']) ? $old_value['consumer_secret'] : '';
            $value['access_key']        = isset($old_value['access_key']) ? $old_value['access_key'] : '';
            $value['access_secret']     = isset($old_value['access_secret']) ? $old_value['access_secret'] : '';
        }

        return $value;
    }

    /**
     * Generates the URL used for an API Call
     * @since   1.0.0
     * @param  string $endpoint API Action
     * @return string URL for API Call
     */
    public function getUrl($endpoint)
    {
        return $this->base_url . $endpoint;
    }

    /**
     * Makes the API Request
     * @since  1.0.0
     */
    public function request($url, $method = 'GET', array $args = array())
    {
        // Requests are handled by the Zippy_AweberAPI and Oauth_Application classes
        // See OAuthApplication::request
    }

    /**
     * Convert arguments to query string and add default arguments
     * @since  1.0.0
     */
    private function getQueryArgs(array $args = array())
    {
        // Requests and QueryArgs are handled by the Zippy_AweberAPI class
    }

    /**
     * Fetches lists and converts them into an email list repository
     * @since  1.0.0
     * @param  string $id a single id number, or 'all' for all ids
     * @return object Zippy_EmailListRepository
     */
    public function fetchLists($id = 'all')
    {
        $zippy = Zippy::instance();
        if ($this->getAccount()) {
            $lists = $zippy->make('email_list_repository');

            if ($this->account !== null) {
                $entries = $this->account->lists->find(array());
                
                if ($lists) {
                    $entries = $entries->data['entries'];
                    
                    foreach ($entries as $entry) {
                        if (is_array($entry)) {
                            $id = $entry['id'];
                            $name = $entry['name'];
                            $list = $zippy->make($this->getBinding('list'), array('id' => $id, 'name' => $name));
                            $lists->add($list);
                        }

                    }
                }
            }

            return $lists;
        }

        return array();
    }

    /**
     * Get a single list object
     * @since  1.0.0
     * @param  int $id the List's ID Number
     * @return object Zippy_EmailList
     */
    public function getList($id)
    {
        $lists  = $this->getLists();
        if (!is_object($lists)) {
            return null;
        }
        
        $list   = $lists->get($id);

        return $list;
    }
    
    /**
     * Get all Lists
     * @since  1.0.0
     * @return object Zippy_EmailListRepository
     */
    public function getLists()
    {
        $zippy = Zippy::instance();
        
        if (false === ($lists = get_transient($this->getTransientName('lists')))) {
            $lists = $this->fetchLists();
            if (is_object($lists)) {
                $val = $lists->toArray('name');
                set_transient($this->getTransientName('lists'), $val, HOUR_IN_SECONDS);
            } else {
                // If this fails, initialize an empty repository instead
                $lists = $zippy->make('email_list_repository');
            }
        } else {
            $repository = $zippy->make('email_list_repository');

            foreach ($lists as $id => $name) {
                $list = $zippy->make($this->getBinding('list'), array('id' => $id, 'name' => $name));
                $repository->add($list);
            }

            $lists = $repository;
        }

        return $lists;
    }

    /**
     * Get lists as a JSON Object
     * @since  1.0.0
     * @return json
     */
    public function getListsJSON()
    {
        $output = array();

        $obj = new stdClass;
            $obj->text = '';
            $obj->value = 0;

        $output[] = $obj;
        
        foreach ($this->getLists()->all() as $list) {
            $obj = new stdClass;
            $obj->text = $list->getName();
            $obj->value = $list->getId();

            $output[] = $obj;
        }

        echo json_encode($output);
        die();
    }

    /**
     * Get a list's subscribers
     * @param  string $list_id The id of the list
     * @return  Object AWeber Subscribers object
     */
    public function getSubscribers($list_id)
    {
        $zippy = Zippy::instance();

        if (($subscribers = $zippy->cache->get('aweber_subscribers_' . $list_id)) !== null) {
            return $subscribers;
        }

        if ($this->getAccount()) {
            $lists = $this->account->lists->find(array('name' => "awlist$list_id"));

            $url = $lists->data['entries'][0]['subscribers_collection_link'];
            $subscribers = $this->account->loadFromUrl($url);

            $zippy->cache->set('aweber_subscribers_' . $list_id, $subscribers);

            // Todo: Add test for if response fails
            return $subscribers;
        }

        return null;
    }

    /**
     * Add a student to a list
     * @since  1.0.0
     * @param  ZippyCourses_Student
     * @param  Zippy_EmailList
     * @return bool
     */
    public function subscribe(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $zippy = Zippy::instance();
   
        $result = false;

        if ($this->getAccount()) {
            $account_id = $this->account->id;
            $list_id = $list->id;
            
            $subscribers = $this->getSubscribers($list_id);
            if ($subscribers === null || !is_object($subscribers)) {
                return false;
            }

            $args = array(
                    'email' => $student->getEmail(),
                    'name'  => $student->getFullName()
            );

            try {
                $result = $subscribers->create($args);
            } catch (Exception $e) {
                $message = 'Caught Exception: '. $e->getMessage() . '\n';
                $zippy->log->log($message);
                $result = false;
            }

            if (!isset($subscribed[$list_id])) {
                $subscribed[$list_id] = array();
            }

            $subscribed[$list_id][] = $student->getEmail();

            $zippy->cache->set('aweber_subscribed', $subscribed);
        }

        return $result ? true : false;
    }

    /**
     * Get a single contact from the list
     * @since  1.0.0
     * @param  ZippyCourses_Student
     * @param  Zippy_AWeberEntry List
     * @return object Single Subscriber
     */
    public function getContact(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $zippy = Zippy::instance();

        if ($this->getAccount()) {
            // Get subscriber and their associated URL
            $subscribers = $this->getSubscribers($list->id);
            $single_subscriber = $subscribers->find(array('email' => $student->email));
            $single_subscriber = $single_subscriber->data["entries"][0];
            $url = $single_subscriber['self_link'];

            // Load subscriber object from URL
            try {
                $subscriber = $this->account->loadFromUrl($url);
            } catch (Exception $e) {
                $message = 'Caught Exception: '. $e->getMessage() . '\n';
                $zippy->log->log($message);
            }
         
            return $subscriber;
        }

        return null;
    }

    /**
     * Remove a student from a list
     * @since  1.0.0
     * @param  ZippyCourses_Student
     * @param  Zippy_EmailList
     * @return bool
     */
    public function unsubscribe(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $zippy = Zippy::instance();

        $list = $this->getList($list->id);

        if ($list === null) {
            return;
        }
        
        $subscriber = $this->getContact($student, $list);

        try {
            $subscriber->delete();
        } catch (Exception $e) {
            $message = 'Caught Exception: '. $e->getMessage() . '\n';
            $zippy->log->log($message);
        }
    }

    /**
     * Get settings values for AWeber Integration
     * @since  1.0.0
     * @return array settings
     */
    public function getSettingValues()
    {
        $default_keys = array('auth_code', 'auth_description');

        $settings = (array) get_option($this->getSettingsName());

        foreach ($default_keys as $key) {
            if (!isset($settings[$key])) {
                $settings[$key] = '';
            }
        }

        return $settings;
    }

    /**
     * Get settings name
     * @since  1.0.0
     * @return string Settings Name
     */
    public function getSettingsName()
    {
        return 'zippy_' . $this->service . '_email_integration_settings';
    }
}
