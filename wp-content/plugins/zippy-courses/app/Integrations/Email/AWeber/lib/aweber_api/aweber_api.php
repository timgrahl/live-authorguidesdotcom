<?php

if (class_exists('Zippy_AWeberAPI')) {
    trigger_error("Duplicate: Another Zippy_AWeberAPI client library is already in scope.", E_USER_WARNING);
}
else {
    require_once('aweber.php');
}
