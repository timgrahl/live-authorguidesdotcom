<?php
/**
 * Id: icontact
 * File Type: email-integration
 * Class: ZippyCourses_iContact_EmailListIntegration
 *
 * @since 1.0.0
 */

class ZippyCourses_iContact_EmailListIntegration extends Zippy_EmailListIntegration
{
    public $id = 'icontact';
    public $service = 'icontact';
    public $name = 'iContact';
    public $settings = array();
    
    public $api;

    private $path;

    public function __construct()
    {
        $this->path = plugin_dir_path(__FILE__);
        $this->url  = plugin_dir_url(__FILE__);

        $this->settings = (array) get_option('zippy_icontact_email_integration_settings');
        $this->enabled  = isset($this->settings['enabled']) ? (bool) $this->settings['enabled'] : false;

        add_filter('zippy_classmap', array($this, 'map'));
        add_filter('zippy_fetch_meta_data', array($this, 'metaboxLists'), 10, 2);
        
        parent::__construct();
    }

    public function map($classes)
    {
        $classes['ZippyCourses_iContact_API'] = $this->path . 'lib/api.php';
        $classes['ZippyCourses_iContact_EmailList'] = $this->path . 'lib/EmailList.php';

        return $classes;
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $repository = $zippy->make('email_list_integration_repository');
        $repository->add($this);

        parent::register();
    }

    public function ajax()
    {
        // add_action('wp_ajax_get_icontact_lists', array($this->api, 'getListsJSON'));
    }

    /**
     * Setup the integration with the appropriate settings and hooks,
     * such as landing page integration list and settings
     * @return void
     */
    public function setup()
    {
        $zippy = Zippy::instance();

        $this->api = new ZippyCourses_iContact_API;

        $this->ajax();

        $zippy->bind($this->api->getBinding('list'), 'ZippyCourses_iContact_EmailList');

        add_filter('zippy_metaboxes', array($this, 'metaboxes'));
        add_action('init', array($this, 'settings'));
    }

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    public function settings()
    {
        $zippy = Zippy::instance();

        $setting_text = ZippyCourses_iContact_EmailListIntegration::getSettingText();
        
        $settings_pages = $zippy->make('settings_pages_repository');
        $settings_page = $settings_pages->fetch('zippy_settings_email_lists');

        $section = $settings_page->createSection($this->getSettingsName(), 'iContact');
            $section->createField(
                'enabled',
                __('Enabled?', ZippyCourses::TEXTDOMAIN),
                'select',
                array(
                    __('No', ZippyCourses::TEXTDOMAIN),
                    __('Yes', ZippyCourses::TEXTDOMAIN)
                )
            );
            $section->createField('username', __('Username', ZippyCourses::TEXTDOMAIN));
            $section->createField('app_id', __('Application ID', ZippyCourses::TEXTDOMAIN));
            $section->createField('password', __('Application Password', ZippyCourses::TEXTDOMAIN));

            $field = $section->createField('icontact-instructions', '', 'raw');
            $field->setValue($setting_text);

    }

    /**
     * Renders the "Get Token" button for the settings page
     * @return string HTML for button
     */
    public static function getSettingText()
    {
        $html = null;
        $html .= '<p>To integrate with iContact, you will need to register an app with iContact.';
        $html .= ' <a href="https://www.icontact.com/developerportal/documentation/register-your-app/">Click here</a> to see how to create an app and get your Application ID and Application Password.</p>';
        $html .= '<p class="zippy-settings-warning">';
        $html .= 'Note: There are some limitations with the iContact integration due to the way the iContact service works. If someone leaves a course and is moved to "unsubscribed" status on a list, they will not be able to re-join that list automatically with the same email address. You must also have a minimum of 2 lists in your iContact account for the integration to function normally.';
        $html .= '</p>';
        return $html;
    }

    /**
     * Integrate with the correct metaboxes
     * @return void
     */
    public function metaboxes($metaboxes)
    {
        if (!$this->enabled) {
            return $metaboxes;
        }

        $dir = $this->path . 'assets/views/metaboxes/';
        $files = array_diff(scandir($dir), array('..', '.'));

        foreach ($files as $key => &$file) {
            if (!is_dir($dir . $file)) {
                $file = $dir . $file;
            } else {
                unset($files[$key]);
            }
        }

        return array_merge($metaboxes, $files);
    }

    /**
     * Make sure that tabs are set up where needed
     * @return void
     */
    protected function addTabs()
    {
        if (!$this->enabled) {
            return;
        }

        add_filter('zippy_enable_course_email_list_tab', '__return_true');
        add_filter('zippy_enable_product_email_list_tab', '__return_true');
    }
}
