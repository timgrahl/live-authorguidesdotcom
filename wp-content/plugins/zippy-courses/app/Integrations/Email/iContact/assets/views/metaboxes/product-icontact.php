<?php
/*
Id: zippy-product-icontact
Name: iContact
Data:       email_lists, icontact_lists
Fields:     email_lists:json
Post Types: product
Context: zippy_product_email_lists_main
Priority: default
Type: email_integration
Service: icontact
Version:     1.0.0
*/
?>

<div class="vue" email-integration="icontact"></div>
<input type="hidden" name="email_lists" value="" />
