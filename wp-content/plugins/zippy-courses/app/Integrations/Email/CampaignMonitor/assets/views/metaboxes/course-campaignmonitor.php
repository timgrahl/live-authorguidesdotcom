<?php
/*
Id:         zippy-course-campaignmonitor
Name:       Campaign Monitor
Data:       email_lists, campaignmonitor_lists
Fields:     email_lists:json
Post Types: course
Context:    zippy_course_email_lists_main
Priority:   default
Type:       email_integration
Service:    campaignmonitor
Version:    1.0.0
*/
?>

<div class="vue" email-integration="campaignmonitor"></div>
<input type="hidden" name="email_lists" value="" />
