<?php
/**
 * Id: madmimi
 * File Type: email-integration
 * Class: ZippyCourses_CampaignMonitor_EmailListIntegration
 *
 * @since 1.0.0
 */

class ZippyCourses_CampaignMonitor_EmailListIntegration extends Zippy_EmailListIntegration
{
    public $id = 'campaignmonitor';
    public $service = 'campaignmonitor';
    public $name = 'Campaign Monitor';
    public $settings = array();
    
    public $api;

    private $path;

    public function __construct()
    {
        $this->path = plugin_dir_path(__FILE__);
        $this->url  = plugin_dir_url(__FILE__);

        $this->settings = (array) get_option('zippy_campaignmonitor_email_integration_settings');
        $this->enabled  = isset($this->settings['enabled']) ? (bool) $this->settings['enabled'] : false;

        add_filter('zippy_classmap', array($this, 'map'));
        add_filter('zippy_fetch_meta_data', array($this, 'metaboxLists'), 10, 2);

        parent::__construct();
    }

    public function map($classes)
    {
        $classes['ZippyCourses_CampaignMonitor_API'] = $this->path . 'lib/api.php';
        $classes['ZippyCourses_CampaignMonitor_EmailList'] = $this->path . 'lib/EmailList.php';

        return $classes;
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $repository = $zippy->make('email_list_integration_repository');
        $repository->add($this);

        parent::register();
    }

    public function ajax()
    {
        add_action('wp_ajax_get_campaignmonitor_lists', array($this->api, 'getListsJSON'));
    }

    /**
     * Setup the integration with the appropriate settings and hooks,
     * such as landing page integration list and settings
     * @return void
     */
    public function setup()
    {
        $zippy = Zippy::instance();

        $this->api = new ZippyCourses_CampaignMonitor_API;

        $this->ajax();

        $zippy->bind($this->api->getBinding('list'), 'ZippyCourses_CampaignMonitor_EmailList');

        add_filter('zippy_metaboxes', array($this, 'metaboxes'));
        add_action('init', array($this, 'settings'));
    }

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    public function settings()
    {
        $zippy = Zippy::instance();

        $settings_pages = $zippy->make('settings_pages_repository');
        $settings_page = $settings_pages->fetch('zippy_settings_email_lists');
        $instructions = $this->getInstructions();

        $section = $settings_page->createSection($this->getSettingsName(), 'Campaign Monitor');
        $section->createField(
            'enabled',
            __('Enabled?', ZippyCourses::TEXTDOMAIN),
            'select',
            array(
                    __('No', ZippyCourses::TEXTDOMAIN),
                    __('Yes', ZippyCourses::TEXTDOMAIN)
                )
        );
        $field = $section->createField('auth_description', __('Instructions', ZippyCourses::TEXTDOMAIN), 'raw');
        $field->setValue($instructions);
        $section->createField('access_token', __('Access Token', ZippyCourses::TEXTDOMAIN));
        $section->createField('refresh_token', __('Refresh Token', ZippyCourses::TEXTDOMAIN));


    }

    public function getInstructions()
    {
        $html = null;
        $html .= '<p><strong>' . __('Step 1:', ZippyCourses::TEXTDOMAIN);
        $html .= '</strong> <a href="https://zippycourses.com/auth/campaign-monitor/" target="_blank">';
        $html .= __('Click here', ZippyCourses::TEXTDOMAIN) . '</a>';
        $html .= __(' to authorize Campaign Monitor', ZippyCourses::TEXTDOMAIN) . '</p>';
        $html .='<p><strong>' . __('Step 2:', ZippyCourses::TEXTDOMAIN) . '</strong> ';
        $html .= __('Paste the access and refresh codes into the fields below.', ZippyCourses::TEXTDOMAIN);
        $html .= '</p>';
        return $html;
    }

    /**
     * Integrate with the correct metaboxes
     * @return void
     */
    public function metaboxes($metaboxes)
    {
        if (!$this->enabled) {
            return $metaboxes;
        }
        $dir = $this->path . 'assets/views/metaboxes/';
        $files = array_diff(scandir($dir), array('..', '.'));

        foreach ($files as $key => &$file) {
            if (!is_dir($dir . $file)) {
                $file = $dir . $file;
            } else {
                unset($files[$key]);
            }
        }

        return array_merge($metaboxes, $files);
    }

    /**
     * Make sure that tabs are set up where needed
     * @return void
     */
    protected function addTabs()
    {
        if (!$this->enabled) {
            return;
        }

        add_filter('zippy_enable_course_email_list_tab', '__return_true');
        add_filter('zippy_enable_product_email_list_tab', '__return_true');
    }

    public function getId()
    {
        return $this->id;
    }
}
