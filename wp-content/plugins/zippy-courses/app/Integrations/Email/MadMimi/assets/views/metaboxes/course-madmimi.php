<?php
/*
Id:         zippy-course-madmimi
Name:       MadMimi
Data:       email_lists, madmimi_lists,
Fields:     email_lists:json
Post Types: course
Context:    zippy_course_email_lists_main
Priority:   default
Type:       email_integration
Service:    madmimi
Version:    1.0.0
*/
?>

<div class="vue" email-integration="madmimi"></div>

<input type="hidden" name="email_lists" value="" />
