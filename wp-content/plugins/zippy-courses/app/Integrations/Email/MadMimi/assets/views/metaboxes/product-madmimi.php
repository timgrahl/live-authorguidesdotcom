<?php
/*
Id: zippy-product-madmimi
Name: MadMimi
Data: email_lists, madmimi_lists
Fields:     email_lists:json
Post Types: product
Context: zippy_product_email_lists_main
Priority: default
Type: email_integration
Service: madmimi
Version:     1.0.0
*/
?>

<div class="vue" email-integration="madmimi"></div>

<input type="hidden" name="email_lists" value="" />
