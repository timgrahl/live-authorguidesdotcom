<?php

class ZippyCourses_MadMimi_API extends Zippy_EmailListIntegrationAPI
{
    protected $service = 'madmimi';

    public $username;
    public $api_key;
    public $base_url = 'http://api.madmimi.com/';

    public function __construct()
    {
        $settings = $this->getSettingValues();

        $this->api_key      = $settings['api_key'];
        $this->username     = $settings['username'];
    }

    public function validateSettings()
    {
        if (empty($this->api_key) || empty($this->api_key)) {
            return false;
        }

        return true;
    }

    public function getUrl($endpoint)
    {
        return $this->base_url . $endpoint;
    }

    public function request($url, $method = 'GET', $args = array())
    {
        if (!$this->validateSettings()) {
            return new WP_Error(__('Mad Mimi settings are missing.', ZippyCourses::TEXTDOMAIN));
        }

        $params = array(
            'method' => $method,
            'timeout' => 10,
            'redirection' => 5,
            'httpversion' => '1.1',
            'blocking' => true,
            'headers' => array(
                'Accept: application/json'
            ),
            'body' => $this->getQueryArgs($args)
        );

        $response = wp_remote_post($url, $params);
            
        if (!is_wp_error($response)) {
            return $response['response']['code'] == 200 ? $response : false;
        } else {
            return $response;
        }
    }

    private function getQueryArgs(array $args = array())
    {
        return array_merge($args, array('api_key' => $this->api_key, 'username' => $this->username));
    }

    public function fetchLists()
    {
        $zippy = Zippy::instance();

        $response = $this->request($this->getUrl('audience_lists/lists.xml'));
        
        $lists = $zippy->make('email_list_repository');
        
        if (!is_wp_error($response)) {
            if ($response) {
                $body = simplexml_load_string($response['body']);

                foreach ($body->list as $list) {
                    $id = (int) $list->attributes()->id;
                    $name = (string) $list->attributes()->name;

                    $list = $zippy->make($this->getBinding('list'), array('id' => $id, 'name' => $name));
                    $lists->add($list);
                }
            }
        } else {
            $zippy->log->log($response->get_error_message());
        }

        return $lists;
    }

    public function getList($id)
    {
        
    }
    
    public function getLists()
    {
        $zippy = Zippy::instance();

        if (false === ($lists = get_transient($this->getTransientName('lists')))) {
            // It wasn't there, so regenerate the data and save the transient
            $lists = $this->fetchLists();
            set_transient($this->getTransientName('lists'), $lists->toArray('name'), HOUR_IN_SECONDS);
        } else {
            $repository = $zippy->make('email_list_repository');

            foreach ($lists as $id => $name) {
                $list = $zippy->make($this->getBinding('list'), array('id' => $id, 'name' => $name));
                $repository->add($list);
            }

            $lists = $repository;
        }

        return $lists;
    }

    public function subscribe(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $zippy = Zippy::instance();

        $args = array(
            'email'         => $student->email,
            'first_name'    => $student->first_name,
            'last_name'     => $student->last_name
        );

        $response = $this->request($this->getUrl('audience_lists/' . $list->id . '/add'), 'POST', $args);

        return $response && !is_wp_error($response) ? true : false;
    }

    public function getContact(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $zippy = Zippy::instance();
        
        $response = $this->request($this->getUrl('lists/' . $list->id . '/members/' . md5($student->email)));

        if (!is_wp_error($response)) {
            return $response;
        } else {
            $zippy->log->log($response->get_error_message());
            return $response;
        }
    }

    public function unsubscribe(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $args = array(
            'email'         => $student->email
        );

        $response = $this->request($this->getUrl('audience_lists/' . $list->id . '/remove'), 'POST', $args);

        return $response && !is_wp_error($response) ? true : false;
    }
    
    public function getSettingValues()
    {
        $default_keys = array('api_key', 'username');

        $settings = (array) get_option($this->getSettingsName());

        foreach ($default_keys as $key) {
            if (!isset($settings[$key])) {
                $settings[$key] = '';
            }
        }

        return $settings;
    }

    public function getSettingsName()
    {
        return 'zippy_' . $this->service . '_email_integration_settings';
    }
}
