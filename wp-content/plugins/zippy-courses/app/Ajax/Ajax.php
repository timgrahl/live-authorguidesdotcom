<?php

class ZippyCourses_Ajax
{
    private static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
            $this->loadAdminAjax();
            $this->loadPublicAjax();
        if (defined('DOING_AJAX') && DOING_AJAX) {
        }
    }

    public function loadAdminAjax()
    {
        $zippy = Zippy::instance();

        $files = $zippy->utilities->file->getClassFiles(
            'ajax',
            'Ajax',
            ZippyCourses::$path . 'app/Ajax/admin/'
        );

        foreach ($files as $file) {
            $item = $zippy->makeFromFile($file);
        }
    }

    private function loadPublicAjax()
    {
        $zippy = Zippy::instance();

        $files = $zippy->utilities->file->getClassFiles(
            'ajax',
            'Ajax',
            ZippyCourses::$path . 'app/Ajax/public/'
        );

        foreach ($files as $file) {
            $item = $zippy->makeFromFile($file);
        }
    }
}
