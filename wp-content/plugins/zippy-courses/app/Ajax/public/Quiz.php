<?php
/**
 * Id: quiz
 * File Type: ajax
 * Class: ZippyCourses_PublicAjax_Quiz
 *
 * @since 1.0.0
 */
class ZippyCourses_PublicAjax_Quiz
{
    public function __construct()
    {
        add_action('wp_ajax_get_quiz_data', array($this, 'getQuizData'));
        add_action('wp_ajax_nopriv_get_quiz_data', array($this, 'getQuizData'));

        add_action('wp_ajax_submit_quiz', array($this, 'submitQuiz'));
        add_action('wp_ajax_nopriv_submit_quiz', array($this, 'submitQuiz'));
    }

    public function getQuizData()
    {
        $zippy = Zippy::instance();

        $quiz_id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;

        if ($quiz_id) {
            $quiz = $zippy->make('quiz', array('id' => $quiz_id));
            $quiz->build();

            echo $quiz->toJson();
        }

        die();
    }

    public function submitQuiz()
    {
        global $wpdb;
        
        $zippy  = Zippy::instance();
        $data   = $_REQUEST;
        
        $user_id = $data['user_id'];
        $results = $data['results'];

        $output = array();

        $output['quiz']         = $results['quiz'];
        $output['score']        = $results['score'];
        $output['submitted']    = $results['submitted'];
        $output['questions']    = array();

        if (isset($results['questions']) && is_array($results['questions'])) {
            foreach ($results['questions'] as $question_id => $response) {
                if (!is_array($response)) {
                    continue;
                }

                $question = array();
                $question['id']         = $question_id;
                $question['question']   = get_the_title($question_id);
                $question['answer']     = array();
                $question['correct']    = $response[0] == $response[1];

                $answers  = array_filter((array) $zippy->utilities->getJsonMeta($question_id, 'answers', true));

                foreach ($answers as $answer) {
                    if (!is_object($answer)) {
                        continue;
                    }

                    if (isset($answer->ID)) {
                        if (is_numeric($response[1]) && $answer->ID == $response[1]) {
                            $question['answer']['correct']          = new stdClass;
                            $question['answer']['correct']->ID      = $answer->ID;
                            $question['answer']['correct']->content = $answer->content;
                        }

                        if (is_numeric($response[0]) && $answer->ID == $response[0]) {
                            $question['answer']['given']            = new stdClass;
                            $question['answer']['given']->ID        = $answer->ID;
                            $question['answer']['given']->content   = $answer->content;
                        }
                    }
                }

                $output['questions'][] = $question;
            }
        }

        $wpdb->insert(
            $wpdb->usermeta,
            array(
                'user_id' => $user_id,
                'meta_key' => 'quiz_results',
                'meta_value' => json_encode($output)
            )
        );

        $this->flushStudentMiddlewareRulesCache($user_id);

        die();
    }

    private function flushStudentMiddlewareRulesCache($student_id)
    {
        global $wpdb;

        $wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'zippy_middleware_rules_cache' AND user_id = '$student_id'");
        $wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'zippy_middleware_rules_cache_timestamp' AND user_id = '$student_id'");
    }
}
