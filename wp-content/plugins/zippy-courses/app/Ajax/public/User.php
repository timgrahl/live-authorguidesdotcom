<?php
/**
 * Id: user
 * File Type: ajax
 * Class: ZippyCourses_PublicAjax_User
 *
 * @since 1.0.0
 */
class ZippyCourses_PublicAjax_User
{
    public function __construct()
    {
        add_action('wp_ajax_save_student_course_start_date', array($this, 'saveCourseStartDate'));
        add_action('wp_ajax_restore_student_tier_access', array($this, 'restoreStudentTierAccess'));
        add_action('wp_ajax_remove_student_tier_access', array($this, 'removeStudentTierAccess'));
        add_action('wp_ajax_unique_email', array($this, 'validate_unique_email'));
        add_action('wp_ajax_nopriv_unique_email', array($this, 'validate_unique_email'));
        add_action('wp_ajax_unique_username', array($this, 'validate_unique_username'));
        add_action('wp_ajax_nopriv_unique_username', array($this, 'validate_unique_username'));
    }

    public function validate_unique_email()
    {
        $zippy = Zippy::instance();

        $validator = $zippy->make('validator');

        if ($validator->uniqueEmail($_POST['email'])) {
            echo json_encode(true);
        } else {
            echo json_encode(__('That email is already in use on this site.', ZippyCourses::TEXTDOMAIN));
        }
        die();
    }

    public function validate_unique_username()
    {
        $zippy = Zippy::instance();

        $validator = $zippy->make('validator');

        if ($validator->uniqueUsername($_POST['username'])) {
            echo json_encode(true);
        } else {
            echo json_encode(__('That username is already in use on this site.', ZippyCourses::TEXTDOMAIN));
        }
        die();
    }

    public function saveCourseStartDate()
    {
        $zippy = Zippy::instance();

        $data = json_decode(file_get_contents('php://input'));

        $user_id        = $data->user_id;
        $course_id      = $data->course_id;
        $start_date     = $zippy->utilities->datetime->getDate($data->start_date);
        $start_dates    = array();
        
        $dates = array_filter(
            (array) $zippy->utilities->maybeJsonDecode(
                get_user_meta(
                    $user_id,
                    'zippy_courses_start_dates',
                    true
                )
            )
        );

        // Make sure all array keys are numeric integers instead of strings
        foreach ($dates as $key => $value) {
            $start_dates[(int) $key] = $value;
        }

        $start_dates[$course_id] = $start_date->format('m/d/Y');

        $update = (bool) update_user_meta($user_id, 'zippy_courses_start_dates', json_encode($start_dates));

        $this->flushStudentMiddlewareRulesCache($user_id);

        echo (int) $update ? '1' : '-1';
        


        die();
    }

    public function restoreStudentTierAccess()
    {
        $zippy = Zippy::instance();

        $data = json_decode(file_get_contents('php://input'));

        $user_id        = $data->user_id;
        $course_id      = $data->course_id;
        $tier_id        = $data->tier_id;
            
        $student = $zippy->make('student', array($user_id));
        echo (int) $student->restoreTierAccess($course_id, $tier_id) ? '1' : '-1';
        
        die();
    }

    public function removeStudentTierAccess()
    {
        $zippy = Zippy::instance();

        $data = json_decode(file_get_contents('php://input'));

        $user_id        = $data->user_id;
        $course_id      = $data->course_id;
        $tier_id        = $data->tier_id;
            
        $student = $zippy->make('student', array($user_id));
        echo (int) $student->removeTierAccess($course_id, $tier_id) ? '1' : '-1';
        
        die();
    }

    private function flushStudentMiddlewareRulesCache($student_id)
    {
        global $wpdb;

        $wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'zippy_middleware_rules_cache' AND user_id = '$student_id'");
        $wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'zippy_middleware_rules_cache_timestamp' AND user_id = '$student_id'");
    }
}
