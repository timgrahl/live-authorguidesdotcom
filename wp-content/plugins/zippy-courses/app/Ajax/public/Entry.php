<?php
/**
 * Id: entry
 * File Type: ajax
 * Class: ZippyCourses_PublicAjax_Entry
 *
 * @since 1.0.0
 */
class ZippyCourses_PublicAjax_Entry
{
    public function __construct()
    {
        add_action('wp_ajax_nopriv_complete_entry', array($this, 'completeEntry'));
        add_action('wp_ajax_complete_entry', array($this, 'completeEntry'));

        add_action('wp_ajax_nopriv_uncomplete_entry', array($this, 'uncompleteEntry'));
        add_action('wp_ajax_uncomplete_entry', array($this, 'uncompleteEntry'));
    }

    public function _toggleEntryComplete()
    {
        $zippy = Zippy::instance();
        
        $data = json_decode(file_get_contents('php://input'));

        if (isset($data->user_id) && isset($data->id)) {
            $student = $zippy->make('student', array($data->user_id));
            $student->toggleEntryComplete($data->id);
        }

        $this->flushStudentMiddlewareRulesCache($data->user_id);

        die();
    }

    public function completeEntry()
    {
        $this->_toggleEntryComplete();
    }

    public function uncompleteEntry()
    {
        $this->_toggleEntryComplete();
    }
    
    private function flushStudentMiddlewareRulesCache($student_id)
    {
        global $wpdb;

        $wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'zippy_middleware_rules_cache' AND user_id = '$student_id'");
        $wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'zippy_middleware_rules_cache_timestamp' AND user_id = '$student_id'");
    }
}
