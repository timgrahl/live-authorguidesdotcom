<?php
/**
 * Id: ux
 * File Type: ajax
 * Class: ZippyCourses_AdminAjax_UX
 *
 * @since 1.0.0
 */
class ZippyCourses_AdminAjax_UX
{
    public function __construct()
    {
        add_action('wp_ajax_zippy_dismiss_news_item', array($this, 'dismissNewsItem'));
    }

    public function dismissNewsItem()
    {
        $zippy = Zippy::instance();
        
        $data = json_decode(file_get_contents('php://input'));

        $url = $data->url;

        $dismissed = get_option('zippy_rss_dismissed', array());

        if (!in_array($url, $dismissed)) {
            $dismissed[] = $url;
        }

        update_option('zippy_rss_dismissed', $dismissed);

        die();
    }
}
