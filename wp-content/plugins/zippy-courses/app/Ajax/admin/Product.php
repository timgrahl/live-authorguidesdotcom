<?php
/**
 * Id: product
 * File Type: ajax
 * Class: ZippyCourses_AdminAjax_Product
 *
 * @since 1.0.0
 */
class ZippyCourses_AdminAjax_Product
{
    public function __construct()
    {
        add_action('wp_ajax_getCoursesAndTiers', array($this, 'getCoursesAndTiers'));
    }

    public function getCoursesAndTiers()
    {
        global$wpdb;

        $zippy = Zippy::instance();

        $courses = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE post_type = 'course' AND post_status != 'auto-draft' AND post_status != 'trash' && post_status != 'inherit'");

        foreach ($courses as &$course) {
            $course->tiers = $zippy->utilities->getJsonMeta($course->ID, 'tiers', true);
        }

        echo json_encode($courses);

        die();
    }
}
