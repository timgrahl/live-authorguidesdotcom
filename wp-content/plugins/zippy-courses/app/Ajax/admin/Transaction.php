<?php
/**
 * Id: transaction
 * File Type: ajax
 * Class: ZippyCourses_AdminAjax_Transaction
 *
 * @since 1.2.1
 */
class ZippyCourses_AdminAjax_Transaction
{
    public function __construct()
    {
        add_action('wp_ajax_update_transaction_amount', array($this, 'updateTransactionAmount'));
    }

    public function updateTransactionAmount()
    {
        $zippy = Zippy::instance();
        
        $data = json_decode(file_get_contents('php://input'));
        $transaction_id = $data->transaction_id;
        $transaction_amount = $data->transaction_amount;

        $transaction = $zippy->make('transaction');
        $transaction->build($transaction_id);
        $transaction->setTotal($transaction_amount);
        $transaction->save();

        die();
    }
}