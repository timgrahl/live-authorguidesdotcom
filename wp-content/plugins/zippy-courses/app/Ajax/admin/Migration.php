<?php
/**
 * Id: migration
 * File Type: ajax
 * Class: ZippyCourses_AdminAjax_Migration
 *
 * @since 1.0.0
 */
class ZippyCourses_AdminAjax_Migration
{
    public function __construct()
    {
        add_action('wp_ajax_zippy_check_upgrade_progress', array($this, 'checkUpgradeProgress'));
        add_action('wp_ajax_zippy_complete_upgrade', array($this, 'completeUpgrade'));
    }

    public function checkUpgradeProgress()
    {
        define('ZIPPY_DOING_UPGRADE', true);
        
        $zippy = Zippy::instance();

        $migration = $zippy->make('migration_v1_0_0');
        $migration->migrate();

        $status = $migration->checkStatus();
        
        set_transient('zippy_upgrade_status', $status, MINUTE_IN_SECONDS * 5);

        echo json_encode($status);

        die();
    }

    public function completeUpgrade()
    {
        $zippy = Zippy::instance();

        $zippy->sessions->flashAdminMessage(__('Your Zippy Courses Data has been successfully upgraded.', ZippyCourses::TEXTDOMAIN));

        $migration = $zippy->make('migration_v1_0_0');
        $migration->cleanup();
        
        delete_transient('zippy_upgrade_status');
        delete_transient('zippy_doing_upgrade');

        update_option('zippy_db_version', '1.0.0');

        die(1);
    }
}
