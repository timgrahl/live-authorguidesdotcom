<?php
/**
 * Id: course
 * File Type: ajax
 * Class: ZippyCourses_AdminAjax_Course
 *
 * @since 1.0.0
 */
class ZippyCourses_AdminAjax_Course
{
    public function __construct()
    {
        add_action('wp_ajax_get_email_lists', array($this, 'ajax_get_email_lists'));
        add_action('wp_ajax_get_posts_list', array($this, 'ajax_get_course_posts_list'));
        add_action('wp_ajax_get_course_tiers', array($this, 'ajax_get_course_tiers'));
        add_action('wp_ajax_get_course_entries', array($this, 'getEntries'));
        add_action('wp_ajax_get_course_products', array($this, 'getProducts'));
        add_action('wp_ajax_get_lesson', array($this, 'ajax_get_lesson'));
        add_action('wp_ajax_save_lesson', array($this, 'ajax_save_lesson'));
        add_action('wp_ajax_get_items', array($this, 'getItems'));
        add_action('wp_ajax_import_items', array($this, 'importItems'));
        add_action('wp_ajax_delete_course', array($this, 'delete'));
    }

    public function ajax_get_email_lists()
    {
        global $wpdb;
        
        $post_id = $_REQUEST['id'];

        $lists = (array) get_post_meta($post_id, 'email_lists', true);

        echo json_encode($lists);

        die(0);
    }

    public function ajax_get_course_posts_list()
    {
        global $wpdb;
        
        $results = $wpdb->get_results("SELECT ID, post_title AS title FROM $wpdb->posts WHERE post_type = 'post' AND post_status != 'auto-draft' AND post_status != 'trash' && post_status != 'inherit'");

        echo json_encode($results);

        die(0);
    }

    public function ajax_get_course_tiers()
    {
        global $wpdb;
        
        $post_id = $_REQUEST['id'];

        $tiers = (array) get_post_meta($post_id, 'tiers', true);

        echo json_encode($tiers);

        die(0);
    }

    public function getProducts()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $post_id    = $_REQUEST['id'];

        $sql        = $wpdb->prepare("SELECT pm.post_id, pm.meta_value FROM $wpdb->postmeta AS pm LEFT JOIN $wpdb->posts AS p ON (p.ID = pm.post_id) WHERE p.post_type = 'product' AND meta_key = %s", 'access');
        $products   = $wpdb->get_results($sql);

        $output = array();
        foreach ($products as $product) {
            $access    = $zippy->utilities->maybeJsonDecode($product->meta_value);

            if ($access->mode == 'product') {
                if ($access->course == $post_id) {
                    $obj = new stdClass;
                    $obj->ID = $product->post_id;
                    $obj->title = get_the_title($product->post_id);
                    $obj->list_order = $this->getProductListOrder($post_id, $product->post_id);
                    $output[] = $obj;
                }
            } else {
                foreach ($access->bundle as $course) {
                    if ($course->id == $post_id && !empty($course->tiers)) {
                        $obj = new stdClass;
                        $obj->ID = $product->post_id;
                        $obj->title = get_the_title($product->post_id);
                        $obj->list_order = $this->getProductListOrder($post_id, $product->post_id);
                        $output[] = $obj;
                    }
                }
            }
        }

        echo json_encode($output);

        die(0);
    }

    /**
     * Returns the Current Product's order in the Products list
     * @param  int $post_id    ID of the Course
     * @param  int $product_id ID of the Product
     * @return int the Product's list order, or 9999 if not included
     */
    protected function getProductListOrder($post_id, $product_id)
    {
        $zippy = Zippy::instance();
        $product_order = get_post_meta($post_id, 'products', true);
        $product_order = $zippy->utilities->maybeJsonDecode($product_order);
        $key = array_search($product_id, $product_order);
        if ($key !== false) {
            return $key;
        }
        return 9999;

    }

    public function getEntries()
    {
        global $wpdb;
        
        $post_id = $_REQUEST['id'];

        $entries = (array) get_post_meta($post_id, 'entries', true);
        
        foreach ($entries as $key => &$entry) {
            $entry->post = get_post($entry->ID);

            if ($entry->post == null) {
                unset($entries[$key]);
            }
        }
        
        echo json_encode($entries);

        die(0);
    }

    public function ajax_get_lesson()
    {
        echo json_encode(get_post($_REQUEST['id']));
        die();
    }

    public function ajax_save_lesson()
    {
        $data = json_decode(file_get_contents('php://input'));
        $lesson = $data->lesson;

        $update = $this->updatePost($lesson);

        echo !is_wp_error($update) ? '1' : '0';

        die();
    }

    private function updatePost($data)
    {
        global $wpdb;

        $id     = $data->ID;
        $post   = get_post($id);

        $title = sanitize_title($data->title);

        if ($post->post_name == $id || strpos($title, $post->post_name) !== false) {
            $slug = wp_unique_post_slug($title, $id, get_post_status($id), get_post_type($id), wp_get_post_parent_id($id));
            $post->post_name = $slug;

        }
        
        return wp_update_post($post, true);
    }

    public function getItems()
    {
        $items = (array) $_REQUEST['items'];
        $entries = array();

        foreach ($items as $item_id) {
            $post   = get_post($item_id);

            if ($post) {
                $entry              = new stdClass;
                $entry->ID          = $post->ID;
                $entry->title       = $post->post_title;
                $entry->post_type   = $post->post_type;
                $entry->post        = $post;

                $entries[] = $entry;
            }
        }

        echo json_encode($entries);

        die(0);
    }

    public function importItems()
    {
        $zippy = Zippy::instance();

        $items = (array) $_REQUEST['items'];
        
        $entries = array();

        foreach ($items as $item_id) {
            $post   = get_post($item_id);

            if ($post) {
                $entry              = new stdClass;
                $entry->ID          = $post->ID;
                $entry->title       = $post->post_title;
                $entry->post_type   = $post->post_type;
                $entry->post        = $post;

                $new_post_id = $zippy->utilities->post->duplicate($post->ID);

                if ($new_post_id !== false) {
                    $entry->ID = $new_post_id;
                    $entry->post = get_post($new_post_id);
                    
                    $entries[] = $entry;
                }
            }
        }

        echo json_encode($entries);

        die(0);
    }

    public function delete() {
        $zippy = Zippy::instance();

        $data = json_decode(file_get_contents('php://input'));

        $id = $data->id;

        if ($id) {
            wp_delete_post($id, true);
        }

        die();
    }
}
