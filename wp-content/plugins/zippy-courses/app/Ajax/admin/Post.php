<?php
/**
 * Id: post
 * File Type: ajax
 * Class: ZippyCourses_AdminAjax_Post
 *
 * @since 1.0.0
 */
class ZippyCourses_AdminAjax_Post
{
    public function __construct()
    {
        add_action('wp_ajax_new_entry', array($this, 'newEntry'));
        add_action('wp_ajax_new_unit', array($this, 'newUnit'));
        add_action('wp_ajax_save_post', array($this, 'savePost'));
        add_action('wp_ajax_get_post_types', array($this, 'getPostTypes'));
        add_action('wp_ajax_get_post_type_entries', array($this, 'getPostTypeEntries'));
        add_action('wp_ajax_save_post_meta', array($this, 'savePostMeta'));
    }

    /**
     * Get the valid Post Type entries
     *
     * @since 1.0.0
     *
     * @todo Move filtering to a separate action / filter
     *
     * @return json
     */
    public function getPostTypeEntries()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $post_types = $_REQUEST['post_types'];
        foreach ($post_types as &$post_type) {
            $post_type = "'$post_type'";
        }

        $post_type_list = implode(', ', $post_types);

        $invalid_post_statuses = array('"auto-draft"','"trash"', '"inherit"');
        $invalid_post_status_list = implode(', ', $invalid_post_statuses);

        $posts = $wpdb->get_results("SELECT ID, post_title AS title, post_type FROM $wpdb->posts WHERE post_type IN ($post_type_list) AND post_status NOT IN ($invalid_post_status_list)");

        $already_used = $zippy->utilities->entry->getAllEntryIds();

        foreach ($posts as $key => &$post) {
            $post->in_use   = in_array($post->ID, $already_used);

            if ($post->post_type == 'page' && $zippy->core_pages->is($post->ID)) {
                unset($posts[$key]);
            }
        }

        echo json_encode(array_values($posts));

        die();
    }

    /**
     * Get a list of valid post types from the system
     *
     * @since 1.0.0
     *
     * @return json
     */
    public function getPostTypes()
    {
        $args = array(
            'public'   => true
        );

        $exclude = array('course', 'product', 'payment', 'zippy_order', 'attachment', 'transaction', 'quiz', 'quiz_question');
        $post_types = get_post_types($args, 'objects');

        $output = array();

        foreach ($post_types as $key => $post_type) {
            if (!in_array($key, $exclude)) {
                $item = new stdClass;
                    $item->post_type = $key;
                    $item->label = $post_type->label;

                $output[] = $item;
            }
        }

        echo json_encode($output);

        die();
    }

    public function newEntry()
    {
        $post_id    = $_REQUEST['id'];
        $post_type  = $_REQUEST['post_type'];
        global $current_user;
        wp_get_current_user();

        if (!$post_id) {
            // Create post object
            $data = array(
              'post_title'    => '',
              'post_content'  => ' ',
              'post_status'   => 'publish',
              'post_author'   => $current_user->ID,
              'post_type'     => $post_type
            );

            // Insert the post into the database
            $post_id = wp_insert_post($data);
        }

        $post = get_post($post_id);

        $entry = new stdClass;
            $entry->ID          = $post->ID;
            $entry->post        = $post;
            $entry->post_type   = $post->post_type;
            $entry->title       = $post->post_title;

        echo json_encode($entry);

        die();
    }

    public function newUnit()
    {
        $post_id    = $_REQUEST['id'];
        $post_type  = $_REQUEST['post_type'];
        $count      = $_REQUEST['num_children'];

        $post = $this->_createPost($post_type);

        $unit = new stdClass;
            $unit->ID          = $post->ID;
            $unit->post        = $post;
            $unit->post_type   = $post->post_type;
            $unit->title       = $post->post_title;
            $unit->entries     = array();

        for ($i=0; $i < $count; $i++) {
            $item = $this->_createPost('lesson');

            $entry = new stdClass;
                $entry->ID          = $item->ID;
                $entry->post        = $item;
                $entry->post_type   = $item->post_type;
                $entry->title       = $item->post_title;

            $unit->entries[] = $entry;
        }

        echo json_encode($unit);

        die();
    }

    private function _createPost($post_type)
    {
        global $current_user;
        wp_get_current_user();

        $data = array(
          'post_title'    => '',
          'post_content'  => ' ',
          'post_status'   => 'publish',
          'post_author'   => $current_user->ID,
          'post_type'     => $post_type
        );

        // Insert the post into the database
        $post_id = wp_insert_post($data, true);

        return !is_wp_error($post_id) ? get_post($post_id) : null;
    }

    public function savePost()
    {
        $data       = json_decode(file_get_contents('php://input'));
        $postdata   = $data->post;
        $meta       = isset($data->meta) ? $data->meta : null;

        if (isset($postdata->ID) && $postdata->ID) {
            $update = $this->updatePost($postdata, true);
            echo !is_wp_error($update) ? '1' : '0';

            if ($meta !== null) {
                foreach ($meta as $key => $value) {
                    $value = json_encode($value);
                    update_post_meta($postdata->ID, $key, wp_slash($value));
                }
            }
        }

        die(0);
    }

    private function updatePost($data)
    {
        global $wpdb;

        $id     = $data->ID;
        $post   = get_post($id);
        $slug  = sanitize_title($data->post_title);

        if ($post->post_name == $id || strpos($slug, $post->post_name) !== false) {
            $slug = wp_unique_post_slug($slug, $id, get_post_status($id), get_post_type($id), wp_get_post_parent_id($id));
            $post->post_name = $slug;
        }
        
        $post->post_title = $data->post_title;

        return wp_update_post($post, true);
    }

    public function savePostMeta()
    {
        $data = json_decode(file_get_contents('php://input'));

        $post_id = $data->id;
        $meta_data = (array) $data->meta;

        if ($post_id && is_array($meta_data)) {
            foreach ($meta_data as $key => $value) {
                if (is_array($value) || is_object($value)) {
                    $value = json_encode($value);
                }
                
                update_post_meta($post_id, $key, $value);
            }

            die(1);
        }

        die(0);
    }
}
