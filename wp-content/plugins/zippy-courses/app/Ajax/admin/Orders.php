<?php
/**
 * Id: orders
 * File Type: ajax
 * Class: ZippyCourses_AdminAjax_Orders
 *
 * @since 1.0.0
 */
class ZippyCourses_AdminAjax_Orders
{
    public function __construct()
    {
        add_action('wp_ajax_resend_registration_email', array($this, 'resendRegistrationEmail'));
        add_action('wp_ajax_clear_ontraport_status', array($this, 'clearOntraportStatus'));
    }

    public function resendRegistrationEmail()
    {
        $zippy = Zippy::instance();

        $data = json_decode(file_get_contents('php://input'));

        if (isset($data->order_id)) {
            $order = $zippy->make('order');
            $order->build($data->order_id);

            $zippy->utilities->orders->resendRegistrationReminder($order);
        }

        die();
    }

    public function clearOntraportStatus()
    {
        $zippy = Zippy::instance();

        $data = json_decode(file_get_contents('php://input'));

        if (isset($data->order_id)) {
            delete_post_meta($data->order_id, 'ontraport_order_status');
        }

        die();
    }
}
