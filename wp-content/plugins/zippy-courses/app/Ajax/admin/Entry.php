<?php
/**
 * Id: entry
 * File Type: ajax
 * Class: ZippyCourses_AdminAjax_Entry
 *
 * @since 1.0.0
 */
class ZippyCourses_AdminAjax_Entry
{
    public function __construct()
    {
        add_action('wp_ajax_generate_file_uid', array($this, 'generateFileUid'));
    }

    public function generateFileUid()
    {
        $zippy = Zippy::instance();
        
        echo $zippy->utilities->entry->generateFileUid();

        die();
    }
}
