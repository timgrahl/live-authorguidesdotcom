<?php

class ZippyCourses_Entry_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        add_filter('pre_get_posts', array($this, 'filter'), 99);

        add_filter('wp_get_nav_menu_items', array($this, 'excludeMenuItems'), 10, 3);

        if ($student !== null) {
            add_filter('previous_post_link', array($this, 'previousPostLink'), 10, 4);
            add_filter('next_post_link', array($this, 'nextPostLink'), 10, 4);

            add_filter('get_next_post_join', array($this, 'adjacentPostJoin'), 10, 3);
            add_filter('get_next_post_where', array($this, 'nextPostWhere'), 10, 3);
            add_filter('get_next_post_sort', array($this, 'adjacentPostSort'), 10, 3);

            add_filter('get_previous_post_join', array($this, 'adjacentPostJoin'), 10, 3);
            add_filter('get_previous_post_where', array($this, 'previousPostWhere'), 10, 3);
            add_filter('get_previous_post_sort', array($this, 'adjacentPostSort'), 10, 3);

            add_filter('the_content', array($this, 'lessonCompletionButton'), 9, 1);
            add_filter('the_content', array($this, 'entryDownloads'));
            add_filter('the_content', array($this, 'downloadCertificate'));
        }

        add_filter('wp_footer', array($this, 'templates'));
        add_filter('post_thumbnail_html', array($this, 'featuredMedia'), 9, 5);
        add_filter('body_class', array($this, 'bodyClass'));
    }

    public function filter($query)
    {
        if (!$query->is_main_query()) {
            return $query;
        }

        if (!is_home() && !is_search()) {
            return $query;
        }

        global $wpdb;

        $zippy = Zippy::instance();

        $middleware = $zippy->middleware->get('student-access');

        $course_ids             = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE post_type = 'course'");
        $protected_content_ids  = array_merge($zippy->utilities->entry->getAllProtectedEntryIds(), $course_ids);

        foreach ($protected_content_ids as $key => $post_id) {
            $p = new stdClass;
            $p->ID = $post_id;

            if ($middleware->run($p)) {
                unset($protected_content_ids[$key]);
            } else {
            }
        }

        $post__not_in = $query->get('post__not_in', array());
        $post__not_in = array_merge($post__not_in, $protected_content_ids);

        $query->set('post__not_in', array_merge($post__not_in, $query->get('post__not_in')));

        return $query;
    }

    public function adjacentPostLink($output, $previous = true)
    {
        global $post;

        if ($post->post_type == 'course') {
            return;
        }

        $zippy = Zippy::instance();
        
        $post_id = $zippy->utilities->post->getPostId();

        $zippy = Zippy::instance();

        $all_entry_ids  = $zippy->utilities->entry->getAllEntryIds();

        if (in_array($post_id, $all_entry_ids)) {
            $adjacent_id = $previous
                ? $zippy->utilities->entry->getPreviousEntryId($post->ID)
                : $zippy->utilities->entry->getNextEntryId($post->ID);

            if (!$adjacent_id) {
                return '';
            }
        }

        return $output;
    }

    public function previousPostLink($output, $format = '', $link = '', $adjacent = '')
    {
        return $this->adjacentPostLink($output);
    }

    public function nextPostLink($output, $format = '', $link = '', $adjacent = '')
    {
        global $post;
        $zippy = Zippy::instance();
        
        $next_link = $this->adjacentPostLink($output, false);
        $next_uncompleted_link = $this->nextUncompletedPostLink($output);
        if ($next_link == $next_uncompleted_link) {
            return $next_link;
        } else {
            $lesson_id = $zippy->utilities->entry->getNextUncompletedEntryId($post->ID);
            $permalink = get_permalink($lesson_id);
            $class = 'class="zippy-hide-next-post"';
            $output = '<a href="' . $permalink . '"' . $class . '>' . $link . '</a>';
            return apply_filters('zippy_courses_next_post_hidden', $output, $lesson_id);
        }
    }

    public function nextUncompletedPostLink($output)
    {
        global $post;

        if ($post->post_type == 'course') {
            return;
        }

        $zippy = Zippy::instance();
        
        $post_id = $zippy->utilities->post->getPostId();

        $all_entry_ids  = $zippy->utilities->entry->getAllEntryIds();

        if (in_array($post_id, $all_entry_ids)) {
            $adjacent_id = $zippy->utilities->entry->getNextUncompletedEntryId($post->ID);
            if (!$adjacent_id) {
                return '';
            }
        }

        return $output;

    }

    public function adjacentPostJoin($join)
    {
        global $post;

        $zippy = Zippy::instance();

        $all_entry_ids = $zippy->utilities->entry->getAllEntryIds();

        if (in_array($post->ID, $all_entry_ids)) {
            $join = '';
        }
        
        return $join;
    }

    public function adjacentPostSort($sort)
    {
        global $post;

        $zippy = Zippy::instance();

        $all_entry_ids = $zippy->utilities->entry->getAllEntryIds();

        if (in_array($post->ID, $all_entry_ids)) {
            $sort = "LIMIT 1";
        }

        return $sort;
    }

    public function previousPostWhere($where)
    {
        return $this->adjacentPostWhere($where);
    }

    public function nextPostWhere($where)
    {
        return $this->adjacentPostWhere($where, false);
    }

    public function adjacentPostWhere($where, $previous = true)
    {
        global $post, $wpdb;

        $zippy = Zippy::instance();

        $all_entry_ids = $zippy->utilities->entry->getAllEntryIds();

        if (in_array($post->ID, $all_entry_ids)) {
            $adjacent_id = $previous
                ? $zippy->utilities->entry->getPreviousEntryId($post->ID)
                : $zippy->utilities->entry->getNextEntryId($post->ID);

            if ($adjacent_id) {
                $where = $wpdb->prepare("WHERE p.ID = %d", $adjacent_id);
            }
        }

        return $where;
    }

    public function entryDownloads($content)
    {
        global $post, $wp_query;

        if (isset($wp_query->query_vars['quiz']) || isset($_GET['results'])) {
            return $content;
        }

        $zippy = Zippy::instance();
        $downloads = $zippy->utilities->getJsonMeta($post->ID, 'downloads', true);
        $output = '';

        if ($downloads && is_array($downloads) && count($downloads)) {
            $output .= '<div class="zippy-downloads">';
            $output .= '<h4>' . __('Downloads', ZippyCourses::TEXTDOMAIN) . '</h4>';
            
            $output .= '<ul>';

            foreach ($downloads as $download) {
                $output .= '<li class="zippy-download-' . $download->type . '"><a href="' . $zippy->utilities->entry->getSecureDownloadUrl($download->uid) . '">' .
                    __('Download', ZippyCourses::TEXTDOMAIN) . ' ' . $download->title .
                '</a></li>';
            }

            $output .= '</ul>';
            $output .= '</div>';
        }
        $output = apply_filters('zippy_courses_entry_downloads', $output);

        $content .= $output;
       
        return $content;
    }
    public function lessonCompletionButton($content)
    {
        global $post;

        $settings       = get_option('zippy_customizer_course_options', array());
        
        // Get the Lesson Completion Button location. 0 == Not Displayed, 1 == Top, 2 == Bottom
        $lesson_completion_button_location  = isset($settings['show_lesson_completion_button']) ? $settings['show_lesson_completion_button']: 0;

        $lesson_completion_button_location = apply_filters('zippy_lesson_completion_button_location', $lesson_completion_button_location);
        
        $post_type = get_post_type($post);
        if ($post_type == 'unit' || $post_type == 'lesson') {
            $button = '<div class="zippy-entry-complete">';
            $button .= do_shortcode('[zippy_complete_lesson]');
            $button .= '</div>';
            switch ($lesson_completion_button_location) {
                case '1':
                    $content = $button . $content;
                    break;
                case '2':
                    $content = $content . $button;
                    break;
                case '3':
                    $content = $button . $content . $button;
                default:
                    break;
            }
        } 
        return $content;
    }

    public function excludeMenuItems($items, $menu, $args)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $item_ids = array();

        foreach ($items as $item) {
            $item_ids[] = $item->object_id;
        }

        $student                = $zippy->cache->get('student');
        $course_ids             = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE post_type = 'course'");
        $protected_content_ids  = array_merge($zippy->utilities->entry->getAllProtectedEntryIds(), $course_ids);
    
        $middleware = $zippy->middleware->get('student-access');
                
        foreach ($protected_content_ids as $key => $post_id) {
            if (!in_array($post_id, $item_ids)) {
                continue;
            }

            $p = new stdClass;
            $p->ID = $post_id;

            $has_access = $middleware->run($p);
            if ($has_access) {
                unset($protected_content_ids[$key]);
            } else {
            }
        }

        $excluded_core_pages = $student === null
            ? array_values($zippy->core_pages->getPrivateOnlyCorePageIds())
            : array_values($zippy->core_pages->getPublicOnlyCorePageIds());

        $protected_content_ids = array_merge($protected_content_ids, $excluded_core_pages);

        foreach ($items as $key => $item) {
            if (in_array($item->object_id, $protected_content_ids)) {
                unset($items[$key]);
            }
        }

        return $items;
    }

    public function templates()
    {
        global $post;

        $zippy = Zippy::instance();
        $entry_ids  = $zippy->utilities->entry->getAllEntryIds();

        if (is_object($post) && in_array($post->ID, $entry_ids)) {
            require_once(ZippyCourses::$path . 'assets/views/templates/public/lesson/lesson-complete.php');
        }
    }

    public function featuredMedia($html, $post_id, $post_thumbnail_id, $size, $attr)
    {
        global $post;

        $zippy = Zippy::instance();
        
        $middleware = $zippy->middleware->get('student-access');
        $post       = get_post($post_id);
        $student    = $zippy->cache->get('student');

        if (get_post_type($post_id) == 'course') {
            if ($student === null || ($middleware !== null && method_exists($middleware, 'run') && !$middleware->run($post))) {
                $featured_media = $zippy->utilities->getJsonMeta($post_id, 'public_featured_media', true);

                $image = isset($featured_media->image) ? $featured_media->image : null;
                $video = isset($featured_media->video) ? $featured_media->video : null;
            } else {
                if ($middleware !== null && $middleware->run($post)) {
                    $featured_media = $zippy->utilities->getJsonMeta($post_id, 'featured_media', true);

                    $image = isset($featured_media->image) ? $featured_media->image : null;
                    $video = isset($featured_media->video) ? $featured_media->video : null;
                } else {
                    $html = '';
                }
            }
        } else {
            if ($middleware !== null && $middleware->run($post)) {
                $featured_media = $zippy->utilities->getJsonMeta($post_id, 'featured_media', true);

                $image = isset($featured_media->image) ? $featured_media->image : null;
                $video = isset($featured_media->video) ? $featured_media->video : null;
            } else {
                $html = '';
            }
        }

        if (isset($image) && is_object($image)) {
            $html = wp_get_attachment_image($image->ID, $size, false, $attr);
        }

        if (isset($video) && !empty($video)) {
            $html = do_shortcode(trim(stripslashes($video)));
        }

        return $html;
    }

    public function bodyClass($classes)
    {
        global $post;
        $zippy = Zippy::instance();

        $classes[] = 'zippy';
        $post_type = get_post_type($post);
        if ($post_type == 'course') {
            $classes[] = 'zippy-course-' . $post->ID;
        } elseif ($post_type == 'unit' || $post_type == 'lesson') {
            $course_id = $zippy->utilities->entry->getCourseId($post->ID);
            $classes[] = 'zippy-course-' . $course_id;
        }

        return $classes;
    }

    public function downloadCertificate($output)
    {
        global $post;
        $zippy = Zippy::instance();

        if ($post->post_type != 'unit' && $post->post_type != 'lesson') {
            return $output;
        }

        $student = $zippy->cache->get('student');
        $course = $zippy->utilities->entry->getCourse($post->ID);

        $last_entry = $course->entries->getLastEntry();
        if ($last_entry->getId() == $post->ID) {
            if (!$student->hasCompletedCourse($course) || !$course->hasCertificate()) {
                return $output;
            }

            $output .= '<a href="' . $zippy->utilities->course->getCertificateUrl($course->getId()) . '" class="zippy-button zippy-button-primary zippy-claim-certificate">';
            $output .= __('Download Certificate of Completion', ZippyCourses::TEXTDOMAIN);
            $output .= '</a>';
        }
        return $output;
    }
}
