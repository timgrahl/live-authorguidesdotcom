<?php

class ZippyCourses_Course_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('the_content', array($this, 'progress'), 10);
        add_filter('the_content', array($this, 'entries'), 11);
        add_filter('the_content', array($this, 'publicDetails'), 99);
        add_filter('the_content', array($this, 'expired'), 100);
        add_filter('the_content', array($this, 'expiresSoon'), 100);

        add_filter('zippy_fetch_meta_data', array($this, 'entryTitles'), 10, 2);
        add_filter('zippy_fetch_meta_data', array($this, 'existingContent'), 10, 2);
    }

    public function progress($content)
    {
        global $post;

        $zippy = Zippy::instance();

        $student        = $zippy->cache->get('student');
        $settings       = get_option('zippy_customizer_course_options', array());
        $show_progress  = isset($settings['show_progress']) ? $settings['show_progress'] == 1 : true;


        if ($show_progress && $student !== null) {
            $student->fetch();

            $course = $student->getCourse($post->ID);
            
            if ($course !== null) {
                $progress = $student->getCourseProgress($course);

                $content = '<p class="zippy-course-progress"><em>' . sprintf(__("You have completed %s&#37; of this course.", ZippyCourses::TEXTDOMAIN), $progress) . '</em></p>' . $content;
            }
        }

        return $content;
    }
    public function entries($content)
    {
        global $post;

        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');

        if (!is_object($post) || $post->post_type !== 'course' || $student === null) {
            return $content;
        }

        $course = $student->getCourse($post->ID);

        if ($course !== null) {
            $view = new ZippyCourses_CourseEntries_View($course);

            return $content .= $view->render();
        }

        return $content;
    }

    public function publicDetails($content)
    {
        global $post;


        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');

        if (!is_object($post) || $post->post_type !== 'course') {
            return $content;
        }

        $student_middleware = $zippy->middleware->get('student-access');
        $member_middleware  = $zippy->middleware->get('member-access');

        $public = get_post_meta($post->ID, 'public', true);

        if (empty($public) || $public != 1) {
            return $content;
        }

        $course = $zippy->make('course', array($post->ID));
        $public_content = get_post_meta($post->ID, 'public_content', true);

        if ($student === null) {
            $content = do_shortcode(wpautop($public_content));
            $content .= $this->_renderCoursePricingOptions($course);
            
            return $this->_memberLoginPrompt($post->ID) . $content;
        } else {
            if (!$student_middleware->run($post) && $member_middleware->run($post)) {
                $content = do_shortcode(wpautop($public_content));
                $content .= $this->_renderCoursePricingOptions($course);

                return $content;
            }
        }

        return $content;
    }

    private function _memberLoginPrompt($id)
    {
        $zippy = Zippy::instance();

        $prompt     = __('Already a member with access to this course?', ZippyCourses::TEXTDOMAIN);
        $link_text  = __('Click here to login', ZippyCourses::TEXTDOMAIN);
        $redirect   = get_permalink($id);
        $url        = $zippy->core_pages->getUrl('login') . '?redirect_to=' . urlencode($redirect);

        return '<div class="zippy-alert zippy-alert-warning">' . $prompt . ' <a href="' . $url . '">' . $link_text . '</a></div>';
    }

    private function _renderCoursePricingOptions(Zippy_Course $course)
    {
        $output = '';

        if (!$course->products->count()) {
            $course->fetchDeep();
        }

        $products = array();
        foreach ($course->products->all() as $course_product) {
            $product = new stdClass;
            $product->id = $course_product->getId();
            $product->title = $course_product->getTitle();
            $product->price_string = $course_product->getPriceString();

            $products[] = $product;
        }

        if (count($products)) {
            $output .= '<h3>' . __('Purchase this Course', ZippyCourses::TEXTDOMAIN) . '</h3>';
            $output .= '<div class="zippy-pricing-options">';
            $output .= '<script type="text/data" class="zippy-course-pricing-data">' . json_encode($products) . '</script>';
            $output .= '</div>';
        }

        return $output;
    }

    public function entryTitles($data, $field)
    {
        global $post;

        if (!is_object($post) || $post->post_type != 'course' || $field != 'entries') {
            return $data;
        }

        $zippy = Zippy::instance();

        $data = is_array($data) ? $data : array();

        foreach ($data as $key => $entry) {
            if (!isset($entry->ID)) {
                continue;
            }

            $entry->title = html_entity_decode(get_the_title($entry->ID), ENT_QUOTES, 'UTF-8');

            foreach ($entry->entries as &$subentry) {
                if (!isset($subentry->ID)) {
                    continue;
                }

                $subentry->title = html_entity_decode(get_the_title($subentry->ID), ENT_QUOTES, 'UTF-8');
            }

            $data[$key] = $entry;
        }

        return $data;
    }

    public function existingContent($data, $field)
    {
        global $post, $wpdb;

        if (!is_object($post) || $post->post_type != 'course' || $field != 'existing_content') {
            return $data;
        }

        $zippy = Zippy::instance();

        $post_types = $zippy->utilities->entry->getPostTypes();
        foreach ($post_types as $key => $post_type) {
            $post_types[$key] = '"' . $post_type->post_type . '"';
        }

        $post_type_list = implode(', ', $post_types);
        
        $invalid_post_statuses = array('"auto-draft"','"trash"', '"inherit"');
        $invalid_post_status_list = implode(', ', $invalid_post_statuses);

        $entries = $wpdb->get_results("SELECT ID, post_title AS title, post_type FROM $wpdb->posts WHERE post_type IN ($post_type_list) AND post_status NOT IN ($invalid_post_status_list)");

        $existing_entries = $zippy->utilities->entry->getAllProtectedEntryIds();
        foreach ($entries as &$entry) {
            $entry->in_use = in_array($entry->ID, $existing_entries);
        }

        return $entries;
    }

    public function expired($content)
    {
        global $post, $wpdb;

        if (!is_object($post) || $post->post_type != 'course') {
            return $content;
        }

        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');
        $course = $zippy->make('course', array($post->ID));

        if ($student === null) {
            return $content;
        }

        $today = $zippy->utilities->datetime->getToday();
        $expiration_date = $zippy->utilities->course->getExpirationDate($student, $course);

        if ($expiration_date instanceof DateTime && $today->format('U') > $expiration_date->format('U')) {
            $notice = '<div class="zippy-alert zippy-alert-warning">' .
                sprintf(
                    __('Your access to this course expired on %s.'),
                    '<strong>' . $expiration_date->format(get_option('date_format')) . '</strong>'
                ) .
            '</div>';

            $content = $notice . $content;
        }

        return $content;
    }

    public function expiresSoon($content)
    {
        global $post, $wpdb;

        if (!is_object($post) || $post->post_type != 'course') {
            return $content;
        }

        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');
        $course = $zippy->make('course', array($post->ID));

        if ($student === null) {
            return $content;
        }

        $today = $zippy->utilities->datetime->getToday();
        $expiration_date = $zippy->utilities->course->getExpirationDate($student, $course);
        
        if ($expiration_date instanceof DateTime && $today->format('U') < $expiration_date->format('U')) {
            $soon_date = clone($expiration_date);
            $soon_date->modify("-1 week");

            if ($today->format('U') > $soon_date->format('U')) {
                $notice = '<div class="zippy-alert zippy-alert-warning">' .
                    sprintf(
                        __('Your access to this course will expire on %s.'),
                        '<strong>' . $expiration_date->format(get_option('date_format')) . '</strong>'
                    ) .
                '</div>';

                $content = $notice . $content;
            }
        }

        return $content;
    }
}
