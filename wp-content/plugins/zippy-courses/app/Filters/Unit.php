<?php

class ZippyCourses_Unit_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('the_content', array($this, 'entries'), 9);
    }

    public function entries($content)
    {
        global $post;

        if (!is_object($post) || $post->post_type !== 'unit') {
            return $content;
        }
        
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        if (!$student) {
            return $content;
        }

        $course_id  = $zippy->utilities->entry->getCourseId($post->ID);
        $course     = $student->getCourse($course_id);
        if ($course === null) {
            return $content;
        }

        $unit   = $course->getEntries()->get($post->ID);
        
        if ($unit !== null) {
            $view = new ZippyCourses_UnitEntries_View($unit);
            return $content .= $view->render();
        }

        return $content;
    }
}