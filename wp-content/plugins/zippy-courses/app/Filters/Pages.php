<?php

class ZippyCourses_Pages_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('the_content', array($this, 'register'), 10);
        add_filter('the_content', array($this, 'buy'), 10);
        add_filter('the_content', array($this, 'thankYou'), 10);

        add_filter('wp_footer', array($this, 'templates'));
    }
    public function register($content)
    {
        global $post;

        $zippy = Zippy::instance();
        
        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'register')) {
            return $content;
        }

        $claim = filter_input(INPUT_GET, 'claim');
        $qv_claim    = get_query_var('zippy_register');

        // Check for a valid free register query var
        if (empty($claim) && !empty($qv_claim)) {
            $product = $zippy->utilities->product->getByUid($qv_claim);
            if ($product && $product->isFree()) {
                $claim = $qv_claim;
            }
        }
        
        $product = $zippy->utilities->product->getByUid($claim);

        if ($product === null) {
            $transaction = $zippy->make('transaction');
            $transaction->buildByTransactionKey($claim);

            $product = $transaction->getProduct();
        }

        if ($product !== null) {
            $courses = $product->getCourses();

            $alert = '';

            if (count($courses)) {
                $alert .= '<div class="zippy-alert zippy-alert-success">';

                $alert .= '<p>' . __('You are registering for the following course(s):', ZippyCourses::TEXTDOMAIN) . '</p>';

                $alert .= '<ul>';
                foreach ($courses as $course_id) {
                    $alert .= '<li>' . get_the_title($course_id) . '</li>';
                }
                $alert .= '</ul>';
                
                $alert .= '</div>';
                
                $prompt     = __('Already have an account?', ZippyCourses::TEXTDOMAIN);
                $link_text  = __('Click here to login', ZippyCourses::TEXTDOMAIN);
                $url        = $zippy->core_pages->getUrl('login') . '?claim=' . $claim;

                $alert .= '<div class="zippy-alert zippy-alert-warning">';
                $alert .= '<p>' . $prompt . ' <a href="' . $url . '">' . $link_text . '</a>';
                $alert .= '</div>';

            }
            
            $content = $alert . $content;
        }

        return $content;
    }

    public function buy($content)
    {
        global $post;

        $zippy = Zippy::instance();
        
        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'buy')) {
            return $content;
        }

        $product = $zippy->utilities->product->getProductByBuyQueryVar();

        if ($product !== null) {
            if (!$zippy->utilities->product->inLaunchWindow($product)) {
                $message = __('This product is not currently available for sale.', ZippyCourses::TEXTDOMAIN);

                $content = '<div class="zippy-alert zippy-alert-error">' . $message . '</div>';
                $product = null;
            }
        }

        if ($product !== null) {
            $content  = wpautop($product->getContent());
            $content .= do_shortcode('[zippy_order_form product="' . $product->getId() . '"]');
        }

        return $content;
    }

    public function thankYou($content)
    {
        global $post, $zippy_transaction_key;

        $zippy = Zippy::instance();

        if (current_user_can('edit_others_posts') || !is_object($post) || !$zippy->core_pages->is($post->ID, 'thank_you')) {
            return $content;
        }

        $zippy_transaction_key = $zippy->utilities->cart->getTransactionKey();

        if (empty($zippy_transaction_key) && $zippy->cache->get('zippy_transaction_key') === null && $zippy->sessions->retrieve('zippy_transaction_key') === null) {
            $message = sprintf(__('It looks like you reached this page without placing an order! If you believe this warning is in error, please send us a message at %s and we’ll get everything sorted out.', ZippyCourses::TEXTDOMAIN), $zippy->utilities->email->getSystemEmailAddress());

            $content = '<div class="zippy-alert zippy-alert-error"><p>' . $message . '</p></div>';
            $content = apply_filters('zippy_courses_thank_you_page_warning_message', $content);
            $product = null;
        }

        return $content;
    }

    public function templates()
    {
        global $post;

        $zippy = Zippy::instance();
        
        if (is_object($post) && ($post->post_type == 'course' || $post->post_type == 'page')) {
            require_once(ZippyCourses::$path . 'assets/views/templates/public/product/pricing.php');
        }
    }
}
