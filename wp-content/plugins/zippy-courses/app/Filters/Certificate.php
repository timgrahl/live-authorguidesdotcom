<?php

class ZippyCourses_Certificate_Filters
{
    protected static $instance;
    protected $certificate;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_action('template_redirect', array($this, 'certificate'), 99);
    }

    public function certificate($output)
    {
        global $post, $wp_query;

        if (!isset($wp_query->query_vars['certificate']) || $post->post_type != 'course') {
            return $output;
        }
        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');
        $this->certificate = $zippy->make('certificate', array($post->ID));

        if ($this->certificate->isAvailable()) {
            $this->certificate->render();
            $zippy->activity->insert($student->getId(), $post->ID, 'certificate_download');
            die();
        } else {
            add_filter('the_content', array($this, 'renderErrors'), 99);
        }
    }

    public function renderErrors($output)
    {
        return $this->certificate->renderErrorMessages();
    }
}
