<?php

class ZippyCourses_Order_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('the_content', array($this, 'details'));

        add_filter('previous_post_link', array($this, 'nextPrevLinks'), 10, 4);
        add_filter('next_post_link', array($this, 'nextPrevLinks'), 10, 4);
    }

    public function details($content)
    {
        global $post;
        global $current_user;

        if (!is_object($post) || $post->post_type != 'zippy_order') {
            return $content;
        }

        $zippy = Zippy::instance();

        $order = $zippy->make('order');
        $order->build($post->ID);

        // If the Order is not Owned By this person, or if they're not an admin, then display an error message
        $middleware = $zippy->middleware->get('user-order-access');
        $access = $middleware->run($order);
        
        if (!$access) {
            $output = $middleware->renderErrorMessages();
            return $output;
        }


        // $refund_status = get_post_meta( $post->ID, 'refund_status', true );
        $refund_status = get_post_meta($post->ID, 'refund_status', true);

        $order_details_output = '';

        $order_details_output .= '<div class="zippy-order-details">';
        $order_details_output .= '<h2>' . __('Order Details', ZippyCourses::TEXTDOMAIN) . '</h2>';

        $order_details_output .= '<p class="zippy-order-date">' . __('Purchased on', ZippyCourses::TEXTDOMAIN) . ': ' . date('M d, Y', strtotime($post->post_date)) . '</p>';

        $order_details_output .= '<p class="zippy-order-product"><em>' . get_the_title($order->getProduct()->getId()) . '</em></p>' .
                    '<p class="zippy-order-amount">' . __('Total', ZippyCourses::TEXTDOMAIN). ': ' . $order->getTransactionsTotalString() . '</p>';

        if ($refund_status) :
            $order_details_output .= '<p class="zippy-order-refunded">' . __('This order has been refunded.', ZippyCourses::TEXTDOMAIN) . '</p>';
        else :
            // $order_details_output .= '<p><a class="zippy-button zippy-refund-request" href="" data-order-id="' . $post->ID . '">' . __('Request Refund', ZippyCourses::TEXTDOMAIN) . '</a></p>';
        endif;

        $order_details_output .= '</div>';

        $order_details_output = apply_filters('zippy_order_details', $order_details_output, $order);
        $order_details_output = apply_filters('zippy_order_details_html', $order_details_output, $order);

        $payments_output = '';

        $payments_output .= '<div class="zippy-order-payments">';
        $payments_output .= '<h2>' . __('Payments', ZippyCourses::TEXTDOMAIN) . '</h2>';

        $payments_output .= '<table class="zippy-order-history-table">';
            
        $payments_output .=     '<thead>' .
                        '<tr>' .
                            '<th class="zippy-table-order-number">' . __('Payment No.', ZippyCourses::TEXTDOMAIN) . '</th>' .
                            '<th class="zippy-table-order-date">' . __('Date', ZippyCourses::TEXTDOMAIN) . '</th>' .
                            '<th class="zippy-table-order-amount">' . __('Amount', ZippyCourses::TEXTDOMAIN) . '</th>' .
                            '<th class="zippy-table-order-type">' . __('Type', ZippyCourses::TEXTDOMAIN) . '</th>' .
                            '<th class="zippy-table-order-actions">' . __('Method', ZippyCourses::TEXTDOMAIN) . '</th>' .
                        '</tr>' .
                    '</thead>' .
                    '<tbody>';

        foreach ($order->getTransactions()->all() as $transaction) :
            $payment_details = $transaction->getDetails();
            $vendor = ucwords(str_replace(array('-', '_'), ' ', get_post_meta($post->ID, 'vendor', true)));
            
            $payments_output .= '<tr>';
                $payments_output .= '<td class="zippy-table-payment-number">' . get_the_title($transaction->getId()) . '</td>';
                $payments_output .= '<td class="zippy-table-payment-date">' . get_the_date($zippy->utilities->datetime->getDateFormat(), $transaction->getId()) . '</td>';
                $payments_output .= '<td class="zippy-table-payment-amount">' . $transaction->getTotalString() . '</td>';
                $payments_output .= '<td class="zippy-table-payment-type">' . $transaction->getTypeLabel() . '</td>';
                $payments_output .= '<td class="zippy-table-payment-vendor">' . $vendor . '</td>';
            $payments_output .= '</tr>';
        endforeach;

        $payments_output .= '</tbody></table>';

        $payments_output .= '</div>';

        $payments_output = apply_filters('zippy_order_payments', $payments_output, $order->getTransactions()->all());
        $payments_output = apply_filters('zippy_order_transactions_html', $payments_output, $order->getTransactions()->all());

        $links = '<a class="zippy-button" href="' . $zippy->core_pages->getUrl('order_history') . '">&laquo; ' . __('Back to Your Orders', ZippyCourses::TEXTDOMAIN) . '</a>';

        return $order_details_output . $payments_output . $links;

        return $content;
    }

    public function registerFirst()
    {
        global $post;

        if (!is_object($post) || $post->post_type != 'product') {
            return;
        }

        $zippy = Zippy::instance();

        $settings = get_option('zippy_payment_general_settings', array());
        $register_first =  isset($settings['payment_flow']) && $settings['payment_flow'] == '0';

        if ($register_first && !is_user_logged_in()) {
            $url = $zippy->core_pages->getUrl('register') . '?redirect_to=' . urlencode($zippy->core_pages->getUrl('buy') . $post->ID);
            wp_redirect($url);
            exit;
        }
    }

    public function nextPrevLinks($output, $format, $link, $adjacent)
    {
        global $post;

        if (!is_object($post) || $post->post_type != 'zippy_order') {
            return $output;
        }

        return;
    }
}
