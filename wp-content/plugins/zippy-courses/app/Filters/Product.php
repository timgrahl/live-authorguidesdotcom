<?php

class ZippyCourses_Product_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('the_content', array($this, 'buy'));
        
        add_filter('template_redirect', array($this, 'requiredAccessLogin'));
        add_filter('the_content', array($this, 'requiredAccess'), 11, 1);

        add_filter('template_redirect', array($this, 'registerFirst'));

        add_filter('previous_post_link', array($this, 'nextPrevLinks'), 10, 4);
        add_filter('next_post_link', array($this, 'nextPrevLinks'), 10, 4);
    }

    public function buy($content)
    {
        global $post;
        $zippy = Zippy::instance();
        if (!is_object($post)
            || ($post->post_type != 'product' && !$zippy->core_pages->is($post->ID, 'buy'))) {
            return $content;
        }

        if ($post->post_type === 'product') {
            $product = $zippy->make('product', array($post->ID));
        } else {
            $product = $zippy->utilities->product->getProductByBuyQueryVar();
        }

        if ($product === null) {
            return $content;
        }

        $requires_ssl = apply_filters('zippy_checkout_requires_ssl', __return_false());

        if ($requires_ssl && !is_ssl()) {
            $alert =  '';
            if (current_user_can('edit_others_posts')) {
                $alert .= '<div class="zippy-alert zippy-alert-warning">';
                $alert .= __('<strong>Warning:</strong> This product will be unavailable for purchase until an SSL certificate is detected.', ZippyCourses::TEXTDOMAIN);
                $alert .= '</div>';
            }

            $alert .= '<div class="zippy-alert zippy-alert-error">';
                $alert .= __('This product is not currently available for sale.', ZippyCourses::TEXTDOMAIN);
            $alert .= '</div>';

            return $alert;
        }

        $is_active = get_post_meta($product->getId(), 'status', true) == 'active';
        if (!$is_active) {
            $alert = '<div class="zippy-alert zippy-alert-error">';
                $alert .= __('This product is not currently available for sale.', ZippyCourses::TEXTDOMAIN);
            $alert .= '</div>';

            return $alert;
        }


        if ($product->getType() == 'free' && current_user_can('edit_others_posts')) {
            $alert = '<div class="zippy-alert zippy-alert-warning">';
            $alert .= __('This product is <strong>Free</strong>, and visitors will be automatically redirected to registration, or their dashboard, when visiting this product.', ZippyCourses::TEXTDOMAIN);
            $alert .= '</div>';

            $content = $alert . $content;
        }

        $content .= do_shortcode('[zippy_order_form product="' . $post->ID . '"]');

        return $content;
    }

    // @TODO: pull product and tier name
    public function requiredAccessLogin()
    {
        global $post;

        if (!is_object($post) || $post->post_type != 'product') {
            return;
        }

        $zippy = Zippy::instance();
        $product = $zippy->make('product', array($post->ID));
        if (!$product->hasExistingAccessRequirement()) {
            return;
        }

        $student = $zippy->cache->get('student');
        if (!$student) {
            $alert .= '<p>';
            $alert .= __('You must log in before you can purchase this product.', ZippyCourses::TEXTDOMAIN);
            $alert .= '</p>';

            $zippy->sessions->flashMessage(
                $alert,
                'warning'
            );
            
            wp_redirect($zippy->core_pages->getUrl('login') . '?redirect_to=' . urlencode(get_permalink($post->ID)));
        }
    }

    public function requiredAccess($content)
    {
        global $post;

        if (!is_object($post) || $post->post_type != 'product') {
            return $content;
        }

        $zippy = Zippy::instance();
        $middleware = $zippy->middleware->get('student-existing-product-access');
        if ($middleware->run($post)) {
            return $content;
        } else {
            return $middleware->renderErrorMessages();
        }


    }

    public function registerFirst()
    {
        global $post;

        if (!is_object($post) || $post->post_type != 'product') {
            return;
        }

        $zippy = Zippy::instance();
        $product = $zippy->make('product', array($post->ID));

        if ($this->requiresRegistrationRedirect()) {
            $login_url = $zippy->core_pages->getUrl('login') . '?redirect_to=' . urlencode($zippy->core_pages->getUrl('buy') . $product->getId() . '?buy-now');
            $zippy->sessions->flashMessage(
                sprintf(
                    __('You must register to purchase %s. If you already have an account, <a href="%s">click here</a> to login.', ZippyCourses::TEXTDOMAIN),
                    '<strong>' . $product->getTitle() . '</strong>',
                    $login_url
                ),
                'warning'
            );
            $url = $zippy->core_pages->getUrl('register') . '?redirect_to=' . urlencode($zippy->core_pages->getUrl('buy') . $post->ID . '?buy-now');
            wp_redirect($url);
            exit;
        }
    }

    /**
     * Check to see if the current Product Page Requires a "Register First" Redirect
     * @return bool
     */
    protected function requiresRegistrationRedirect()
    {
        global $post;
        $zippy = Zippy::instance();

        $product = $zippy->make('product', array($post->ID));
        $settings = get_option('zippy_payment_general_settings', array());
        $register_first =  isset($settings['payment_flow']) && $settings['payment_flow'] == '0';
        $payment_method = isset($settings['method']) ? $settings['method'] : null;
        $is_active = get_post_meta($product->getId(), 'status', true) == 'active';
        $payment_method_requires_redirect = apply_filters('zippy_courses_payment_method_requires_register_first_redirect', false);

        return $register_first
            && !is_user_logged_in()
            && $payment_method_requires_redirect
            && $is_active;
    }

    public function nextPrevLinks($output, $format = '', $link = '', $adjacent = '')
    {
        global $post;

        if (!is_object($post) || $post->post_type != 'product') {
            return $output;
        }

        return;
    }
}
