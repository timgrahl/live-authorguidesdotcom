<?php

class ZippyCourses_Student_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('the_content', array($this, 'entries'), 11);
        add_filter('the_content', array($this, 'publicDetails'), 10);
    }

    public function entries($content)
    {
        global $post;

        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');

        if (!is_object($post) || $post->post_type !== 'course' || $student === null) {
            return $content;
        }

        $course = $student->getCourse($post->ID);

        if ($course !== null) {
            $view = new ZippyCourses_CourseEntries_View($course);

            return $content .= $view->render();
        }

        return $content;
    }

    public function publicDetails($content)
    {
        global $post;

        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');

        if (!is_object($post) || $post->post_type !== 'course' || $student !== null) {
            return $content;
        }

        $public = get_post_meta($post->ID, 'public', true);

        if (empty($public) || $public != 1) {
            return $content;
        }

        $course = $zippy->make('course', array($post->ID));
        $public_content = get_post_meta($post->ID, 'public_content', true);

        if (!empty($public_content)) {
            $content = wpautop($public_content);
            $content .= $this->_renderCoursePricingOptions($course);
        }

        return $this->_memberLoginPrompt($post->ID) . $content;
    }

    private function _memberLoginPrompt($id)
    {
        $zippy = Zippy::instance();

        $prompt     = __('Already a member with access to this course?', ZippyCourses::TEXTDOMAIN);
        $link_text  = __('Click here to login', ZippyCourses::TEXTDOMAIN);
        $url        = $zippy->core_pages->getUrl('login') . '/?11';

        return $prompt;
    }

    private function _renderCoursePricingOptions(Zippy_Course $course)
    {
        $output = '';

        if (!$course->products->count()) {
            $course->fetchDeep();
        }

        $products = $course->products->toArray('title');

        $output .= '<div class="zippy-course-pricing">';

        $output .= '<div class="zippy-product-selection">';
        $output .= '<select>';

        foreach ($course->products->all() as $product) {
            $output .= '<option value="' . $product->getId() . '">' . $product->getTitle() . '</option>';
        }

        $output .= '</select>';
        $output .= '</div>';

        $output .= '<div class="zippy-product-price">';
        $output .= '<p><strong>' . __('Price', ZippyCourses::TEXTDOMAIN) . ':</strong></p>';
        $output .= '</div>';
    
        $output .= '<div class="zippy-buy-product"><a href="">' . apply_filters('zippy_buy_button_text',__('Buy Now', ZippyCourses::TEXTDOMAIN)) . '</a></div>';
        $output .= '</div>';
        
        /**
         * Filter the output of the products HTML for a single course in the Course Directory
         *
         * @since 1.0.0
         *
         * @see   ZippyCourses_CourseDirectory_ShortcodeView::renderCoursePricingOptions
         *
         * @param string                $output The HTML that will be output
         * @param Zippy_Course   $course The Course object
         */
        return apply_filters('zippy_course_directory_products_html', $output, $course);
    }
}
