<?php

class ZippyCourses_Notifications_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('lostpassword_url', array($this, 'lostPasswordUrl'));
    }

    /**
     * Reset the Forgot Password URL on the Zippy Courses login page
     * While Keeping it the same for Admin/Login Pages
     */
    public function lostPasswordUrl($url)
    {
        $zippy = Zippy::instance();
        $zippy_forgot_password_url = $zippy->core_pages->getUrl('forgot_password');
        if ($this->isAdminLoginPage()) {
            return $url;
        }
        return $zippy_forgot_password_url;
    }
    protected function isAdminLoginPage()
    {
        return in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
    }
}
