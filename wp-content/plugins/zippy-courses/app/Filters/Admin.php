<?php

class ZippyCourses_Admin_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('zippy_admin_page_title', array($this, 'newStudentTitleButton'), 10, 2);
        add_filter('admin_body_class', array($this, 'bodyClass'));
        add_filter('hidden_meta_boxes', array($this, 'hiddenMetaBoxes'), 10, 3);
        add_filter('post_submitbox_start', array($this, 'returnToCourse'), 10, 3);
    }

    public function hiddenMetaBoxes($hidden, $screen, $use_defaults)
    {
        $screen = get_current_screen();
        $locked = array('course', 'unit', 'lesson', 'quiz', 'quiz_question', 'zippy_order', 'product', 'transaction');

        if (in_array($screen->id, $locked)) {
            return array('slugdiv');
        }

        return $hidden;
    }

    public function returnToCourse()
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post)) {
            return;
        }

        $course_id = $zippy->utilities->entry->getCourseId($post->ID);


        if ($course_id) {
            echo '<div class="zippy-save-and-return">';
                echo '<input name="save" type="submit" class="button button-large" accesskey="p" value="' .
                    __('Save', ZippyCourses::TEXTDOMAIN) .
                '" />';

                echo '<input name="save" type="submit" class="button button-primary button-large" accesskey="p" value="' .
                    __('Save and Return to Course', ZippyCourses::TEXTDOMAIN) . '" />';

            echo '</div>';
        }
    }

    public function bodyClass($classes)
    {
        global $post;

        if (is_object($post)) {
            $zippy = Zippy::instance();

            $protected_entries = $zippy->utilities->entry->getAllProtectedEntryIds();

            if (in_array($post->ID, $protected_entries)) {
                $classes .= ' zippy-entry ';
            }
        }

        return $classes;
    }

    public function newStudentTitleButton($title, $id)
    {
        if ($id == 'zippy-students') {
            $title .= '<a href="' . admin_url('admin.php?page=zippy-new-student') . '" class="page-title-action">' .
                __('Add New', ZippyCourses::TEXTDOMAIN) .
            '</a>';
        }

        return $title;
    }
}
