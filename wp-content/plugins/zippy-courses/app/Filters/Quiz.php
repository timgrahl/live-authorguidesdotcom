<?php

class ZippyCourses_Quiz_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('the_content', array($this, 'quiz'));
        add_filter('the_content', array($this, 'entryQuiz'));
        add_filter('the_content', array($this, 'quizResults'));
        add_filter('the_content', array($this, 'quizButton'));

        add_filter('previous_post_link', array($this, 'hideNextPrev'), 12, 4);
        add_filter('next_post_link', array($this, 'hideNextPrev'), 12, 4);

        add_filter('zippy_fetch_meta_data', array($this, 'populateQuizQuestions'), 10, 2);
    }

    public function populateQuizQuestions($data, $field)
    {
        global $post;

        if (!is_object($post) || $post->post_type != 'quiz' || $field != 'questions') {
            return $data;
        }

        $zippy = Zippy::instance();

        $data = is_array($data) ? $data : array();

        foreach ($data as $key => $question) {
            if (!isset($question->ID)) {
                return;
            }

            $question->post = new stdClass;
            $question->post->ID = $question->ID;
            $question->post->post_title = get_the_title($question->ID);

            $question->correct = (int) get_post_meta($question->ID, 'correct', true);
            $question->answers = $zippy->utilities->getJsonMeta($question->ID, 'answers', true);

            unset($question->ID);

            $data[$key] = $question;
        }

        return $data;
    }

    public function quizButton($content)
    {
        global $post, $wp_query;

        if (isset($wp_query->query_vars['quiz'])) {
            return $content;
        }

        $zippy = Zippy::instance();

        $quiz = $zippy->utilities->entry->getQuiz($post->ID);

        if ($quiz) {
            $content .= '<p class="zippy-take-quiz"><a href="' . get_permalink() . 'quiz" class="zippy-button zippy-button-primary">' . __('Take Quiz', ZippyCourses::TEXTDOMAIN) . '</a></p>';
        }

        return $content;
    }

    public function quizResults($content)
    {
        global $post, $wp_query;

        if (!isset($wp_query->query_vars['quiz']) || !isset($_GET['results'])) {
            return $content;
        }

        $zippy = Zippy::instance();
        $quiz = $zippy->utilities->entry->getQuiz($post->ID);
        $retake_url = $post->post_type == 'quiz' ? get_permalink($post->ID) : get_permalink($post->ID) . 'quiz';
        $student = $zippy->cache->get('student');

        $results = get_user_meta($student->getId(), 'quiz_results');
        $results = end($results);
        $results = json_decode($results);
        
        $num_questions = count($results->questions);
        $num_correct = ceil($results->score / 100 * $num_questions);

        $output = '';

        $output .= '<div class="zippy-quiz-results">';
        $output .= '<div class="zippy-alert zippy-alert-success">';
        $output .= '<p>' .
            sprintf(
                __("You scored a %s&#37; by answering %s out of %s questions correctly", ZippyCourses::TEXTDOMAIN),
                $results->score,
                $num_correct,
                $num_questions
            ) . '</p>';
        $output .= '</div>';

        foreach ($results->questions as $question) {
            $output .= '<div class="zippy-quiz-results-question ' .
                ($question->correct ? 'zippy-question-correct' : 'zippy-question-incorrect')
            . '">';
            $output .= '<h4>' . $question->question . '</h4>';
            if ($question->correct) {
                $output .= '<p><strong>Your Answer:</strong> ' . stripslashes($question->answer->given->content) . '</p>';
            } else {
                $output .= '<p><s><strong>Your Answer:</strong> ' . stripslashes($question->answer->given->content) . '</s></p>';
                $output .= '<p><strong>Correct Answer:</strong> ' . stripslashes($question->answer->correct->content) . '</p>';
            }
            $output .= '</div>';
        }
        $output .= '</div>';

        $output .= '<div class="zippy-quiz-results-buttons">';
        
        $output .= '<a href="' . get_permalink($post->ID) . '" class="zippy-button zippy-return-to-quiz-button">&laquo; ' . __('Return to Lesson', ZippyCourses::TEXTDOMAIN) . '</a> ';

        $output .= '<a href="' . $retake_url . '" class="zippy-button zippy-retake-quiz-button">' . __('Retake Quiz', ZippyCourses::TEXTDOMAIN) . '</a> ';

        $next_id = $zippy->utilities->entry->getNextEntryId($post->ID);

        if ($next_id) {
            $output .= '<a href="' . get_permalink($next_id) . '" class="zippy-button zippy-button-primary zippy-quiz-next-entry-button">' .
                __('Next Lesson', ZippyCourses::TEXTDOMAIN) .
            '&raquo;</a> ';
        }
        $output .= '</div>';
        
        $content = $output;

        return $content;
    }

    public function entryQuiz($content)
    {
        global $post, $wp_query;

        if (!isset($wp_query->query_vars['quiz']) || isset($_GET['results'])) {
            return $content;
        }

        $zippy = Zippy::instance();

        $quiz = $zippy->utilities->entry->getQuiz($post->ID);

        if ($quiz === null) {
            return $content;
        }

        return $this->_buildQuiz($quiz);
    }

    public function quiz($content)
    {
        global $post;

        if (!is_object($post) || $post->post_type !== 'quiz') {
            return $content;
        }

        if (isset($_GET['results'])) {
            return $content;
        }
       
        $zippy = Zippy::instance();

        $quiz = $zippy->make('quiz', array($post->ID));
        $quiz->ID = $quiz->id;

        return $this->_buildQuiz($quiz);
    }

    private function _buildQuiz($quiz)
    {
        global $post;

        if (!is_object($post)) {
            return;
        }

        $zippy = Zippy::instance();

        $path = ZippyCourses::$path . 'assets/views/templates/public/quiz/quiz.php';

        $content    = '';
        $questions  = $zippy->utilities->quiz->getQuestions($quiz->ID);

        if (count($questions)) {
            ob_start();
            include($path);
            $template = ob_get_clean();

            $passing = isset($quiz->required) && $quiz->required->pass ? $quiz->pass : 0;

            $attributes = 'id="zippy-quiz" style="display: none;" data-id="' . $quiz->ID . '" data-pass="' . $passing . '" ';

            $content = '<div ' . $attributes . '>';
            $content .= $template;
            $content .= '</div>';
        } else {
            $content = '<div class="zippy-alert zippy-alert-error zippy-quiz-empty-alert">';
            $content .= '<p>' . __('This quiz has no questions.  Please try again later after they have been added.', ZippyCourses::TEXTDOMAIN) . '</p>';
            $content .= '<p><a href="' . get_permalink($post->ID) . '">&laquo;' . __('Back to Lesson', ZippyCourses::TEXTDOMAIN) . '</a></p>';

            $content .= '</div>';
        }

        return $content;
    }

    public function hideNextPrev($output, $format = '', $link = '', $adjacent = '')
    {
        global $post, $wp_query;

        if (isset($wp_query->query_vars['quiz']) || isset($_GET['results'])) {
            return '';
        }

        return $output;
    }
}
