<?php

class ZippyCourses_Comment_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('the_comments', array($this, 'comments'));
    }

    public function comments($comments)
    {
        $zippy = Zippy::instance();

        $middleware = $zippy->middleware->get('student-access');

        if ($middleware) {
            foreach ($comments as $key => $comment) {
                $cpost = new stdClass;
                $cpost->ID = $comment->comment_post_ID;

                if (!$middleware->run($cpost)) {
                    unset($comments[$key]);
                }
            }
        }

        return array_values($comments);
    }
}
