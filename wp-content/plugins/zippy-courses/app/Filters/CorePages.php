<?php

class ZippyCourses_CorePages_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('the_content', array($this, 'dashboard'));
        add_filter('the_content', array($this, 'login'));
        add_filter('the_content', array($this, 'registration'));
        add_filter('the_content', array($this, 'account'));
        add_filter('the_content', array($this, 'editAccount'));
        add_filter('the_content', array($this, 'thankYou'));
        add_filter('the_content', array($this, 'buy'));
        add_filter('the_content', array($this, 'orderHistory'));
        add_filter('the_content', array($this, 'courseDirectory'));
        add_filter('the_content', array($this, 'forgotPassword'));
        add_filter('the_content', array($this, 'resetPassword'));

        add_filter('pre_get_posts', array($this, 'filter'));
    }

    public function dashboard($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'dashboard')) {
            return $content;
        }

        $content .= do_shortcode('[zippy_dashboard]');

        return $content;
    }

    public function login($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'login')) {
            return $content;
        }

        $content .= do_shortcode('[zippy_login_form]');

        return $content;
    }

    public function registration($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'register')) {
            return $content;
        }

        $content .= do_shortcode('[zippy_registration_form]');

        return $content;
    }

    public function account($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'account')) {
            return $content;
        }

        $content .= do_shortcode('[zippy_account]');

        return $content;
    }

    public function editAccount($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'edit_account')) {
            return $content;
        }

        $content .= do_shortcode('[zippy_edit_account]');

        return $content;
    }

    public function thankYou($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'thank_you')) {
            return $content;
        }
        $transaction_key = $zippy->utilities->cart->getTransactionKey();
        if (empty($post->post_content) && (!empty($transaction_key) || current_user_can('edit_posts'))) {

            $content = $zippy->utilities->post->getDefaultThankYouText();
            $content = wpautop(do_shortcode($content));
        }

        // If there is a Product associated with this course, check for a custom thank-you message
        $product = $zippy->utilities->product->getProductByTransactionKey($transaction_key);
        if ($product) {
            $content = $product->getCustomMessage($content, 'thank_you');
            $content = wpautop($content);
        }
        return $content;
    }

    public function buy($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'buy')) {
            return $content;
        }

        return $content;
    }

    public function orderHistory($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'order_history')) {
            return $content;
        }

        $content .= do_shortcode('[zippy_order_history]');

        return $content;
    }

    public function courseDirectory($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'course_directory')) {
            return $content;
        }

        $content .= do_shortcode('[zippy_course_directory]');

        return $content;
    }

    public function forgotPassword($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'forgot_password')) {
            return $content;
        }

        $content .= do_shortcode('[zippy_forgot_password]');

        return $content;
    }

    public function resetPassword($content)
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'reset_password')) {
            return $content;
        }

        $content .= do_shortcode('[zippy_reset_password]');

        return $content;
    }

    public function filter($query)
    {
        if (current_user_can('edit_others_posts')) {
            return $query;
        }

        if (!is_home() && !is_search()) {
            return $query;
        }
        
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        if ($student === null) {
            $post__not_in = array_values($zippy->core_pages->getPrivateOnlyCorePageIds());
        } else {
            $post__not_in = array_values($zippy->core_pages->getPublicOnlyCorePageIds());
        }

        $query->set('post__not_in', array_merge($query->get('post__not_in'), $post__not_in));

        return $query;
    }
}
