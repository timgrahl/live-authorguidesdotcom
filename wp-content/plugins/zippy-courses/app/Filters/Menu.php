<?php

class ZippyCourses_Menu_Filters
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_filter('wp_get_nav_menu_items', array($this, 'excludeMenuItems'), 11, 3);
    }

    public function excludeMenuItems($items, $menu, $args)
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        foreach ($items as $key => $item) {
            $item->zippy_visibility = get_post_meta($item->ID, '_menu_item_zippy_visibility', true);

            switch ($item->zippy_visibility) {
                case '2':
                    if ($student === null) {
                        unset($items[$key]);
                    }
                    break;
                case '1':
                    if ($student !== null) {
                        unset($items[$key]);
                    }
                    break;
                case '0':
                default:
                    break;
            }
        }

        return array_values($items);
    }
}
