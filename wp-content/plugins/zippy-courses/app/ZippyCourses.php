<?php

class ZippyCourses
{
    const VERSION = '1.3.2';
    const DB_VERSION = '1.1.0';
    const TEXTDOMAIN = 'zippy-courses';

    // Pseuedo Constants
    public static $path;
    public static $url;

    private static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        self::$path   = plugin_dir_path(dirname(__FILE__));
        self::$url    = plugin_dir_url(dirname(__FILE__));

        $this->actions();
    }

    private function actions()
    {
        $zippy = Zippy::instance();

        add_action('plugins_loaded', array($this, 'loadTextdomain'));
        add_action('plugins_loaded', array($this, 'routes'));
        add_action('plugins_loaded', array($this, 'ajax'));
        add_action('plugins_loaded', array($this, 'transactionProcessor'));
        add_action('plugins_loaded', array($this, 'forms'));
        add_action('plugins_loaded', array($this, 'paymentGatewayIntegrations'));
        add_action('plugins_loaded', array($this, 'emailListIntegrations'));
        add_action('plugins_loaded', array($this, 'initCache'));
        add_action('plugins_loaded', array($this, 'widgets'));
        add_action('plugins_loaded', array($this, 'mailer'));
        add_action('plugins_loaded', array($this, 'forums'));
        add_action('plugins_loaded', array($this, 'actionHooks'));
        add_action('plugins_loaded', array($this, 'customizer'));

        add_action('init', array($this, 'postTypes'), 9);
        add_action('init', array($this, 'filters'), 9);
        add_action('init', array($this, 'middlewares'), 1);

        add_action('admin_print_scripts', array($this, 'adminAssets'));
        add_action('wp_enqueue_scripts', array($this, 'publicAssets'));

        // Hooks Needed only in admin
        if (is_admin()) {
            add_action('current_screen', array($this, 'dequeueWpeCommonCSS'), 11);

            add_action('plugins_loaded', array($this, 'updater'));
            add_action('plugins_loaded', array($this, 'adminUX'));
            add_action('plugins_loaded', array($this, 'report'));
            
            add_action('admin_init', array($this, 'exporter'));
            add_action('admin_init', array($this, 'postTypeTabs'), 10);
            add_action('admin_init', array($this, 'metaboxes'), 9);

            add_action('init', array($this, 'settings'), 9);
            add_action('init', array($this, 'adminPages'));
            

            add_filter('upload_mimes', array($this, 'mimeTypes'), 10, 1);
        }

        // Hooks Needed only in public
        if (!is_admin()) {
            add_action('plugins_loaded', array($this, 'analytics'));
            add_action('plugins_loaded', array($this, 'shortcodes'));
            add_action('init', array($this, 'download'), 9);
            add_action('init', array($this, 'alerts'), 9);
            add_action('init', array($this, 'setupStudent'), 2);
        }
    }

    /**
     * Load plugin textdomain.
     *
     * @since 1.0.0
     */
    public function loadTextdomain() {
        load_plugin_textdomain(ZippyCourses::TEXTDOMAIN, false, dirname(plugin_basename(dirname(__FILE__))) . '/lang' ); 
    }

    public function settings()
    {
        $zippy = Zippy::instance();
        $zippy->make('settings');
    }

    public function shortcodes()
    {
        $zippy = Zippy::instance();

        $zippy->make('shortcodes');
    }

    public function widgets()
    {
        new ZippyCourses_Widgets;
    }

    public function analytics()
    {
        $zippy = Zippy::instance();
        $zippy->make('analytics');
    }

    public function postTypes()
    {
        $zippy = Zippy::instance();
        $zippy->make('post_types');
    }

    public function postTypeTabs()
    {
        $zippy = Zippy::instance();
        $zippy->make('post_type_tabs');
    }

    public function metaboxes()
    {
        $zippy = Zippy::instance();
        $mb = $zippy->make('metaboxes');
        $mb->build();
    }

    public function adminPages()
    {
        $zippy = Zippy::instance();
        $zippy->make('admin_pages');
    }

    public function emailListIntegrations()
    {
        if (defined('DOING_AJAX') && DOING_AJAX) {
            return;
        }
        
        $zippy = Zippy::instance();
        $zippy->make('email_list_integrations');
    }

    public function paymentGatewayIntegrations()
    {
        $zippy = Zippy::instance();
        
        $zippy->make('payment_gateway_ipn');
        $zippy->make('payment_gateway_integrations');
        
        new ZippyCourses_PaymentGatewayIntegrations;
    }

    public function transactionProcessor()
    {
        $zippy = Zippy::instance();
        $zippy->make('transaction_processor');
    }

    public function ajax()
    {
        $zippy = Zippy::instance();
        $ajax = $zippy->make('ajax');
    }

    public function initCache()
    {
        $cache = new ZippyCourses_Cache;
    }

    public function routes()
    {
        $zippy = Zippy::instance();
        $zippy->make('routes');
    }

    public function forms()
    {
        $zippy = Zippy::instance();
        $zippy->make('forms');
    }

    public function ipnListeners()
    {
        $zippy = Zippy::instance();
        $zippy->make('payment_gateway_listeners');
    }

    public function alerts()
    {
        $zippy = Zippy::instance();
        $zippy->make('alerts');
    }

    public function setupStudent()
    {
        global $current_user;

        $zippy = Zippy::instance();

        if (!is_admin() && $current_user->ID) {
            $student = $zippy->make('student', array($current_user->ID));
            $student->fetch();

            $zippy->cache->set('student', $student);
        }
    }

    public function middlewares()
    {
        new ZippyCourses_Middleware;
    }

    public function mailer()
    {
        $zippy = Zippy::instance();
        $zippy->make('mailer');
    }

    public function forums()
    {
        new ZippyCourses_ForumIntegrations;
    }

    public function updater()
    {
        new ZippyCourses_PluginUpdater;
    }

    public function download()
    {
        new ZippyCourses_SecureDownload;
    }

    public function exporter()
    {
        new ZippyCourses_ZippyCourses_Exporter;
    }

    public function filters()
    {
        ZippyCourses_Course_Filters::instance();
        ZippyCourses_Quiz_Filters::instance();

        if (is_admin()) {
            ZippyCourses_Admin_Filters::instance();
        }

        if (!is_admin()) {
            ZippyCourses_Pages_Filters::instance();
            ZippyCourses_CorePages_Filters::instance();
            ZippyCourses_Unit_Filters::instance();
            ZippyCourses_Entry_Filters::instance();
            ZippyCourses_Product_Filters::instance();
            ZippyCourses_Order_Filters::instance();
            ZippyCourses_Comment_Filters::instance();
            ZippyCourses_Menu_Filters::instance();
            ZippyCourses_Certificate_Filters::instance();
            ZippyCourses_Notifications_Filters::instance();
        }
    }

    public function actionHooks()
    {
        if (is_admin()) {
            ZippyCourses_Admin_Actions::instance();
        }

        if (!is_admin()) {
            ZippyCourses_CorePages_Actions::instance();
        }
        
        ZippyCourses_Entry_Actions::instance();
        ZippyCourses_Product_Actions::instance();
        ZippyCourses_Order_Actions::instance();
        ZippyCourses_Student_Actions::instance();
    }

    public function publicAssets()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        wp_enqueue_script('jquery');
        wp_enqueue_script('underscore');
        wp_enqueue_script('zippy-plugins', ZippyCourses::$url . 'assets/js/public/plugins.js', array('jquery'), ZippyCourses::VERSION, true);
        wp_enqueue_script('zippy-js', ZippyCourses::$url . 'assets/js/public/zippy.js', array('zippy-plugins', 'underscore'), ZippyCourses::VERSION, true);
        
        wp_enqueue_style('zippy', ZippyCourses::$url . 'assets/css/public.css', array(), ZippyCourses::VERSION);

        wp_enqueue_script('vue', ZippyCourses::$url . 'assets/js/vendor/vue/vue.min.js', array('jquery', 'underscore'), '0.12.7', true);
        wp_enqueue_script('zippy-public', ZippyCourses::$url . 'assets/js/zippy-public.js', array('zippy-js', 'vue'), ZippyCourses::VERSION, true);

        $localized = array(
            'analytics_id' => $zippy->cache->get('analytics_id'),
            'ajaxurl'      => admin_url('admin-ajax.php')
        );

        if ($student) {
            $localized['user_id'] = $student->getId();
        }

        wp_localize_script('zippy-js', 'Zippy', $localized);
    }

    public function adminAssets()
    {
        wp_register_style('jquery-ui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
        
        wp_enqueue_style('zippy', ZippyCourses::$url . 'assets/css/admin.css', array(), ZippyCourses::VERSION, 'screen');
        wp_enqueue_style('jquery-ui');

        wp_enqueue_script('jquery-ui-sortable');
        wp_enqueue_script('jquery-ui-datepicker');

        wp_enqueue_script('zippy-plugins', ZippyCourses::$url . 'assets/js/admin/plugins.js', array('jquery'), ZippyCourses::VERSION, 'screen');
        wp_enqueue_script('zippy', ZippyCourses::$url . 'assets/js/admin/admin.js', array('zippy-plugins'), ZippyCourses::VERSION, 'screen');
        
        wp_enqueue_script('vue', ZippyCourses::$url . 'assets/js/vendor/vue/vue.min.js', array('jquery', 'underscore', 'jquery-ui-sortable', 'jquery-ui-datepicker'), '0.11.10', 'screen');
        wp_enqueue_script('zippy-admin', ZippyCourses::$url . 'assets/js/admin/zippy-admin.js', array('vue'), ZippyCourses::VERSION, 'screen');

        wp_localize_script('zippy', 'ZippyCourses', array(
            'js'        => ZippyCourses::$path . 'assets/js/',
            'strings'   => ZippyCourses::getStrings(),
            'admin_url' => admin_url(),
            'site_url'  => home_url(),
            'options'   => array(
                'posts_per_page' => get_option('posts_per_page', 10)
            )
        ));

    }

    public function adminUX()
    {
        $zippy = Zippy::instance();

        ZippyCourses_AdminUserExperience::instance();

        $notices = $zippy->make('admin_notices');
    }

    public static function getStrings()
    {
        return array(
            'new_course_button' => __('New Course', self::TEXTDOMAIN),
            'resend_registration_reminder_email' => __('The registration reminder has been resent.', ZippyCourses::TEXTDOMAIN),
            'clear_ontraport_status' => __('The Ontraport Order status has been cleared.', ZippyCourses::TEXTDOMAIN),
        );
    }

    public function mimeTypes($mime_types)
    {
        $mime_types['json'] = 'application/json';
        return $mime_types;
    }

    public function report()
    {
        new ZippyCourses_Remote_Analytics;
    }

    public function customizer()
    {
        new ZippyCourses_Customizer;
    }

    public function dequeueWpeCommonCSS()
    {
        $screen = get_current_screen();

        if ($screen->id == 'course' || $screen->id == 'admin_page_zippy-student') {
            wp_dequeue_style('wpe-common');
            wp_deregister_style('wpe-common');
        }
    }
}
