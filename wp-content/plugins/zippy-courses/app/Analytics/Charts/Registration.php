<?php

class ZippyCourses_Registration_Chart extends Zippy_Chart
{
    /**
     * A key => value array of data
     *
     * @since 1.0.0
     *
     * @var array
     */
    public $data = array();

    /**
     * The title of the chart
     * @var string
     */
    public $title;

    public $num_days = 21;

    public function __construct()
    {
        $this->title = '';
    }

    public function fetchData()
    {
        global $wpdb;

        $zippy = Zippy::instance();
        
        $table = $wpdb->prefix . 'zippy_user_activity';

        $sql = "SELECT * FROM $table WHERE type = 'registration'";

        $results = $wpdb->get_results($sql);

        $this->data = $this->parseData($results);

        return $this->data;
    }

    public function parseData(array $data)
    {
        $zippy = Zippy::instance();
        $output = $this->initializeDays();

        foreach ($data as $item) {
            $date = $zippy->utilities->datetime->getDate($item->create_time);
            $key  = $date->format('m-d-Y');

            if (isset($output[$key])) {
                $output[$key]++;
            }
        }

        return $output;
    }

    private function initializeDays()
    {
        $zippy = Zippy::instance();

        $output = array();
        $today = $zippy->utilities->datetime->getToday();

        for ($i=0; $i < $this->num_days; $i++) {
            $key = $today->format('m-d-Y');
            $output[$key] = 0;

            $today->modify('-1 day');
        }

        return array_reverse($output);
    }
}
