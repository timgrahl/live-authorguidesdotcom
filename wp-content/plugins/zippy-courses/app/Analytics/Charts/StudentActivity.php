<?php

class ZippyCourses_StudentActivity_Chart extends Zippy_Chart
{
    /**
     * A key => value array of data
     *
     * @since 1.0.0
     *
     * @var array
     */
    public $data = array();

    /**
     * The title of the chart
     * @var string
     */
    public $title;

    public $student_id;

    public $num_days = 21;

    public function __construct($student_id)
    {
        $this->title = '';
        $this->student_id = $student_id;
    }

    public function fetchData()
    {
        global $wpdb;

        $table = $wpdb->prefix . 'zippy_user_activity';

        $sql = $wpdb->prepare("SELECT * FROM $table WHERE user_id = %d", $this->student_id);

        $results = $wpdb->get_results($sql);
        $views = array();

        foreach ($results as $r) {
            if ($r->type == 'view') {
                $views[] = $r;
            }
        }

        $this->data = $this->parseData($views);

        return $this->data;
    }

    public function parseData(array $data)
    {
        $zippy = Zippy::instance();
        $output = $this->initializeDays();

        foreach ($data as $item) {
            $date = $zippy->utilities->datetime->getDate($item->create_time);
            $key  = $date->format('m-d-Y');

            if (isset($output[$key])) {
                $output[$key]++;
            }
        }

        return $output;
    }

    private function initializeDays()
    {
        $zippy = Zippy::instance();

        $output = array();
        $today = $zippy->utilities->datetime->getToday();

        for ($i=0; $i < $this->num_days; $i++) {
            $key = $today->format('m-d-Y');
            $output[$key] = 0;

            $today->modify('-1 day');
        }

        return array_reverse($output);
    }
}
