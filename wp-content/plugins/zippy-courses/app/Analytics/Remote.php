<?php

class ZippyCourses_Remote_Analytics
{
    public function __construct()
    {
        add_action('admin_init', array($this, 'report'));
    }

    public function getEmailListServices()
    {
        global $wpdb;

        $sql = "SELECT * FROM $wpdb->options WHERE option_name LIKE '%_email_integration_settings%'";
        $results = $wpdb->get_results($sql);

        $services = array();

        foreach ($results as $result) {
            $name  = str_replace(array('zippy_', '_email_integration_settings'), '', $result->option_name);
            $value = maybe_unserialize($result->option_value);

            if (is_array($value) && isset($value['enabled']) && $value['enabled']) {
                $services[] = $name;
            }
        }

        return $services;
    }

    public function report()
    {
        if (defined('DOING_AJAX') && DOING_AJAX) {
            return;
        }

        global $wpdb;

        $zippy = Zippy::instance();

        $scheduled = get_option('zippy_report_details_schedule');
        $scheduled = $scheduled ? $scheduled : 0;

        $now = $zippy->utilities->datetime->getNow();

        if ($scheduled < $now->format('U')) {
            $expire = $zippy->utilities->datetime->getNow();
            $expire->modify('+1 day');

            update_option('zippy_report_details_schedule', $expire->format('U'));
            
            $url    = 'https://zippycourses.com/?zippy_log=remote_statistics';
            $data   = $this->getData();

            $response = wp_remote_post($url, array(
                'body' => $data,
                'sslverify' => false
            ));
        }
    }

    public function getData()
    {
        $zippy = Zippy::instance();

        $data = array();

        $license_settings = get_option('zippy_license_settings');
        $license = isset( $license_settings['license_key'] ) ? $license_settings['license_key'] : '';

        $plugins = get_option('active_plugins', array());

        $installed_plugins  = get_plugins();
        $active_plugins     = array();

        $payment_settings = get_option('zippy_payment_general_settings', array());
        $payment_gateway  = isset($payment_settings['method']) ? $payment_settings['method'] : 'paypal';

        foreach ($plugins as $plugin) {
            $active_plugins[$plugin] = array(
                'name'      => $installed_plugins[$plugin]['Name'],
                'version'   => $installed_plugins[$plugin]['Version']
            );
        }

        $data['site_url'] = home_url();
        $data['license'] = $license;
        $data['plugins'] = $plugins;
        $data['num_students'] = $zippy->utilities->student->getCount();
        $data['php_version'] = phpversion();
        $data['payment_gateway'] = $payment_gateway;
        $data['email_list_services'] = $this->getEmailListServices();
        $data['wp_version'] = get_bloginfo('version');
        $data['num_courses'] = $zippy->utilities->course->getCount();
        $data['num_orders'] = $zippy->utilities->orders->getNumberOfOrders();

        return $data;
    }
}
