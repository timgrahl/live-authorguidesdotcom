<?php
class ZippyCourses_StudentViews_DataTable extends Zippy_DataTable
{
    public $dataset;

    public function __construct(Zippy_Dataset $dataset)
    {
        $this->dataset = $dataset;
        $this->filtered = array('duration', 'avg_duration');
    }

    public function filterValue($key, $value)
    {
        if (!in_array($key, $this->filtered)) {
            return $value;
        }

        switch ($key) {
            case 'duration':
                return $this->filterDuration($value);
                break;
            case 'avg_duration':
                return $this->filterAverageDuration($value);
                break;
            default:
                return $value;
                break;
        }

        return $value;
    }

    private function filterDuration($value)
    {
        $zippy = Zippy::instance();
        $seconds = ceil($value / 1000);
        $now = $zippy->utilities->datetime->getNow();
        $diff = clone($now);
        $diff->modify("-$seconds seconds");

        return human_time_diff($now->format('U'), $diff->format('U'));
    }

    private function filterAverageDuration($value)
    {
        return '~' . $this->filterDuration($value);
    }
}
