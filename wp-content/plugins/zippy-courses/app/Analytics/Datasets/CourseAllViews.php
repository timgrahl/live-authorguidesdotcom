<?php
class ZippyCourses_CourseAllViews_Dataset extends Zippy_Dataset
{
    public $course_id;

    public function __construct($course_id)
    {
        $this->course_id = $course_id;

        parent::__construct();
    }

    public function fetchData()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $table = $wpdb->prefix . 'zippy_user_activity';

        $ids = $zippy->utilities->course->getCourseEntryIds($this->course_id);
        $list = implode(',', $ids);

        $sql = "SELECT * FROM $table WHERE item_id in ($list)";

        $results = $wpdb->get_results($sql);
        $views = array();

        foreach ($results as $r) {
            if ($r->type == 'view') {
                $views[] = $r;
            }
        }

        return $views;
    }

    public function parseData(array $data)
    {
        $output = array();

        foreach ($data as $item) {
            if (!isset($output[$item->item_id])) {
                $output[$item->item_id] = array(
                    'ID' => $item->item_id,
                    'title' => get_the_title($item->item_id),
                    'views' => 0,
                    'duration' => 0
                );
            }

            $output[$item->item_id]['views']++;
            $output[$item->item_id]['duration'] += ceil($item->duration);
        }

        foreach ($output as &$item) {
            $item['avg_duration'] = $item['views'] > 0 ? $item['duration'] / $item['views'] : 0;
        }

        return $output;
    }
}
