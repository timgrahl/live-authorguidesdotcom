<?php

class ZippyCourses_StudentDownloads_Dataset extends Zippy_Dataset
{
    public $student_id;

    public function __construct($student_id)
    {
        $this->student_id = $student_id;

        parent::__construct();
    }

    public function fetchData()
    {
        global $wpdb;

        $table = $wpdb->prefix . 'zippy_user_activity';

        $sql = $wpdb->prepare("SELECT * FROM $table WHERE user_id = %d ORDER BY create_time", $this->student_id);

        $results = $wpdb->get_results($sql);
        $views = array();

        foreach ($results as $r) {
            if ($r->type == 'download') {
                $views[] = $r;
            }
        }

        return $views;
    }

    public function parseData(array $data)
    {
        $zippy = Zippy::instance();

        $output = array();

        foreach ($data as $item) {
            if (!isset($output[$item->item_id])) {
                $output[$item->item_id] = array(
                    'ID' => $item->item_id,
                    'title' => $zippy->utilities->entry->getDownloadTitle($item->item_id),
                    'views' => 0                );
            }

            $output[$item->item_id]['views']++;
            $last_viewed = $item->create_time;
            $last_viewed = new DateTime($last_viewed);
            $last_viewed->setTimeZone($zippy->utilities->datetime->getTimezone());
            $output[$item->item_id]['last_viewed'] = $last_viewed->format('m-d-Y H:i:s');
        }


        
        return $output;
    }
}
