<?php

class ZippyCourses_StudentLessonsCompleted_Dataset extends Zippy_Dataset
{
    public $student_id;

    public function __construct($student_id)
    {
        $this->student_id = $student_id;

        parent::__construct();
    }

    public function fetchData()
    {
        $zippy = Zippy::instance();
        $student = $zippy->make('student', array($this->student_id));
        $student->fetch();
        $completed_lessons = $student->getCompleted();

        return $completed_lessons;
    }

    public function parseData(array $data)
    {
        $zippy = Zippy::instance();

        $output = array();

        foreach ($data as $key => $value) {
            $lesson_array = array();
            $course = $zippy->make('course', array($key));

            foreach ($value as $lesson_id) {
                $lesson_array[] = array(
                    'ID' => $lesson_id,
                    'title' => get_the_title($lesson_id)
                    ) ;
            }
            $output[] = array(
                'ID'    => $key,
                'title' => $course->getTitle(),
                'lessons' => $lesson_array
                );
        }
        return $output;
    }
}
