<?php

class ZippyCourses_StudentCompletion_Dataset extends Zippy_Dataset
{
    public $student_id;

    public function __construct($student_id)
    {
        $this->student_id = $student_id;

        parent::__construct();
    }

    public function fetchData()
    {
        global $wpdb;
        $zippy = Zippy::instance();
        $data = json_decode(get_user_meta($this->student_id, 'courses_completed', true));
        return !empty($data) ? $data : array();
    }

    public function parseData(array $data)
    {
        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');
        $output = array();

        foreach ($data as $course_id) {
            $completion_date = get_user_meta($student->getId(), "course_{course_id}_completion_date", true);

            $completion_date = date_i18n('F d, Y', strtotime($completion_date));
            $output[$course_id]['course_id'] = $course_id;
            $output[$course_id]['title'] = get_the_title($course_id);
            $output[$course_id]['completion_date'] = $completion_date;
        }
        
        return $output;
    }
}
