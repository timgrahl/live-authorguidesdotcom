<?php

class ZippyCourses_StudentRecentViews_Dataset extends Zippy_Dataset
{
    public $student_id;

    public function __construct($student_id)
    {
        $this->student_id = $student_id;

        parent::__construct();
    }

    public function fetchData()
    {
        global $wpdb;

        $table = $wpdb->prefix . 'zippy_user_activity';

        $sql = $wpdb->prepare("SELECT * FROM $table WHERE user_id = %d", $this->student_id);

        $results = $wpdb->get_results($sql);
        $views = array();

        foreach ($results as $r) {
            if ($r->type == 'view') {
                $views[] = $r;
            }
        }

        return $views;
    }

    public function parseData(array $data)
    {
        $zippy = Zippy::instance();

        $output = array();

        $minimum = $zippy->utilities->datetime->getToday();
        $minimum->modify('-1 month');

        foreach ($data as $item) {
            $event_date = $zippy->utilities->datetime->getDate($item->create_time);

            if ($event_date->format('U') < $minimum->format('U')) {
                continue;
            }

            if (!isset($output[$item->item_id])) {
                $output[$item->item_id] = array(
                    'ID' => $item->item_id,
                    'title' => get_the_title($item->item_id),
                    'views' => 0,
                    'duration' => 0
                );
            }

            $output[$item->item_id]['views']++;
            $output[$item->item_id]['duration'] += ceil($item->duration);
        }

        foreach ($output as &$item) {
            $item['avg_duration'] = $item['views'] > 0 ? $item['duration'] / $item['views'] : 0;
        }

        return $output;
    }
}
