<?php

class ZippyCourses_Analytics
{
    public function __construct()
    {
        add_action('template_redirect', array($this, 'createView_Event'));
        add_action('init', array($this, 'updateView_Events'));
    }

    /**
     * Create a view event in the database
     *
     * @since  1.0.0
     * @todo   Move the setting of the _last_active to a more appropriate place in the code
     * @return void
     */
    public function createView_Event()
    {
        global $post, $current_user;

        if (!is_admin() && is_object($post) && $current_user->ID) {
            $zippy = Zippy::instance();
            $insert_id = $zippy->activity->insert($current_user->ID, $post->ID, 'view');

            $zippy->cache->set('analytics_id', $insert_id);

            $now = $limit = $zippy->utilities->datetime->getNow();
            update_user_meta($current_user->ID, '_last_active', $now->format('U'));
        }
    }

    public function updateView_Events()
    {
        global $wpdb;
        
        $zippy = Zippy::instance();

        $data = isset($_COOKIE['zippy']) ? $_COOKIE['zippy'] : '';
        $json = json_decode(stripslashes($data), true);

        $activities = isset($json['analytics']) ? $json['analytics'] : array();

        foreach ($activities as $key => $activity) {
            $analytics_id = $activity['ID'];
            $duration = $wpdb->get_var($wpdb->prepare("SELECT duration FROM {$zippy->activity->table} WHERE ID = %s", $analytics_id));

            if ($duration == $activity['duration']) {
                unset($activities[$key]);
            } else {
                $zippy->activity->update($analytics_id, $activity);
            }
        }

        $json['analytics'] = $activities;
        $_COOKIE['zippy'] = json_encode(array());
    }
}
