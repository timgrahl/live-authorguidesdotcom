<?php

class ZippyCourses_QuizResults_Result implements Zippy_RepositoryObject
{
    public $id;

    public function __construct($data)
    {
        $this->id = $data->umeta_id;

        $this->quiz = ZippyCourses_QuizResults_Quiz::build($data->meta_value, false);
        $this->quiz->importResults($data);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getScore()
    {
        $scores = $this->quiz->getScores();
        $score = reset($scores);

        return is_array($score) && isset($score['score']) ? $score['score'] : 0;
    }

    public function getUserId()
    {
        $scores = $this->quiz->getScores();
        $score = reset($scores);

        return is_array($score) && isset($score['user_id']) ? $score['user_id'] : 0;
    }

    public function getDate()
    {
        $zippy = Zippy::instance();
        $scores = $this->quiz->getScores();
        $score = reset($scores);

        return is_array($score) && isset($score['date']) ? $zippy->utilities->datetime->getFormattedDate($score['date']) : 0;
    }

    public function getQuiz()
    {
        return $this->quiz;
    }
}
