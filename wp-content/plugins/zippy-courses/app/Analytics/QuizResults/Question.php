<?php

class ZippyCourses_QuizResults_Question implements Zippy_RepositoryObject
{
    public $id;
    public $question;
    public $answers;
    
    public $correct_response_count = 0;
    public $response_count = 0;

    public function __construct($id)
    {
        $this->id       = $id;
        $this->answers  = new ZippyCourses_QuizResults_Answers;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setQuestion($question)
    {
        $this->question = $question;
        return $this;
    }

    public static function build(stdClass $data, $prepopulate_answers = true)
    {
        $question = new self($data->id);
        $question->question = $data->question;

        if ($prepopulate_answers) {
            $question->buildQuestionAnswers();
        }

        return $question;
    }

    /**
     * Get the current questions that the quiz has, and add them to the quiz
     * @return self
     */
    public function buildQuestionAnswers()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $answers = $zippy->utilities->getJsonMeta($this->getId(), 'answers', true);

        if (is_array($answers)) {
            foreach ($answers as $answer_data) {
                $this->handleResponse($answer_data->content);
            }
        }

        return $this;
    }

    public function handleResponse($response, $increment = false)
    {
        if (($answer = $this->answers->getBy('answer', $response)) === null) {
            $next_id = $this->answers->getNextId();

            $answer = new ZippyCourses_QuizResults_Answer($next_id);
            $answer->setAnswer($response);

            $this->answers->add($answer);
        }

        if ($increment) {
            $answer->incrementCount();
        }

        return $this;
    }

    public function incrementCorrectResponseCount()
    {
        $this->correct_response_count++;
        return $this;
    }

    public function incrementResponseCount()
    {
        $this->response_count++;
        return $this;
    }

    public function export()
    {
        $output = new stdClass;

        $output->id = $this->id;
        $output->correct_response_count = $this->correct_response_count;
        $output->response_count = $this->response_count;
        $output->question = $this->question;
        $output->answers = $this->answers->export();

        return $output;
    }

    public function __clone()
    {
        $this->answers = clone($this->answers);

        foreach ($this->answers->all() as $item) {
            $new_item = clone($item);
            $this->answers->add($new_item);
        }
    }
}