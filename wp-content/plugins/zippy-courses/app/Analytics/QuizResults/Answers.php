<?php

class ZippyCourses_QuizResults_Answers extends Zippy_Repository
{
    public function __construct()
    {
        $this->addValidType('ZippyCourses_QuizResults_Answer');
    }

    public function export()
    {
        return $this->all();
    }
}
