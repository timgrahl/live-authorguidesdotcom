<?php

class ZippyCourses_QuizResults_Answer implements Zippy_RepositoryObject
{
    public $answer;
    public $count = 0;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    public function incrementCount()
    {
        $this->count++;
    }
}
