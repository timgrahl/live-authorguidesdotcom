<?php

class ZippyCourses_QuizResults_Quizzes extends Zippy_Repository
{
    public $get_users;

    public function __construct($get_users = false)
    {
        $this->addValidType('ZippyCourses_QuizResults_Quiz');
    }

    public function add(Zippy_RepositoryObject $item)
    {
        $item->get_users = $this->get_users;

        return parent::add($item);
    }

    public function export()
    {
        $output = array();

        foreach ($this->all() as $quiz) {
            $output[] = $quiz->export();
        }

        return $output;
    }
}
