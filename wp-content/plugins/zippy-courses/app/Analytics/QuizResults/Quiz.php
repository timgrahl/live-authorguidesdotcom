<?php

class ZippyCourses_QuizResults_Quiz implements Zippy_RepositoryObject
{
    public $id;
    public $count = 0;
    public $scores = array();
    public $users = array();
    public $questions = array();
    public $get_users = false;

    public function __construct($id)
    {
        $this->id = $id;
        $this->questions = new ZippyCourses_QuizResults_Questions;
    }

    public static function build($data, $prepopulate_questions = true)
    {
        $quiz = new self($data->quiz);
        $quiz->addQuestions($data->questions, $prepopulate_questions);

        if ($prepopulate_questions) {
            $quiz->buildQuizQuestions();
        }

        return $quiz;
    }

    /**
     * Get the current questions that the quiz has, and add them to the quiz
     * @return self
     */
    public function buildQuizQuestions()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $questions = $zippy->utilities->getJsonMeta($this->getId(), 'questions', true);

        if (is_array($questions)) {
            foreach ($questions as $question_data) {
                $data = new stdClass;
                $data->id = $question_data->ID;
                $data->question = get_the_title($question_data->ID);

                if (($question = $this->questions->getBy('question', $data->question)) === null) {
                    $question = ZippyCourses_QuizResults_Question::build($data);
                    $this->questions->add($question);
                }
            }
        }

        return $this;
    }

    public function incrementCount()
    {
        $this->count++;
    }

    public function addQuestions(array $questions, $prepopulate_answers = true)
    {
        foreach ($questions as $question_data) {
            $question = $this->questions->getBy('question', $question_data->question);

            if ($question === null) {
                $question = ZippyCourses_QuizResults_Question::build($question_data, $prepopulate_answers);
            }

            if ($question !== null) {
                $this->questions->add($question);
            }
        }

        return $this;
    }

    public function addScore($user_id, $result_id, $score, DateTime $date)
    {
        $this->scores[] = array(
            'id'        => $result_id,
            'user_id'   => $user_id,
            'score'     => $score,
            'date'      => $date
        );

        return $this;
    }

    public function addUser($user_id)
    {
        if (!$this->get_users) {
            return;
        }

        global $wpdb;

        if (!array_key_exists($user_id, $this->users)) {
            $data = get_userdata($user_id);

            $user = new stdClass;
            $user->id          = $user_id;
            $user->name        = "{$data->first_name} {$data->last_name}";
            $user->username    = $data->user_login;

            $this->users[$user_id] = $user;
        }

        return $this;
    }

    public function importResults($raw)
    {
        $zippy = Zippy::instance();

        $this->incrementCount();

        $questions  = $raw->meta_value->questions;
        $user_id    = $raw->user_id;
        $result_id  = $raw->umeta_id;
        $score      = $raw->meta_value->score;
        $date       = $zippy->utilities->datetime->getDateFromTimestamp($raw->meta_value->submitted);

        $this->addQuestions($questions);
        $this->addScore($user_id, $result_id, $score, $date);
        $this->addUser($user_id);

        foreach ($questions as $question_data) {
            $question = $this->questions->getBy('question', $question_data->question);

            if ($question !== null) {
                $question->incrementResponseCount();
                
                $question->handleResponse($question_data->answer->given->content, true);
                $question->handleResponse($question_data->answer->correct->content);

                if ($question_data->correct) {
                    $question->incrementCorrectResponseCount();
                }
            }
        }

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function export()
    {
        $output = new stdClass;

        $output->id = $this->id;
        $output->count = $this->count;
        $output->scores = $this->scores;
        $output->users = $this->users;
        $output->questions = $this->questions->export();

        return $output;
    }

    /**
     * Gets the value of scores.
     *
     * @return mixed
     */
    public function getScores()
    {
        return $this->scores;
    }

    /**
     * Sets the value of scores.
     *
     * @param mixed $scores the scores
     *
     * @return self
     */
    public function setScores($scores)
    {
        $this->scores = $scores;

        return $this;
    }

    public function __clone()
    {
        $this->questions = clone($this->questions);

        foreach ($this->questions->all() as $item) {
            $new_item = clone($item);
            $this->questions->add($new_item);
        }
    }
}
