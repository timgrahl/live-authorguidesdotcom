<?php

class ZippyCourses_QuizResults_Questions extends Zippy_Repository
{
    public function __construct()
    {
        $this->addValidType('ZippyCourses_QuizResults_Question');
    }

    public function export()
    {
        $output = array();

        foreach ($this->items as $question) {
            $output[] = $question->export();
        }

        return $output;
    }
}
