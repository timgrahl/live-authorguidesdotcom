<?php

class ZippyCourses_QuizResults_Results extends Zippy_Repository
{
    public $get_users;

    public function __construct($get_users = false)
    {
        $this->addValidType('ZippyCourses_QuizResults_Result');
    }

    public function add(Zippy_RepositoryObject $item)
    {
        $item->get_users = $this->get_users;
        return parent::add($item);
    }

    public function fetchByQuiz($quiz_id)
    {
        $this->fetch($quiz_id);

        return $this;
    }

    public function fetchByStudent($student_id)
    {
        $this->fetch(null, $student_id);

        return $this;
    }

    public function fetch($quiz_id = null, $student_id = null)
    {
        global $wpdb;

        $from  = $this->getFromClause();
        $where = $this->getWhereClause($quiz_id, $student_id);

        $sql   = "SELECT * $from $where";

        $records = $wpdb->get_results($sql);
        
        foreach ($records as $record) {
            $record->meta_value = json_decode($record->meta_value);
            $result = new ZippyCourses_QuizResults_Result($record);
            
            $this->add($result);
        }
    }

    public function getWhereClause($quiz = null, $student = null)
    {
        global $wpdb;

        $clause = $wpdb->prepare("WHERE meta_key = %s", 'quiz_results');

        if ($student !== null) {
            $clause .= $wpdb->prepare(" AND user_id = %d", $student);
        }

        if ($quiz !== null) {
            $clause .= ' AND meta_value LIKE \'{"quiz":'. $quiz . '%\'';
        }

        return $clause;
    }

    public function getOrderClause()
    {
        $orderby = filter_input(INPUT_GET, 'orderby');
        $orderby = $orderby == 'date' ? 'umeta_id' : '';

        $order  = filter_input(INPUT_GET, 'order');
        
        return !empty($orderby) && !empty($order)
            ? "ORDER BY $orderby $order"
            : "ORDER BY umeta_id DESC";
    }

    public function getLimitClause()
    {
        $paged  = (int) filter_input(INPUT_GET, 'paged');
        $paged  = $paged ? $paged : 1;

        $offset = $this->per_page * ($paged - 1);
        
        return "LIMIT $offset,{$this->per_page}";
    }

    public function getFromClause()
    {
        global $wpdb;

        return " FROM $wpdb->usermeta";
    }

    public function summarize()
    {
        $results = $this->all();

        $summary = array();

        foreach ($results as $result) {
            $quiz = $result->quiz;

            if (!isset($summary[$quiz->id])) {
                $summary[$quiz->id] = clone($quiz);
                $summary[$quiz->id]->scores = array();
                $summary[$quiz->id]->count = 0;
                $summary[$quiz->id]->questions->truncate();
                $summary[$quiz->id]->buildQuizQuestions();
            }

            $quiz_summary = $summary[$quiz->id];
            $quiz_summary->count++;

            foreach ($quiz->scores as $score) {
                $quiz_summary->scores[] = $score;
            }

            $question_ids = $quiz->questions->toArray('id');
            foreach ($question_ids as $question_id) {
                
                $quiz_question = $quiz->questions->get($question_id);

                if (($question = $quiz_summary->questions->get($question_id)) === null) {
                    $question = $quiz_question;
                    $question->buildQuestionAnswers();

                    $quiz_summary->questions->add($question);

                } else {
                    $question->response_count          += $quiz_question->response_count;
                    $question->correct_response_count  += $quiz_question->correct_response_count;
                }

                foreach ($quiz_question->answers->all() as $answer) {
                    if ($question->answers->getBy('answer', $answer->answer) === null) {
                        $next_id = $question->answers->getNextId();
                        $answer->id = $next_id;

                        $question->answers->add($answer);
                    } else {
                        $question_answer = $question->answers->getBy('answer', $answer->answer);
                        $question_answer->count += $answer->count;
                    }
                }
            }
        }


        return $summary;
    }

    public function exportSummary()
    {
        $summary = $this->summarize();

        $export = array();

        foreach ($summary as &$quiz) {
            foreach ($quiz->scores as &$score) {
                $score = $score['score'];
            }

            $quiz->questions = $quiz->questions->all();
            foreach ($quiz->questions as &$question) {
                
                $question->answers = $question->answers->all();

                foreach ($question->answers as &$answer) {
                    unset($answer->id);
                }
                
            }

            unset($quiz->users);
        }

        return count($summary) > 1 ? $summary : reset($summary);
    }
}
