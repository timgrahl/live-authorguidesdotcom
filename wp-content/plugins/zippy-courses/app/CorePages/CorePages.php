<?php

class ZippyCourses_CorePages
{
    protected static $instance;
    private $pages;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $defaults = array(
            'dashboard'         => false,
            'login'             => false,
            'logout'            => false,
            'register'          => false,
            'account'           => false,
            'edit_account'      => false,
            'buy'               => false,
            'order_history'     => false,
            'forgot_password'   => false,
            'reset_password'    => false,
            'thank_you'         => false,
            'course_directory'  => false,
        );

        $this->pages = array_merge($defaults, get_option('zippy_core_pages', array()));
    }

    public function getList()
    {
        return array(
            'dashboard'         => array('id' => $this->get('dashboard'), 'title' => __('Dashboard', ZippyCourses::TEXTDOMAIN)),
            'login'             => array('id' => $this->get('login'), 'title' => __('Login', ZippyCourses::TEXTDOMAIN)),
            'logout'            => array('id' => $this->get('logout'), 'title' => __('Logout', ZippyCourses::TEXTDOMAIN)),
            'register'          => array('id' => $this->get('register'), 'title' => __('Register', ZippyCourses::TEXTDOMAIN)),
            'account'           => array('id' => $this->get('account'), 'title' => __('Account', ZippyCourses::TEXTDOMAIN)),
            'edit_account'      => array('id' => $this->get('edit_account'), 'title' => __('Edit Account', ZippyCourses::TEXTDOMAIN)),
            'buy'               => array('id' => $this->get('buy'), 'title' => __('Buy Now', ZippyCourses::TEXTDOMAIN)),
            'order_history'     => array('id' => $this->get('order_history'), 'title' => __('Order History', ZippyCourses::TEXTDOMAIN)),
            'forgot_password'   => array('id' => $this->get('forgot_password'), 'title' => __('Forgot Password', ZippyCourses::TEXTDOMAIN)),
            'reset_password'    => array('id' => $this->get('reset_password'), 'title' => __('Reset Password', ZippyCourses::TEXTDOMAIN)),
            'thank_you'         => array('id' => $this->get('thank_you'), 'title' => __('Thank You', ZippyCourses::TEXTDOMAIN)),
            'course_directory'  => array('id' => $this->get('course_directory'), 'title' => __('Course Directory', ZippyCourses::TEXTDOMAIN))
        );
    }

    public function getIds()
    {
        return array(
            'dashboard'         => array('id' => $this->get('dashboard'), 'title' => __('Dashboard', ZippyCourses::TEXTDOMAIN)),
            'login'             => array('id' => $this->get('login'), 'title' => __('Login', ZippyCourses::TEXTDOMAIN)),
            'logout'            => array('id' => $this->get('logout'), 'title' => __('Logout', ZippyCourses::TEXTDOMAIN)),
            'register'          => array('id' => $this->get('register'), 'title' => __('Register', ZippyCourses::TEXTDOMAIN)),
            'account'           => array('id' => $this->get('account'), 'title' => __('Account', ZippyCourses::TEXTDOMAIN)),
            'edit_account'      => array('id' => $this->get('edit_account'), 'title' => __('Edit Account', ZippyCourses::TEXTDOMAIN)),
            'buy'               => array('id' => $this->get('buy'), 'title' => __('Buy Now', ZippyCourses::TEXTDOMAIN)),
            'order_history'     => array('id' => $this->get('order_history'), 'title' => __('Order History', ZippyCourses::TEXTDOMAIN)),
            'forgot_password'   => array('id' => $this->get('forgot_password'), 'title' => __('Forgot Password', ZippyCourses::TEXTDOMAIN)),
            'reset_password'    => array('id' => $this->get('reset_password'), 'title' => __('Reset Password', ZippyCourses::TEXTDOMAIN)),
            'thank_you'         => array('id' => $this->get('thank_you'), 'title' => __('Thank You', ZippyCourses::TEXTDOMAIN)),
            'course_directory'  => array('id' => $this->get('course_directory'), 'title' => __('Course Directory', ZippyCourses::TEXTDOMAIN))
        );
    }

    /**
     * Wrapper for self::getId
     *
     * @since 1.0.0
     *
     * @param  string   $core_page  Core Page slug
     *
     * @return int      $id         Post ID
     */
    public function get($core_page)
    {
        return $this->getId($core_page);
    }

    /**
     * Get the ID of a particular core page.
     *
     * @since 1.0.0
     *
     * @param  string   $core_page  Core Page slug
     *
     * @return int      $id         Post ID
     */
    public function getId($core_page)
    {
        return isset($this->pages[$core_page]) ? $this->pages[$core_page] : 0;
    }

    public function getPublicOnlyCorePageIds()
    {
        return array(
            'login'             => $this->getId('login'),
            'forgot_password'   => $this->getId('forgot_password'),
            'reset_password'    => $this->getId('reset_password'),
        );
    }

    public function getPrivateOnlyCorePageIds()
    {
        return array(
            'dashboard'         => $this->getId('dashboard'),
            'logout'            => $this->getId('logout'),
            'account'           => $this->getId('account'),
            'edit_account'      => $this->getId('edit_account'),
            'order_history'     => $this->getId('order_history'),
        );
    }

    public function getSlug($key)
    {
        global $wpdb;

        $id = $this->getId($key);

        $sql = $wpdb->prepare("SELECT post_name FROM $wpdb->posts WHERE ID = %s LIMIT 1", $id);

        $slug = $wpdb->get_var($sql);
        $parent_id = wp_get_post_parent_id($id);
        if ($parent_id > 0) {
            $sql = $wpdb->prepare("SELECT post_name FROM $wpdb->posts WHERE ID = %s LIMIT 1", $parent_id);
            $parent_slug = $wpdb->get_var($sql);
            $slug = $parent_slug . '/' . $slug;
        }
        // zdd($slug);
        return $slug;
    }

    /**
     * Get the URL of a Core Page
     *
     * @param  slug     $core_page  Core Page slug
     *
     * @return string   URL of Core Page
     */
    public function getUrl($core_page)
    {
        return isset($this->pages[$core_page]) ? get_permalink($this->pages[$core_page]) : '';
    }

    /**
     * Determine whether a Post ID is a Core Page.  If a core_page slug is provided,
     * determine whether it is a specific core page.
     *
     * @param  int      $id        Post ID
     * @param  string   $core_page Core Page slug
     *
     * @return boolean
     */
    public function is($id, $core_page = '')
    {
        if (empty($core_page)) {
            return in_array($id, $this->pages);
        }

        return isset($this->pages[$core_page]) && $this->pages[$core_page] == $id ? true : false;
    }

    public function getProtectedCorePages()
    {
        $protected = array(
            'dashboard',
            'logout',
            'account',
            'edit_account',
            'order_history',
        );

        $output = array();

        foreach ($protected as $key) {
            $output[$key] = $this->get($key);
        }

        return $output;
    }

    public function getPublicCorePages()
    {
        $public = array(
            'login',
            'register',
            'forgot_password',
            'reset_password',
            'buy',
            'thank_you',
        );

        $output = array();

        foreach ($public as $key) {
            $output[$key] = $this->get($key);
        }

        return $output;
    }
}
