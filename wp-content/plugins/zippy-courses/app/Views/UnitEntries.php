<?php

class ZippyCourses_UnitEntries_View
{
    public $unit;

    public function __construct(Zippy_Entry $unit)
    {
        $this->unit = $unit;
    }

    public function render()
    {
        $output = '';

        $heading = apply_filters('zippy_unit_entries_heading', __('Lessons', ZippyCourses::TEXTDOMAIN));

        // If there are no entries, don't output the heading
        $entries = $this->unit->getEntries()->all();
        if (!empty($entries)) {
            $output .= '<h2>' . $heading . '</h2>';
        }
        
        foreach ($this->unit->getEntries()->all() as $entry) {
            $output .= $this->renderEntry($entry);
        }
        
        return $this->wrap($output);
    }

    private function wrap($input)
    {
        return '<div class="zippy-unit-entries">' . $input . '</div>';
    }

    private function renderEntry(Zippy_Entry $entry)
    {
        $view = new ZippyCourses_Entry_View($entry);
        $output = do_shortcode($view->render());

        return $output;
    }

    private function renderDescription(Zippy_Entry $entry)
    {
        $settings       = get_option('zippy_customizer_unit_options', array());
        $show_excerpt   = isset($settings['show_excerpt']) ? $settings['show_excerpt'] == 1 : true;
        $excerpt        = $entry->getExcerpt();

        return $show_excerpt
            ? '<div class="zippy-lesson-description">' . wpautop($excerpt) . '</div>'
            : '';
    }

    private function renderEntryDateAvailable(Zippy_Entry $entry)
    {
        if ($entry->isAvailable()) {
            return;
        }
        
        $msg  = '<strong>' . __('Available: ', ZippyCourses::TEXTDOMAIN) . '</strong>';
        $msg .= $entry->getAvailabilityMessage();

        return wpautop($msg);
    }

    private function renderEntryTitle(Zippy_Entry $entry)
    {
        return $entry->isAvailable()
            ? $this->renderAvailableEntryTitle($entry)
            : $this->renderUnavailableEntryTitle($entry);
    }

    private function renderAvailableEntryTitle(Zippy_Entry $entry)
    {
        return '<h4 class="zippy-entry-title zippy-course-' . $entry->getPostType() . '-title">' .
            '<a href="' . get_permalink($entry->getId()) . '">' .
                get_the_title($entry->getId()) .
            '</a>' .
        '</h4>';
    }

    private function renderUnavailableEntryTitle(Zippy_Entry $entry)
    {
        return '<h4 class="zippy-entry-title zippy-course-' . $entry->getPostType() . '-title">' .
            get_the_title($entry->getId()) .
        '</h4>';
    }
}
