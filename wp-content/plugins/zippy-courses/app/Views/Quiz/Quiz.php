<?php

class ZippyCourses_Quiz_View
{
    public $quiz;

    public function __construct(Zippy_Quiz $quiz)
    {
        $this->quiz = $quiz;
    }

    public function render()
    {
        $output = '';
       
        return $output;
    }

    public function renderQuestions()
    {
        $output = '';

        foreach ($this->quiz->getQuestions() as $question) {
            $output .= $this->renderQuestion($question);
        }

        return $output;
    }

    public function renderQuestion(Zippy_Question $question)
    {
        $output  = '<div class="zippy-question" data-question="' . $question->getTitle() . '">';
        $output .= '<h4 class="zippy-question-text">' . $question->getText() . '</h4>';
        $output .= '<ul class="zippy-question-answers">';
        foreach ($question->getAnswers() as $key => $answer) {
            $output .= '<li><label><input type="radio" name="zippy-question[' . $question->getId() . ']" value="' . $answer->ID . '" /> <span>' . $answer->content . '</span></label></li>';
            
        }
        $output .= '</ul>';
        $output .= '</div>';

        return $output;
    }

    public function renderQuizNav()
    {
        return '<div class="zippy-quiz-nav">' .
            '<a href="" class="zippy-button zippy-quiz-prev">' .
                '&laquo; ' . __('Previous Question', ZippyCourses::TEXTDOMAIN) .
            '</a>' .

            '<a href="" class="zippy-button zippy-quiz-next">' .
                __('Next Question', ZippyCourses::TEXTDOMAIN) . ' &raquo;' .
            '</a>' .

            '<a href="" class="zippy-button zippy-quiz-review">' .
                __('Review Your Answers', ZippyCourses::TEXTDOMAIN) .
            '</a>' .
        '</div>';
    }

    public function renderAnswers()
    {
        
    }
}

/*

    global $post, $wpcs_student; 

    $quiz_id = get_post_meta( $post->ID, 'lesson_quiz', true );

    if( is_object( $post ) && $post->post_type == 'quiz' )
        $quiz_id = $post->ID;

    $quiz = $quiz_id ? get_post( $quiz_id ) : false;
    $quiz = $quiz ? new Zippy_Apps_Quizzes_Models_Quiz( $quiz ) : false;

    $nonce = wp_create_nonce( 'zippy_quiz_' . $quiz->ID . '_student_' . $wpcs_student->ID );

?>

<?php if( $quiz ) : ?>

<form class="zippy-courses-quiz" method="POST" action="">
    
    <input type="hidden" name="zippy_quiz_id" value="<?php echo $quiz->ID; ?>">
    <input type="hidden" name="zippy_quiz_nonce" value="<?php echo $nonce; ?>">


    <div class="zippy-pre-submission">
        
        <?php $i = 1; ?>
        <?php foreach( $quiz->questions as $question ) : ?>
        
            <div class="zippy-question-response" data-question="<?php echo $question->ID; ?>">
                <h4 class="zippy-question-text"><?php echo  $i . '. ' . $question->post_title; ?></h4>
                <p><strong>You Replied:</strong> <span class="zippy-question-selected-response"></span> <a href="" class="zippy-question-edit" data-question="<?php echo $question->ID; ?>">(Change)</a></p>
            </div>
            <?php $i++; ?>
        <?php endforeach; ?>
    
        <p class="zippy-quiz-submit"><input type="submit" class="zippy-button zippy-button-primary" value="Submit Quiz" /></p>

    </div>

</form>

<?php endif; ?>
*/
