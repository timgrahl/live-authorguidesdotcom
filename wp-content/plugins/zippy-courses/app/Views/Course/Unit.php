<?php

class ZippyCourses_Unit_View
{
    public $unit;

    public function __construct(Zippy_Entry $unit)
    {
        $this->unit = $unit;
    }

    public function render()
    {
        $output = '';

        $classes = array('zippy-entry', 'zippy-unit', 'zippy-course-' . $this->unit->getPostType());
        $classes = implode(' ', $classes);

        $post_type  = $this->unit->getPostType();
        $id         = $this->unit->getId();
        $url        = get_permalink($id);
        $excerpt    = $this->unit->getExcerpt();

        $output .=  '<div class="' . $classes . '">';
        $output .=  $this->renderTitle();

        $output .=  '<div class="zippy-unit-entries">';
        foreach ($this->unit->entries->all() as $subentry) {
            $view = new ZippyCourses_Entry_View($subentry);
            $output .= $view->render();
        }
        $output .=  '</div>';
        $output .= '</div>';

        return apply_filters('zippy_filter_course_entries_unit_html', $output, $this->unit);
    }

    public function renderTitle()
    {
        $output = $this->unit->isAvailable()
            ? $this->renderAvailableTitle()
            : $this->renderUnavailableTitle();

        $output = apply_filters('zippy_filter_course_entries_unit_title_html', $output, $this->unit);
        
        return $output;
    }

    private function renderAvailableTitle()
    {
        return '<h3 class="zippy-unit-title zippy-course-' . $this->unit->getPostType() . '-title">' .
            '<a href="' . get_permalink($this->unit->getId()) . '">' .
                get_the_title($this->unit->getId()) .
            '</a>' .
        '</h4>';
    }

    private function renderUnavailableTitle()
    {
        return '<h3 class="zippy-unit-title zippy-course-' . $this->unit->getPostType() . '-title">' .
            get_the_title($this->unit->getId()) .
        '</h3>';
    }
}
