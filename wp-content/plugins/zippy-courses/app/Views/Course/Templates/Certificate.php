<?php
    /**
     * Template for Zippy Courses Certificates
     * Available Data:
     *   $data = array(
            'styles',
            'certificate_title',
            'student_name',
            'course_name',
            'completion_date',
            'instructor',
            'institution',
            );
     */
    
?>

<?php echo $data['styles'] ?>

<div class="zippy-completion-certificate">
    <p class="zippy-certificate-header">
        <?php echo $data['certificate_title']; ?>
    </p>
    <p class="zippy-certificate-body-text">
        <?php _e('This certifies that', ZippyCourses::TEXTDOMAIN); ?>
    </p>
    <p class="zippy-certificate-name">
        <?php echo $data['student_name']; ?>
    </p>
    <p class="zippy-certificate-body-text">
        <?php _e('completed', ZippyCourses::TEXTDOMAIN); ?>
    </p>
    <p class="zippy-certificate-course-name">
        <?php echo $data['course_name']; ?>
    </p>
    <p class="zippy-certificate-body-text">
        <?php _e('on ', ZippyCourses::TEXTDOMAIN); ?>
    </p>
    <p class="zippy-certificate-body-text">
        <?php echo $data['completion_date']; ?>
    </p>
    <p class="zippy-certificate-footer-signature position-bottom-right">
        <?php echo $data['instructor']; ?>
    </p>
    <p class="zippy-certificate-footer-signature position-bottom-left">
        <?php echo $data['institution']; ?>
    </p>
</div>