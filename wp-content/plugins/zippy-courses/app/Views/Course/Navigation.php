<?php

class ZippyCourses_Course_Navigation_View extends Zippy_View
{
    public $course;
    public $context;

    public function __construct(Zippy_Course $course, $context = 'course')
    {
        $this->course   = $course;
        $this->context  = $context;
    }

    public function render()
    {
        switch ($this->context) {
            case 'course':
                $output = $this->_renderCourseContext();
                break;
            case 'unit':
                $output = $this->_renderUnitContext();
                break;
            default:
                $output = $this->_renderEntryContext();
                break;
        }

        return $output;
    }

    private function _renderCourseContext()
    {
        return $this->_renderEntries($this->course->getEntries());
    }

    private function _renderUnitContext()
    {
        global $post;

        $unit = $this->course->getEntries()->get($post->ID);
        
        return $unit !== null ? $this->_renderEntries($unit->getEntries()) : '';
    }

    private function _renderEntryContext()
    {
        global $post;

        $zippy = Zippy::instance();
        $unit  = $zippy->utilities->entry->getEntryUnit($this->course, $post->ID);

        return $unit !== null ? $this->_renderEntries($unit->getEntries()) : '';
    }

    private function _renderEntries(Zippy_Entries $entries)
    {
        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');
        $middleware = $zippy->middleware->get('student-access');

        $output = '';

        if ($entries->count()) {
            $output .= '<ul class="course-navigation-list">';
            foreach ($entries->all() as $entry) {
                $classes = $student->isCompleted($entry->getId()) ? 'zippy-entry zippy-entry-complete' : 'zippy-entry';
                $output .= '<li class="' . $classes . '">';
                
                $output .= '<p class="zippy-entry-title">';
                if ($middleware->run($entry)) {
                    $output .= '<a href="' . get_permalink($entry->getId()) . '">' . get_the_title($entry->getId()) . '</a>';
                } else {
                    $output .= get_the_title($entry->getId());
                }
                $output .= '</p>';

                if (!$middleware->run($entry)) {
                    $output .= '<p class="zippy-entry-access">' . $entry->getAvailabilityMessage() . '</p>';
                }

                $output .= '</li>';
            }
            $output .= '</ul>';
        }

        return $output;
    }
}
