<?php

class ZippyCourses_Certificate_View extends Zippy_View
{
    public $certificate;

    public function __construct(Zippy_CourseCertificate $certificate)
    {
        $this->certificate = $certificate;
    }

    public function render()
    {
        if (!class_exists('Dompdf')) {
            require_once(ZippyCourses::$path . '/vendor/dompdf/dompdf_config.inc.php');
        }
        $certificate_title = $this->certificate->course->getTitle() . '_certificate.pdf';
        
        $data = array(
            'certificate_title' => $this->certificate->getTitle(),
            'student_name' => $this->certificate->student->getFullName(),
            'course_name'   => wordwrap($this->certificate->course->getTitle(), 40, '<br>'),
            'completion_date' => $this->certificate->getCompletionDate(),
            'instructor'    => wordwrap($this->certificate->getInstructor(), 25, '<br>'),
            'institution' => wordwrap($this->certificate->getInstitution(), 20, '<br>')
            );
        // Due to Limitations in DomPDF, we need to add some special Spans
        // to accomplish our "Small Caps" certificate styles.
        $data = $this->addStringSpans($data);

        $data['styles'] =$this->loadStyles();

        // Get the certificate template HTML
        ob_start();
        require 'Templates/Certificate.php';
        $output = ob_get_clean();
        $dompdf = new Dompdf();
        // echo $output;
        // die();
        $dompdf->load_html($output);
        $dompdf->set_paper('8.5x11', 'landscape');
        $dompdf->render();
        $dompdf->stream($certificate_title);
        return $output;
    }

    protected function addStringSpans($data)
    {
        // First Get each Individual String
        foreach ($data as $data_key => $string) {
            // Then Turn those strings into an array of characters
            $string_array = str_split($string);
            foreach ($string_array as $string_key => $character) {
                // Finally, if the letter is capitalized, add the necessary <span>
                if (ctype_upper($character)) {
                        $string_array[$string_key] ='<span class="certificate-uppercase-letter">' . $character  . '</span>';
                }
            }

            // Replace the String in the Data Array
            $string = implode('', $string_array);
            $data[$data_key] = $string;
        }

        return $data;
    }

    protected function loadStyles()
    {

        $output = '<style>';
        $output .= $this->renderBorderStyles();
        ob_start();
        require(ZippyCourses::$path . 'assets/css/certificate.css');
        $output .= ob_get_clean();
        $output .= '</style>';
        return $output;
    }

    protected function renderBorderStyles()
    {
        $path = parse_url(ZippyCourses::$url, PHP_URL_PATH);
        $url = '.' . $path . 'assets/images/certificate-border-1.jpg';
        $style = '.zippy-completion-certificate {';
        $style .= 'background-image: url(' . $url . ');';
        $style .= '}';

        return $style;
    }
}
