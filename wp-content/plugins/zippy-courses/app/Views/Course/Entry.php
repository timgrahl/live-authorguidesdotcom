<?php

class ZippyCourses_Entry_View
{
    public $entry;

    public function __construct(Zippy_Entry $entry)
    {
        $this->entry = $entry;
    }

    public function render()
    {
        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');

        $output = '';
        
        $post_type  = $this->entry->getPostType();

        $classes = array('zippy-entry', 'zippy-item', 'zippy-course-' . $post_type);
        if ($student !== null && $student->isCompleted($this->entry->getId())) {
            $classes[] = 'zippy-entry-complete';
        }

        $classes = implode(' ', $classes);

        $output .=  '<div class="' . $classes . '">';
            $output .= $this->renderTitle();
            $output .= $this->renderEntryDateAvailable();
            $output .= $this->renderDescription();
        $output .= '</div>';

        return apply_filters('zippy_filter_course_entry_html', $output, $this->entry);
    }

    private function renderDescription()
    {
        $settings       = get_option('zippy_customizer_course_options', array());
        $show_excerpt   = isset($settings['show_excerpt']) ? $settings['show_excerpt'] == 1 : true;
        $excerpt        = $this->entry->getExcerpt();

        return $show_excerpt
            ? '<div class="zippy-lesson-description">' . wpautop($excerpt) . '</div>'
            : '';
    }

    private function renderEntryDateAvailable()
    {
        if ($this->entry->isAvailable()) {
            return;
        }
        
        return '<div class="zippy-entry-availability">' . wpautop($this->entry->getAvailabilityMessage()) . '</div>';
    }

    private function renderTitle()
    {
        return $this->entry->isAvailable()
            ? $this->renderAvailableTitle()
            : $this->renderUnavailableTitle();
    }

    private function renderAvailableTitle()
    {
        return '<h4 class="zippy-entry-title zippy-course-' . $this->entry->getPostType() . '-title">' .
            '<a href="' . get_permalink($this->entry->getId()) . '">' .
                get_the_title($this->entry->getId()) .
            '</a>' .
        '</h4>';
    }

    private function renderUnavailableTitle()
    {
        return '<h4 class="zippy-entry-title zippy-course-' . $this->entry->getPostType() . '-title">' .
            get_the_title($this->entry->getId()) .
        '</h4>';
    }
}
