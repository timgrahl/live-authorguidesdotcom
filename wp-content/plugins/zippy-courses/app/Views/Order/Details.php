<?php

class ZippyCourses_OrderDetails_View extends Zippy_View
{
    public $model;

    public function __construct(Zippy_Order $order)
    {
        $this->model = $order;
    }

    public function render()
    {
        $output = '<table class="zippy-table zippy-order-details-table">';
        $output .= $this->renderTableHeader();
        $output .= $this->renderTableBody();
        $output .= '</table>';

        return $output;
    }

    public function renderTableHeader()
    {
        $output = '<thead>';
            $output .= '<tr>';
                $output .= '<th>' . __('Customer', ZippyCourses::TEXTDOMAIN) . '</th>';
                $output .= '<th>' . __('Product', ZippyCourses::TEXTDOMAIN) . '</th>';
                $output .= '<th>' . __('Method', ZippyCourses::TEXTDOMAIN) . '</th>';
            $output .= '</tr>';
        $output .= '</thead>';

        return $output;
    }

    public function renderTableBody()
    {
        $output = '<tr>';
            $output .= '<td>' . $this->_renderUsersList() . '</td>';
            $output .= '<td>' . $this->_renderProduct() . '</td>';
            $output .= '<td>' . $this->_renderGateway() . '</td>';
        $output .= '</tr>';

        return $output;
    }

    private function _renderUsersList()
    {
        global $post;

        $zippy = Zippy::instance();

        $users = $zippy->utilities->user->getOwnerList();

        $output  = '<select>';
            $output .= '<option value="-1">' . __('Unclaimed', ZippyCourses::TEXTDOMAIN). '</option>';
        foreach ($users as $user_id => $name) {
            $output .= '<option value="' . $user_id . '" ' . selected($post->post_author, $user_id, false) . '>' . $name . '</option>';
        }
        $output .= '</select>';

        return $output;
    }

    private function _renderProduct()
    {
        $product_url = get_edit_post_link($this->model->getProduct()->getId());

        return '<a href="' . $product_url . '">' . $this->model->getProduct()->getTitle() . '</a>';
    }

    private function _renderGateway()
    {
        return ucwords(str_replace(array('-', '_'), ' ', $this->model->getGateway()));
    }
}
