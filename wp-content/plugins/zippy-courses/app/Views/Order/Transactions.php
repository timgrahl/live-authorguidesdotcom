<?php

class ZippyCourses_OrderTransactions_View extends Zippy_View
{
    public $model;

    public function __construct(Zippy_Order $order)
    {
        $this->model = $order;
    }

    public function render()
    {
        $transactions = $this->model->transactions;

        $output = '<table class="zippy-table zippy-order-transactions-table">';
        $output .= $this->renderTableHeader();
        $output .= $this->renderTableBody();
        $output .= '</table>';

        return $output;
    }

    public function renderTableHeader()
    {
        $output = '<thead>';
            $output .= '<tr>';
                $output .= '<th>' . __('Transaction ID', ZippyCourses::TEXTDOMAIN) . '</th>';
                $output .= '<th>' . __('Vendor ID', ZippyCourses::TEXTDOMAIN) . '</th>';
                $output .= '<th>' . __('Type', ZippyCourses::TEXTDOMAIN) . '</th>';
                $output .= '<th>' . __('Total', ZippyCourses::TEXTDOMAIN) . '</th>';
                $output .= '<th>' . __('Status', ZippyCourses::TEXTDOMAIN) . '</th>';
            $output .= '</tr>';
        $output .= '</thead>';

        return $output;
    }

    public function renderTableBody()
    {
        $output = '<tbody>';
        foreach ($this->model->transactions->all() as $transaction) {
            $view = new ZippyCourses_OrderTransaction_View($transaction);
            $output .= $view->render();
        }
        $output .= '</tbody>';

        return $output;
    }
    public function renderTitle()
    {
        $edit_url = get_edit_post_link($this->model->getId());
        return '<a href="' . $edit_url . '">' . $this->model->getTitle() . '</a>';
    }
}
