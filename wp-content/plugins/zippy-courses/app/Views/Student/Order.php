<?php

class ZippyCourses_StudentOrder_View extends Zippy_View
{
    public $model;

    public function __construct(Zippy_Order $order)
    {
        $this->model = $order;
    }

    public function render()
    {
        $output = '<tr>';
            $output .= '<td>' . $this->renderTitle() . '</td>';
            $output .= '<td>' . $this->renderType() . '</td>';
            $output .= '<td>' . $this->renderTotal() . '</td>';
            $output .= '<td>' . ucfirst($this->model->getStatus()) . '</td>';
        $output .= '</tr>';

        return $output;
    }

    public function renderTotal()
    {
        $zippy = Zippy::instance();

        $symbol = $zippy->currencies->getSymbol($this->model->getCurrency());

        return $symbol . number_format_i18n($this->model->getTotal(), 2);
    }

    public function renderTitle()
    {
        $edit_url = get_edit_post_link($this->model->getId());
        return '<a href="' . $edit_url . '">' . $this->model->getTitle() . '</a>';
    }

    public function renderType()
    {
        $type = __('Single Payment', ZippyCourses::TEXTDOMAIN);

        switch ($this->model->getType()) {
            case 'payment-plan':
                $type = __('Payment Plan Payment', ZippyCourses::TEXTDOMAIN);
                break;
            case 'subscription':
                $type = __('Subscription Payment', ZippyCourses::TEXTDOMAIN);
                break;
            case 'free':
                $type = __('Free', ZippyCourses::TEXTDOMAIN);
                break;
            case 'single':
            default:
                break;
        }

        return $type;
    }
}
