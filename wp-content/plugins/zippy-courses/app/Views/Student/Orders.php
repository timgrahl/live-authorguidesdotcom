<?php

class ZippyCourses_StudentOrders_View extends Zippy_View
{
    public $model;

    public function __construct(ZippyCourses_Student $student)
    {
        $this->model = $student;
    }

    public function render()
    {
        $output = '<table class="zippy-table zippy-student-odrers-table">';
        $output .= $this->renderTableHeader();
        $output .= $this->renderTableBody();
        $output .= '</table>';

        return $output;
    }

    public function renderTableHeader()
    {
        $output = '<thead>';
            $output .= '<tr>';
                $output .= '<th>' . __('Order ID', ZippyCourses::TEXTDOMAIN) . '</th>';
                $output .= '<th>' . __('Type', ZippyCourses::TEXTDOMAIN) . '</th>';
                $output .= '<th>' . __('Total', ZippyCourses::TEXTDOMAIN) . '</th>';
                $output .= '<th>' . __('Status', ZippyCourses::TEXTDOMAIN) . '</th>';
            $output .= '</tr>';
        $output .= '</thead>';

        return $output;
    }

    public function renderTableBody()
    {
        $orders = $this->model->getOrders();

        $output = '<tbody>';
        foreach ($orders->all() as $order) {
            $view = new ZippyCourses_StudentOrder_View($order);
            $output .= $view->render();
        }
        $output .= '</tbody>';

        return $output;
    }
}
