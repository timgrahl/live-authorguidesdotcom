<?php

class ZippyCourses_CourseEntries_View
{
    public $course;

    public function __construct(Zippy_Course $course)
    {
        $this->course = $course;
    }

    public function render()
    {
        $output = '';

        if ($this->course->entries->count()) {
            $heading = apply_filters('zippy_course_entries_heading', __('Units & Lessons', ZippyCourses::TEXTDOMAIN));
            $output .= '<h2>' . $heading . '</h2>';

            foreach ($this->course->getEntries()->all() as $entry) {
                $output .= $entry->getType() == 'item'
                    ? $this->renderEntry($entry)
                    : $this->renderUnit($entry);
            }
            
            return $this->wrap($output);
        }
        
        return $output;
    }

    private function wrap($input)
    {
        return '<div class="zippy-course-entries">' . $input . '</div>';
    }

    private function renderEntry(Zippy_Entry $entry)
    {
        $view = new ZippyCourses_Entry_View($entry);
        $output = do_shortcode($view->render());

        return $output;
    }

    private function renderUnit(Zippy_Entry $entry)
    {
        $view = new ZippyCourses_Unit_View($entry);
        return do_shortcode($view->render());
    }

    private function renderEntryDateAvailable(Zippy_Entry $entry)
    {
        if ($entry->isAvailable()) {
            return;
        }
        
        return '<div class="zippy-entry-availability">' . wpautop($entry->getAvailabilityMessage()) . '</div>';
    }

    private function renderEntryTitle(Zippy_Entry $entry)
    {
        return $entry->isAvailable()
            ? $this->renderAvailableEntryTitle($entry)
            : $this->renderUnavailableEntryTitle($entry);
    }

    private function renderAvailableEntryTitle(Zippy_Entry $entry)
    {
        return '<h4 class="zippy-entry-title zippy-course-' . $entry->getPostType() . '-title">' .
            '<a href="' . get_permalink($entry->getId()) . '">' .
                get_the_title($entry->getId()) .
            '</a>' .
        '</h4>';
    }

    private function renderUnavailableEntryTitle(Zippy_Entry $entry)
    {
        return '<h4 class="zippy-entry-title zippy-course-' . $entry->getPostType() . '-title">' .
            get_the_title($entry->getId()) .
        '</h4>';
    }

    private function renderUnitTitle(Zippy_Entry $unit)
    {
        return $unit->isAvailable()
            ? $this->renderAvailableUnitTitle($unit)
            : $this->renderUnavailableUnitTitle($unit);
    }

    private function renderAvailableUnitTitle(Zippy_Entry $unit)
    {
        return '<h3 class="zippy-unit-title zippy-course-' . $unit->getPostType() . '-title">' .
            '<a href="' . get_permalink($unit->getId()) . '">' .
                get_the_title($unit->getId()) .
            '</a>' .
        '</h4>';
    }

    private function renderUnavailableUnitTitle(Zippy_Entry $unit)
    {
        return '<h3 class="zippy-unit-title zippy-course-' . $unit->getPostType() . '-title">' .
            get_the_title($unit->getId()) .
        '</h3>';
    }
}
