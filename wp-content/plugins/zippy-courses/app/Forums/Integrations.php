<?php

class ZippyCourses_ForumIntegrations
{
    public function __construct()
    {
        $this->prepare();
    }

    public function prepare()
    {
        $zippy = Zippy::instance();

        $files = $zippy->utilities->file->getIntegrationFiles(
            'forum-integration',
            'ForumIntegration',
            ZippyCourses::$path . 'app/Integrations/Forums/'
        );

        foreach ($files as $file) {
            $item = $zippy->makeFromFile($file);
        }
    }
}
