<?php

class ZippyCourses_PluginUpdater
{
    public $author;
    public $store_url;
    public $item_name;
    public $plugin_file;
    public $license_key;

    public function __construct()
    {
        $this->author       = 'Zippy Courses';
        $this->license_key  = $this->_setupLicenseKey();
        $this->store_url    = 'https://zippycourses.com';
        $this->item_name    = 'Zippy Courses Plugin';
        $this->plugin_file  = ZippyCourses::$path . 'zippy-courses.php';

        $this->hooks();
    }

    public function hooks()
    {
        add_action('plugins_loaded', array($this, 'update'), 11);
    }

    private function _setupLicenseKey()
    {
        // retrieve our license key from the DB
        $license_settings  = get_option('zippy_license_settings', array('license_key' => ''));
        $this->license_key = isset($license_settings['license_key']) ? trim($license_settings['license_key']) : '';
    }

    public function update()
    {
        // setup the updater
        $edd_updater = new EDD_SL_Plugin_Updater(
            $this->getStoreUrl(),
            $this->getPluginFile(),
            array(
                'version'   => ZippyCourses::VERSION,
                'license'   => $this->getLicenseKey(),
                'item_name' => $this->getItemName(),
                'author'    => $this->getAuthor(),
                'url'       => home_url()
            )
        );
    }

    public function getLicenseKey()
    {
        if ($this->license_key === null) {
            $this->_setupLicenseKey();
        }

        return $this->license_key;
    }

    public function getStoreUrl()
    {
        return $this->store_url;
    }

    public function getItemName()
    {
        return $this->item_name;
    }

    public function getPluginFile()
    {
        return $this->plugin_file;
    }

    public function getAuthor()
    {
        return $this->author;
    }
}
