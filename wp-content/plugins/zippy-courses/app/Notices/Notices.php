<?php

class ZippyCourses_Alerts
{
    /**
     * Instance of self
     * @var self
     */
    private static $instance;

    private $data = array();

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_filter('the_content', array($this, 'show'), 11);
    }

    public function show($content)
    {
        $zippy = Zippy::instance();

        $list = $zippy->sessions->fetchMessages();

        $notices = '';
        foreach ($list as $type => $messages) {
            foreach ($messages as $message) {
                $notices .= '<div class="zippy-alert zippy-alert-' . $type . '">';
                    $notices .= $message;
                $notices .= '</div>';
            }
        }

        return $notices . $content;
    }
}
