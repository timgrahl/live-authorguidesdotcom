<?php

class ZippyCourses_PostTypeTabs extends Zippy_Tabs
{
    public function __construct()
    {
        $this->addValidType('Zippy_PostTypeTab');

        $this->defaultTabs();

        add_action('edit_form_top', array($this, 'renderPostTypeTabs'));
    }

    public function defaultTabs()
    {
        $this->addCourseTabs();
        $this->addProductTabs();
        $this->addQuizTabs();
    }

    private function addCourseTabs()
    {
        $zippy = Zippy::instance();

        $tab = $zippy->make('post_type_tab', array('id' => 'course-details', 'title' => __('Course Details', ZippyCourses::TEXTDOMAIN), 'selectors' => array('#post-body-content', '#postbox-container-1', '#postbox-container-2'), 'post_types' => array('course')));
        $this->add($tab);

        $tab = $zippy->make('post_type_tab', array('id' => 'course-entries', 'title' =>  __('Units & Lessons', ZippyCourses::TEXTDOMAIN), 'selectors' => array('#zippy_course_entries_region'), 'post_types' => array('course')));
        $this->add($tab);

        $tab = $zippy->make('post_type_tab', array('id' => 'course-public-details', 'title' =>  __('Public Details', ZippyCourses::TEXTDOMAIN), 'selectors' => array('#zippy_course_public_details_region'), 'post_types' => array('course')));
        $this->add($tab);

        $tab = $zippy->make('post_type_tab', array('id' => 'course-additional-content', 'title' =>  __('Additional Content', ZippyCourses::TEXTDOMAIN), 'selectors' => array('#zippy_course_additional_content_region'), 'post_types' => array('course')));
        $this->add($tab);

        $show_course_email_list_tab = apply_filters('zippy_enable_course_email_list_tab', false);

        if ($show_course_email_list_tab) {
            $tab = $zippy->make('post_type_tab', array('id' => 'course-email-lists', 'title' => __('Email Lists', ZippyCourses::TEXTDOMAIN), 'selectors' => array('#zippy_course_email_lists_region'), 'post_types' => array('course')));
            $this->add($tab);
        }
    }

    private function addProductTabs()
    {
        $zippy = Zippy::instance();

        $show_product_email_list_tab = apply_filters('zippy_enable_product_email_list_tab', false);
        
        $tab = $zippy->make('post_type_tab', array('id' => 'product-details', 'title' => 'Product Details', 'selectors' => array('#post-body-content', '#postbox-container-1', '#postbox-container-2'), 'post_types' => array('product')));
        $this->add($tab);

        $tab = $zippy->make('post_type_tab', array('id' => 'product-launch-windows', 'title' => 'Launch Windows', 'selectors' => array('#zippy_product_launch_windows_region'),'post_types' =>  array('product')));
        $this->add($tab);

        $tab = $zippy->make('post_type_tab', array('id' => 'product-advanced', 'title' => 'Advanced', 'selectors' => array('#zippy_product_advanced_region'), 'post_types' => array('product')));
        $this->add($tab);

        $tab = $zippy->make('post_type_tab', array('id' => 'product-confirmation-messages', 'title' => 'Confirmation Messages', 'selectors' => array('#zippy_product_confirmation_messages_region'), 'post_types' => array('product')));
        $this->add($tab);

        if ($show_product_email_list_tab) {
            $tab = $zippy->make('post_type_tab', array('id' => 'product-email-lists', 'title' => 'Email Lists','selectors' =>  array('#zippy_product_email_lists_region'), 'post_types' => array('product')));
            $this->add($tab);
        }
    }

    private function addQuizTabs()
    {
        $zippy = Zippy::instance();
        
        $tab = $zippy->make('post_type_tab', array('id' => 'quiz-details', 'title' => 'Details', 'selectors' => array('#post-body-content', '#postbox-container-1', '#postbox-container-2'), 'post_types' => array('quiz')));
        $this->add($tab);

        $tab = $zippy->make('post_type_tab', array('id' => 'quiz-results', 'title' => __('Results', ZippyCourses::TEXTDOMAIN), 'selectors' => array('#zippy_quiz_results_region'), 'post_types' => array('quiz')));
        $this->add($tab);
    }


    public function renderPostTypeTabs()
    {
        global $post;

        if ($this->postTypeHasTabs()) {
            echo '<div class="zippy-ux-tabs zippy-post-type-tabs"><ul>';
            $i = 0;
            $active_tab = filter_input(INPUT_GET, 'zippy-tab');
            $existing_tab = $this->where('id', $active_tab);
            $tab_exists = $active_tab ? !empty($existing_tab) : false;

            foreach ($this->all() as $tab) {
                if (is_object($post) && in_array($post->post_type, $tab->getPostTypes()) && $tab instanceof Zippy_PostTypeTab) {
                    $is_active = $tab_exists
                        ? $tab->getId() == $active_tab
                        : $i < 1;

                    if ($is_active) {
                        $tab->render(true);
                        $i++;
                    } else {
                        $tab->render();
                    }
                }
            }
            echo '</ul></div>';
        }
    }

    private function postTypeHasTabs()
    {
        global $post;

        $has_tabs = false;

        foreach ($this->all() as $tab) {
            if (is_object($post) && in_array($post->post_type, $tab->getPostTypes()) && $tab instanceof Zippy_PostTypeTab) {
                $has_tabs = true;
            }
        }

        return $has_tabs;
    }
}
