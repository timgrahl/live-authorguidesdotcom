<?php

class ZippyCourses_OrderStudent_ListTableFilter extends Zippy_ListTableFilter
{
    public function render()
    {
        $val = filter_input(INPUT_GET, '_filter_' . $this->getId());

        $output = '<select name="_filter_' . $this->getId() . '" class="zippy-list-table-filter zippy-' . $this->getId() . '-list-table-filter">';
        $output .= '<option value="">' . $this->getLabel() . '</option>';
        $output .= '<option value="0">' . __('Unclaimed', ZippyCourses::TEXTDOMAIN) . '</option>';

        foreach ($this->options as $key => $value) {
            $output .= '<option value="' . $key . '" ' . selected($val, $key, false) . '>' . $value . '</option>';
        }
        $output .= '</select>';

        return $output;
    }

    protected function _defaultOptions()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $students = $wpdb->get_results("SELECT ID, user_email, display_name FROM $wpdb->users ORDER BY display_name");
        $options = array();

        foreach ($students as $student) {
            $options[$student->ID] = $student->display_name . ' (' . $student->user_email . ')';
        }

        $this->options = $options;
    }

    public function filterClauses($clauses)
    {
        global $wpdb;

        $student = filter_input(INPUT_GET, '_filter_student');
        
        if (!$student && $student !== '0') {
            return $clauses;
        }

        $where = $wpdb->prepare(
            " AND $wpdb->posts.post_author = %s ",
            $student
        );

        $clauses['where'] .= $where;

        return $clauses;
    }
}
