<?php

class ZippyCourses_Joined_ListTableFilter extends Zippy_ListTableFilter
{
    public function render()
    {
        $val = filter_input(INPUT_GET, $this->getId());

        $output = '<select name="' . $this->getId() . '" class="zippy-list-table-filter zippy-students-list-table-filter">';
        $output .= '<option value="">' . $this->getLabel() . '</option>';

        foreach ($this->options as $key => $value) {
            $output .= '<option value="' . $key . '" ' . selected($val, $key, false) . '>' . $value . '</option>';
        }
        $output .= '</select>';

        return $output;
    }

    protected function _defaultOptions()
    {
        $this->options = array(
            'today' => __('Today', ZippyCourses::TEXTDOMAIN),
            'week' => __('This Week', ZippyCourses::TEXTDOMAIN),
            'month' => __('This Month', ZippyCourses::TEXTDOMAIN),
            'quarter' => __('This Quarter', ZippyCourses::TEXTDOMAIN),
            'biannual' => __('This Half Year', ZippyCourses::TEXTDOMAIN),
            'year' => __('This Year', ZippyCourses::TEXTDOMAIN),
            'years' => '&gt; ' . __('1 Year Ago', ZippyCourses::TEXTDOMAIN),
        );
    }

    public function filter($student)
    {
        $valid = false;

        $joined = filter_input(INPUT_GET, $this->getId());

        if (empty($joined)) {
            return true;
        }

        if (!($student instanceof ZippyCourses_Student) || !method_exists($student, 'getJoined')) {
            return $valid;
        }

        $zippy = Zippy::instance();
        
        switch ($joined) {
            case 'years':
            case 'year':
                $limit = $zippy->utilities->datetime->getToday();
                $limit->modify('-1 year');
                break;
            case 'biannual':
                $limit = $zippy->utilities->datetime->getToday();
                $limit->modify('-6 months');
                break;
            case 'quarter':
                $limit = $zippy->utilities->datetime->getToday();
                $limit->modify('-3 months');
                break;
            case 'month':
                $limit = $zippy->utilities->datetime->getToday();
                $limit->modify('-30 days');
                break;
            case 'week':
                $limit = $zippy->utilities->datetime->getToday();
                $limit->modify('-1 week');
                break;
            case 'today':
            default:
                $limit = $zippy->utilities->datetime->getToday();
                break;
        }

        $valid = $joined == 'years'
            ? $student->getJoined('U') < $limit->format('U')
            : $student->getJoined('U') > $limit->format('U');

        return $valid;
    }
}
