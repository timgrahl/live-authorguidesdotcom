<?php

class ZippyCourses_QuizResultsQuiz_ListTableFilter extends Zippy_ListTableFilter
{
    public function render()
    {
        $val = filter_input(INPUT_GET, $this->getId());

        $output = '<select name="' . $this->getId() . '" class="zippy-list-table-filter zippy-' . $this->getId() . '-list-table-filter">';
        $output .= '<option value="">' . $this->getLabel() . '</option>';

        foreach ($this->options as $key => $value) {
            $output .= '<option value="' . $key . '" ' . selected($val, $key, false) . '>' . $value . '</option>';
        }
        $output .= '</select>';

        return $output;
    }

    protected function _defaultOptions()
    {
        $zippy = Zippy::instance();

        $quiz_ids = $zippy->utilities->quiz->getAllQuizIds();
        $quizzes = array();

        foreach ($quiz_ids as $quiz_id) {
            $quizzes[$quiz_id] = get_the_title($quiz_id);
        }

        $this->options = $quizzes;
    }

    public function filter($quiz)
    {
    }
}
