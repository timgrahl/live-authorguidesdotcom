<?php

class ZippyCourses_OrderProduct_ListTableFilter extends Zippy_ListTableFilter
{
    public function render()
    {
        $val = filter_input(INPUT_GET, '_filter_' . $this->getId());

        $output = '<select name="_filter_' . $this->getId() . '" class="zippy-list-table-filter zippy-' . $this->getId() . '-list-table-filter">';
        $output .= '<option value="">' . $this->getLabel() . '</option>';

        foreach ($this->options as $key => $value) {
            $output .= '<option value="' . $key . '" ' . selected($val, $key, false) . '>' . $value . '</option>';
        }
        $output .= '</select>';

        return $output;
    }

    protected function _defaultOptions()
    {
        $zippy = Zippy::instance();

        $product_ids = $zippy->utilities->product->getAllProductIds();
        $options = array();

        foreach ($product_ids as $product_id) {
            $options[$product_id] = get_the_title($product_id);
        }

        $this->options = $options;
    }

    public function filterClauses($clauses)
    {
        global $wpdb;

        $product = filter_input(INPUT_GET, '_filter_product');
        
        if (!$product) {
            return $clauses;
        }

        if (strpos($clauses['join'], "INNER JOIN {$wpdb->postmeta}") === false) {
            // zpm1 = Zippy Post Meta 1
            $clauses['join'] .= " INNER JOIN $wpdb->postmeta AS zpm1 ON ( $wpdb->posts.ID = zpm1.post_id ) ";
        }

        if (empty($clauses['groupby'])) {
            $clauses['groupby'] = " $wpdb->posts.ID ";
        }

        $where = $wpdb->prepare(
            " AND (
                zpm1.meta_key = %s AND
                zpm1.meta_value LIKE %s
            ) ",
            'product',
            $product
        );

        $clauses['where'] .= $where;

        return $clauses;
    }
}
