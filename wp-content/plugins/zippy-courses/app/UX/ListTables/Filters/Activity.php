<?php

class ZippyCourses_Activity_ListTableFilter extends Zippy_ListTableFilter
{
    public function render()
    {
        $active = filter_input(INPUT_GET, 'active_since');

        $output = '<select name="' . $this->getId() . '" class="zippy-list-table-filter zippy-students-list-table-filter">';
        $output .= '<option value="">' . $this->getLabel() . '</option>';

        foreach ($this->options as $key => $value) {
            $output .= '<option value="' . $key . '" ' . selected($active, $key, false) . '>' . $value . '</option>';
        }
        $output .= '</select>';

        return $output;
    }

    protected function _defaultOptions()
    {
        $this->options = array(
            'today' => __('Today', ZippyCourses::TEXTDOMAIN),
            'week' => __('This Week', ZippyCourses::TEXTDOMAIN),
            'month' => __('This Month', ZippyCourses::TEXTDOMAIN),
            'quarter' => __('This Quarter', ZippyCourses::TEXTDOMAIN),
            'biannual' => __('This Half Year', ZippyCourses::TEXTDOMAIN),
            'year' => __('This Year', ZippyCourses::TEXTDOMAIN),
            'years' => '&gt; ' . __('1 Year Ago', ZippyCourses::TEXTDOMAIN),
            'never' => __('Never', ZippyCourses::TEXTDOMAIN),
        );
    }

    public function filter($student)
    {
        $active = filter_input(INPUT_GET, $this->getId());

        if (empty($active)) {
            return true;
        }
        
        $valid = false;

        if (!($student instanceof ZippyCourses_Student) || !method_exists($student, 'getJoined')) {
            return $valid;
        }
        
        $zippy = Zippy::instance();
        
        switch ($active) {
            case 'years':
            case 'year':
                $limit = $zippy->utilities->datetime->getToday();
                $limit->modify('-1 year');
                break;
            case 'biannual':
                $limit = $zippy->utilities->datetime->getToday();
                $limit->modify('-6 months');
                break;
            case 'quarter':
                $limit = $zippy->utilities->datetime->getToday();
                $limit->modify('-3 months');
                break;
            case 'month':
                $limit = $zippy->utilities->datetime->getToday();
                $limit->modify('-30 days');
                break;
            case 'week':
                $limit = $zippy->utilities->datetime->getToday();
                $limit->modify('-1 week');
                break;
            case 'today':
                $limit = $zippy->utilities->datetime->getToday();
                break;
            case 'never':
                break;
            default:
                $limit = $zippy->utilities->datetime->getToday();
                break;
        }

        if ($active == 'never') {
            $valid = $student->getLastActive() === null;
        } else {
            $valid = $active == 'years'
                ? $student->getLastActive('U') <= $limit->format('U')
                : $student->getLastActive('U') >= $limit->format('U');
        }

        return $valid;
    }
}
