<?php

class ZippyCourses_QuizResultsStudent_ListTableFilter extends Zippy_ListTableFilter
{
    public function render()
    {
        $val = filter_input(INPUT_GET, $this->getId());

        $output = '<select name="' . $this->getId() . '" class="zippy-list-table-filter zippy-' . $this->getId() . '-list-table-filter">';
        $output .= '<option value="">' . $this->getLabel() . '</option>';

        foreach ($this->options as $key => $value) {
            $output .= '<option value="' . $key . '" ' . selected($val, $key, false) . '>' . $value . '</option>';
        }
        $output .= '</select>';

        return $output;
    }

    protected function _defaultOptions()
    {
        $zippy = Zippy::instance();

        $student_ids = $zippy->utilities->student->getStudentIds();
        $students = array();

        foreach ($student_ids as $student_id) {
            $student_data = get_userdata($student_id);

            if (is_object($student_data)) {
                $first_name = isset($student_data->first_name) ? $student_data->first_name : '';
                $last_name  = isset($student_data->last_name) ? $student_data->last_name : '';
                $username      = isset($student_data->user_login) ? $student_data->user_login: '';

                $students[$student_id] = "{$first_name} {$last_name} ({$username})";
            }
        }

        $this->options = $students;
    }

    public function filter($student)
    {
    }
}
