<?php

class ZippyCourses_StudentCourse_ListTableFilter extends Zippy_ListTableFilter
{
    public function render()
    {
        $val = filter_input(INPUT_GET, $this->getId());

        $output = '<select name="' . $this->getId() . '" class="zippy-list-table-filter zippy-' . $this->getId() . '-list-table-filter">';
        $output .= '<option value="">' . $this->getLabel() . '</option>';

        foreach ($this->options as $key => $value) {
            $output .= '<option value="' . $key . '" ' . selected($val, $key, false) . '>' . $value . '</option>';
        }
        $output .= '</select>';

        return $output;
    }

    protected function _defaultOptions()
    {
        $zippy = Zippy::instance();

        $course_ids = $zippy->utilities->course->getAllCourseIds();
        $options = array();

        foreach ($course_ids as $course_id) {
            $options[$course_id] = get_the_title($course_id);
        }

        $this->options = $options;
    }

    public function filter($student)
    {
        $valid = false;

        $course = filter_input(INPUT_GET, $this->getId());
        if (empty($course)) {
            return true;
        }

        if (!($student instanceof ZippyCourses_Student)) {
            return $valid;
        }

        $zippy = Zippy::instance();
        $course_ids = $student->getCourseIds();

        foreach ($course_ids as $course_id) {
            if ($course_id == $course) {
                $valid = true;
            }
        }


        return $valid;
    }
}
