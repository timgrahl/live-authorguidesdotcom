<?php

class ZippyCourses_QuizResults_ListTable extends Zippy_ListTable
{
    public $per_page = 10;
    public $singular = 'quiz-result';
    public $plural = 'quiz-results';
    public $screen = 'zippy-analytics';
    public $item_type = 'quiz-result';

    public function getData()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $from  = $this->getFromClause();
        $where = $this->getWhereClause();
        $order = $this->getOrderClause();
        $limit = $this->getLimitClause();

        $records = $wpdb->get_results("SELECT * $from $where $order $limit");
        $count   = $wpdb->get_var("SELECT COUNT(*) $from $where $order");

        $this->_pagination_args['total_items'] = $count;
        $this->_pagination_args['total_pages'] = $count / $this->per_page;

        $results = new ZippyCourses_QuizResults_Results;

        foreach ($records as &$record) {
            $record->meta_value = json_decode($record->meta_value);
            $result = new ZippyCourses_QuizResults_Result($record);
            $results->add($result);
        }

        return $results->all();
    }

    public function prepare_items()
    {
        $this->items = $this->getData();
    }

    public function getWhereClause()
    {
        global $wpdb;

        $student = filter_input(INPUT_GET, 'student');
        $quiz    = filter_input(INPUT_GET, 'quiz');

        $clause = $wpdb->prepare("WHERE meta_key = %s", 'quiz_results');

        if (!empty($student)) {
            $clause .= $wpdb->prepare(" AND user_id = %d", $student);
        }

        if (!empty($quiz)) {
            $clause .= ' AND (meta_value LIKE \'{"quiz":"'. $quiz . '"%\' OR meta_value LIKE \'{"quiz":'. $quiz . '%\') ';
        }

        return $clause;
    }

    public function getOrderClause()
    {
        $orderby = filter_input(INPUT_GET, 'orderby');
        $orderby = $orderby == 'date' ? 'umeta_id' : '';

        $order  = filter_input(INPUT_GET, 'order');
        
        return !empty($orderby) && !empty($order)
            ? "ORDER BY $orderby $order"
            : "ORDER BY umeta_id DESC";
    }

    public function getLimitClause()
    {
        $paged  = (int) filter_input(INPUT_GET, 'paged');
        $paged  = $paged ? $paged : 1;

        $offset = $this->per_page * ($paged - 1);
        
        return "LIMIT $offset,{$this->per_page}";
    }

    public function getFromClause()
    {
        global $wpdb;

        return " FROM $wpdb->usermeta";
    }

    public function setupColumns()
    {
        $student   = new ZippyCourses_QuizResultStudent_ListTableColumn(
            'student',
            __('Student', ZippyCourses::TEXTDOMAIN)
        );

        $quiz   = new ZippyCourses_QuizResultQuiz_ListTableColumn(
            'quiz',
            __('Quiz', ZippyCourses::TEXTDOMAIN)
        );

        $score  = new ZippyCourses_QuizResultScore_ListTableColumn(
            'score',
            __('Score', ZippyCourses::TEXTDOMAIN)
        );

        $date   = new ZippyCourses_QuizResultDate_ListTableColumn(
            'date',
            __('Date', ZippyCourses::TEXTDOMAIN)
        );
            $date->setSortable(true);

        $actions  = new ZippyCourses_QuizResultActions_ListTableColumn(
            'actions',
            __('Actions', ZippyCourses::TEXTDOMAIN)
        );
        
        $this->columns->add($quiz);
        $this->columns->add($student);
        $this->columns->add($score);
        $this->columns->add($date);
        $this->columns->add($actions);
    }

    public function setupFilters()
    {
        $quiz = new ZippyCourses_QuizResultsQuiz_ListTableFilter(
            'quiz',
            __('Filter by Quiz...', ZippyCourses::TEXTDOMAIN)
        );
        $this->filters->add($quiz);

        $student = new ZippyCourses_QuizResultsStudent_ListTableFilter(
            'student',
            __('Filter by Student...', ZippyCourses::TEXTDOMAIN)
        );

        $this->filters->add($student);
        $this->filters->add($quiz);
    }
}
