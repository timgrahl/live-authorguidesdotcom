<?php

class ZippyCourses_QuizEntry_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($quiz_id)
    {
        $zippy = Zippy::instance();

        $output = '';
        $entry_ids = $zippy->utilities->quiz->getCourseEntryIds($quiz_id);
        end($entry_ids);

        $last_key = key($entry_ids);
        reset($entry_ids);

        foreach ($entry_ids as $key => $entry_id) {
            $course_id = $zippy->utilities->entry->getCourseId($entry_id);
            $output .= '<a href="">' . get_the_title($entry_id) . '</a>';

            if ($key != $last_key) {
                $output .= '<br/>';
            }
        }

        echo $output;
    }
}
