<?php

class ZippyCourses_ProductCourse_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($product_id)
    {
        $zippy = Zippy::instance();

        $output  = '';

        $courses = $zippy->utilities->product->getCourses($product_id);

        foreach ($courses as $course_id) {
            if ($course_id) {
                $output .= '<a href="' . get_edit_post_link($course_id) . '">' . get_the_title($course_id) . '</a><br/>';
            }
        }

        echo $output;
    }
}
