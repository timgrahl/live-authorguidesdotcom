<?php

class ZippyCourses_QuizResultStudent_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($result)
    {
        $user_id = $result->getUserId();
        $userdata = get_userdata($user_id);

        $user = new stdClass;
        $user->id          = $user_id;
        $user->name        = "{$userdata->first_name} {$userdata->last_name}";
        $user->username    = $userdata->user_login;

        $output = '<a href="' . admin_url('admin.php?page=zippy-student&ID=' . $user->id) . '">' . $user->name . '</a>';

        return $output;
    }
}
