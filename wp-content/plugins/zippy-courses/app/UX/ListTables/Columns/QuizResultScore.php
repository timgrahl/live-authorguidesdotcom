<?php

class ZippyCourses_QuizResultScore_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($result)
    {
        $output = $result->getScore() . '%';

        return $output;
    }
}
