<?php

class ZippyCourses_OrderTotal_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($order_id)
    {
        $zippy = Zippy::instance();

        $order = $zippy->make('order');
        $order->build($order_id);

        echo $order->getTotalString();
    }
}
