<?php

class ZippyCourses_QuizResultActions_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($result)
    {
        $output = '<a href="' . admin_url('admin.php?page=zippy-quiz-results&id=' . $result->getId()) . '" class="button">' . __('View Detailed Results') . '</a>';

        return $output;
    }
}
