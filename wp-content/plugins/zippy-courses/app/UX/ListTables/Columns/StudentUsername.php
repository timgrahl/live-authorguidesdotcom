<?php

class ZippyCourses_StudentUsername_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($student)
    {
        $url = admin_url('admin.php?page=zippy-student&ID=' . $student->getId());
        
        return '<a href="' . $url . '"><strong>' . $student->username . '</strong></a>';
    }

    public function sort($data)
    {
        if (!(reset($data) instanceof ZippyCourses_Student)) {
            return $data;
        }

        $zippy = Zippy::instance();

        $students = $zippy->make('students');
        $students->addItems($data);

        $order = filter_input(INPUT_GET, 'order');
        $order = $order == 'asc' ? 'ASC' : 'DESC';

        $results = $students->sortBy('username');

        return $order == 'ASC' ? $results :array_reverse($results);
    }
}
