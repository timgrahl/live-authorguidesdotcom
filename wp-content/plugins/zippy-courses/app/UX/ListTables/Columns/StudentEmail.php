<?php

class ZippyCourses_StudentEmail_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($student)
    {
        return '<a href="mailto:' . $student->getEmail() . '">' . $student->getEmail() . '</a>';
    }
}
