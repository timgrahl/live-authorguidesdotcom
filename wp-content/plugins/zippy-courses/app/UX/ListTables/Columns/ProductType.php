<?php

class ZippyCourses_ProductType_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($product_id)
    {
        $zippy = Zippy::instance();

        $product = $zippy->make('product', array($product_id));

        $output  = $product->getTypeLabel();

        echo $output;
    }
}
