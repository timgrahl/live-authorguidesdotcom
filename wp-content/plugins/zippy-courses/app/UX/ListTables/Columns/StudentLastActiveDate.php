<?php

class ZippyCourses_StudentLastActiveDate_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($student)
    {
        $zippy = Zippy::instance();
        return $student->getLastActive($zippy->utilities->datetime->getDateFormat());
    }

    public function sort($data)
    {
        if (!(reset($data) instanceof ZippyCourses_Student)) {
            return $data;
        }

        $zippy = Zippy::instance();

        $students = $zippy->make('students');
        $students->addItems($data);

        $order = filter_input(INPUT_GET, 'order');
        $order = $order == 'asc' ? 'ASC' : 'DESC';

        $results = $students->sortBy('last_active');

        return $order == 'ASC' ? $results : array_reverse($results);
    }
}
