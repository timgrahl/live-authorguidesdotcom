<?php

class ZippyCourses_ProductPrice_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($product_id)
    {
        $zippy = Zippy::instance();

        $product = $zippy->make('product', array($product_id));

        $output  = $product->getPriceString();

        echo $output;
    }
}
