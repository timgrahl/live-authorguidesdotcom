<?php

class ZippyCourses_OrderCustomer_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($order_id)
    {
        $zippy = Zippy::instance();

        $output = '<em>' . __('Unclaimed', ZippyCourses::TEXTDOMAIN) . '</em>';

        $order = $zippy->make('order');
        $order->build($order_id);

        $owner_id = $order->getOwner();

        if ($owner_id > 0) {
            $owner = $zippy->make('student', array($owner_id));
            $owner->fetch();

            $output = '<a href="' . admin_url('admin.php?page=zippy-student&ID=' . $owner_id) . '">' . $owner->getFullName() . '</a>';
        }

        echo $output;
    }
}
