<?php

class ZippyCourses_ProductOrders_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($product_id)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $query_id = 'get-product-' . $product_id . '-orders';

        if (($query = $zippy->queries->get($query_id)) === null) {
            global $wpdb;

            $sql = $wpdb->prepare(
                "SELECT COUNT(*)
                 FROM $wpdb->postmeta AS pm
                 LEFT JOIN $wpdb->posts AS p ON (pm.post_id = p.ID)
                 WHERE
                    p.post_type = %s AND
                    pm.meta_key = %s AND
                    pm.meta_value = %s",
                'zippy_order',
                'product',
                $product_id
            );

            $query = $zippy->make('query', array('id' => $query_id));
            $query->setSql($sql);

            $zippy->queries->add($query);
        }

        $output = $query->getVar();

        echo $output;
    }
}
