<?php

class ZippyCourses_OrderProduct_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($order_id)
    {
        $product_id = get_post_meta($order_id, 'product', true);

        echo '<a href="' . get_edit_post_link($product_id) . '">' . get_the_title($product_id) . '</a>';
    }
}
