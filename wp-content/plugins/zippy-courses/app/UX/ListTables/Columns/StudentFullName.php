<?php

class ZippyCourses_StudentFullName_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($student)
    {
        return $student->getFullName();
    }
}
