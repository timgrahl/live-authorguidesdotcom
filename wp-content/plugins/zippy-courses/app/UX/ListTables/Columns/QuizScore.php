<?php

class ZippyCourses_QuizScore_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($quiz_id)
    {
        $zippy = Zippy::instance();

        $results = $zippy->utilities->quiz->getAllResultsForQuiz($quiz_id);
        $score = 0;
        
        if (count($results)) {
            foreach ($results as $result) {
                $score += $result->score;
            }

            $output = number_format_i18n(($score / count($results)), 2) . '%';
            $output .= ' (' . count($results) . ' ' . __('attempts', ZippyCourses::TEXTDOMAIN) . ')';
        } else {
            $output = '<em>' . __('No results yet.', ZippyCourses::TEXTDOMAIN) . '</em>';
        }

        echo $output;
    }
}
