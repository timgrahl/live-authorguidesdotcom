<?php

class ZippyCourses_StudentCourses_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($student)
    {
        $course_ids = $student->getCourseIds();
        $output = '';

        foreach ($course_ids as $course_id) {
            $output .= '<a href="' . get_edit_post_link($course_id) . '">' . get_the_title($course_id) . '</a><br/>';
        }

        return $output;
    }
}
