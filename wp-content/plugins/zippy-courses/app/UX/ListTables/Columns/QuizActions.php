<?php

class ZippyCourses_QuizActions_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($quiz_id)
    {
        $output = '<a href="' . admin_url('post.php?post=' . $quiz_id) . '&action=edit&zippy-tab=quiz-results" class="button">' .
            __('View Breakdown', ZippyCourses::TEXTDOMAIN) .
        '</a> ';

        $output .= '<a href="' . admin_url('admin.php?page=zippy-analytics&tab=quiz-submissions&quiz=' . $quiz_id) . '" class="button">' .
            __('View All Results', ZippyCourses::TEXTDOMAIN) .
        '</a>';

        echo $output;
    }
}
