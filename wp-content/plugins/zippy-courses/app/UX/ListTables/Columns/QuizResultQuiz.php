<?php

class ZippyCourses_QuizResultQuiz_ListTableColumn extends Zippy_ListTableColumn
{
    public function render($result)
    {
        $title = get_the_title($result->getQuiz()->getId());
        
        $output = '<a href="' . get_edit_post_link($result->getQuiz()->getId()) . '">' . $title . '</a>';

        return $output;
    }
}
