<?php

class ZippyCourses_Students_ListTable extends Zippy_ListTable
{
    public $singular = 'student';
    public $plural = 'students';
    public $screen = 'zippy-students';
    public $item_type = 'student';

    public function getData()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $students = $zippy->make('students');

        $search = filter_input(INPUT_GET, 's');
        $args = array();

        $args['role']            = "Subscriber";

        if (!empty($search)) {
            $args['search']         = "*{$search}*";
            $args['search_columns'] = array( 'user_login', 'user_email', 'display_name');
        }

        $args = apply_filters('zippy_courses_student_table_query_args', $args);

        $user_query = new WP_User_Query($args);

        foreach ($user_query->get_results() as $user) {
            $student = $zippy->make('student', array($user->ID));
            $students->add($student);
        }
        $students->prepareStudentTableData();
        return $students->all();
    }

    public function setupColumns()
    {
        $email      = new ZippyCourses_StudentEmail_ListTableColumn(
            'email',
            __('Email', ZippyCourses::TEXTDOMAIN)
        );

        $username   = new ZippyCourses_StudentUsername_ListTableColumn(
            'username',
            __('Username', ZippyCourses::TEXTDOMAIN)
        );
            $username->setSortable(true);


        $full_name  = new ZippyCourses_StudentFullName_ListTableColumn(
            'full_name',
            __('Full Name', ZippyCourses::TEXTDOMAIN)
        );

        $courses    = new ZippyCourses_StudentCourses_ListTableColumn(
            'courses',
            __('Courses', ZippyCourses::TEXTDOMAIN)
        );
        
        $join_date  = new ZippyCourses_StudentJoinDate_ListTableColumn(
            'join_date',
            __('Join Date', ZippyCourses::TEXTDOMAIN)
        );
            $join_date->setSortable(true);
        
        $last_active_date = new ZippyCourses_StudentLastActiveDate_ListTableColumn(
            'last_active_date',
            __('Last Active Date', ZippyCourses::TEXTDOMAIN)
        );
            $last_active_date->setSortable(true);
        
        $this->columns->add($username);
        $this->columns->add($email);
        $this->columns->add($full_name);
        $this->columns->add($courses);
        $this->columns->add($join_date);
        $this->columns->add($last_active_date);
    }

    public function setupFilters()
    {
        $activity = new ZippyCourses_Activity_ListTableFilter(
            'active_since',
            __('Filter by Last Active...', ZippyCourses::TEXTDOMAIN)
        );
        $this->filters->add($activity);

        $joined = new ZippyCourses_Joined_ListTableFilter(
            'joined',
            __('Filter by Join Date...', ZippyCourses::TEXTDOMAIN)
        );
        $this->filters->add($joined);

        $courses = new ZippyCourses_StudentCourse_ListTableFilter(
            'student_courses',
            __('Filter By Course...', ZippyCourses::TEXTDOMAIN)
        );

        $this->filters->add($courses);

    }
}
