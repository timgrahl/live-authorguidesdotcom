<?php 

class ZippyCourses_Activity_ListTable_Filter extends Zippy_ListTable_Filter
{
    public function render()
    {
        $active = filter_input(INPUT_GET, 'active_since');

        $output = '<select name="' . $this->getId() . '">';
        foreach ($this->options as $key => $value) {
            $output .= '<option value="' . $key . '" ' . selected($active, $key, false) . '>' . $value . '</option>';
        }
        $output .= '</select>';
    }

    protected function _defaultOptions()
    {
        $this->options = array(
            '' => '',
            'day' => __('24 hours', ZippyCourses::TEXTDOMAIN),
            'week' => __('1-7 Days (Week)', ZippyCourses::TEXTDOMAIN),
            'month' => __('7-30 Days (Month)', ZippyCourses::TEXTDOMAIN),
            'quarter' => __('1-3 Months', ZippyCourses::TEXTDOMAIN),
            'biannual' => __('3-6 Months', ZippyCourses::TEXTDOMAIN),
            'year' => __('6-12 Months', ZippyCourses::TEXTDOMAIN),
            'years' => '&gt; ' . __('1 Year', ZippyCourses::TEXTDOMAIN),
        );
    }
}
