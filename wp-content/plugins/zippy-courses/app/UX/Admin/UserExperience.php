<?php

class ZippyCourses_AdminUserExperience
{
    private static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_filter('wp_editor_expand', array($this, 'removeEditorExpand'), 10, 2);
        add_action('in_admin_header', array($this, 'renderBanner'));
        
        add_filter('wp_edit_nav_menu_walker', array($this, 'adminNavWalker'), 10, 2);
        add_action('wp_update_nav_menu_item', array($this, 'updateAdminNavWalkerCustomFields'), 10, 3);
        add_filter('wp_setup_nav_menu_item', array($this, 'setupAdminNavWalkerCustomFields'));
    }

    /**
     * Disable Distraction Free Writing and remove the editor expand script
     * because they cause very wonky results when switching tabs in chrome.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function removeEditorExpand($val, $post_type)
    {
        $disabled = array('course');
        return !in_array($post_type, $disabled);
    }

    /**
     * Add the Zippy Courses Banner to screens that require it.
     *
     * @todo Create a filterable array in case people want to add their own pages
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function renderBanner()
    {
        $screen = get_current_screen();
        
        $zippy_screens = array(
            'course',
            'course_order',
            'unit',
            'lesson',
            'quiz',
            'edit-quiz',
            'bundle',
            'edit-bundle',
            'product',
            'edit-course',
            'edit-course_order',
            'edit-product',
            'edit-unit',
            'edit-lesson',
            'zippy-courses_page_zippy-stats',
            'zippy-courses_page_zippy-settings',
            'zippy-courses_page_zippy-students',
            'toplevel_page_zippy-courses'
        );
        
        if (in_array($screen->id, $zippy_screens)) {
            echo '<div class="zippy-banner">
                <a href="https://zippycourses.com/docs/" target="_blank">View Documentation</a>
                <a href="https://zippycourses.com/contact/" target="_blank">Get Support</a>
                <h4><a href="https://zippycourses.com/">Zippy Courses</a></h4>
            </div>';
        }
    }

    public function adminNavWalker($walker, $menu_id)
    {
        return 'ZippyCourses_AdminNavWalker';
    }

    /**
     * Save menu custom fields
     *
     * @access      public
     * @since       1.1.0
     * @return      void
    */
    public function updateAdminNavWalkerCustomFields($menu_id, $menu_item_db_id, $args)
    {
        // Check if element is properly sent
        if (isset($_REQUEST['menu-item-zippy_visibility']) &&
            is_array($_REQUEST['menu-item-zippy_visibility']) &&
            isset($_REQUEST['menu-item-zippy_visibility'][$menu_item_db_id])
        ) {
            $value = $_REQUEST['menu-item-zippy_visibility'][$menu_item_db_id];
            update_post_meta($menu_item_db_id, '_menu_item_zippy_visibility', $value);
        }
    }

    public function setupAdminNavWalkerCustomFields($menu_item)
    {
        $menu_item->zippy_visibility = get_post_meta($menu_item->ID, '_menu_item_zippy_visibility', true);
        return $menu_item;
    }
}
