<?php

class ZippyCourses_AdminNotices
{
    protected $repository;

    public function __construct(Zippy_AdminNotices $repository)
    {
        $this->repository = $repository;
        add_action('admin_init', array($this, 'defaultMessages'));
    }

    public function defaultMessages()
    {
        $zippy = Zippy::instance();

        $db_version = get_option('zippy_db_version', false);

        if (!$db_version || version_compare($db_version, '1.0.0', '<')) {
            $this->_databaseUpgradeRequired_v1_0_0();
        } else {
            $this->_databaseUpgradeInProgress();
            $this->_licenseKeyInstalled();
            $this->_licenseKeyExpired();
            $this->_paymentGateways();
            $this->_newsFeed();
            $this->_corePages();
            $this->_corePageUsedTwice();
        }

        $this->_socialTriggersThemeUpate();
        $this->_zippyCoursesThemeUpate();
        $this->_thankYouMissingShortcodes();
        $this->_permalinks();
        $this->_flashedMessages();
    }

    private function _licenseKeyInstalled()
    {
        $zippy = Zippy::instance();

        $license = new ZippyCourses_License;
        $license_key = $license->getLicenseKey();
        if (empty($license_key)) {
            $notice = $zippy->make(
                'admin_notice',
                array(
                    'id' => 'license-key-missing',
                    'message' => __(
                        'Please set your license key to get Zippy Courses updates and support.',
                        ZippyCourses::TEXTDOMAIN
                    ),
                    'type' => 'warning'
                )
            );

            $notice->addActionButton(
                __('Set License Key', ZippyCourses::TEXTDOMAIN),
                admin_url('admin.php?page=zippy-settings&&tab=zippy_settings_general')
            );

            $this->repository->add($notice);
        }
    }

    private function _licenseKeyExpired()
    {
        $zippy = Zippy::instance();

        $licensing = new ZippyCourses_License;
        $license = $licensing->getLicense();

        $today   = $zippy->utilities->datetime->getToday();
        $expires = $zippy->utilities->datetime->getDate($license['expires']);
        
        if ($today->format('U') > $expires->format('U')) {
            $notice = $zippy->make(
                'admin_notice',
                array(
                    'id' => 'license-key-expired',
                    'message' => __(
                        'Your Zippy Courses License Key has expired.',
                        ZippyCourses::TEXTDOMAIN
                    ),
                    'type' => 'error'
                )
            );

            $notice->addActionButton(
                __('Renew Your License', ZippyCourses::TEXTDOMAIN),
                'https://zippycourses.com/'
            );

            $this->repository->add($notice);
        }
    }

    private function _paymentGateways()
    {
        $zippy = Zippy::instance();

        $gateway_configured = apply_filters('zippy_active_gateway_configured', __return_false());

        if (!$gateway_configured) {
            $notice = $zippy->make(
                'admin_notice',
                array(
                    'id' => 'payment-gateway-unconfigured',
                    'message' => __(
                        'The current payment gateway has not been configured.',
                        ZippyCourses::TEXTDOMAIN
                    ),
                    'type' => 'error'
                )
            );

            $notice->addActionButton(
                __('Configure Gateway', ZippyCourses::TEXTDOMAIN),
                admin_url('admin.php?page=zippy-settings&tab=zippy_settings_payment')
            );

            $this->repository->add($notice);
        }
    }

    private function _newsFeed()
    {
        $zippy = Zippy::instance();

        $rss  = $zippy->make('rss');
        $item = $rss->getItems(1);

        if ($item && !empty($item)) {
            $item = reset($item); // Get as an object
            $dismissed_updates = (array) get_option('zippy_rss_dismissed', array());

            if (is_object($item) && !in_array($item->get_permalink(), $dismissed_updates)) {
                $notice = $zippy->make(
                    'admin_notice',
                    array(
                        'id' => 'news-update',
                        'message' => $item->get_title(),
                        'type' => 'success'
                    )
                );

                $notice->addActionButton(
                    __('Read More', ZippyCourses::TEXTDOMAIN),
                    $item->get_permalink(),
                    'button-primary'
                );

                $notice->addActionButton(
                    __('Dismiss', ZippyCourses::TEXTDOMAIN),
                    $item->get_permalink(),
                    'dismiss-zippy-news-item'
                );

                $this->repository->add($notice);
            }
        }
    }

    private function _userDeleted()
    {
    }

    private function _corePageUsedTwice()
    {
        $zippy = Zippy::instance();

        $list = array();

        foreach ($zippy->core_pages->getList() as $key => $details) {
            if ($details['id']) {
                $list[] = $details['id'];
            }
        }

        
        $unique = array_unique($list);

        if (count($list) > count($unique)) {
            $notice = $zippy->make(
                'admin_notice',
                array(
                    'id' => 'core-pages-unique',
                    'message' => __(
                        'You are using the same page as a Core Page more than once, which may cause unexpected results.',
                        ZippyCourses::TEXTDOMAIN
                    ),
                    'type' => 'error'
                )
            );

            $notice->addActionButton(
                __('Fix Core Pages', ZippyCourses::TEXTDOMAIN),
                admin_url('admin.php?page=zippy-settings&tab=zippy_settings_core_pages')
            );

            $this->repository->add($notice);
        }
    }

    private function _corePages()
    {
        $zippy = Zippy::instance();

        $missing = array();
        foreach ($zippy->core_pages->getList() as $key => $details) {
            if (!$details['id'] && $key !== 'course_directory') {
                $missing[] = $details['title'];
            }
        }

        if (count($missing)) {
            $last = array_pop($missing);
            $missing_pages = count($missing) > 0
                ? implode(', ', $missing) . '</strong> ' . __('and', ZippyCourses::TEXTDOMAIN) . ' <strong>' . $last
                : ($last !== null
                    ? $last
                    : ''
                );

            $notice = $zippy->make(
                'admin_notice',
                array(
                    'id' => 'core-pages-missing',
                    'message' => sprintf(
                        __(
                            'The following Core Pages have not been configured for Zippy Courses: <strong>%s</strong>',
                            ZippyCourses::TEXTDOMAIN
                        ),
                        $missing_pages
                    ),
                    'type' => 'error'
                )
            );

            $notice->addActionButton(
                __('Configure Core Pages', ZippyCourses::TEXTDOMAIN),
                admin_url('admin.php?page=zippy-settings&tab=zippy_settings_core_pages')
            );

            $this->repository->add($notice);
        }
    }

    public function _flashedMessages()
    {
        $zippy = Zippy::instance();

        $list = $zippy->sessions->fetchAdminMessages();

        $notices = '';
        $i = 0;
        foreach ($list as $type => $messages) {
            foreach ($messages as $message) {
                $notice = $zippy->make(
                    'admin_notice',
                    array(
                        'id' => 'zippy-flash-message-' . $i,
                        'message' => $message,
                        'type' => $type
                    )
                );

                $this->repository->add($notice);
                $i++;
            }
        }
    }

    /**
     * Set a notice for the upgrading the database
     * @return void
     */
    private function _databaseUpgradeRequired_v1_0_0()
    {
        $zippy = Zippy::instance();

        $db_version     = get_option('zippy_db_version', false);
        $wpcs_version   = get_option('wpcs_db_version', false);

        if (!$db_version && $wpcs_version !== false) {
            $notice = $zippy->make(
                'admin_notice',
                array(
                    'id' => 'database-upgrade-required-v1_0_0',
                    'message' => __(
                        'Your Zippy Courses database is out of date, and <strong>must be upgraded</strong> for Zippy Courses to continue to work correctly.',
                        ZippyCourses::TEXTDOMAIN
                    ),
                    'type' => 'error'
                )
            );

            $notice->addActionButton(
                __('Upgrade Database', ZippyCourses::TEXTDOMAIN),
                admin_url('index.php?page=zippy-upgrade')
            );

            $this->repository->add($notice);
        }
    }

    private function _databaseUpgradeInProgress()
    {
        $zippy = Zippy::instance();

        $in_progress = get_option('zippy_doing_upgrade', false);

        if ($in_progress) {
            $notice = $zippy->make(
                'admin_notice',
                array(
                    'id' => 'database-upgrade-in-progress',
                    'message' => __(
                        'A Zippy Courses Database Upgrade has been stopped before finishing.  You <strong>must</strong> allow it to finish for your site to function as expected.',
                        ZippyCourses::TEXTDOMAIN
                    ),
                    'type' => 'error'
                )
            );

            $notice->addActionButton(
                __('Finish Upgrade', ZippyCourses::TEXTDOMAIN),
                admin_url('index.php?page=zippy-upgrade&zippy-upgrade-action=start')
            );

            $this->repository->add($notice);
        }
    }

    public function _thankYouMissingShortcodes()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $post_id = filter_input(INPUT_GET, 'post');
        $settings   = get_option('zippy_payment_general_settings');
        $method     = isset($settings['method']) ? $settings['method'] : false;


        if (!empty($post_id) && $zippy->core_pages->is($post_id, 'thank_you')) {
            $content = $wpdb->get_var("SELECT post_content FROM $wpdb->posts WHERE ID = $post_id");

            if ($method == 'samcart') {
                $notice = $zippy->make(
                    'admin_notice',
                    array(
                        'id' => 'samcart-thank-you-information',
                        'message' => __(
                            'You are using Samcart as your payment provider. When a purchase is made, students will be sent an email with their login information. Be sure to let your students know to check their email for their registration information.',
                            ZippyCourses::TEXTDOMAIN
                        ),
                        'type' => 'success'
                        )
                );
                $this->repository->add($notice);
            } else if (empty($content)) {
                $notice = $zippy->make(
                    'admin_notice',
                    array(
                        'id' => 'empty-thank-you',
                        'message' => __(
                            'Your Thank You page is empty and will currently display the Zippy Courses default Thank You page message.  If you would like to customize, please be sure to include the following shortcodes: <code>[zippy_button type="register"]</code> and <code>[zippy_button type="register-login"]</code>',
                            ZippyCourses::TEXTDOMAIN
                        ),
                        'type' => 'warning'
                    )
                );

                $this->repository->add($notice);
            } else {
                if (strpos($content, 'zippy_button type="register-login"') === false &&
                    strpos($content, 'zippy_button type="register"') === false &&
                    strpos($content, "zippy_button type='register-login'") === false &&
                    strpos($content, "zippy_button type='register'") === false &&
                    strpos($content, "zippy_button type=register-login") === false &&
                    strpos($content, "zippy_button type=register") === false
                ) {
                    $notice = $zippy->make(
                        'admin_notice',
                        array(
                            'id' => 'missing-thank-you-shortcodes',
                            'message' => __(
                                'Your Thank You page is missing shortcodes for buttons that allow people to login or register to claim their orders.  Please add [zippy_button type="register"] or [zippy_button type="register-login"] to resolve this issue.',
                                ZippyCourses::TEXTDOMAIN
                            ),
                            'type' => 'warning'
                        )
                    );
                    $this->repository->add($notice);
                }
            }
            do_action('zippy_thank_you_shortcodes_notice');
        }
    }

    public function _zippyCoursesThemeUpate()
    {
        $zippy = Zippy::instance();

        $theme = wp_get_theme();

        if ((
                $theme->get_stylesheet() == 'zippy-courses-theme' ||
                $theme->get('TextDomain') == 'zippy-courses-theme' ||
                $theme->get('TextDomain') == 'wpcs'
            ) &&
            version_compare($theme->get('Version'), '1.0.0', '<')
        ) {
            $notice = $zippy->make(
                'admin_notice',
                array(
                    'id' => 'zippy-courses-theme-update',
                    'message' => __(
                        'Please update your Zippy Courses Theme to work with the latest version of the Zippy Courses plugin.',
                        ZippyCourses::TEXTDOMAIN
                    ),
                    'type' => 'warning'
                )
            );

            $notice->addActionButton(
                __('Update Themes', ZippyCourses::TEXTDOMAIN),
                admin_url('themes.php'),
                'button-primary'
            );

            $this->repository->add($notice);
        }
    }

    public function _socialTriggersThemeUpate()
    {
        $zippy = Zippy::instance();

        $theme = wp_get_theme();

        if ($theme->get('TextDomain') == 'zippy-social-triggers' && version_compare($theme->get('Version'), '1.1.0', '<')) {
            $notice = $zippy->make(
                'admin_notice',
                array(
                    'id' => 'zippy-social-triggers-theme-update',
                    'message' => __(
                        'Please update the Social Triggers Theme for Zippy Courses to work with the latest version of the Zippy Courses plugin.',
                        ZippyCourses::TEXTDOMAIN
                    ),
                    'type' => 'warning'
                )
            );

            $notice->addActionButton(
                __('Update Themes', ZippyCourses::TEXTDOMAIN),
                admin_url('themes.php'),
                'button-primary'
            );

            $this->repository->add($notice);
        }
    }

    public function _permalinks()
    {
        $zippy = Zippy::instance();

        $structure = get_option('permalink_structure', '');

        if (empty($structure)) {
            $notice = $zippy->make(
                'admin_notice',
                array(
                    'id' => 'zippy-requires-permalinks',
                    'message' => __(
                        'Zippy Courses requires that you configure your permalinks. Please do so before attempting to sell any courses.',
                        ZippyCourses::TEXTDOMAIN
                    ),
                    'type' => 'warning'
                )
            );

            $notice->addActionButton(
                __('Update Permalinks', ZippyCourses::TEXTDOMAIN),
                admin_url('options-permalink.php'),
                'button-primary'
            );

            $this->repository->add($notice);
        }
    }
}
