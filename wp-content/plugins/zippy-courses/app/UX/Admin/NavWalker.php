<?php

class ZippyCourses_AdminNavWalker extends Walker_Nav_Menu_Edit
{
    /**
     * Start the element output.
     *
     * @see \Walker_Nav_Menu_Edit::start_el()
     * @since 1.1.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   Not used.
     * @param int    $id     Not used.
     */
    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        parent::start_el($output, $item, $depth, $args, $id);

        if ($item->ID != $item->object_id) {
            return;
        }

        $doc = new DOMDocument();

        /**
         * The default method that native WordPress the Walker_Nav_Menu_Edit class uses to build nested menu HTML creates
         * invalid HTML.  Normally, this causes DOMDocument::loadHTML to throw a warning.  To avoid this, we enable the
         * usage of libxml interal errors and then toggle it off right after, avoiding the error being thrown, and
         * therefore visible, on sites with WP_DEBUG on.
         */
        @$doc->loadHTML($output . '</li>'); // We add the closing LI to pass valid HTML to DOMDocument
       
        $element    = $this->getVisibilityContainer($doc, $item);
        $ref        = $doc->getElementById("menu-item-settings-{$item->ID}");

        if (!is_object($ref)) {
            return;
        }

        $ref->insertBefore($element, $ref->firstChild);

        // Save the HTML, strip the doctype, wrappers, and the trailing </li> tag
        $doc->removeChild($doc->doctype);

        $html = $doc->saveHTML();
        $html = trim(str_replace(array('<body>', '<html>', '</body>', '</html>'), '', $html));
        $html = substr($html, 0, -5);
    
        // Set the Output to be $html
        // Nothing is returned, as $output is a reference
        $output = $html;
    }

    public function getVisibilityContainer(DOMDocument $doc, $item)
    {
        $element = $doc->createElement('div');
        $element->setAttribute('class', 'zippy-menu-item-container');
        $element->setAttribute('id', 'zippy-menu-item-container-' . $item->ID);

        $label = $doc->createElement('label', __('Visibility'));
        $label->setAttribute('for', 'edit-menu-item-zippy_visibility-' . $item->ID);

        $dropdown = $doc->createElement('select');
        $dropdown->setAttribute('id', 'edit-menu-item-zippy_visibility-' . $item->ID);
        $dropdown->setAttribute('class', 'widefat edit-menu-item-custom');
        $dropdown->setAttribute('name', 'menu-item-zippy_visibility[' . $item->ID . ']');

        $option0 = $doc->createElement('option', 'All');
        $option0->setAttribute('value', '0');
        if (isset($item->zippy_visibility) && $item->zippy_visibility == '0') {
            $option0->setAttribute('selected', 'selected');
        }

        $option1 = $doc->createElement('option', 'Non-Students Only');
        $option1->setAttribute('value', '1');
        if (isset($item->zippy_visibility) && $item->zippy_visibility == '1') {
            $option1->setAttribute('selected', 'selected');
        }

        $option2 = $doc->createElement('option', 'Students Only');
        $option2->setAttribute('value', '2');
        if (isset($item->zippy_visibility) && $item->zippy_visibility == '2') {
            $option2->setAttribute('selected', 'selected');
        }

        $dropdown->appendChild($option0);
        $dropdown->appendChild($option1);
        $dropdown->appendChild($option2);

        $element->appendChild($label);
        $element->appendChild($dropdown);

        return $element;
    }
}
