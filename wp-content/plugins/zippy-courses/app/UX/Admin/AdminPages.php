<?php

class ZippyCourses_AdminPages
{
    public function __construct()
    {
        $this->createDefaults();

        add_action('admin_init', array($this, 'grantStudentAccess'));
        add_action('admin_init', array($this, 'duplicateCourse'));
        add_action('admin_init', array($this, 'resetApiKey'));
        add_action('admin_menu', array($this, 'createGettingStartedPage'));
        add_action('admin_menu', array($this, 'createUpgradePage'));
        add_action('admin_footer', array($this, 'templates'));
    }

    public function templates()
    {
        include(ZippyCourses::$path . 'assets/views/templates/admin/email-list.php');
        include(ZippyCourses::$path . 'assets/views/templates/admin/modals/course/access-bulk-edit.php');
        include(ZippyCourses::$path . 'assets/views/templates/admin/modals/course/access-config.php');
        include(ZippyCourses::$path . 'assets/views/templates/admin/modals/course/additional-content.php');
        include(ZippyCourses::$path . 'assets/views/templates/admin/modals/course/add-unit.php');
        include(ZippyCourses::$path . 'assets/views/templates/admin/modals/course/bulk-edit-additional-content-access.php');
        include(ZippyCourses::$path . 'assets/views/templates/admin/modals/course/delete-additional-content-entry.php');
        include(ZippyCourses::$path . 'assets/views/templates/admin/modals/course/delete-entry.php');
        include(ZippyCourses::$path . 'assets/views/templates/admin/modals/course/existing-content.php');
        include(ZippyCourses::$path . 'assets/views/templates/admin/modals/course/scheduling-bulk-edit.php');
        include(ZippyCourses::$path . 'assets/views/templates/admin/modals/course/scheduling-config.php');
    }

    public function duplicateCourse()
    {
        $post_id = filter_input(INPUT_GET, 'post');
        $action  = filter_input(INPUT_GET, 'action');

        if (!empty($post_id) && get_post_type($post_id) == 'course' && $action == 'duplicate') {
            $zippy = Zippy::instance();

            $old_course_id = $post_id;
            $new_course_id = $zippy->utilities->post->duplicatePost($old_course_id);

            if (!is_wp_error($new_course_id)) {
                $zippy->utilities->post->duplicateMeta($old_course_id, $new_course_id);

                wp_redirect(admin_url('post.php?post=' . $new_course_id . '&action=edit'));
                exit;
            }
        }
    }

    public function resetApiKey()
    {
        $page       = filter_input(INPUT_GET, 'page');
        $reset      = filter_input(INPUT_GET, 'reset_api_key');
        $settings   = get_option('zippy_advanced_settings', array());

        if ($page == 'zippy-settings' && $reset == '1') {
            $settings['api_key'] = Zippy_API::generateApiKey();
            
            update_option('zippy_advanced_settings', $settings);
        }

        return;
    }

    public function grantStudentAccess()
    {
        $zippy = Zippy::instance();

        $student_id = (int) filter_input(INPUT_GET, 'ID');
        $product_id = (int) filter_input(INPUT_GET, 'grant_access');

        if ($student_id && $product_id) {
            $zippy->access->grant($student_id, $product_id);
        }
    }

    private function createDefaults()
    {
        $this->createDashboardPage();
        $this->createSettingsPage();
        $this->createStudentsPage();
        $this->createStudentPage();
        $this->createNewStudentPage();
        $this->createImportExportPage();
        $this->createAnalyticsPage();
        // $this->createTestingPage();
        $this->createQuizResultsPage();
    }

    private function createDashboardPage()
    {
        $zippy = Zippy::instance();

        $page = new Zippy_AdminPage('zippy-courses', 'Zippy Courses');
        $page->setPosition('58.9');

        $zippy->admin_pages->add($page);

        return $page;
    }

    private function createSettingsPage()
    {
        $zippy = Zippy::instance();

        $page = new Zippy_AdminPage('zippy-settings', __('Settings', ZippyCourses::TEXTDOMAIN));
        $page->setParent('zippy-courses');
        
        $zippy->admin_pages->add($page);

        return $page;
    }

    private function createTestingPage()
    {
        $zippy = Zippy::instance();

        $page = new Zippy_AdminPage('zippy-testing', 'Testing');
        $page->setParent('zippy-courses');

        $zippy->admin_pages->add($page);

        return $page;
    }

    private function createStudentsPage()
    {
        $zippy = Zippy::instance();

        $page = new Zippy_AdminPage('zippy-students', __('Students', ZippyCourses::TEXTDOMAIN));
        $page->setParent('zippy-courses');
        
        $zippy->admin_pages->add($page);

        return $page;
    }

    private function createStudentPage()
    {
        $zippy = Zippy::instance();

        $page = new Zippy_AdminPage('zippy-student', '');
        $page->setParent('hidden');

        $zippy->admin_pages->add($page);

        return $page;
    }

    private function createQuizResultsPage()
    {
        $zippy = Zippy::instance();

        $page = new Zippy_AdminPage('zippy-quiz-results', '');
        $page->setParent('hidden');

        $zippy->admin_pages->add($page);

        return $page;
    }

    private function createNewStudentPage()
    {
        $zippy = Zippy::instance();

        $page = new Zippy_AdminPage('zippy-new-student', '');
        $page->setParent('hidden');

        $zippy->admin_pages->add($page);

        return $page;
    }

    private function createImportExportPage()
    {
        $zippy = Zippy::instance();

        $page = new Zippy_AdminPage('zippy-import-export', __('Import/Export', ZippyCourses::TEXTDOMAIN));
        $page->setParent('zippy-courses');
        
        $zippy->admin_pages->add($page);

        return $page;
    }

    private function createAnalyticsPage()
    {
        $zippy = Zippy::instance();

        $page = new Zippy_AdminPage('zippy-analytics', __('Analytics', ZippyCourses::TEXTDOMAIN));
        $page->setParent('zippy-courses');
        
        $zippy->admin_pages->add($page);

        return $page;
    }

    public function createGettingStartedPage()
    {
        // Getting Started Page
        add_dashboard_page(
            __('Getting started with Zippy Courses', ZippyCourses::TEXTDOMAIN),
            __('Getting started with Zippy Courses', ZippyCourses::TEXTDOMAIN),
            'edit_others_posts',
            'zippy-getting-started',
            array($this, 'gettingStarted')
        );

        remove_submenu_page('index.php', 'zippy-getting-started');
    }

    public function gettingStarted()
    {
        include ZippyCourses::$path . 'assets/views/admin-pages/zippy-getting-started.php';
    }

    public function createUpgradePage()
    {
        // Getting Started Page
        add_dashboard_page(
            __('Upgrade Zippy Courses', ZippyCourses::TEXTDOMAIN),
            __('Upgrade Zippy Courses', ZippyCourses::TEXTDOMAIN),
            'edit_others_posts',
            'zippy-upgrade',
            array($this, 'upgrade')
        );

        remove_submenu_page('index.php', 'zippy-upgrade');
    }

    public function upgrade()
    {
        include ZippyCourses::$path . 'assets/views/admin-pages/zippy-upgrade.php';
    }
}
