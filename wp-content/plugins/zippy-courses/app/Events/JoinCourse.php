<?php

class ZippyCourses_JoinCourse_Event extends Zippy_Event
{
    public $student;
    public $course;
    public $tier;

    public function __construct(ZippyCourses_Student $student, ZippyCourses_Course $course)
    {
        $this->student  = $student;
        $this->course   = $course;
    }
}
