<?php

class ZippyCourses_CourseCompletion_Event extends Zippy_Event
{
    public $student;
    public $course;

    public function __construct(ZippyCourses_Student $student, Zippy_Course $course)
    {
        $this->student  = $student;
        $this->course   = $course;
    }
}
