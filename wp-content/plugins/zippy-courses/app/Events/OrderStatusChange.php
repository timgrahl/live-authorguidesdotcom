<?php

class ZippyCourses_OrderStatusChange_Event extends Zippy_Event
{
    public $order;
    public $old_status;
    public $new_status;

    public function __construct(Zippy_Order $order, $old_status, $new_status)
    {
        $this->order        = $order;
        $this->new_status   = $new_status;
        $this->old_status   = $old_status;
    }
}
