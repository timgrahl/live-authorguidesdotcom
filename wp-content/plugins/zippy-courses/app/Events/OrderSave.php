<?php

class ZippyCourses_OrderSave_Event extends Zippy_Event
{
    public $order;

    public function __construct(Zippy_Order $order)
    {
        $this->order = $order;
    }
}
