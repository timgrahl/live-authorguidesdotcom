<?php

class ZippyCourses_OrderOwnerChange_Event extends Zippy_Event
{
    public $order;
    public $old_owner;
    public $new_owner;

    public function __construct(Zippy_Order $order, $old_owner, $new_owner)
    {
        $this->order       = $order;
        $this->new_owner   = $new_owner;
        $this->old_owner   = $old_owner;
    }
}
