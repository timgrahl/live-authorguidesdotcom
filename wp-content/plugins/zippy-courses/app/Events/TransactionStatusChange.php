<?php

class ZippyCourses_TransactionStatusChange_Event extends Zippy_Event
{
    public $transaction;
    public $old_status;
    public $new_status;

    public function __construct(Zippy_Transaction $transaction, $old_status, $new_status)
    {
        $this->transaction  =  $transaction;
        $this->new_status   = $new_status;
        $this->old_status   = $old_status;
    }
}
