<?php

class ZippyCourses_JoinProduct_Event extends Zippy_Event
{
    public $student;
    public $product;

    public function __construct(ZippyCourses_Student $student, Zippy_Product $product)
    {
        $this->student = $student;
        $this->product = $product;
    }
}
