<?php

class ZippyCourses_Registration_Event extends Zippy_Event
{
    public $student;

    public function __construct(ZippyCourses_Student $student)
    {
        $this->student = $student;
    }
}
