<?php

class ZippyCourses_LeaveCourse_Event extends Zippy_Event
{
    public $student;
    public $course;
    public $tier;

    public function __construct(ZippyCourses_Student $student, ZippyCourses_Course $course, ZippyCourses_Tier $tier)
    {
        $this->student  = $student;
        $this->course   = $course;
        $this->tier     = $tier;
    }
}
