<?php

class ZippyCourses_SaveTransaction_Event extends Zippy_Event
{
    public $transaction;

    public function __construct(Zippy_Transaction $transaction)
    {
        $this->transaction = $transaction;
    }
}
