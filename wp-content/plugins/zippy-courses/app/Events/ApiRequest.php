<?php

class ZippyCourses_ApiRequest_Event extends Zippy_Event
{
    public $endpoint;
    public $method;
    public $parameters;
    public $data;
    public $response;

    public function __construct($endpoint, $method, array $parameters, $data)
    {
        $this->endpoint     = $endpoint;
        $this->method       = $method;
        $this->parameters   = $parameters;
        $this->data         = $data;
    }
}
