<?php

class ZippyCourses_NewStudent_Event extends Zippy_Event
{
    public $student;
    public $order;

    public function __construct(ZippyCourses_Student $student, Zippy_Order $order)
    {
        $this->student = $student;
        $this->order   = $order;
    }
}
