<?php

class ZippyCourses_LandingPageSubscribe_Event extends Zippy_Event
{
    public $student;
    public $list;
    public $service;

    public function __construct(ZippyCourses_Student $student, Zippy_EmailList $list, $service)
    {
        $this->student = $student;
        $this->list = $list;
        $this->service = $service;
    }
}
