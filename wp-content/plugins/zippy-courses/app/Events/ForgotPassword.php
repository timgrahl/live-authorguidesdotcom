<?php

class ZippyCourses_ForgotPassword_Event extends Zippy_Event
{
    public $student;

    public function __construct(ZippyCourses_Student $student)
    {
        $this->student  = $student;
    }
}
