<?php

class ZippyCourses_NewOrder_Event extends Zippy_Event
{
    public $order;

    public function __construct(Zippy_Order $order)
    {
        $this->order = $order;
    }
}
