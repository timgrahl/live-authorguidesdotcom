<?php

class ZippyCourses_TransactionNotification_Event extends Zippy_Event
{
    public $gateway;
    public $post;
    public $get;
    public $request;
    public $raw;

    public function __construct($gateway = '')
    {
        $this->gateway  = $gateway;
        $this->post     = $_POST;
        $this->raw_post = file_get_contents('php://input');
        $this->get      = array();
        foreach ($_GET as $key => $val) {
            $this->get[$key] = urldecode($val);
        }

        $this->request  = $_REQUEST;
    }
}
