<?php

class ZippyCourses_Product
{
    public $id;
    
    public function __construct($id, $fetch = false)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmailList($service)
    {
        $zippy = Zippy::instance();
        
        $api = $zippy->make('mailchimp_api', array('api_key' => 'e9c467a53318338644c7beaa8923ac75-us10'));

        return $api->getLists()->last();
    }
}
