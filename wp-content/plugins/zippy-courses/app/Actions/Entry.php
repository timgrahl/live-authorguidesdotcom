<?php

class ZippyCourses_Entry_Actions
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_action('template_redirect', array($this, 'unpublished'));
        add_action('save_post', array($this, 'flushStudentCaches'), 10, 1);
    }

    /**
     * If a non-editor or higher role attempts to view an item in a course, then redirect them to the dashboard
     *
     * @since   1.0.2
     * @return  void
     */
    public function unpublished()
    {
        global $post;

        if (!is_object($post)) {
            return;
        }

        if (current_user_can('edit_others_posts')) {
            return;
        }

        $zippy = Zippy::instance();

        $course_id = $zippy->utilities->entry->getCourseId($post->ID);

        if (!$course_id) {
            return;
        }

        if (get_post_status($course_id) == 'draft') {
            $entry_ids = $zippy->utilities->course->getCourseEntryIds($course_id);

            if (in_array($post->ID, $entry_ids)) {
                $zippy->sessions->flashMessage(
                    __('That Course has not been published yet! Please try again later.', ZippyCourses::TEXTDOMAIN),
                    'warning'
                );

                wp_redirect($zippy->core_pages->getUrl('dashboard'));
                exit;
            }
        }
    }

    public function flushStudentCaches($post_id)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $entry_ids = $zippy->utilities->entry->getAllProtectedEntryIds();
        
        if (get_post_type($post_id) == 'course' || get_post_type($post_id) == 'zippy_order' || get_post_type($post_id) == 'product' || in_array($post_id, $entry_ids)) {
            $wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'zippy_middleware_rules_cache'");
            $wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'zippy_middleware_rules_cache_timestamp'");
        }
    }
}
