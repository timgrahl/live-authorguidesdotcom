<?php

class ZippyCourses_Student_Actions
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_action('template_redirect', array($this, 'ipTracking'));
        add_action('after_setup_theme', array($this, 'removeAdminBar'));
        add_action('init', array($this, 'registerListener'));
    }
    
    public function registerListener()
    {
        $listener = new ZippyCourses_NewStudentListener;
    }

    public function ipTracking()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        if ($student !== null) {
            $now = $zippy->utilities->datetime->getNow();

            $ip_address = $student->getRealIpAddress();

            $ip_addresses = array_filter((array) get_user_meta($student->getId(), '_zippy_ip_addresses', true));

            $exists = false;
            foreach ($ip_addresses as $key => $ip) {
                if ($ip['address'] == $ip_address) {
                    $exists = $key;
                }
            }

            $record = $exists !== false ? $ip_addresses[$key] : array('address' => $ip_address);
            $record['time'] = $now->format('U');

            if ($exists !== false) {
                $ip_addresses[$key] = $record;
            } else {
                $ip_addresses[] = $record;
            }

            update_user_meta($student->getId(), '_zippy_ip_addresses', $ip_addresses);
        }
    }

    function removeAdminBar() {
        if (!current_user_can('edit_others_posts') && !is_admin()) {
            show_admin_bar(false);
        }
    }
}
