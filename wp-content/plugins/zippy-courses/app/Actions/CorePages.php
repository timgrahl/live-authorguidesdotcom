<?php

class ZippyCourses_CorePages_Actions
{
    protected static $instance;
    public $logout_redirect_url;
    public $dashboard_redirect_url;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        $zippy = Zippy::instance();
        $this->logout_redirect_url = apply_filters('zippy_courses_logout_redirect_url', home_url());


        add_action('template_redirect', array($this, 'login'), 1);
        add_action('template_redirect', array($this, 'logout'), 1);
        add_action('template_redirect', array($this, 'register'));
        add_action('template_redirect', array($this, 'studentOnly'), 9);
    }

    public function logout()
    {
        global $post;

        if (!is_object($post)) {
            return;
        }

        $zippy = Zippy::instance();

        if ($zippy->core_pages->is($post->ID, 'logout')) {
            $zippy->sessions->flashMessage(__('You have been logged out.', ZippyCourses::TEXTDOMAIN));

            wp_logout();
            wp_redirect($this->logout_redirect_url);
            exit;
        }
    }

    public function login()
    {
        global $post;
        global $current_user;

        if (!is_object($post)) {
            return;
        }

        $zippy = Zippy::instance();

        if ($zippy->core_pages->is($post->ID, 'login') && is_user_logged_in()) {
            // If the student is already logged in and uses a "Claim" order link, make sure the order is claimed
            $claim = filter_input(INPUT_GET, 'claim', FILTER_SANITIZE_STRING);
            if (!empty($claim)) {
                $student = $zippy->make('student', array($current_user->ID));
                $student->fetch();
                $zippy->utilities->product->claimProductByUid($student, $claim);
            }
            wp_redirect($zippy->core_pages->getUrl('dashboard'));
            exit;
        }
    }

    public function register()
    {
        global $post, $current_user;

        if (!is_object($post)) {
            return;
        }

        $zippy = Zippy::instance();
        $redirect = $zippy->core_pages->getUrl('dashboard');

        if ($zippy->core_pages->is($post->ID, 'register') && is_user_logged_in()) {
            $student = $zippy->make('student', array($current_user->ID));
            $claim = filter_input(INPUT_GET, 'claim', FILTER_SANITIZE_STRING);


            if (!empty($claim)) {
                if (current_user_can('edit_others_posts')) {
                    $zippy->sessions->flashMessage(
                        __('Your order has been completed, but you are currently logged in as an administrator, so your order cannot be claimed. Visit the "Orders" menu to view your completed order.', ZippyCourses::TEXTDOMAIN),
                        'warning'
                    );
                    wp_redirect($zippy->core_pages->getUrl('dashboard'));
                    exit;
                }


                $product = $zippy->utilities->product->getByUid($claim);

                if ($product === null) {
                    $transaction = $zippy->make('transaction');
                    $transaction->buildByTransactionKey($claim);

                    $zippy->access->claim($student, $transaction);
                } else {
                    $zippy->access->grant($current_user->ID, $product->getId());
                    $courses = $product->getCourses();
                    foreach ($courses as &$course) {
                        $course = get_the_title($course);
                    }
                    
                    $zippy->sessions->flashMessage(
                        sprintf(
                            __('You have joined the following course(s): %s', ZippyCourses::TEXTDOMAIN),
                            '<strong>' . implode(', ', $courses) . '</strong>'
                        )
                    );
                    $redirect = apply_filters('zippy_courses_purchase_complete_redirect', $redirect, $current_user->ID);
                }
            }

            wp_redirect($redirect);
            exit;
        }
    }

    public function studentOnly()
    {
        global $post;

        if (!is_object($post) || is_front_page() || is_home()) {
            return;
        }

        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');
        $needs_redirect = $student === null
            ? array_values($zippy->core_pages->getPrivateOnlyCorePageIds())
            : array_values($zippy->core_pages->getPublicOnlyCorePageIds());

        if (in_array($post->ID, $needs_redirect)) {
            if ($student === null) {
                $zippy->sessions->flashMessage(
                    __('You must be logged in to view that page.', ZippyCourses::TEXTDOMAIN),
                    'error'
                );
                wp_redirect($zippy->core_pages->getUrl('login') . '?redirect_to=' . urlencode(get_permalink($post->ID)));
                exit;
            } else {
                $zippy->sessions->flashMessage(
                    __('You are already logged in!', ZippyCourses::TEXTDOMAIN),
                    'warning'
                );
                wp_redirect($zippy->core_pages->getUrl('dashboard'));
                exit;
            }

        }
    }
}
