<?php

class ZippyCourses_Admin_Actions
{
        protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_action('redirect_post_location', array($this, 'redirectAfterSave'), 10, 2);
        add_action('admin_init', array($this, 'welcome'), 1);
        add_filter('pre_update_option_zippy_license_settings', array($this, 'clearLicenseTransients'));
        add_action('admin_init', array($this, 'redirectFromAdmin'), 1);
        add_action('do_meta_boxes', array($this, 'removeFeaturedImageMetabox'));
    }

    public function redirectFromAdmin()
    {
        if (!defined('DOING_AJAX')) {
            $zippy = Zippy::instance();

            if (is_user_logged_in() && !current_user_can('edit_others_posts')) {
                $url = $zippy->core_pages->getUrl('dashboard');
                $url = empty($url) ? home_url() : $url;
                
                wp_redirect($url);
                exit;
            }
        }
    }

    public function redirectAfterSave($location, $post_id)
    {
        $zippy = Zippy::instance();

        if (isset($_POST['zippy_return']) && $_POST['zippy_return'] == '1') {
            $course_id = $zippy->utilities->entry->getCourseId($post_id);

            if ($course_id) {
                $url = get_edit_post_link($course_id, '');
                if (get_post_type($post_id) == 'product') {
                    $url .= '&zippy-tab=course-public-details';
                } else {
                    $url .= '&zippy-tab=course-entries';
                }

                return $url;
            }
        }

        if (isset($_POST['zippy_save_and_go_to'] ) && !empty($_POST['zippy_save_and_go_to'])) {
            $goto = $_POST['zippy_save_and_go_to'];

            if (is_numeric($goto)) {
                $url = get_edit_post_link($_POST['zippy_save_and_go_to'], '');
                if (get_post_type($goto) == 'course') {
                    if (get_post_type($post_id) == 'product') {
                        $url .= '&zippy-tab=course-public-details';
                    } else {
                        $url .= '&zippy-tab=course-entries';

                    }
                }
            } else {
                $url = $goto;
            }

            return $url;
        }

        return $location;
    }

    public function welcome()
    {
        // Bail if no activation redirect
        if (!get_transient('_zippy_activation_redirect')) {
            return;
        }

        // Delete the redirect transient
        delete_transient('_zippy_activation_redirect');

        wp_safe_redirect(admin_url('index.php?page=zippy-getting-started'));
        exit;
    }

    public function clearLicenseTransients($value)
    {
        delete_option('zippy_license_check');
        delete_option('zippy_license');
        
        return $value;
    }

    public function isActive()
    {
        $license = $this->getLicense();

        return is_array($license) && isset($license['status']) && $license['status'] == 'valid';
    }

    public function removeFeaturedImageMetabox()
    {
        $post_types = array();

        $args = array(
           'public'   => true
        );

        $output = 'names';
        $operator = 'and';

        $excluded = array('attachment', 'zippy_order', 'transaction');
        $post_types = get_post_types($args, $output);

        foreach ($post_types as $post_type) {
            remove_meta_box('postimagediv', $post_type, 'side');
        }
    }
}
