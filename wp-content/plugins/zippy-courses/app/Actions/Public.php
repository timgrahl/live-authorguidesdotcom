<?php

class ZippyCourses_Public_Actions
{
    public function __construct()
    {
        add_action( 'wp_footer', array($this, 'attribution'));
    }

    public function attribution()
    {
        $settings = get_option('zippy_customizer_styles', array());
        $attribution = isset($settings['attribution']) ? $settings['attribution'] : '1';

        if( $attribution ) :
            echo '<div class="zippy-attribution">Powered by <a href="https://zippycourses.com" target="_blank">Zippy Courses</a></div>';
        endif;
    }
}