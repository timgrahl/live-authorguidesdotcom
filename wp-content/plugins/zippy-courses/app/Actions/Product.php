<?php

class ZippyCourses_Product_Actions
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_action('wp_footer', array($this, 'conversionPixels'));
        add_action('save_post', array($this, 'uid'));
        add_action('save_post', array($this, 'checkForEmptyProduct'));
        add_action('template_redirect', array($this, 'redirectOwned'));
        add_action('template_redirect', array($this, 'redirectFree'), 9);
        add_action('init', array($this, 'registerListener'));
    }

    public function registerListener()
    {
        $listener = new ZippyCourses_ProductListener;
    }

    public function conversionPixels()
    {
        global $current_user;

        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        if ($student !== null && !current_user_can('edit_others_posts')) {
            $pixels_used = get_user_meta($student->getId(), '_zippy_product_conversion_pixels_triggered', true);
            $pixels_used = array_filter(explode(',', $pixels_used));
            
            $products    = $student->getProductsOwned();

            foreach ($products as $product_id) {
                if (!in_array($product_id, $pixels_used)) {
                    $pixel = get_post_meta($product_id, '_zippy_fb_conversion_pixel', true);
                    
                    if ($pixel) {
                        echo $pixel;
                    }

                    $pixels_used[] = $product_id;
                }
            }

            update_user_meta($student->getId(), '_zippy_product_conversion_pixels_triggered', implode(',', $pixels_used));
        }
    }

    public function redirectOwned()
    {
        global $post;

        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        if (!is_object($post) || $post->post_type != 'product' || $student === null || current_user_can('edit_others_posts')) {
            return;
        }

        $owned = $student->getProductsOwned();

        if (in_array($post->ID, $owned)) {
            $zippy->sessions->flashMessage(
                __("You already own that product! If you're interested in more, please visit the <a href=\"" . $zippy->core_pages->getUrl('course_directory') . "\">Course Directory</a>", ZippyCourses::TEXTDOMAIN),
                'success'
            );

            wp_redirect($zippy->core_pages->getUrl('dashboard'));
            exit;
        }
    }

    public function redirectFree()
    {
        global $post;

        $zippy = Zippy::instance();
        
        if (current_user_can('edit_others_posts') ||
            ! is_object($post) ||
            $post->post_status !== 'publish' ||
            (
                get_post_type() !== 'product' &&
                ! $zippy->core_pages->is($post->ID, 'buy')
            )
        ) {
            return;
        }

        $zippy = Zippy::instance();
        
        $product = $post->post_type == 'product'
            ? $zippy->make('product', array($post->ID))
            : $zippy->utilities->product->getProductByBuyQueryVar();

        if ($product === null) {
            return;
        }

        $is_active = get_post_meta($product->getId(), 'status', true) == 'active';

        if ($product->getType() == 'free'
            && $zippy->utilities->product->inLaunchWindow($product)
            && $is_active) {
            wp_redirect($zippy->core_pages->getUrl('register') . '?claim=' . $product->getUid());
            exit;
        }
    }

    public function uid($post_id)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $product = $zippy->make('product', array($post_id));

        return $product->getUid();
    }

    public function checkForEmptyProduct($post_id)
    {
        $zippy = Zippy::instance();

        $access = $zippy->utilities->getJsonMeta($post_id, 'access', true);
        if (!is_object($access) || !isset($access->mode)) {
            return;
        }

        $missing = false;

        if ($access->mode == 'product') {
            $missing = $access->tier === '';
            if ($missing) {
                $access->tier = 1;
                $access = json_encode($access);
                update_post_meta($post_id, 'access', $access);
            }
        }

        if ($access->mode == 'bundle') {
            $missing = true;

            foreach ($access->bundle as $bundle) {
                if (!empty($bundle->tiers)) {
                    $missing = false;
                    break;
                }
            }
        }

        if ($missing) {
            $zippy->sessions->flashAdminMessage(
                __('You forgot to set a Tier for your product! Assigning the "Basic" tier by default.', ZippyCourses::TEXTDOMAIN),
                'warning',
                1
            );
        }
    }
}
