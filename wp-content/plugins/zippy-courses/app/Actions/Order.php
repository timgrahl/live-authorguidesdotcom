<?php

class ZippyCourses_Order_Actions
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_action('update_post_metadata', array($this, 'statusChange'), 10, 5);
    }

    public function statusChange($meta_type, $object_id, $meta_key, $meta_value, $prev_value = '')
    {
        if (get_post_type($object_id) == 'zippy_order' && $meta_key == 'status') {
            $zippy = Zippy::instance();

            $val = get_post_meta($object_id, 'status', true);

            $order = $zippy->make('order');
            $order->build($object_id);
            $order->setStatus($meta_value);

            $zippy->events->fire(new ZippyCourses_OrderStatusChange_Event($order, $val, $meta_value));
        }
    }
}
