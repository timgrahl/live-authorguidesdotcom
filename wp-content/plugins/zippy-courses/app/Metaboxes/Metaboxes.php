<?php

class ZippyCourses_Metaboxes
{
    private $directory;

    public function __construct()
    {
        $this->directory = ZippyCourses::$path . 'assets/views/metaboxes/';
    }

    public function build()
    {
        $zippy = Zippy::instance();

        $metaboxes = $this->fetch();
        
        foreach ($metaboxes as $mb) {
            $metabox = $zippy->make('metabox', array('id' => $mb['id'], 'args' => $mb));
            $zippy->metaboxes->add($metabox);
        }
    }

    private function fetch()
    {
        $files = $this->getFileList();
        $metaboxes = array();

        foreach ($files as $file) {
            $data = get_file_data($file, $this->getDefaultHeaders());
            $data['view'] = $file;
            $metaboxes[] = $data;
        }

        return $this->sort($metaboxes);
    }

    private function getDefaultHeaders()
    {
        return array(
            'id' => 'Id',
            'title' => 'Name',
            'help' => 'Help',
            'vars' => 'Fields',
            'data' => 'Data',
            'context' => 'Context',
            'priority' => 'Priority',
            'order' => 'Order',
            'post_types' => 'Post Types',
            'type' => 'Type',
            'service' => 'Service',
            'version' => 'Version'
        );
    }

    public function getFileList()
    {
        $files = array_diff(scandir($this->directory), array('..', '.'));

        foreach ($files as $key => &$file) {
            if (!is_dir($this->directory . $file)) {
                $file = $this->directory . $file;
            } else {
                unset($files[$key]);
            }
        }

        $metaboxes = apply_filters('zippy_metaboxes', array_values($files));

        return $metaboxes;
    }

    private function validateFile($file)
    {
        return true;
    }

    private function validateFiles(array $files)
    {
        return array_values($files);
    }

    private function sort($metaboxes)
    {

        foreach ($metaboxes as &$mb) {
            if ($mb['order'] === '' || !is_numeric($mb['order'])) {
                $mb['order'] = 10;
            }
            
            $mb['order'] = (int) $mb['order'];
        }

        usort($metaboxes, array($this, 'sortByOrder'));

        return $metaboxes;
    }

    private function sortByOrder($a, $b)
    {
        return $a['order'] - $b['order'];
    }
}
