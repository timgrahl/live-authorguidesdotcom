<?php

class ZippyCourses_MetaboxRegions extends Zippy_Repository
{
    public function __construct()
    {
        $this->addValidType('Zippy_MetaboxRegion');
        
        $this->adminDashboardRegions();
        $this->studentRegions();

        $this->courseUnitsAndLessonsRegions();
        $this->coursePublicDetailsRegions();
        $this->courseAdditionalContentRegions();
        $this->courseEmailListsRegions();
        $this->courseSettingsRegions();

        $this->productEmailListsRegions();
        $this->productLaunchWindowsRegions();
        $this->productAdvancedRegions();
        $this->productConfirmationMessagesRegions();

        $this->quizResultsRegions();

        $this->hooks();
    }

    private function hooks()
    {
        add_action('dbx_post_sidebar', array($this, 'render'));
    }

    public function register()
    {
        foreach ($this->all() as $region) {
            $region->register();
        }
    }

    public function render()
    {
        global $post;

        $zippy = Zippy::instance();

        foreach ($this->all() as $region) {
            if (is_object($post) && in_array($post->post_type, $region->getPostTypes())) {
                $region->render();
            }

            if (!is_object($post)) {
                $screen = get_current_screen();

                $screen_id = $screen->id;

                foreach ($region->getPostTypes() as $post_type) {
                    $substring  = '_page_' . $post_type;
                    $valid      = $zippy->utilities->stringEndsWith($screen_id, $substring);

                    if ($valid) {
                        $region->render();
                    }
                }
            }
        }
    }

    public function adminDashboardRegions()
    {
        $region = new Zippy_MetaboxRegion('zippy_admin_dashboard_region', array('zippy-courses'));
            $region->addSection('zippy_admin_dashboard_left');
            $region->addSection('zippy_admin_dashboard_right');

        $this->add($region);
    }

    public function studentRegions()
    {
        $region = new Zippy_MetaboxRegion('zippy_student_region', array('zippy-student'));
            $region->addSection('zippy_student_top');
            $region->addSection('zippy_student_left');
            $region->addSection('zippy_student_right');

        $this->add($region);
    }

    public function courseUnitsAndLessonsRegions()
    {
        $region = new Zippy_MetaboxRegion('zippy_course_entries_region', array('course'));

        $region->addSection('zippy_course_entries_main');
        $region->addSection('zippy_course_entries_side');

        $this->add($region);
    }

    public function coursePublicDetailsRegions()
    {
        $region = new Zippy_MetaboxRegion('zippy_course_public_details_region', array('course'));

        $region->addSection('zippy_course_public_details_top');
        $region->addSection('zippy_course_public_details_main');
        $region->addSection('zippy_course_public_details_side');

        $this->add($region);
    }

    public function courseAdditionalContentRegions()
    {
        $region = new Zippy_MetaboxRegion('zippy_course_additional_content_region', array('course'));

        $region->addSection('zippy_course_additional_content_main');
        $region->addSection('zippy_course_additional_content_side');

        $this->add($region);
    }

    public function courseEmailListsRegions()
    {
        $region = new Zippy_MetaboxRegion('zippy_course_email_lists_region', array('course'));

        $region->addSection('zippy_course_email_lists_main');
        $region->addSection('zippy_course_email_lists_side');

        $this->add($region);
    }


    public function courseSettingsRegions()
    {
        $region = new Zippy_MetaboxRegion('zippy_course_settings_region', array('course'));

        $region->addSection('zippy_course_settings_main');
        $region->addSection('zippy_course_settings_side');

        $this->add($region);
    }

    public function productEmailListsRegions()
    {
         $region = new Zippy_MetaboxRegion('zippy_product_email_lists_region', array('product'));

        $region->addSection('zippy_product_email_lists_main');
        $region->addSection('zippy_product_email_lists_side');

        $this->add($region);
    }

    public function productLaunchWindowsRegions()
    {
        $region = new Zippy_MetaboxRegion('zippy_product_launch_windows_region', array('product'));

        $region->addSection('zippy_product_launch_windows_main');
        $region->addSection('zippy_product_launch_windows_side');

        $this->add($region);
    }

    public function productAdvancedRegions()
    {
        $region = new Zippy_MetaboxRegion('zippy_product_advanced_region', array('product'));

        $region->addSection('zippy_product_advanced_main');
        $region->addSection('zippy_product_advanced_side');

        $this->add($region);
    }

    public function productConfirmationMessagesRegions()
    {
        $region = new Zippy_MetaboxRegion('zippy_product_confirmation_messages_region', array('product'));
        $region->addSection('zippy_product_confirmation_messages_main');
        $region->addSection('zippy_product_confirmation_messages_side');

        $this->add($region);
    }

    public function quizResultsRegions()
    {
        $region = new Zippy_MetaboxRegion('zippy_quiz_results_region', array('quiz'));

        $region->addSection('zippy_quiz_results_main');
        $region->addSection('zippy_quiz_results_side');

        $this->add($region);
    }
}
