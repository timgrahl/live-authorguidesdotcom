<?php

/**
 * This cache is used to store information that will be used in multiple places
 * throughout a single page load.  Most commonly, it is used to create and store data
 * that is useful in various places in the admin.
 *
 * This cache is not persisted in the database, it is only meant as an optimization
 * to reduce the number of database calls during a page load.
 */
class ZippyCourses_Cache
{
    public function __construct()
    {
        $zippy = Zippy::instance();

        $this->setup();

        add_action('init', array($this, 'buildInit'), 1);
        add_action('admin_init', array($this, 'buildAdminInit'), 1);
        add_action('add_meta_boxes', array($this, 'buildAddMetaBoxes'), 1);
        add_action('wp', array($this, 'buildWp'), 1);
        add_action('shutdown', array($this, 'persistStudentCache'));
    }

    public function setup()
    {
        $zippy = Zippy::instance();
        
        $zippy->cache->set('middleware', array());
    }

    public function buildInit()
    {

    }

    public function buildAddMetaBoxes()
    {
        $this->buildOrder();
        $this->buildTransaction();
    }

    public function buildAdminInit()
    {
    }

    public function buildWp()
    {
    }

    private function buildOrder()
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || $post->post_type !== 'zippy_order') {
            return;
        }

        $order = $zippy->make('order');
        $order->build($post->ID);
        $order->transactions->fetchByMeta('order_id', $post->ID);

        $zippy->cache->set('order', $order);
    }

    private function buildTransaction()
    {
        global $post;

        $zippy = Zippy::instance();

        if (!is_object($post) || $post->post_type !== 'transaction') {
            return;
        }

        $txn = $zippy->make('transaction');
        $txn->build($post->ID);

        $zippy->cache->set('transaction', $txn);
    }

    public function persistStudentCache()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        if ($student) {
            // $student->persistMiddlewareCache();
            $student->persistMiddlewareRulesCache();
        }
    }
}
