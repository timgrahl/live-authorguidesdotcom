<?php

class ZippyCourses_ZippyCourses_Exporter
{
    /** @var array A list of all of the post types that we want to export */
    protected $post_types = array(
        'course', 'unit', 'lesson', 'zippy_order', 'payment', 'product', 'quiz', 'quiz_question'
    );

    /** @var array A list of fields we don't want to export */
    protected $excluded = array(
        'post_attributes' => array(
            'ID', 'guid', 'post_author', 'to_ping', 'ping_status', 'pinged', 'menu_order', 'post_content_filtered'
        ),
        'post_meta' => array(
            '_edit_lock', '_edit_last'
        ),
        'comment_attributes' => array(
            'comment_ID'
        ),
        'user_meta' => array(
            'wp_user-', 'admin_', 'rich_editing', 'screen_', '_last_login', '_last_active', 'dismissed_', 'show_admin_bar_front', 'wp_capabilities', 'session_', 'use_ssl', 'comment_shortcuts'
        )
    );

    public function __construct()
    {
        add_action('admin_post_export_zippy_courses', array( $this, 'downloadZippyCoursesJson'));
        add_action('admin_post_export_zippy_student_csv', array( $this, 'downloadStudentsCsv'));
        add_action('admin_post_export_zippy_sample_student_csv', array( $this, 'downloadSampleStudentsCsv'));
        add_action('admin_post_export_zippy_orders_csv', array( $this, 'downloadOrdersCsv'));

    }

    public function downloadZippyCoursesJson()
    {
        // do whatever you do to populate $text, then...
        header('Content-Type: application/json');
        header('Content-Disposition: attachment; filename="zippy-courses.json"');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

        echo $this->exportJson();

        exit;
    }

    public function downloadStudentsCsv()
    {
        // do whatever you do to populate $text, then...
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="zippy-students.csv"');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

        $type = $_REQUEST['type'];
        $ids  = explode(',', $_REQUEST['ids']);

        echo $this->exportCsv($type, $ids);

        exit;
    }

    public function downloadOrdersCsv()
    {
        $ids  = !empty($_REQUEST['ids']) ? explode(',', $_REQUEST['ids']) : array();
        $start_date = $_REQUEST['start_date'] != 'NaN' ? $_REQUEST['start_date'] : null;
        $end_date = $_REQUEST['end_date'] != 'NaN' ? $_REQUEST['end_date'] : null;

        $this->exportOrdersCsv($ids, $start_date, $end_date);
    }

    public function downloadSampleStudentsCsv()
    {
        $type = '';
        $ids = '';
        $this->exportSampleCsv($type, $ids);
    }

    /**
     * Export all of the users from WordPress users table for use in the Zippy Courses importer
     *
     * @todo        Restrict this to students, not just all users
     *
     * @since       0.9.19
     * @param       int|string  $user_id    WordPress User ID
     * @return      array       An array of user objects and their meta data
     */
    protected function _exportUsers()
    {
        global $wpdb;

        $output = array();
        
        $user_ids = $wpdb->get_col("SELECT ID FROM $wpdb->users");

        foreach ($user_ids as $user_id) {
            // Skip non student
            if (user_can($user_id, 'edit_others_posts')) {
                continue;
            }

            $user = $wpdb->get_row("SELECT * FROM $wpdb->users WHERE ID = $user_id");
            $user->meta = $this->_exportMeta($user_id, 'user');
                
            $output[] = $user;
        }

        return $output;
    }

    /**
     * Export all of type of post from WordPress posts table for use in the Zippy Courses importer
     *
     * @since       0.9.19
     * @param       int|string  $user_id    WordPress Post ID
     * @return      array       An array of post objects and their meta data
     */
    protected function _exportPosts()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $content_ids = $zippy->utilities->entry->getAllProtectedEntryIds();

        $output = array();

        $post_type_list = '';
        foreach ($this->post_types as $post_type) {
            $post_type_list .= '"' . $post_type . '",';
        }

        $post_type_list = rtrim($post_type_list, ',');
        
        $ids = $wpdb->get_col(
            $wpdb->prepare(
                "SELECT ID FROM $wpdb->posts WHERE post_type IN ($post_type_list) AND post_status != %s",
                'auto-draft'
            )
        );

        $ids = array_map('intval', $ids);
        $ids = array_unique(array_merge($ids, $content_ids));

        foreach ($ids as $id) {
            $obj = get_post($id);
            $obj->meta = $this->_exportMeta($id);
            $obj->meta->import_author = $obj->post_author;

            foreach ($this->excluded['post_attributes'] as $ea) {
                unset($obj->{$ea});
            }

            if (!isset($output[$obj->post_type])) {
                $output[$obj->post_type] = array();
            }

            $output[$obj->post_type][] = $obj;
        }
           
        return $output;
    }

    /**
     * Export the meta we want, while excluding the pieces that we don't want.
     *
     * We are using this instead of the built in get_{post|user}_meta because it is more
     * convenient to export raw, serialized meta data than having Wordpress
     * unserialize it.  This also helps in the JSON import.
     *
     * @since  0.9.19
     *
     * @param  int|string   $obj_id     WordPress object ID
     * @param  string       $type       Object type, should be post or user
     *
     * @return stdClass     Object with meta key/value pairs as properties
     */
    protected function _exportMeta($obj_id, $type = 'post')
    {

        global $wpdb;
        
        $table = $wpdb->prefix . $type . 'meta';
        $sql = "SELECT meta_key, meta_value FROM $table WHERE {$type}_id = '%s'";
        $sql = sprintf($sql, $obj_id);
        
        $results = $wpdb->get_results($sql, ARRAY_A);
        
        $output = array();

        foreach ($results as $row) {
            extract($row);

            $exclude = false;
            if (isset($this->excluded[$type . '_meta'])) {
                foreach ($this->excluded[$type.'_meta'] as $mk) {
                    if (strpos($mk, $meta_key) !== false) {
                        $exclude = true;
                        break;
                    }
                }
            }

            if ($exclude) {
                continue;
            }

            if (isset($output[ $meta_key ])) {
                if (!is_array($output[ $meta_key ])) {
                    $output[$meta_key] = array($output[ $meta_key ]);
                }
                $output[ $meta_key ][] = $meta_value;
            } else {
                $output[ $meta_key ] = $meta_value;
            }
        }
        
        $meta = new stdClass;

        foreach ($output as $k => $v) {
            $meta->$k = $v;
        }

        $meta->import_id = $obj_id;

        if ($type == 'post') {
            $meta->import_author = $wpdb->get_var("SELECT post_author FROM $wpdb->posts WHERE ID = '$obj_id'");
        }

        return $meta;

    }

    /**
     * Export the data to a .json file
     * @param  ZippyCourse
     * @return void
     */
    public function _createFile($data)
    {

        $file = __DIR__ . '/zippy-courses-export.json';
        // Write the contents back to the fil
        file_put_contents($file, $data);
    }

    protected function _exportComments()
    {

        global $wpdb;

        $output = array();

        $get_comments_list = array();
        foreach ($this->data['posts'] as $type) {
            foreach ($type as $p) {
                $get_comments_list[] = $p->meta->import_id;
            }
        }

        $get_comments_list = implode(',', $get_comments_list);

        $sql = "SELECT * FROM $wpdb->comments WHERE comment_post_ID IN ($get_comments_list)";
        $comments = $wpdb->get_results($sql);

        foreach ($comments as $comment) {
            $comment->meta = $this->_exportMeta($comment->comment_ID, 'comment');
                $comment->meta->import_author = $comment->user_id;
                $comment->meta->import_id = $comment->comment_ID;
            
            foreach ($this->excluded['comment_attributes'] as $ea) {
                unset($comment->{$ea});
            }

            $output[] = $comment;
        }
        return $output;
    }

    protected function _exportAttachments()
    {
        global $wpdb;

        $upload_dir = wp_upload_dir();

        $post_list = array();
        foreach ($this->data['posts'] as $type) {
            foreach ($type as $p) {
                $post_list[] = $p->meta->import_id;
            }
        }

        $post_list = implode(',', $post_list);

        $sql = $wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_type = %s AND post_parent IN ($post_list)", 'attachment');
        $attachments = $wpdb->get_results($sql);
        $output = array();

        foreach ($attachments as $key => $attachment) {
            if (!wp_attachment_is_image($attachment->ID)) {
                unset($attachments[$key]);
            } else {
                $attachment->meta = $this->_exportMeta($attachment->ID);
                $attachment->meta->import_id = $attachment->ID;
                $attachment->meta->import_parent_id = $attachment->post_parent;

                $metadata = maybe_unserialize($attachment->meta->{'_wp_attachment_metadata'});

                $baseurl = $upload_dir['url'] . '/';
                $basename = basename($metadata['file']);
                
                $files = array($baseurl . $basename);

                if (isset($metadata['sizes']) && is_array($metadata['sizes'])) {
                    foreach ($metadata['sizes'] as $size) {
                        $files[] = str_replace($basename, $size['file'], $files[0]);
                    }
                }
                
                $attachment->meta->import_files = serialize($files);

                $output[] = $attachment;
            }
        }

        return $output;

    }

    /**
     * Get the data in the exporter
     *
     * @since 0.9.19
     * @param  string       $format Either 'raw' or 'json'
     * @return string|array         Either a JSON encoded string or the raw $data array
     */
    public function export()
    {
        $this->data = array(
            'export_meta' => array(
                'type' => 'zippy-courses'
            ),
            'posts' => $this->_exportPosts(),
            'comments' => array(),
            'users' => $this->_exportUsers()
        );

        $this->data['comments']     = $this->_exportComments();
        $this->data['attachments']  = $this->_exportAttachments();

        $encoded_data = str_replace('<', '\u003C', $this->data);
        $encoded_data = str_replace('>', '\u003E', $this->data);
        $encoded_data = str_replace('<', '\u0022', $this->data);
        
        $output = $this->data;
         
        return $output;
    }

    public function exportJson()
    {
        return json_encode($this->export());
    }

    public function exportCsv($type, $ids)
    {
        $zippy = Zippy::instance();

        $student_ids = array();

        if ($type == 'product') {
            $student_ids = $zippy->utilities->student->getStudentsWithProducts($ids);
        }

        if ($type == 'course') {
            $student_ids = $zippy->utilities->student->getStudentsWithCourses($ids);
        }

        // If the "No Courses" option is selected, get in users without any courses
        if (in_array('0', $ids)) {
            $student_ids = array_merge($student_ids, $zippy->utilities->student->getStudentsWithNoCourses());
        }

        $data = array(
            apply_filters(
                'zippy_courses_exports_student_fields',
                array('ID', 'FirstName', 'LastName', 'Email', 'Username')
            )
        );

        foreach ($student_ids as $student_id) {
            if ($student_id) {
                $userdata = get_userdata($student_id);

                // Filter out any administrator accounts from the export
                if (user_can($userdata->ID, 'edit_others_posts')) {
                    continue;
                }

                if ($userdata) {
                    $data[] = apply_filters(
                        'zippy_courses_exports_student_values',
                        array(
                            $userdata->ID,
                            $userdata->first_name,
                            $userdata->last_name,
                            $userdata->user_email,
                            $userdata->user_login
                        ),
                        $userdata
                    );
                }
            }
        }
        
        $csv = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');

        foreach ($data as $fields) {
            fputcsv($csv, $fields);
        }

        rewind($csv);

        // put it all in a variable
        $output = stream_get_contents($csv);

        // do whatever you do to populate $text, then...
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="zippy-students.csv"');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        
        echo $output;
        
        exit;
    }

    public function exportOrdersCsv($ids, $start_date, $end_date)
    {
        $zippy = Zippy::instance();

        // TODO: Add information related to subscriptions

        $orders = $zippy->utilities->orders->getOrderIds($start_date, $end_date);
        $data = array(
            apply_filters(
                'zippy_courses_exports_order_fields',
                array( 'ID', 'Date', 'User', 'Product', 'Amount', 'Type')
            )
        );
        foreach ($orders as $order_id) {
            if ($order_id) {
                $order = $zippy->make('order');
                $order->build($order_id);
                $product = $order->getProduct();

                if (!$product) {
                    continue;
                }

                if (!empty($ids) && !in_array($product->getId(), $ids)) {
                    continue;
                }

                $owner = $order->getOwner();
                $owner_data = get_userdata($owner);
                $first_name = !empty($owner_data->first_name) ? $owner_data->first_name : null;
                $last_name = !empty($owner_data->last_name) ? $owner_data->last_name : null;
                $owner_name = $first_name . ' ' . $last_name;

                $product_title = $product->getTitle();
                

                if ($order->id) {
                    $data[] = apply_filters(
                        'zippy_courses_exports_order_values',
                        array(
                            $order->getTitle(),
                            $order->getDate()->format('m/d/y H:i:s'),
                            $owner_name,
                            $product_title,
                            $order->getTotal(),
                            $order->getTypeLabel(),
                        ),
                        $order
                    );
                }
            }
        }
        
        $csv = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');

        foreach ($data as $fields) {
            fputcsv($csv, $fields);
        }

        rewind($csv);

        // put it all in a variable
        $output = stream_get_contents($csv);

        // do whatever you do to populate $text, then...
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="zippy-orders.csv"');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        
        echo $output;
        
        exit;
    }

    public function exportSampleCsv($type, $ids)
    {
        $zippy = Zippy::instance();

        $data = array(
            array( 'ID', 'FirstName', 'LastName', 'Email', 'Username' ),
            array( '123', 'Derek', 'Halpern', 'support@zippycourses.com', 'derek' ),
        );
        
        $csv = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');

        foreach ($data as $fields) {
            fputcsv($csv, $fields);
        }

        rewind($csv);

        // put it all in a variable
        $output = stream_get_contents($csv);

        // do whatever you do to populate $text, then...
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="zippy-students.csv"');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        
        echo $output;
        
        exit;
    }
}
