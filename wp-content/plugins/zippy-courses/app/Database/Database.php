<?php

/**
 * Setup and store the database table information requried by Zippy Courses
 *
 * @since   1.0.0
 */
class ZippyCourses_Database {

    protected $tables;

    public function __construct(Zippy_DatabaseTables $tables)
    {
        $this->tables = $tables;

        $this->prepareDefaults();
    }

    public function prepareDefaults()
    {
        global $wpdb;

        $old_activity_table = $wpdb->prefix . 'wpcs_user_activity';
        $sql                = $wpdb->prepare("SHOW TABLES LIKE %s", "%$old_activity_table%");
        $results            = $wpdb->get_results($sql);

        if (count($results) < 1) {
            $this->userActivityTable();
        }
    }

    /**
     * Create the user Activity Table
     * @return
     */
    public function userActivityTable()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $table = $zippy->make('database_table', array('user_activity'));
            
            $id = $table->addColumn('ID', 'bigint')
                ->setTypeParameters(20)
                ->setPrimaryKey(true)
                ->setAutoIncrement(true);

            $user_id = $table->addColumn('user_id', 'int')
                ->setTypeParameters(10)
                ->setKey(true)
                ->setDefault(0);

            $item_id = $table->addColumn('item_id', 'varchar')
                ->setTypeParameters(10)
                ->setDefault(0);

            $type = $table->addColumn('type', 'varchar')
                ->setTypeParameters(20)
                ->setKey(true)
                ->setDefault('');

            $create_time = $table->addColumn('create_time', 'timestamp')
                ->setDefault('CURRENT_TIMESTAMP');

            $duration = $table->addColumn('duration', 'int')
                ->setTypeParameters(10)
                ->setDefault(0);

        $this->tables->add($table);
    }
}
