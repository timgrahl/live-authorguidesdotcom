<?php

/**
 * Setup and store the database table information requried by Zippy Courses
 *
 * @since   1.0.0
 */
class ZippyCourses_Migration extends Zippy_DatabaseMigration {

    protected $tables;

    /**
     * IMPORTANT: The order of instantiating these classes is important, so be careful when re-ordering
     */
    public function __construct()
    {
        new ZippyCourses_Transaction_v1_0_0_MigrationStep;
        new ZippyCourses_Order_v1_0_0_MigrationStep;
        new ZippyCourses_Product_v1_0_0_MigrationStep;
        new ZippyCourses_Course_v1_0_0_MigrationStep;
        new ZippyCourses_Posts_v1_0_0_MigrationStep;
        new ZippyCourses_User_v1_0_0_MigrationStep;
    }

    public function buildSteps()
    {

    }
}
