<?php

/**
 * Setup and store the database table information requried by Zippy Courses
 *
 * @since   1.0.0
 */
class ZippyCourses_v1_0_0_DatabaseMigration extends Zippy_DatabaseMigration
{
    /**
     * Build the default steps for this migration
     *
     * IMPORTANT: The order of instantiating these classes is important, so be careful when re-ordering
     *
     * @since 1.0.0
     */
    public function buildSteps()
    {
        $this->steps->add(new ZippyCourses_v1_0_0_Settings_MigrationStep);
        $this->steps->add(new ZippyCourses_v1_0_0_Transaction_MigrationStep);
        $this->steps->add(new ZippyCourses_v1_0_0_Order_MigrationStep);
        $this->steps->add(new ZippyCourses_v1_0_0_Product_MigrationStep);
        $this->steps->add(new ZippyCourses_v1_0_0_Course_MigrationStep);
        $this->steps->add(new ZippyCourses_v1_0_0_Posts_MigrationStep);
        $this->steps->add(new ZippyCourses_v1_0_0_LandingPages_MigrationStep);
        $this->steps->add(new ZippyCourses_v1_0_0_Quiz_MigrationStep);
        $this->steps->add(new ZippyCourses_v1_0_0_QuizQuestions_MigrationStep);
        $this->steps->add(new ZippyCourses_v1_0_0_User_MigrationStep);
        $this->steps->add(new ZippyCourses_v1_0_0_Activity_MigrationStep);
    }
}
