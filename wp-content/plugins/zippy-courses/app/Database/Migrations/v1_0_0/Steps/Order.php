<?php

class ZippyCourses_v1_0_0_Order_MigrationStep extends Zippy_MigrationStep
{
    public $reads = 0;
    public $writes = 0;

    public function __construct()
    {
        $this->id = 'orders';
        $this->label = __('Orders', ZippyCourses::TEXTDOMAIN);
    }

    public function migrate()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        if ($this->isCompleted() || $count >= self::COUNT_LIMIT) {
            return;
        }

        if (($migrated = get_transient('zippy_v1_0_0_orders_migrated')) === false) {
            $migrated = array();
        }

        $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s", 'course_order');
        $ids = $wpdb->get_col($sql);

        $removable_keys = array(
            'order_details',
            'order_type',
            'order_uid',
            'order_status',
            'vendor_id',
            'vendor',
            'receipt_sent',
            'registration_reminder_sent',
        );

        foreach ($ids as $id) {
            if ($count >= self::COUNT_LIMIT) {
                break;
            }

            if (in_array($id, $migrated)) {
                continue;
            }

            $this->backupMeta($id);

            $this->_migrateDetails($id);
            $this->_migrateStatus($id);
            $this->_migrateProduct($id);

            $this->removeMeta($id, $removable_keys);

            $migrated[] = $id;
            $count++;
            
            set_transient('zippy_v1_0_0_orders_migrated', $migrated, MINUTE_IN_SECONDS * 10);

            $zippy->cache->set('zippy_upgrade_counter', $count);
        }

        if (count($migrated) == count($ids)) {
            $this->_changeToZippyOrder();
            $this->completeStep();
        }
        
        if ($count >= self::COUNT_LIMIT) {
            return;
        }
    }

    private function _changeToZippyOrder()
    {
        global $wpdb;

        $sql = $wpdb->prepare("UPDATE $wpdb->posts SET post_type = %s WHERE post_type = %s", 'zippy_order', 'course_order');

        $wpdb->query($sql);
    }

    private function _migrateProduct($post_id)
    {
        $order_details = (array) get_post_meta($post_id, 'order_details', true);
        $product = 0;

        if (isset($order_details['product'][0])) {
            $product = $order_details['product'][0];
        }

        update_post_meta($post_id, 'product', $product);

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateStatus($post_id)
    {
        global $wpdb;

        $status = 'complete';

        $order_details = (array) get_post_meta($post_id, 'order_details', true);
        $type = isset($order_details['type']) ? $order_details['type'] : 'single';

        if ($type !== 'single') {
            if ($type == 'subscription') {
                $status = 'active';
            } else {
                $recurring_table = $wpdb->prefix . 'wpcs_user_recurring_payment';
                $recurring_record = $wpdb->get_row($wpdb->prepare("SELECT * FROM $recurring_table WHERE order_id = %s", $post_id));

                if ($recurring_record) {
                    $total_payments = $recurring_record->total_payments;
                    $num_payments = $recurring_record->num_payments_made;

                    $status = $num_payments < $total_payments ? 'active' : 'complete';
                }
            }
        }

        $refund_status = get_post_meta($post_id, 'refund_status', true);

        if ($refund_status == '1') {
            $status = 'refund';
        }
        
        update_post_meta($post_id, 'status', $status);

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateDetails($post_id)
    {
        global $wpdb;

        $recurring_table = $wpdb->prefix . 'wpcs_user_recurring_payment';

        $order_details = (array) get_post_meta($post_id, 'order_details', true);

        $type = isset($order_details['type']) ? $order_details['type'] : 'single';
        $recurring = $type == 'subscription' || $type == 'payment-plan' || $type == 'payment_plan';
        $amount = 0;
        $num_payments = $type == 'single' ? 1 : 0;

        $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_parent = %s", $post_id);
        $payments = $wpdb->get_col($sql);

        $payment_id = count($payments) ? $payments[0] : false;

        if ($payment_id) {
            $payment_details = (array) get_post_meta($payment_id, 'payment_details', true);
            $amount = isset($payment_details['total']) ? $payment_details['total'] : 0;
        }

        $recurring_record = $wpdb->get_row($wpdb->prepare("SELECT * FROM $recurring_table WHERE order_id = %s", $post_id));

        if ($recurring_record) {
            $num_payments = $recurring_record->total_payments;
        }

        $details = array(
            'type' => $type,
            'recurring' => $recurring,
            'num_payments' => 0,
            'amount' => $amount
        );

        update_post_meta($post_id, 'details', true);

        // Clean it up
        $this->incrementCurrentStep();
    }

    public function analyze()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) ID FROM $wpdb->posts WHERE post_type = %s", 'course_order');
        $num_orders = $wpdb->get_var($sql);

        $this->_analyzeOrder($num_orders);
        $this->_analyzeDetails($num_orders);
        $this->_analyzeProduct($num_orders);
        $this->_analyzeStatus($num_orders);
    }

    private function _analyzeOrder($count)
    {
        $this->actions += $count;
    }

    private function _analyzeProduct($count)
    {
        $this->actions += $count;
    }

    private function _analyzeStatus($count)
    {
        $this->actions += $count;
    }

    private function _analyzeDetails($count)
    {
        global $wpdb;
        
        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->postmeta WHERE meta_key = %s", 'payment_details');
        $num_payments = $wpdb->get_var($sql);

        // Per Payment
        $this->actions += $num_payments;
        
        // Per Order
        $this->actions += $count;
    }
}
