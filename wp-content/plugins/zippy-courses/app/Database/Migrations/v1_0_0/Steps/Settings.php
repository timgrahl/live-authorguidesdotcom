<?php

/**
 * wpcs_license_settings    -> zippy_license_settings
 * zippy_course_settings -> zippy_course_settings
 * zippy_order_num     -> zippy_order_number
 * zippy_currency      -> zippy_payment_general_settings[currency]
 * course_pay_before_register -> zippy_payment_general_settings[payment_flow]
 * course_payment_method -> zippy_payment_general_settings[method]
 * wpcs_paypal -> zippy_paypal_payment_gateway_settings
 * zippy_ontraport     -> zippy_ontraport_payment_gateway_settings
 * zippy_getresponse   -> zippy_getresponse_email_integration_settings
 * zippy_constant_contact -> zippy_constantcontact_email_integration_settings[token]
 * zippy_campaign_monitor -> zippy_campaignmonitor_email_integration_settings[access_token]
 * wpcs_stripe -> zippy_stripe_payment_gateway_settings
 * wpcs_infusionsoft -> zippy_infusionsoft_payment_gateway_settings
 * zippy_ontraport -> zippy_ontraport_payment_gateway_settings
 * zp_authorizedotnet -> zippy_authorizedotnet_payment_gateway_settings
 * zp_clickbank -> zippy_clickbank_payment_gateway_settings
 * zp_oneshoppingcart -> zippy_oneshoppingcart_payment_gateway_settings
 * zippy_getresponse[api_key] -> zippy_getresponse_email_integration_settings[api_url]
 * zp_madmimi[username] -> zippy_madmimi_email_integration_settings[api_url]
 * wpcs_mailchimp[api_key] -> zippy_mailchimp_email_integration_settings[auth_code]
 * course_emails[support_email] -> zippy_system_emails_general_settings[sender_email]
 * course_emails[registration_title] -> zippy_system_emails_registration_settings[title]
 * zp_activecampaign[api_url] -> zippy_activecampaign_email_integration_settings[api_url]
 * zp_icontact[enabled] -> zippy_icontact_email_integration_settings[api_url]
 * wpcs_aweber[auth] -> zippy_aweber_email_integration_settings[auth_code]
 * zp_ifs_email[enabled] -> ...
 * zp_ontraport_email[enabled] -> ...
*/
class ZippyCourses_v1_0_0_Settings_MigrationStep extends Zippy_MigrationStep
{
    public $actions = 0;

    public function __construct()
    {
        $this->id = 'settings';
        $this->label = __('Settings', ZippyCourses::TEXTDOMAIN);
    }

    public function migrate()
    {
        $zippy = Zippy::instance();

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        if ($this->isCompleted() || $count >= self::COUNT_LIMIT) {
            return;
        }

        $this->backup();

        $settings = array(
            'license'           => '_migrateLicenseSettings',
            'order_number'      => '_migrateOrderNumber',
            'payment'           => '_migrateGeneralPaymentSettings',
            'paypal'            => '_migratePayPalSettings',
            'ontraport'         => '_migrateOntraportSettings',
            'getresponse'       => '_migrateGetResponseSettings',
            'constantcontact'   => '_migrateConstantContactSettings',
            'campaignmonitor'   => '_migrateCampaignMonitorSettings',
            'stripe'            => '_migrateStripeSettings',
            'infusionsoft'      => '_migrateInfusionsoftGatewaySettings',
            'authorizedotnet'   => '_migrateAuthorizeDotNetSettings',
            'clickbank'         => '_migrateClickBankSettings',
            'oneshoppingcart'   => '_migrateOneShoppingCartSettings',
            'madmimi'           => '_migrateMadMimiSettings',
            'mailchimp'         => '_migrateMailChimpSettings',
            'activecampaign'    => '_migrateActiveCampaignSettings',
            'icontact'          => '_migrateIContactSettings',
            'aweber'            => '_migrateAweberSettings',
            'emails'            => '_migrateSystemEmails',
            'core_pages'        => '_migrateCorePages',
            'sidebars_widgets'  => '_migrateWidgets'
        );

        if (($migrated = get_transient('zippy_v1_0_0_settings_migrated')) === false) {
            $migrated = array();
        }
        
        foreach ($settings as $id => $func) {
            if (in_array($id, $migrated) || $count >= self::COUNT_LIMIT) {
                continue;
            }

            call_user_func(array($this, $func));
            
            $migrated[] = $id;
            $count++;

            set_transient('zippy_v1_0_0_settings_migrated', $migrated, MINUTE_IN_SECONDS * 10);

            $zippy->cache->set('zippy_upgrade_counter', $count);
        }

        if (count($migrated) == count($settings)) {
            $this->completeStep();
        }
        
        if ($count >= self::COUNT_LIMIT) {
            return;
        }
    }

    public function analyze()
    {
        $this->_analyzeLicenseSettings();
        $this->_analyzeOrderNumber();
        $this->_analyzeCorePages();
        $this->_analyzeGeneralPaymentSettings();
        $this->_analyzePayPalSettings();
        $this->_analyzeOntraportSettings();
        $this->_analyzeGetResponseSettings();
        $this->_analyzeConstantContactSettings();
        $this->_analyzeCampaignMonitorSettings();
        $this->_analyzeStripeSettings();
        $this->_analyzeInfusionsoftGatewaySettings();
        $this->_analyzeAuthorizeDotNetSettings();
        $this->_analyzeClickBankSettings();
        $this->_analyzeOneShoppingCartSettings();
        $this->_analyzeMadMimiSettings();
        $this->_analyzeMailChimpSettings();
        $this->_analyzeActiveCampaignSettings();
        $this->_analyzeIContactSettings();
        $this->_analyzeAweberSettings();
    }

    private function backup()
    {
        $keys = array_unique(array('wpcs_license_settings', 'zippy_course_settings', 'zippy_order_num', 'zippy_currency', 'course_pay_before_register', 'course_payment_method', 'wpcs_paypal', 'zippy_ontraport', 'zippy_getresponse', 'zippy_constant_contact', 'zippy_campaign_monitor', 'wpcs_stripe', 'wpcs_infusionsoft', 'zippy_ontraport', 'zp_authorizedotnet', 'zp_clickbank', 'zp_oneshoppingcart', 'zippy_getresponse', 'zp_madmimi', 'wpcs_mailchimp', 'course_emails', 'zp_activecampaign', 'zp_icontact', 'wpcs_aweber', 'zp_ifs_email', 'zp_ontraport_email', 'wpcs_core_pages', 'sidebar_widgets', 'widget_zippy-widget-lesson-complete', 'widget_zippy-widget-your-courses', 'widget_zippy-widget-login-account', 'widget-zippy-widget-course-navigation'));

        $backup = array();

        foreach ($keys as $key) {
            $backup[$key] = get_option($key);
        }

        add_option('_zippy_upgrade_v1_0_0_settings_backup', $backup);
    }

    private function _simpleMigration($new_key, $old_key)
    {
        global $wpdb;

        delete_option($new_key);
        
        $sql = $wpdb->prepare(
            "UPDATE $wpdb->options SET option_name = %s WHERE option_name=%s",
            $new_key,
            $old_key
        );

        $wpdb->query($sql);

        $this->incrementCurrentStep();

        delete_option($old_key);
    }

    private function _analyzeLicenseSettings()
    {
        $this->actions++;
    }

    private function _migrateLicenseSettings()
    {
        $this->_simpleMigration('zippy_license_settings', 'wpcs_license_settings');
    }

    private function _analyzeOrderNumber()
    {
        $this->actions++;
    }

    private function _analyzeCorePages()
    {
        $this->actions++;
    }

    private function _migrateOrderNumber()
    {
        $this->_simpleMigration('zippy_order_number', 'zippy_order_num');
    }

    private function _migrateGeneralPaymentSettings()
    {
        $flow       = get_option('course_pay_before_register', '1');
        $method     = get_option('course_payment_method', 'paypal');
        $currency   = get_option('zippy_currency', 'USD');

        $settings = array(
            'payment_flow'  => $flow,
            'method'        => $method,
            'currency'      => $currency
        );

        update_option('zippy_payment_general_settings', $settings);

        delete_option('zippy_currency');
        delete_option('course_payment_method');
        delete_option('course_pay_before_register');
    }

    private function _analyzeGeneralPaymentSettings()
    {
        $this->actions++;
    }

    private function _migratePayPalSettings()
    {
        $old = get_option(
            'wpcs_paypal',
            array(
                'email' => '',
                'pdt_enabled' => '',
                'pdt_token' => ''
            )
        );

        $settings = array(
            'email'     => isset($old['email']) ? $old['email'] : '',
            'pdt_token' => isset($old['pdt_token']) ? $old['pdt_token'] : '',
            'test_mode' => isset($old['test_mode']) ? 1 : 0
        );

        update_option('zippy_paypal_payment_gateway_settings', $settings);
        
        delete_option('wpcs_paypal');
    }

    private function _analyzePayPalSettings()
    {
        $this->actions++;
    }
    
    private function _migrateOntraportSettings()
    {
        $this->_simpleMigration('zippy_ontraport_payment_gateway_settings', 'zippy_ontraport');
    }

    private function _analyzeOntraportSettings()
    {
        $this->actions++;
    }

    private function _migrateGetResponseSettings()
    {
        $this->_simpleMigration('zippy_getresponse_email_integration_settings', 'zippy_getresponse');
    }

    private function _analyzeGetResponseSettings()
    {
        $this->actions++;
    }

    private function _migrateConstantContactSettings()
    {
        $this->_simpleMigration('zippy_constantcontact_email_integration_settings', 'zippy_constant_contact');
    }

    private function _analyzeConstantContactSettings()
    {
        $this->actions++;
    }

    private function _migrateCampaignMonitorSettings()
    {
        $old = get_option(
            'zippy_campaign_monitor',
            array(
                'token' => '',
                'refresh' => ''
            )
        );

        $settings = array(
            'enabled'       => isset($old['enabled']) ? $old['enabled'] : '0',
            'access_token'  => isset($old['token']) ? $old['token'] : '',
            'refresh_token' => isset($old['refresh']) ? $old['refresh'] : ''
        );

        update_option('zippy_campaignmonitor_email_integration_settings', $settings);
        
        delete_option('zippy_campaign_monitor');

        $this->incrementCurrentStep();
    }

    private function _analyzeCampaignMonitorSettings()
    {
        $this->actions++;
    }

    private function _migrateStripeSettings()
    {
        $old = get_option(
            'wpcs_stripe',
            array(
                'secret_key' => '',
                'publishable_key' => '',
                'test_mode' => ''
            )
        );

        $settings = array(
            'secret_key'        => isset($old['secret_key']) ? $old['secret_key'] : '',
            'publishable_key'   => isset($old['publishable_key']) ? $old['publishable_key'] : '',
            'test_mode'         => (int) isset($old['test_mode']) && $old['test_mode'] == 'test',
            'paypal'            => '0'
        );

        update_option('zippy_stripe_payment_gateway_settings', $settings);
        
        delete_option('wpcs_stripe');

        $this->incrementCurrentStep();
    }

    private function _analyzeStripeSettings()
    {
        $this->actions++;
    }

    private function _migrateInfusionsoftGatewaySettings()
    {
        $this->_simpleMigration('zippy_infusionsoft_payment_gateway_settings', 'wpcs_infusionsoft');
    }

    private function _analyzeInfusionsoftGatewaySettings()
    {
        $this->actions++;
    }

    private function _migrateAuthorizeDotNetSettings()
    {
        $this->_simpleMigration('zippy_authorizedotnet_payment_gateway_settings', 'zp_authorizedotnet');
    }

    private function _analyzeAuthorizeDotNetSettings()
    {
        $this->actions++;
    }

    private function _migrateClickBankSettings()
    {
        $this->_simpleMigration('zippy_clickbank_payment_gateway_settings', 'zp_clickbank');
    }

    private function _analyzeClickBankSettings()
    {
        $this->actions++;
    }

    private function _migrateOneShoppingCartSettings()
    {
        global $wpdb;

        $old = get_option(
            'zp_oneshoppingcart',
            array(
                'merchant_key' => '',
                'merchant_id' => ''
            )
        );

        $settings = array(
            'api_key'       => isset($old['merchant_key']) ? $old['merchant_key'] : '',
            'merchant_id'   => isset($old['merchant_id']) ? $old['merchant_id'] : ''
        );

        update_option('zippy_oneshoppingcart_payment_gateway_settings', $settings);
        
        delete_option('zp_oneshoppingcart');

        $this->incrementCurrentStep();
    }

    private function _analyzeOneShoppingCartSettings()
    {
        $this->actions++;
    }

    private function _migrateMadMimiSettings()
    {
        $this->_simpleMigration('zippy_madmimi_email_integration_settings', 'zp_madmimi');
    }

    private function _analyzeMadMimiSettings()
    {
        $this->actions++;
    }

    private function _migrateMailChimpSettings()
    {
        $old = get_option(
            'wpcs_mailchimp',
            array(
                'api_key' => '',
                'optin_type' => '1'
            )
        );

        $settings = array(
            'enabled'           => isset($old['enabled']) ? $old['enabled'] : '0',
            'api_key'   => isset($old['api_key']) ? $old['api_key'] : '',
            'double_optin'         => (int) isset($old['optin_type']) && $old['optin_type'] == 0
        );

        update_option('zippy_mailchimp_email_integration_settings', $settings);
        
        delete_option('wpcs_mailchimp');

        $this->incrementCurrentStep();
    }

    private function _analyzeMailChimpSettings()
    {
        $this->actions++;
    }

    private function _migrateActiveCampaignSettings()
    {
        $this->_simpleMigration('zippy_activecampaign_email_integration_settings', 'zp_activecampaign');
    }

    private function _analyzeActiveCampaignSettings()
    {
        $this->actions++;
    }

    private function _migrateIContactSettings()
    {
        $this->_simpleMigration('zippy_icontact_email_integration_settings', 'zp_icontact');
    }

    private function _analyzeIContactSettings()
    {
        $this->actions++;
    }

    private function _migrateAweberSettings()
    {
        global $wpdb;

        // a:6:{s:7:"enabled";s:1:"1";s:4:"auth";s:139:"AzoJoBMLsDwrsARvUs2vdpCS|iMvVkrxZ7UPJCb8xSyPfpQYwOp6XeGyWheoEZqih|AqJKsDMBdMsk2f9s8W4N47WT|YmWMXATqeKl0Or7EFqw0TgglOBlMQpJZk1QBySMN|okr0x8|";s:12:"consumer_key";s:24:"AzoJoBMLsDwrsARvUs2vdpCS";s:15:"consumer_secret";s:40:"iMvVkrxZ7UPJCb8xSyPfpQYwOp6XeGyWheoEZqih";s:13:"access_secret";s:40:"p6s6FKMsG9upKjnL7f7QSvJwiqsU9rQpYt85RlbX";s:10:"access_key";s:24:"AgqTwklKbXEcE2huR84C7Yh4";}
        // 
        $old = get_option('wpcs_aweber', array(
            'auth'              => '',
            'consumer_key'      => '',
            'consumer_secret'   => '',
            'access_key'        => '',
            'access_secret'     => ''
        ));

        $settings = array(
            'enabled'           => isset($old['enabled']) ? $old['enabled'] : '0',
            'auth_code'         => isset($old['auth']) ? $old['auth'] : '',
            'consumer_key'      => isset($old['consumer_key']) ? $old['consumer_key'] : '',
            'consumer_secret'   => isset($old['consumer_secret']) ? $old['consumer_secret'] : '',
            'access_key'        => isset($old['access_key']) ? $old['access_key'] : '',
            'access_secret'     => isset($old['access_secret']) ? $old['access_secret'] : ''
        );

        update_option('zippy_aweber_email_integration_settings', $settings);
        
        delete_option('wpcs_aweber', $settings);

        $this->incrementCurrentStep();
    }

    private function _analyzeAweberSettings()
    {
        $this->actions++;
    }

    private function _migrateSystemEmails()
    {
        $emails = get_option('course_emails', array());

        $details = array(
            'sender_email' =>isset($emails['support_email']) ? $emails['support_email'] : '',
            'sender_name' => isset($emails['email_from']) ? $emails['email_from'] : '',
        );
        update_option('zippy_system_emails_general_settings', $details);

        $registration = array(
            'title' => isset($emails['registration_title']) ? $emails['registration_title'] : '',
            'content' => isset($emails['registration']) ? $emails['registration'] : '',
            'enabled' => isset($emails['registration_send']) && $emails['registration_send'] == 1 ? array(1) : ''
        );
        update_option('zippy_system_emails_registration_settings', $registration);

        $forgot_password = array(
            'title' => isset($emails['forgot_password_title']) ? $emails['forgot_password_title'] : '',
            'content' => isset($emails['forgot_password']) ? $emails['forgot_password'] : '',
            'enabled' => isset($emails['forgot_password_send']) && $emails['forgot_password_send'] == 1 ? array(1) : ''
        );

        update_option('zippy_system_emails_forgot_password_settings', $forgot_password);

        $purchase_receipt = array(
            'title' => isset($emails['purchase_title']) ? $emails['purchase_title'] : '',
            'content' => isset($emails['purchase']) ? $emails['purchase'] : '',
            'enabled' => isset($emails['purchase_send']) && $emails['purchase_send'] == 1 ? array(1) : ''
        );

        update_option('zippy_system_emails_purchase_receipt_settings', $purchase_receipt);

        $registration_reminder = array(
            'title' => isset($emails['registration_reminder_title']) ? $emails['registration_reminder_title'] : '',
            'content' => isset($emails['registration_reminder']) ? $emails['registration_reminder'] : '',
            'enabled' => isset($emails['registration_reminder_send']) && $emails['registration_reminder_send'] == 1 ? array(1) : ''
        );

        update_option('zippy_system_emails_registration_reminder_settings', $registration_reminder);

        $new_student = array(
            'title' => isset($emails['new_student_title']) ? $emails['new_student_title'] : '',
            'content' => isset($emails['new_student']) ? $emails['new_student'] : '',
            'enabled' => isset($emails['new_student_send']) && $emails['new_student_send'] == 1 ? array(1) : ''
        );

        update_option('zippy_system_emails_new_student_settings', $new_student);

        delete_option('course_emails');
        
        $this->incrementCurrentStep(6);
    }

    public function _migrateCorePages()
    {
        $old = get_option('wpcs_core_pages', array(
            'student_home' => '',
            'dashboard' => '',
            'login' => '',
            'logout' => '',
            'register' => '',
            'account' => '',
            'edit_account' => '',
            'order_history' => '',
            'buy' => '',
            'thank_you' => '',
            'forgot_password' => '',
            'reset_password' => '',
        ));

        $settings = array(
            'dashboard'         => isset($old['dashboard']) ? $old['dashboard'] : '',
            'login'             => isset($old['login']) ? $old['login'] : '',
            'logout'            => isset($old['logout']) ? $old['logout'] : '',
            'register'          => isset($old['register']) ? $old['register'] : '',
            'account'           => isset($old['account']) ? $old['account'] : '',
            'edit_account'      => isset($old['edit_account']) ? $old['edit_account'] : '',
            'buy'               => isset($old['buy']) ? $old['buy'] : '',
            'order_history'     => isset($old['order_history']) ? $old['order_history'] : '',
            'forgot_password'   => isset($old['forgot_password']) ? $old['forgot_password'] : '',
            'reset_password'    => isset($old['reset_password']) ? $old['reset_password'] : '',
            'thank_you'         => isset($old['thank_you']) ? $old['thank_you'] : '',
            'course_directory'  => isset($old['course_directory']) ? $old['course_directory'] : ''
        );
        
        update_option('zippy_core_pages', $settings);
        delete_option('wpcs_core_pages');

        $this->incrementCurrentStep();
    }

    public function _migrateWidgets()
    {
        global $wpdb;

        $zippy_widgets = array(
            'widget_zippy-widget-login-account'     => 'widget_zippy-login-account-widget',
            'widget_zippy-widget-your-courses'      => 'widget_zippy-your-courses-widget',
            'widget_zippy-widget-lesson-complete'   => 'widget_zippy-complete-entry-widget',
            'widget_zippy-widget-course-navigation' => 'widget_zippy-course-navigation-widget'
        );

        foreach ($zippy_widgets as $old => $new) {
            $old_widget = get_option($old, false);

            if ($old_widget) {
                foreach ($old_widget as $key => &$settings) {
                    if (!is_numeric($key)) {
                        continue;
                    }

                    $widget_settings = new stdClass;
                    $widget_settings->selected = array();
                    $widget_settings->type = isset($old_widget['zippy_student_only']) ? $old_widget['zippy_student_only'] : '0';

                    $settings['zippy_widget_settings'] = json_encode($widget_settings);
                }
               
                update_option($new, $old_widget);
            }
        }

        $sidebars_widgets = get_option('sidebars_widgets');

        foreach ($sidebars_widgets as $sidebar => &$widgets) {
            if (is_array($widgets)) {
                foreach ($widgets as &$widget) {
                    foreach ($zippy_widgets as $old => $new) {
                        $old = str_replace('widget_', '', $old);
                        $new = str_replace('widget_', '', $new);

                        if (strpos($widget, $old) !== false) {
                            $widget = str_replace($old, $new, $widget);
                        }
                    }
                }
            }
        }

        update_option('sidebars_widgets', $sidebars_widgets);
    }
}
