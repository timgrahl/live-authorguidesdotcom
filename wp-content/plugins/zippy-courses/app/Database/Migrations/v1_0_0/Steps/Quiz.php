<?php

class ZippyCourses_v1_0_0_Quiz_MigrationStep extends Zippy_MigrationStep
{
    public $reads = 0;
    public $writes = 0;

    public function __construct()
    {
        $this->id = 'quizzes';
        $this->label = __('Quizzes', ZippyCourses::TEXTDOMAIN);
    }

    public function migrate()
    {
        $zippy = Zippy::instance();

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        if ($this->isCompleted() || $count >= self::COUNT_LIMIT) {
            return;
        }

        global $wpdb;

        if (($migrated = get_transient('zippy_v1_0_0_quizzes_migrated')) === false) {
            $migrated = array();
        }

        $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s", 'quiz');
        $ids = $wpdb->get_col($sql);

        foreach ($ids as $id) {
            if (in_array($id, $migrated) || $count >= self::COUNT_LIMIT) {
                continue;
            }

            $this->_backupMeta($id);
            $this->_migrateQuestions($id);

            $migrated[] = $id;
            $count++;
            
            set_transient('zippy_v1_0_0_quizzes_migrated', $migrated, MINUTE_IN_SECONDS * 10);

            $zippy->cache->set('zippy_upgrade_counter', $count);
        }

        if (count($migrated) == count($ids)) {
            $this->completeStep();
        }
        
        if ($count >= self::COUNT_LIMIT) {
            return;
        }
    }

    private function _backupMeta($post_id)
    {
        $meta = get_post_meta($post_id);

        add_post_meta($post_id, '_zippy_migration_backup', $meta, true);
    }

    public function analyze()
    {

    }

    public function _migrateQuestions($post_id)
    {
        $old_questions = get_post_meta($post_id, '_questions_order', true);
        $old_questions = array_filter(explode(',', $old_questions));

        $questions = array();

        foreach ($old_questions as $old_question) {
            $question     = new stdClass;
            $question->ID = $old_question;

            $questions[] = $question;
        }
        
        update_post_meta($post_id, 'questions', json_encode($questions));
    }
}
