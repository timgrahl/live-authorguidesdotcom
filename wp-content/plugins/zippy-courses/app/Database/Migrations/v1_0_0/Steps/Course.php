<?php

class ZippyCourses_v1_0_0_Course_MigrationStep extends Zippy_MigrationStep
{
    public $actions = 0;

    public function __construct()
    {
        $this->id = 'courses';
        $this->label = __('Courses', ZippyCourses::TEXTDOMAIN);
    }

    public function migrate()
    {
        $zippy = Zippy::instance();

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        if ($this->isCompleted() || $count >= self::COUNT_LIMIT) {
            return;
        }
        
        global $wpdb;
         
        if (($migrated = get_transient('zippy_v1_0_0_courses_migrated')) === false) {
            $migrated = array();
        }

        $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s", 'course');
        $ids = $wpdb->get_col($sql);

        foreach ($ids as $id) {
            if (in_array($id, $migrated) || $count >= self::COUNT_LIMIT) {
                continue;
            }

            $this->backupMeta($id);

            $this->_migrateEntries($id);

            $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

            $this->_migrateConfig($id);
            $this->_migrateTiers($id);
            $this->_migratePublicContent($id);
            $this->_migrateProducts($id);
            $this->_migrateEmailLists($id);

            $migrated[] = $id;
            $count++;
            
            set_transient('zippy_v1_0_0_courses_migrated', $migrated, HOUR_IN_SECONDS * 10);

            $zippy->cache->set('zippy_upgrade_counter', $count);
        }

        if (count($migrated) == count($ids)) {
            $this->completeStep();
        }
        
        if ($count >= self::COUNT_LIMIT) {
            return;
        }
    }

    private function _migrateEmailLists($id)
    {
        $services = array();

        $service = new stdClass;
        $service->service = '';
        $service->per_tier = false;
        $service->list = '0';
        $service->tiers = array();

        $keys = array(
            'mailchimp'         => 'wpcs_mc_course_list',
            'aweber'            => 'wpcs_aweber_course_list',
            'activecampaign'    => 'zp_activecampaign_list',
            'getresponse'       => 'getresponse_campaign',
            'infusionsoft'      => 'zp_infusionsoft_tag',
            'ontraport'         => 'zp_ontraport_tag',
            'constantcontact'   => 'zippy_constant_contact_lists',
            'icontact'          => 'zp_icontact_list',
            'madmimi'           => 'zp_madmimi_list',
            'campaignmonitor'   => 'zippy_campaign_monitor_lists',
        );

        $removable_keys = array_values($keys);

        foreach ($keys as $key => $meta_key) {
            $value = get_post_meta($id, $meta_key, true);

            if (!empty($value)) {
                $email_service = clone ($service);
                $email_service->service = $key;

                if ($key == 'aweber') {
                    $email_service->list = "$value";
                } else {
                    $email_service->list = $value;
                }

                $services[] = $email_service;
            }
        }

        update_post_meta($id, 'email_lists', json_encode($services));

        // Clean it up
        $this->removeMeta($id, $removable_keys);
        $this->incrementCurrentStep();
    }

    private function _migrateDownloads($post_id)
    {
        $removable_keys = array('_files_order');

        $ids = get_post_meta($post_id, '_files_order', true);
        $ids = array_filter(explode(',', $ids));

        $downloads = array();
        foreach ($ids as $id) {
            $downloads[] = $this->_migrateDownload($id);
        }
    
        update_post_meta($post_id, 'downloads', json_encode($downloads));

        // Clean it up
        $this->removeMeta($post_id, $removable_keys);
        $this->incrementCurrentStep();
    }

    private function _migrateDownload($id)
    {
        $removable_keys = array(
            'file_uid',
            'url',
            'type',
            'other_type'
        );

        $this->backupMeta($id);

        $download = new stdClass;
        
        $uid        = get_post_meta($id, 'file_uid', true);
        $url        = get_post_meta($id, 'url', true);
        $type       = get_post_meta($id, 'type', true);
        $other_type = get_post_meta($id, 'other_type', true);

        $download->uid = $uid;
        $download->url = $url;
        $download->title = get_the_title($id);
        $download->type = empty($other_type) ? $type : $other_type;

        // Clean it up
        // $this->removeMeta($id, $removable_keys);
        $this->incrementCurrentStep();

        return $download;
    }

    private function _migrateEntry($entry_id)
    {
        $zippy = Zippy::instance();

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        $this->backupMeta($entry_id);

        $removable_keys = array(
            'entries',
            '_files_order',
            'tiers',
            'open_state',
            'lesson_quiz',
            'lesson_quiz-required',
            'day_of_week',
            'num_days'
        );

        $quiz_id = (int) get_post_meta($entry_id, 'lesson_quiz', true);
        $quiz_required = is_array(get_post_meta($entry_id, 'lesson_quiz_required', true));

        $day_of_week = get_post_meta($entry_id, 'day_of_week', true);
        $day_of_week = $day_of_week !== '' ? $day_of_week : null;

        $num_days    = get_post_meta($entry_id, 'num_days', true);
        $num_days    = $num_days !== '' ? (int) $num_days : null;

        $entry                          = new stdClass;
        $entry->ID                      = $entry_id;
        $entry->title                   = get_the_title($entry_id);
        $entry->post_type               = get_post_type($entry_id);
        $entry->scheduling              = new stdClass;
        $entry->scheduling->day_of_week = $day_of_week;
        $entry->scheduling->num_days    = $num_days;
        $entry->scheduling->required    = false;

        // Handle Quizzes
        $entry->quiz = new stdClass;
            $entry->quiz->ID = $quiz_id;
            $entry->quiz->pass = 0;
            $entry->quiz->required = new stdClass;
                $entry->quiz->required->completion = $quiz_required;
                $entry->quiz->required->pass = false;

        // Handle Tiers
        $entry_tiers = get_post_meta($entry_id, 'tiers', true);
        $entry_tiers = explode(',', $entry_tiers);

        // WE can't use array_filter because there are some 0 values
        foreach ($entry_tiers as $key => $tier) {
            if ($tier === '') {
                unset($entry_tiers[$key]);
            } else {
                $entry_tiers[$key] = (int) $tier;
            }
        }

        $entry->tiers = array_values($entry_tiers);

        $count++;
        $zippy->cache->set('zippy_upgrade_counter', $count);

        // Handle Entries
        $entry_entries = get_post_meta($entry_id, 'entries', true);
        $entry_entries = array_filter(explode(',', $entry_entries));
        $entries = array();

        if (!empty($entry_entries)) {
            foreach ($entry_entries as $subentry_id) {
                $entries[] = $this->_migrateEntry($subentry_id);
            }
        }

        $entry->entries = $entries;

        $this->_migrateDownloads($entry_id);

        // Clean it up
        $this->removeMeta($entry_id, $removable_keys);
        $this->incrementCurrentStep(2);

        return $entry;
    }

    private function _migrateEntries($id)
    {
        $removable_keys = array(
            '_pricing-options_order'
        );

        $ids = get_post_meta($id, '_lessons-and-units_order', true);
        $ids = array_filter(explode(',', $ids));

        $entries = array();

        foreach ($ids as $entry_id) {
            $entries[] = $this->_migrateEntry($entry_id);
        }
        
        update_post_meta($id, 'entries', json_encode($entries));

        // Clean it up
        // $this->removeMeta($id, $removable_keys);
        $this->incrementCurrentStep();
    }

    private function _migratePublicContent($id)
    {
        $removable_keys = array(
            'zippy_public_featured_image',
            'zippy_public_featured_video',
            'zippy_public_featured_media_type',
            'public_featured_image_id',
            'description',
            'zippy_directory_content',
            'enable-public-details'
        );

        $is_public  = (int) is_array(get_post_meta($id, 'enable-public-details', true));

        $content    = get_post_meta($id, 'zippy_directory_content', true);
        $excerpt    = get_post_meta($id, 'description', true);

        $featured_video         = get_post_meta($id, 'zippy_public_featured_video', true);
        $featured_image_id      = (int) get_post_meta($id, 'public_featured_image_id', true);
        $featured_image_url     = get_post_meta($id, 'public_featured_image', true);
        $featured_media_type    = get_post_meta($id, 'zippy_public_featured_media_type', true);

        $featured_media = new stdClass;
        $featured_media->type  = $featured_media_type == 'video' ? 'video' : 'image';
        $featured_media->image = new stdClass;
        $featured_media->image->ID = $featured_image_id;
        $featured_media->image->url = $featured_image_url;
        $featured_media->video = str_replace('"', '\\\\\"', $featured_video);

        update_post_meta($id, 'public_featured_media', json_encode($featured_media));
        update_post_meta($id, 'public_content', $content);
        update_post_meta($id, 'public_excerpt', $excerpt);
        update_post_meta($id, 'public', $is_public);

        // Clean it up
        $this->removeMeta($id, $removable_keys);
        $this->incrementCurrentStep();
    }

    private function _migrateConfig($id)
    {
        $removable_keys = array(
            'schedule_type',
            'schedule_start_type',
            'cal_start'
        );

        $schedule_type = get_post_meta($id, 'schedule_type', true);
        $schedule_type = $schedule_type == 'all' ? 'all' : 'drip';

        $start_type = get_post_meta($id, 'schedule_start_type', true);
        $start_type = $start_type == 'join' ? 'join' : 'date';

        $start_date = get_post_meta($id, 'cal_start', true);

        $config = new stdClass;
        $config->scheduling = new stdClass;
            $config->scheduling->type = $schedule_type;
            $config->scheduling->start = new stdClass;
                $config->scheduling->start->type = $start_type;
                $config->scheduling->start->date = $start_date;
            $config->scheduling->control = new stdClass;
                $config->scheduling->control->type = 'previous';
                $config->scheduling->control->use = 'item';
        
        $config->access     = new stdClass;
            $config->access->use = 'item';

        update_post_meta($id, 'config', $config);

        // Clean it up
        $this->removeMeta($id, $removable_keys);
        $this->incrementCurrentStep();
    }

    private function _migrateProducts($id)
    {
        $removable_keys = array('_pricing-options_order');

        $pricing_options = array_filter(explode(',', get_post_meta($id, '_pricing-options_order', true)));
        $products = array();

        foreach ($pricing_options as $product) {
            $public = get_post_meta($product, 'public', true);

            if (is_array($public)) {
                $products[] = (string) $product;
            }
        }

        update_post_meta($id, 'products', json_encode($products));
        
        // Clean it up
        $this->removeMeta($id, $removable_keys);
        $this->incrementCurrentStep();
    }

    private function _migrateTiers($id)
    {
        $removable_keys = array('tier');

        $tiers = array();
        $old_tiers = get_post_meta($id, 'tier', true);

        if (is_array($old_tiers)) {
            foreach ($old_tiers as $tier_id => $data) {
                $tier = new stdClass;
                $tier->ID = $tier_id;
                $tier->title = $data['title'];

                $tiers[] = $tier;
            }
        }

        update_post_meta($id, 'tiers', json_encode($tiers));
        
        // Clean it up
        $this->removeMeta($id, $removable_keys);
        $this->incrementCurrentStep();
    }

    public function analyze()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = %s", 'course');
        $num_courses = $wpdb->get_var($sql);

        // Entries Analysis
        $this->_analyzeEntries();
        $this->_analyzeConfig($num_courses);
        $this->_analyzeTiers($num_courses);
        $this->_analyzePublicContent($num_courses);
        $this->_analyzeProducts($num_courses);
        $this->_analyzeEmailLists($num_courses);
    }

    /**
     * About 8 queries for each Entry
     *
     * @return int $count
     */
    public function _analyzeEntries()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = %s", '_lessons-and-units-order');
        $entries = $wpdb->get_col($sql);

        $this->actions    += count($entries);

        foreach ($entries as $list) {
            $list = array_filter(explode(',', $list));
            $this->actions    += count($list) * 2;
        }

        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = %s", 'entries');
        $entries = $wpdb->get_col($sql);

        foreach ($entries as $list) {
            $list = array_filter(explode(',', $list));
            $this->actions    += count($list) * 2;
        }
    }

    public function _analyzeConfig($count)
    {
        $this->actions += $count;
    }

    public function _analyzeTiers($count)
    {
        $this->actions += $count;
    }

    public function _analyzePublicContent($count)
    {
        $this->actions += $count;
    }

    public function _analyzeProducts($count)
    {
        $this->actions += $count;
    }

    public function _analyzeEmailLists($count)
    {
        $this->actions += $count;
    }
}
