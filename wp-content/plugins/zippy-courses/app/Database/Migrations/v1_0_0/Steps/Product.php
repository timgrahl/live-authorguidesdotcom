<?php

class ZippyCourses_v1_0_0_Product_MigrationStep extends Zippy_MigrationStep
{
    public $reads = 0;
    public $writes = 0;
    
    public function __construct()
    {
        $this->id = 'products';
        $this->label = __('Products', ZippyCourses::TEXTDOMAIN);
    }

    public function backupMeta($post_id)
    {
        $post_parent = wp_get_post_parent_id($post_id);
        $meta = get_post_meta($post_id);
        $meta['_zippy_post_parent'] = array($post_parent);

        add_post_meta($post_id, '_zippy_migration_backup', $meta, true);
    }

    public function migrate()
    {
        $zippy = Zippy::instance();

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        if ($this->isCompleted() || $count >= self::COUNT_LIMIT) {
            return;
        }

        global $wpdb;

        if (($migrated = get_transient('zippy_v1_0_0_products_migrated')) === false) {
            $migrated = array();
        }

        $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s", 'course_product');
        $ids = $wpdb->get_col($sql);

        foreach ($ids as $id) {
            if (in_array($id, $migrated) || $count >= self::COUNT_LIMIT) {
                continue;
            }
            
            $this->backupMeta($id);

            $this->_migratePricing($id);
            $this->_migrateEmailLists($id);
            $this->_migrateSlug($id);
            $this->_migrateInfusionsoft($id);
            $this->_migrateOneShoppingCart($id);
            $this->_migrateClickBank($id);
            $this->_migrateStatus($id);
            $this->_migrateStripe($id);
            $this->_migrateLaunchWindows($id);
            $this->_migrateAccess($id);

            $migrated[] = $id;
            $count++;
            
            set_transient('zippy_v1_0_0_products_migrated', $migrated, MINUTE_IN_SECONDS * 10);

            $zippy->cache->set('zippy_upgrade_counter', $count);
        }

        if (count($migrated) == count($ids)) {
            $this->_changePricingOptionsToProducts();
            $this->completeStep();
        }
        
        if ($count >= self::COUNT_LIMIT) {
            return;
        }
    }

    private function _changePricingOptionsToProducts()
    {
        global $wpdb;

        $sql = $wpdb->prepare("UPDATE $wpdb->posts SET post_type = %s WHERE post_type = %s", 'product', 'course_product');

        $wpdb->query($sql);
    }

    private function _migrateEmailLists($post_id)
    {
        update_post_meta($post_id, 'email_lists', '{}');

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateSlug($post_id)
    {
        global $wpdb;

        $slug       = get_post_meta($post_id, 'slug', true);
        $post_name  = wp_unique_post_slug($slug, $post_id, get_post_status($post_id), get_post_type($post_id), wp_get_post_parent_id($post_id));

        $wpdb->update($wpdb->posts, array('post_name' => $post_name), array('ID' => $post_id));
        
        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateInfusionsoft($post_id)
    {
        $removable_keys = array('infusionsoft');
        
        $infusionsoft = (array) get_post_meta($post_id, 'infusionsoft', true);

        $product_id     = isset($infusionsoft['product_id']) ? $infusionsoft['product_id'] : false;
        $order_form_url = isset($infusionsoft['order_form_url']) ? $infusionsoft['order_form_url'] : false;

        if ($product_id && $order_form_url) {
            update_post_meta($post_id, 'infusionsoft_product_id', $product_id);
            update_post_meta($post_id, 'infusionsoft_order_form_url', $order_form_url);
        }

        // Clean it up
        $this->removeMeta($post_id, $removable_keys);
        $this->incrementCurrentStep();
    }

    private function _migrateOneShoppingCart($post_id)
    {
        $removable_keys = array('oneshoppingcart');

        $oneshoppingcart = (array) get_post_meta($post_id, 'oneshoppingcart', true);

        $product_id     = isset($oneshoppingcart['product_id']) ? $oneshoppingcart['product_id'] : false;
        $order_form_url = isset($oneshoppingcart['order_form_url']) ? $oneshoppingcart['order_form_url'] : false;

        if ($product_id && $order_form_url) {
            update_post_meta($post_id, 'oneshoppingcart_product_id', $product_id);
            update_post_meta($post_id, 'oneshoppingcart_order_form_url', $order_form_url);
        }

        // Clean it up
        $this->removeMeta($post_id, $removable_keys);
        $this->incrementCurrentStep();
    }

    private function _migrateClickBank($post_id)
    {
        $removable_keys = array('clickbank');

        $clickbank = (array) get_post_meta($post_id, 'clickbank', true);

        $product_id     = isset($clickbank['product_id']) ? $clickbank['product_id'] : false;

        if ($product_id) {
            update_post_meta($post_id, 'clickbank_product_id', $product_id);
        }

        // Clean it up
        $this->removeMeta($post_id, $removable_keys);
        $this->incrementCurrentStep();
    }

    private function _migrateStatus($post_id)
    {
        $status = get_post_meta($post_id, 'status', true);
        $status = isset($status[0]) && $status[0] == 'active' ? 'active' : 'inactive';

        update_post_meta($post_id, 'status', $status);

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateStripe($post_id)
    {
        $removable_keys = array('stripe_objects');

        $stripe_objects = array_filter((array) get_post_meta($post_id, 'stripe_objects', true));

        if (!empty($stripe_objects)) {
            $plans = json_encode($stripe_objects);

            update_post_meta($post_id, 'stripe_plans', $plans);
        }
        
        // Clean it up
        $this->removeMeta($post_id, $removable_keys);
        $this->incrementCurrentStep();
    }

    private function _migrateLaunchWindows($post_id)
    {
        global $wpdb;

        $parent_id          = wp_get_post_parent_id($post_id);
        $schedule_type      = get_post_meta($parent_id, 'schedule_type', true);
        $old_launch_windows = array_filter((array) get_post_meta($post_id, 'launch_window', true));

        $launch_windows                 = new stdClass;
        $launch_windows->launch_windows = array();
        $launch_windows->enabled        = (bool) ($schedule_type == 'evergreen');

        foreach ($old_launch_windows as $item) {
            $launch_window              = new stdClass;
            $launch_window->open_date   = $item['open_date'];
            $launch_window->close_date  = $item['close_date'];
            $launch_window->start_date  = $item['start_date'];

            $launch_windows->launch_windows[] = $launch_window;
        }

        // We do not need a delete because we are overwriting the old value with our update
        update_post_meta($post_id, 'launch_windows', json_encode($launch_windows));

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateAccess($post_id)
    {
        global $wpdb;

        $removable_keys = array('bundle_course', 'tier');

        $parent_id = wp_get_post_parent_id($post_id);
        $tier_id   = get_post_meta($post_id, 'tier', true);

        $access = new stdClass;
        $access->mode = get_post_type($parent_id) == 'course' ? 'product' : 'bundle';

        $access->course = $access->mode == 'product' ? (string) $parent_id : "0";
        $access->tier   = $access->mode == 'product' ? (string) $tier_id : "0";
        $access->bundle = array();
        
        if ($access->mode == 'bundle') {
            $courses = array_filter((array) get_post_meta($parent_id, 'bundle_course', true));
            if (!empty($courses)) {

                foreach ($courses as $course_id => $tiers) {
                    $item = new stdClass;
                    $item->id = (string) $course_id;
                    $item->tiers = array();

                    foreach ($tiers as $tier_id) {
                        $item->tiers[] = (int) $tier_id;
                    }
                    
                    $access->bundle[] = $item;
                }
            }
        }

        $wpdb->update($wpdb->posts, array('post_parent' => 0), array('ID' => $post_id));
        update_post_meta($post_id, 'access', json_encode($access));

        // Clean it up
        $this->removeMeta($post_id, $removable_keys);
        $this->incrementCurrentStep();
    }

    private function _migratePricing($post_id)
    {
        $removable_keys = array('pricing_type', 'single', 'subscription', 'payment_plan');

        $pricing_type   = get_post_meta($post_id, 'pricing_type', true);
        
        $single         = (array) get_post_meta($post_id, 'single', true);
        $subscription   = (array) get_post_meta($post_id, 'subscription', true);
        $payment_plan   = (array) get_post_meta($post_id, 'payment_plan', true);
            
        if ($pricing_type == 'single' &&isset($single['price']) && $single['price'] == '0') {
            $pricing_type = 'free';
        }

        $pricing = new stdClass;
        
        $pricing->type = $pricing_type;

        $pricing->price = new stdClass;
        $pricing->price->free = new stdClass;
            $pricing->price->free->amount = '0.00';

        $pricing->price->single = new stdClass;
        if (is_array($single) && isset($single['price'])) {
            $single['price'] = (float) $single['price'];
            $pricing->price->single->amount = isset($single['price'])
                ? number_format($single['price'], 2, '.', ',')
                : 0.00;
        } else {
            $pricing->price->single->amount = 0.00;
        }

        $pricing->price->subscription = new stdClass;
        if (is_array($subscription) && isset($subscription['price'])) {
            $subscription['price'] = (float) $subscription['price'];
            $pricing->price->subscription->amount = isset($subscription['price'])
                ? number_format($subscription['price'], 2, '.', ',')
                : 0.00;
            $pricing->price->subscription->frequency = 'month';
        } else {
            $pricing->price->subscription->amount = 0.00;
            $pricing->price->subscription->frequency = 'month';
        }

        $pricing->price->payment_plan = new stdClass;
        if (is_array($subscription) && isset($subscription['price'])) {
            $payment_plan['price_per_payment'] = (float) $payment_plan['price_per_payment'];
            $pricing->price->payment_plan->amount = isset($payment_plan['price_per_payment'])
                ? number_format($payment_plan['price_per_payment'], 2, '.', ',')
                : 0.00;
            $pricing->price->payment_plan->frequency = 'month';
            $pricing->price->payment_plan->num_payments = isset($payment_plan['num_payments'])
                ? (int) $payment_plan['num_payments']
                : 0;
        } else {
            $pricing->price->payment_plan->amount = 0.00;
            $pricing->price->payment_plan->frequency = 'month';
            $pricing->price->payment_plan->num_payments = 1;
        }

        update_post_meta($post_id, 'pricing', json_encode($pricing));

        // Clean it up
        $this->removeMeta($post_id, $removable_keys);
        $this->incrementCurrentStep();
    }

    public function analyze()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = %s", 'course_product');
        $num_products = $wpdb->get_var($sql);

        // For number of rows to change
        $this->actions += $num_products;

        // Entries Analysis
        $this->_analyzePricing($num_products);
        $this->_analyzeEmailLists($num_products);
        $this->_analyzeSlug($num_products);
        $this->_analyzeInfusionsoft();
        $this->_analyzeOneShoppingCart();
        $this->_analyzeClickBank();
        $this->_analyzeStatus($num_products);
        $this->_analyzeStripe($num_products);
        $this->_analyzeLaunchWindows($num_products);
        $this->_analyzeAccess($num_products);
    }

    public function _analyzePricing($count)
    {
        $this->actions += $count;
    }

    public function _analyzeEmailLists($count)
    {
        $this->actions += $count;
    }

    public function _analyzeSlug($count)
    {
        $this->actions += $count;
    }

    public function _analyzeInfusionsoft()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->postmeta WHERE meta_key = %s", 'infusionsoft');
        $count = $wpdb->get_var($sql);

        $this->actions += $count;
    }

    public function _analyzeOneShoppingCart()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->postmeta WHERE meta_key = %s", 'oneshoppingcart');
        $count = $wpdb->get_var($sql);

        $this->actions += $count;
    }

    public function _analyzeClickBank()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->postmeta WHERE meta_key = %s", 'clickbank');
        $count = $wpdb->get_var($sql);

        $this->actions += $count;
    }

    public function _analyzeStatus($count)
    {
        $this->actions += $count;
    }

    public function _analyzeStripe()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->postmeta WHERE meta_key = %s", 'stripe_objects');
        $count = $wpdb->get_var($sql);

        $this->actions += $count;
    }

    public function _analyzeLaunchWindows($count)
    {
        $this->actions += $count;
    }

    public function _analyzeAccess($count)
    {
        $this->actions += $count;
    }
}
