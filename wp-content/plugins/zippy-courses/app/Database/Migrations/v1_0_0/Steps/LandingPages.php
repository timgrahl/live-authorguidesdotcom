<?php

class ZippyCourses_v1_0_0_LandingPages_MigrationStep extends Zippy_MigrationStep
{
    public $reads = 0;
    public $writes = 0;

    public function __construct()
    {
        $this->id = 'landing_pages';
        $this->label = __('Landing Pages', ZippyCourses::TEXTDOMAIN);
    }

    public function migrate()
    {
        $zippy = Zippy::instance();

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        if ($this->isCompleted() || $count >= self::COUNT_LIMIT) {
            return;
        }

        global $wpdb;

        if (($migrated = get_transient('zippy_v1_0_0_landing_pages_migrated')) === false) {
            $migrated = array();
        }

        $ids_with_landing_pages = $this->getPostIdsWithLandingPages();

        foreach ($ids_with_landing_pages as $id) {
            if (in_array($id, $migrated) || $count >= self::COUNT_LIMIT) {
                continue;
            }

            $this->backupMeta($id);
            $this->_migrateLandingPages($id);

            $migrated[] = $id;
            $count++;
            
            set_transient('zippy_v1_0_0_landing_pages_migrated', $migrated, MINUTE_IN_SECONDS * 10);

            $zippy->cache->set('zippy_upgrade_counter', $count);
        }

        if (count($migrated) == count($ids_with_landing_pages)) {
            $this->completeStep();
        }
        
        if ($count >= self::COUNT_LIMIT) {
            return;
        }
    }

    public function _migrateLandingPages($post_id)
    {
        global $wpdb;

        $removable_keys = array(
            'landing_page_options',
            'landing_page_type',
            'landing_page_thank_you_url',
            'landing_page_list_service',
        );

        $options        = (array) get_post_meta($post_id, 'landing_page_options', true);
        $type           = get_post_meta($post_id, 'landing_page_type', true);
        $service        = get_post_meta($post_id, 'landing_page_service', true);
        $list           = get_post_meta($post_id, "landing_page_list_{$service}", true);
        $thank_you_url  = get_post_meta($post_id, 'landing_page_thank_you_url', true);

        $this->backupMeta($post_id);

        $landing_page = new stdClass;
        $landing_page->visitor = new stdClass;
        $landing_page->visitor->headline = isset($options['visitor_prompt']) ? $options['visitor_prompt'] : '';
        $landing_page->visitor->button = isset($options['visitor_btn']) ? $options['visitor_btn'] : '';

        $landing_page->student = new stdClass;
        $landing_page->student->headline = isset($options['student_prompt']) ? $options['student_prompt'] : '';
        $landing_page->student->button = isset($options['student_btn']) ? $options['student_btn'] : '';
        
        $landing_page->type         = !empty($type) ? $type : 'product';
        $landing_page->product      = isset($options['product_id']) ? $options['product_id'] : '0';
        $landing_page->list         = !empty($list) ? $list : '0';
        $landing_page->service      = !empty($service) ? $service : '0';
        $landing_page->disclaimer   = isset($options['disclaimer']) ? $options['disclaimer'] : '0';

        update_post_meta($post_id, 'landing_page', json_encode($landing_page));
    }

    public function getPostIdsWithLandingPages()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key LIKE %s GROUP BY post_id", '%landing_page%');
        $post_ids = $wpdb->get_col($sql);

        return $post_ids;
    }

    public function analyze()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->postmeta WHERE meta_key LIKE %s GROUP BY post_id", '%landing_page%');
        $num_rows = $wpdb->get_var($sql);
    }
}
