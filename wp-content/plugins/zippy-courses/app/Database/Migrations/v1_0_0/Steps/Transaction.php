<?php

class ZippyCourses_v1_0_0_Transaction_MigrationStep extends Zippy_MigrationStep
{
    public function __construct()
    {
        $this->id = 'transactions';
        $this->label = __('Transactions', ZippyCourses::TEXTDOMAIN);
    }

    public function migrate()
    {
        $zippy = Zippy::instance();

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        if ($this->isCompleted() || $count >= self::COUNT_LIMIT) {
            return;
        }
        
        global $wpdb;

        if (($migrated = get_transient('zippy_v1_0_0_transactions_migrated')) === false) {
            $migrated = array();
        }

        $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s", 'payment');
        $ids = $wpdb->get_col($sql);
        
        $removable_keys = array('payment_details');

        foreach ($ids as $id) {
            if (in_array($id, $migrated) || $count >= self::COUNT_LIMIT) {
                continue;
            }

            $this->backupMeta($id);

            $this->_migrateDetails($id);
            $this->_migrateProduct($id);
            $this->_migrateCustomer($id);
            $this->_migrateOrderId($id);
            $this->_migrateVendor($id);
            $this->_migrateVendorId($id);
            $this->_migrateTransactionKey($id);
            $this->_migrateRecurring($id);
            $this->_migrateRecurringId($id);

            $this->removeMeta($id, $removable_keys);

            $migrated[] = $id;
            $count++;

            set_transient('zippy_v1_0_0_transactions_migrated', $migrated, MINUTE_IN_SECONDS * 10);

            $zippy->cache->set('zippy_upgrade_counter', $count);
        }

        if (count($migrated) == count($ids)) {
            $this->_changeToTransaction();
            $this->completeStep();
        }

        if ($count >= self::COUNT_LIMIT) {
            return;
        }

    }

    private function _changeToTransaction()
    {
        global $wpdb;

        $sql = $wpdb->prepare("UPDATE $wpdb->posts SET post_type = %s WHERE post_type = %s", 'transaction', 'payment');

        $wpdb->query($sql);
    }

    private function _migrateProduct($post_id)
    {
        $order_id       = wp_get_post_parent_id($post_id);
        $order_details  = get_post_meta($order_id, 'order_details', true);
        $product        = isset($order_details['product'][0]) ? $order_details['product'][0] : 0;

        update_post_meta($post_id, 'product', $product);

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateOrderId($post_id)
    {
        $order_id       = wp_get_post_parent_id($post_id);
        
        update_post_meta($post_id, 'order_id', $order_id);

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateVendor($post_id)
    {
        $order_id       = wp_get_post_parent_id($post_id);
        $vendor         = get_post_meta($order_id, 'vendor', true);

        $vendor         = isset($order_details['vendor']) ? $order_details['vendor'] : '';

        update_post_meta($post_id, 'vendor', $vendor);

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateVendorId($post_id)
    {
        $old_details    = (array) get_post_meta($post_id, 'payment_details', true);
        $vendor_id      = isset($old_details['vendor_id']) ? $old_details['vendor_id'] : '';

        update_post_meta($post_id, 'vendor_id', $vendor_id);

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateCustomer($post_id)
    {
        $zippy = Zippy::instance();

        $order_id       = wp_get_post_parent_id($post_id);
        $customer_data  = get_post_meta($order_id, 'customer', true);

        $customer = $zippy->make('customer');

        if (isset($customer_data['user_email'])) {
            $customer->setEmail($customer_data['user_email']);
        }

        if (isset($customer_data['user_firstname'])) {
            $customer->setFirstName($customer_data['user_firstname']);
        }

        if (isset($customer_data['user_lastname'])) {
            $customer->setLastName($customer_data['user_lastname']);
        }

        if (isset($customer_data['ID'])) {
            $customer->setId($customer_data['ID']);
        }

        update_post_meta($post_id, 'customer', json_encode($customer));

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateTransactionKey($post_id)
    {
        $zippy = Zippy::instance();

        $order_id       = wp_get_post_parent_id($post_id);
        $order_key      = get_post_meta($order_id, 'order_uid', true);

        $key            = !empty($order_key) ? $order_key : $zippy->utilities->transaction->generateKey();

        update_post_meta($post_id, 'transaction_key', $key);

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateRecurring($post_id)
    {
        $order_id       = wp_get_post_parent_id($post_id);
        $old_details    = (array) get_post_meta($post_id, 'payment_details', true);
        $type           = isset($old_details['type']) ? $old_details['type'] : 'single';

        $recurring      = (bool) $type !== 'single';
        
        update_post_meta($post_id, 'recurring', $recurring);

        // Clean it up
        $this->incrementCurrentStep();
    }

    private function _migrateRecurringId($post_id)
    {
        $order_id       = wp_get_post_parent_id($post_id);
        $old_details    = (array) get_post_meta($post_id, 'payment_details', true);
        $type           = isset($old_details['type']) ? $old_details['type'] : 'single';

        $recurring      = (bool) $type !== 'single';
        $recurring_id   = 0;

        if ($recurring) {
            global $wpdb;

            $recurring_table  = $wpdb->prefix . 'wpcs_user_recurring_payment';
            $recurring_record = $wpdb->get_row($wpdb->prepare("SELECT * FROM $recurring_table WHERE order_id = %s", $order_id));
            if ($recurring_record) {
                $recurring_id = $recurring_record->vendor_id;
            }
        }
        
        update_post_meta($post_id, 'recurring_id', $recurring_id);
    }

    private function _migrateDetails($post_id)
    {
        global $wpdb;

        $order_id       = wp_get_post_parent_id($post_id);
        $old_details    = (array) get_post_meta($post_id, 'payment_details', true);
        $transaction    = get_post($post_id);
        $datetime       = new DateTime($transaction->post_date_gmt);
        $order_details  = get_post_meta($order_id, 'order_details', true);

        $type           = isset($old_details['type']) ? $old_details['type'] : 'single';
        $num_payments   = $type == 'single' ? 1 : 0;
        $vendor         = get_post_meta($order_id, 'vendor', true);
        $vendor_id      = isset($old_details['vendor_id']) ? $old_details['vendor_id'] : '';
        $total          = isset($old_details['total']) ? $old_details['total'] : 0;
        $fee            = isset($old_details['fees']) ? $old_details['fees'] : 0;
        $tax            = isset($old_details['tax']) ? $old_details['tax'] : 0;
        $currency       = isset($old_details['currency']) ? $old_details['currency'] : 'USD';
        $mode           = isset($old_details['mode']) ? $old_details['mode'] : 'live';
        $method         = $vendor == 'paypal' ? 'gateway' : 'credit_card';
        $status         = get_post_meta($order_id, 'refund_status', true) == 1 ? 'refund' : 'complete';
        $product        = isset($order_details['product'][0]) ? $order_details['product'][0] : 0;
        $raw            = isset($old_details['raw']) ? $old_details['raw'] : '';

        $details = array(
            'vendor_id'     => $vendor_id,
            'total'         => $total,
            'fee'           => $fee,
            'tax'           => $tax,
            'currency'      => $currency,
            'type'          => $type,
            'timestamp'     => $datetime->format('U'),
            'status'        => $status,
            'mode'          => $mode,
            'method'        => $method,
            'num_payments'  => $num_payments,
            'product'       => $product,
            'raw'           => $raw
        );

        update_post_meta($post_id, 'details', $details);

        // Clean it up
        $this->incrementCurrentStep();
    }

    public function analyze()
    {
        global $wpdb;

        $sql    = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = %s", 'payment');
        $count  = $wpdb->get_var($sql);

        $this->actions += $count;

        $this->_analyzeDetails($count);
        $this->_analyzeProduct($count);
        $this->_analyzeCustomer($count);
        $this->_analyzeOrderId($count);
        $this->_analyzeVendor($count);
        $this->_analyzeVendorId($count);
        $this->_analyzeTransactionKey($count);
        $this->_analyzeRecurring($count);
        $this->_analyzeRecurringId($count);
    }

    public function _analyzeDetails($count)
    {
        $this->actions += $count;
    }

    public function _analyzeProduct($count)
    {
        $this->actions += $count;
    }

    public function _analyzeCustomer($count)
    {
        $this->actions += $count;
    }

    public function _analyzeOrderId($count)
    {
        $this->actions += $count;
    }

    public function _analyzeVendor($count)
    {
        $this->actions += $count;
    }

    public function _analyzeVendorId($count)
    {
        $this->actions += $count;
    }

    public function _analyzeTransactionKey($count)
    {
        $this->actions += $count;
    }

    public function _analyzeRecurring($count)
    {
        $this->actions += $count;
    }

    public function _analyzeRecurringId($count)
    {
        $this->actions += $count;
    }
}
