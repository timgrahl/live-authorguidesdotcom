<?php

class ZippyCourses_v1_0_0_User_MigrationStep extends Zippy_MigrationStep
{
    public $reads = 0;
    public $writes = 0;

    public function __construct()
    {
        $this->id = 'users';
        $this->label = __('Students', ZippyCourses::TEXTDOMAIN);
    }

    public function migrate()
    {
        $zippy = Zippy::instance();
        
        add_filter('zippy_disable_emails', '__return_true');

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        if ($this->isCompleted() || $count >= self::COUNT_LIMIT) {
            return;
        }
        
        global $wpdb;

        if (($migrated = get_transient('zippy_v1_0_0_users_migrated')) === false) {
            $migrated = array();
        }

        $sql = $wpdb->prepare("SELECT user_id FROM $wpdb->usermeta WHERE meta_key = %s OR meta_key = %s OR meta_key = %s GROUP BY user_id", 'additional_permissions', 'products', 'products_owned');
        $ids = $wpdb->get_col($sql);


        foreach ($ids as $id) {
            if (in_array($id, $migrated) || $count >= self::COUNT_LIMIT) {
                continue;
            }

            $this->backupMeta($id);

            $this->_migratePermissions($id);
            $this->_migrateCompletedEntries($id);
            $this->_migrateStartDates($id);
            $this->_migrateProductsOwned($id);

            $migrated[] = $id;
            $count++;

            set_transient('zippy_v1_0_0_users_migrated', $migrated, MINUTE_IN_SECONDS * 10);

            $zippy->cache->set('zippy_upgrade_counter', $count);
        }

        $this->_migrateQuizResults();

        if (count($migrated) == count($ids)) {
            $this->completeStep();
        }
        
        if ($count >= self::COUNT_LIMIT) {
            return;
        }
    }

    public function backupMeta($user_id)
    {
        $meta = get_user_meta($user_id);
        add_user_meta($user_id, '_zippy_migration_backup', $meta, true);
    }

    public function removeMeta($user_id, array $keys)
    {
        foreach ($keys as $meta_key) {
            delete_user_meta($user_id, $meta_key);
        }
    }

    public function _migrateProductsOwned($user_id)
    {
        $zippy = Zippy::instance();

        $student = $zippy->make('student', array($user_id));
        $student->fetch();

        $new_products_owned = $student->getProductsOwned();
        $old_products_owned = array_filter((array) get_user_meta($user_id, 'products_owned', true));

        foreach ($old_products_owned as $entry) {
            if (!in_array($entry['product'], $new_products_owned)) {
                $zippy->access->grant($user_id, $entry['product']);

                $student = $zippy->make('student', array($user_id));
                $student->fetch();

                $orders = $student->getOrders();
                $order  = $orders->last();

                if ($order) {
                    global $wpdb;

                    $wpdb->update(
                        $wpdb->posts,
                        array(
                            'post_date'     => $entry['time'],
                            'post_date_gmt' => $entry['time']
                        ),
                        array(
                            'ID' => $order->getId()
                        )
                    );
                }
            }
        }
    }

    public function _migratePermissions($user_id)
    {
        $removable_keys = array('additional_permissions');

        $old_permissions    = get_user_meta($user_id, 'additional_permissions', true);

        $permissions        = new stdClass;
        $permissions->granted = array();
        $permissions->revoked = array();

        $products           = array();

        if (isset($old_permissions['granted']) && is_array($old_permissions['granted'])) {
            foreach ($old_permissions['granted'] as $permission) {
                if (get_post_type($permission['id']) == 'product' || get_post_type($permission['id']) == 'course_product') {
                    $products[] = $permission['id'];
                } else {
                    $new_perm = new stdClass;
                    $new_perm->course   = $permission['id'];
                    $new_perm->tier     = $permission['tier'];
                    $new_perm->time     = $permission['time'];

                    $permissions->granted[] = $new_perm;
                }
            }
        }

        if (isset($old_permissions['revoked']) && is_array($old_permissions['revoked'])) {
            foreach ($old_permissions['revoked'] as $permission) {
                foreach ($products as $key => $product_id) {
                    if ($permission['id'] == $product_id) {
                        unset($products[$key]);
                    }
                }

                foreach ($permissions->granted as $key => $perm) {
                    if ($perm->course == $permission['id']) {
                        unset($permissions->granted[$key]);
                    }
                }
            }
        }

        $products       = array_values($products);
        $permissions->granted = array_values($permissions->granted);
        $permissions->revoked = array_values($permissions->revoked);

        update_user_meta($user_id, 'zippy_permissions', json_encode($permissions));
        update_user_meta($user_id, 'products', json_encode($products));

        // Clean it up
        $this->removeMeta($user_id, $removable_keys);
        $this->incrementCurrentStep();
    }

    public function _migrateCompletedEntries($user_id)
    {
        $removable_keys = array('completed_lessons');

        $completed = get_user_meta($user_id, 'completed_lessons', true);

        update_user_meta($user_id, 'completed_entries', $completed);
        
        // Clean it up
        $this->removeMeta($user_id, $removable_keys);
        $this->incrementCurrentStep();
    }

    public function _migrateStartDates($user_id)
    {
        $zippy = Zippy::instance();

        $start_dates = get_user_meta($user_id, 'zippy_courses_start_dates', true);

        $output = array();

        if (is_array($start_dates)) {
            foreach ($start_dates as $course_id => $start_date) {
                if (!empty($start_date) && strtotime($start_date) !== false) {
                    $date = $zippy->utilities->datetime->getDate($start_date);
                    $output[$course_id] = $date->format('m/d/Y');
                }
            }
        }

        update_user_meta($user_id, 'zippy_courses_start_dates', json_encode($start_dates));
        
        // Clean it up
        $this->incrementCurrentStep();
    }

    public function _migrateQuizResults()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT * FROM $wpdb->usermeta WHERE meta_key = %s", 'quiz_results');
        $results = $wpdb->get_results($sql);

        foreach ($results as $row) {
            $result = maybe_unserialize($row->meta_value);
        
            if (!is_array($result)) {
                continue;
            }

            $submitted = new DateTime($result['submitted']);
            $result['submitted'] = (int) $submitted->format('U');

            $questions = array();
            $old_questions = $result['questions'];
            
            foreach ($old_questions as $old_question_id => $old_question) {
                $question = new stdClass;
                $question->id = (int) $old_question_id;
                $question->question = $old_question['question'];
                $question->correct = $old_question['answer']['correct'] == $old_question['answer']['given'];
                
                $question->answer = new stdClass;

                $question->answer->given = new stdClass;
                $question->answer->given->ID = 0;
                $question->answer->given->content = $old_question['answer']['given'];
                $question->answer->correct = new stdClass;
                $question->answer->correct->ID = 0;
                $question->answer->correct->content = $old_question['answer']['correct'];

                $questions[] = $question;
            }

            $result['questions'] = $questions;

            $output = new stdClass;
            $output->quiz       = (int) $result['quiz'];
            $output->score      = (int) $result['score'];
            $output->questions  = $result['questions'];
            $output->submitted  = $result['submitted'];

            $update_sql = $wpdb->prepare("UPDATE $wpdb->usermeta SET meta_value = %s WHERE umeta_id = %d", json_encode($output), $row->umeta_id);
            $wpdb->query($update_sql);
        }
        
        // Clean it up
        $this->incrementCurrentStep();
    }

    public function analyze()
    {
        $this->_analyzeCompletedEntries();
        $this->_analyzeStartDates();
        $this->_analyzePermissions();
        $this->_analyzeQuizResults();
    }

    public function _analyzeCompletedEntries()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->usermeta WHERE meta_key = %s GROUP BY user_id", 'completed_lessons');
        $count = $wpdb->get_var($sql);

        $this->actions += $count;
    }

    public function _analyzeStartDates()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->usermeta WHERE meta_key = %s GROUP BY user_id", 'zippy_courses_start_dates');
        $count = $wpdb->get_var($sql);

        $this->actions += $count;
    }

    public function _analyzeQuizResults()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->usermeta WHERE meta_key = %s GROUP BY user_id", 'quiz_results');
        $count = $wpdb->get_var($sql);

        $this->actions += $count;
    }

    public function _analyzePermissions()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->usermeta WHERE meta_key = %s GROUP BY user_id", 'additional_permissions');
        $count = $wpdb->get_var($sql);

        $this->actions += $count;
    }
}
