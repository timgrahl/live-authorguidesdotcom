<?php

class ZippyCourses_v1_0_0_QuizQuestions_MigrationStep extends Zippy_MigrationStep
{
    public $reads = 0;
    public $writes = 0;

    public function __construct()
    {
        $this->id = 'quiz_questions';
        $this->label = __('Quiz Questions', ZippyCourses::TEXTDOMAIN);
    }

    public function migrate()
    {
        $zippy = Zippy::instance();

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        if ($this->isCompleted() || $count >= self::COUNT_LIMIT) {
            return;
        }
        
        global $wpdb;

        if (($migrated = get_transient('zippy_v1_0_0_quiz_questions_migrated')) === false) {
            $migrated = array();
        }

        $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s", 'question');
        $ids = $wpdb->get_col($sql);

        $this->_changePostType();

        foreach ($ids as $id) {
            if (in_array($id, $migrated) || $count >= self::COUNT_LIMIT) {
                continue;
            }

            $this->backupMeta($id);
            $this->_migrateAnswers($id);

            $migrated[] = $id;
            $count++;

            set_transient('zippy_v1_0_0_quiz_questions_migrated', $migrated, MINUTE_IN_SECONDS * 10);

            $zippy->cache->set('zippy_upgrade_counter', $count);
        }

        if (count($migrated) == count($ids)) {
            $this->completeStep();
        }
        
        if ($count >= self::COUNT_LIMIT) {
            return;
        }
    }

    private function _changePostType()
    {
        global $wpdb;

        $sql = $wpdb->prepare("UPDATE $wpdb->posts SET post_type = %s WHERE post_type = %s", 'question', 'quiz_question');
        $wpdb->query($sql);
    }

    public function analyze()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = %s", 'question');
        $count = $wpdb->get_var($sql);

        $this->actions = $count * 2;
    }

    public function _migrateAnswers($post_id)
    {
        $old_answers = array_filter((array) get_post_meta($post_id, 'answers', true));
        $answers = array();

        $correct = null;

        foreach ($old_answers as $key => $old_answer) {
            $answer     = new stdClass;
            $answer->ID = $key;
            $answer->content = $old_answer['answer'];

            if (isset($old_answer['correct'])) {
                $correct = (int) $key;
            }

            $answers[] = $answer;
        }
        
        update_post_meta($post_id, 'answers', json_encode($answers));

        if ($correct !== null) {
            update_post_meta($post_id, 'correct', (string) $correct);
        }
    }
}
