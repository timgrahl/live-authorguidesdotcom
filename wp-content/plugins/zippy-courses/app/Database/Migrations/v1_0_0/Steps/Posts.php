<?php

class ZippyCourses_v1_0_0_Posts_MigrationStep extends Zippy_MigrationStep
{
    public $reads = 0;
    public $writes = 0;

    public function __construct()
    {
        $this->id = 'posts';
        $this->label = __('Post Data', ZippyCourses::TEXTDOMAIN);
    }

    public function migrate()
    {
        $zippy = Zippy::instance();

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        if ($this->isCompleted() || $count >= self::COUNT_LIMIT) {
            return;
        }

        global $wpdb;

        if (($migrated = get_transient('zippy_v1_0_0_posts_migrated')) === false) {
            $migrated = array();
        }

        $ids_with_featured_media = $this->getPostIdsWithFeaturedMedia();

        foreach ($ids_with_featured_media as $id) {
            if (in_array($id, $migrated) || $count >= self::COUNT_LIMIT) {
                continue;
            }

            $this->backupMeta($id);
            $this->_migrateFeaturedMedia($id);

            $migrated[] = $id;
            $count++;
            
            set_transient('zippy_v1_0_0_posts_migrated', $migrated, MINUTE_IN_SECONDS * 10);

            $zippy->cache->set('zippy_upgrade_counter', $count);
        }

        if (count($migrated) == count($ids_with_featured_media)) {
            $this->completeStep();
        }
        
        if ($count >= self::COUNT_LIMIT) {
            return;
        }
    }

    public function _migrateFeaturedMedia($post_id)
    {
        $removable_keys = array(
            'zippy_featured_image',
            'zippy_featured_video',
            'zippy_featured_media_type',
        );

        $featured_video         = get_post_meta($post_id, 'zippy_featured_video', true);
        $featured_image_id      = (int) get_post_meta($post_id, '_thumbnail_id', true);
        $featured_image_url     = get_post_meta($post_id, 'zippy_featured_image', true);
        $featured_media_type    = get_post_meta($post_id, 'zippy_featured_media_type', true);

        $featured_media = new stdClass;
        $featured_media->type  = $featured_media_type == 'video' ? 'video' : 'image';
        $featured_media->image = new stdClass;
        $featured_media->image->ID = $featured_image_id;
        $featured_media->image->url = $featured_image_url;
        $featured_media->video = str_replace('"', '\\\\\"', $featured_video);

        update_post_meta($post_id, 'featured_media', json_encode($featured_media));
    }

    public function getPostIdsWithFeaturedMedia()
    {
         global $wpdb;

         $sql = $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key LIKE %s GROUP BY post_id", '%featured_media%');

         return $wpdb->get_col($sql);
    }

    private function _removeMeta($post_id, array $keys)
    {
        foreach ($keys as $meta_key) {
            delete_post_meta($post_id, $meta_key);
        }
    }

    public function analyze()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->postmeta WHERE meta_key IN (%s, %s, %s, %s)", 'zippy_featured_image', 'zippy_featured_video', 'zippy_featured_media_type', '_thumbnail_id');
        $num_rows = $wpdb->get_var($sql);

        $this->reads = $num_rows * 4;
        $this->writes = $num_rows;
    }
}
