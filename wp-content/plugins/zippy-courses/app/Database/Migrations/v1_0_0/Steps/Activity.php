<?php

class ZippyCourses_v1_0_0_Activity_MigrationStep extends Zippy_MigrationStep
{
    public function __construct()
    {
        $this->id = 'activity_table';
        $this->label = __('Database Tables', ZippyCourses::TEXTDOMAIN);
    }

    public function migrate()
    {
        global $wpdb;
        
        $zippy = Zippy::instance();

        $count = $zippy->cache->get('zippy_upgrade_counter') !== null ? $zippy->cache->get('zippy_upgrade_counter') : 0;

        if ($this->isCompleted() || $count >= self::COUNT_LIMIT) {
            return;
        }
         
        if (($migrated = get_transient('zippy_v1_0_0_activity_table_migrated')) === false) {
            $migrated = array();
        }

        $old_activity_table = $wpdb->prefix . 'wpcs_user_activity';
        $new_activity_table = $wpdb->prefix . 'zippy_user_activity';

        $old_table_exists = $wpdb->get_results("SHOW TABLES LIKE '$old_activity_table'");
        $new_table_exists = $wpdb->get_results("SHOW TABLES LIKE '$new_activity_table'");
        
        if (count($new_table_exists) && count($old_table_exists)) {
            $wpdb->query("DROP TABLE $new_activity_table");
        }

        if (!count($new_table_exists) && count($old_table_exists)) {
            $rename_sql     = "RENAME TABLE $old_activity_table TO $new_activity_table";
            $rename_query   = $wpdb->query($rename_sql);
        }

        $item_type_sql = "ALTER TABLE $new_activity_table MODIFY item_id VARCHAR(10)";
        $item_type_query = $wpdb->query($item_type_sql);

        $collation_sql = "ALTER TABLE $new_activity_table CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci";
        $collation_query = $wpdb->query($collation_sql);

        $key1_exists = $wpdb->get_results("SHOW INDEX FROM $new_activity_table WHERE KEY_NAME = 'user_id'");
        if (!count($key1_exists)) {
            $index1_sql = "ALTER TABLE $new_activity_table ADD KEY user_id (user_id)";
            $index1_query = $wpdb->query($index1_sql);
        }

        $key2_exists = $wpdb->get_results("SHOW INDEX FROM $new_activity_table WHERE KEY_NAME = 'type'");
        if (!count($key1_exists)) {
            $index2_sql = "ALTER TABLE $new_activity_table ADD KEY type (type)";
            $index2_query = $wpdb->query($index2_sql);
        }

        $count += 25;

        $zippy->cache->set('zippy_upgrade_counter', $count);

        $this->completeStep();
    }

    private function _migrateTableName()
    {
        
    }

    public function analyze()
    {
    }
}
