<?php

class ZippyCourses_Installer
{
    private static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
    }

    public static function activate($network_wide)
    {
        if (function_exists('is_multisite') && is_multisite()) {
            if ($network_wide) {
                // Get all blog ids
                $blog_ids = self::get_blog_ids();

                foreach ($blog_ids as $blog_id) {
                    switch_to_blog($blog_id);
                    self::singleActivate();
                }

                restore_current_blog();

            } else {
                self::singleActivate();
            }

        } else {
            self::singleActivate();
        }
    }

    /**
     * Fired for each blog when the plugin is activated.
     *
     * @since   1.0
     */
    private static function singleActivate()
    {
        $zippy = Zippy::instance();

        self::database();
        self::emails();
        self::corePages();
        self::thankYouPage();
        self::clearMiddlewareCache();

        set_transient('_zippy_activation_redirect', 1, 30);
    }

    private static function database()
    {
        $tables = Zippy_DatabaseTables::instance();
        $db = new ZippyCourses_Database($tables);

        $tables->install();

        self::databaseUpgrade_v1_1_0();
    }

    private static function emails()
    {
        $zippy = Zippy::instance();
        $registration = $zippy->utilities->email->getDefaultRegistrationEmail();
        $forgot_password = $zippy->utilities->email->getDefaultForgotPasswordEmail();
        $purchase = $zippy->utilities->email->getDefaultPurchaseReceiptEmail();
        $registration_reminder = $zippy->utilities->email->getDefaultRegistrationReminderEmail();
        $new_student = $zippy->utilities->email->getDefaultNewStudentEmail();
        $course_completion = $zippy->utilities->email->getDefaultCourseCompletionEmail();

        $emails = array(
            'zippy_system_emails_purchase_receipt_settings'         => $purchase,
            'zippy_system_emails_new_student_settings'              => $new_student,
            'zippy_system_emails_registration_reminder_settings'    => $registration_reminder,
            'zippy_system_emails_forgot_password_settings'          => $forgot_password,
            'zippy_system_emails_registration_settings'             => $registration,
            'zippy_system_emails_course_completion_settings'        => $course_completion
        );

        foreach ($emails as $key => $value) {
            $email = get_option($key, array());

            if (empty($email) || (isset($email['content']) && empty($email['content']))) {
                update_option($key, $value);
            }
        }
    }

    public static function corePages()
    {
        global $wpdb, $current_user;

        $zippy = Zippy::instance();

        $pages = $zippy->core_pages->getList();
        $core_pages = array();

        foreach ($pages as $key => $details) {
            $post_id = 0;

            if (!$details['id']) {
                // See if a page exists with the correct slug or title
                $sql     = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_title = %s OR post_name = %s", $details['title'], $key);
                $post_id = $wpdb->get_var($sql);

                if (!$post_id) {
                    $post_id = wp_insert_post(
                        array(
                            'post_title'        => $details['title'],
                            'post_content'      => ' ',
                            'post_status'       => 'publish',
                            'post_author'       => $current_user->ID,
                            'post_type'         => 'page',
                            'comment_status'    => 'closed'
                        ),
                        true
                    );

                    if (is_wp_error($post_id)) {
                        $post_id = 0;
                    }
                }
                
                $core_pages[$key] = $post_id;
            } else {
                $core_pages[$key] = $details['id'];
            }

        }

        update_option('zippy_core_pages', $core_pages);
    }

    public static function thankYouPage()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $content = '';
        $content .= '<p>' . __('Thank you for your purchase!', ZippyCourses::TEXTDOMAIN) . '</p>';
        $content .= '<p>' . __('Please proceed to register your account. We have sent you an email with a link to register as well. If you run into any difficulties, please contact [zippy_contact_email_link] and we will help resolve any issues.', ZippyCourses::TEXTDOMAIN) . '</p>';
        $content .= '<p>[zippy_button type="register" style="primary"]</p>';
        $content .= '<p>' . __('Or if you are already a member, please login to claim your account:', ZippyCourses::TEXTDOMAIN) . '</p>';
        $content .= '<p>[zippy_button type="register-login"]</p>';

        if (!$zippy->core_pages->get('thank_you')) {
            return;
        }

        $thank_you_content = $wpdb->get_var($wpdb->prepare("SELECT post_content FROM $wpdb->posts WHERE ID = %s", $zippy->core_pages->get('thank_you')));

        if (empty($thank_you_content)) {
            $sql = $wpdb->prepare("UPDATE $wpdb->posts SET post_content = %s WHERE ID = %d", $thank_you_content, $zippy->core_pages->get('thank_you'));
            $wpdb->query($sql);
        }
    }


    /**
     * Upgrade the database to 1.1.0
     * @return void
     */
    public static function databaseUpgrade_v1_1_0()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $db_version     = get_option('zippy_db_version', false);

        if ($db_version && version_compare($db_version, '1.1.0', '<')) {
            $product_ids = $wpdb->get_col(
                $wpdb->prepare(
                    "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s",
                    "scheduling_override",
                    "1"
                )
            );

            foreach ($product_ids as $product_id) {
                update_post_meta($product_id, 'overrides_enabled', 1);
            }

            update_option('zippy_db_version', '1.1.0');
        }
    }

    public static function clearMiddlewareCache()
    {
        global $wpdb;

        $wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'zippy_middleware_rules_cache'");
        $wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'zippy_middleware_rules_cache_timestamp'");
    }
}
