<?php


class ZippyCourses_Uninstaller
{
    private static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        
    }


    /**
     * Fired when the plugin is deactivated.
     *
     * @since   0.9
     *
     * @param    boolean    $network_wide    True if WPMU superadmin uses
     *                                       "Network Deactivate" action, false if
     *                                       WPMU is disabled or plugin is
     *                                       deactivated on an individual blog.
     */
    public static function deactivate( $network_wide ) {

        if ( function_exists( 'is_multisite' ) && is_multisite() ) {

            if ( $network_wide ) {

                // Get all blog ids
                $blog_ids = self::get_blog_ids();

                foreach ( $blog_ids as $blog_id ) {

                    switch_to_blog( $blog_id );
                    self::singleDeactivate();

                }

                restore_current_blog();

            } else {
                self::singleDeactivate();
            }

        } else {
            self::singleDeactivate();
        }

    }    

    /**
     * Fired for each blog when the plugin is deactivated.
     *
     * @since   1.0
     */
    private static function singleDeactivate() {   

    }    
}
