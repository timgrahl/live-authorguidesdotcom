<?php

class ZippyCourses_ResetPassword_Form extends Zippy_Form
{
    public function __construct($id)
    {
        parent::__construct($id);
    }

    public function defaultFields()
    {
        $zippy          = Zippy::instance();

        $key        = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_STRING);
        $user_id    = filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING);

        $this->addPasswordField('reset-password', __('Password', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required');

        $this->addPasswordField('password_confirm', __('Confirm Password', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,equalTo:reset-password');

        $this->addHiddenField('password_reset_key')
             ->setSavable(false)
             ->setValue($key);

        $this->addHiddenField('user_id')
             ->setSavable(false)
             ->setValue($user_id);

    }
}
