<?php

class ZippyCourses_EditAccount_Form extends Zippy_Form
{
    public function __construct($id)
    {
        parent::__construct($id);

        $this->setSubmitText(__('Update Details', ZippyCourses::TEXTDOMAIN));
    }

    public function defaultFields()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        $this->addTextField('first_name', __('First Name', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->setValue($student->getFirstName())
             ->setDefault($student->getFirstName())
             ->validate('required,minlength:2');

        $this->addTextField('last_name', __('Last Name', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->setValue($student->getLastName())
             ->setDefault($student->getLastName())
             ->validate('required,minlength:2');
             
        $this->addEmailField('user_email', __('Email Address', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->setValue($student->getEmail())
             ->setDefault($student->getEmail())
             ->validate('required,email');

        $this->addTextField('user_login', __('Username', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->setValue($student->getUsername())
             ->setDefault($student->getUsername())
             ->addAttribute('disabled', 'disabled');

        $this->addTextField('user_display_name', __('Display Name As:', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->setValue($student->getDisplayName())
             ->setDefault($student->getDisplayName());

        $this->addHiddenField('user_id')
             ->setSavable(false)
             ->setValue($student->getId());
    }
}
