<?php

class ZippyCourses_Test_Form extends Zippy_Form
{
    public function __construct($id)
    {
        $zippy = Zippy::instance();

        $this->setSubmitText(__('Upload', ZippyCourses::TEXTDOMAIN));

        parent::__construct($id);
    }

    public function defaultFields()
    {
        $zippy          = Zippy::instance();

        $this->addRadioField('working', array(
            'foo',
            'bar',
            'damn',
            'girl'
        ))
             ->setSavable(false);
    }
}
