<?php

class ZippyCourses_LandingPageProduct_Form extends Zippy_Form
{
    public function __construct($id)
    {
        parent::__construct($id);

        $this->setSubmitText(__('Update Details', ZippyCourses::TEXTDOMAIN));
    }

    public function defaultFields()
    {
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        $student_id = !is_null($student) ? $student->getId() : 0;

        $this->addTextField('full_name', __('Full Name', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,minlength:2');

        $this->addEmailField('user_email', __('Email Address', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,email,unique_email');

        $this->addTextField('user_login', __('Username', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,unique_username');

        $this->addPasswordField('user_pass', __('Password', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,minlength:7');

        $this->addHiddenField('student_id')
             ->setSavable(false)
             ->setValue($student_id);

        $this->addHiddenField('landing_page_id')
             ->setSavable(false);
    }
}
