<?php

class ZippyCourses_StudentImportGrantAccess_Form extends Zippy_Form
{
    public function __construct($id)
    {
        $zippy = Zippy::instance();

        $this->setBottom('');

        $this->setSubmitText(__('Grant Access', ZippyCourses::TEXTDOMAIN));

        parent::__construct($id);
    }

    public function defaultFields()
    {
        $zippy          = Zippy::instance();

        $this->addHiddenField('student_ids');
        $this->addCheckboxField('products', array())
             ->setSavable(false);
    }
}
