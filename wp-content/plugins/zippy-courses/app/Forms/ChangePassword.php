<?php

class ZippyCourses_ChangePassword_Form extends Zippy_Form
{
    public function __construct($id)
    {
        parent::__construct($id);

        $this->setSubmitText(__('Change Password', ZippyCourses::TEXTDOMAIN));
    }

    public function defaultFields()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        $this->addPasswordField('password', __('Password', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,minlength:7');

        $this->addPasswordField('confirm_password', __('Confirm Password', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,equalTo:password');

        $this->addHiddenField('user_id')
             ->setSavable(false)
             ->setValue($student->getId());
    }
}
