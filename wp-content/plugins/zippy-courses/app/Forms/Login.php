<?php

class ZippyCourses_Login_Form extends Zippy_Form
{
    public function __construct($id)
    {
        $zippy = Zippy::instance();

        $this->setBottom('<a href="' . $zippy->core_pages->getUrl('forgot_password') . '" class="forgot-password-link">' . __('Forgot Password?', ZippyCourses::TEXTDOMAIN) . '</a>');

        $this->setSubmitText(__('Login', ZippyCourses::TEXTDOMAIN));

        parent::__construct($id);
    }

    public function defaultFields()
    {
        $zippy          = Zippy::instance();

        $claim          = filter_input(INPUT_GET, 'claim', FILTER_SANITIZE_STRING);
        $redirect_to    = filter_input(INPUT_GET, 'redirect_to', FILTER_SANITIZE_STRING);

        $redirect_to    = empty($redirect_to) ? $zippy->core_pages->getUrl('dashboard') : $redirect_to;

        $this->addTextField('username', __('Username', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->setPlaceholder(__('Username', ZippyCourses::TEXTDOMAIN))
             ->validate('required');

        $this->addPasswordField('password', __('Password', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->setPlaceholder(__('Password', ZippyCourses::TEXTDOMAIN))
             ->validate('required');

        $this->addHiddenField('claim')
             ->setSavable(false)
             ->setValue($claim);

        $this->addHiddenField('redirect_to')
             ->setSavable(false)
             ->setValue($redirect_to);
    }
}
