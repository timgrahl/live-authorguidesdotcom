<?php

class ZippyCourses_NewStudent_Form extends Zippy_Form
{
    public function __construct($id)
    {
        parent::__construct($id);

        $this->addClass('zippy-new-student-form');
        $this->setSubmitText(__('Create Student', ZippyCourses::TEXTDOMAIN));

        $this->addSubmitClass('button');
        $this->addSubmitClass('button-primary');
    }

    public function defaultFields()
    {
        $zippy = Zippy::instance();

        $redirect    = empty($redirect) ? home_url() : $redirect;
        
        $this->addTextField('first_name', __('First Name', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,minlength:2');

        $this->addTextField('last_name', __('Last Name', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,minlength:2');
        
        $this->addEmailField('user_email', __('Email Address', ZippyCourses::TEXTDOMAIN))
            ->setSavable(false)
            ->validate('required,email,unique_email');

        $this->addTextField('user_login', __('Username', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,unique_username');

        $this->addPasswordField('user_pass', __('Password', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,password,minlength:7');

        $this->addPasswordField('user_pass2', __('Password Confirmation', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,password,equalTo:user_pass');
    }
}
