<?php

class ZippyCourses_ForgotPassword_Form extends Zippy_Form
{
    public function __construct($id)
    {
        parent::__construct($id);
    }

    public function defaultFields()
    {
        $zippy          = Zippy::instance();

        $this->addTextField('user_login_or_email', __('Username or Email', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required');
    }
}
