<?php

class ZippyCourses_LandingPageList_Form extends Zippy_Form
{
    public function __construct($id)
    {
        parent::__construct($id);

        $this->setSubmitText(__('Update Details', ZippyCourses::TEXTDOMAIN));
    }

    public function defaultFields()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        $this->addTextField('full_name', __('Full Name', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,minlength:2');

        $this->addEmailField('user_email', __('Email Address', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,email');

        $this->addHiddenField('student_id')
             ->setSavable(false)
             ->setValue($student_id);

        $this->addHiddenField('landing_page_id')
             ->setSavable(false);
    }
}
