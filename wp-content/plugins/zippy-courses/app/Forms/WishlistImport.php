<?php

class ZippyCourses_WishlistImport_Form extends Zippy_Form
{
    public function __construct($id)
    {
        $zippy = Zippy::instance();

        $this->setBottom('');

        $this->setSubmitText(__('Upload', ZippyCourses::TEXTDOMAIN));

        parent::__construct($id);
    }

    public function defaultFields()
    {
        $zippy          = Zippy::instance();

        $this->addHiddenField('zippy_import_file', '')
             ->setSavable(false);

        $this->addHiddenField('zippy_import_action')
            ->setValue('import-wishlist-summary')
            ->setSavable(false);

        $this->addHiddenField('summary', '')
            ->setSavable(false);
    }
}
