<?php

class ZippyCourses_Registration_Form extends Zippy_Form
{
    public function __construct($id)
    {
        $this->addClass('zippy-registration-form');
        parent::__construct($id);
    }

    public function defaultFields()
    {
        $zippy = Zippy::instance();

        $claim       = filter_input(INPUT_GET, 'claim', FILTER_SANITIZE_STRING);
        $qv_claim    = get_query_var('zippy_register');
        $redirect    = filter_input(INPUT_GET, 'redirect_to', FILTER_SANITIZE_STRING);
        $redirect    = empty($redirect) ? $zippy->core_pages->getUrl('dashboard') : $redirect;

        // Check for a valid free register query var
        if (empty($claim) && !empty($qv_claim)) {
            $product = $zippy->utilities->product->getByUid($qv_claim);

            if ($product->getType() == 'free' || ($product->getType() == 'single' && $product->getAmount() == 0)) {
                $claim = $qv_claim;
            }
        }
        
        $transaction = $zippy->make('transaction');
        $transaction->buildByTransactionKey($claim);

        $customer = $transaction->getCustomer();

        $this->addTextField('first_name', __('First Name', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,minlength:2');

        $this->addTextField('last_name', __('Last Name', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,minlength:2');
        
        if ($customer instanceof Zippy_Customer) {
            $validator = $zippy->make('validator');
            $requires_payment_email = $zippy->utilities->transaction->requiresPaymentEmail($transaction);
            $email = $customer->getEmail();

            if (!empty($email) && $validator->uniqueEmail($customer->getEmail()) && $requires_payment_email) {
                $this->addEmailField('user_email_display', __('Email Address', ZippyCourses::TEXTDOMAIN))
                     ->setSavable(false)
                     ->setValue($customer->getEmail())
                     ->addAttribute('disabled', 'disabled')
                     ->validate('required,email,unique_email');

                $this->addHiddenField('user_email')
                     ->setSavable(false)
                     ->setValue($customer->getEmail());
            } else {
                $this->addEmailField('user_email', __('Email Address', ZippyCourses::TEXTDOMAIN))
                 ->setSavable(false)
                 ->validate('required,email,unique_email');
            }
        } else {
            $this->addEmailField('user_email', __('Email Address', ZippyCourses::TEXTDOMAIN))
                 ->setSavable(false)
                 ->validate('required,email,unique_email');
        }

        $this->addTextField('user_login', __('Username', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,unique_username');

        $this->addPasswordField('user_pass', __('Password', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,password,minlength:7');

        $this->addPasswordField('user_pass2', __('Password Confirmation', ZippyCourses::TEXTDOMAIN))
             ->setSavable(false)
             ->validate('required,password,equalTo:user_pass');

        $this->addHiddenField('claim')
             ->setSavable(false)
             ->setValue($claim);

        $this->addHiddenField('redirect_to')
             ->setSavable(false)
             ->setValue($redirect);
    }
}
