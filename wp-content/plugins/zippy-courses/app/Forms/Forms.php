<?php

class ZippyCourses_Forms
{
    public function __construct()
    {
        $zippy = Zippy::instance();
        $forms = $zippy->make('forms_repository');

        // Setup the initial forms!
        $forms->register('registration', 'ZippyCourses_Registration_Form');
        $forms->register('new_student', 'ZippyCourses_NewStudent_Form');
        $forms->register('order', 'ZippyCourses_Order_Form');
        $forms->register('login', 'ZippyCourses_Login_Form');
        $forms->register('import', 'ZippyCourses_Import_Form');
        $forms->register('import_student', 'ZippyCourses_StudentImport_Form');
        $forms->register('import_student_grant_access', 'ZippyCourses_StudentImportGrantAccess_Form');
        $forms->register('import_wishlist', 'ZippyCourses_WishlistImport_Form');
        $forms->register('import_wishlist_students', 'ZippyCourses_WishlistImportStudents_Form');
        $forms->register('forgot_password', 'ZippyCourses_ForgotPassword_Form');
        $forms->register('reset_password', 'ZippyCourses_ResetPassword_Form');
        $forms->register('edit_account', 'ZippyCourses_EditAccount_Form');
        $forms->register('change_password', 'ZippyCourses_ChangePassword_Form');
        $forms->register('landing_page_product', 'ZippyCourses_LandingPageProduct_Form');
        $forms->register('landing_page_list', 'ZippyCourses_LandingPageList_Form');
        $forms->register('test', 'ZippyCourses_Test_Form');
    }
}
