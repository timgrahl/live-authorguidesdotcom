<?php

class ZippyCourses_StudentImport_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        $zippy = Zippy::instance();

        $file = $this->form->fields->get('zippy_import_file')->getValue();
        $action = $this->form->fields->get('zippy_import_action')->getValue();

        $upload = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));
        $importer = new ZippyCourses_Student_Importer($upload['file']);

        $url = $importer->validate()
            ? admin_url('/admin.php?page=zippy-import-export&importer=student&step=2&src=' . $upload['file'])
            : admin_url('/admin.php?page=zippy-import-export&importer=student&step=error');
            
        wp_redirect($url);
        exit;
    }

    public function fail()
    {
    }
}