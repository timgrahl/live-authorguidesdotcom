<?php

class ZippyCourses_LandingPageProduct_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        $zippy = Zippy::instance();

        $student_id       = $this->form->getFields()->get('student_id')->getValue();
        $username       = $this->form->getFields()->get('user_login')->getValue();
        $full_name       = $this->form->getFields()->get('full_name')->getValue();
        $password       = $this->form->getFields()->get('user_pass')->getValue();
        $email       = $this->form->getFields()->get('user_email')->getValue();
        $landing_page_id =  $this->form->getFields()->get('landing_page_id')->getValue();

        $landing_page   = $zippy->utilities->getJsonMeta($landing_page_id, 'landing_page', true);
        $product_id     = isset($landing_page->product) && !empty($landing_page->product) ? $landing_page->product : 0;

        if (!$product_id) {
            return;
        }

        $student = $student_id ? $zippy->make('student', array($student_id)) : $zippy->make('student');
        $student->setPassword($password);
        $student->setUsername($username);
        $student->setEmail($email);
        $student->setFullName($full_name);
        $student->analyzeName($full_name);

        if (!$student_id) {
            $student->register();
            $student->auth();
        }

        $zippy->access->grant($student->getId(), $product_id);
        wp_redirect($zippy->core_pages->getUrl('dashboard'));
        exit;
    }

    public function fail()
    {
        $zippy = Zippy::instance();

        $message = '<p>' . __('We could not process your submission for the following reasons:', ZippyCourses::TEXTDOMAIN) . '</p>';
        $message .= '<ul>';
        foreach ($this->getValidationMessages() as $m) {
            $message .= '<li>' . $m . '</li>';
        }
        $message .= '</ul>';
        $message .= '<p>' . __('Please try again.', ZippyCourses::TEXTDOMAIN) . '</p>';
        
        $zippy->sessions->flashMessage($message, 'error', 0);
    }
}
