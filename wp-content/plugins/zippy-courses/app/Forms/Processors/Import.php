<?php

class ZippyCourses_Import_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        $zippy = Zippy::instance();

        $file   = $this->form->fields->get('zippy_import_file')->getValue();
        $action = $this->form->fields->get('zippy_import_action')->getValue();

        switch ($action) {
            case 'import-zippy-courses':
                $upload = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));
                $importer = new ZippyCourses_ZippyCourses_Importer($upload['file']);

                $url = $importer->validate()
                    ? admin_url('/admin.php?page=zippy-import-export&importer=zippy-courses&step=2&src=' . $upload['file'])
                    : admin_url('/admin.php?page=zippy-import-export&importer=zippy-courses&step=error');
                    
                wp_redirect($url);
                exit;
                break;
            case 'import-student':
                $upload = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));
                $importer = new ZippyCourses_Student_Importer($upload['file']);

                $url = $importer->validate()
                    ? admin_url('/admin.php?page=zippy-import-export&importer=student&step=2&src=' . $upload['file'])
                    : admin_url('/admin.php?page=zippy-import-export&importer=student&step=error');
                    
                wp_redirect($url);
                exit;
                break;
            case 'import-wlm-content':
                $upload = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));
                $importer = new ZippyCourses_WishlistMember_Importer($upload['file']);

                $url = $importer->validate()
                    ? admin_url('/admin.php?page=zippy-import-export&importer=wishlist-member&step=2&src=' . $upload['file'])
                    : admin_url('/admin.php?page=zippy-import-export&importer=wishlist-member&step=error');
                    
                wp_redirect($url);
                exit;
                break;
            default:
                break;
        }
    }

    public function fail()
    {
    }
}