<?php

class ZippyCourses_WishlistImportStudents_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        $zippy = Zippy::instance();

        $file = $this->form->fields->get('zippy_import_file')->getValue();

        $upload = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));
        $importer = new ZippyCourses_WishlistMember_Importer($upload['file']);

        $url = $importer->validate()
            ? admin_url('/admin.php?page=zippy-import-export&importer=wishlist-member-students&step=2a&src=' . $upload['file'])
            : admin_url('/admin.php?page=zippy-import-export&importer=wishlist-member-students&step=error');
            
        wp_redirect($url);
        exit;
    }

    public function fail()
    {
    }
}