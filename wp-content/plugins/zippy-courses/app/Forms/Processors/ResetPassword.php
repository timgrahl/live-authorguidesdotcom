<?php

class ZippyCourses_ResetPassword_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $user_id    = $this->form->getFields()->get('user_id')->getValue();
        $password   = $this->form->getFields()->get('reset-password')->getValue();
        $key        = $this->form->getFields()->get('password_reset_key')->getValue();

        wp_set_password($password, $user_id);
        
        $zippy->utilities->user->deleteUsedPasswordResetKey($user_id, $key);

        $zippy->sessions->flashMessage(
            __("Your password has been changed. You may login to your account with your new password.", ZippyCourses::TEXTDOMAIN),
            'success'
        );

        wp_redirect($zippy->core_pages->getUrl('login'));
        exit;
    }

    public function fail()
    {
        $zippy = Zippy::instance();

        $message  = '<p><strong>' . __('Please fix the following errors to complete this form:', ZippyCourses::TEXTDOMAIN) . '</strong></p>';
        $message .= '<ul>';
        foreach ($this->getValidationMessages() as $m) {
            $message .= '<li>' . $m . '</li>';
        }
        $message .= '</ul>';

        $zippy->sessions->flashMessage($message, 'error', 0);
    }
}
