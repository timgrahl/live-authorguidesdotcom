<?php

class ZippyCourses_ChangePassword_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        $zippy = Zippy::instance();

        $user_id    = $this->form->getFields()->get('user_id')->getValue();
        $password   = $this->form->getFields()->get('password')->getValue();

        wp_set_password($password, $user_id);

        $zippy->sessions->flashMessage(
            __("Your password has been changed. Please login to your account with your new password.", ZippyCourses::TEXTDOMAIN),
            'success'
        );

        wp_redirect($zippy->core_pages->getUrl('login'));
        exit;
    }

    public function fail()
    {
        $zippy = Zippy::instance();

        $message  = '<p><strong>' . __('Your password was not changed because of the following errors:', ZippyCourses::TEXTDOMAIN) . '</strong></p>';
        
        $message .= '<ul>';
        foreach ($this->getValidationMessages() as $m) {
            $message .= '<li>' . $m . '</li>';
        }
        $message .= '</ul>';

        $zippy->sessions->flashMessage($message, 'error', 0);
    }
}
