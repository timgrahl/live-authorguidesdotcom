<?php

class ZippyCourses_NewStudent_FormProcessor extends Zippy_FormProcessor
{
    public function fail()
    {
        $zippy = Zippy::instance();
    }

    public function execute()
    {
        $zippy = Zippy::instance();

        $student = $this->createStudent();

        if ($student->ID) {
            $url = admin_url('admin.php?page=zippy-student&ID=' . $student->ID);
            wp_redirect($url);
            exit;
        } else {
        }
    }

    private function createStudent()
    {
        global $zippy_student;

        $zippy = Zippy::instance();

        $student = $zippy->make('student');

        $student->username      = $this->form->getFields()->get('user_login')->getValue();
        $student->first_name    = $this->form->getFields()->get('first_name')->getValue();
        $student->last_name     = $this->form->getFields()->get('last_name')->getValue();
        $student->email         = $this->form->getFields()->get('user_email')->getValue();
        $student->password      = $this->form->getFields()->get('user_pass')->getValue();
        $student->role          = 'subscriber';

        $student->register();

        $zippy->cache->set('student', $student);
        
        $zippy->activity->insert($student->getId(), 0, 'registration');

        return $student->getId() ? $this->saveRegistrationMeta($student) : $student;
    }

    private function saveRegistrationMeta(ZippyCourses_Student $student)
    {
        foreach ($this->form->getFields()->all() as $field) {
            if ($field->getSavable()) {
                $student->saveMeta($field->getName(), $field->getValue());
            }
        }

        return $student;
    }
}
