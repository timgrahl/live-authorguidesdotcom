<?php

class ZippyCourses_Order_FormProcessor extends Zippy_FormProcessor
{
    public function process()
    {
        if ($this->requiresRegistration()) {
            $this->redirectToRegistration();
        } else {
            if ($this->validate()) {
                $this->execute();
            } else {
                $this->fail();
            }
        }

        return;
    }

    public function requiresRegistration()
    {
        $settings = get_option('zippy_payment_general_settings', array());
        $payment_flow = isset($settings['payment_flow']) ? $settings['payment_flow'] : '1';

        return $payment_flow == '0' && !is_user_logged_in();
    }

    public function redirectToRegistration()
    {
        $zippy = Zippy::instance();

        $product_id = filter_input(INPUT_POST, 'product_id');
        $product = $zippy->make('product', array($product_id));
        $redirect   = $zippy->core_pages->getUrl('buy') . $product_id . '?buy-now';
        $url        = $zippy->core_pages->getUrl('register') . '?redirect_to=' . urlencode($redirect);

        $login_url = $zippy->core_pages->getUrl('login') . '?redirect_to=' . urlencode($zippy->core_pages->getUrl('buy') . $product->getId() . '?buy-now');


        $zippy->sessions->flashMessage(
            sprintf(
                __('You must register to purchase %s. If you already have an account, <a href="%s">click here</a> to login.', ZippyCourses::TEXTDOMAIN),
                '<strong>' . $product->getTitle() . '</strong>',
                $login_url
            ),
            'warning'
        );

        wp_redirect($url);
        exit;
    }

    public function fail()
    {
        $zippy = Zippy::instance();
        
        $zippy->sessions->flashMessage(__('This product cannot be purchased at this time.', ZippyCourses::TEXTDOMAIN), 'error', 0);

        return null;
    }

    public function execute()
    {
        return $this->fail();
    }
}
