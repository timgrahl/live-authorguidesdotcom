<?php

class ZippyCourses_LandingPageList_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        $zippy = Zippy::instance();

        $student_id = $this->form->getFields()->get('student_id')->getValue();
        $student    = $student_id ? $zippy->make('student', array($student_id)) : false;
        if ($student){
            $student->fetch();
        }
        

        if ($student_id && $student && $student->getEmail()) {
            $full_name  = $student->getFullName();
            $email      = $student->getEmail();
        } else {
            $full_name  = $this->form->getFields()->get('full_name')->getValue();
            $email      = $this->form->getFields()->get('user_email')->getValue();

            $student = $zippy->make('student');
            $student->setEmail($email);
            $student->setFullName($full_name);
            $student->analyzeName($full_name);
        }

        $landing_page_id    = $this->form->getFields()->get('landing_page_id')->getValue();
        $landing_page       = $zippy->utilities->getJsonMeta($landing_page_id, 'landing_page', true);
        $list               = isset($landing_page->list) ? new Zippy_EmailList($landing_page->list) : null;
        $service            = isset($landing_page->service) ? $landing_page->service : 0;
        $thank_you_url      = isset($landing_page->thank_you_url) ? $landing_page->thank_you_url: home_url();

        if ($list && $student && $service) {
            $event = new ZippyCourses_LandingPageSubscribe_Event($student, $list, $service);
            $zippy->events->fire($event);

            wp_redirect($thank_you_url);
            exit;
        }

        wp_redirect(home_url());
        exit;
    }

    public function fail()
    {
        $zippy = Zippy::instance();

        $message = '<p>' . __('We could not process your submission for the following reasons:', ZippyCourses::TEXTDOMAIN) . '</p>';
        $message .= '<ul>';
        foreach ($this->getValidationMessages() as $m) {
            $message .= '<li>' . $m . '</li>';
        }
        $message .= '</ul>';
        $message .= '<p>' . __('Please try again.', ZippyCourses::TEXTDOMAIN) . '</p>';
        
        $zippy->sessions->flashMessage($message, 'error', 0);
    }
}
