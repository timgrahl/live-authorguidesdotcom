<?php

class ZippyCourses_Login_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        $zippy = Zippy::instance();

        $username       = $this->form->getFields()->get('username')->getValue();
        $password       = $this->form->getFields()->get('password')->getValue();
        $claim          = $this->form->getFields()->get('claim')->getValue();
        $redirect_to    = $this->form->getFields()->get('redirect_to')->getValue();

        $user = wp_signon(
            array(
                'user_login'    => $username,
                'user_password' => $password,
                'remember'      => true
            ),
            is_ssl()
        );

        if (is_wp_error($user)) {
            $zippy->sessions->flashMessage(
                $user->get_error_message(),
                'error',
                0
            );
        } else {
            $redirect_to = apply_filters('zippy_courses_login_redirect', $redirect_to, $user->ID);
            if (!empty($claim)) {
                $student = $zippy->make('student', array($user->ID));
                $student->fetch();

                $this->processClaim($student, $claim);
                $redirect_to = apply_filters('zippy_courses_purchase_complete_redirect', $redirect_to, $user->ID);
            }

            wp_redirect($redirect_to);
            exit;
        }
    }

    public function fail()
    {
    }

    private function processClaim(ZippyCourses_Student $student, $transaction_key)
    {
        $zippy = Zippy::instance();
        
        $transaction = $zippy->make('transaction');
        $transaction->buildByTransactionKey($transaction_key);

        if ($transaction->getProduct() instanceof Zippy_Product) {
            $zippy->access->claim($student, $transaction);
        } else {
            $product = $zippy->utilities->product->getByUid($transaction_key);

            if ($product !== null) {
                $zippy->access->grant($student->getId(), $product->getId(), true);
            }
        }
    }
}
