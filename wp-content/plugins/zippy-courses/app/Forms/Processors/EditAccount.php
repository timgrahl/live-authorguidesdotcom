<?php

class ZippyCourses_EditAccount_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $user_id        = $this->form->getFields()->get('user_id')->getValue();
        $username       = $this->form->getFields()->get('user_login')->getValue();
        $first_name     = $this->form->getFields()->get('first_name')->getValue();
        $last_name      = $this->form->getFields()->get('last_name')->getValue();
        $email          = $this->form->getFields()->get('user_email')->getValue();
        $display_name   = $this->form->getFields()->get('user_display_name')->getValue();

        $check_username = username_exists($username) !== false && username_exists($username) != $user_id;
        $check_email    = email_exists($email) !== false && email_exists($email) != $user_id;

        $messages = array();

        if ($check_username) {
            $messages[] = __('That username is already in use. Please choose another.', ZippyCourses::TEXTDOMAIN);
        }

        if ($check_email) {
            $messages[] = __('That email is already in use. Please choose another.', ZippyCourses::TEXTDOMAIN);
        }

        if (count($messages)) {
            return $this->processingError($messages);
        }

        $student = $zippy->make('student', array($user_id));
        $student->fetch();

        $student->setEmail($email);
        $student->setUsername($username);
        $student->setFirstName($first_name);
        $student->setLastName($last_name);
        $student->setDisplayName($display_name);

        $student->update();

        $zippy->sessions->flashMessage(
            __('Your account details have been updated.', ZippyCourses::TEXTDOMAIN),
            'success',
            0
        );
    }

    public function fail()
    {
        $zippy = Zippy::instance();

        $message  = '<p><strong>' . __('Your account could not be updated because of the following errors:', ZippyCourses::TEXTDOMAIN) . '</strong></p>';
        
        $message .= '<ul>';
        foreach ($this->getValidationMessages() as $m) {
            $message .= '<li>' . $m . '</li>';
        }
        $message .= '</ul>';

        $zippy->sessions->flashMessage($message, 'error', 0);
    }

    private function processingError($messages)
    {
        $zippy = Zippy::instance();

        $message  = '<p><strong>' . __('Your account could not be updated because of the following errors:', ZippyCourses::TEXTDOMAIN) . '</strong></p>';
        
        $message .= '<ul>';
        foreach ($messages as $m) {
            $message .= '<li>' . $m . '</li>';
        }
        $message .= '</ul>';

        $zippy->sessions->flashMessage($message, 'error', 0);

    }
}
