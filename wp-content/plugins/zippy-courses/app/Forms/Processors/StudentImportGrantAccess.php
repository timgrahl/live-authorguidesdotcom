<?php

class ZippyCourses_StudentImportGrantAccess_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        $zippy = Zippy::instance();

        $student_ids = $this->form->fields->get('student_ids')->getValue();
        $student_ids = array_filter(explode(',', $student_ids));
        $products = $this->form->fields->get('products')->getValue();

        if (is_array($student_ids) && is_array($products)) {
            foreach ($student_ids as $student_id) {
                foreach ($products as $product_id) {
                    $zippy->access->grant($student_id, $product_id);
                }
            }
        }
            
        wp_redirect(admin_url('/admin.php?page=zippy-import-export&importer=student&step=3'));
        exit;
    }

    public function fail()
    {
    }
}
