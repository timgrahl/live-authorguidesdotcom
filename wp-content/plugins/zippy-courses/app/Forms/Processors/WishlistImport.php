<?php

class ZippyCourses_WishlistImport_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        $zippy = Zippy::instance();

        $file       = $this->form->fields->get('zippy_import_file')->getValue();
        $summary    = $this->form->fields->get('summary')->getValue();
            
        set_transient('zippy_wlm_import_summary', $summary, 15 * MINUTE_IN_SECONDS);

        $url = admin_url('/admin.php?page=zippy-import-export&importer=wishlist-member&step=3&src=' . $file);

        wp_redirect($url);
        exit;
    }

    public function fail()
    {
    }
}