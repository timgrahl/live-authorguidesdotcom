<?php

class ZippyCourses_ForgotPassword_FormProcessor extends Zippy_FormProcessor
{
    public function execute()
    {
        $zippy = Zippy::instance();

        $user = get_user_by('email', $_POST['user_login_or_email']);
        $user = $user instanceof WP_User ? $user : get_user_by('login', $_POST['user_login_or_email']);
        
        if ($user) {
            $student = $zippy->make('student', array($user->ID));
            $student->fetch();

            $key = $zippy->utilities->user->generatePasswordResetKey();
            $student->addPasswordResetKey($key);
            
            $zippy->events->fire(new ZippyCourses_ForgotPassword_Event($student));

            $zippy->sessions->flashMessage(
                __("Success!  Please check your inbox for details on resetting your password.", ZippyCourses::TEXTDOMAIN),
                'success',
                0
            );

        } else {
            $this->processingError();
        }
    }

    public function fail()
    {
    }

    private function processingError()
    {
        $zippy = Zippy::instance();
        
        $zippy->sessions->flashMessage(
            __("We could not find a user with that email or username. Please try again.", ZippyCourses::TEXTDOMAIN),
            'error',
            0
        );
    }
}
