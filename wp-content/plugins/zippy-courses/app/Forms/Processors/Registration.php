<?php

class ZippyCourses_Registration_FormProcessor extends Zippy_FormProcessor
{
    public function fail()
    {
        $zippy = Zippy::instance();
        // $zippy->session->flashMessage('There was an error processing your request', 'error');
    }

    public function execute()
    {
        $zippy = Zippy::instance();

        $student = $this->createStudent();

        if ($student->ID) {
            $student->auth();

            // Claim any orders that are present
            $txn_key  = $this->form->getFields()->get('claim')->getValue();
            
            if ($txn_key) {
                $this->processClaim($student, $txn_key);
            }
            
            $this->redirect();

        } else {
        }
    }

    private function createStudent()
    {
        global $zippy_student;

        $zippy = Zippy::instance();

        $student = $zippy->make('student');

        $student->username      = $this->form->getFields()->get('user_login')->getValue();
        $student->first_name    = $this->form->getFields()->get('first_name')->getValue();
        $student->last_name     = $this->form->getFields()->get('last_name')->getValue();
        $student->email         = $this->form->getFields()->get('user_email')->getValue();
        $student->password      = $this->form->getFields()->get('user_pass')->getValue();
        $student->role          = 'subscriber';

        $student->register();

        $zippy->cache->set('student', $student);
        
        $zippy->activity->insert($student->getId(), 0, 'registration');

        return $student->getId() ? $this->saveRegistrationMeta($student) : $student;
    }

    private function saveRegistrationMeta(ZippyCourses_Student $student)
    {
        foreach ($this->form->getFields()->all() as $field) {
            if ($field->getSavable()) {
                $student->saveMeta($field->getName(), $field->getValue());
            }
        }

        return $student;
    }

    /**
     * Claim keys are one of two things: A Transaction Key or a Product UID.  Both are unique ids for those resources.
     * If the transaction has a product, we process it as a transaction key, otherwise we attempt to process it as a
     * free product.
     *
     * @since  1.0.0
     *
     * @param  ZippyCourses_Student $student
     * @param  string               $claim_key
     *
     * @return void
     */
    private function processClaim(ZippyCourses_Student $student, $claim_key)
    {
        $zippy = Zippy::instance();
        
        $transaction = $zippy->make('transaction');
        $transaction->buildByTransactionKey($claim_key);

        $product = $transaction->getProduct();

        if ($product instanceof Zippy_Product) {
            $zippy->access->claim($student, $transaction);
        } else {
            $product = $zippy->utilities->product->getByUid($claim_key);

            if ($product instanceof Zippy_Product && $product->getType() == 'free') {
                $zippy->access->grant($student->getId(), $product->getId(), true);
            }
        }
    }

    private function redirect()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        // Flash a quick message saying welcome
        $zippy->sessions->flashMessage(sprintf(__('Welcome to %1$s!', ZippyCourses::TEXTDOMAIN), get_bloginfo('name')));
        $redirect_query = filter_input(INPUT_GET, 'redirect_to');

        $redirect = !empty($redirect_query) ? $redirect_query : $this->form->getFields()->get('redirect_to')->getValue();
        if ($student->getId()) {
            $redirect = apply_filters('zippy_courses_login_redirect', $redirect, $student->getId());
            $redirect = apply_filters('zippy_courses_purchase_complete_redirect', $redirect, $student->getId());
            wp_redirect($redirect);
            exit;
        }
    }
}
