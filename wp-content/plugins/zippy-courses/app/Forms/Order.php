<?php

class ZippyCourses_Order_Form extends Zippy_Form
{
    public function __construct($id)
    {
        $zippy = Zippy::instance();

        $this->setSubmitText(__('Order Now', ZippyCourses::TEXTDOMAIN));

        parent::__construct($id);
    }

    public function defaultFields()
    {
    }
}
