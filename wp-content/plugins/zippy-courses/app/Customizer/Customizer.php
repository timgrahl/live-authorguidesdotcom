<?php

class ZippyCourses_Customizer
{
    public function __construct()
    {
        $this->controls();
        
        add_action('wp_head', array($this, 'styles'));
    }

    public function controls()
    {
        $zippy = Zippy::instance();

        $panel = $zippy->make(
            'customizer_panel',
            array(
                'id'        => 'zippy',
                'title'     => 'Zippy Courses'
            )
        );

        $this->_styleSections($panel);
        $this->_courseSections($panel);
        $this->_unitSections($panel);
        $this->_dashboardSections($panel);
        $this->_directorySections($panel);

        $zippy->customizer->panels->add($panel);
    }

    public function _courseSections(Zippy_Customizer_Panel $panel)
    {
        $zippy = Zippy::instance();
        $section = $zippy->make(
            'customizer_section',
            array(
                'id'        => 'course_options',
                'title'     => __('Course Options', ZippyCourses::TEXTDOMAIN)
            )
        );

        $section->setDescription(
            __(
                'Control various display options when viewing a Course.',
                ZippyCourses::TEXTDOMAIN
            )
        );

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'show_excerpt',
                'label'     => __('Show Excerpts?', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
        $section->controls->add($control);

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'show_featured_media',
                'label'     => __('Show Featured Media?', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
        $section->controls->add($control);

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'show_progress',
                'label'     => __('Show Lessons Completion Percentage?', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
        $section->controls->add($control);
        
        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'units_lessons_completion_percentage',
                'label'     => __('Include both Units and Lessons in Completion Percentages?', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            '0' => __('Both Units and Lessons', ZippyCourses::TEXTDOMAIN),
            '1' => __('Only Units', ZippyCourses::TEXTDOMAIN),
            '2' => __('Only Lessons', ZippyCourses::TEXTDOMAIN),
        ));
        $section->controls->add($control);
        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'show_lesson_completion_button',
                'label'     => __('Lesson Completion Button Location', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            '0' => __('None', ZippyCourses::TEXTDOMAIN),
            '1' => __('Top', ZippyCourses::TEXTDOMAIN),
            '2' => __('Bottom', ZippyCourses::TEXTDOMAIN),
            '3' => __('Both', ZippyCourses::TEXTDOMAIN),

        ));
        $section->controls->add($control);

        $panel->sections->add($section);
    }

    public function _unitSections(Zippy_Customizer_Panel $panel)
    {
        $zippy = Zippy::instance();
        $section = $zippy->make(
            'customizer_section',
            array(
                'id'        => 'unit_options',
                'title'     => __('Unit Options', ZippyCourses::TEXTDOMAIN)
            )
        );

        $section->setDescription(
            __(
                'Control various display options when viewing a Unit.',
                ZippyCourses::TEXTDOMAIN
            )
        );

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'show_excerpt',
                'label'     => __('Show Excerpts?', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
        $section->controls->add($control);

        $panel->sections->add($section);
    }

    public function _dashboardSections(Zippy_Customizer_Panel $panel)
    {
        $zippy = Zippy::instance();
        $section = $zippy->make(
            'customizer_section',
            array(
                'id'        => 'dashboard_options',
                'title'     => __('Dashboard Options', ZippyCourses::TEXTDOMAIN)
            )
        );

        $section->setDescription(
            __(
                'Control the display of the Courses in the Student Dashboard with the settings below.',
                ZippyCourses::TEXTDOMAIN
            )
        );

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'excerpt',
                'label'     => __('Show Excerpts?', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
        $section->controls->add($control);

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'featured_image',
                'label'     => __('Show Featured Image?', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
        $section->controls->add($control);

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'course_order',
                'label'     => __('Course Order', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            'alphanumeric_asc' => __('A to Z', ZippyCourses::TEXTDOMAIN),
            'alphanumeric_desc' => __('Z to A', ZippyCourses::TEXTDOMAIN),
            'publish_asc' => __('Old to New', ZippyCourses::TEXTDOMAIN),
            'publish_desc' => __('New to Old', ZippyCourses::TEXTDOMAIN),
        ));
        $section->controls->add($control);

        $panel->sections->add($section);
    }
    public function _directorySections(Zippy_Customizer_Panel $panel)
    {
       
        $zippy = Zippy::instance();

        $zippy = Zippy::instance();
        $section = $zippy->make(
            'customizer_section',
            array(
                'id'        => 'directory_options',
                'title'     => __('Directory Options', ZippyCourses::TEXTDOMAIN)
            )
        );
        $section->setDescription(
            __(
                'Control the display of the Course Directory with the settings below.',
                ZippyCourses::TEXTDOMAIN
            )
        );

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'excerpt',
                'label'     => __('Show Excerpts?', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
        $section->controls->add($control);

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'featured_image',
                'label'     => __('Show Featured Image?', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
        $section->controls->add($control);

        $panel->sections->add($section);
    }

    public function _styleSections(Zippy_Customizer_Panel $panel)
    {
        $show_styles = apply_filters('zippy_filter_include_customizer_styles', true);
        if (!$show_styles) {
            return;
        }
        
        $zippy = Zippy::instance();
        $section = $zippy->make(
            'customizer_section',
            array(
                'id'        => 'styles',
                'title'     => __('Styles', ZippyCourses::TEXTDOMAIN)
            )
        );
        $section->setDescription(
            __(
                'Zippy Courses allows you to enable and disable styling for certain elements, giving you more flexibility in integrating Zippy Courses with your favorite themes.  Use the controls below to turn the styles on or off.',
                ZippyCourses::TEXTDOMAIN
            )
        );

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'forms',
                'label'     => __('Forms', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select'
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
            
        $section->controls->add($control);

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'alerts',
                'label'     => __('Alerts', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select',
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));

        $section->controls->add($control);

        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'buttons',
                'label'     => __('Buttons', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select'
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
    
        $section->controls->add($control);

        // Course Directory Settings
        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'directory',
                'label'     => __('Directory', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select'
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
    
        $section->controls->add($control);

        // Dashboard Settings
        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'dashboard',
                'label'     => __('Dashboard', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select'
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
    
        $section->controls->add($control);

        // Tables Settings
        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'tables',
                'label'     => __('Tables', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select'
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
    
        $section->controls->add($control);

        // Course Settings
        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'course',
                'label'     => __('Course', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select'
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
    
        $section->controls->add($control);

        // Course Settings
        $control = $zippy->make(
            'customizer_control',
            array(
                'id'        => 'attribution',
                'label'     => __('Attribution', ZippyCourses::TEXTDOMAIN),
                'type'      => 'select'
            )
        );
        $control->setChoices(array(
            '1' => __('Yes', ZippyCourses::TEXTDOMAIN),
            '0' => __('No', ZippyCourses::TEXTDOMAIN),
        ));
    
        $section->controls->add($control);

        $panel->sections->add($section);
    }

    public function styles()
    {
        $show_styles = apply_filters('zippy_filter_include_customizer_styles', true);
        if (!$show_styles) {
            return;
        }

        $styles = get_option('zippy_customizer_styles', array());

        $map = array(
            'forms'     => ZippyCourses::$path . 'assets/css/customizer/forms.css',
            'alerts'    => ZippyCourses::$path . 'assets/css/customizer/alerts.css',
            'buttons'   => ZippyCourses::$path . 'assets/css/customizer/buttons.css',
            'course'    => ZippyCourses::$path . 'assets/css/customizer/course.css',
            'directory' => ZippyCourses::$path . 'assets/css/customizer/directory.css',
            'dashboard' => ZippyCourses::$path . 'assets/css/customizer/dashboard.css',
            'tables'    => ZippyCourses::$path . 'assets/css/customizer/tables.css',
            'quiz'      => ZippyCourses::$path . 'assets/css/customizer/quiz.css',
        );

        echo "<!-- Start Zippy Courses Custom Styles -->\n<style>\n";
        foreach ($map as $key => $file) {
            if ((isset($styles[$key]) && $styles[$key]) || !isset($styles[$key])) {
                $this->_showStylesheet($file);
            }
        }
        echo "\n</style>\n<!-- END Zippy Courses Custom Styles -->\n\n";
    }

    private function _showStylesheet($file)
    {
        if (is_readable($file)) {
            ob_start();
            require_once($file);
            $styles = ob_get_clean();
            $styles = preg_replace('/\s+/', ' ', $styles);
            
            echo $styles;
        }
    }
}
