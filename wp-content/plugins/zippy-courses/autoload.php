<?php
/**
 * There is not a suitable autoloading solution for plugin files in WordPress,
 * so we've created a classmap for use in our plugin that we'll pass to an spl_autoload_register
 * function in order to map our autoload
 */
// Setup autoload for this plugin
function zippy_courses_autoload($class)
{
    $path   = dirname(__FILE__) . DIRECTORY_SEPARATOR;
    $map    = include($path . 'classmap.php');
    if (array_key_exists($class, $map)) {
        $filepath = substr($map[$class], 0, 1) === '/' ? $map[$class] : $path . $map[$class];
        if (file_exists($filepath)) {
            require_once($filepath);
            return;
        }
    }
    $rules = array(
        array(
            'before'    => 'ZippyCourses',
            'after'     => 'Middleware',
            'path'      => $path . 'app' . DIRECTORY_SEPARATOR . 'Middleware' . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'Zippy',
            'after'     => 'MiddlewareRule',
            'path'      => $path . 'lib' . DIRECTORY_SEPARATOR . 'Middleware' . DIRECTORY_SEPARATOR . 'Rules' . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'ZippyCourses',
            'after'     => 'ListTableColumn',
            'path'      => $path . 'app' . DIRECTORY_SEPARATOR . 'UX' . DIRECTORY_SEPARATOR . 'ListTables' . DIRECTORY_SEPARATOR . 'Columns' . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'ZippyCourses',
            'after'     => 'ListTableFilter',
            'path'      => $path . 'app' . DIRECTORY_SEPARATOR . 'UX' . DIRECTORY_SEPARATOR . 'ListTables' . DIRECTORY_SEPARATOR . 'Filters' . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'ZippyCourses',
            'after'     => 'Widget',
            'path'      => $path . 'app' . DIRECTORY_SEPARATOR . 'Widgets' . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'ZippyCourses',
            'after'     => 'Filters',
            'path'      => $path . 'app' . DIRECTORY_SEPARATOR . 'Filters' . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'ZippyCourses',
            'after'     => 'Actions',
            'path'      => $path . 'app' . DIRECTORY_SEPARATOR . 'Actions' . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'Zippy',
            'after'     => 'Utilities',
            'path'      => $path . 'lib' . DIRECTORY_SEPARATOR . 'Helpers' . DIRECTORY_SEPARATOR . 'Utilities'  . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'ZippyCourses',
            'after'     => 'Event',
            'path'      => $path . 'app' . DIRECTORY_SEPARATOR . 'Events' . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'ZippyCourses',
            'after'     => 'ShortcodeView',
            'path'      => $path . 'app' . DIRECTORY_SEPARATOR . 'Shortcodes' . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'ZippyCourses',
            'after'     => 'Form',
            'path'      => $path . 'app' . DIRECTORY_SEPARATOR . 'Forms' . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'ZippyCourses',
            'after'     => 'FormProcessor',
            'path'      => $path . 'app' . DIRECTORY_SEPARATOR . 'Forms' . DIRECTORY_SEPARATOR . 'Processors' . DIRECTORY_SEPARATOR
        ),
        array(
            'before'    => 'Zippy',
            'after'     => 'FormField',
            'path'      => $path . 'lib' . DIRECTORY_SEPARATOR . 'Forms' . DIRECTORY_SEPARATOR . 'Fields' . DIRECTORY_SEPARATOR
        ),
    );
    foreach ($rules as $rule) {
        if (strpos($class, "{$rule['before']}_") !== false && strpos($class, "_{$rule['after']}") !== false) {
            $filename = str_replace(array("{$rule['before']}_", "_{$rule['after']}"), '', $class) . '.php';
            $filepath = $rule['path'] . $filename;
            if (file_exists($filepath)) {
                require_once($filepath);
                return;
            }
        }
    }
    return;
}
spl_autoload_register('zippy_courses_autoload');
// Scratchpad functions
function zdd($data = null)
{
    var_dump($data);
    die();
}
