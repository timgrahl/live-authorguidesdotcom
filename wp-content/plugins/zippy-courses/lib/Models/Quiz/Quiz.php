<?php

class Zippy_Quiz extends Zippy_Model
{
    public $id;

    public $questions = array();

    public function __construct($id)
    {
        $this->id = $id;

        if ($this->id) {
            $this->build();
        }
    }

    public function build()
    {
        $this->questions = $this->buildQuestions();
    }

    private function buildQuestions()
    {
        $zippy = Zippy::instance();

        $questions = (array) $zippy->utilities->getJsonMeta($this->id, 'questions', true);

        foreach ($questions as $key => $question) {
            if (!is_object($question) || !isset($question->ID)) {
                continue;
            }

            $question->title    = get_the_title($question->ID);
            $question->answers  = $zippy->utilities->getJsonMeta($question->ID, 'answers', true);
            if (is_array($question->answers)) {
                foreach ($question->answers as &$answer) {
                    $answer->question = null;
                    $answer->selection = null;
                }
            }
            $question->correct  = (int) get_post_meta($question->ID, 'correct', true);

            $questions[$key] = $question;
        }

        return $questions;
    }

    public function toJson()
    {
        return json_encode($this->questions);
    }


    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the value of id.
     *
     * @param mixed $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
