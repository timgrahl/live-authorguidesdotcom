<?php

class Zippy_QuizResults extends Zippy_Model
{
    public $id;

    public $questions = array();

    public function __construct($id)
    {
        $this->id = $id;

        if ($this->id) {
            $this->build();
        }
    }

    public function build()
    {
        $this->questions = $this->buildQuestions();
    }

    private function buildQuestions()
    {
        $zippy = Zippy::instance();

        $questions = $zippy->utilities->getJsonMeta($this->id, 'questions', true);

        foreach ($questions as $key => $question) {
            if (!is_object($question) || !isset($question->ID)) {
                continue;
            }

            $question->title    = get_the_title($question->ID);
            $question->answers  = $zippy->utilities->getJsonMeta($question->ID, 'answers', true);
            $question->correct  = (int) get_post_meta($question->ID, 'correct', true);

            $questions[$key] = $question;
        }

        return $questions;
    }

    public function toJson()
    {
        return json_encode($this->questions);
    }
}
