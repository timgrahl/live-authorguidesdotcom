<?php

abstract class Zippy_Widget_Model extends WP_Widget implements Zippy_RepositoryObject
{
    public $id;
    public $title;
    public $description;
    public $admin_view_path;
    public $public_view_path;

    public function __construct()
    {
        parent::__construct(
            $this->getId(),
            $this->getTitle(),
            array(
                'classname'  => 'zippy-widget ' . $this->getId() .'-class',
                'description' => $this->getDescription()
            )
        );

        // Refreshing the widget's cached output with each new post
        add_action('save_post', array($this, 'flushWidgetCache'));
        add_action('deleted_post', array($this, 'flushWidgetCache'));
        add_action('switch_theme', array($this, 'flushWidgetCache'));
    }

    abstract public function getDefaultValues();

    public function form($instance)
    {
        $instance = wp_parse_args(
            (array) $instance,
            $this->getDefaultValues()
        );

        extract($instance);

        // Display the admin form
        if (is_readable($this->getAdminViewPath())) {
            include($this->getAdminViewPath());
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function flushWidgetCache()
    {
        wp_cache_delete($this->getId(), 'widget');
    }

    /**
     * Processes the widget's options to be saved.
     *
     * @param array new_instance The new instance of values to be generated via the update.
     * @param array old_instance The previous instance of values before the update.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = $new_instance;
        return $instance;

    }

    /**
     * Outputs the content of the widget.
     *
     * @param array args  The array of form elements
     * @param array instance The current instance of the widget
     */
    public function widget($args, $instance)
    {
        // Check if there is a cached output
        $cache = wp_cache_get($this->getId(), 'widget');

        if (!is_array($cache)) {
            $cache = array();
        }

        if (!isset($args['widget_id'])) {
            $args['widget_id'] = $this->id;
        }

        if (isset($cache[$args['widget_id']])) {
            return print $cache[$args['widget_id']];
        }
            
        extract($args, EXTR_SKIP);
        extract($instance, EXTR_SKIP);

        ob_start();
        if (is_readable($this->getPublicViewPath())) {
            include($this->getPublicViewPath());
        }
        $widget_content = ob_get_clean();
        
        $widget_string  = $before_widget;
        $widget_string .= $widget_content;
        $widget_string .= $after_widget;

        $cache[$args['widget_id']] = $widget_string;

        wp_cache_set($this->getId(), $cache, 'widget');

        if ($this->isVisible()) {
            print $widget_string;            
        }
    }

    /**
     * Gets the value of title.
     *
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the value of title.
     *
     * @param mixed $title the title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Gets the value of description.
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the value of description.
     *
     * @param mixed $description the description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Gets the value of admin_view_path.
     *
     * @return mixed
     */
    public function getAdminViewPath()
    {
        return $this->admin_view_path;
    }

    /**
     * Sets the value of admin_view_path.
     *
     * @param mixed $admin_view_path the admin view path
     *
     * @return self
     */
    public function setAdminViewPath($admin_view_path)
    {
        $this->admin_view_path = $admin_view_path;

        return $this;
    }

    /**
     * Gets the value of public_view_path.
     *
     * @return mixed
     */
    public function getPublicViewPath()
    {
        return $this->public_view_path;
    }

    /**
     * Sets the value of public_view_path.
     *
     * @param mixed $public_view_path the public view path
     *
     * @return self
     */
    public function setPublicViewPath($public_view_path)
    {
        $this->public_view_path = $public_view_path;

        return $this;
    }

    public function isVisible()
    {
        return true;
    }
}
