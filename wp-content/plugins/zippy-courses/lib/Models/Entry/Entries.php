<?php
/**
 * A list of Zippy Course Entries, commonly used as members of courses.
 *
 * @since   1.0.0
 */
class Zippy_Entries extends Zippy_Repository
{
    public function __construct()
    {
        $this->addValidType('Zippy_Entry');
    }

    public function getRecursive($id = '')
    {
        if ($this->get($id) !== null) {
            return $this->get($id);
        }

        foreach ($this->all() as $item) {
            if ($item->entries->get($id) !== null) {
                return $item->entries->get($id);
            }
        }
        
        return;
    }

    public function getEntryUnitId($id = '')
    {
        $unit_id = null;

        foreach ($this->all() as $item) {
            if ($item->entries->get($id) !== null) {
                $unit_id = $item->getId();
            }
        }
        
        return $unit_id;
    }

    public function getNextEntry($id)
    {
        $zippy = Zippy::instance();

        $flat = $this->makeFlat();
        $entry_ids = array_keys($flat->all());

        $current = (int) $id;
        $next_id = 0;

        if (in_array($id, $entry_ids)) {
            reset($entry_ids);

            while (key($entry_ids) !== null && current($entry_ids) !== $id) {
                next($entry_ids);
            }

            $next_id = (int) next($entry_ids);
        }

        $next_entry = $next_id ? $flat->get($next_id) : null;

        return $next_entry;
    }

    public function getEntryUnit($id)
    {
        $entry = $this->get($id);
        foreach ($this->all() as $entry) {
            if ($entry->getType() != 'unit') {
                continue;
            }
            
            foreach ($entry->entries->all() as $subentry) {
                if ($subentry->getId() == $id) {
                    $unit = $entry;
                }

                if ($unit) {
                    break;
                }
            }

            if ($unit) {
                break;
            }
        }

        return $unit;
    }

    public function getPreviousUnit($id)
    {
        $zippy = Zippy::instance();
        if (get_post_type($id) == 'lesson') {
            $unit = $this->getEntryUnit($id);
        } else {
            $unit = $this->get($id);
        }

        $previous_unit = null;

        foreach ($this->all() as $current_unit) {
            if ($current_unit->getId() === $id) {
                return $previous_unit;
            }
            // If this isn't the current unit, set this as the "previous" unit and continue the loop
            $previous_unit = $current_unit;
        }

        return null;
    }

    public function getPreviousEntry($id)
    {
        $zippy = Zippy::instance();

        $flat = $this->makeFlat();
        $entry_ids = array_keys($flat->all());

        $current = (int) $id;
        $previous_id = 0;

        if (in_array($id, $entry_ids)) {
            end($entry_ids);

            while (key($entry_ids) !== null && current($entry_ids) !== $id) {
                prev($entry_ids);
            }

            $previous_id = (int) prev($entry_ids);
        }

        $previous_entry = $previous_id ? $flat->get($previous_id) : null;

        return $previous_entry;
    }

    public function getLastEntry()
    {
        $flat = $this->makeFlat();
        return $flat->last();
    }

    public function makeFlat()
    {
        $zippy = Zippy::instance();

        $flat = $zippy->make('entries');

        foreach ($this->all() as $entry) {
            $flat->add($entry);

            foreach ($entry->entries->all() as $subentry) {
                $flat->add($subentry);
            }
        }

        return $flat;
    }
}
