<?php

class Zippy_Entry extends Zippy_Model
{
    public $id;
    
    /**
     * The date this entry should be available
     *
     * @since   1.0.0
     *
     * @var     DateTime
     */
    public $date_available;

    /**
     * Entry Type
     *
     * @since   1.0.0
     *
     * @var string
     */
    public $type = 'item';
    
    /**
     * Post type of entry
     *
     * @since   1.0.0
     *
     * @var string
     */
    public $post_type;

    /**
     * The tiers for this Entry
     * @var array
     */
    public $tiers;

    /**
     * Scheduling
     * @var stdClass
     */
    public $scheduling;

    /**
     * Quiz
     * @var stdClass
     */
    public $quiz;
    
    /**
     * Units will have their own entries.
     * @var ZippyCourses_CourseEntries|null
     */
    public $entries;

    public $excerpt;

    public $middleware;

    public function __construct($details)
    {
        $zippy = Zippy::instance();

        $this->entries      = $zippy->make('entries');
        $this->scheduling   = new stdClass;
        $this->quiz         = new stdClass;

        $this->middleware = $zippy->middleware->get('student-access');
        if (!$this->middleware) {
            add_action('init', array($this, 'setupMiddleware'));
        }
    
        $this->parse($details);
    }
    public function setupMiddleware()
    {
        $zippy = Zippy::instance();
        $this->middleware   = $zippy->middleware->get('student-access');
    }

    public function parse($details = array())
    {
        $this->id                       = $details->ID;
        $this->tiers                    = $details->tiers;
        
        $this->type                     = $details->post_type == 'unit' ? 'unit' : 'item';
        $this->post_type                = $details->post_type;
        
        if (isset($details->scheduling)) {
            $this->scheduling->day_of_week  = $details->scheduling->day_of_week !== null ? (int) $details->scheduling->day_of_week : null;
            $this->scheduling->num_days     = $details->scheduling->num_days;
            $this->scheduling->required     = isset($details->scheduling->required) ? $details->scheduling->required : false;
            $this->scheduling->exempt       = isset($details->scheduling->exempt) ? $details->scheduling->exempt : false;
        }
        
        if (isset($details->quiz)) {
            $this->quiz->ID                 = $details->quiz->ID;
            $this->quiz->pass               = $details->quiz->pass;
            $this->quiz->required           = new stdClass;
                $this->quiz->required->completion   = $details->quiz->required->completion;
                $this->quiz->required->pass         = $details->quiz->required->pass;
        }

        if (isset($details->entries)) {
            $this->parseEntries($details->entries);
        }
    }

/**
 *--------------------------------------------------------------------------
 * BOOLEAN TESTS
 *--------------------------------------------------------------------------
 */
    /**
     * Is this entry a Unit?
     *
     * @since   1.0.0
     *
     * @return  boolean
     */
    public function isUnit()
    {
        return $this->type == 'unit';
    }

    /**
     * Is this entry an Item?
     *
     * @since   1.0.0
     *
     * @return  boolean
     */
    public function isItem()
    {
        return $this->type == 'item';
    }

/**
 *--------------------------------------------------------------------------
 * GETTERS & SETTERS
 *--------------------------------------------------------------------------
 */

    /**
     * Gets the value of type.
     *
     * @since 1.0.0
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the value of type.
     *
     * @param mixed $type the type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets the value of post_type.
     *
     * @since 1.0.0
     *
     * @return mixed
     */
    public function getPostType()
    {
        return $this->post_type;
    }

    /**
     * Sets the value of post_type.
     *
     * @since 1.0.0
     *
     * @param mixed $post_type the post type
     *
     * @return self
     */
    public function setPostType($post_type)
    {
        $this->post_type = $post_type;

        return $this;
    }

    /**
     * Gets the Units will have their own entries.
     *
     * @since 1.0.0
     *
     * @return ZippyCourses_CourseEntries|null
     */
    public function getEntries()
    {
        return $this->entries;
    }

    /**
     * Sets the Units will have their own entries.
     *
     * @since 1.0.0
     *
     * @param ZippyCourses_CourseEntries|null $entries the entries
     *
     * @return self
     */
    public function setEntries($entries)
    {
        $this->entries = $entries;

        return $this;
    }

    /**
     * Gets the The date this entry should be available.
     *
     * @since 1.0.0
     *
     * @return DateTime
     */
    public function getDateAvailable()
    {
        return $this->date_available;
    }

    /**
     * Sets the The date this entry should be available.
     *
     * @since 1.0.0
     *
     * @param DateTime $date
     *
     * @return self
     */
    public function setDateAvailable(DateTime $date)
    {
        $this->date_available = clone($date);

        return $this;
    }

    public function parseEntries(array $entries)
    {
        foreach ($entries as $details) {
            $this->entries->add(new Zippy_Entry($details));
        }
    }

    /**
     * Gets the The tiers for this Entry.
     *
     * @return array
     */
    public function getTiers()
    {
        return $this->tiers;
    }

    /**
     * Sets the The tiers for this Entry.
     *
     * @param array $tiers the tiers
     *
     * @return self
     */
    public function setTiers(array $tiers)
    {
        $this->tiers = $tiers;

        return $this;
    }

    /**
     * Gets the Scheduling.
     *
     * @return stdClass
     */
    public function getScheduling()
    {
        return $this->scheduling;
    }

    /**
     * Sets the Scheduling.
     *
     * @param stdClass $scheduling the scheduling
     *
     * @return self
     */
    public function setScheduling(stdClass $scheduling)
    {
        $this->scheduling = $scheduling;

        return $this;
    }

    /**
     * Gets the Quiz.
     *
     * @return stdClass
     */
    public function getQuiz()
    {
        return $this->quiz;
    }

    /**
     * Get the passing grade of a quiz
     *
     * @return stdClass
     */
    public function getQuizPassingGrade()
    {
        return $this->quiz->pass;
    }

    /**
     * Does this entry a quiz to be completed?
     *
     * @return bool
     */
    public function getQuizCompletionRequired()
    {
        return $this->quiz->required->completion;
    }

    /**
     * Does this entry require a passing quiz grade?
     *
     * @return bool
     */
    public function getQuizPassRequired()
    {
        return $this->quiz->required->pass;
    }

    /**
     * Sets the Quiz.
     *
     * @param stdClass $quiz the quiz
     *
     * @return self
     */
    public function setQuiz(stdClass $quiz)
    {
        $this->quiz = $quiz;

        return $this;
    }

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the value of id.
     *
     * @param mixed $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getExcerpt()
    {
        global $wpdb;

        $postdata = get_post($this->getId());

        if (!empty($postdata->post_excerpt)) {
            $this->excerpt = $postdata->post_excerpt;
        } else {
            $this->excerpt = strip_shortcodes(wp_trim_words($postdata->post_content, 40));
        }

        return $this->excerpt;
    }

    public function isAvailable()
    {
        $zippy = Zippy::instance();
        $result = $this->middleware->run($this);
        $this->setAvailabilityMessage();
        return $result;
    }

    public function getAvailabilityMessage()
    {
        if (empty($this->error_messages)) {
            $this->error_messages = implode(' ', $this->middleware->getErrors());
        }
        
        return $this->error_messages;

        // return implode(' ', $this->middleware->getErrors());
    }
    public function setAvailabilityMessage()
    {
        if (empty($this->error_messages)) {
            $this->error_messages = implode(' ', $this->middleware->getErrors());
        }
        
        return $this;

        // return implode(' ', $this->middleware->getErrors());
    }

    public function hasQuiz()
    {
        return (bool) $this->quiz->ID;
    }

    public function isRequired()
    {
        return isset($this->scheduling->required) && $this->scheduling->required;
    }

    public function isExempt()
    {
        return isset($this->scheduling->exempt) && $this->scheduling->exempt;
    }
}
