<?php

class Zippy_CourseCertificate
{
    public $data;

    public $enabled;
    public $title;
    public $institution;
    public $instructor;
    public $requirement;
    public $course;


    public $middleware;
    public $student;

    public function __construct($course_id)
    {

        $zippy = Zippy::instance();
        $this->fetchData($course_id);


        $this->student = $zippy->cache->get('student');
        $this->course = $zippy->make('course', array($course_id));
        $this->middleware = $zippy->middleware->get('student-course-completion');
    }

    public function fetchData($course_id)
    {
        $data = get_post_meta($course_id, 'certificate', true);
        $this->data = json_decode($data);
        if (isset($_GET['preview_certificate'])) {
            $this->data->title = isset($_GET['title']) ? $_GET['title'] : $data->title;
            $this->data->institution = isset($_GET['institution']) ? $_GET['institution'] : $data->institution;
            $this->data->instructor = isset($_GET['instructor']) ? $_GET['instructor'] : $data->instructor;
        }
        $this->enabled = $this->data->enabled;
        $this->title = !empty($this->data->title) ? $this->data->title : __('Certificate of Completion', ZippyCourses::TEXTDOMAIN);
        $this->institution = !empty($this->data->institution) ? $this->data->institution : get_bloginfo();
        $this->instructor = !empty($this->data->instructor) ? $this->data->instructor : '';
        $this->requirement = $this->data->requirement;
    }

    public function getRequirement()
    {
        return $this->requirement;
    }

    public function getTitle()
    {
        return $this->title;
    }
    public function getInstitution()
    {
        return $this->institution;
    }

    public function getInstructor()
    {
        return $this->instructor;
    }

    public function getCompletionDate()
    {
        $zippy = Zippy::instance();
        $student_id = $this->student->getId();
        $course_id = $this->course->getId();
        $completion_date = get_user_meta($student_id, "course_{$course_id}_completion_date", true);
        return date_i18n('F d, Y', strtotime($completion_date));
    }

    public function render()
    {
        $view = new ZippyCourses_Certificate_View($this);
        return $view->render();
    }

    public function isAvailable()
    {
        return $this->middleware->run($this->course);
    }
    public function renderErrorMessages()
    {
        return $this->middleware->renderErrorMessages();
    }
}
