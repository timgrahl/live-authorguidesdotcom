<?php
/**
 * A set of configuration parameters for our course.
 *
 * @since 1.0.0
 */
class Zippy_CourseConfig
{
    public $scheduling;
    public $access;

    public function __construct(
        Zippy_CourseSchedulingConfig $scheduling,
        Zippy_CourseAccessConfig $access
    ) {
        $this->scheduling   = $scheduling;
        $this->access       = $access;
    }

    /**
     * Check to see if a Unit Entry has the Scheduling Bypass enabled,
     * And is elligible to be bypassed based on current settings
     * @param  Zippy_Entry $entry the entry that maybe should be bypassed
     * @return boolean            If the unit is elligible to be bypassed
     */
    public function hasUnitBypass(Zippy_Entry $entry)
    {
        return $entry->scheduling->exempt && $this->scheduling->getControlItem() == 'unit';
    }
}
