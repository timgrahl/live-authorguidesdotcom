<?php
/**
 * A Course's access parameters
 *
 * @since 1.0.0
 */
class Zippy_CourseAccessConfig
{
    /**
     * Access Control Details
     *
     * @since 1.0.0
     *
     * @var stdClass
     */
    public $control;

    /**
     * Inject defaults
     */
    public function __construct()
    {
        $control = new stdClass;
            $control->use  = 'item';

        $this->control = $control;
    }

    public function getControlItem()
    {
        return $this->control->use;
    }

    public function merge($data)
    {
        if (is_array($data)) {
            $data = json_decode(json_encode($data));
        }

        return $this->mergeObject($data);
    }

    private function mergeObject($data)
    {
        $this->control->use = isset($data->use) ? $data->use : $this->control->use;
    }
}
