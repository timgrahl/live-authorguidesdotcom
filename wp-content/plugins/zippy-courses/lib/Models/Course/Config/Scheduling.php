<?php
/**
 * A Course's scheduling parameters
 *
 * @since 1.0.0
 */
class Zippy_CourseSchedulingConfig
{
    /**
     * Scheduling Type
     *
     * @since 1.0.0
     *
     * @var string
     */
    public $type = 'drip';

    /**
     * Schedule Control Details
     *
     * @since 1.0.0
     *
     * @var stdClass
     */
    public $control;

    /**
     * Scheduling Start Details
     *
     * @since 1.0.0
     *
     * @var stdClass
     */
    public $start;

    /**
     * Inject defaults
     */
    public function __construct()
    {
        $this->type = 'drip';
        
        $start = new stdClass;
            $start->type = 'join';
            $start->date = null;

        $this->start = $start;

        $end = new stdClass;
            $end->type = 'none';
            $end->date = null;
            $end->duration = 0;

        $this->end = $end;

        $control = new stdClass;
            $control->type = 'previous';
            $control->use  = 'item';

        $this->control = $control;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getStartType()
    {
        return $this->start->type;
    }

    public function getStartDate()
    {
        return $this->start->date;
    }

    public function getEndType()
    {
        return $this->end->type;
    }

    public function getEndDate()
    {
        return $this->end->date;
    }

    public function getExpirationDate()
    {
        $zippy = Zippy::instance();
        return $zippy->utilities->datetime->getDate($this->end->date);
    }

    public function getEndDuration()
    {
        return $this->end->duration;
    }

    public function getExpirationDuration()
    {
        return $this->end->duration;
    }

    public function getControl()
    {
        return $this->control;
    }

    public function getControlType()
    {
        return $this->control->type;
    }

    public function getControlItem()
    {
        return $this->control->use;
    }

    public function merge($data)
    {
        if (is_array($data)) {
            $data = json_decode(json_encode($data));
        }

        return $this->mergeObject($data);
    }

    private function mergeObject($data)
    {
        $this->type             = isset($data->type) ? $data->type : $this->type;
        $this->control->use     = isset($data->control->use) ? $data->control->use : $this->control->use;
        $this->control->type    = isset($data->control->type) ? $data->control->type : $this->control->type;
        $this->start->type      = isset($data->start->type) ? $data->start->type : $this->start->type;
        $this->start->date      = isset($data->start->date) ? $data->start->date : $this->start->date;
        $this->end->type        = isset($data->end->type) ? $data->end->type : $this->end->type;
        $this->end->date        = isset($data->end->date) ? $data->end->date : $this->end->date;
        $this->end->duration    = isset($data->end->duration) ? $data->end->duration : $this->end->duration;
    }
}
