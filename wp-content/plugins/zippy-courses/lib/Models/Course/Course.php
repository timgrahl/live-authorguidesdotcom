<?php

class Zippy_Course extends Zippy_Model
{
    public $id;

    /**
     * Course Configuration details
     * @var Zippy_CourseConfig
     */
    public $config;

    /**
     * Entries
     * @var Zippy_Entries
     */
    public $entries;

    /**
     * Additioanl Content
     * @var Zippy_Entries
     */
    public $additional_content;

    /**
     * List of products attached to this Course
     * @var ZippyCourses_Products
     */
    public $products;

    public $tiers = array();

    private $fetched = false;
    /**
     * WP_Post Data
     * @var WP_Post
     */
    public $postdata;

    /**
     * Object Metadata
     * @var array
     */
    public $metadata;

    public $excerpt;
    
    public $public_excerpt;

    public $title;

    public $published;

    protected $cache;

    protected $start_date;

    public function __construct($id = 0)
    {
        $zippy = Zippy::instance();

        $this->id                   = $id;
        $this->entries              = $zippy->make('entries');
        $this->additional_content   = $zippy->make('entries');
        $this->products             = $zippy->make('products');
        $this->config               = $zippy->make('course_config');

        if ($id) {
            if (!$this->loadFromCache()) {
                $this->fetch();
            }
        }
    }

    private function loadFromCache()
    {
        $zippy = Zippy::instance();
        $cache = $zippy->cache->get("course_{$this->id}");
        if (!$cache) {
            return false;
        }

        $this->entries              = $cache->entries;
        $this->additional_content   = $cache->additional_content;
        $this->products             = $cache->products;
        $this->tiers                = $cache->tiers;
        $this->metadata             = $cache->metadata;
        $this->config               = $cache->config;
        $this->excerpt              = $cache->excerpt;
        $this->public_excerpt       = $cache->public_excerpt;

        return true;
    }

    public function saveCache()
    {
        $zippy = Zippy::instance();
        $cache = new StdClass;

        $cache->entries              = $this->entries;
        $cache->additional_content   = $this->additional_content;
        $cache->products             = $this->products;
        $cache->config               = $this->config;
        $cache->tiers                = $this->tiers;
        $cache->metadata             = $this->metadata;
        $cache->excerpt              = $this->excerpt;
        $cache->public_excerpt       = $this->public_excerpt;

        $cache = $zippy->cache->set("course_{$this->id}", $cache);
    }

    public function fetch()
    {
        $zippy = Zippy::instance();
        
        $this->metadata = $this->fetchMeta();

        if (isset($this->metadata['entries'])) {
            $entries = $this->metadata['entries'];
            $json = is_string($entries) ? json_decode($entries) : null;
            $entries = $json === null ? $entries : $json;
            $entries = is_array($entries) ? $entries : array();

            $this->buildEntries($entries);
        }

        if (isset($this->metadata['additional_content'])) {
            $additional_content = $this->metadata['additional_content'];
            $json = is_string($additional_content) ? json_decode($additional_content) : null;
            $additional_content = $json === null ? $additional_content : $json;
            $additional_content = is_array($additional_content) ? $additional_content : array();
            
            $this->buildAdditionalContent($additional_content);
        }

        if (isset($this->metadata['tiers'])) {
            $this->tiers = $zippy->utilities->maybeJsonDecode($this->metadata['tiers']);
        }

        if (isset($this->metadata['config'])) {
            $json = is_string($this->metadata['config']) ? json_decode($this->metadata['config']) : null;
            $config = $json === null ? $this->metadata['config'] : $json;

            if (isset($config->access)) {
                $this->config->access->merge($config->access);
            }

            if (isset($config->scheduling)) {
                $this->config->scheduling->merge($config->scheduling);
            }
        }
        $this->saveCache();
        $this->fetched = true;
    }

    public function fetchPostData()
    {
        $zippy = Zippy::instance();
        $this->postdata = get_post($this->id);
        $this->title = isset($this->postdata->post_title) ? $this->postdata->post_title : get_the_title($this->id);
        $this->published = isset($this->postdata->post_date)
            ? $zippy->utilities->datetime->getDate($this->postdata->post_date)
            : $zippy->utilities->datetime->getNow();
    }

    public function fetchDeep()
    {
        $this->fetchProducts();
    }

    private function fetchMeta()
    {
        $metadata = get_post_meta($this->id);
        $metadata = is_array($metadata) ? $metadata : array();

        foreach ($metadata as &$meta) {
            if (count($meta) == 1) {
                $meta = maybe_unserialize($meta[0]);
            } else {
                foreach ($meta as &$m) {
                    $m = maybe_unserialize($m);
                }
            }
        }

        return $metadata;
    }

    private function fetchProducts()
    {
        $zippy = Zippy::instance();

        $product_ids = isset($this->metadata['products']) ? $zippy->utilities->maybeJsonDecode($this->metadata['products']) : array();

        foreach ($product_ids as $product_id) {
            $product = $zippy->make('product', array('id' => $product_id));
            $this->products->add($product);
        }

        return $this->products;
    }

    public function getPostData()
    {
        if (empty($this->postdata)) {
            $this->fetchPostData();
        }
        return $this->postdata;
    }

    /**
     * Gets the List of products attached to this Course.
     *
     * @return ZippyCourses_Products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Sets the List of products attached to this Course.
     *
     * @param ZippyCourses_Products $products the products
     *
     * @return self
     */
    public function setProducts(ZippyCourses_Products $products)
    {
        $this->products = $products;

        return $this;
    }

    public function getExcerpt()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT post_excerpt, post_content FROM $wpdb->posts WHERE ID = %s", $this->getId());
        $data = $wpdb->get_row($sql);

        if (!empty($data->post_excerpt)) {
            $this->excerpt = $data->post_excerpt;
        } else {
            $this->excerpt = strip_shortcodes(wp_trim_words($data->post_content, 40));
        }

        return $this->excerpt;
    }

    public function getPublicExcerpt()
    {
        global $wpdb;
        
        $public_excerpt = get_post_meta($this->getId(), 'public_excerpt', true);
        $public_content = get_post_meta($this->getId(), 'public_content', true);

        if (!empty($public_excerpt)) {
            $this->public_excerpt = $public_excerpt;
        } else {
            $this->public_excerpt = strip_shortcodes(wp_trim_words($public_content, 40));
        }

        return $this->public_excerpt;
    }

    public function getFeaturedImage($size = 'large')
    {
        $image = isset($this->metadata['featured_media']->image) ? $this->metadata['featured_media']->image : false;

        if ($image) {
            $image = wp_get_attachment_image_src($image->ID, $size);
        }

        return $image;
    }

    public function getPublicFeaturedImage($size = 'large')
    {
        $zippy = Zippy::instance();

        $public_featured_media  = $zippy->utilities->getJsonMeta($this->getId(), 'public_featured_media', true);
        $featured_media         = $zippy->utilities->getJsonMeta($this->getId(), 'featured_media', true);

        $image = isset($public_featured_media->image->ID) ? $public_featured_media->image : false;

        $image = is_object($image) && $image->ID
            ? $image
            : (isset($featured_media->image->ID)
                ? $featured_media->image
                : false
            );
       
        if ($image) {
            $image = wp_get_attachment_image_src($image->ID, $size);
        }

        return $image;
    }

    public function getFeaturedMedia()
    {

    }

    public function getPublicFeaturedMedia()
    {

    }

    public function getTiers()
    {
        if (!$this->fetched) {
            $this->fetch();
        }

        return $this->tiers;
    }

    public function getTitle()
    {
        if (empty($this->postdata)) {
            $this->fetchPostData();
        }

        if (is_object($this->postdata) && !is_wp_error($this->postdata)) {
            return $this->postdata->post_title;
        }

        return null;
    }
    /**
     * Gets the value of id.
     *
     * @since 1.0.0
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the Course Configuration details.
     *
     * @since 1.0.0
     *
     * @return Zippy_CourseConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Sets the Course Configuration details.
     *
     * @since 1.0.0
     *
     * @param Zippy_CourseConfig $config the config
     *
     * @return self
     */
    public function setConfig(Zippy_CourseConfig $config)
    {
        $this->config = $config;

        return $this;
    }

    public function buildEntries(array $entries)
    {
        foreach ($entries as $details) {
            $this->entries->add(new Zippy_Entry($details));
        }
    }

    public function buildAdditionalContent(array $entries)
    {
        foreach ($entries as $details) {
            $this->additional_content->add(new Zippy_Entry($details));
        }
    }

    public function prepareForStudent(DateTime $start_date, array $tiers = array())
    {
            $this->entries = $this->_filterEntriesByTier($this->entries);
            $this->entries = $this->_filterDrafts($this->entries);
            $this->_calculateSchedule($start_date);
    }

    private function _calculateSchedule(DateTime $start_date)
    {
        if ($this->config->scheduling->getType() == 'drip') {
            if ($this->config->scheduling->getControlType() == 'previous') {
                $this->_calculateFromLastEntry($start_date);
            } else {
                $this->_calculateFromCourseStart($start_date);
            }
        } else {
            foreach ($this->entries->all() as $entry) {
                $entry->setDateAvailable($start_date);

                foreach ($entry->entries->all() as $subentry) {
                    $subentry->setDateAvailable($start_date);
                }
            }
        }
    }

    public function getStartDate()
    {
        if (empty($this->start_date)) {
            $zippy = Zippy::instance();
            $student = $zippy->cache->get('student');
            $data    = $student->getCourseData();
            $course_data = $data[$this->getId()];
            $this->start_date = $course_data['start_date'] instanceof DateTime ? $course_data['start_date'] : $zippy->utilities->datetime->getNow();
        }
        return $this->start_date;
    }

    private function _calculateFromLastEntry(DateTime $start_date)
    {
        $date_available = clone($start_date);

        foreach ($this->entries->all() as $entry) {
            if ($this->config->scheduling->getControlItem() == 'item') {
                if ($entry->getType() == 'item') {
                    $this->_calculateItemDateAvailable($entry, $date_available, $start_date);
                } else {
                    $this->_calculateUnitDateAvailable($entry, $date_available, $start_date);
                }
            } else {
                $this->_calculateItemDateAvailable($entry, $date_available, $start_date);

                foreach ($entry->entries->all() as $subentry) {
                    $subentry->setDateAvailable($entry->getDateAvailable());
                }
            }
        }
    }

    private function _calculateFromCourseStart(DateTime $start_date)
    {
        foreach ($this->entries->all() as $entry) {
            $date_available = clone($start_date);

            if ($this->config->scheduling->getControlItem() == 'item') {
                if ($entry->getType() == 'item') {
                    $this->_calculateItemDateAvailable($entry, $date_available, $start_date);
                } else {
                    if (!$entry->scheduling->exempt) {
                        foreach ($entry->entries->all() as $subentry) {
                            $date_available = clone($start_date);
                            $this->_calculateItemDateAvailable($subentry, $date_available, $start_date);

                            if ($entry->getDateAvailable() === null || $date_available->format('U') <= $entry->getDateAvailable()->format('U')) {
                                $entry->setDateAvailable($date_available);
                            }
                        }

                        $entries = $entry->entries->all();
                        if (empty($entries)) {
                            $this->_calculateItemDateAvailable($entry, $date_available, $start_date);
                        }
                    } else {
                        $this->_calculateUnitDateAvailable($entry, $date_available, $start_date);
                    }
                }
            } else {
                if (!$entry->scheduling->exempt) {
                    $this->_calculateItemDateAvailable($entry, $date_available, $start_date);

                    foreach ($entry->entries->all() as $subentry) {
                        $subentry->setDateAvailable($date_available);
                    }
                } else {
                    $this->_calculateUnitDateAvailable($entry, $date_available, $start_date);
                }
            }
        }
    }

    private function _calculateItemDateAvailable(Zippy_Entry $entry, DateTime $date, $start_date = null)
    {
        $zippy = Zippy::instance();

        if (!$entry->scheduling->exempt) {
            if (is_numeric($entry->scheduling->num_days)) {
                $date->modify('+' . $entry->scheduling->num_days . ' days');
            } else {
                $this->_addDayOfWeek($date, $entry->scheduling->day_of_week);
            }
            
            $entry->setDateAvailable($date);
        } else {
            $date_exempt = $zippy->utilities->datetime->getToday();
            $date_exempt->modify('-1 day');

            $entry->setDateAvailable($date_exempt);

            foreach ($entry->entries->all() as $subentry) {
                $subentry->setDateAvailable($date_exempt);
            }
        }
    }

    private function _calculateUnitDateAvailable(Zippy_Entry $entry, DateTime $date, $start_date = null, $clone = true)
    {
        $zippy = Zippy::instance();
        $entries = $entry->entries->all();

        if (empty($entries)) {
            return $this->_calculateItemDateAvailable($entry, $date, $start_date);
        }

        if (!$this->config->hasUnitBypass($entry)) {
            // Clone the date so that we do not modify the date available, and so that we can use it when we calculate the
            // next lesson's information.
            $udate          = $clone ? clone($date) : $date;
            $first_entry    = $entry->entries->first();
            $has_exempt     = false;
            foreach ($entry->entries->all() as $subentry) {
                if ($subentry->scheduling->exempt) {
                    $has_exempt = true;
                }
                if ($has_exempt) {
                    break;
                }
            }
            if (!$has_exempt) {
                if ($first_entry->scheduling->num_days !== null) {
                    $udate->modify('+' . $first_entry->scheduling->num_days . ' days');
                } else {
                    $udate = $this->_addDayOfWeek($udate, $first_entry->scheduling->day_of_week);
                }
            } else {
                if ($start_date instanceof DateTime) {
                    $udate = $start_date;
                } else {
                    $udate = $date;
                }
            }
            
            $entry->setDateAvailable($udate);
            foreach ($entry->entries->all() as $subentry) {
                $this->_calculateItemDateAvailable($subentry, $date, $start_date);
            }
        } else {
            $date_exempt = $zippy->utilities->datetime->getToday();
            $date_exempt->modify('-1 day');
            $entry->setDateAvailable($date_exempt);
            foreach ($entry->entries->all() as $subentry) {
                $subentry->setDateAvailable($date_exempt);
            }
        }
        
        return $entry;
    }

    private function _addDayOfWeek(DateTime $date, $day_of_week)
    {
        // day_of_week is stored as a value between 1-7, with 7 being sunday
        // so we need to correct for this to be in line with how DateTime calculates days of the week
        if ($day_of_week == 7) {
            $day_of_week = 0;
        }
        
        $days_of_week   = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        
        if ($date->format('w') == $day_of_week) {
            $date->modify($days_of_week[$day_of_week]);
            $date->modify('+1 week');
        } else {
            $date->modify($days_of_week[$day_of_week]);
        }

        return $date;
    }

    private function _filterEntriesByTier(Zippy_Entries $entries)
    {
        $zippy = Zippy::instance();

        $middleware = $zippy->middleware->get('student-tier-access');

        if ($middleware) {
            foreach ($entries->all() as $key => $entry) {
                $has_tier = $middleware->run($entry);

                if (!$has_tier) {
                    $entries->remove($key);
                    continue;
                }

                foreach ($entry->entries->all() as $k => $subentry) {
                    $has_tier = $middleware->run($subentry);

                    if (!$has_tier) {
                        $entry->entries->remove($k);
                    }
                }
            }
        }

        return $entries;
    }

    private function _filterDrafts(Zippy_Entries $entries)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        if (!current_user_can('edit_others_posts')) {
            $ids = array();

            foreach ($entries->all() as $key => $entry) {
                $ids[] = $entry->getId();

                foreach ($entry->entries->all() as $k => $subentry) {
                    $ids[] = $subentry->getId();
                }
            }
            
            if (empty($ids)) {
                return $entries;
            }

            $ids_list = implode(',', $ids);
            $published = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE post_status = 'publish' AND ID IN ($ids_list)");

            foreach ($entries->all() as $key => $entry) {
                if (!in_array($entry->getId(), $published)) {
                    $entries->remove($key);
                    continue;
                }

                foreach ($entry->entries->all() as $k => $subentry) {
                    if (!in_array($subentry->getId(), $published)) {
                        $entry->entries->remove($k);
                    }
                }
            }
        }

        return $entries;
    }

    /**
     * Gets the Entries.
     *
     * @return Zippy_Entries
     */
    public function getEntries()
    {
        return $this->entries;
    }

    public function getEntryIds()
    {
        $output = array();

        foreach ($this->entries->all() as $entry) {
            $output[] = $entry->getId();

            foreach ($entry->entries->all() as $subentry) {
                $output[] = $subentry->getId();
            }
        }

        return $output;
    }

    /**
     * Gets an entry, first from Entries, then from Additional Content
     *
     * @return Zippy_Entry
     */
    public function getEntry($id)
    {
        if (($entry = $this->entries->getRecursive($id)) !== null) {
            return $entry;
        }

        if (($entry = $this->additional_content->get($id)) !== null) {
            return $entry;
        }

        return null;
    }

    public function hasCertificate()
    {
        $zippy = Zippy::instance();
        $certificate_data = $zippy->utilities->getJsonMeta($this->getId(), 'certificate', true);
        if (($certificate_data) && $certificate_data->enabled == true) {
            return true;
        }
        return false;
    }

    /**
     * Sets the Entries.
     *
     * @param Zippy_Entries $entries the entries
     *
     * @return self
     */
    public function setEntries(Zippy_Entries $entries)
    {
        $this->entries = $entries;

        return $this;
    }

    /**
     * Gets the Additioanl Content.
     *
     * @return Zippy_Entries
     */
    public function getAdditionalContent()
    {
        return $this->additional_content;
    }

    /**
     * Sets the Additioanl Content.
     *
     * @param Zippy_Entries $additional_content the additional content
     *
     * @return self
     */
    public function setAdditionalContent(Zippy_Entries $additional_content)
    {
        $this->additional_content = $additional_content;

        return $this;
    }
}
