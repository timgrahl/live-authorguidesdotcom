<?php

class Zippy_Forum extends Zippy_Model
{
    public $protected;
    public $courses;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCoursesAndTiers()
    {
        $zippy = Zippy::instance();

        if ($this->courses === null) {
            $this->courses = array_filter(
                (array) $zippy->utilities->getJsonMeta($this->id, 'zippy_forum_access', true)
            );
        }

        return $this->courses;
    }

    public function isProtected()
    {
        if ($this->protected === null) {
            $this->protected = (bool) get_post_meta($this->id, 'zippy_forum_access_enabled', true);
        }

        return $this->protected;
    }

    public function hasCourseTier($course_id, $tier_id)
    {
        if (!$this->isProtected()) {
            return true;
        }

        $access = false;
        
        foreach ($this->getCoursesAndTiers() as $course) {
            if ($course->id == $course_id) {
                foreach ($course->tiers as $tier) {
                    
                    if ($tier == $tier_id) {
                        $access = true;                        
                    }

                    if ($access) {
                        break;
                    }
                }
            }

            if ($access) {
                break;
            }
        }

        return $access;
    }
}
