<?php

/**
 * This handles data about each column for a particular database table that gets built using Zippy Courses
 *
 * @since   1.0.0
 *
 * @todo    Extract the generateing of a field into it's own class
 */
class Zippy_DatabaseTableColumn implements Zippy_RepositoryObject
{
    public $id;
    public $type;
    public $type_parameters;
    public $nullable = false;
    public $auto_increment = false;
    public $primary_key = false;
    public $unique_key = false;
    public $key = false;
    public $default;

    public function __construct($id, $type)
    {
        $this->id   = $id;
        $this->type = $type;
    }

/**
 *--------------------------------------------------------------------------
 * RENDERING METHODS
 *--------------------------------------------------------------------------
 */
    /**
     * Handles the generation of the SQL for this column
     *
     * @since 1.0.0
     *
     * @return self
     */
    public function generateSql()
    {
        $output = '  ';

        $output .= $this->generateId();
        $output .= $this->generateType();
        $output .= $this->generateNullable();
        $output .= $this->generateDefault();
        $output .= $this->generateAutoIncrement();

        $output = trim($output) . ",\n";

        return $output;
    }

    /**
     * Generates the column name
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function generateId()
    {
        return "{$this->id} ";
    }

    /**
     * Generates column type, and appends parameters when necessary
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function generateType()
    {
        return "{$this->type}" . $this->generateTypeParameters();
    }

    /**
     * Appends parameters such as string or int length
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function generateTypeParameters()
    {
        $integer_types = array('int', 'integer', 'smallint', 'tinyint', 'bigint', 'mediumint');

        $output = $this->type_parameters !== null ? "({$this->type_parameters}) " : ' ';

        if (array_search(strtolower($this->type), $integer_types)) {
            $output .= 'UNSIGNED ';
        }

        return $output;
    }

    /**
     * Outputs Primary Key in a dbDelta compliant format
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function generatePrimaryKey()
    {
        return "PRIMARY KEY  ({$this->id}),\n";
    }

    /**
     * Outputs Unique Key in a dbDelta compliant format
     *
     * @since 1.0.0
     *
     * @return self
     */
    public function generateUniqueKey()
    {
        return "UNIQUE KEY {$this->id} ({$this->id}),\n";
    }

    /**
     * Outputs Key (Index) in a dbDelta compliant format
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function generateKey()
    {
        return "KEY {$this->id} ({$this->id}),\n";
    }

    /**
     * Outputs Null / Not Null
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function generateNullable()
    {
        return $this->nullable ? "NULL " : "NOT NULL ";
    }

    /**
     * Generates correctly formatted Default for column
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function generateDefault()
    {
        $integer_types = array('int', 'integer', 'smallint', 'tinyint', 'bigint', 'mediumint');

        $output = $this->default !== null ? "DEFAULT '{$this->default}' " : "";

        if ($this->type == 'timestamp' && $this->default == 'CURRENT_TIMESTAMP') {
            $output = "DEFAULT CURRENT_TIMESTAMP ";
        }

        if (array_search(strtolower($this->type), $integer_types) !== false) {
            $output = $this->default !== null ? "DEFAULT {$this->default} " : "";
        }

        return $output;
    }

    /**
     * Outputs Auto Increment in a dbDelta compliant format
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function generateAutoIncrement()
    {
        return $this->auto_increment ? "AUTO_INCREMENT " : "";
    }

/**
 *--------------------------------------------------------------------------
 * GETTERS & SETTERS
 *--------------------------------------------------------------------------
 */

    /**
     * Gets the value of id.
     *
     * @since 1.0.0
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the value of id.
     *
     * @since 1.0.0
     *
     * @param mixed $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the value of type.
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the value of type.
     *
     * @since 1.0.0
     *
     * @param mixed $type the type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets the value of nullable.
     *
     * @return mixed
     */
    public function getNullable()
    {
        return $this->nullable;
    }

    /**
     * Sets the value of nullable.
     *
     * @since 1.0.0
     *
     * @param mixed $nullable the nullable
     *
     * @return self
     */
    public function setNullable($nullable)
    {
        $this->nullable = $nullable;

        return $this;
    }

    /**
     * Gets the value of primary_key.
     *
     * @since 1.0.0
     *
     * @return mixed
     */
    public function getPrimaryKey()
    {
        return $this->primary_key;
    }

    /**
     * Sets the value of primary_key.
     *
     * @since 1.0.0
     *
     * @param mixed $primary_key the primary key
     *
     * @return self
     */
    public function setPrimaryKey($primary_key)
    {
        $this->primary_key = $primary_key;
        $this->unique_key = false;
        $this->key = false;

        return $this;
    }

    /**
     * Gets the value of unique_key.
     *
     * @since 1.0.0
     *
     * @return mixed
     */
    public function getUniqueKey()
    {
        return $this->unique_key;
    }

    /**
     * Sets the value of unique_key.
     *
     * @since 1.0.0
     *
     * @param mixed $unique_key the unique key
     *
     * @return self
     */
    public function setUniqueKey($unique_key)
    {
        $this->unique_key = $unique_key;
        $this->primary_key = false;
        $this->key = false;

        return $this;
    }

    /**
     * Gets the value of key.
     *
     * @since 1.0.0
     *
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Sets the value of key
     *
     * @since 1.0.0
     *
     * @param mixed $key the key
     *
     * @return self
     */
    public function setKey($key)
    {
        $this->key = $key;
        $this->unique_key = false;
        $this->primary_key = false;

        return $this;
    }

    /**
     * Gets the value of default.
     *
     * @since 1.0.0
     *
     * @return mixed
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Sets the value of default.
     *
     * @since 1.0.0
     *
     * @param mixed $default the default
     *
     * @return self
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Gets the value of type_parameters.
     *
     * @since 1.0.0
     *
     * @return mixed
     */
    public function getTypeParameters()
    {
        return $this->type_parameters;
    }

    /**
     * Sets the value of type_parameters
     *
     * @since 1.0.0
     *
     * @param mixed $type_parameters The Type Parameters
     *
     * @return self
     */
    public function setTypeParameters($type_parameters)
    {
        $this->type_parameters = $type_parameters;

        return $this;
    }

    /**
     * Gets the value of auto_increment.
     *
     * @since 1.0.0
     *
     * @return mixed
     */
    public function getAutoIncrement()
    {
        return $this->auto_increment;
    }

    /**
     * Sets the value of auto_increment.
     *
     * @since 1.0.0
     *
     * @param mixed $auto_increment the auto increment
     *
     * @return self
     */
    public function setAutoIncrement($auto_increment)
    {
        $this->auto_increment = $auto_increment;

        return $this;
    }
}
