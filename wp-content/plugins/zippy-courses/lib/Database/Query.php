<?php
/**
 * @since   1.0.0
 */
class Zippy_DatabaseQuery implements Zippy_RepositoryObject
{
    /**
     * ID
     *
     * @since 1.0.0
     *
     * @var string
     */
    public $id;

    /**
     * Prepared SQL
     *
     * @since 1.0.0
     *
     * @var string
     */
    public $sql;

    /**
     * Results of query
     *
     * @since 1.0.0
     *
     * @var mixed
     */
    public $results;

    public function __construct($id, $sql = '')
    {
        $this->id   = $id;
        $this->sql  = $sql;
    }

/**
 *--------------------------------------------------------------------------
 * GETTERS & SETTERS
 *--------------------------------------------------------------------------
 */
    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the value of id.
     *
     * @param mixed $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the value of sql.
     *
     * @return mixed
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * Sets the value of sql.
     *
     * @param mixed $sql the sql
     *
     * @return self
     */
    public function setSql($sql)
    {
        $this->sql = $sql;

        return $this;
    }

    public function getResults()
    {
        global $wpdb;
        
        $zippy = Zippy::instance();

        if ($this->results === null) {
            $sql = $this->getSql();
            $this->results = !empty($sql) ? $wpdb->get_results($this->getSql()) : null;
        }
        
        if ($this->results !== null) {
            $this->results = maybe_unserialize($this->results);
            $this->results = $zippy->utilities->maybeJsonDecode($this->results);
        }

        return $this->results;
    }

    public function getCol()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        if ($this->results === null) {
            $sql = $this->getSql();
            $this->results = !empty($sql) ? $wpdb->get_col($this->getSql()) : null;
        }

        if ($this->results !== null) {
            $this->results = maybe_unserialize($this->results);
            $this->results = $zippy->utilities->maybeJsonDecode($this->results);
        }

        return $this->results;
    }

    public function getVar()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        if ($this->results === null) {
            $sql = $this->getSql();
            $this->results = !empty($sql) ? $wpdb->get_var($this->getSql()) : null;
        }

        if ($this->results !== null) {
            $this->results = maybe_unserialize($this->results);
            $this->results = $zippy->utilities->maybeJsonDecode($this->results);
        }

        return $this->results;
    }

    public function flushResults()
    {
        $this->results = null;
    }
}
