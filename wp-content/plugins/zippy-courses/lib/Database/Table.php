<?php

class Zippy_DatabaseTable implements Zippy_RepositoryObject
{
    public $id;
    public $table;
    public $db_version;
    public $mode = 'create';

    const TABLE_PREFIX = 'zippy_';

    public function __construct($id)
    {
        global $wpdb;

        $this->id       = $id;
        $this->table    = $wpdb->prefix . self::TABLE_PREFIX . $id;
        $this->columns  = new Zippy_DatabaseTableColumns;
    }

    public function addColumn($id, $type)
    {
        $zippy = Zippy::instance();
        
        $column = $zippy->make('database_table_column', array('id' => $id));
        $column->setType($type);
        
        $this->columns->add($column);

        return $this->columns->get($id);
    }

    public function install()
    {
        dbDelta($this->generateSql());
    }

    public function generateSql()
    {
        global $wpdb;
        $output = '';

        switch ($this->mode) {
            case 'update':

                break;
            case 'create':
            default:
                $output .= "CREATE TABLE {$this->table} ";
                break;
        }

        $output .=  "(\n";

        $output .= $this->generateColumnsSql();

        $output .= $this->generateKeysSql();

        $output .=  ") ";
    
        $output .= $wpdb->get_charset_collate();

        $output .= " ENGINE=InnoDB;";
        
        return $output;
        
    }

    public function generateColumnsSql()
    {
        $output = '';

        foreach ($this->columns->all() as $column) {
            $output .= $column->generateSql();
        }

        return $output;
    }

    public function generateKeysSql()
    {
        $output = '';
        $primary_keys = $this->columns->where('primary_key', true, '===');
        $column = reset($primary_keys);

        if ($column) {
            $output .= $column->generatePrimaryKey();
        }

        $unique_keys = $this->columns->where('unique_key', true, '===');
        foreach ($unique_keys as $column) {
            $output .= $column->generateUniqueKey();
        }

        $keys = $this->columns->where('key', true, '===');
        foreach ($keys as $column) {
            $output .= $column->generateKey();
        }

        return rtrim(trim($output), ',') . "\n";
    }

    public function getId()
    {
        return $this->id;
    }
}
