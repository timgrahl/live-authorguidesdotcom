<?php

class Zippy_DatabaseTableColumns extends Zippy_Repository
{
    public function __construct()
    {
        $this->addValidType('Zippy_DatabaseTableColumn');
    }
}
