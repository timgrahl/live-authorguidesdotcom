<?php

class Zippy_DatabaseTables extends Zippy_Repository
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->addValidType('Zippy_DatabaseTable');
    }

    public function install()
    {
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        
        foreach ($this->all() as $table) {
            $table->install();
        }
    }
}
