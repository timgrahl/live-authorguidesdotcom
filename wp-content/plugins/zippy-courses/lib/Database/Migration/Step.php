<?php

abstract class Zippy_MigrationStep extends Zippy_Model
{
    const COUNT_LIMIT = 100;
    
    public $id;
    public $label;
    public $actions = 0;

    public function getActions()
    {
        return $this->actions;
    }

    public function getId()
    {
        return $this->id;
    }
    /**
     * Get a quick idea of how many steps will be necessary in the migration
     *
     * @return int $count
     */
    abstract public function analyze();
    abstract public function migrate();

    public function backupMeta($post_id)
    {
        $meta = get_post_meta($post_id);
        add_post_meta($post_id, '_zippy_migration_backup', $meta, true);
    }

    public function removeMeta($post_id, array $keys)
    {
        foreach ($keys as $meta_key) {
            delete_post_meta($post_id, $meta_key);
        }
    }

    public function incrementCurrentStep($i = 1)
    {
        if (($current_step = get_transient('zippy_upgrade_current_step')) === false) {
            $current_step = 1;
        }

        $current_step += $i;

        $this->clearMemory();
    }

    public function clearMemory()
    {
        global $wpdb, $wp_actions;
        $wpdb->queries = array();
        $wp_actions = array();
        wp_cache_flush();
    }

    public function clearTransients()
    {
        delete_transient('zippy_v1_0_0_' . $this->id . '_status');
        delete_transient('zippy_v1_0_0_' . $this->id . '_migrated');
    }

    public function completeStep()
    {
        set_transient('zippy_v1_0_0_' . $this->id . '_status', 'complete');
    }

    public function isCompleted()
    {
        $status = get_transient('zippy_v1_0_0_' . $this->id . '_status');

        return $status == 'complete';
    }

    /**
     * Gets the value of label.
     *
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Sets the value of label.
     *
     * @param mixed $label the label
     *
     * @return self
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }
}
