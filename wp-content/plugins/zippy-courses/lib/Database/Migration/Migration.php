<?php

abstract class Zippy_DatabaseMigration
{
    public $steps;
    public $actions = 0;

    public function __construct(Zippy_MigrationSteps $steps)
    {
        $this->steps = $steps;
        $this->buildSteps();
    }

    abstract public function buildSteps();

    public function getSteps()
    {
        return $this->steps;
    }

    public function analyze()
    {
        foreach ($this->steps->all() as $step) {
            $step->analyze();
            $this->actions    += $step->getActions();
        }
    }

    public function migrate()
    {
        $zippy = Zippy::instance();

        $zippy->cache->set('zippy_upgrade_counter', 0);

        foreach ($this->steps->all() as $step) {
            $step->migrate();
        }
    }

    public function getActions()
    {
        return $this->actions;
    }

    public function checkStatus()
    {
        $status = true;

        foreach ($this->steps->all() as $step) {
            if (!$step->isCompleted()) {
                $status = false;
            }

            if (!$status) {
                break;
            }
        }

        return $status ? 'complete' : $this->getCurrent();
    }

    public function cleanup()
    {
        foreach ($this->steps->all() as $step) {
            $step->clearTransients();
        }        
    }

    public function getCurrent()
    {
        $current = null;

        foreach ($this->steps->all() as $step) {
            if ($step->isCompleted() || $current !== null) {
                continue;
            }

            $current = $step->getLabel();
        }

        return $current === null ? 'complete' : $current;
    }
}
