<?php

/**
 * @since   1.0.0
 */
class Zippy_DatabaseQueries extends Zippy_Repository
{
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->addValidType('Zippy_DatabaseQuery');

        add_action('admin_init', array($this, 'stripSearchQueryArgs'));
        add_filter('user_search_columns', array($this, 'addDisplayNameToUserQuery'));
    }

    public function addDisplayNameToUserQuery($search_columns) {
        $search_columns[] = 'display_name';
        return $search_columns;
    }

    public function stripSearchQueryArgs()
    {
        if (isset($_GET['page']) &&
            (
                $_GET['page'] == 'zippy-students' ||
                $_GET['page'] == 'zippy-analytics'
            ) &&
            !empty($_GET['_wp_http_referer'])
        ) {
             wp_redirect(remove_query_arg(array('_wp_http_referer', '_wpnonce'), wp_unslash($_SERVER['REQUEST_URI'])));
             exit;
        }
    }
}
