<?php

class Zippy_Email
{
    public $user;
    public $order;
    public $event;
    public $type;
    public $content;
    public $title;
    public $enabled = true;

    public function __construct($type, Zippy_User $user, Zippy_Event $event)
    {
        $zippy = Zippy::instance();

        $this->type     = $type;
        $this->user     = $user;
        $this->event    = $event;
        $this->setup();
    }

    public function getRecipients()
    {
        if ($this->type == 'new_student' || $this->type == 'course_completion') {
            return isset($option['sender_email']) && !empty($option['sender_email'])
                ? $option['sender_email']
                : get_bloginfo('admin_email');
        }
        
        return $this->user->email;
    }

    public function getTitle()
    {
        return $this->doShortcodes($this->title);
    }

    public function getContent()
    {
        return wpautop($this->doShortcodes($this->content));
    }

    public function getHeaders()
    {
        return  'From: ' . $this->headers['name'] . ' <' . $this->headers['email'] . '>' . "\r\n" .
                'Content-Type: text/html' . "\r\n";
    }

    private function doShortcodes($content)
    {
        $zippy = Zippy::instance();

        $find = array(
            '/\[first_name\]/',
            '/\[last_name\]/',
            '/\[username\]/',
            '/\[password\]/',
            '/\[reset_password_url\]/',
            '/\[forgot_password_url\]/',
            '/\[contact_email\]/',
            '/\[registration_claim_url\]/',
            '/\[login_claim_url\]/',
            '/\[site_name\]/',
            '/\[order_course\]/',
            '/\[order_receipt\]/',
            '/\[order_history_url\]/',
            '/\[course\]/',
            '/\[student_admin_profile_link\]/',
            '/\[completed_course\]/',
        );

        // HTML tags to replace BBcode
        $replace = array(
            $this->user->getFirstName(),
            $this->user->getLastName(),
            $this->user->getUsername(),
            $this->user->getPassword(),
            $this->user->getPasswordRecoveryUrl(),
            $zippy->core_pages->getUrl('forgot_password'),
            $this->headers['email'],
            $zippy->utilities->orders->getRegistrationUrl($this->event->order),
            $zippy->utilities->orders->getLoginUrl($this->event->order),
            get_bloginfo('name'),
            $zippy->utilities->orders->getCourses($this->event->order),
            $zippy->utilities->orders->getReceipt($this->event->order),
            $zippy->core_pages->getUrl('order_history'),
            $this->getCourseTitle(),
            $this->user->getProfileLink(),
        );

        $filtered = preg_replace($find, $replace, $content);

        return $filtered;
    }

    /**
     * Checks if a Course event is attached to this event.
     * If yes, get the title of that course
     * If no, get a list of courses on the associated order
     * @return string Course or list of courses
     */
    private function getCourseTitle()
    {
        $zippy = Zippy::instance();
        if (isset($this->event->course)) {
            return get_the_title($this->event->course->getId());
        }

        return $zippy->utilities->orders->getCourseList($this->event->order);
    }

    private function setup()
    {
        $this->setupEmail();
        $this->setupHeaders();
    }

    private function setupEmail()
    {
        $zippy = Zippy::instance();
        $option_name    = 'zippy_system_emails_' . $this->type . '_settings';
        $option         = get_option($option_name, array());

        $this->content  = isset($option['content']) ? $option['content'] : '';
        $this->title    = isset($option['title']) ? $option['title'] : '';
        $this->enabled  = isset($option['enabled']) && is_array($option['enabled']) ? true : false;
        if ($this->event->hasProduct()) {
            $product = $zippy->make('product', array($this->event->order->product->getId()));
            $product->setEmailContent($this);
        }
    }

    private function setupHeaders()
    {
        $option = get_option('zippy_system_emails_general_settings', array());

        $email        = isset($option['sender_email']) && !empty($option['sender_email'])
                            ? $option['sender_email']
                            : get_bloginfo('admin_email');

        $name         = isset($option['sender_name']) && !empty($option['sender_name'])
                            ? $option['sender_name']
                            : get_bloginfo('blogname');

        $this->headers = array('email' => $email, 'name' => $name);
    }
}
