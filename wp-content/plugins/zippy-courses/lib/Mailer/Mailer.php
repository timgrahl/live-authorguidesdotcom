<?php

class Zippy_Mailer extends Zippy_Listener
{
    public function __construct()
    {
        $this->register();
    }

    public function register()
    {
        $zippy = Zippy::instance();
        
        $zippy->events->register('ForgotPassword', $this);
        $zippy->events->register('Registration', $this);
        $zippy->events->register('NewOrder', $this);
        $zippy->events->register('NewStudent', $this);
        $zippy->events->register('CourseCompletion', $this);
    }

    public function handle(Zippy_Event $event)
    {
        if (!isset($event->order)) {
            $event->order = 0;
        }

        if (apply_filters('zippy_disable_emails', false)) {
            return;
        }

        switch ($event->getEventName()) {
            case 'ForgotPassword':
                $event->type = 'forgot_password';
                $event->user = $event->student;
                $this->send($event);
                break;
            case 'NewOrder':
                $event->type = 'purchase_receipt';
                $event->user = $event->order->getCustomer();

                if ($event->user instanceof Zippy_User) {
                    $this->send($event);
                }

                if ($event->order->getOwner() < 1) {
                    $event->type = 'registration_reminder';
                    $this->send($event);
                }

                break;
            case 'Registration':
                $event->type = 'registration';
                $event->user = $event->student;
                $this->send($event);
                break;
            case 'NewStudent':
                $event->type = 'new_student';
                $event->user = $event->student;
                $this->send($event);
                break;
            case 'CourseCompletion':
                $event->type = 'course_completion';
                $event->user = $event->student;
                $this->send($event);
                break;
            default:
                break;
        }
    }

    public function send(Zippy_Event $event)
    {
        $zippy = Zippy::instance();

        if (!isset($event->user) || !isset($event->user->email)) {
            return;
        }
        
        $email = $zippy->make('email', array('type' => $event->type, 'user' => $event->user, 'event' => $event));

        if (!$email->enabled) {
            return;
        }

        $sent = wp_mail(
            $email->getRecipients(),
            $email->getTitle(),
            $email->getContent(),
            $email->getHeaders()
        );

        if ($sent) {
            $zippy->log('Sent "' . $email->getTitle() . '" to ' . $email->getRecipients(), 'EMAIL_SENT');
        } else {
            $zippy->log('Failed sending "' . $email->getTitle() . '" to ' . $email->getRecipients(), 'EMAIL_ERROR');
        }
    }
}
