<?php

class Zippy_MetaboxRegionSection
{
    public $screen = 'post';
    public $context;

    public function __construct($context)
    {
        $this->context      = $context;
    }

    public function setScreen($screen)
    {
        $this->screen = $screen;
        return $this;
    }

    public function render()
    {
        $post_id = isset($_GET['post_id']) ? $_GET['post_id'] : 0;
        $post = get_post($post_id);

        if ($post !== null) {
            do_meta_boxes($this->screen, $this->context, $post);
        } else {
            do_meta_boxes($this->screen, $this->context, '');
        }
    }

    public function getId()
    {
        return $this->id;
    }
}
