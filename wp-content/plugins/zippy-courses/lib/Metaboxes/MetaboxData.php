<?php

class Zippy_MetaboxData
{
    private $field;
    private $required_keys;
    private $type;

    public function __construct($field_data)
    {
        $this->parse($field_data);
    }

    private function parse($field_data)
    {
        $parameters = explode(':', trim($field_data));

        $this->field = $parameters[0];

        if (isset($parameters[1])) {
            $type = trim($parameters[1]);

            switch ($type) {
                case 'array':
                    $this->type = 'array';
                    if (isset($parameters[2])) {
                        $this->required_keys = explode('|', trim($parameters[2]));
                    }
                    break;
                case 'json':
                    $this->type = 'json';
                    break;
                case 'int':
                case 'integer':
                    $this->type = 'integer';
                    break;
                case 'string':
                default:
                    $this->type = 'string';
                    break;
            }
        } else {
            $this->type = 'string';
        }
    }

    public function fetch($post_id)
    {
        if ($post_id == 0) {
            return null;
        }

        $data = '';

        switch ($this->type) {
            case 'array':
                $data = (array) get_post_meta($post_id, $this->field, true);
                break;
            case 'json':
                $data = get_post_meta($post_id, $this->field, true);
                $data = is_string($data) ? json_decode($data) : $data;
                break;
            case 'string':
            default:
                $data = get_post_meta($post_id, $this->field, true);
                $json = is_string($data) ? json_decode($data) : $data;

                $data = $json !== null ? $json : $data;
                break;
        }

        return apply_filters('zippy_fetch_meta_data', $data, $this->field);
    }

    public function getField()
    {
        return $this->field;
    }

    public function getType()
    {
        return $this->type;
    }
}
