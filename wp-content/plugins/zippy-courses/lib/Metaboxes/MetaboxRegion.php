<?php

class Zippy_MetaboxRegion extends Zippy_Model
{
    public $id;
    public $title;
    public $post_types = array();
    public $sections = array();

    public function __construct($id, array $post_types = array())
    {
        $this->id           = $id;
        $this->post_types   = $post_types;
    }

    public function render()
    {
        echo '<div class="zippy-metabox-region" id="' . $this->id . '">';

        foreach ($this->sections as &$section) {
            foreach ($this->post_types as $post_type) {
                $section->setScreen($post_type);
                $section->render();
            }
        }
        echo '</div>';
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPostTypes()
    {
        return $this->post_types;
    }

    public function addSection($context)
    {
        $this->sections[$context] = new Zippy_MetaboxRegionSection($context);

        return $this->sections[$context];
    }
}
