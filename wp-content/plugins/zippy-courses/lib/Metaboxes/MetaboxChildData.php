<?php

class Zippy_MetaboxChildData
{
    private $meta_key;
    private $child_key;

    public function __construct($data)
    {
        $this->parse($data);
    }

    private function parse($data)
    {
        $parameters = explode(':', trim($data));

        $this->meta_key = $parameters[0];

        if (isset($parameters[1])) {
            $this->child_key = trim($parameters[1]);
        }
    }

    public function fetch($post_id)
    {
        if ($post_id == 0) {
            return null;
        }

        $data = $this->child_key === null ? $this->fetchByMeta($post_id) : $this->fetchByChildMeta($post_id);

        return apply_filters('zippy_fetch_meta_child_data', $data, $this->meta_key);
    }

    public function getKey()
    {
        return $this->meta_key;
    }

    public function fetchByMeta($post_id)
    {
        global $wpdb;

        $data   = get_post_meta($post_id, $this->meta_key, true);
        $data   = is_array($data) ? implode(',', $data) : trim($data);

        return $this->fetchList($data);
    }

    public function fetchByChildMeta($post_id)
    {
        global $wpdb;

        $data = $wpdb->get_col($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", $this->child_key, $post_id));

        return $this->fetchList(implode(',', $data));
    }

    private function fetchList($list)
    {
        global $wpdb;

        return $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->posts WHERE ID IN(%s)", $list));
    }
}
