<?php

class Zippy_Metaboxes extends Zippy_Repository
{
    private static $instance;

    private $metaboxes = array();

    private function __construct()
    {
        $this->addValidType('Zippy_Metabox');

        add_action('admin_init', array($this, 'register'));
        add_action('save_post', array($this, 'save'));
    }

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }
    
    public function register()
    {
        foreach ($this->all() as $mb) {
            $mb->register();
        }
    }

    public function save($post_id)
    {
        foreach ($this->all() as $mb) {
            $mb->save($post_id);
        }
    }
}
