<?php

class Zippy_Metabox extends Zippy_Model
{
    public $id;
    public $title;
    public $priority = 'default';
    public $context = 'normal';
    public $view;
    public $vars = array();
    public $data = array();
    public $children = array();
    public $post_types = array('post');

    public function __construct($id, array $args = array())
    {
        $this->id = $id;
        $this->processArgs($args);
    }

    public function register()
    {
        if (in_array('all', $this->post_types)) {
            $this->globalRegister();
        } else {
            foreach ($this->post_types as $post_type) {
                add_meta_box($this->getId(), $this->getTitle(), array($this, 'render'), $post_type, $this->getContext(), $this->getPriority());
            }
        }
    }

    public function globalRegister()
    {
        $post_types = array();

        $args = array(
           'public'   => true
        );

        $output = 'names';
        $operator = 'and';

        $excluded = array('attachment', 'zippy_order', 'transaction');
        $post_types = get_post_types($args, $output);

        foreach ($post_types as $post_type) {
            if (!in_array($post_type, $excluded)) {
                add_meta_box($this->getId(), $this->getTitle(), array($this, 'render'), $post_type, $this->getContext(), $this->getPriority());
            }
        }
    }

    public function render()
    {
        return apply_filters("zippy_metabox_html", $this->renderView(), $this->id);
    }

    private function renderView()
    {
        $file = $this->getViewFile();
 
        if ($file) {
            global $post;

            $zippy = Zippy::instance();

            $view = $zippy->make('file_view', array('file' => $file, 'vars' => $this->getVars(), 'data' => $this->getData(), 'children' => $this->getChildren()));
           
            echo $view->render();
        }
    }

    private function getViewFile()
    {
        if ($this->view === null) {
            $file = ZippyCourses::$path . 'assets/views/metaboxes/' . $this->id . '.php';
            if (file_exists($file)) {
                return $file;
            }
        }

        return file_exists($this->view) ? $this->view : false;
    }

    private function processArgs(array $args)
    {
        foreach ($args as $key => $arg) {
            if (property_exists($this, $key)) {
                if ($key == 'post_types') {
                    $this->{$key} = is_array($arg) ? $arg : explode(',', $arg);
                    // Trim whitespaces from the post types array
                    $this->{$key} = array_map('trim', $this->{$key});
                } else {
                    $this->{$key} = $arg;
                }
            }
        }

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title ? $this->title : '';
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function getPostTypes()
    {
        return $this->post_types;
    }

    public function getVars()
    {
        if (!is_array($this->vars)) {
            $this->vars = explode(',', $this->vars);
        }

        return $this->vars;
    }

    public function getChildren()
    {
        if (!is_array($this->children)) {
            $this->children = explode(',', $this->children);
        }

        return $this->children;
    }

    public function getData()
    {
        if (!is_array($this->data)) {
            $this->data = explode(',', $this->data);
        }

        return array_filter($this->data);
    }

    public function save($post_id)
    {
        if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX) || (defined('DOING_CRON') && DOING_CRON)) {
            return;
        }
        
        $zippy = Zippy::instance();

        $post = get_post($post_id);

        if ((is_object($post) && !in_array($post->post_type, $this->post_types)) && !in_array('all', $this->post_types)) {
            return;
        }

        $vars = $this->getVars();
        $fields = array();

        foreach ($this->getVars() as $var) {
            $field = $zippy->make('data', array('field_data' => trim($var)));
            $fields[] = $field;
        }

        foreach ($fields as $field) {
            $value = isset($_POST[$field->getField()])
                ? (is_string($_POST[$field->getField()])
                    ? html_entity_decode($_POST[$field->getField()])
                    : $_POST[$field->getField()])
                : false;

            if ($value !== false) {
                if ($field->getType() == 'json') {
                    // $value = preg_replace('/\\\"/',"\"", $value);
                    update_post_meta($post_id, $field->getField(), $value);
                } else {
                    update_post_meta($post_id, $field->getField(), $_POST[$field->getField()]);
                }
            } else {
                delete_post_meta($post_id, $field->getField());
            }
        }
    }
}
