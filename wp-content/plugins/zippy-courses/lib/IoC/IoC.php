<?php

class Zippy_IoC
{
    private static $instance;

    /**
     * References for our IoC container
     * @var array
     */
    private $container = array();

    /**
     * Get an instance of this singleton
     * @return \self
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Bootstrap our singleton
     */
    private function __construct()
    {
        $this->registerBindings();
    }

    /**
     * Bind a key to an associated class, and set whether we should use it
     * as a singleton
     *
     * @param  string  $key       Reference for class
     * @param  string  $class     Name of class
     * @param  boolean $singleton Whether or not to use a singleton
     * @return self
     */
    public function bind($key, $class, $singleton = false)
    {
        $this->container[$key] = array(
            'class'     => $class,
            'singleton' => $singleton,
            'instance'  => null
        );

        return $this;
    }

    /**
     * Make a new object, and all of it's dependencies
     * @param  string $key  Reference key for our class
     * @param  array  $args Arguments to pass to new object
     * @return $ojbect|null New object or null
     */
    public function make($key, $args = array())
    {
        $key = $this->getKey($key);

        if ($key) {
            return $this->fetch($key, $args);
        }

        return;
    }

    /**
     * Return a singleton or an object based on it's key
     * @param  string $key              Reference key for the requested class
     * @param  array  $args             Arguments to pass to new object
     * @return $singleton|$object       Singleton instance or new ojbect
     */
    private function fetch($key, $args = array())
    {
        return $this->container[$key]['singleton'] ? $this->getSingletonInstance($key) : $this->getObject($key, $args);
    }

    /**
     * Get an instance of our singleton using the instance method.
     *
     * @param  string $key Reference key for our class
     * @return $instance   Instance of the requested singleton
     */
    private function getSingletonInstance($key)
    {
        return call_user_func(array($this->container[$key]['class'], 'instance'));
    }

    private function getObject($key, $args = array())
    {
        $reference  = $this->container[$key];
        $class      = $this->container[$key]['class'];
        $instance   = new ReflectionClass($class);

        if ($this->constructorHasArgs($class)) {
            $parameters = $this->buildParams($key, $args);
            $instance = $instance->newInstanceArgs($parameters);

            return $instance;
        }

        return $instance->newInstance();
    }

    /**
     * Does a constructor have arguments in it's class, or can we just
     * call new up an object?
     *
     * @param  string $class Name of the class
     * @uses   \ReflectionClass
     * @return bool
     */
    private function constructorHasArgs($class)
    {
        $r = new ReflectionClass($class);
        return $r->hasMethod('__construct') && count($r->getMethods()) > 0 && count($r->getMethod('__construct')->getParameters()) > 0;
    }

    /**
     * Build the parameters requried for the constructor of a class
     * @param  string $key  Reference key for our class
     * @param  array  $args A list of arguments passed in from make command
     * @return $output      The built up object
     */
    private function buildParams($key, $args = array())
    {
        $zippy = Zippy::instance();

        $reference  = $this->container[$key];
        $class      = $this->container[$key]['class'];

        $r = new ReflectionClass($class);
        $output = array();

        $params = $r->getMethod('__construct')->getParameters();

        if ($zippy->utilities->array->isAssociative($args)) {
            if ($key == 'settings_tabs_view') {
                // Associative
            }

            foreach ($params as $k => $param) {
                if (array_key_exists($param->getName(), $args)) {
                    $output[$k] = $args[$param->getName()];
                } else {
                    $output[$k] = $this->buildParam($param);
                }
            }
        } else {
            if (count($params) == count($args)) {
                $i = 0;
                $args = array_values($args);
                foreach ($params as $k => $param) {
                    $output[$param->getName()] = $args[$i];
                    $i++;
                }
            } else {
                foreach ($params as $k => $param) {
                    $output[$k] = $this->buildParam($param);
                }
            }
        }

        return $output;
    }

    private function buildParam(ReflectionParameter $param)
    {
        if ($param->getClass() !== null) {
            return $this->make($param->getClass()->getName());
        } elseif ($param->isArray()) {
            return array();
        } elseif ($param->isOptional()) {
            return $param->getDefaultValue();
        } else {
            return '';
        }
    }

    /**
     * Get the key for a reference, whether it's a key or the name of the class
     * @param  string $query A key or a class name
     * @return string|null
     */
    private function getKey($query)
    {
        if (isset($this->container[$query])) {
            return $query;
        }

        foreach ($this->container as $key => $reference) {
            if ($reference['class'] == $query) {
                return $key;
            }
        }

        return;
    }

    /**
     * Bind the default relationships between a key and their counterpart
     * classes into the Inversion of Control container
     *
     * @return void
     */
    private function registerBindings()
    {
        // Primary app
        $this->bind('zippy_courses', 'ZippyCourses', true);
    
        // Utilities
        $this->bind('utilities', 'Zippy_Utilities');
        $this->bind('array_utilities', 'Zippy_ArrayUtilities');
        $this->bind('log', 'Zippy_Log', true);
        $this->bind('activity', 'Zippy_Activity', true);
        $this->bind('analytics', 'ZippyCourses_Analytics');
        
        // Access
        $this->bind('access', 'Zippy_Access', true);

        // Mailer
        $this->bind('mailer', 'Zippy_Mailer');
        $this->bind('email', 'Zippy_Email');
        
        // Cache
        $this->bind('cache', 'Zippy_Cache');

        // Routes
        $this->bind('routes', 'ZippyCourses_Routes', true);
        $this->bind('core_pages', 'ZippyCourses_CorePages', true);

        // Admin Pages
        $this->bind('admin_pages_repository', 'Zippy_AdminPages', true);
        $this->bind('admin_page', 'Zippy_AdminPage');
        $this->bind('admin_pages', 'ZippyCourses_AdminPages');

        // Database
        $this->bind('query', 'Zippy_DatabaseQuery');
        $this->bind('queries', 'Zippy_DatabaseQueries', true);
        $this->bind('database', 'ZippyCourses_Database');
        $this->bind('database_table_column', 'Zippy_DatabaseTableColumn');
        $this->bind('database_table_columns', 'Zippy_DatabaseTableColumns');
        $this->bind('database_table', 'Zippy_DatabaseTable');
        $this->bind('database_tables', 'Zippy_DatabaseTables', true);

        // Forms
        $this->bind('field', 'Zippy_FormField');
        
        // Settings Pages
        $this->bind('settings', 'ZippyCourses_Settings');
        $this->bind('settings_pages_repository', 'Zippy_SettingsPages', true);
        $this->bind('settings_page', 'Zippy_SettingsPage');
        $this->bind('settings_section_repository', 'Zippy_SettingsSections', true);
        $this->bind('settings_section', 'Zippy_SettingsSection');
        $this->bind('settings_field', 'Zippy_SettingsField');
        $this->bind('settings_pages', 'ZippyCourses_SettingsPages');

        // Metaboxes
        $this->bind('metabox_repository', 'Zippy_Metaboxes', true);
        $this->bind('metabox_regions', 'ZippyCourses_MetaboxRegions');
        $this->bind('metabox_region', 'Zippy_MetaboxRegion');
        $this->bind('metabox_region_section', 'Zippy_MetaboxRegionSection');
        $this->bind('metabox', 'Zippy_Metabox');
        $this->bind('metaboxes', 'ZippyCourses_Metaboxes');
        $this->bind('data', 'Zippy_MetaboxData');
        $this->bind('child_data', 'Zippy_MetaboxChildData');
        
        // Post Types
        $this->bind('post_type_repository', 'Zippy_PostTypes', true);
        $this->bind('post_type', 'Zippy_PostType');
        $this->bind('post_types', 'ZippyCourses_PostTypes');

        // Tabs
        $this->bind('tabs', 'Zippy_Tabs');
        $this->bind('tab', 'Zippy_Tab');
        $this->bind('settings_tabs', 'Zippy_SettingsTabs');
        $this->bind('settings_tab', 'Zippy_SettingsTab');
        $this->bind('pages_tabs', 'Zippy_Pages_Tabs');
        $this->bind('pages_tab', 'Zippy_Pages_Tab');
        $this->bind('post_type_tabs', 'ZippyCourses_PostTypeTabs');
        $this->bind('post_type_tab', 'Zippy_PostTypeTab');

        // Forms
        $this->bind('registration_form', 'ZippyCourses_Registration_Form');
        $this->bind('order_form', 'ZippyCourses_Order_Form');
        $this->bind('login_form', 'ZippyCourses_Login_Form');
        $this->bind('edit_account_form', 'ZippyCourses_EditAccount_Form');
        $this->bind('change_password_form', 'ZippyCourses_EditAccount_Form');

        // Form Processors
        $this->bind('form_processor', 'Zippy_FormProcessor');
        $this->bind('registration_form_processor', 'ZippyCourses_Registration_FormProcessor');
       
        $this->bind('login_form', 'ZippyCourses_Login_Form');
        $this->bind('login_form_processor', 'ZippyCourses_Login_FormProcessor');
        
        // Form Resources
        $this->bind('forms_repository', 'Zippy_Forms', true);
        $this->bind('forms', 'ZippyCourses_Forms');

        $this->bind('fields_repository', 'Zippy_FieldsRepository');
        $this->bind('field', 'Zippy_FormField');
        $this->bind('text_field', 'Zippy_Text_FormField');
        $this->bind('html_field', 'Zippy_Html_FormField');
        $this->bind('file_field', 'Zippy_File_FormField');
        $this->bind('hidden_field', 'Zippy_Hidden_FormField');
        $this->bind('password_field', 'Zippy_Password_FormField');
        $this->bind('email_field', 'Zippy_Email_FormField');
        $this->bind('checkbox_field', 'Zippy_Checkbox_FormField');
        $this->bind('select_field', 'Zippy_Select_FormField');
        $this->bind('radio_field', 'Zippy_Radio_FormField');
        $this->bind('textarea_field', 'Zippy_Textarea_FormField');
        $this->bind('datepicker_field', 'Zippy_DatePicker_FormField');


        // Panels
        $this->bind('panels', 'Zippy_Panels');
        $this->bind('panel', 'Zippy_Panel');
        $this->bind('panel', 'Zippy_Panel');
        $this->bind('settings_panels', 'Zippy_SettingsPanels');
        $this->bind('settings_panel', 'Zippy_SettingsPanel');
        $this->bind('post_type_panels', 'ZippyCourses_PostTypePanels');
        $this->bind('post_type_panel', 'Zippy_PostTypePanel');
        $this->bind('panel_repository', 'Zippy_Panels', true);

        // Events
        $this->bind('dispatcher', 'Zippy_Dispatcher');
        $this->bind('new_student_event', 'ZippyCourses_NewStudent_Event');
        $this->bind('join_product_event', 'ZippyCourses_JoinProduct_Event');

        // Views of various sorts
        $this->bind('file_view', 'Zippy_FileView');
        $this->bind('form_view', 'Zippy_FormView');
        $this->bind('field_view', 'Zippy_FormFieldView');
        $this->bind('tabs_view', 'Zippy_TabsView');
        $this->bind('tab_view', 'Zippy_TabView');
        $this->bind('settings_tabs_view', 'Zippy_SettingsTabsView');
        $this->bind('settings_tab_view', 'Zippy_SettingsTabView');
        $this->bind('settings_panels_view', 'Zippy_SettingsPanelsView');
        $this->bind('settings_panel_view', 'Zippy_SettingsPanelView');

        $this->bind('pages_tabs_view', 'Zippy_Pages_TabsView');
        $this->bind('pages_tab_view', 'Zippy_Pages_TabView');

        $this->bind('panels_view', 'Zippy_PanelsView');
        $this->bind('panel_view', 'Zippy_PanelView');

        // Users
        $this->bind('student', 'ZippyCourses_Student');
        $this->bind('students', 'ZippyCourses_Students');
        $this->bind('users', 'Zippy_Users');
        $this->bind('visitor', 'ZippyCourses_Visitor');
        $this->bind('customer', 'Zippy_Customer');

        // Shortcodes
        $this->bind('shortcodes', 'ZippyCourses_Shortcodes');
        $this->bind('shortcode', 'Zippy_Shortcode');
        $this->bind('registration_form_shortcode_view', 'ZippyCourses_RegistrationForm_ShortcodeView');
        $this->bind('order_form_shortcode_view', 'ZippyCourses_OrderForm_ShortcodeView');
        $this->bind('course_directory_shortcode_view', 'ZippyCourses_CourseDirectory_ShortcodeView');
        $this->bind('dashboard_shortcode_view', 'ZippyCourses_Dashboard_ShortcodeView');

        $this->bind('login_shortcode_view', 'ZippyCourses_Login_ShortcodeView');
        $this->bind('order_history_shortcode_view', 'ZippyCourses_OrderHistory_ShortcodeView');
        $this->bind('forgot_password_shortcode_view', 'ZippyCourses_ForgotPassword_ShortcodeView');
        $this->bind('reset_password_shortcode_view', 'ZippyCourses_ResetPassword_ShortcodeView');
        $this->bind('edit_account_shortcode_view', 'ZippyCourses_EditAccount_ShortcodeView');
        $this->bind('account_shortcode_view', 'ZippyCourses_Account_ShortcodeView');
        $this->bind('complete_lesson_shortcode_view', 'ZippyCourses_CompleteLesson_ShortcodeView');
        $this->bind('first_name_shortcode_view', 'ZippyCourses_FirstName_ShortcodeView');
        $this->bind('last_name_shortcode_view', 'ZippyCourses_LastName_ShortcodeView');
        $this->bind('username_shortcode_view', 'ZippyCourses_Username_ShortcodeView');
        $this->bind('button_shortcode_view', 'ZippyCourses_Button_ShortcodeView');
        $this->bind('buy_now_shortcode_view', 'ZippyCourses_BuyNow_ShortcodeView');
        $this->bind('contact_email_link_shortcode_view', 'ZippyCourses_ContactEmailLink_ShortcodeView');
        $this->bind('progress_bar_shortcode_view', 'ZippyCourses_ProgressBar_ShortcodeView');

        // Validator
        $this->bind('validator', 'Zippy_Validator', true);

        // Middleware
        $this->bind('middlewares', 'Zippy_Middlewares', true);
        $this->bind('middleware_rules', 'Zippy_MiddlewareRules');

        // Email List Integrations
        $this->bind('email_list_integration', 'Zippy_EmailListIntegration');
        $this->bind('email_list_integration_repository', 'Zippy_EmailListIntegrationRepository', true);
        
        $this->bind('email_list', 'Zippy_EmailList');
        $this->bind('email_list_repository', 'Zippy_EmailListRepository');

        $this->bind('email_list_integrations', 'ZippyCourses_EmailListIntegrations');
        
        // Cart
        $this->bind('product', 'Zippy_Product');
        $this->bind('products', 'ZippyCourses_Products');
        $this->bind('order', 'Zippy_Order');
        $this->bind('orders', 'Zippy_Orders');
        $this->bind('currencies', 'Zippy_Currencies');
        $this->bind('transaction', 'Zippy_Transaction');
        $this->bind('transactions', 'Zippy_Transactions');
        $this->bind('transaction_processor', 'Zippy_TransactionProcessor');

        // Payment Gateway
        $this->bind('payment_gateway_ipn', 'Zippy_PaymentGatewayIPN');
        $this->bind('payment_gateway_integrations', 'Zippy_PaymentGatewayIntegrations', true);
        $this->bind('payment', 'Zippy_PaymentGateways', true);
        $this->bind('payment_gateway', 'Zippy_PaymentGateway');
        $this->bind('payment_gateway_listener', 'Zippy_PaymentGateway_Listener');

        // Models, Repositories & Dependencies
        $this->bind('course', 'Zippy_Course');
            $this->bind('course_config', 'Zippy_CourseConfig');
                $this->bind('course_scheduling_config', 'Zippy_CourseSchedulingConfig');
                    $this->bind('course_access_config', 'Zippy_CourseAccessConfig');
            $this->bind('certificate', 'Zippy_CourseCertificate');

        $this->bind('courses', 'Zippy_Courses');
        $this->bind('entry', 'Zippy_Entry');
        $this->bind('entries', 'Zippy_Entries');
            
        $this->bind('quiz', 'Zippy_Quiz');
        $this->bind('forum', 'Zippy_Forum');

        $this->bind('sessions', 'Zippy_Sessions', true);
        $this->bind('alerts', 'ZippyCourses_Alerts', true);
        $this->bind('admin_notice_repository', 'Zippy_AdminNotices', true);
        $this->bind('admin_notice', 'Zippy_AdminNotice');
        $this->bind('admin_notices', 'ZippyCourses_AdminNotices');

        // Admin Tables
        
        $this->bind('students_table', 'ZippyCourses_Students_ListTable');
        $this->bind('list_table_column', 'Zippy_ListTableColumn');
        $this->bind('list_table_columns', 'Zippy_ListTableColumns');

        $this->bind('list_table_filter', 'Zippy_ListTableFilter');
        $this->bind('list_table_filters', 'Zippy_ListTableFilters');
    
        // RSS
        $this->bind('rss', 'ZippyCourses_RSS');

        // Licensing
        $this->bind('license', 'ZippyCourses_License');

        // AJAX
        $this->bind('ajax', 'ZippyCourses_Ajax', true);

        // API
        $this->bind('api', 'Zippy_API', true);

        // Migration
        $this->bind('migration', 'Zippy_DatabaseMigration');
        $this->bind('migration_step', 'Zippy_MigrationStep');
        $this->bind('migration_steps', 'Zippy_MigrationSteps');
        $this->bind('migration_v1_0_0', 'ZippyCourses_v1_0_0_DatabaseMigration');
    
        // Customizer
        $this->bind('customizer', 'Zippy_Customizer');

        $this->bind('customizer_panel', 'Zippy_Customizer_Panel');
        $this->bind('customizer_panels', 'Zippy_Customizer_Panels');
        
        $this->bind('customizer_section', 'Zippy_Customizer_Section');
        $this->bind('customizer_sections', 'Zippy_Customizer_Sections');

        $this->bind('customizer_control', 'Zippy_Customizer_Control');
        $this->bind('customizer_controls', 'Zippy_Customizer_Controls');
        
        // Hook for further registrations
        do_action('zippy_register_ioc_bindings', $this);
    }
}
