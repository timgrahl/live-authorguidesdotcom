<?php

class Zippy_Shortcode extends Zippy_Model
{
    public $id;
    public $view;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function register()
    {
        add_shortcode($this->id, array($this->view, 'render'));
    }

    /**
     * Gets the value of view.
     *
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Sets the value of view.
     *
     * @param mixed $view the view
     *
     * @return self
     */
    public function setView(Zippy_ShortcodeView $view)
    {
        $view->setShortcode($this);

        $this->view = $view;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }
}
