<?php

abstract class Zippy_ShortcodeView extends Zippy_Model
{
    public $shortcode;
    
    public function __construct($id)
    {
        $this->id = $id;
    }

    abstract public function render($atts, $content = '');

    /**
     * Gets the value of shortcode.
     *
     * @return mixed
     */
    public function getShortcode()
    {
        return $this->shortcode;
    }

    /**
     * Sets the value of shortcode.
     *
     * @param mixed $shortcode the shortcode
     *
     * @return self
     */
    public function setShortcode($shortcode)
    {
        $this->shortcode = $shortcode;
        return $this;
    }

    public function parseAttributes($atts)
    {
        return shortcode_atts(
            array(
            ),
            $atts,
            $this->shortcode->getId()
        );
    }

    public function show()
    {
        echo $this->render();
    }

    public function getId()
    {
        return $this->id;
    }
}
