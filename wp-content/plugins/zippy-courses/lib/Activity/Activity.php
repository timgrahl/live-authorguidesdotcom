<?php

class Zippy_Activity
{
    protected static $instance;
    public $table;

    const TABLE_PREFIX = 'zippy_';

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        global $wpdb;

        $this->table    = $wpdb->prefix . self::TABLE_PREFIX . 'user_activity';
    }

    public function insert($user_id, $item_id, $type, $duration = 0)
    {
        global $wpdb;

        $wpdb->insert(
            $this->table,
            array(
                'user_id'   => $user_id,
                'item_id'   => $item_id,
                'type'      => $type,
                'duration'  => $duration
            ),
            array('%d','%s','%s','%d')
        );

        return $wpdb->insert_id;
    }

    public function update($activity_id, $data)
    {
        global $wpdb;

        $wpdb->update(
            $this->table,
            $data,
            array('ID' => $activity_id)
        );

        return $wpdb->insert_id;
    }
}
