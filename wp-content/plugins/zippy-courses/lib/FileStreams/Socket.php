<?php

class Zippy_Socket_FileStream extends Zippy_FileStream
{
    public function fetch()
    {
        $retbytes = true;

        $buffer = "";
        $cnt =0;
        $chunk_size = 1024 * 1024;

        $handle = fopen($this->url, "rb");

        if ($handle === false) {
            return false;
        }

        while (!feof($handle)) {
            $buffer = fread($handle, $chunk_size);
            
            echo $buffer;

            ob_flush();
            flush();
            
            if ($retbytes) {
                $cnt += strlen($buffer);
            }
        }

        $status = fclose($handle);

        if ($retbytes && $status) {
            return $cnt; // return num. bytes delivered like readfile() does.
        }

        return $status;
    }
}
