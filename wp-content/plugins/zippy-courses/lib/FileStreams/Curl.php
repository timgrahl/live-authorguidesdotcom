<?php

class Zippy_Curl_FileStream extends Zippy_FileStream
{
    public function fetch()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_BUFFERSIZE, 2048);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_WRITEFUNCTION, array($this, '_progress'));

        curl_exec($ch);

        if (curl_exec($ch) === false) {
        }

        curl_close($ch);
    }

    public function _progress($ch, $str)
    {
        echo $str;

        // For better memory usage
        ob_flush();
        flush();

        // Return length of data; -1 letse CURL know we're done.
        return strlen($str) ? strlen($str) : -1;
    }
}
