<?php

abstract class Zippy_FileStream
{
    public $url;

    protected $headers;

    public function __construct($url)
    {
        $this->url = $url;

        $this->_defaultHeaders();
    }

    abstract public function fetch();

    public function download()
    {
        $this->_setHeaders();
        $this->fetch();
        exit;
    }

    private function _getFilesize()
    {

        if (ini_get('allow_url_fopen')) {
            $headers = get_headers($this->url, true);
        } else {
            $headers = wp_get_http_headers($this->url);
        }
        
        $headers = is_array($headers) ? array_change_key_case($headers) : $headers;

        $filesize = isset($headers['content-length']) ? $headers['content-length'] : false;

        if ($filesize) {
            if (is_array($filesize)) {
                $filesize = end($filesize);
            }
        }
        return $filesize;
    }

    protected function _getFilename()
    {
        $basename = basename($this->url);

        if (strpos($basename, '?') !== false) {
            $pos = strpos($basename, '?');
            $basename = substr($basename, 0, $pos);
        }

        return $basename;
    }

    protected function _defaultHeaders()
    {
        $this->headers = array(
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename=' . $this->_getFileName(),
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public'
        );

        if (($filesize = $this->_getFilesize()) !== false) {
            $this->headers['Content-Length'] = $filesize;
        }
    }

    protected function _setHeaders()
    {
        foreach ($this->headers as $key => $value) {
            header("{$key}: {$value}");
        }
    }
}
