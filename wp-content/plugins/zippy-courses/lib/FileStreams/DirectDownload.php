<?php

class Zippy_DirectDownload_FileStream extends Zippy_FileStream
{
    public function fetch()
    {
        wp_redirect($this->url);
        exit();
    }
}
