<?php

class Zippy_Dispatcher
{
    private $events;
    private $listeners;

    public function fire(Zippy_Event $event)
    {
        $event_name = $event->getEventName();

        if (isset($this->listeners[$event_name])) {
            foreach ($this->listeners[$event_name] as $listener) {
                call_user_func_array(array($listener, 'handle'), array($event));
            }
        }

        do_action('zippy_event_' . $event->getSnakeName(), $event);
    }

    public function register($event, Zippy_Listener $listener)
    {
        if (!isset($this->listeners[$event])) {
            $this->listeners[$event] = array();
        }

        $this->listeners[$event][] = $listener;
    }
}
