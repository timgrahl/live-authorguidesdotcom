<?php

abstract class Zippy_Listener implements Zippy_RepositoryObject
{
    public $id;
    public $event;
    
    abstract public function handle(Zippy_Event $event);
    abstract protected function register();

    public function getId()
    {
        return $this->id;
    }
}
