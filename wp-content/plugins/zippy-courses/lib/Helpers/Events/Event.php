<?php

abstract class Zippy_Event
{
    public function getEventName()
    {
        $r = new ReflectionClass($this);
        return str_replace(array('Zippy_', 'ZippyCourses_', '_Event'), '', $r->getName());
    }

    public function getSnakeName()
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $this->getEventName(), $matches);
        
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = strtolower($match);
        }
        
        return implode('_', $ret);
    }
    public function hasProduct()
    {
        return (isset($this->order) && isset($this->order->product));
    }
}
