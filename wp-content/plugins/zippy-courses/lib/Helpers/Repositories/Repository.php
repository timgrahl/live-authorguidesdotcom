<?php

class Zippy_Repository
{
    public $items = array();
    public $valid_types = array();

    public function add(Zippy_RepositoryObject $item)
    {
        if ($this->validateItemType($item)) {
            $this->items[$item->id] = $item;
        }
        
        return $this;
    }

    public function getIndex($id)
    {
        if (!is_numeric($id)) {
            $keys = array_keys($this->all());
            $index = array_search($id, $keys);

            // If array_search does not find a key, then simply add it to the end of the array
            $index = $index !== false ? $index : count($keys);
        } else {
            $index = $id;
        }

        return $index ? $index : null;
    }

    public function insertBefore(Zippy_RepositoryObject $item, $id = '')
    {
        if ($this->get($id) !== null) {
            $index = $this->getIndex($id);
            $this->insert($item, $index);
            return $this;
        }

        return $this->add($item);
    }

    public function insertAfter(Zippy_RepositoryObject $item, $id = '')
    {
        if ($this->get($id) !== null) {
            $index = $this->getIndex($id) + 1;
            $this->insert($item, $index);
            return $this;
        }

        return $this->add($item);
    }

    public function insert(Zippy_RepositoryObject $item, $index)
    {
        $before = array_slice($this->all(), 0, $index);
        $after  = array_slice($this->all(), $index);

        $items = array_merge($before, array($item->getId() => $item), $after);
        
        $this->truncate()->addItems($items);

        return $this;
    }

    public function remove($id)
    {
        if (isset($this->items[$id])) {
            unset($this->items[$id]);
        }

        return $this;
    }

    public function addItems(array $items)
    {
        foreach ($items as $item) {
            $this->add($item);
        }
    }

    public function get($id = '')
    {
        return isset($this->items[$id]) ? $this->items[$id] : null;
    }

    public function sortBy($key, $desc = true)
    {
        $output = clone($this);
        $output->items = array();

        // Make sure we have the key, and if not, return the current repository
        $first = $this->first();
        if (!property_exists(get_class($first), $key)) {
            return $this->all();
        }

        /**
         * @todo Update how Type is determined
         */

        $date_fields = array('joined', '_last_active', 'date');
        $type = !in_array(strtolower($key), $date_fields) ? 'string' : 'datetime';

        switch ($type) {
            case 'datetime':
                $this->items = $this->sortByDatetime($key);
                break;
            default:
                $this->items = $this->sortByString($key);
                break;
        }

        if ($desc) {
            $this->reverse();
        }

        return $this->items;
    }

    private function sortByString($key)
    {
        global $zippy_repository_sort_key;

        $zippy_repository_sort_key = $key;
        
        $input = $this->all();
        usort($input, array($this, 'sorter'));

        $zippy_repository_sort_key = null;

        return $input;
    }

    private function sortByDateTime($key)
    {
        global $zippy_repository_sort_key;

        $zippy_repository_sort_key = $key;
        
        $input = $this->all();
        usort($input, array($this, 'datetimeSorter'));

        $zippy_repository_sort_key = null;

        return $input;
    }

    private function sorter($a, $b)
    {
        global $zippy_repository_sort_key;

        if ($a->{$zippy_repository_sort_key} == $b->{$zippy_repository_sort_key}) {
            return 0;
        }
        
        return ($a->{$zippy_repository_sort_key} < $b->{$zippy_repository_sort_key}) ? -1 : 1;
    }

    private function datetimeSorter($a, $b)
    {
        global $zippy_repository_sort_key;

        if ($a->{$zippy_repository_sort_key}->format('U') == $b->{$zippy_repository_sort_key}->format('U')) {
            return 0;
        }
        
        return ($a->{$zippy_repository_sort_key}->format('U') < $b->{$zippy_repository_sort_key}->format('U')) ? -1 : 1;
    }

    public function getBy($key, $value)
    {
        $output = null;

        foreach ($this->items as $item) {
            if ($output !== null) {
                break;
            }

            if (isset($item->{$key}) && $item->{$key} == $value) {
                $output = $item;
            }
        }

        return $output;
    }

    public function all()
    {
        return $this->items;
    }

    public function count()
    {
        return count($this->items);
    }

    private function validateItemType(Zippy_RepositoryObject $item)
    {
        $is_valid = false;

        foreach ($this->valid_types as $type) {
            if ($item instanceof $type) {
                $is_valid = true;
                break;
            }
        }

        return $is_valid;
    }

    public function first()
    {
        return count($this->items) ? reset($this->items) : null;
    }

    public function where($key, $value, $operator = '==')
    {
        $output = array();

        foreach ($this->items as $item) {
            switch ($operator) {
                case '!=':
                    if ($item->{$key} != $value) {
                        $output[] = $item;
                    }
                    break;
                case '=':
                case '==':
                default:
                    if ($item->{$key} == $value) {
                        $output[] = $item;
                    }
                    break;
            }
        }

        return $output;
    }

    public function where2($key, $value, $operator = '==')
    {
        $output = clone($this);
        $output->items = array();

        foreach ($this->items as $item) {
            switch ($operator) {
                case '!=':
                    if ($item->{$key} != $value) {
                        $output->add($item);
                    }
                    break;
                case '=':
                case '==':
                default:
                    if ($item->{$key} == $value) {
                        $output->add($item);
                    }
                    break;
            }
        }

        return $output;
    }

    public function last()
    {
        return end($this->items);
    }

    public function has($key)
    {
        return isset($this->items[$key]);
    }

    public function toArray($prop = null)
    {
        $output = array();

        foreach ($this->items as $item) {
            $output[$item->id] = $prop !== null ? $item->{$prop} : $item;
        }

        return $output;
    }

    public function toJSONList($prop = null)
    {
        $data   = $this->toArray($prop);

        $item = new stdClass;
            $item->value = '';
            $item->text  = '';

        $output = array($item);
        foreach ($data as $key => $value) {
            $item = new stdClass;
            $item->value = $key;
            $item->text  = $value;

            $output[] = $item;
        }

        return $output;
    }

    public function keys()
    {
        return array_keys($this->items);
    }

    public function fetchByMeta($key, $value)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $items = $wpdb->get_col($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", $key, $value));
        $type = $this->valid_types[0];

        foreach ($items as $post_id) {
            $new_item = $zippy->make($type);
            $new_item->build($post_id);
            
            $this->add($new_item);
        }

        return $this;
    }

    public function fetchPostsById(array $post_ids)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $type = $this->valid_types[0];

        foreach ($post_ids as $post_id) {
            $new_item = $zippy->make($type, array('id' => $post_id));
            $this->add($new_item);
        }

        return $this;
    }

    public function addValidType($type)
    {
        if (!in_array($type, $this->valid_types)) {
            $this->valid_types[] = $type;
        }

        return $this;
    }

    public function truncate()
    {
        $this->items = array();

        return $this;
    }

    public function segment($length, $offset, $type = 'array')
    {
        if ($type == 'array') {
            return array_slice(array_values($this->all()), $offset, $length);
        }
        if ($type == 'object') {
            $items_loaded = 0;
            $items_offset = 0;
            foreach ($this->all() as $item) {
                if ($items_offset < $offset) {
                    $this->remove($item->getId());
                    $items_offset++;
                    continue;
                }

                if ($items_loaded < $length) {
                    $items_loaded++;
                } else {
                    $this->remove($item->getId());
                }
            }
            return $this;
        }
    }

    public function getNextId()
    {
        $ids = $this->toArray('id');

        $next_id = 0;

        foreach ($ids as $id) {
            if ($id >= $next_id) {
                $next_id = $id + 1;
            }
        }

        return $next_id;
    }

    public function reverse()
    {
        $this->items = array_reverse($this->items);
        return $this;
    }
}
