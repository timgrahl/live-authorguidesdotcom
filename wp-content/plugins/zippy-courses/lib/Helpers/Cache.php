<?php

class Zippy_Cache
{
    public $data = array();

    public function get($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    public function set($key, $value)
    {
        $this->data[$key] = $value;
        return $this;
    }

    public function exists($key)
    {
        return isset($this->data[$key]);
    }
}
