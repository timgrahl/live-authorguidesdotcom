<?php

class Zippy_Quiz_Utilities
{
    public function getAllQuizIds()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $query_id = 'zippy_get_all_quiz_ids';

        if (($query = $zippy->queries->get($query_id)) === null) {
            global $wpdb;

            $query = $zippy->make('query', array('id' => $query_id));
            $query->setSql("SELECT ID FROM $wpdb->posts WHERE post_type = 'quiz'");

            $zippy->queries->add($query);
        }

        return $query->getCol();
    }

    public function getCourseEntryIds($quiz_id)
    {
        $zippy = Zippy::instance();

        $quiz_course_entry_ids = $zippy->cache->get('quiz_course_entry_ids');
        $quiz_course_entry_ids = $quiz_course_entry_ids !== null ? $quiz_course_entry_ids : array();

        if (isset($quiz_course_entry_ids[$quiz_id])) {
            return $quiz_course_entry_ids[$quiz_id];
        }

        $course_ids = $zippy->utilities->course->getAllCourseIds();
        $entry_ids = array();

        foreach ($course_ids as $course_id) {
            $entries = array_filter((array) $zippy->utilities->getJsonMeta($course_id, 'entries', true));

            foreach ($entries as $entry) {
                if (isset($entry->quiz->ID) && $quiz_id == $entry->quiz->ID) {
                    $entry_ids[] = $entry->ID;
                }

                foreach ($entry->entries as $subentry) {
                    if (isset($subentry->quiz->ID) && $quiz_id == $subentry->quiz->ID) {
                        $entry_ids[] = $subentry->ID;
                    }
                }
            }
        }

        $quiz_course_entry_ids[$quiz_id] = $entry_ids;

        $zippy->cache->set('quiz_course_entry_ids', $quiz_course_entry_ids);

        return $entry_ids;
    }

    public function getAllResultsForQuiz($quiz_id)
    {
        $zippy = Zippy::instance();

        $query_id = "zippy_get_all_quiz_{$quiz_id}_results";

        if (($query = $zippy->queries->get($query_id)) === null) {
            global $wpdb;

            $query = $zippy->make('query', array('id' => $query_id));
            $query->setSql(
                $wpdb->prepare(
                    "SELECT
                        meta_value
                     FROM
                        $wpdb->usermeta
                     WHERE 
                        meta_key = %s AND
                        (
                            meta_value LIKE %s OR
                            meta_value LIKE %s
                        )
                    ",
                    'quiz_results',
                    '{"quiz":"' . $quiz_id . '"%',
                    '{"quiz":' . $quiz_id . '%'
                )
            );

            $zippy->queries->add($query);
        }

        $results = $query->getCol();

        foreach ($results as &$result) {
            $result = json_decode($result);
        }

        return $results;
    }

    public function getAverageScore($quiz_id)
    {
        $results = $this->getAllResultsForQuiz($quiz_id);

        $score = 0;

        if (count($results)) {
            foreach ($results as $result) {
                $score += $result->score;
            }

            $score = ($score / count($results));
        }

        return $score;
    }

    public function getResult($result_id)
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT meta_value FROM $wpdb->usermeta WHERE umeta_id = %d", $result_id);

        $result = $wpdb->get_var($sql);
        return $result ? json_decode($result) : null;
    }

    public function getQuestions($quiz_id)
    {
        $zippy = Zippy::instance();

        $questions = array_filter((array) $zippy->utilities->getJsonMeta($quiz_id, 'questions', true));

        return $questions;
    }
}
