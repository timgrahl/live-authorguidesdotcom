<?php

class Zippy_Cart_Utilities
{
    public function getCurrency()
    {
        $settings = get_option('zippy_payment_general_settings');

        if (!isset($settings['currency'])) {
            return 'USD';
        }

        return $settings['currency'];
    }

    public function getOrderStatusList()
    {
        return array(
            '' => '',
            'pending' => __('Pending', ZippyCourses::TEXTDOMAIN),
            'complete' => __('Completed', ZippyCourses::TEXTDOMAIN),
            'refund' => __('Refunded', ZippyCourses::TEXTDOMAIN),
            'active' => __('Active', ZippyCourses::TEXTDOMAIN),
            'cancel' => __('Cancelled', ZippyCourses::TEXTDOMAIN),
            'revoke' => __('Revoked', ZippyCourses::TEXTDOMAIN)
        );
    }

    public function getActivePaymentMethod()
    {
        $settings = get_option('zippy_payment_general_settings');

        return isset($settings['method']) ? $settings['method'] : '';
    }

    public function getTransactionKey()
    {
        global $zippy_transaction_key;

        $zippy = Zippy::instance();
        
        if (empty($zippy_transaction_key)) {
            if ($zippy->cache->get('zippy_transaction_key') !== null) {
                $zippy_transaction_key = $zippy->cache->get('zippy_transaction_key');
            }

            if ($zippy->sessions->retrieve('zippy_transaction_key') !== null) {
                $zippy_transaction_key = $zippy->sessions->retrieve('zippy_transaction_key');
            }
        }

        return apply_filters('zippy_filter_get_transaction_key', $zippy_transaction_key);
    }

    public function getLoginClaimUrl()
    {
        $zippy = Zippy::instance();
        
        $transaction_key = $this->getTransactionKey();

        return $zippy->core_pages->getUrl('login') . '?claim=' . $transaction_key;
    }

    public function getRegistrationClaimUrl()
    {
        $zippy = Zippy::instance();

        $transaction_key = $this->getTransactionKey();

        return $zippy->core_pages->getUrl('register') . '?claim=' . $transaction_key;
    }

}
