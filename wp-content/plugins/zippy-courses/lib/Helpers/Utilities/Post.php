<?php

class Zippy_Post_Utilities
{
    /**
     * Duplicate a post
     *
     * @since  1.0.0
     *
     * @param  int  $post_id
     * @param  bool $with_meta
     *
     * @return int|bool
     */
    public function duplicate($post_id, $with_meta = true)
    {
        $ignore = array(
            'ID','post_date', 'post_date_gmt', 'to_ping', 'pinged', 'post_modified', 'post_modified_gmt',
            'post_content_filtered', 'guid', 'menu_order', 'comment_count'
        );

        $post = get_post($post_id);

        foreach ($ignore as $field) {
            unset($post->{$field});
        }

        $new_post_id = wp_insert_post($post, true);

        if (!is_wp_error($new_post_id)) {
            if ($with_meta) {
                $this->copyMeta($post_id, $new_post_id);
            }

            return $new_post_id;
        }
        
        return false;
    }

    /**
     * Copy meta from one post to another
     *
     * @since  1.0.0
     *
     * @param  int  $old_post_id
     * @param  int  $new_post_id
     *
     * @return void
     */
    public function copyMeta($old_post_id, $new_post_id)
    {
        $ignore = array('_edit_last', '_edit_lock');

        $meta = get_post_meta($old_post_id);

        foreach ($ignore as $field) {
            if (isset($meta[$field])) {
                unset($meta[$field]);
            }
        }

        foreach ($meta as $meta_key => $values) {
            $unique = count($values) < 2;
            foreach ($values as $meta_value) {
                add_post_meta($new_post_id, $meta_key, $meta_value, $unique);
            }
        }
    }

    public function duplicatePost($old_post_id)
    {
        $new_entry_args = get_post($old_post_id);
        unset($new_entry_args->ID);
        unset($new_entry_args->post_name);

        return wp_insert_post($new_entry_args, true);
    }

    public function duplicateMeta($old_post_id, $new_post_id)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $meta = get_post_meta($old_post_id);

        foreach ($meta as $meta_key => $values) {
            switch ($meta_key) {
                case 'entries':
                    $entries = $zippy->utilities->maybeJsonDecode($values[0]);

                    foreach ($entries as $key => $entry) {
                        if (!is_wp_error(($new_entry_id = $this->duplicatePost($entry->ID)))) {
                            $this->duplicateMeta($entry->ID, $new_entry_id);
                            $entry->ID = (int) $new_entry_id;
                            $entries[$key] = $entry;
                        }

                        foreach ($entry->entries as $k => $subentry) {
                            if (!is_wp_error(($new_subentry_id = $this->duplicatePost($subentry->ID)))) {
                                $this->duplicateMeta($subentry->ID, $new_subentry_id);

                                $subentry->ID = (int) $new_subentry_id;
                                $entries[$key]->entries[$k] = $subentry;
                            }
                        }
                    }

                    $wpdb->insert(
                        $wpdb->postmeta,
                        array(
                            'post_id' => $new_post_id,
                            'meta_key' => $meta_key,
                            'meta_value' => json_encode($entries),
                        )
                    );

                    break;
                case 'downloads':
                    $downloads = $zippy->utilities->maybeJsonDecode($values[0]);
                    if (empty($downloads) || (!is_array($downloads) && !is_object($downloads))) {
                        break;
                    }

                    foreach ($downloads as $download) {
                        if (empty($download)) {
                            continue;
                        }

                        $download->uid = $zippy->utilities->entry->generateFileUid();
                    }
                    $wpdb->insert(
                        $wpdb->postmeta,
                        array(
                            'post_id' => $new_post_id,
                            'meta_key' => $meta_key,
                            'meta_value' => json_encode($downloads)
                        )
                    );
                    break;
                default:
                    $unique = count($values) == 1;
                    foreach ($values as $value) {
                        $wpdb->insert(
                            $wpdb->postmeta,
                            array(
                                'post_id' => $new_post_id,
                                'meta_key' => $meta_key,
                                'meta_value' => $value,
                            )
                        );
                    }
                    break;
            }
        }
    }

    public function getPublicFeaturedImageId($post_id)
    {
        $zippy = Zippy::instance();
        
        $public_featured_media = $zippy->utilities->getJsonMeta($post_id, 'public_featured_media', true);
        $featured_media = $zippy->utilities->getJsonMeta($post_id, 'featured_media', true);

        $image = isset($public_featured_media->image) ? $public_featured_media->image : false;

        if (!$image || $image->ID == 0) {
            $image = isset($featured_media->image) ? $featured_media->image : false;
        }

        return isset($image->ID) ? $image->ID : 0;
    }

    public function getPublicFeaturedMedia($post_id)
    {
        $zippy = Zippy::instance();
        
        $public_featured_media = $zippy->utilities->getJsonMeta($post_id, 'public_featured_media', true);
        $featured_media = $zippy->utilities->getJsonMeta($post_id, 'featured_media', true);

        $image = isset($public_featured_media->image) ? $public_featured_media->image : false;
        $video = isset($public_featured_media->video) ? $public_featured_media->video : false;

        if ((!$image || $image->ID == 0) && !$video) {
            $image = isset($featured_media->image) ? $featured_media->image : false;
            $video = isset($featured_media->video) ? $featured_media->video : false;
        }

        if ((!$image || $image->ID == 0) && !$video) {
            return false;
        }

        return $video ? $video : $image;
    }

    public function getFeaturedMedia($post_id)
    {
        $zippy = Zippy::instance();
        
        $featured_media = $zippy->utilities->getJsonMeta($post_id, 'featured_media', true);

        $image = isset($featured_media->image) ? $featured_media->image : false;
        $video = isset($featured_media->video) ? $featured_media->video : false;

        if ((!$image || $image->ID == 0) && !$video) {
            return false;
        }

        return $video
            ? $video
            : ($image
                ? $image->ID
                : false
            );
    }

    public function getPostId()
    {
        global $post;
        $post_id = null;
        if (is_front_page()) {
            $post_id = get_option('page_on_front');
        } elseif (is_home()) {
            $post_id = get_option('page_for_posts');
        } elseif (is_404()) {
            $post_id = 0;
        } else {
            if (is_object($post)) {
                $post_id = $post->ID;
            }
        }

        return $post_id;
    }

    public function hasFeaturedMedia($post_id)
    {
        return (bool) $this->getFeaturedMedia($post_id);
    }

    public function hasPublicFeaturedMedia($post_id)
    {
        return (bool) $this->getPublicFeaturedMedia($post_id);
    }

    public function getDefaultThankYouText()
    {
        $content =      __('Thank you for your purchase!', ZippyCourses::TEXTDOMAIN) . "\n\n" .
                        __('Please proceed to register your account. We have sent you an email with a link to register as well. If you run into any difficulties, please contact [zippy_contact_email_link] and we will help resolve any issues.', ZippyCourses::TEXTDOMAIN) . "\n\n" .
                        "[zippy_button type=\"register\" style=\"primary\"] \n\n" .
                        __('Or if you are already a member, please login to claim your account:', ZippyCourses::TEXTDOMAIN) . " \n\n" .
                        "[zippy_button type=\"register-login\"] \n\n";

        return $content;
    }
}
