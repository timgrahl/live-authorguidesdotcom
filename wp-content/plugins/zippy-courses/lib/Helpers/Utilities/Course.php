<?php

class Zippy_Course_Utilities
{
    public function getTierTitle(Zippy_Course $course, $tier_id)
    {
        $zippy = Zippy::instance();

        $tiers = array_filter((array) $zippy->utilities->getJsonMeta($course->getId(), 'tiers', true));

        $title = '';

        foreach ($tiers as $tier) {
            if ($tier->ID == $tier_id) {
                $title = $tier->title;
            }
        }

        return $title;
    }

    public function getCourseProduct(ZippyCourses_Student $student, Zippy_Course $course)
    {
        $zippy = Zippy::instance();

        $course_product_id  = null;
        $products           = $student->getProductsOwned();
        $product_course_ids = array();

        foreach ($products as $product_id) {
            $course_ids = $zippy->utilities->product->getCourses($product_id);

            if (in_array($course->getId(), $course_ids)) {
                $course_product_id = $product_id;
            }

            if ($course_product_id) {
                break;
            }
        }

        return $course_product_id ? $zippy->make('product', array($course_product_id)) : $course_product_id;
    }

    public function getAllCourseProducts(ZippyCourses_Student $student, Zippy_Course $course)
    {
        $zippy = Zippy::instance();

        $course_product_id  = null;
        $products           = $student->getProductsOwned();
        $course_products = array();

        foreach ($products as $product_id) {
            $course_ids = $zippy->utilities->product->getCourses($product_id);

            if (in_array($course->getId(), $course_ids)) {
                $course_products[] = $zippy->make('product', array($product_id));
            }
        }

        return $course_products;
    }

    public function getExpirationDate(ZippyCourses_Student $student, Zippy_Course $course)
    {
        $zippy = Zippy::instance();

        $student->fetch();

        $orders           = $student->getOrders();
        $start_dates      = $student->getCourseStartDates();
        $products         = $this->getProductIdsForCourse($course->getId());
        $date             = null;

        $scheduling_type  = $course->getConfig()->scheduling->getType();
        $expiration_type  = $course->getConfig()->scheduling->getEndType();
        if ($expiration_type =='duration' && isset($start_dates[$course->getId()])) {
            $expiration_date = clone($start_dates[$course->getId()]);
            $expiration_duration    = $course->getConfig()->scheduling->getExpirationDuration();

            $expiration_date->modify("+{$expiration_duration} days");

            $date = $expiration_date;
        } else {
            foreach ($orders->all() as $order) {
                $product = $order->getProduct();
                if ($product !== null && in_array($product->getId(), $products)) {
                    $expiration_date = $order->getExpirationDate($course->getId());

                    // This value can return null if one order for a course has an expiration date
                    // But a second order does not (such as if someone purchased two products for the same course)
                    if (!$expiration_date) {
                        return null;
                    }

                    $date = $date instanceof DateTime
                        ? ( $date->format('U') < $expiration_date->format('U')
                            ? $expiration_date
                            : $date
                        )
                        : $expiration_date;
                }
            }
        }

        return $date;
    }

    public function getAllCoursesAndTiers()
    {
        global $wpdb;
            
        $zippy = Zippy::instance();

        if (($courses = $zippy->cache->get("zippy_all_courses_and_tiers")) !== null) {
            return $courses;
        }

        $invalid_status = $zippy->utilities->getInvalidPostStatusList();
        $raw_course_ids = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE post_type = 'course' AND post_status NOT IN ($invalid_status)");
        $courses = array();

        foreach ($raw_course_ids as $course_id) {
            $course = $zippy->make('course', array($course_id));
            $course->fetch();

            $courses[$course_id] = array(
                'id' => $course_id,
                'title' => $course->getTitle(),
                'tiers' => array()
            );

            foreach ($course->getTiers() as $tier) {
                $courses[$course_id]['tiers'][$tier->ID] = array(
                    'id' => $tier->ID,
                    'title' => $tier->title
                );
            }

            $courses[$course_id]['tiers'] = array_values($courses[$course_id]['tiers']);
        }

        return array_values($courses);
    }

    public function getAllCourseIds()
    {
        global $wpdb;
            
        $zippy = Zippy::instance();

        $query_id = 'zippy_get_all_course_ids';

        if (($query = $zippy->queries->get($query_id)) === null) {
            global $wpdb;

            $invalid_post_statuses = array('"auto-draft"','"trash"', '"inherit"');
            $invalid_post_status_list = implode(', ', $invalid_post_statuses);

            $query = $zippy->make('query', array('id' => $query_id));
            $query->setSql("SELECT ID FROM $wpdb->posts WHERE post_type = 'course' AND post_title != 'Auto Draft' AND post_status NOT IN ($invalid_post_status_list)");

            $zippy->queries->add($query);
        }

        return $query->getCol();
    }

    public function getCourseEntryIds($course_id)
    {
        global $wpdb;
            
        $zippy = Zippy::instance();
        
        $all_course_entry_ids = $zippy->utilities->entry->getAllEntryIds(false);
               
        return isset($all_course_entry_ids[$course_id]) ? $all_course_entry_ids[$course_id] : array();
    }

    public function getProductIdsForCourse($course_id)
    {
        $zippy = Zippy::instance();

        $course_product_ids = array();

        $product_ids = $zippy->utilities->product->getAllProductIds();

        foreach ($product_ids as $product_id) {
            $access = $zippy->utilities->getJsonMeta($product_id, 'access', true);

            if (is_object($access) && $access->mode == 'bundle') {
                foreach ($access->bundle as $course) {
                    if ($course->id && $course->id == $course_id && !empty($course->tiers)) {
                        $course_product_ids[] = $product_id;
                    }
                }
            } else {
                if (is_object($access) && $access->course && $access->course == $course_id && $access->tier !== '') {
                    $course_product_ids[] = $product_id;
                }
            }
        }

        return array_unique($course_product_ids);
    }

    public function getCount()
    {
        return count($this->getAllCourseIds());
    }

    public function getCertificateUrl($course_id)
    {
        $link = get_permalink($course_id);
        $link = $link . 'certificate';
        return $link;
    }
}
