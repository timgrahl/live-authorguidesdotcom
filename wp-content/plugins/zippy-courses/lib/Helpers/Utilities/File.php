<?php

class Zippy_File_Utilities
{
    public function getClassFiles($file_type, $data_key, $default = '')
    {
        /**
         * Filter: zippy_{$type}_file_directories
         *
         * Gets a list of all directories for this type, for both Zippy Courses and Zippy Courses extensions
         *
         * @since   1.0.0
         *
         * @param   array    $directories   A list of source directories
         */
        $directories = empty($default) ? array() : array($default);
        $directories = apply_filters("zippy_{$file_type}_directories", $directories);

        /**
         * Filter: zippy_{$type}_files
         *
         * Gets a list of all files for this type from directories, for both Zippy Courses and Zippy Courses extensions
         *
         * @since   1.0.0
         *
         * @param   array    $files   A list of all middleware files, before they are validated
         */
        $files = apply_filters("zippy_{$file_type}_files", $this->getDirectoryFiles($directories));
        
        foreach ($files as $key => $file) {
            $terms = array('id' => 'Id', 'class' => 'Class', 'file_type' => 'File Type');
            $data = get_file_data($file, $terms);

            if (!isset($data['file_type']) || $data['file_type'] !== $file_type) {
                unset($files[$key]);
            }
        }

        return array_values($files);
    }

    public function getIntegrationFiles($file_type, $data_key, $default = '')
    {
        /**
         * Filter: zippy_{$type}_file_directories
         *
         * Gets a list of all directories for this type, for both Zippy Courses and Zippy Courses extensions
         *
         * @since   1.0.0
         *
         * @param   array    $directories   A list of source directories
         */
        $directories = empty($default) ? array() : array($default);
        $directories = apply_filters("zippy_{$file_type}_directories", $directories);

        $files = array();

        foreach ($directories as $directory) {
            $directory_files = array_diff(scandir($directory), array('..', '.'));

            foreach ($directory_files as $f) {
                if (is_dir($directory . $f)) {
                    $filepath = $directory . $f . '/integration.php';

                    if (is_readable($filepath)) {
                        $files[] = $filepath;
                    }
                }
            }
        }

        foreach ($files as $key => $file) {
            $terms = array('id' => 'Id', 'class' => 'Class', 'file_type' => 'File Type');
            $data = get_file_data($file, $terms);


            if (!isset($data['file_type']) || $data['file_type'] !== $file_type) {
                unset($files[$key]);
            }
        }

        return array_values($files);
    }

    public function getDirectoryFiles($directory)
    {
        $files = array();

        if (!is_array($directory)) {
            if (is_dir($directory)) {
                return $this->_getDirectoryFiles($directory);
            } else {
                return array();
            }
        } else {
            foreach ($directory as $dir) {
                if (!is_dir($dir)) {
                    continue;
                }
                
                $dir_files = $this->_getDirectoryFiles($dir);

                $files = array_merge($files, $dir_files);
            }
        }

        $files = array_values(array_unique($files));

        return $files;
    }

    private function _getDirectoryFiles($directory_path)
    {
        $files = array();

        $Directory  = new RecursiveDirectoryIterator($directory_path);
        $Iterator   = new RecursiveIteratorIterator($Directory, RecursiveIteratorIterator::CHILD_FIRST);
        $Regex      = new RegexIterator($Iterator, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);

        foreach (iterator_to_array($Regex) as $file) {
            $file = $file[0];

            if (!in_array($file, $files)) {
                $files[] = $file;
            }
        }

        return $files;
    }
}
