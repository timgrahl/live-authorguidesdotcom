<?php

class Zippy_Admin_Utilities
{
    public function getAnalyticsTabs()
    {
        $tabs = array(
            'courses' => ZippyCourses::$path . 'assets/views/admin-pages/analytics/courses.php',
            'quiz-submissions' => ZippyCourses::$path . 'assets/views/admin-pages/analytics/quizzes.php',
        );

        return apply_filters('zippy_filter_admin_analytics_tabs', $tabs);
    }

    public function getLicenseKey()
    {
        $license = get_option('zippy_license_settings', array());

        return isset($license['license_key']) ? trim($license['license_key']) : '';
    }
}
