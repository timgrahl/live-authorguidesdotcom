<?php

class Zippy_User_Utilities
{
    public function getOwnerList()
    {
        global $wpdb;

        $output = array('-1' => 'Unclaimed');

        $users = $wpdb->get_results("SELECT ID, user_email, display_name FROM $wpdb->users ORDER BY display_name");

        foreach ($users as $user) {
            $output[$user->ID] = "{$user->display_name} - {$user->user_email}";
        }

        return $output;
    }

    public function generatePasswordResetKey($length = 24)
    {
        global $wpdb;

        $chars      = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
        $key        = '';

        for ($i=0; $i < $length; $i++) {
            $key .= $chars[rand(0, (count($chars) - 1))];
        }

        $exists = $wpdb->get_var($wpdb->prepare("SELECT umeta_id FROM $wpdb->usermeta WHERE meta_key = %s AND meta_value = %s", 'password_reset_key', $key));

        return $exists === null ? $key : $this->generatePasswordResetKey();
    }

    public function deleteExpiredPasswordResetKey($umeta_id)
    {
        global $wpdb;

        $sql = $wpdb->prepare(
            "DELETE FROM $wpdb->usermeta 
             WHERE umeta_id = %d",
            $umeta_id
        );

        return $wpdb->query($sql);
    }

    public function getLastPasswordResetKey($user_id)
    {
        $zippy = Zippy::instance();

        $student = $zippy->make('student', array($user_id));

        $password_reset_keys = $student->getPasswordResetKeys();

        $keys = $zippy->utilities->array->pluck($password_reset_keys, 'key');

        return last($keys);
    }

    public function deleteUsedPasswordResetKey($user_id, $key)
    {
        global $wpdb;

        $reset_keys = get_user_meta($user_id, 'password_reset_keys', true);

        $get_id_sql = $wpdb->prepare("SELECT umeta_id FROM $wpdb->usermeta WHERE meta_value = %s", $key);
        $delete_sql = $wpdb->prepare("DELETE FROM $wpdb->usermeta WHERE meta_value = %s", $key);
        
        $umeta_id   = $wpdb->get_var($get_id_sql);

        foreach ($reset_keys as $k => $val) {
            if ($val['umeta_id'] == $umeta_id) {
                unset($reset_keys[$k]);
            }
        }

        update_user_meta($user_id, 'password_reset_keys', $reset_keys);

        $wpdb->query($delete_sql);
    }

    public function validatePasswordResetKey($user_id, $reset_key)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $student = $zippy->make('student', array($user_id));
        $keys = $zippy->utilities->array->pluck($student->getPasswordResetKeys(), 'key');

        if (!in_array($reset_key, $keys)) {
            return false;
        }

        $sql = $wpdb->prepare(
            "SELECT umeta_id
             FROM $wpdb->usermeta
             WHERE user_id = %d
             AND meta_value = %s",
            $user_id,
            $reset_key
        );

        return $wpdb->get_var($sql) !== null;
    }
}
