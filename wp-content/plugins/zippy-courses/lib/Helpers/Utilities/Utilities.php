<?php

class Zippy_Utilities
{
    public $array;

    public function __construct()
    {
        $this->admin     = new Zippy_Admin_Utilities;
        $this->file      = new Zippy_File_Utilities;
        $this->datetime  = new Zippy_DateTime_Utilities;
        $this->email     = new Zippy_Email_Utilities;
        $this->cart      = new Zippy_Cart_Utilities;
        $this->array     = new Zippy_Array_Utilities;
        $this->product   = new Zippy_Product_Utilities;
        $this->orders    = new Zippy_Order_Utilities;
        $this->course    = new Zippy_Course_Utilities;
        $this->entry     = new Zippy_Entry_Utilities;
        $this->post      = new Zippy_Post_Utilities;
        $this->user      = new Zippy_User_Utilities;
        $this->student   = new Zippy_Student_Utilities;
        $this->quiz      = new Zippy_Quiz_Utilities;
        $this->directory = new Zippy_Directory_Utilities;
        $this->dashboard = new Zippy_Dashboard_Utilities;
        $this->transaction = new Zippy_Transaction_Utilities;
    }

    public function generateUniqueAlphanumeric($length = 8)
    {
        $chars      = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
        $string     = '';

        for ($i=0; $i < $length; $i++) {
            $string .= $chars[rand(0, (count($chars) - 1))];
        }

        return $string;
    }

    public function getInvalidPostStatus()
    {
        return array('auto-draft','trash', 'inherit');
    }

    public function getInvalidPostStatusList()
    {
        $statuses = $this->getInvalidPostStatus();

        foreach ($statuses as &$status) {
            $status = '"' . $status . '"';
        }

        return implode(',', $statuses);
    }

    public function stringEndsWith($string, $substring)
    {
        $strlen = strlen($string);
        $substrlen = strlen($substring);
        
        if ($substrlen > $strlen) {
            return false;
        }

        return substr_compare($string, $substring, $strlen - $substrlen, $substrlen) === 0;
    }

    public function getCurrentStudent()
    {
        $zippy = Zippy::instance();

        return $zippy->cache->get('student');
    }

    public function getJsonMeta($post_id, $key, $unique = true)
    {
        $value = get_post_meta($post_id, $key, $unique);

        return $this->maybeJsonDecode($value);
    }

    public function maybeJsonDecode($value)
    {
        $json = is_string($value) ? json_decode(stripcslashes(htmlentities($value))) : null;

        // Do an extra attempt, just in case stripcslashes invalidates the json
        if ($json === null && is_string($value)) {
            $json = json_decode($value);
        }

        return $json === null ? $value : $json;
    }

    public function deepJsonDecode($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = $this->deepJsonDecode($value);
            }
        }

        if (is_object($data)) {
            $array = (array) $data;

            foreach ($array as $key => $value) {
                $data->{$key} = $this->deepJsonDecode($value);
            }
        }

        return $this->maybeJsonDecode($data);
    }
}
