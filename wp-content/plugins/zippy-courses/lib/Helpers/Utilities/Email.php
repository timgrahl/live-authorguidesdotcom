<?php

class Zippy_Email_Utilities
{
    public function getSystemEmailAddress()
    {
        $settings = get_option('zippy_system_emails_general_settings', array());

        return isset($settings['sender_email']) && !empty($settings['sender_email'])
            ? $settings['sender_email']
            : get_option('admin_email');
    }

    public function getSystemEmailSender()
    {
        $settings = get_option('zippy_system_emails_general_settings', array());

        return isset($settings['sender_name']) && !empty($settings['sender_name'])
            ? $settings['sender_name']
            : get_option('blogname');
    }

    public function emailExists($setting)
    {
        return isset($setting['title'])
            && isset($setting['content'])
            && (!empty($setting['content']) || !empty($setting['title']));
    }

    public function setDefaultEmail($email_setting)
    {
        switch ($email_setting) {
            case 'zippy_system_emails_registration_settings':
                $email = $this->getDefaultRegistrationEmail();
                break;
            case 'zippy_system_emails_forgot_password_settings':
                $email = $this->getDefaultForgotPasswordEmail();
                break;
            case 'zippy_system_emails_purchase_receipt_settings':
                $email = $this->getDefaultPurchaseReceiptEmail();
                break;
            case 'zippy_system_emails_registration_reminder_settings':
                $email = $this->getDefaultRegistrationReminderEmail();
                break;
            case 'zippy_system_emails_new_student_settings':
                $email = $this->getDefaultNewStudentEmail();
                break;
            case 'zippy_system_emails_course_completion_settings':
                $email = $this->getDefaultCourseCompletionEmail();
                break;
            default:
                break;
        }
        update_option($email_setting, $email);
    }

    public function getDefaultRegistrationEmail()
    {
        $registration = array(
            'title'     => __('Thank You For Registering', ZippyCourses::TEXTDOMAIN),
            'enabled'   => array('1'),
            'content'   => __(
'Dear [first_name],

Welcome to [site_name]!  We\'re thrilled that you\'ve chosen to become a member.  Here are the details for your account for your records:

Username: [username]
Password: [password]

Forgot your password? You can visit this page to reset it: [forgot_password_url]

If we can help you in any way as a member of our site (or heck, anything else that is even vaguely related, just ask!), feel free to shoot us a note at [contact_email].

Cheers,

The Staff
[site_name]

PS. If you ever forget your password, you can recover it by visiting [forgot_password_url]',
                ZippyCourses::TEXTDOMAIN
            )
        );

        return $registration;
    }

    public function getDefaultForgotPasswordEmail()
    {
        $forgot_password = array(
            'title'     => __('Reset Your Password', ZippyCourses::TEXTDOMAIN),
            'enabled'   => array('1'),
            'content'   => __(
'Dear [first_name],

We received a request to reset your password for [site_name].

Your username is [username]
                
To reset your password, please access the link below:

[reset_password_url]

If you did not request this password reset, please ignore this message and your password will stay the same.

Cheers,

The Staff
            [site_name]',
                ZippyCourses::TEXTDOMAIN
            )
        );

        return $forgot_password;
    }

    public function getDefaultPurchaseReceiptEmail()
    {
        $purchase = array(
            'title'     => __('Thank You For Your Purchase', ZippyCourses::TEXTDOMAIN),
            'enabled'   => array('1'),
            'content'   => __(
'Dear [first_name],

Thank you for your purchase!  Here are the details of your order:

[order_receipt]

You can also access your order details at any time by visiting your <a href="[order_history_url]">Order History</a> page.

If you have any questions regarding your order, please contact us at [contact_email] and we\'ll be happy assist you in any way possible.

Cheers,

The Staff
[site_name]',
                ZippyCourses::TEXTDOMAIN
            )
        );

        return $purchase;
    }

    public function getDefaultRegistrationReminderEmail()
    {
        $registration_reminder = array(
            'title'     => __('Claim Your Order', ZippyCourses::TEXTDOMAIN),
            'enabled'   => array('1'),
            'content'   => __(
'Hello!

Thank you again for your purchase. You will be receiving a separate email with your purchase details.  This friendly reminder is so you can claim your order if you were in such a hurry that you did not at your time of purchase.

If you\'re already a member, please <a href="[login_claim_url]">click here</a> to log in and claim your order.

If you\'re new around these parts, please <a href="[registration_claim_url]">click here</a> to register

Questions? Need help getting started? Please contact us at [contact_email] and we\'ll be happy assist you in any way possible.

Cheers,

The Staff
[site_name]',
                ZippyCourses::TEXTDOMAIN
            )
        );

        return $registration_reminder;
    }

    public function getDefaultNewStudentEmail()
    {
        $new_student = array(
            'title'     => __('[first_name] joined a course!', ZippyCourses::TEXTDOMAIN),
            'content'   => __(
'Congratulations!

[first_name] [last_name] just joined the following course(s):

[course]

[student_admin_profile_link]
Cheers,

Zippy Courses
[site_name]',
                ZippyCourses::TEXTDOMAIN
            )
        );

        return $new_student;
    }

    public function getDefaultCourseCompletionEmail()
    {
        $course_completion = array(
            'title'     => __('[first_name] completed [course]!', ZippyCourses::TEXTDOMAIN),
            'content'   => __(
'Congratulations!

[first_name] [last_name] just completed the following course:

[course]

Cheers,

Zippy Courses
[site_name]',
                ZippyCourses::TEXTDOMAIN
            )
        );

        return $course_completion;
    }

    public function getSystemEmailSubject($message_type)
    {
        $option_name    = 'zippy_system_emails_' . $message_type . '_settings';
        $option         = get_option($option_name, array());

        return isset($option['title']) ? $option['title'] : '';
    }
    
    public function getSystemEmailText($message_type)
    {
        $option_name    = 'zippy_system_emails_' . $message_type . '_settings';
        $option         = get_option($option_name, array());

        return isset($option['content']) ? $option['content'] : '';
        

    }
}
