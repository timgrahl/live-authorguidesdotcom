<?php

class Zippy_Product_Utilities
{
    public function getCoursesAndTiers($product_id)
    {
        $zippy = Zippy::instance();

        if ($product_id === null) {
            return array();
        }

        $courses_and_tiers = array();

        $access = $zippy->utilities->getJsonMeta($product_id, 'access');
        if (!is_object($access) || !isset($access->mode)) {
            return $courses_and_tiers;
        }

        if ($access->mode == 'product') {
            $courses_and_tiers[] = array(
                'id' => (int) $access->course,
                'tiers' => array((int) $access->tier)
            );
        } else {
            if (isset($access->bundle) && is_array($access->bundle)) {
                foreach ($access->bundle as $course) {
                    if (!empty($course->tiers)) {
                        $courses_and_tiers[] = array(
                            'id' => (int) $course->id,
                            'tiers' => $course->tiers
                        );
                    }

                }
            }
        }

        return $courses_and_tiers;
    }

    public function getCourses($product_id)
    {
        $courses_and_tiers = $this->getCoursesAndTiers($product_id);

        $courses = array();

        foreach ($courses_and_tiers as $course) {
            if (!empty($course['tiers'])) {
                $courses[] = $course['id'];
            }
        }

        return $courses;
    }

    public function getTiers($product_id)
    {
        $courses_and_tiers = $this->getCoursesAndTiers($product_id);

        $tiers = array();

        foreach ($courses_and_tiers as $course) {
            $tiers[$course['id']] = $course['tiers'];
        }

        return $tiers;
    }

    public function getAll()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $invalid_post_statuses = $zippy->utilities->getInvalidPostStatus();
        $invalid_post_status_list = '';

        foreach ($invalid_post_statuses as $post_status) {
            $invalid_post_status_list .= '"' . $post_status . '",';
        }

        $invalid_post_status_list = rtrim($invalid_post_status_list, ',');

        $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s AND post_status NOT IN ($invalid_post_status_list)", 'product');

        $products       = $zippy->make('products');
        $product_ids    = $wpdb->get_col($sql);

        foreach ($product_ids as $product_id) {
            $product = $zippy->make('product', array($product_id));
            $products->add($product);
        }

        return $products;
    }

    public function getBySlug($slug)
    {
        global $wpdb;

        $zippy = Zippy::instance();
        $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type = 'product' LIMIT 1", $slug);

        $id = $wpdb->get_var($sql);

        return $this->getById($id);
    }

    public function getByUid($uid)
    {
        global $wpdb;

        $zippy = Zippy::instance();
        $sql = $wpdb->prepare(
            "SELECT
                post_id
             FROM
                $wpdb->postmeta
             WHERE
                meta_key = %s AND
                meta_value = %s
            LIMIT 1",
            'uid',
            $uid
        );
    
        $id = $wpdb->get_var($sql);

        return $id ? $this->getById($id) : null;
    }

    public function getById($id)
    {
        global $wpdb;

        if (!$id) {
            return null;
        }

        $zippy = Zippy::instance();

        $product = get_post($id);

        return $product !== null && $product->post_type == 'product' ? $zippy->make('product', array($id)) : null;
    }

    public function inLaunchWindow(Zippy_Product $product, $date = null)
    {
        if (!$product->getLaunchWindowsEnabled()) {
            return true;
        }
    
        $zippy          = Zippy::instance();

        $date           = $date instanceof DateTime ? $date : $zippy->utilities->datetime->getNow();
        $launch_windows = $product->getLaunchWindows();

        $in_window = false;

        foreach ($launch_windows as $launch_window) {
            $open  = $zippy->utilities->datetime->getDate($launch_window->open_date);
            $close = $zippy->utilities->datetime->getDate($launch_window->close_date);

            $in_window = $date->format('U') >= $open->format('U') && $date->format('U') <= $close->format('U');

            if ($in_window) {
                break;
            }
        }

        return $in_window;
    }

    public function getProductsNotOwned(ZippyCourses_Student $student)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $invalid_post_statuses = $zippy->utilities->getInvalidPostStatus();
        $invalid_post_status_list = '';

        $products_owned = $student->getProductsOwned();

        foreach ($invalid_post_statuses as $post_status) {
            $invalid_post_status_list .= '"' . $post_status . '",';
        }

        $invalid_post_status_list = rtrim($invalid_post_status_list, ',');

        $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s AND post_status NOT IN ($invalid_post_status_list)", 'product');
        $product_ids = $wpdb->get_col($sql);

        $products = $zippy->make('products');
        foreach ($product_ids as $product_id) {
            if (!in_array($product_id, $products_owned)) {
                $product = $zippy->make('product', array($product_id));
                $products->add($product);
            }
        }

        return $products;
    }

    public function getProductByBuyQueryVar()
    {
        global $post;

        $buy = trim(get_query_var('buy')) != '' ? get_query_var('buy') : null;

        $product = $this->getById($buy);

        if ($product === null && $buy) {
            $product = $this->getByUid($buy);
        }

        if ($product === null && $buy) {
            $product = $this->getBySlug($buy);
        }

        return $product;
    }

    public function getAllProductIds()
    {
        global $wpdb;
            
        $zippy = Zippy::instance();

        $query_id = 'zippy_get_all_product_ids';

        $invalid_post_statuses = array('"auto-draft"','"trash"', '"inherit"');
        $invalid_post_status_list = implode(', ', $invalid_post_statuses);

        if (($query = $zippy->queries->get($query_id)) === null) {
            global $wpdb;

            $query = $zippy->make('query', array('id' => $query_id));
            $query->setSql("SELECT ID FROM $wpdb->posts WHERE post_type = 'product' AND post_status NOT IN ($invalid_post_status_list)");

            $zippy->queries->add($query);
        }

        return $query->getCol();
    }

    public function getCount()
    {
        global $wpdb;

        $sql = "SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = 'product'";

        return $wpdb->get_var($sql);
    }

    public function getCountryArray()
    {
        $countries = array(
            'US' => __('United States', ZippyCourses::TEXTDOMAIN),
            'GB' => __('United Kingdom', ZippyCourses::TEXTDOMAIN),
            'CA' => __('Canada', ZippyCourses::TEXTDOMAIN),
            'MX' => __('Mexico', ZippyCourses::TEXTDOMAIN),
            'NAN' => __('-------', ZippyCourses::TEXTDOMAIN),
            'AF' => __('Afghanistan', ZippyCourses::TEXTDOMAIN),
            'AX' => __('Aland Islands', ZippyCourses::TEXTDOMAIN),
            'AL' => __('Albania', ZippyCourses::TEXTDOMAIN),
            'DZ' => __('Algeria', ZippyCourses::TEXTDOMAIN),
            'AS' => __('American Samoa', ZippyCourses::TEXTDOMAIN),
            'AD' => __('Andorra', ZippyCourses::TEXTDOMAIN),
            'AO' => __('Angola', ZippyCourses::TEXTDOMAIN),
            'AI' => __('Anguilla', ZippyCourses::TEXTDOMAIN),
            'AQ' => __('Antarctica', ZippyCourses::TEXTDOMAIN),
            'AG' => __('Antigua And Barbuda', ZippyCourses::TEXTDOMAIN),
            'AR' => __('Argentina', ZippyCourses::TEXTDOMAIN),
            'AM' => __('Armenia', ZippyCourses::TEXTDOMAIN),
            'AW' => __('Aruba', ZippyCourses::TEXTDOMAIN),
            'AU' => __('Australia', ZippyCourses::TEXTDOMAIN),
            'AT' => __('Austria', ZippyCourses::TEXTDOMAIN),
            'AZ' => __('Azerbaijan', ZippyCourses::TEXTDOMAIN),
            'BS' => __('Bahamas', ZippyCourses::TEXTDOMAIN),
            'BH' => __('Bahrain', ZippyCourses::TEXTDOMAIN),
            'BD' => __('Bangladesh', ZippyCourses::TEXTDOMAIN),
            'BB' => __('Barbados', ZippyCourses::TEXTDOMAIN),
            'BY' => __('Belarus', ZippyCourses::TEXTDOMAIN),
            'BE' => __('Belgium', ZippyCourses::TEXTDOMAIN),
            'BZ' => __('Belize', ZippyCourses::TEXTDOMAIN),
            'BJ' => __('Benin', ZippyCourses::TEXTDOMAIN),
            'BM' => __('Bermuda', ZippyCourses::TEXTDOMAIN),
            'BT' => __('Bhutan', ZippyCourses::TEXTDOMAIN),
            'BO' => __('Bolivia', ZippyCourses::TEXTDOMAIN),
            'BA' => __('Bosnia And Herzegovina', ZippyCourses::TEXTDOMAIN),
            'BW' => __('Botswana', ZippyCourses::TEXTDOMAIN),
            'BV' => __('Bouvet Island', ZippyCourses::TEXTDOMAIN),
            'BR' => __('Brazil', ZippyCourses::TEXTDOMAIN),
            'IO' => __('British Indian Ocean Territory', ZippyCourses::TEXTDOMAIN),
            'BN' => __('Brunei Darussalam', ZippyCourses::TEXTDOMAIN),
            'BG' => __('Bulgaria', ZippyCourses::TEXTDOMAIN),
            'BF' => __('Burkina Faso', ZippyCourses::TEXTDOMAIN),
            'BI' => __('Burundi', ZippyCourses::TEXTDOMAIN),
            'KH' => __('Cambodia', ZippyCourses::TEXTDOMAIN),
            'CM' => __('Cameroon', ZippyCourses::TEXTDOMAIN),
            'CA' => __('Canada', ZippyCourses::TEXTDOMAIN),
            'CV' => __('Cape Verde', ZippyCourses::TEXTDOMAIN),
            'KY' => __('Cayman Islands', ZippyCourses::TEXTDOMAIN),
            'CF' => __('Central African Republic', ZippyCourses::TEXTDOMAIN),
            'TD' => __('Chad', ZippyCourses::TEXTDOMAIN),
            'CL' => __('Chile', ZippyCourses::TEXTDOMAIN),
            'CN' => __('China', ZippyCourses::TEXTDOMAIN),
            'CX' => __('Christmas Island', ZippyCourses::TEXTDOMAIN),
            'CC' => __('Cocos (Keeling) Islands', ZippyCourses::TEXTDOMAIN),
            'CO' => __('Colombia', ZippyCourses::TEXTDOMAIN),
            'KM' => __('Comoros', ZippyCourses::TEXTDOMAIN),
            'CG' => __('Congo', ZippyCourses::TEXTDOMAIN),
            'CD' => __('Congo, Democratic Republic', ZippyCourses::TEXTDOMAIN),
            'CK' => __('Cook Islands', ZippyCourses::TEXTDOMAIN),
            'CR' => __('Costa Rica', ZippyCourses::TEXTDOMAIN),
            'CI' => __('Cote D\'Ivoire', ZippyCourses::TEXTDOMAIN),
            'HR' => __('Croatia', ZippyCourses::TEXTDOMAIN),
            'CU' => __('Cuba', ZippyCourses::TEXTDOMAIN),
            'CY' => __('Cyprus', ZippyCourses::TEXTDOMAIN),
            'CZ' => __('Czech Republic', ZippyCourses::TEXTDOMAIN),
            'DK' => __('Denmark', ZippyCourses::TEXTDOMAIN),
            'DJ' => __('Djibouti', ZippyCourses::TEXTDOMAIN),
            'DM' => __('Dominica', ZippyCourses::TEXTDOMAIN),
            'DO' => __('Dominican Republic', ZippyCourses::TEXTDOMAIN),
            'EC' => __('Ecuador', ZippyCourses::TEXTDOMAIN),
            'EG' => __('Egypt', ZippyCourses::TEXTDOMAIN),
            'SV' => __('El Salvador', ZippyCourses::TEXTDOMAIN),
            'GQ' => __('Equatorial Guinea', ZippyCourses::TEXTDOMAIN),
            'ER' => __('Eritrea', ZippyCourses::TEXTDOMAIN),
            'EE' => __('Estonia', ZippyCourses::TEXTDOMAIN),
            'ET' => __('Ethiopia', ZippyCourses::TEXTDOMAIN),
            'FK' => __('Falkland Islands (Malvinas)', ZippyCourses::TEXTDOMAIN),
            'FO' => __('Faroe Islands', ZippyCourses::TEXTDOMAIN),
            'FJ' => __('Fiji', ZippyCourses::TEXTDOMAIN),
            'FI' => __('Finland', ZippyCourses::TEXTDOMAIN),
            'FR' => __('France', ZippyCourses::TEXTDOMAIN),
            'GF' => __('French Guiana', ZippyCourses::TEXTDOMAIN),
            'PF' => __('French Polynesia', ZippyCourses::TEXTDOMAIN),
            'TF' => __('French Southern Territories', ZippyCourses::TEXTDOMAIN),
            'GA' => __('Gabon', ZippyCourses::TEXTDOMAIN),
            'GM' => __('Gambia', ZippyCourses::TEXTDOMAIN),
            'GE' => __('Georgia', ZippyCourses::TEXTDOMAIN),
            'DE' => __('Germany', ZippyCourses::TEXTDOMAIN),
            'GH' => __('Ghana', ZippyCourses::TEXTDOMAIN),
            'GI' => __('Gibraltar', ZippyCourses::TEXTDOMAIN),
            'GR' => __('Greece', ZippyCourses::TEXTDOMAIN),
            'GL' => __('Greenland', ZippyCourses::TEXTDOMAIN),
            'GD' => __('Grenada', ZippyCourses::TEXTDOMAIN),
            'GP' => __('Guadeloupe', ZippyCourses::TEXTDOMAIN),
            'GU' => __('Guam', ZippyCourses::TEXTDOMAIN),
            'GT' => __('Guatemala', ZippyCourses::TEXTDOMAIN),
            'GG' => __('Guernsey', ZippyCourses::TEXTDOMAIN),
            'GN' => __('Guinea', ZippyCourses::TEXTDOMAIN),
            'GW' => __('Guinea-Bissau', ZippyCourses::TEXTDOMAIN),
            'GY' => __('Guyana', ZippyCourses::TEXTDOMAIN),
            'HT' => __('Haiti', ZippyCourses::TEXTDOMAIN),
            'HM' => __('Heard Island & Mcdonald Islands', ZippyCourses::TEXTDOMAIN),
            'VA' => __('Holy See (Vatican City State)', ZippyCourses::TEXTDOMAIN),
            'HN' => __('Honduras', ZippyCourses::TEXTDOMAIN),
            'HK' => __('Hong Kong', ZippyCourses::TEXTDOMAIN),
            'HU' => __('Hungary', ZippyCourses::TEXTDOMAIN),
            'IS' => __('Iceland', ZippyCourses::TEXTDOMAIN),
            'IN' => __('India', ZippyCourses::TEXTDOMAIN),
            'ID' => __('Indonesia', ZippyCourses::TEXTDOMAIN),
            'IR' => __('Iran, Islamic Republic Of', ZippyCourses::TEXTDOMAIN),
            'IQ' => __('Iraq', ZippyCourses::TEXTDOMAIN),
            'IE' => __('Ireland', ZippyCourses::TEXTDOMAIN),
            'IM' => __('Isle Of Man', ZippyCourses::TEXTDOMAIN),
            'IL' => __('Israel', ZippyCourses::TEXTDOMAIN),
            'IT' => __('Italy', ZippyCourses::TEXTDOMAIN),
            'JM' => __('Jamaica', ZippyCourses::TEXTDOMAIN),
            'JP' => __('Japan', ZippyCourses::TEXTDOMAIN),
            'JE' => __('Jersey', ZippyCourses::TEXTDOMAIN),
            'JO' => __('Jordan', ZippyCourses::TEXTDOMAIN),
            'KZ' => __('Kazakhstan', ZippyCourses::TEXTDOMAIN),
            'KE' => __('Kenya', ZippyCourses::TEXTDOMAIN),
            'KI' => __('Kiribati', ZippyCourses::TEXTDOMAIN),
            'KR' => __('Korea', ZippyCourses::TEXTDOMAIN),
            'KW' => __('Kuwait', ZippyCourses::TEXTDOMAIN),
            'KG' => __('Kyrgyzstan', ZippyCourses::TEXTDOMAIN),
            'LA' => __('Lao People\'s Democratic Republic', ZippyCourses::TEXTDOMAIN),
            'LV' => __('Latvia', ZippyCourses::TEXTDOMAIN),
            'LB' => __('Lebanon', ZippyCourses::TEXTDOMAIN),
            'LS' => __('Lesotho', ZippyCourses::TEXTDOMAIN),
            'LR' => __('Liberia', ZippyCourses::TEXTDOMAIN),
            'LY' => __('Libyan Arab Jamahiriya', ZippyCourses::TEXTDOMAIN),
            'LI' => __('Liechtenstein', ZippyCourses::TEXTDOMAIN),
            'LT' => __('Lithuania', ZippyCourses::TEXTDOMAIN),
            'LU' => __('Luxembourg', ZippyCourses::TEXTDOMAIN),
            'MO' => __('Macao', ZippyCourses::TEXTDOMAIN),
            'MK' => __('Macedonia', ZippyCourses::TEXTDOMAIN),
            'MG' => __('Madagascar', ZippyCourses::TEXTDOMAIN),
            'MW' => __('Malawi', ZippyCourses::TEXTDOMAIN),
            'MY' => __('Malaysia', ZippyCourses::TEXTDOMAIN),
            'MV' => __('Maldives', ZippyCourses::TEXTDOMAIN),
            'ML' => __('Mali', ZippyCourses::TEXTDOMAIN),
            'MT' => __('Malta', ZippyCourses::TEXTDOMAIN),
            'MH' => __('Marshall Islands', ZippyCourses::TEXTDOMAIN),
            'MQ' => __('Martinique', ZippyCourses::TEXTDOMAIN),
            'MR' => __('Mauritania', ZippyCourses::TEXTDOMAIN),
            'MU' => __('Mauritius', ZippyCourses::TEXTDOMAIN),
            'YT' => __('Mayotte', ZippyCourses::TEXTDOMAIN),
            'MX' => __('Mexico', ZippyCourses::TEXTDOMAIN),
            'FM' => __('Micronesia, Federated States Of', ZippyCourses::TEXTDOMAIN),
            'MD' => __('Moldova', ZippyCourses::TEXTDOMAIN),
            'MC' => __('Monaco', ZippyCourses::TEXTDOMAIN),
            'MN' => __('Mongolia', ZippyCourses::TEXTDOMAIN),
            'ME' => __('Montenegro', ZippyCourses::TEXTDOMAIN),
            'MS' => __('Montserrat', ZippyCourses::TEXTDOMAIN),
            'MA' => __('Morocco', ZippyCourses::TEXTDOMAIN),
            'MZ' => __('Mozambique', ZippyCourses::TEXTDOMAIN),
            'MM' => __('Myanmar', ZippyCourses::TEXTDOMAIN),
            'NA' => __('Namibia', ZippyCourses::TEXTDOMAIN),
            'NR' => __('Nauru', ZippyCourses::TEXTDOMAIN),
            'NP' => __('Nepal', ZippyCourses::TEXTDOMAIN),
            'NL' => __('Netherlands', ZippyCourses::TEXTDOMAIN),
            'AN' => __('Netherlands Antilles', ZippyCourses::TEXTDOMAIN),
            'NC' => __('New Caledonia', ZippyCourses::TEXTDOMAIN),
            'NZ' => __('New Zealand', ZippyCourses::TEXTDOMAIN),
            'NI' => __('Nicaragua', ZippyCourses::TEXTDOMAIN),
            'NE' => __('Niger', ZippyCourses::TEXTDOMAIN),
            'NG' => __('Nigeria', ZippyCourses::TEXTDOMAIN),
            'NU' => __('Niue', ZippyCourses::TEXTDOMAIN),
            'NF' => __('Norfolk Island', ZippyCourses::TEXTDOMAIN),
            'MP' => __('Northern Mariana Islands', ZippyCourses::TEXTDOMAIN),
            'NO' => __('Norway', ZippyCourses::TEXTDOMAIN),
            'OM' => __('Oman', ZippyCourses::TEXTDOMAIN),
            'PK' => __('Pakistan', ZippyCourses::TEXTDOMAIN),
            'PW' => __('Palau', ZippyCourses::TEXTDOMAIN),
            'PS' => __('Palestinian Territory, Occupied', ZippyCourses::TEXTDOMAIN),
            'PA' => __('Panama', ZippyCourses::TEXTDOMAIN),
            'PG' => __('Papua New Guinea', ZippyCourses::TEXTDOMAIN),
            'PY' => __('Paraguay', ZippyCourses::TEXTDOMAIN),
            'PE' => __('Peru', ZippyCourses::TEXTDOMAIN),
            'PH' => __('Philippines', ZippyCourses::TEXTDOMAIN),
            'PN' => __('Pitcairn', ZippyCourses::TEXTDOMAIN),
            'PL' => __('Poland', ZippyCourses::TEXTDOMAIN),
            'PT' => __('Portugal', ZippyCourses::TEXTDOMAIN),
            'PR' => __('Puerto Rico', ZippyCourses::TEXTDOMAIN),
            'QA' => __('Qatar', ZippyCourses::TEXTDOMAIN),
            'RE' => __('Reunion', ZippyCourses::TEXTDOMAIN),
            'RO' => __('Romania', ZippyCourses::TEXTDOMAIN),
            'RU' => __('Russian Federation', ZippyCourses::TEXTDOMAIN),
            'RW' => __('Rwanda', ZippyCourses::TEXTDOMAIN),
            'BL' => __('Saint Barthelemy', ZippyCourses::TEXTDOMAIN),
            'SH' => __('Saint Helena', ZippyCourses::TEXTDOMAIN),
            'KN' => __('Saint Kitts And Nevis', ZippyCourses::TEXTDOMAIN),
            'LC' => __('Saint Lucia', ZippyCourses::TEXTDOMAIN),
            'MF' => __('Saint Martin', ZippyCourses::TEXTDOMAIN),
            'PM' => __('Saint Pierre And Miquelon', ZippyCourses::TEXTDOMAIN),
            'VC' => __('Saint Vincent And Grenadines', ZippyCourses::TEXTDOMAIN),
            'WS' => __('Samoa', ZippyCourses::TEXTDOMAIN),
            'SM' => __('San Marino', ZippyCourses::TEXTDOMAIN),
            'ST' => __('Sao Tome And Principe', ZippyCourses::TEXTDOMAIN),
            'SA' => __('Saudi Arabia', ZippyCourses::TEXTDOMAIN),
            'SN' => __('Senegal', ZippyCourses::TEXTDOMAIN),
            'RS' => __('Serbia', ZippyCourses::TEXTDOMAIN),
            'SC' => __('Seychelles', ZippyCourses::TEXTDOMAIN),
            'SL' => __('Sierra Leone', ZippyCourses::TEXTDOMAIN),
            'SG' => __('Singapore', ZippyCourses::TEXTDOMAIN),
            'SK' => __('Slovakia', ZippyCourses::TEXTDOMAIN),
            'SI' => __('Slovenia', ZippyCourses::TEXTDOMAIN),
            'SB' => __('Solomon Islands', ZippyCourses::TEXTDOMAIN),
            'SO' => __('Somalia', ZippyCourses::TEXTDOMAIN),
            'ZA' => __('South Africa', ZippyCourses::TEXTDOMAIN),
            'GS' => __('South Georgia And Sandwich Isl.', ZippyCourses::TEXTDOMAIN),
            'ES' => __('Spain', ZippyCourses::TEXTDOMAIN),
            'LK' => __('Sri Lanka', ZippyCourses::TEXTDOMAIN),
            'SD' => __('Sudan', ZippyCourses::TEXTDOMAIN),
            'SR' => __('Suriname', ZippyCourses::TEXTDOMAIN),
            'SJ' => __('Svalbard And Jan Mayen', ZippyCourses::TEXTDOMAIN),
            'SZ' => __('Swaziland', ZippyCourses::TEXTDOMAIN),
            'SE' => __('Sweden', ZippyCourses::TEXTDOMAIN),
            'CH' => __('Switzerland', ZippyCourses::TEXTDOMAIN),
            'SY' => __('Syrian Arab Republic', ZippyCourses::TEXTDOMAIN),
            'TW' => __('Taiwan', ZippyCourses::TEXTDOMAIN),
            'TJ' => __('Tajikistan', ZippyCourses::TEXTDOMAIN),
            'TZ' => __('Tanzania', ZippyCourses::TEXTDOMAIN),
            'TH' => __('Thailand', ZippyCourses::TEXTDOMAIN),
            'TL' => __('Timor-Leste', ZippyCourses::TEXTDOMAIN),
            'TG' => __('Togo', ZippyCourses::TEXTDOMAIN),
            'TK' => __('Tokelau', ZippyCourses::TEXTDOMAIN),
            'TO' => __('Tonga', ZippyCourses::TEXTDOMAIN),
            'TT' => __('Trinidad And Tobago', ZippyCourses::TEXTDOMAIN),
            'TN' => __('Tunisia', ZippyCourses::TEXTDOMAIN),
            'TR' => __('Turkey', ZippyCourses::TEXTDOMAIN),
            'TM' => __('Turkmenistan', ZippyCourses::TEXTDOMAIN),
            'TC' => __('Turks And Caicos Islands', ZippyCourses::TEXTDOMAIN),
            'TV' => __('Tuvalu', ZippyCourses::TEXTDOMAIN),
            'UG' => __('Uganda', ZippyCourses::TEXTDOMAIN),
            'UA' => __('Ukraine', ZippyCourses::TEXTDOMAIN),
            'AE' => __('United Arab Emirates', ZippyCourses::TEXTDOMAIN),
            'GB' => __('United Kingdom', ZippyCourses::TEXTDOMAIN),
            'US' => __('United States', ZippyCourses::TEXTDOMAIN),
            'UM' => __('United States Outlying Islands', ZippyCourses::TEXTDOMAIN),
            'UY' => __('Uruguay', ZippyCourses::TEXTDOMAIN),
            'UZ' => __('Uzbekistan', ZippyCourses::TEXTDOMAIN),
            'VU' => __('Vanuatu', ZippyCourses::TEXTDOMAIN),
            'VE' => __('Venezuela', ZippyCourses::TEXTDOMAIN),
            'VN' => __('Viet Nam', ZippyCourses::TEXTDOMAIN),
            'VG' => __('Virgin Islands, British', ZippyCourses::TEXTDOMAIN),
            'VI' => __('Virgin Islands, U.S.', ZippyCourses::TEXTDOMAIN),
            'WF' => __('Wallis And Futuna', ZippyCourses::TEXTDOMAIN),
            'EH' => __('Western Sahara', ZippyCourses::TEXTDOMAIN),
            'YE' => __('Yemen', ZippyCourses::TEXTDOMAIN),
            'ZM' => __('Zambia', ZippyCourses::TEXTDOMAIN),
            'ZW' => __('Zimbabwe', ZippyCourses::TEXTDOMAIN),
        );

        return $countries;
    }

    public function claimProductByUid(ZippyCourses_Student $student, $uid)
    {
        $zippy = Zippy::instance();
        $product = $this->getByUid($uid);

        if ($product === null) {
            $transaction = $zippy->make('transaction');
            $transaction->buildByTransactionKey($uid);

            $zippy->access->claim($student, $transaction);
        } else {
            $zippy->access->grant($current_user->ID, $product->getId());
            $courses = $product->getCourses();
            foreach ($courses as &$course) {
                $course = get_the_title($course);
            }
            $zippy->sessions->flashMessage(
                sprintf(
                    __('You have joined the following course(s): %s', ZippyCourses::TEXTDOMAIN),
                    '<strong>' . implode(', ', $courses) . '</strong>'
                )
            );
        }
    }
    
    public function getProductByTransactionKey($transaction_key)
    {
        $zippy = Zippy::instance();
        $transaction = $zippy->make('transaction');
        $transaction->buildByTransactionKey($transaction_key);
        return $transaction->getProduct();
    }
    /**
     * Return an Array of Product IDs and the courses they grant access to
     * @return array
     */
    public function getAllProductCourseIds()
    {
        global $wpdb;
        $zippy = Zippy::instance();
        // Get the Access meta value when a Product is the post type
        $sql = "
        SELECT pm.post_id, pm.meta_value
        FROM $wpdb->postmeta AS pm
        LEFT JOIN $wpdb->posts as p ON (p.ID = pm.post_id)
        WHERE p.post_type = 'product'
        AND pm.meta_key = 'access';
        ";
        
        $products = $wpdb->get_results($sql);
        $product_ids = array();
        foreach ($products as $product) {
            $access_data = $zippy->utilities->maybeJsonDecode($product->meta_value);

            if ($access_data->mode == 'product') {
                $product_ids[$product->post_id][] = $access_data->course;
            } else {
                foreach ($access_data->bundle as $single_course) {
                    if (!empty($single_course->tiers)) {
                        $product_ids[$product->post_id][] = $single_course->id;
                    }
                }
            }
        }
        return $product_ids;
    }

    public function getProductCourseIds($product_id)
    {
        $product_ids = $this->getAllProductCourseIds();
        return $product_ids[$product_id];
    }
}
