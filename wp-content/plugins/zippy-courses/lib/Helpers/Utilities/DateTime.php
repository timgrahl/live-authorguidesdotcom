<?php

class Zippy_DateTime_Utilities
{
    public function getDate($date)
    {
        if ($date === null) {
            return $this->getNow();
        }

        return new DateTime($date, $this->getTimezone());
    }

    public function getDateFromTimestamp($timestamp)
    {
        if ($timestamp === null) {
            return $this->getNow();
        }
        $date = new DateTime('@' . $timestamp);
        $date->setTimezone($this->getTimezone());
        return $date;
    }

    public function getToday()
    {
        return new DateTime('today', $this->getTimezone());
    }

    public function getNow()
    {
        return new DateTime('now', $this->getTimezone());
    }

    public function getTimezone()
    {
        $tz = $this->getTimezoneString();
        return new DateTimeZone($tz);
    }

    /**
     * Returns the timezone string for a site, even if it's set to a UTC offset
     *
     * Adapted from http://www.php.net/manual/en/function.timezone-name-from-abbr.php#89155
     *
     * @return string valid PHP timezone string
     */
    public static function getTimezoneString() {

        // if site timezone string exists, return it
        if ($timezone = get_option( 'timezone_string')) {
            return $timezone;
        }
     
        // get UTC offset, if it isn't set then return UTC
        if (0 === ($utc_offset = get_option( 'gmt_offset', 0 ))) {
            return 'UTC';
        }
     
        // adjust UTC offset from hours to seconds
        $utc_offset *= 3600;
     
        // attempt to guess the timezone string from the UTC offset
        if ($timezone = timezone_name_from_abbr('', $utc_offset, 0 )) {
            return $timezone;
        }

        // last try, guess timezone string manually
        $is_dst = date('I');

        foreach (timezone_abbreviations_list() as $abbr) {
            foreach ($abbr as $city) {
                if ($city['dst'] == $is_dst && $city['offset'] == $utc_offset) {
                    return $city['timezone_id'];
                }
            }
        }
         
        // fallback to UTC
        return 'UTC';
    }

    public function getDateFormat()
    {
        $format = get_option('date_format');
        return !empty($format) ? $format : 'F j, Y';
    }


    public function getFormattedDate(DateTime $date)
    {
        $format = $this->getDateFormat();
        return date_i18n($format, strtotime($date->format('F j, Y')));
    }
}
