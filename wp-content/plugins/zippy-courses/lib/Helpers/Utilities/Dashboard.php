<?php

class Zippy_Dashboard_Utilities
{
    public function featuredMediaVisible()
    {
        $settings = get_option('zippy_customizer_dashboard_options', array());

        return (isset($settings['featured_image']) && $settings['featured_image']) || !isset($settings['featured_image']);
    }

    public function excerptsVisible()
    {
        $settings = get_option('zippy_customizer_dashboard_options', array());

        return (isset($settings['excerpt']) && $settings['excerpt']) || !isset($settings['excerpt']);
    }
}
