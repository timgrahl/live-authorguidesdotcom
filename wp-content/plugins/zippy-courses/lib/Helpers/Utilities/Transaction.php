<?php

class Zippy_Transaction_Utilities
{
    public function generateKey($length = 8)
    {
        global $wpdb;

        $chars      = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
        $key        = '';

        for ($i=0; $i < $length; $i++) {
            $key .= $chars[rand(0, (count($chars) - 1))];
        }

        $exists = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'transaction_key', $key));

        return $exists === null ? $key : $this->generateKey();
    }

    public function requiresPaymentEmail(Zippy_Transaction $transaction)
    {
        $exceptions = array('paypal');
        $gateway = $transaction->getGateway();
        // zdd($gateway);
        if (in_array($gateway, $exceptions)) {
            return false;
        }
        return true;
    }
}
