<?php

class Zippy_Order_Utilities
{
    public function getCourses($order)
    {
        if (!($order instanceof Zippy_Order)) {
            return false;
        }

        $zippy = Zippy::instance();

        $product_id = $order->getProduct() ? $order->getProduct()->getId() : false;

        if ($product_id) {
            $course_ids = $zippy->utilities->product->getCourses($product_id);
            $courses = array();

            foreach ($course_ids as $course_id) {
                $courses[] = get_the_title($course_id);
            }

            return implode(', ', $courses);
        }

        return false;
    }

    public function getCourseList($order)
    {
        if (!($order instanceof Zippy_Order)) {
            return false;
        }

        $zippy = Zippy::instance();

        $product_id = $order->getProduct() ? $order->getProduct()->getId() : false;

        $courses = $product_id ? $zippy->utilities->product->getCourses($product_id) : array();

        foreach ($courses as $key => $course_id) {
            $courses[$key] = get_the_title($course_id);
        }

        return !empty($courses) ? implode(', ', $courses) : '';
    }

    public function getReceipt($order)
    {
        if (!($order instanceof Zippy_Order)) {
            return false;
        }

        $product        = $order->getProduct();
        $product_title  = $product instanceof Zippy_Product ? $product->getTitle() : '';

        $date           = get_the_date('M d, Y', $order->getId());

        $total          = $order->getTotal();

        $order->fetchTransactions();

        $output = "\n\n";

            $output .= 'Date: ' . $date . "\n";
            $output .= 'Product: ' . $product_title . "\n";
            $output .= 'Total: ' . $order->getTotalString();

        $output .= "\n\n";

        return $output;
    }

    public function getLoginUrl($order)
    {
        return $this->getClaimUrl($order, 'login');
    }

    public function getRegistrationUrl($order)
    {
        return $this->getClaimUrl($order, 'register');
    }

    private function getClaimUrl($order, $core_page)
    {
        if (!($order instanceof Zippy_Order)) {
            return false;
        }

        $zippy = Zippy::instance();

        if (!$order->transactions->count()) {
            $order->fetchTransactions();
        }

        $transaction = $order->transactions->first();

        $base = $zippy->core_pages->getUrl($core_page);

        return $base . '?claim=' . $transaction->getKey();
    }

    public function getVendorRecurringId($order_id)
    {
        global $wpdb;

        $sql = $wpdb->prepare(
            "SELECT
                pm1.meta_value
             FROM
                $wpdb->postmeta AS pm1
             LEFT JOIN
                $wpdb->postmeta AS pm2
             ON
                (pm1.post_id = pm2.post_id)
             WHERE
                pm2.meta_key = %s AND
                pm2.meta_value = $order_id AND
                pm1.meta_key = %s",
            'order_id',
            'recurring_id'
        );

        return $wpdb->get_var($sql);
    }

    public function resendRegistrationReminder(Zippy_Order $order)
    {
        $zippy = Zippy::instance();

        $customer = $order->getCustomer();

        if ($customer instanceof Zippy_Customer) {
            $event = new ZippyCourses_NewOrder_Event($order);
            $event->type = 'registration_reminder';
            $event->user = $event->order->getCustomer();

            $email = $zippy->make(
                'email',
                array(
                    'type' => $event->type,
                    'user' => $event->user,
                    'event' => $event
                )
            );

            
            $sent = wp_mail(
                $email->getRecipients(),
                $email->getTitle(),
                $email->getContent(),
                $email->getHeaders()
            );

            if ($sent) {
                $zippy->log('Sent "' . $email->getTitle() . '" to ' . $email->getRecipients(), 'EMAIL_SENT');
                $zippy->log($email->getContent(), 'EMAIL_MSG');
            } else {
                $zippy->log('Failed sending "' . $email->getTitle() . '" to ' . $email->getRecipients(), 'EMAIL_ERROR');
            }
        }
    }

    public function getNumberOfOrders()
    {
        global $wpdb;

        return $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = 'zippy_order'");
    }

    public function getOrderIds($start_date = '', $end_date = '')
    {
        global $wpdb;
        $zippy = Zippy::instance();

        $start_date = !empty($start_date) ? new DateTime("@$start_date") : new DateTime("@0");
        $end_date = !empty($end_date) ? new DateTime("@$end_date") : $zippy->utilities->datetime->getToday();
        $end_date->modify('+1 day');

        $sql = $wpdb->prepare(
            "SELECT ID FROM $wpdb->posts WHERE post_type = %s AND post_date BETWEEN %s and %s",
            'zippy_order',
            $start_date->format('Y-m-d H:i:s'),
            $end_date->format('Y-m-d H:i:s')
        );

        return $wpdb->get_col($sql);

    }

    public function getRecentOrderIds($num_days = 30)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $tomorrow = $zippy->utilities->datetime->getToday();
        $tomorrow->modify('+1 day');

        $limit = $zippy->utilities->datetime->getToday();
        $limit->modify("-$num_days days");

        $sql = $wpdb->prepare(
            "SELECT ID FROM $wpdb->posts WHERE post_type = %s AND post_date BETWEEN %s AND %s",
            'zippy_order',
            $limit->format('Y-m-d H:i:s'),
            $tomorrow->format('Y-m-d H:i:s')
        );

        return $wpdb->get_col($sql);
    }

    public function getAverageValueOfOrders(array $order_ids)
    {
        global $wpdb;

        $list = implode(',', $order_ids);

        if (!count($order_ids)) {
            return 0;
        }

        $sql = $wpdb->prepare(
            "SELECT meta_value
             FROM $wpdb->postmeta
             WHERE 
                meta_key = %s AND
                post_id IN ($list)
            ",
            'details'
        );

        $details = $wpdb->get_col($sql);

        $total = 0;
        foreach ($details as $detail) {
            $total += isset($detail['amount']) ? $detail['amount'] : 0;
        }

        return count($details) ? number_format_i18n(($total / count($details)), 2) : 0;
    }

    public function getCount()
    {
        global $wpdb;

        $sql = "SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = 'zippy_order'";

        return $wpdb->get_var($sql);
    }
    public function getAllUnclaimedOrderIds()
    {
        global $wpdb;
        $sql = "SELECT ID FROM $wpdb->posts WHERE post_type = 'zippy_order' AND post_author <= 0";
        return $wpdb->get_col($sql);
    }
}
