<?php

class Zippy_Student_Utilities
{
    /**
     * Retrieve the data for the #zippy-student-courses metabox
     *
     * @since  1.0.0
     *
     * @param  ZippyCourses_Student $student
     *
     * @return json
     */
    public function getCourseMetaboxData(ZippyCourses_Student $student)
    {
        $zippy = Zippy::instance();

        $output = array();

        $course_tiers   = $student->getCourseTiers();
        $courses        = $student->getCourses();
        $start_dates    = $student->getCourseStartDates();
        $date_format    = '';

        foreach ($course_tiers as $course_id => $tiers) {
            $course = $courses->get($course_id);

            if ($course === null) {
                continue;
            }

            $start_date = isset($start_dates[$course_id])
                ? $start_dates[$course_id]->format('m/d/Y')
                : $zippy->utilities->datetime->getToday()->format('m/d/Y');

            $output[] = array(
                'id'            => $course_id,
                'title'         => get_the_title($course_id),
                'tiers'         => $tiers,
                'start_date'    => $start_date,
                'progress'      => $student->getCourseProgress($course)
            );
        }

        return $output;
    }

    public function getStudentIds()
    {
        global $wpdb;

        $sql = "SELECT post_author FROM $wpdb->posts WHERE post_type = 'zippy_order' GROUP BY post_author";
        $students = $wpdb->get_col($sql);

        return $students;
    }

    public function getNumberOfStudents()
    {
        return count($this->getStudentIds());
    }

    public function getRecentStudentIds($num_days = 30)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $tomorrow = $zippy->utilities->datetime->getToday();
        $tomorrow->modify('+1 day');

        $limit = $zippy->utilities->datetime->getToday();
        $limit->modify("-$num_days days");

        $sql = $wpdb->prepare(
            "SELECT ID FROM $wpdb->users WHERE user_registered BETWEEN %s AND %s",
            $limit->format('Y-m-d H:i:s'),
            $tomorrow->format('Y-m-d H:i:s')
        );

        $ids = $wpdb->get_col($sql);

        $student_ids = $this->getStudentIds();

        foreach ($student_ids as $key => $id) {
            if (!in_array($id, $ids)) {
                unset($student_ids[$key]);
            }
        }

        return array_values($student_ids);
    }

    public function getStudentsWithCourses($course_ids)
    {
        $zippy = Zippy::instance();

        $product_ids = array();

        foreach ($course_ids as $course_id) {
            $product_ids = array_merge(
                $product_ids,
                $zippy->utilities->course->getProductIdsForCourse($course_id)
            );
        }

        $student_ids_with_products      = $this->getStudentsWithProducts($product_ids);
        $student_ids_with_permissions   = $this->getStudentsWithPermissions($course_ids);

        return array_unique(array_merge($student_ids_with_permissions, $student_ids_with_products));
    }

    public function getStudentsWithCourse($course_id)
    {
        $product_ids = $zippy->utilities->course->getProductIdsForCourse($course_id);

        return $this->getStudentWithProducts($product_ids);
    }

    public function getStudentsWithPermissions($course_ids)
    {
        global $wpdb;

        $students = array();

        $permissions = $wpdb->get_results("SELECT * FROM $wpdb->usermeta WHERE meta_key = 'zippy_permissions'");

        foreach ($permissions as $row) {
            $permission = json_decode($row->meta_value);

            if (isset($permission->granted) && isset($permission->revoked)) {
                foreach ($permission->granted as $gperm) {
                    if (in_array($gperm->course, $course_ids)) {
                        $revoked = false;

                        if (is_array($permission->revoked)) {
                            foreach ($permission->revoked as $rperm) {
                                if ($gperm->course == $rperm->course) {
                                    $revoked = true;
                                }
                            }
                        }

                        if (!$revoked) {
                            $students[] = $row->user_id;
                        }
                    }
                }
            }
        }

        return array_unique(array_filter($students));
    }

    public function getStudentsWithProducts($product_ids)
    {
        $students = array();

        foreach ($product_ids as $product_id) {
            $students = array_merge($students, $this->getStudentsWithProduct($product_id));
        }

        return array_unique(array_filter($students));
    }

    public function getStudentsWithProduct($product_id)
    {
        global $wpdb;

        $user_ids = array();

        $results = $wpdb->get_results("SELECT user_id, meta_value FROM $wpdb->usermeta WHERE meta_key = 'products'");

        foreach ($results as $result) {
            $user_id    = $result->user_id;
            $products   = maybe_unserialize($result->meta_value);

            if (in_array($product_id, $products)) {
                $user_ids[] = $user_id;
            }
        }

        $product_owner_ids_sql = $wpdb->prepare(
            "SELECT
                p.post_author
             FROM
                $wpdb->postmeta AS pm
             LEFT JOIN
                $wpdb->posts AS p 
             ON 
                (pm.post_id = p.ID)
             WHERE 
                pm.meta_key = %s AND
                pm.meta_value = %s
             GROUP BY p.post_author",
            'product',
            $product_id
        );

        $product_owner_ids = $wpdb->get_col($product_owner_ids_sql);

        return array_unique(array_values(array_filter(array_merge($user_ids, $product_owner_ids))));
    }

    public function getCount()
    {
        global $wpdb;

        $sql = $wpdb->prepare("SELECT DISTINCT COUNT($wpdb->users.ID) FROM $wpdb->users LEFT JOIN $wpdb->usermeta ON ($wpdb->users.ID = $wpdb->usermeta.user_id) WHERE $wpdb->usermeta.meta_key = %s OR $wpdb->usermeta.meta_key = %s", 'products', 'zippy_permissions');

        


        return $wpdb->get_var($sql);
    }

    public function getIpAddresses(ZippyCourses_Student $student)
    {
        return array_filter((array) get_user_meta($student->getId(), '_zippy_ip_addresses', true));
    }

    public function getIpAddressesSortedByDate(ZippyCourses_Student $student)
    {
        $ip_addresses = $this->getIpAddresses($student);

        usort($ip_addresses, array($this, '_sortByTimestamp'));

        return $ip_addresses;
    }

    public function _sortByTimestamp($a, $b)
    {
        if ($a['time'] == $b['time']) {
            return 0;
        }
        
        return ($a['time'] < $b['time']) ? -1 : 1;
    }
    public function getStudentsWithNoCourses()
    {
        global $wpdb;
        $all_users = $this->getAllUsers();
        $users_with_products = $this->getStudentIDs();

        foreach ($all_users as $key => $user_id) {
            if (in_array($user_id, $users_with_products)) {
                unset($all_users[$key]);
            }
        }
        return $all_users;
    }
    
    public function getAllUsers()
    {
        global $wpdb;
        $sql = "SELECT ID from $wpdb->users";
        return $wpdb->get_col($sql);
    }
}
