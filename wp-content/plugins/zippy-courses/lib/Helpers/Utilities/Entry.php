<?php

class Zippy_Entry_Utilities
{
    public function getAllProtectedEntryIds($flat = true)
    {
        $zippy = Zippy::instance();

        $content_ids = array();
        
        $entry_ids      = $this->getAllEntryIds($flat);
        $additional_ids = $this->getAllAdditionalContentIds($flat);

        $content_ids = $flat
            ? $zippy->utilities->array->merge($entry_ids, $additional_ids)
            : $zippy->utilities->array->mergeMultidimensional($entry_ids, $additional_ids);

        return $content_ids;
    }

    public function getNumberOfUnits()
    {
        global $wpdb;

        return (int) $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = 'unit'");
    }

    public function getNumberOfEntries()
    {
        global $wpdb;

        $list = implode(',', $this->getAllEntryIds(true));
        $sql = "SELECT COUNT(*) FROM $wpdb->posts WHERE ID IN ($list) AND post_type != 'unit'";

        return (int) $wpdb->get_var($sql);
    }

    /**
     * Get the ID of the next entry
     *
     * @todo   Move student-quiz-access check to another location
     *
     * @since  1.0.0
     * @param  [type] $entry_id [description]
     * @return [type]           [description]
     */
    public function getNextEntryId($entry_id)
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        $course_id = $this->getCourseId($entry_id);
        $course    = $course_id ? $student->getCourse($course_id) : null;

        $next_id   = null;

        if ($course !== null) {
            $next = $course->entries->getNextEntry($entry_id);

            $middleware = $zippy->middleware->get('student-access');

            $next_id = $next !== null && ($middleware->run($next) || current_user_can('edit_others_posts'))? $next->getId() : null;
        }

        return $next_id;
    }
    public function getNextUncompletedEntryId($entry_id)
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        $course_id = $this->getCourseId($entry_id);
        $course    = $course_id ? $student->getCourse($course_id) : null;

        $next_id   = null;

        if ($course !== null) {
            $next = $course->entries->getNextEntry($entry_id);

            $middleware = $zippy->middleware->get('student-access-exclude-completion');

            $next_id = $next !== null && ($middleware->run($next) || current_user_can('edit_others_posts'))? $next->getId() : null;
        }

        return $next_id;
    }


    public function getPreviousEntryId($entry_id)
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        $course_id = $this->getCourseId($entry_id);
        $course    = $course_id ? $student->getCourse($course_id) : null;

        $previous_id   = null;

        if ($course !== null) {
            $previous = $course->entries->getPreviousEntry($entry_id);
            $previous_id = $previous !== null ? $previous->getId() : null;
        }

        return $previous_id;
    }

    public function getAllAdditionalContentIds($flat = true)
    {
        $entry_ids = $this->_getEntryIds('additional_content');

        if ($flat && is_array($entry_ids)) {
            $flattened = array();

            foreach ($entry_ids as $course_entry_ids) {
                $flattened = array_merge($flattened, $course_entry_ids);
            }

            $entry_ids = array_unique($flattened);
        }

        return $entry_ids;
    }

    public function getAllEntryIds($flat = true)
    {
        $entry_ids = $this->_getEntryIds('entries', 'entry');

        if ($flat && is_array($entry_ids)) {
            $flattened = array();

            foreach ($entry_ids as $course_entry_ids) {
                $flattened = array_merge($flattened, $course_entry_ids);
            }

            $entry_ids = array_unique($flattened);
        }

        return $entry_ids;
    }

    public function getAllEntries()
    {
        $query      = $this->_getAllEntriesQuery($meta_key);
        $entries    = $query->getResults();

        return $this->_processEntries($this->filterEntries($entries));
    }

    private function _getEntryIds($meta_key, $cache_id = '')
    {
        $zippy      = Zippy::instance();
        $cache_id   = !empty($cache_id) ? $cache_id : $meta_key;

        if (($entry_ids = $zippy->cache->get("zippy_all_{$cache_id}_ids")) !== null) {
            return $entry_ids;
        }

        $query      = $this->_getAllEntriesQuery($meta_key);
        $entry_ids  = $this->_processEntryIds($query->getResults());

        if ($entry_ids) {
            $zippy->cache->set("zippy_all_{$cache_id}_ids", $entry_ids);
        }

        return $entry_ids;
    }

    private function _getAllEntriesQuery($meta_key)
    {
        $zippy = Zippy::instance();

        $query_id = 'get-all-' . $meta_key;

        if (($query = $zippy->queries->get($query_id)) === null) {
            global $wpdb;

            $query = $zippy->make('query', array('id' => $query_id));
            $query->setSql(
                $wpdb->prepare(
                    "SELECT post_id AS ID, 
                            meta_value AS entries
                     FROM $wpdb->postmeta 
                     WHERE meta_key = %s",
                    $meta_key
                )
            );

            $zippy->queries->add($query);
        }

        return $query;
    }
    
    /**
     * Get the Course IDs that an entry is attached to.
     *
     * @since  1.0.0
     *
     * @param  string $entry_id [description]
     * @return int    $course_id
     */
    public function getCourseId($object_id)
    {
        $cache_key  = 'entry_' . $object_id . '_course_id';
        $zippy = Zippy::instance();

        $object_course_id = $zippy->cache->get($cache_key);

        if ($object_course_id !== null) {
            return $object_course_id;
        }

        $entries = $this->getAllEntryIds(false);

        foreach ($entries as $course_id => $entry_ids) {
            if (in_array($object_id, $entry_ids)) {
                $object_course_id = $course_id;
                break;
            }
        }

        $zippy->cache->set($cache_key, $object_course_id);

        return $object_course_id;
    }

    /**
     * Get the Course that an entry is attached to.
     *
     * @since  1.0.0
     *
     * @param  string $entry_id [description]
     * @return int    $course_id
     */
    public function getCourse($object_id)
    {
        $zippy = Zippy::instance();

        $course_id = $this->getCourseId($object_id);

        return $course_id !== null ? $zippy->make('course', array($course_id)) : null;
    }

    /**
     * Get the Course IDs that an entry is attached to.
     *
     * @todo   Reduce database calls by making this a single get_post_meta into explode :)
     *
     * @since  1.0.0
     *
     * @param  [type] $entry_id [description]
     * @return [type]           [description]
     */
    public function getCourseIds($object_id)
    {
        $zippy = Zippy::instance();

        if (($course_ids = $zippy->cache->get("entry_course_ids")) !== null) {
            if (isset($course_ids[$object_id])) {
                return $course_ids[$object_id];
            }
        } else {
            $course_ids = array();
        }

        $output = array();

        $ids = $this->getAllProtectedEntryIds(false);

        foreach ($ids as $course_id => $entry_ids) {
            if (in_array($object_id, $entry_ids)) {
                $output[] = $course_id;
            }
        }

        $course_ids[$object_id] = $output;

        $zippy->cache->set('entry_course_ids', $course_ids);
       
        return $output;
    }

    public function getCourseTiers($entry_id)
    {
        $zippy = Zippy::instance();

        global $wpdb;

        $output = array();

        $entries_query      = $this->_getAllEntriesQuery('entries');
        $entry_tiers        = $this->_processEntryTiers($entries_query->getResults());

        $ac_entries_query   = $this->_getAllEntriesQuery('additional_content');
        $ac_tiers           = $this->_processEntryTiers($ac_entries_query->getResults());

        $output = array();

        $tiers = array_merge_recursive($entry_tiers, $ac_tiers);

        // We added a _ to the beginning of the keys in _processEntryTiers, so that array_merge_recursive handled
        // it without resetting numeric keys.  This small foreach replaces them.
        foreach ($tiers as $key => $values) {
            $new_key = (int) str_replace('_', '', $key);
            $tiers[$new_key] = $values;
            unset($tiers[$key]);
        }

        return $tiers;
    }

    private function _unserializeSubentries($entry)
    {
        $zippy = Zippy::instance();
        $entry->entries = $zippy->utilities->maybeJsonDecode($entry->entries);

        return $entry;
    }

    private function _processEntries($entries)
    {
        if (!is_array($entries)) {
            return $entries;
        }

        $entries = $this->filterEntries($entries);
        $entries = array_map(array($this, 'jsonDecodeEntries'), $entries);

        return $entries;
    }

    private function _processEntryIds($entries)
    {
        if (!is_array($entries)) {
            return $entries;
        }

        $entries = $this->_processEntries($entries);
        $entries = $this->mapEntryIds($entries);

        return $entries;
    }

    private function _processEntryTiers($entries)
    {
        if (!is_array($entries)) {
            return $entries;
        }

        $entries = $this->_processEntries($entries);
        $entries = $this->mapEntryTiers($entries);

        return $entries;
    }

    private function jsonDecodeEntries($entry)
    {
        $zippy = Zippy::instance();

        $entry->entries = $zippy->utilities->maybeJsonDecode($entry->entries);
        return $entry;
    }

    private function filterEntries($entries)
    {
        $zippy = Zippy::instance();

        $invalid_post_statuses      = $zippy->utilities->getInvalidPostStatus();
        $invalid_post_status_list   = $zippy->utilities->getInvalidPostStatusList();

        foreach ($entries as $key => $entry) {
            $entry_postdata = get_post($entry);
            $post_status = $entry_postdata->post_status;

            if (in_array($post_status, $invalid_post_statuses)) {
                unset($entries[$key]);
            }
        }

        return $entries;
    }

    private function mapEntryIds(array $courses)
    {
        $output = array();

        foreach ($courses as $course) {
            $output[$course->ID] = array();

            if (isset($course->entries) && is_array($course->entries)) {
                foreach ($course->entries as $entry) {
                    $output[$course->ID][] = $entry->ID;

                    if (isset($entry->entries)) {
                        foreach ($entry->entries as $subentry) {
                            $output[$course->ID][] = $subentry->ID;
                        }
                    }
                }
            }
        }

        return $output;
    }

    private function mapEntryTiers(array $courses)
    {
        $output = array();

        foreach ($courses as $course) {
            $course_id = "_{$course->ID}";
            $output[$course_id] = array();

            foreach ($course->entries as $entry) {
                if (!isset($output[$course->ID][$entry->ID])) {
                    $output[$course_id][$entry->ID] = array();
                }

                foreach ($entry->tiers as $tier) {
                    $output[$course_id][$entry->ID][] = $tier;
                }

                if (isset($entry->entries)) {
                    foreach ($entry->entries as $subentry) {
                        if (!isset($output[$course->ID][$subentry->ID])) {
                            $output[$course_id][$subentry->ID] = array();
                        }
                        
                        foreach ($entry->tiers as $tier) {
                            $output[$course_id][$subentry->ID][] = $tier;
                        }
                    }
                }
            }
        }

        foreach ($output as $course_id => &$entries) {
            foreach ($entries as &$tiers) {
                $tiers = array_unique($tiers);
            }
        }
        
        return $output;
    }

    public function getQuiz($entry_id)
    {
        $zippy = Zippy::instance();

        $course_id = $this->getCourseId($entry_id);
        if ($course_id === null) {
            return;
        }

        $course = $zippy->make('course', array($course_id));

        $entry = $course->entries->getRecursive($entry_id);
        if ($entry === null) {
            return;
        }

        return $entry->hasQuiz() ? $entry->getQuiz() : null;
    }

    public function generateFileUid()
    {
        $zippy = Zippy::instance();

        if (($uids = $zippy->cache->get('file_uids')) === null) {
            $uids = $this->_getFileUids();
            $zippy->cache->set('file_uids', $uids);
        }

        $chars      = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
        $uid        = '';

        for ($i=0; $i < 8; $i++) {
            $uid .= $chars[rand(0, (count($chars) - 1))];
        }

        return !in_array($uid, $uids) ? $uid : $this->generateFileUid();
    }

    private function _getFileUids()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $sql = $wpdb->prepare(
            "SELECT
                meta_value
             FROM
                $wpdb->postmeta
             WHERE
                meta_key = %s",
            'downloads'
        );

        $results = $wpdb->get_col($sql);

        $uids = array();

        foreach ($results as $r) {
            $downloads = $zippy->utilities->maybeJsonDecode($r);

            if (is_array($downloads)) {
                foreach ($downloads as $download) {
                    if (isset($download->uid)) {
                        $uids[] = $download->uid;
                    }
                }
            }
        }

        return array_values(array_filter($uids));
    }

    public function getAllDownloads()
    {
        global $wpdb;

        $zippy = Zippy::instance();
        $cache_id = 'zippy_all_downloads';

        if (($downloads = $zippy->cache->get($cache_id)) !== null) {
            return $downloads;
        }

        $sql = $wpdb->prepare(
            "SELECT
                post_id,
                meta_value
             FROM
                $wpdb->postmeta
             WHERE
                meta_key = %s",
            'downloads'
        );

        $results = $wpdb->get_results($sql);

        $output = array();

        foreach ($results as $r) {
            $downloads = array_filter((array) $zippy->utilities->maybeJsonDecode($r->meta_value));

            foreach ($downloads as $download) {
                if (!isset($download->uid)) {
                    continue;
                }
                if (!isset($output[$download->uid])) {
                    $output[$download->uid] = array(
                        'entry_ids'  => array(
                                            $r->post_id
                                        ),
                        'uid'       => $download->uid,
                        'title'     => $download->title,
                        'url'       => $download->url
                    );
                } else {
                    $output[$download->uid]['entry_ids'][] = $r->post_id;
                }
                
            }
        }

        $downloads = $output;

        return $downloads;
    }

    public function getSecureDownloadUrl($uid)
    {
        return home_url('secure-download/' . $uid);
    }

    public function isCourseEntry($id)
    {
        $entry_ids = $this->getAllEntryIds();

        return in_array($id, $entry_ids);
    }

    public function getEntryUnit(Zippy_Course $course, $id)
    {
        $unit = null;

        foreach ($course->entries->all() as $entry) {
            if ($entry->getType() != 'unit') {
                continue;
            }

            foreach ($entry->entries->all() as $subentry) {
                if ($subentry->getId() == $id) {
                    $unit = $entry;
                }

                if ($unit) {
                    break;
                }
            }

            if ($unit) {
                break;
            }
        }

        return $unit;
    }

    public function getEntryUnitId(Zippy_Course $course, $id)
    {
        $unit = $this->getEntryUnit($course, $id);

        return $unit instanceof Zippy_Entry ? $unit->getId() : null;
    }

    public function getDownloadTitle($id)
    {
        global $wpdb;

        if (is_numeric($id)) {
            return get_the_title($id);
        }

        $sql = "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = 'downloads' AND meta_value LIKE '%\"uid\":\"$id\"%'";
        $result = $wpdb->get_var($sql);

        $results = array_filter((array) json_decode($result));

        foreach ($results as $download) {
            if (isset($download->uid) && $download->uid == $id) {
                return $download->title;
            }
        }

        return '';
    }

    public function getPostTypes()
    {
        $args = array(
            'public'   => true
        );

        $exclude = array('course', 'product', 'payment', 'zippy_order', 'attachment', 'transaction', 'quiz', 'quiz_question');
        $post_types = get_post_types($args, 'objects');

        $output = array();

        foreach ($post_types as $key => $post_type) {
            if (!in_array($key, $exclude)) {
                $item = new stdClass;
                    $item->post_type = $key;
                    $item->label = $post_type->label;

                $output[] = $item;
            }
        }

        return $output;
    }
}
