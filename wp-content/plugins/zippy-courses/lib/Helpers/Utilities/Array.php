<?php

class Zippy_Array_Utilities
{
    public function isAssociative(array $array)
    {
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }

    /**
     * Return the first child from a multidimensional array where a key and value pair
     * are found. Similar to underscores.js _.findWhere.
     *
     * @since 1.0.0
     *
     * @example
     *
     *     $data = array(
     *         array('ID' => 1, ...),
     *         array('ID' => 2, ...),
     *         ...
     *     );
     *
     *     $var = $zippy->utilities->findWhere($data, 'ID', 2);
     *     // returns array('ID' => 2, ...)
     *
     * @param  array    $array  A multidimensional array
     * @param  string   $key    The key to search for
     * @param  string   $value  The value to search for
     * @return array|null
     */
    public function findWhere(array $array, $key)
    {
        $result = null;

        foreach ($array as $ak => $a) {
            if ($result !== null) {
                break;
            }

            foreach ($a as $k => $v) {
                if ($k == $key && $v == $value) {
                    $result = $array[$ak];
                }
            }
        }

        return $result;
    }

    public function pluck(array $arrays, $key)
    {
        $output = array();

        foreach ($arrays as $array) {
            if (is_array($array) && isset($array[$key])) {
                $output[] = $array[$key];
            }

            if (is_object($array) && isset($array->{$key})) {
                $output[] = $array->{$key};
            }
        }

        return $output;
    }

    public function pluckByKey(array $array, $key)
    {
        return isset($array[$key]) ? $array[$key] : null;
    }

    public function flatten(array $array)
    {
        $flattened = array();

        foreach ($array as $key => $values) {
            $flattened = array_merge($flattened, $values);
        }

        return array_unique($flattened);
    }

    public function mergeMultidimensional(array $array1, array $array2)
    {
        foreach ($array2 as $key => $values) {
            if (!isset($array1[$key])) {
                $array1[$key] = array();
            }
            
            $array1[$key] = array_unique(
                array_merge(
                    $array1[$key],
                    $values
                )
            );
        }

        return $array1;
    }

    public function merge(array $array1, array $array2)
    {
        return array_unique(array_merge($array1, $array2));
    }
}
