<?php
abstract class Zippy_Dataset
{
    protected $data;

    abstract public function fetchData();
    abstract public function parseData(array $data);
    
    public function __construct()
    {
        $this->data = $this->parseData($this->fetchData());
    }

    /**
     * Gets the data items
     *
     * @since 1.0.0
     *
     * @param array $data
     *
     * @return self
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Sets the data items
     *
     * @since 1.0.0
     *
     * @param array $data
     *
     * @return self
     */
    public function setData(array $data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Return a key => value array of data headers based on the structure of a data item
     *
     * @since   1.0.0
     *
     * @return  array($key => $parsed_data_name)
     */
    public function getDataHeaders()
    {
        $item = reset($this->data);

        $keys = array_keys($item);
        $output = array();
        foreach ($keys as $key) {
            $output[$key] = $this->parseName($key);
        }

        return $output;
    }

    /**
     * Receive an array key and return a capitalized column name
     *
     * @since 1.0.0
     *
     * @param  string $key
     *
     * @return string
     */
    private function parseName($key)
    {
        return ucwords(str_replace(array('-', '_'), ' ', $key));
    }

    public function toJSON()
    {
        return json_encode($this->getData());
    }
}
