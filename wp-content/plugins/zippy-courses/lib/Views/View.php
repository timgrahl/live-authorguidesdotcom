<?php

abstract class Zippy_View
{
    abstract public function render();

    public function show()
    {
        echo $this->render();
    }
}
