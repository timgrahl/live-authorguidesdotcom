<?php

class Zippy_SettingsPanelsView extends Zippy_RepositoryView
{
    private $panels;

    public function __construct(Zippy_Repository $panels)
    {
        parent::__construct($panels);
        $this->panels = &$this->repository;
    }

    public function render()
    {
        return $this->wrap($this->renderPanels() . $this->renderItems());
    }

    public function show()
    {
        echo $this->render();
    }

    protected function renderPanels()
    {
        $zippy = Zippy::instance();

        if ($this->panels->count() < 2) {
            return;
        }

        $current_id = $this->getCurrentId();
        
        $output = '<div class="zippy-panels-list zippy-settings-panels-list"><ul>';

        foreach ($this->panels->all() as $panel) {
            $item_view = $zippy->make('settings_panel_view', array($panel));

            if ($panel->getId() == $current_id) {
                $item_view->getPanel()->setCurrent(true);
            }

            $output .= $item_view->renderPanel();
        }
        $output .= '</ul></div>';

        return $output;
    }

    protected function renderItems()
    {
        $zippy = Zippy::instance();

        $output = '<div class="' . $this->getPanelsClasses() . '">';
        $current_id = $this->getCurrentId();
        
        foreach ($this->repository->all() as $item) {
            $view_class = get_class($item) . 'View';
            if (class_exists($view_class)) {
                $item_view = $zippy->make($view_class, array($item));

                if ($item->getId() == $current_id) {
                    $item_view->getPanel()->current = true;
                }

                $output .= $item_view->render();
            } else {
                $output .= $view_class . ' does not exist.<br/>';
            }
        }

        $output .= '</div>';

        return $output;
    }

    private function wrap($input)
    {
        $wrap = '<form method="POST" action="options.php" class="zippy-panels-container">';
        $wrap .= $this->getSettings_FormFields();
        $wrap .= $input;
        // $wrap .= $this->getSubmitButton();
        $wrap .= '</form>';

        return $wrap;
    }

    private function getSubmitButton()
    {
        ob_start();
        submit_button();
        return ob_get_clean();
    }
    private function getSettings_FormFields()
    {
        ob_start();

        settings_fields($this->panels->getPage());

        return ob_get_clean();
    }

    public function setPanels(Zippy_Repository $panels)
    {
        $this->items = $panels;
        $this->panels = &$this->items;
    }

    public function getCurrentId()
    {
        $current_panel = isset($_GET['panel']) ? $_GET['panel'] : false;

        return $current_panel && $this->panels->has($current_panel) ? $current_panel : $this->panels->first()->getId();
    }

    public function getPanelsClasses()
    {
        $classes = "zippy-panels zippy-settings-panels";
        return $this->panels->count() > 1 ? $classes : $classes . ' zippy-panels-single-panel';
    }
}
