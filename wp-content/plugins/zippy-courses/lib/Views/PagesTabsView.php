<?php

class Zippy_Pages_TabsView extends Zippy_RepositoryView
{
    private $tabs;

    public function __construct(Zippy_Repository $tabs)
    {
        parent::__construct($tabs);
        $this->tabs = &$this->repository;
    }

    public function render()
    {
        return $this->wrap($this->renderTabs() . $this->renderItems());
    }

    protected function renderTabs()
    {
        $zippy = Zippy::instance();

        $current_tab_id = $this->getCurrentTabId();

        $output = '<div class="zippy-tabs-list"><ul>';
        foreach ($this->tabs->all() as $tab) {
            $item_view = $zippy->make('pages_tab_view', array($tab));

            if ($current_tab_id == $tab->getId()) {
                $item_view->getTab()->setCurrent(true);
            }
            
            $output .= $item_view->renderTab();
        }
        $output .= '</ul></div>';

        return $output;
    }

    protected function renderItems()
    {
        $zippy = Zippy::instance();

        $current_tab_id = $this->getCurrentTabId();
        $item = $this->tabs->get($current_tab_id);
        
        $output = '<div class="zippy-tabs">';
        if ($item) {
            $item_view = $zippy->make('pages_tab_view', array($item));
            $output .= '<div class="zippy-tab" data-tab-id="' . $item->getId() . '">';
            $output .= $item_view->render();
            $output .= '</div>';
        }

        $output .= '</div>';

        return $output;
    }

    private function wrap($input)
    {
        return '<div class="zippy-tabs-container">' . $input . '</div>';
    }

    public function setTabs(Zippy_Repository $tabs)
    {
        $this->items = $tabs;
        $this->tabs = &$this->items;
    }

    public function getCurrentTabId()
    {
        $current_tab = isset($_GET['tab']) ? $_GET['tab'] : false;

        return $current_tab && $this->tabs->has($current_tab) ? $current_tab : $this->tabs->first()->getId();
    }
}
