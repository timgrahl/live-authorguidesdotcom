<?php

class Zippy_FileView
{
    public $file;
    public $post_id;

    public function __construct($post_id, $file)
    {
        $this->post_id = $post_id;
        $this->file = $file;
    }

    public function render()
    {
        global $post;

        $meta = get_post_meta($this->post_id);

        include $this->file;
    }
}
