<?php

class Zippy_FormView extends Zippy_View
{
    private $form;

    public function __construct(Zippy_Form $form)
    {
        $this->form = $form;
    }

    public function renderFields()
    {
        $zippy = Zippy::instance();

        $output = '';
        $fields = $this->form->getFields()->all();

        foreach ($fields as $field) {
            $field_view = $zippy->make('field_view', array($field));
            $output .= $field_view->render();
        }

        return $output;
    }

    public function checkForFileField()
    {
        $has_field = false;

        foreach ($this->form->fields->all() as $field) {
            if ($field->getType() == 'file') {
                $has_field = true;
            }

            if ($has_field) {
                break;
            }
        }

        return $has_field;
    }

    public function renderOpenTag()
    {
        $has_file_field = $this->checkForFileField();

        if ($has_file_field) {
            $this->form->addHiddenField('MAX_FILE_SIZE')
                ->setValue((5*1024*2014));
        }

        return '<form class="' . $this->renderClasses() . '" action="' . $this->form->getAction() . '" method="' . $this->form->getMethod() . '" ' . $this->renderEnctype() . '>';
    }

    public function renderEnctype()
    {
        $has_file_field = $this->checkForFileField();
        
        return $has_file_field ? ' enctype="multipart/form-data" ' : '';
    }

    public function renderClasses()
    {
        return implode(' ', $this->form->getClasses());
    }

    public function renderCloseTag()
    {
        return '</form>';
    }

    public function renderSubmit()
    {
        $classes    = $this->form->getSubmitClasses();
        $class_list = implode(' ', $classes);

        return '<div class="zippy-input zippy-input-submit">' .
            '<input type="submit" class="' . $class_list . '" value="' . $this->form->getSubmitText() . '" />' .
        '</div>';
    }

    public function renderBoilerplate()
    {
        return '<input type="hidden" name="zippy_form" value="' . $this->form->getId() . '">';
    }

    public function render()
    {
        return $this->renderBefore() .
        $this->renderOpenTag() .
        $this->renderTop() .
        $this->renderFields() .
        $this->renderBoilerplate() .
        $this->renderSubmit() .
        $this->renderBottom() .
        $this->renderCloseTag() .
        $this->renderAfter();
    }

    public function show()
    {
        echo $this->render();
    }

    public function renderTop()
    {
        return $this->form->getTop();
    }

    public function renderBottom()
    {
        return $this->form->getBottom();
    }

    public function renderAfter()
    {
        return $this->form->getAfter();
    }

    public function renderBefore()
    {
        return $this->form->getBefore();
    }
}
