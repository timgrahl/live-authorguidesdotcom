<?php

class Zippy_FileView extends Zippy_View
{
    public $file;
    public $vars;
    public $data;

    public function __construct($file, array $vars = array(), array $data = array())
    {
        $this->file     = $file;
        $this->vars     = $vars;
        $this->data     = $data;
    }

    public function render()
    {
        global $post;

        $zippy = Zippy::instance();

        $data = get_file_data($this->file, $this->getDefaultHeaders());

        if ($post) {
            foreach ($this->getVars() as $var) {
                $data_item = $zippy->make('data', array('field_data' => trim($var)));
                ${$data_item->getField()} = $data_item->fetch($post->ID);
            }
        }

        ob_start();
        
        include($this->file);

        $output = $this->appendData(ob_get_clean());

        if (isset($data['type']) && $data['type'] == 'email_integration') {
            $output .= '<div class="email-integration-data" data-service="' . $data['service'] . '"></div>';
        }

        return $output;
    }

    private function getDefaultHeaders()
    {
        return array(
            'id' => 'Id',
            'title' => 'Name',
            'help' => 'Help',
            'vars' => 'Fields',
            'data' => 'Data',
            'context' => 'Context',
            'priority' => 'Priority',
            'order' => 'Order',
            'post_types' => 'Post Types',
            'type' => 'Type',
            'service' => 'Service',
            'version' => 'Version'
        );
    }

    public function getVars()
    {
        if ($this->vars === null) {
            return array();
        }

        return is_array($this->vars) ? $this->vars : explode(',', $this->vars);
    }

    public function getData()
    {
        if ($this->data === null) {
            return array();
        }

        return is_array($this->data) ? $this->data : explode(',', $this->data);
    }

    public function appendData($html)
    {
        global $post;

        $zippy = Zippy::instance();

        $data_fields = $this->getData();
        
        if (count($data_fields)) {
            $data = array();
            foreach ($this->getData() as $var) {
                $data_item = $zippy->make('data', array('field_data' => trim($var)));

                $value = $data_item->fetch($post->ID);

                $json = is_string($value) ? json_decode($value) : null;

                if ($json !== null) {
                    $value = $json;
                }

                $data[$data_item->getField()] = $value;
            }
            
            $html .= '<script type="text/json" class="zippy-mb-data">' . json_encode($data) . '</script>';
        }
        
        return $html;
    }
}
