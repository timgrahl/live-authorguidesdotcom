<?php

class Zippy_TabsView extends Zippy_RepositoryView
{
    private $tabs;

    public function __construct(Zippy_Repository $tabs)
    {
        parent::__construct($tabs);
        $this->tabs = &$this->repository;
    }

    public function render()
    {
        return $this->wrap($this->renderTabs() . $this->renderItems());
    }

    protected function renderTabs()
    {
        $output = '<div class="zippy-tabs-list"><ul>';
        foreach ($this->tabs->all() as $tab) {
            $output .= '<li data-tab-id="' . $tab->getId() . '">' . $tab->getTitle() . '</li>';
        }
        $output .= '</ul></div>';

        return $output;
    }

    protected function renderItems()
    {
        $zippy = Zippy::instance();

        $output = '<div class="zippy-tabs">';

        foreach ($this->repository->all() as $item) {
            $output .= '<div class="zippy-tab" data-tab-id="' . $item->getId() . '">';
            
            $view_class = get_class($item) . 'View';
            if (class_exists($view_class)) {
                $item_view = $zippy->make($view_class, array($item));
                $output .= $item_view->render();
            } else {
                $output .= $view_class . ' does not exist.<br/>';
            }

            $output .= '</div>';
        }

        $output .= '</div>';

        return $output;
    }

    private function wrap($input)
    {
        return '<div class="zippy-tabs-container">' . $input . '</div>';
    }
}
