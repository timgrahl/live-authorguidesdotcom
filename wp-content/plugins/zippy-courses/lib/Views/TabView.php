<?php

class Zippy_TabView extends Zippy_View
{
    private $tab;
    private $path;

    public function __construct(Zippy_Tab $tab)
    {
        $this->path = ZippyCourses::$path . 'assets/views/tabs/';
        $this->tab = $tab;
    }

    public function renderTab()
    {
        return '<li>' . $this->tab->getTitle() . '</li>';
    }

    public function render()
    {
        if ($this->tab->getContent()) {
            return $this->tab->getContent();
        }

        $file = $this->path . $this->tab->getId() . '.php';
        if (file_exists($file) && is_readable($file)) {
            $zippy = Zippy::instance();
            $file_view = $zippy->make('file_view', array('file' => $file));
            return $file_view->render();
        }

        return;
    }
}
