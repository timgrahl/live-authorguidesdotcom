<?php

class Zippy_Pages_TabView extends Zippy_View
{
    private $tab;
    private $path;

    public function __construct(Zippy_Tab $tab)
    {
        $this->path = ZippyCourses::$path . 'assets/views/tabs/';
        $this->tab = $tab;
    }

    public function renderTab()
    {
        $url = $this->getTabUrl();

        return $this->tab->getCurrent()
                ? '<li class="active"><a href="' . $url . '">' . $this->tab->getTitle() . '</a></li>'
                : '<li><a href="' . $url . '">' . $this->tab->getTitle() . '</a></li>';
    }

    public function render()
    {
        return $this->tab->view instanceof Zippy_View ? $this->tab->view->render() : '';
    }

    public function getTab()
    {
        return $this->tab;
    }

    public function getTabUrl()
    {
        $screen = get_current_screen();

        $page = filter_input(INPUT_GET, 'page');

        return admin_url('admin.php?page=' . $page . '&tab=' . $this->tab->getId());
    }
}
