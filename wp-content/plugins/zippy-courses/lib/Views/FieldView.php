<?php

class Zippy_FormFieldView extends Zippy_View
{
    public $field;

    public function __construct(Zippy_FormField $field)
    {
        $this->field = $field;

    }

    public function render()
    {
        $output = '';

        switch ($this->field->getType()) {
            case 'hidden':
                $output .= '<input type="' . $this->field->getType() . '" name="' . $this->field->getName() . '"' . $this->renderValue() . $this->renderJqueryValidationRules() . '/>';
                break;
            case 'select':
                $output .= '<div ' . $this->renderContainerClasses() . '>';
                $output .= '<p>' . $this->renderLabel() . '<select' . $this->renderId() . $this->renderName() . $this->renderJqueryValidationRules() . $this->renderAttributes() . $this->renderData() . $this->renderClass() . '>';
                $output .= $this->renderSelectOptions();
                $output .= '</select></p></div>';
                break;
            case 'checkbox':
                $output .= $this->renderCheckbox();
                break;
            case 'radio':
                $output .= $this->renderRadio();
                break;
            case 'textarea':
                $output .= '<div ' . $this->renderContainerClasses() . '>';
                $output .= '<p>' . $this->renderLabel() . '<textarea ' . $this->renderId() . $this->renderName() . $this->renderJqueryValidationRules() . $this->renderAttributes() . $this->renderPlaceholder() . $this->renderData() . $this->renderClass() . '>' . $this->renderValue(true) . '</textarea></p>';
                $output .= '</div>';
                break;
            case 'datepicker':
                $output .= '<div ' . $this->renderContainerClasses() . '>';
                $output .= '<p>' . $this->renderLabel() . '<input type="' . $this->field->getType() . '" ' . $this->renderId() . $this->renderName() . $this->renderValue() . $this->renderJqueryValidationRules() . $this->renderAttributes() . $this->renderPlaceholder() . $this->renderData() . $this->renderClass() . ' /></p>';
                $output .= '</div>';
                break;
            case 'html':
                $output .= $this->renderValue(true);
                break;
            
            default:
                $output .= '<div ' . $this->renderContainerClasses() . '>';
                $output .= '<p>' . $this->renderLabel() . '<input type="' . $this->field->getType() . '" ' . $this->renderId() . $this->renderName() . $this->renderValue() . $this->renderJqueryValidationRules() . $this->renderAttributes() . $this->renderPlaceholder() . $this->renderData() . $this->renderClass() . ' /></p>';
                $output .= '</div>';
                break;
        }

        return $output;
    }

    public function renderName()
    {
        if ($this->field->getNameVisibility()) {
            if ($this->field->getType() == 'checkbox') {
                return ' name="' . $this->field->getName() . '[]" ';
            } else {
                return ' name="' . $this->field->getName() . '" ';
            }
        }

        return;
    }

    public function renderValue($unformatted = false)
    {
        if ($this->field->showsPostedValue()) {
            if ($unformatted) {
                return $this->field->getValue(true);
            } else {
                return ' value="' . $this->field->getValue(true) . '" ';
            }
        }

        return ' value="" ';
    }

    public function renderLabel()
    {
        if ($this->field->getLabelVisibility()) {
            return '<label>' . $this->field->getLabel() . '</label>';
        }

        return '';
    }

    public function renderPlaceholder()
    {
        return ' placeholder="' . $this->field->getPlaceholder() . '" ';
    }

    public function renderAttributes()
    {
        $output = '';

        foreach ($this->field->getAttributes() as $key => $value) {
            $output .= $key . '="' . $value . '" ';
        }

        return $output;
    }

    public function renderData()
    {
        $output = '';

        foreach ($this->field->getData() as $key => $value) {
            $output .= 'data-' . $key . '="' . $value . '" ';
        }

        return $output;
    }

    public function renderClass()
    {
        return ' class="zippy-field zippy-field-' . $this->field->getType() . '" ';
    }

    public function renderJqueryValidationRules()
    {
        $rules = $this->field->getRules();

        if (empty($rules)) {
            return;
        }

        $valid = array('required', 'email', 'minlength', 'unique_email', 'unique_username', 'equalTo');
        $output = new stdClass();

        foreach ($rules as $rule => $data) {
            if (in_array($rule, $valid)) {
                $output->{$rule} = true;

                if (count($data['args']) == 1) {
                    switch ($rule) {
                        case 'equalTo':
                            $output->{$rule} = '#zippy-field-' . $data['args'][0];
                            break;
                        default:
                            $output->{$rule} = is_numeric($data['args'][0]) ? (int) $data['args'][0] : $data['args'][0];
                            break;
                    }
                }
            }
        }

        return ' data-rules=\'' . json_encode($output) . '\' ';
    }

    public function renderId()
    {
        return ' id="zippy-field-' . $this->field->getId() . '" ';
    }

    public function renderContainerClasses()
    {
        $classes = array('zippy-input', 'zippy-input-' . $this->field->getType());
        $classes = array_merge($classes, $this->field->getContainerClasses());

        return ' class="' . implode(' ', $classes) . '" ';
    }

    public function renderSelectOptions()
    {
        $output = '';

        foreach ($this->field->options as $key => $value) {
            $output .= '<option value="' . $key . '" ' . selected($key, $this->field->getValue(), false) . '>' . $value . '</option>';
        }

        return $output;
    }

    public function renderCheckbox()
    {
        $output = '';
        $output .= '<ul>';
        foreach ($this->field->options as $key => $value) {
            $checked = is_array($this->field->getValue()) && in_array($key, $this->field->getValue()) ? ' checked="checked" ' : '';
            $output .= '<li><label><input type="checkbox"' . $this->renderId() . $this->renderName() . $this->renderJqueryValidationRules() . $this->renderAttributes() . $this->renderData() . $this->renderClass() . ' value="' . $key . '"' . $checked . '>' . $value . '</label></li>';
        }
        $output .= '</ul>';

        return $output;
    }

    public function renderRadio()
    {
        $output = '';
        $output .= '<ul>';
        foreach ($this->field->options as $key => $value) {
            $checked = is_array($this->field->getValue()) && in_array($key, $this->field->getValue()) ? ' checked="checked" ' : '';
            $output .= '<li><label><input type="radio"' . $this->renderId() . $this->renderName() . $this->renderJqueryValidationRules() . $this->renderAttributes() . $this->renderData() . $this->renderClass() . $checked . '>' . $value . '</label></li>';
        }
        $output .= '</ul>';

        return $output;
    }
}
