<?php

abstract class Zippy_RepositoryView extends Zippy_View
{
    public $repository;

    public function __construct(Zippy_Repository $repository)
    {
        $this->repository = $repository;
    }

    abstract protected function renderItems();
}
