<?php

class Zippy_PanelView extends Zippy_View
{
    private $panel;
    private $path;

    public function __construct(Zippy_Panel $panel)
    {
        $this->path = ZippyCourses::$path . 'assets/views/panels/';
        $this->panel = $panel;
    }

    public function renderPanel()
    {
        return $this->panel->getCurrent()
            ? '<li><a href="#" data-panel-id="' . $this->panel->getId() . '" class="active">' . $this->panel->getTitle() . '</a></li>'
            : '<li><a href="#" data-panel-id="' . $this->panel->getId() . '">' . $this->panel->getTitle() . '</a></li>';
    }

    public function render()
    {
        $classes = 'zippy-panel';
        $classes = $this->panel->getCurrent() ? $classes . ' active' : $classes;

        $output = '<div class="' . $classes . '" data-panel-id="' . $this->panel->getId() . '">';
        $output .= $this->panel->view->render();
        $output .= '</div>';
        
        return $output;
    }

    public function getPanel()
    {
        return $this->panel;
    }
}
