<?php

class Zippy_PanelsView extends Zippy_RepositoryView
{
    private $panels;

    public function __construct(Zippy_Repository $panels)
    {
        parent::__construct($panels);
        $this->panels = &$this->repository;
    }

    public function render()
    {
        return $this->wrap($this->renderPanels() . $this->renderItems());
    }
    
    protected function renderPanels()
    {
        $zippy = Zippy::instance();

        if ($this->panels->count() < 2) {
            return;
        }

        $current_id = $this->getCurrentId();
        
        $output = '<div class="zippy-panels-list"><ul>';

        foreach ($this->panels->all() as $panel) {
            $item_view = $zippy->make('panel_view', array($panel));

            if ($panel->getId() == $current_id) {
                $item_view->getPanel()->setCurrent(true);
            }

            $output .= $item_view->renderPanel();
        }
        $output .= '</ul></div>';

        return $output;
    }

    protected function renderItems()
    {
        $zippy = Zippy::instance();

        $output = '<div class="' . $this->getPanelsClasses() . '">';
        $current_id = $this->getCurrentId();
        
        foreach ($this->repository->all() as $item) {
            $view_class = get_class($item) . 'View';
            if (class_exists($view_class)) {
                $item_view = $zippy->make($view_class, array($item));

                if ($item->getId() == $current_id) {
                    $item_view->getPanel()->current = true;
                }

                $output .= $item_view->render();
            } else {
                $output .= $view_class . ' does not exist.<br/>';
            }
        }

        $output .= '</div>';

        return $output;
    }

    private function wrap($input)
    {
        return '<div class="zippy-panels-container">' . $input . '</div>';
    }

    public function getCurrentId()
    {
        $current_panel = isset($_GET['panel']) ? $_GET['panel'] : false;

        return $current_panel && $this->panels->has($current_panel) ? $current_panel : $this->panels->first()->getId();
    }

    public function getPanelsClasses()
    {
        $classes = "zippy-panels";
        return $this->panels->count() > 1 ? $classes : $classes . ' zippy-panels-single-panel';
    }
}
