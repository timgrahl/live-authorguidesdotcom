<?php

class Zippy_PostTypes extends Zippy_Repository
{
    private $app;

    private static $instance;

    private function __construct()
    {
        $this->addValidType('Zippy_PostType');
        add_action('init', array($this, 'register'));
    }

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function register()
    {
        foreach ($this->all() as $cpt) {
            $cpt->register();
        }
    }
}
