<?php

class Zippy_PostType extends Zippy_Model
{
    public $id;
    public $singular_name;
    public $plural_name;
    public $supports;
    public $capability_type = 'post';
    
    public $exclude_from_search = false;
    public $publicly_queryable = true;
    public $rewrite = true;
    public $public = true;
    public $has_archive = true;

    /**
     * Table Columns
     * @var Zippy_ListTableColumns
     */
    public $table_columns;

    /**
     * Table Filters
     * @var Zippy_ListTableFilters
     */
    public $table_filters;

    // Defaults1
    protected $show_in_menu = true;

    public function __construct($id, $singular_name, $plural_name = '')
    {
        $zippy = Zippy::instance();

        $this->id               = $id;
        $this->singular_name    = $singular_name;
        $this->plural_name      = $plural_name;

        $this->table_columns    = $zippy->make('list_table_columns');
        $this->table_filters    = $zippy->make('list_table_filters');
    }

    public function register()
    {
        $labels = array(
            'name'                => $this->getPluralName(),
            'singular_name'       => $this->getSingularName(),
            'add_new'             => __('Add New ', ZippyCourses::TEXTDOMAIN) . $this->getSingularName(),
            'add_new_item'        => __('Add New ', ZippyCourses::TEXTDOMAIN) . $this->getSingularName(),
            'edit_item'           => __('Edit ', ZippyCourses::TEXTDOMAIN) . $this->getSingularName(),
            'new_item'            => __('New ', ZippyCourses::TEXTDOMAIN) . $this->getSingularName(),
            'view_item'           => __('View ', ZippyCourses::TEXTDOMAIN) . $this->getSingularName(),
            'search_items'        => __('Search ', ZippyCourses::TEXTDOMAIN) . $this->getPluralName(),
            'not_found'           => __('No ', ZippyCourses::TEXTDOMAIN) . $this->getSingularName() . __(' found', ZippyCourses::TEXTDOMAIN),
            'not_found_in_trash'  => __('No ', ZippyCourses::TEXTDOMAIN) . $this->getSingularName() . __(' found in trash', ZippyCourses::TEXTDOMAIN),
            'parent_item_colon'   => __('Parent ', ZippyCourses::TEXTDOMAIN) . $this->getSingularName() . ':',
            'menu_name'           => $this->getPluralName()
        );
    
        $args = array(
            'labels'              => $labels,
            'public'              => $this->getPublic(),
            'show_ui'             => true,
            'show_in_menu'        => $this->show_in_menu,
            'show_in_admin_bar'   => true,
            'menu_position'       => 50,
            'show_in_nav_menus'   => true,
            'publicly_queryable'  => $this->getPubliclyQueryable(),
            'exclude_from_search' => $this->getExcludeFromSearch(),
            'has_archive'         => $this->getHasArchive(),
            'query_var'           => true,
            'can_export'          => true,
            'rewrite'             => $this->getRewrite(),
            'capability_type'     => $this->getCapabilityType(),
            'supports'            => $this->supports
        );
        
        register_post_type($this->id, $args);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSingularName()
    {
        return $this->singular_name;
    }

    public function getPluralName()
    {
        if ($this->plural_name == '') {
            $this->plural_name = $this->singular_name . 's';
        }

        return $this->plural_name;
    }

    public function setParentMenu($parent)
    {
        $this->show_in_menu = $parent;
        return $this;
    }

    public function setParentPostType($post_type)
    {
        return $this->setParentMenu('edit.php?post_type=' . $post_type);
    }

    public function setSupports(array $supports)
    {
        $this->supports = $supports;
        return $this;
    }

    public function addTableColumn(Zippy_ListTableColumn $column)
    {
        $column->addPostType($this->getId());
        $this->table_columns->add($column);

        return $this;
    }
    
    public function addTableFilter(Zippy_ListTableFilter $filter)
    {
        $filter->addPostType($this->getId());
        $this->table_filters->add($filter);

        return $this;
    }

    /**
     * Sets the value of id.
     *
     * @param mixed $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Sets the value of singular_name.
     *
     * @param mixed $singular_name the singular name
     *
     * @return self
     */
    public function setSingularName($singular_name)
    {
        $this->singular_name = $singular_name;

        return $this;
    }

    /**
     * Sets the value of plural_name.
     *
     * @param mixed $plural_name the plural name
     *
     * @return self
     */
    public function setPluralName($plural_name)
    {
        $this->plural_name = $plural_name;

        return $this;
    }

    /**
     * Gets the value of supports.
     *
     * @return mixed
     */
    public function getSupports()
    {
        return $this->supports;
    }

    /**
     * Gets the value of exclude_from_search.
     *
     * @return mixed
     */
    public function getExcludeFromSearch()
    {
        return $this->exclude_from_search;
    }

    /**
     * Sets the value of exclude_from_search.
     *
     * @param mixed $exclude_from_search the exclude from search
     *
     * @return self
     */
    public function setExcludeFromSearch($exclude_from_search)
    {
        $this->exclude_from_search = $exclude_from_search;

        return $this;
    }

    /**
     * Gets the value of publicly_queryable.
     *
     * @return mixed
     */
    public function getPubliclyQueryable()
    {
        return $this->publicly_queryable;
    }

    /**
     * Sets the value of publicly_queryable.
     *
     * @param mixed $publicly_queryable the publicly queryable
     *
     * @return self
     */
    public function setPubliclyQueryable($publicly_queryable)
    {
        $this->publicly_queryable = $publicly_queryable;

        return $this;
    }

    /**
     * Gets the value of rewrite.
     *
     * @return mixed
     */
    public function getRewrite()
    {
        return $this->rewrite === true ? $this->rewrite : array('slug' => $this->rewrite);
    }

    /**
     * Sets the value of rewrite.
     *
     * @param mixed $rewrite the rewrite
     *
     * @return self
     */
    public function setRewrite($rewrite)
    {
        $this->rewrite = $rewrite;

        return $this;
    }

    /**
     * Gets the Table Columns.
     *
     * @return Zippy_ListTableColumns
     */
    public function getTableColumns()
    {
        return $this->table_columns;
    }

    /**
     * Sets the Table Columns.
     *
     * @param Zippy_ListTableColumns $table_columns the table columns
     *
     * @return self
     */
    public function setTableColumns(Zippy_ListTableColumns $table_columns)
    {
        $this->table_columns = $table_columns;

        return $this;
    }

    /**
     * Gets the value of show_in_menu.
     *
     * @return mixed
     */
    public function getShowInMenu()
    {
        return $this->show_in_menu;
    }

    /**
     * Sets the value of show_in_menu.
     *
     * @param mixed $show_in_menu the show in menu
     *
     * @return self
     */
    public function setShowInMenu($show_in_menu)
    {
        $this->show_in_menu = $show_in_menu;

        return $this;
    }

    /**
     * Gets the value of public.
     *
     * @return mixed
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Sets the value of public.
     *
     * @param mixed $public the public
     *
     * @return self
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Gets the value of capability_type.
     *
     * @return mixed
     */
    public function getCapabilityType()
    {
        return $this->capability_type;
    }

    /**
     * Sets the value of capability_type.
     *
     * @param mixed $capability_type the capability type
     *
     * @return self
     */
    public function setCapabilityType($capability_type)
    {
        $this->capability_type = $capability_type;

        return $this;
    }

    /**
     * Gets the Table Filters.
     *
     * @return Zippy_ListTableFilters
     */
    public function getTableFilters()
    {
        return $this->table_filters;
    }

    /**
     * Sets the Table Filters.
     *
     * @param Zippy_ListTableFilters $table_filters the table filters
     *
     * @return self
     */
    public function setTableFilters(Zippy_ListTableFilters $table_filters)
    {
        $this->table_filters = $table_filters;

        return $this;
    }

    public function getHasArchive()
    {
        return $this->has_archive;
    }

    public function setHasArchive($has_archive)
    {
        $this->has_archive = (bool) $has_archive;

        return $this;
    }
}
