<?php

class Zippy_API
{
    const VERSION = '0.1';

    private static $instance;

    protected $endpoints = array();
    protected $errors = array();
    protected $response;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->configureDefaultEndpoints();
        $this->response = new stdClass;

        $this->hooks();
    }

    public function hooks()
    {
        add_action('template_redirect', array($this, 'listen'));
    }

    public function configureDefaultEndpoints()
    {
        $this->addEndpoint('student');
    }

    public function listen()
    {
        $endpoint = get_query_var('zippy_api_endpoint', null);
        $api_version = get_query_var('zippy_api_version', null);

        if ($endpoint !== null && $api_version !== null) {
            header('Content-Type: application/json');
            $this->handle();
            exit;
        }
    }

    public function getMethod()
    {
        return filter_input(INPUT_SERVER, 'REQUEST_METHOD');
    }

    public function handle()
    {
        if ($this->validate()) {
            $method = $this->getMethod();

            switch ($method) {
                case 'PUT':
                    break;
                case 'DELETE':
                    break;
                case 'POST':
                    $this->_handlePost();
                    break;
                case 'GET':
                default:
                    $this->_handleGet();
                    break;
            }
        } else {
            $this->response->errors = $this->errors;
        }
        
        echo json_encode($this->response);
    }

    private function _handleGet()
    {
        $zippy = Zippy::instance();

        $query = get_query_var('zippy_api_endpoint', null);

        $parameters = explode("/", $query);
        $endpoint = array_shift($parameters);
        
        $data = $_GET;
        if (isset($data['api_key'])) {
            unset($data['api_key']);
        }

        $event = new ZippyCourses_ApiRequest_Event($endpoint, 'GET', $parameters, $data);

        $zippy->events->fire($event);

        if ($event->response !== undefined) {
            $this->response = $event->response;
        }
    }

    private function _handlePost()
    {
        $zippy = Zippy::instance();

        $query = get_query_var('zippy_api_endpoint', null);

        $parameters = explode("/", $query);
        $endpoint = array_shift($parameters);
        
        $data = $_POST;
        if (empty($data)) {
            $data = parse_str(file_get_contents('php://input'));
        }

        $event = new ZippyCourses_ApiRequest_Event($endpoint, 'POST', $parameters, $data);

        $zippy->events->fire($event);

        if ($event->response !== null) {
            $this->response = $event->response;
        }
    }

    public function validate()
    {
        $key = filter_input(INPUT_GET, 'api_key');
        
        $query = get_query_var('zippy_api_endpoint', null);
        $params = explode("/", $query);
        $endpoint = $params[0];

        if (!array_key_exists($endpoint, $this->endpoints)) {
            $this->errors[] = 'E003: This resource does not exist.';
            status_header(400);

            return false;
        }

        if (empty($key)) {
            $this->errors[] = 'E001: Could not complete request. You must provide an API Key.';
            status_header(401);

            return false;
        } else {
            if (!$this->validateApiKey($key)) {
                $this->errors[] = 'E002: You must provide a valid and active API Key.';
                status_header(401);

                return false;
            }
        }

        return true;
    }

    public function validateApiKey($key)
    {
        $settings = get_option('zippy_advanced_settings', array());
        $api_key = isset($settings['api_key']) ? $settings['api_key'] :'';

        return !empty($api_key) && $key == $api_key;
    }

    public function addEndpoint($endpoint)
    {
        $this->endpoints[$endpoint] = array();
    }

    public static function generateApiKey()
    {
        $zippy = Zippy::instance();

        $chars      = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
        $api_key    = '';

        for ($i=0; $i < 24; $i++) {
            $api_key .= $chars[rand(0, (count($chars) - 1))];
        }

        return $api_key;
    }
}
