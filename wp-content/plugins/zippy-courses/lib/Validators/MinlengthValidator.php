<?php

abstract class Zippy_Validator
{
    abstract public function validate($data);
}
