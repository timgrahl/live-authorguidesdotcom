<?php

class Zippy_Validator
{
    private $rules;

    private static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->addRule('minlength', array($this, 'minlength'));
        $this->addRule('maxlength', array($this, 'maxlength'));
        $this->addRule('min', array($this, 'min'));
        $this->addRule('max', array($this, 'max'));
        $this->addRule('required', array($this, 'required'));
        $this->addRule('length_range', array($this, 'lengthRange'));
        $this->addRule('range', array($this, 'range'));
        $this->addRule('email', array($this, 'email'));
        $this->addRule('email_exists', array($this, 'emailExists'));
        $this->addRule('unique_email', array($this, 'uniqueEmail'));
        $this->addRule('username_exists', array($this, 'usernameExists'));
        $this->addRule('unique_username', array($this, 'uniqueUsername'));
        $this->addRule('equalTo', array($this, 'equalTo'));
    }

    public function minlength($input, $min = 0)
    {
        return strlen($input) >= $min;
    }

    public function emailExists($email)
    {
        global $wpdb;

        $exists = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->users WHERE user_email = %s", $email));

        return (bool) $exists;
    }

    public function uniqueEmail($email)
    {
        return !$this->emailExists($email);
    }

    public function uniqueUsername($username)
    {
        return !$this->usernameExists($username);
    }

    public function usernameExists($username)
    {
        global $wpdb;

        $exists = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->users WHERE user_login = %s", $username));

        return (bool) $exists;
    }

    public function email($input)
    {
        return filter_var($input, FILTER_VALIDATE_EMAIL);
    }

    public function maxlength($input, $max = 100)
    {
        return strlen($input) <= $max;
    }

    public function lengthRange($input, $min, $max)
    {
        return $this->minlength($input, $min) && $this->maxlength($input, $max);
    }

    public function range($input, $min, $max)
    {
        return $this->min($input, $min) && $this->max($input, $max);
    }

    public function min($input, $min = 0)
    {
        return $input >= $min;
    }

    public function max($input, $max = 100)
    {
        return $input <= $max;
    }

    public function equalTo($input, $var)
    {
        return isset($_POST[$var]) ? $input == $_POST[$var] : false;
    }

    public function required($input)
    {
        return is_string($input) 
            ? strlen(trim($input)) > 0
            : (is_object($input) || is_array($input)
                ? count($input) > 0
                : false
            );
    }

    public function addRule($id, $function)
    {
        $this->rules[$id] = $function;
        return $this;
    }

    public function validate($rule, $data, array $parameters = array())
    {
        array_unshift($parameters, $data);

        return isset($this->rules[$rule]) ? call_user_func_array($this->rules[$rule], $parameters) : true;
    }
}
