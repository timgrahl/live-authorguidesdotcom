<?php

class Zippy_Customizer
{
    private static $instance;
    public $panels;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct()
    {
        $this->panels = new Zippy_Customizer_Panels;

        $this->hooks();
    }

    public function hooks()
    {
        add_action('customize_register', array($this, 'register'));
    }

    public function register($wp_customize)
    {
        $this->panels->register($wp_customize);
    }
}
