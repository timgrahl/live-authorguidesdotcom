<?php

class Zippy_Customizer_Section extends Zippy_Model
{
    public $id;
    public $title;
    public $priority = 10;
    public $description;
    public $controls;
    
    /**
     * The panel to add this Section to
     * @var Zippy_Customizer_Panel
     */
    public $panel;

    public function __construct($id, $title = '', $priority = 10)
    {
        $zippy = Zippy::instance();

        $this->id           = $id;
        $this->title        = $title;
        $this->priority     = $priority;

        $this->controls     = $zippy->make('customizer_controls');
    }

    public function register($wp_customize)
    {
        $wp_customize->add_section(
            $this->getName(),
            array(
                'title'         => $this->getTitle(),
                'priority'      => $this->getPriority(),
                'panel'         => $this->getPanel()->getName(),
                'description'   => $this->getDescription()
            )
        );

        foreach ($this->controls->all() as $control) {
            $control->setSection($this);
            $control->register($wp_customize);
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function getSections()
    {
        return $this->sections;
    }

    public function setPanel(Zippy_Customizer_Panel $panel)
    {
        $this->panel = $panel;
        return $this;
    }

    public function getPanel()
    {
        return $this->panel;
    }

    /**
     * Gets the value of description.
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the value of description.
     *
     * @param mixed $description the description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getName()
    {
        if ($this->panel !== null) {
            return $this->getPanel()->getName() . $this->getId();
        }

        return 'customizer_' . $this->getId();
    }
}
