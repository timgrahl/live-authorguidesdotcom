<?php

class Zippy_Customizer_Panels extends Zippy_Repository
{
    public function __construct()
    {
        $this->addValidType('Zippy_Customizer_Panel');
    }

    public function register($wp_customize)
    {
        foreach ($this->all() as $panel) {
            $panel->register($wp_customize);
        }
    }
}
