<?php

class Zippy_Customizer_Panel extends Zippy_Model
{
    public $id;
    public $setting;
    public $title;
    public $description;
    public $priority = 10;
    public $sections;
    
    public function __construct($id, $setting, $title = '', $description = '', $priority = 10)
    {
        $zippy = Zippy::instance();

        $this->id           = $id;
        $this->setting      = $setting;
        $this->title        = $title;
        $this->description  = $description;
        $this->priority     = $priority;

        $this->sections     = $zippy->make('customizer_sections');
    }

    public function register($wp_customize)
    {
        $wp_customize->add_panel(
            $this->getName(),
            array(
                'title'         => $this->getTitle(),
                'priority'      => $this->getPriority(),
                'description'   => $this->getDescription()
            )
        );

        foreach ($this->sections->all() as $section) {
            $section->setPanel($this);
            $section->register($wp_customize);
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Gets the value of setting.
     *
     * @return mixed
     */
    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * Sets the value of setting.
     *
     * @param mixed $setting the setting
     *
     * @return self
     */
    public function setSetting($setting)
    {
        $this->setting = $setting;

        return $this;
    }

    public function getName()
    {
        return $this->getId() . '_customizer_';
    }
}
