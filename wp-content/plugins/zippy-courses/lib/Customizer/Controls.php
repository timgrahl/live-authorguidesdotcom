<?php

class Zippy_Customizer_Controls extends Zippy_Repository
{
    public function __construct()
    {
        $this->addValidType('Zippy_Customizer_Control');
    }

    public function register()
    {
        foreach ($this->all() as $section) {
            $section->register();
        }
    }
}
