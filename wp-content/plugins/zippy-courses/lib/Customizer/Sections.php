<?php

class Zippy_Customizer_Sections extends Zippy_Repository
{
    public function __construct()
    {
        $this->addValidType('Zippy_Customizer_Section');
    }

    public function register()
    {
        foreach ($this->all() as $section) {
            $section->register();
        }
    }
}
