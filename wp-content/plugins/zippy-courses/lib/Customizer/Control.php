<?php

class Zippy_Customizer_Control extends Zippy_Model
{
    public $id;
    public $label;
    public $type;
    public $priority = 10;
    public $active_callback;
    public $choices = array();
    
    /**
     * The section to add this Control to
     * @var Zippy_Customizer_Section
     */
    public $section;

    public function __construct($id, $label = '', $type = '', $priority = 10)
    {
        $zippy = Zippy::instance();

        $this->id           = $id;
        $this->label        = $label;
        $this->type         = $type;
        $this->priority     = $priority;
    }

    public function register($wp_customize)
    {
        $control    = null;
        $type       = $this->getType();
    
        $wp_customize->add_setting($this->getName(), array(
            'type' => 'option',
            'capability'     => 'edit_theme_options'
        ));

        switch ($this->getType()) {
            case 'text':
            case 'select':
            case 'radio':
            case 'checkbox':
                $control = $this->_getStandardControl($wp_customize);
                break;
            case 'color':
                $control = $this->_getColorControl($wp_customize);
                break;
            case 'upload':
                $control = $this->_getUploadControl($wp_customize);
                break;
            case 'image':
                $control = $this->_getImageControl($wp_customize);
                break;
            default:
                break;
        }

        if ($control !== null) {
            $wp_customize->add_control($control);
        }
    }

    public function getParameters()
    {
        $parameters = array(
            'settings'          => $this->getName(),
            'label'             => $this->getLabel(),
            'capability'        => 'edit_theme_options',
            'section'           => $this->getSection()->getName()
        );

        if ($this->getType() !== 'text') {
            $parameters['type'] = $this->getType();
        }

        $_includes_choices = array('select', 'checkbox', 'radio');
        if (in_array($this->type, $_includes_choices)) {
            $parameters['choices'] = $this->getChoices();
        }

        if ($this->active_callback !== null) {
            $parameters['active_callback'] = $this->getActiveCallback();
        }

        return $parameters;
    }

    public function _getStandardControl($wp_customize)
    {
        $control = new WP_Customize_Control(
            $wp_customize,
            $this->getName(),
            $this->getParameters()
        );

        return $control;
    }

    public function _getColorControl($wp_customize)
    {

    }

    public function _getUploadControl($wp_customize)
    {

    }

    public function _getImageControl($wp_customize)
    {

    }

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function getSection()
    {
        return $this->section;
    }

    public function setPanel(Zippy_Customizer_Panel $panel)
    {
        $this->panel = $panel;
        return $this;
    }

    public function getPanel()
    {
        return $this->panel;
    }

    /**
     * Gets the value of choices.
     *
     * @return mixed
     */
    public function getChoices()
    {
        return $this->choices;
    }

    /**
     * Sets the value of choices.
     *
     * @param mixed $choices the choices
     *
     * @return self
     */
    public function setChoices(array $choices)
    {
        $this->choices = $choices;

        return $this;
    }

    /**
     * Sets the value of choices.
     *
     * @param mixed $choices the choices
     *
     * @return self
     */
    public function addChoice($choice)
    {
        $this->choices[] = $choice;

        return $this;
    }

    /**
     * Gets the value of type.
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the value of type.
     *
     * @param mixed $type the type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Sets the The section to add this Control to.
     *
     * @param Zippy_Customizer_Section $section the section
     *
     * @return self
     */
    public function setSection(Zippy_Customizer_Section $section)
    {
        $this->section = $section;

        return $this;
    }

    public function getName()
    {
        if ($this->section !== null) {
            return $this->getSection()->getName() . '[' . $this->getId() . ']';
        }

        return 'zippy_customizer_' . $this->getId();
    }

}
