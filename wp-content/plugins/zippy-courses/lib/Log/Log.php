<?php

class Zippy_Log
{
    protected static $instance;

    const SEPARATOR = '    ';

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->setup();
    }

    public function log($message, $type = 'LOG')
    {
        if (!$this->isReady()) {
            return;
        }

        $zippy  = Zippy::instance();
        $date   = $zippy->utilities->datetime->getNow();

        $log_msg = $date->format("Y-m-d H:i:s") . self::SEPARATOR . $type . self::SEPARATOR . $message . "\r\n";

        @file_put_contents($this->getFile(), $log_msg, FILE_APPEND);
    }

    public function setup()
    {
        $upload_dir = wp_upload_dir();
        $file       = get_option('zippy_log_file', false);

        $file_ready = $file !== false ? $this->fileIsReady($file) : false;

        if (!$file_ready) {
            $dir        = $upload_dir['basedir'] . '/zippy-courses/';
            $dir_ready  = $this->directoryIsReady($dir);

            $file = $dir . 'zippy-' . $this->generateUnique() . '.log';

            if ($dir_ready) {
                $created = @file_put_contents($file, '');

                if ($created !== false) {
                    update_option('zippy_log_file', $file);
                }
            }
        }
    }

    private function getFile()
    {
        return get_option('zippy_log_file');
    }

    private function fileExists($file)
    {
        return file_exists($file);
    }

    private function fileIsReady($file)
    {
        return $file && is_readable($file) && is_writable($file);
    }

    private function directoryIsReady($dir)
    {
        $ready = wp_mkdir_p($dir);

        return $ready && is_dir($dir) && is_writable($dir);
    }

    public function generateUnique($length = 16)
    {
        $chars      = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
        $key        = '';

        for ($i=0; $i < $length; $i++) {
            $key .= $chars[rand(0, (count($chars) - 1))];
        }

        return $key;
    }

    public function isReady()
    {
        $file       = get_option('zippy_log_file', false);

        if (!$file) {
            return false;
        }

        if (!$this->fileIsReady($file)) {
            return false;
        }

        return true;
    }
}
