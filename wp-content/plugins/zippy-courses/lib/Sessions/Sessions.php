<?php

class Zippy_Sessions
{
    /**
     * Instance of self
     * @var self
     */
    private static $instance;

    private $data = array();

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->init();
    }

    private function init()
    {
        if (!session_id()) {
            do_action('zippy_courses_before_session_start');
            session_start();
        }

        if (!isset($_SESSION['zippy'])) {
            $_SESSION['zippy'] = array(
                'flash' => array()
            );
        }

        $this->data = &$_SESSION['zippy'];

        $this->decrement();
    }

    public function store($key, $data)
    {
        $this->data[$key] = $data;
    }

    public function retrieve($key)
    {
        if (!is_array($this->data)) {
            $this->data = array();
        }

        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    public function delete($key)
    {
        if (isset($this->data[$key])) {
            unset($this->data[$key]);
        }

        return $this;
    }

    public function flush()
    {
        $this->data = array();
        $this->init();

        return $this;
    }

    public function flash($key, $data, $duration = 1)
    {
        if (!is_array($this->data)) {
            $this->data = array();
        }

        if (!is_array($this->data['flash'])) {
            $this->data['flash'] = array();
        }

        if (!isset($this->data['flash'][$duration])) {
            $this->data['flash'][$duration] = array();
        }

        $this->data['flash'][$duration][$key] = $data;
        
        return $this;
    }

    public function flashMessage($message, $type = 'success', $duration = 1)
    {
        $this->initMessages($duration);

        $valid_types = array('success', 'warning', 'error');

        if (in_array($type, $valid_types)) {
            $this->data['flash'][$duration]['messages'][$type][] = $message;
        }

        return $this;
    }

    public function flashAdminMessage($message, $type = 'success', $duration = 1)
    {
        $this->initAdminMessages($duration);

        $valid_types = array('success', 'warning', 'error');

        if (in_array($type, $valid_types)) {
            $this->data['flash'][$duration]['admin_messages'][$type][] = $message;
        }

        return $this;
    }

    private function initMessages($duration)
    {
        if ($this->fetch('messages', $duration) === null) {
            $this->flash('messages', array('success' => array(), 'warning' => array(), 'error' => array()), $duration);
        }

        return $this;
    }

    private function initAdminMessages($duration)
    {
        if ($this->fetch('admin_messages', $duration) === null) {
            $this->flash('admin_messages', array('success' => array(), 'warning' => array(), 'error' => array()), $duration);
        }

        return $this;
    }

    public function fetch($key, $duration = 0)
    {
        if (is_array($this->data) && isset($this->data['flash'])) {
            return isset($this->data['flash'][$duration][$key]) ? $this->data['flash'][$duration][$key] : null;
        } else {
            return null;
        }
    }

    public function fetchMessages()
    {
        $messages = $this->fetch('messages');

        return is_array($messages) ? $messages : array('success' => array(), 'warning' => array(), 'error' => array());
    }

    public function fetchAdminMessages()
    {
        $messages = $this->fetch('admin_messages');

        return is_array($messages) ? $messages : array('success' => array(), 'warning' => array(), 'error' => array());
    }

    private function decrement()
    {
        $updated = array();

        if (is_array($this->data) && isset($this->data['flash']) && !empty($this->data['flash'])) {
            foreach ($this->data['flash'] as $duration => $data) {
                if ($duration > 0) {
                    $updated[($duration - 1)] = $data;
                }
            }

            $this->data['flash'] = $updated;
        }

        return $this;
    }
}
