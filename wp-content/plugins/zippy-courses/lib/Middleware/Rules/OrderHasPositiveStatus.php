<?php

class Zippy_OrderHasPositiveStatus_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You do not own access to this content.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'order-has-positive-status';
    }

    public function handle($object)
    {
        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }
        
        if (!($object instanceof Zippy_Order)) {
            $this->exitEarly(true);
        }

        $zippy = Zippy::instance();

        $valid_statuses = array('active', 'complete');
        $valid_types    = array('subscription', 'payment-plan', 'payment_plan');

        $status     = $object->getStatus();
        $access     = in_array($status, $valid_statuses);
        
        if (!$access) {
            $type = $object->getType();

            if ($status == 'pending' && in_array($type, $valid_types)) {
                $now    = $zippy->utilities->datetime->getNow();
                $valid  = clone($object->getDate());
                $valid->modify('+2 hours');

                $access = $valid->format('U') > $now->format('U');
            }
        }
        $access = apply_filters('zippy_courses_order_has_positive_access', $access, $object, $status);

        return $access ? $this->pass($object) : $this->fail($object);
    }
}
