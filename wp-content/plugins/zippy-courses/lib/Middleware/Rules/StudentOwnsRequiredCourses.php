<?php

class Zippy_StudentOwnsRequiredCourses_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('This Product requires you already own a different course.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'product-requires-existing-access';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();
        $id = isset($object->ID) ? $object->ID : $object->id;
        $student = $zippy->cache->get('student');

        if (get_post_type($id) !== 'product') {
            return $this->exitEarly();
        }

        $product = $zippy->make('product', array($id));
        $required_courses = $product->getRequiredCourses();

        if ($student->hasTierAccess($required_courses->course, $required_courses->tier)) {
            $access = true;
        } else {
            $access = false;
            $this->updateErrorMessage($required_courses->course, $required_courses->tier);
        }

        
        return $access ? $this->pass($object) : $this->fail($object);
    }

    public function updateErrorMessage($course_id, $tier_id)
    {
        $zippy = Zippy::instance();
        $course = $zippy->make('course', array($course_id));
        $tiers = $course->getTiers();
        $course_title = $course->getTitle();
        
        foreach ($tiers as $tier) {
            if ($tier->ID == $tier_id) {
                $tier_title = $tier->title;
            }
        }
        
        $error_message = sprintf(__('This Product requires you already own the %s tier of %s.', ZippyCourses::TEXTDOMAIN), $tier_title, $course_title);
        $this->setErrorMessage($error_message);
    }
}
