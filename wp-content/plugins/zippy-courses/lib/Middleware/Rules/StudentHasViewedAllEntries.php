<?php

class Zippy_StudentHasViewedAllEntries_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You have not yet viewed all the lessons in this course.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'student-has-viewed-to-all-entries';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        $id         = isset($object->ID) ? $object->ID : $object->id;
        if (get_post_type($id) != 'course') {
            $course_id = $zippy->utilities->entry->getCourseId($id);
        } else {
            $course_id = $id;
        }
        $course = $student->getCourse($course_id);


        $course_entries = $course->entries->makeFlat();
        $all_views_dataset = new ZippyCourses_CourseAllViews_Dataset($course_id);
        $views_data = $all_views_dataset->getData();
        $access = true;
        foreach ($course_entries->all() as $entry) {
            // If a Student doesn't have an analtyics record for the entry, set access to false.
            if (!isset($views_data[$entry->getId()])) {
                $access = false;
            }
        }

        return $access ? $this->pass($object) : $this->fail($object);
    }
}
