<?php

class Zippy_UserIsNotLoggedIn_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You cannot view this content while logged in.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'user-is-not-logged-in';
    }

    public function handle($object)
    {
        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }

        if (is_user_logged_in()) {
            return $this->exitEarly(false);
        }

        return $this->pass($object);
    }
}
