<?php

class Zippy_ItemIsNotPubliclyVisible_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You are attempting to access a protected entry without the correct permissions.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'item-is-not-publicly-visible';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }

        $id = isset($object->ID) ? $object->ID : $object->id;
        $post_type  = isset($object->post_type) ? $object->post_type : get_post_type($id);

        if ($post_type !== 'course' || is_user_logged_in()) {
            return $this->pass($object);
        }

        $public = get_post_meta($id, 'public', true);

        $access = !empty($public) && $public == 1;

        return $access ? $this->exitEarly() : $this->fail($object);
    }
}
