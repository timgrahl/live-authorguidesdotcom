<?php

class Zippy_StudentHasAdditionalContentAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage('');
        $this->id = 'student-has-additional-content-access';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        $id         = isset($object->ID) ? $object->ID : $object->id;

        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }

        if (get_post_type($id) == 'course') {
            return $this->pass($object);
        }

        $additional_content_ids = array();

        foreach ($student->getCourses()->all() as $course) {
            $course_ac_ids = $course->additional_content->toArray('id');
            $additional_content_ids = array_merge($additional_content_ids, $course_ac_ids);
        }

        $additional_content_ids = array_unique($additional_content_ids);

        $access = in_array($id, $additional_content_ids);

        return $access ? $this->exitEarly() : $this->pass($object);
    }
}
