<?php

class Zippy_StudentHasExpirationAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('Your access to this content has expired.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'student-has-expiration-access';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        $id         = isset($object->ID) ? $object->ID : $object->id;

        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }
        
        $post_type  = isset($object->post_type) ? $object->post_type : get_post_type($id);
        $course_id  = $post_type == 'course' ? $id : $zippy->utilities->entry->getCourseId($id);

        if ($course_id === null || $student === null || $post_type == 'course') {
            return $this->pass($object);
        }

        $today              = $zippy->utilities->datetime->getToday();
        $course             = $zippy->make('course', array($course_id));
        $expiration_date    = $zippy->utilities->course->getExpirationDate($student, $course);

        $access             = $expiration_date instanceof DateTime
                                ? $today->format('U') <= $expiration_date->format('U')
                                : true;

        return $access ? $this->pass($object) : $this->fail($object);
    }
}
