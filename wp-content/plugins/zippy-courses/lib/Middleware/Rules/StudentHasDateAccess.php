<?php

class Zippy_StudentHasDateAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('This content has not yet been made available.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'student-has-date-access';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        $id         = isset($object->ID) ? $object->ID : $object->id;
        
        if ($this->checkCache($object) !== null) {
            $cached_value = $this->checkCache($object);
            if ($cached_value['value'] < 0) {
                $this->updateDateAvailable($object);
            }

            return $this->handleCachedResult($object, $this->checkCache($object));
        }
        
        if (get_post_type($id) == 'course') {
            return $this->pass($object);
        }

        $course_ids = $zippy->utilities->entry->getCourseIds($id);

        $access = false;
        $today = $zippy->utilities->datetime->getToday();

        foreach ($course_ids as $course_id) {
            if ($access) {
                break;
            }

            $entry = null;
            $date_available = '';

            $course = $student->getCourse($course_id);
            
            if ($course !== null) {
                $entry  = $course->entries->getRecursive($id);
            }

            if ($entry !== null) {
                if (!($entry->getDateAvailable() instanceof DateTime)) {
                    // @TODO: Figure out why this wouldn't happen
                } else {
                    //@TODO: Figure out why this isn't already always midnight
                    $available = $entry->getDateAvailable();
                    $available->setTime(0,0,0);
                    $access = $available->format('U') <= $today->format('U');
                    $date_available = $zippy->utilities->datetime->getFormattedDate($entry->getDateAvailable());
                }
            }

            if (!$access) {
                $this->setErrorMessage(
                    sprintf(
                        __('This will be available on %s.', ZippyCourses::TEXTDOMAIN),
                        $date_available
                    )
                );
            }
        }

        return $access ? $this->pass($object) : $this->fail($object);
    }

    private function updateDateAvailable($object)
    {
        $zippy = Zippy::instance();

        $id         = $this->getObjectId($object);

        $course_ids = $zippy->utilities->entry->getCourseIds($id);

        $access = false;
        $today = $zippy->utilities->datetime->getToday();

        foreach ($course_ids as $course_id) {
            if ($access) {
                break;
            }

            $entry = null;
            $date_available = '';

            $student    = $zippy->cache->get('student');
            $course     = $student->getCourse($course_id);
            
            if ($course !== null) {
                $entry  = $course->entries->getRecursive($id);
            }

            if ($entry !== null) {
                if (!($entry->getDateAvailable() instanceof DateTime)) {
                    // @TODO: Figure out why this wouldn't happen
                } else {
                    //@TODO: Figure out why this isn't already always midnight
                    $available = $entry->getDateAvailable();
                    $available->setTime(0,0,0);
                    $access = $available->format('U') <= $today->format('U');
                    $date_available = $zippy->utilities->datetime->getFormattedDate($entry->getDateAvailable());
                }
            }

            if (!$access) {
                $this->setErrorMessage(
                    sprintf(
                        __('This will be available on %s.', ZippyCourses::TEXTDOMAIN),
                        $date_available
                    )
                );
            }
        }
    }
}
