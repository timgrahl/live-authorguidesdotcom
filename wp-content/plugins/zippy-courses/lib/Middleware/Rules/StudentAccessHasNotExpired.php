<?php

class Zippy_StudentAccessHasNotExpired_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('Your access to this content has expired.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'student-access-has-not-expired';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        $id         = isset($object->ID) ? $object->ID : $object->id;
        
        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }
        
        if (get_post_type($id) == 'course') {
            return $this->pass($object);
        }

        $course_ids = $zippy->utilities->entry->getCourseIds($id);

        $access = false;
        $today = $zippy->utilities->datetime->getToday();

        foreach ($course_ids as $course_id) {
            if ($access) {
                break;
            }

            $entry = null;
            $date_available = '';

            $course = $student->getCourse($course_id);
            
            if ($course !== null) {
                $entry  = $course->entries->getRecursive($id);
            }

            if ($entry !== null) {
                if (!($entry->getDateAvailable() instanceof DateTime)) {
                }
                
                $access = $entry->getDateAvailable()->format('U') <= $today->format('U');
                    $date_available = $zippy->utilities->datetime->getFormattedDate($entry->getDateAvailable());
            }

            if (!$access) {
                $this->setErrorMessage(
                    sprintf(
                        __('This will be available on %s.', ZippyCourses::TEXTDOMAIN),
                        $date_available
                    )
                );
            }
        }

        return $access ? $this->pass($object) : $this->fail($object);
    }
}
