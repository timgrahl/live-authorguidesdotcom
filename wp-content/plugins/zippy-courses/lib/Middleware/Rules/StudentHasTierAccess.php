<?php

class Zippy_StudentHasTierAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You do not have access to the correct tier(s) to view this content.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'student-has-tier-access';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        $id         = isset($object->ID) ? $object->ID : $object->id;
        $post_type  = isset($object->post_type) ? $object->post_type : get_post_type($id);

        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }
        
        if ($post_type == 'course') {
            // For Courses:
            // If the student has access to at least one tier, pass
            // If not, or all tier access is revoked, fail
            $tiers = $student->getCourseTiers();

            if (isset($tiers[$id])) {
                foreach ($tiers[$id] as $tier) {
                    if ($tier['status']) {
                        return $this->pass($object);
                    }
                }
            }
            return $this->fail($object);
        }

        if ($student === null) {
            return $this->fail($object);
        }

        $tiers      = $student->getCourseTiers();
        $access     = false;

        foreach ($zippy->utilities->entry->getCourseIds($id) as $course_id) {
            $course         = $zippy->make('course', array($course_id));
            $course_tiers   = isset($tiers[$course_id]) ? $tiers[$course_id] : array();
            $entry          = $course->getEntry($id);
            $control        = $course->config->access->getControlItem();

            if ($entry) {
                $access = $this->_hasTier($course, $entry, $course_tiers, $control);

                if ($access) {
                    break;
                }
            }
        }

        return $access ? $this->pass($object) : $this->exitEarly(false);
    }

    /**
     * Determine whether an entry has the necessary tiers
     *
     * @since  1.0.0
     *
     * @param  Zippy_Entry $entry
     * @param  array       $tiers   An array of tier IDs
     * @param  string      $control Check based on item or unit
     *
     * @return boolean
     */
    private function _hasTier(Zippy_Course $course, Zippy_Entry $entry, array $tiers = array(), $control = 'item')
    {
        $zippy = Zippy::instance();

        $has_tier = false;

        if ($control == 'item') {
            $has_tier = $entry->type == 'item'
                ? $this->_itemHasTier($entry, $tiers)
                : ($entry->getEntries()->count() > 0
                    ? $this->_unitChildrenHaveTier($entry, $tiers)
                    : $this->_itemHasTier($entry, $tiers)
                );
        } else {
            $unit = $zippy->utilities->entry->getEntryUnit($course, $entry->getId());
            $has_tier = $unit === null ? $this->_itemHasTier($entry, $tiers) : $this->_itemHasTier($unit, $tiers);
        }

        return $has_tier;
    }

    /**
     * Determine whether a unit has tier access
     *
     * @since  1.0.0
     *
     * @param  Zippy_Entry $entry
     * @param  array       $tiers   An array of tier IDs
     *
     * @return boolean
     */
    private function _unitChildrenHaveTier(Zippy_Entry $entry, array $tiers)
    {
        $has_tier = false;



        foreach ($entry->entries->all() as $subentry) {
            foreach ($subentry->getTiers() as $tier) {
                $has_tier = $this->_itemHasTier($subentry, $tiers);

                if ($has_tier) {
                    break;
                }
            }

            if ($has_tier) {
                break;
            }
        }

        return $has_tier;
    }

    /**
     * Determine whether an item has tier access
     *
     * @since  1.0.0
     *
     * @param  Zippy_Entry $entry
     * @param  array       $tiers   An array of tier IDs
     *
     * @return boolean
     */
    private function _itemHasTier(Zippy_Entry $entry, array $tiers)
    {
        $has_tier = false;

        foreach ($entry->getTiers() as $tier) {
            if (array_key_exists($tier, $tiers) && $tiers[$tier]['status']) {
                $has_tier = true;
            }

            if ($has_tier) {
                break;
            }
        }

        return $has_tier;
    }
}
