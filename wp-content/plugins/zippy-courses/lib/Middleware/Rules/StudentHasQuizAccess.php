<?php
/**
 * @todo    Make this independent of $student->getCourse() if possible
 */
class Zippy_StudentHasQuizAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You must take and pass a quiz earlier in the course to access this entry.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'student-has-quiz-access';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        $student->fetch();

        $id         = isset($object->ID) ? $object->ID : $object->id;
        
        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }

        if (get_post_type($id) == 'course') {
            return $this->pass($object);
        }

        $course_id     = $zippy->utilities->entry->getCourseId($id);

        $access = false;

        $courses = $student->getCourses();
        $course = $student->getCourse($course_id);

        if ($course === null) {
            return $access ? $this->pass($object) : $this->fail($object);
        }

        $graph  = $this->buildQuizGraph($course);

        $access = true;
        foreach ($graph as $entry_id => $quiz) {
            // Only check everything up to this entry in the course
            if ($entry_id == $id) {
                break;
            }

            // "Skip" checks
            if ($quiz->ID == 0 ||
                (!$quiz->required->pass && !$quiz->required->completion)
            ) {
                continue;
            }

            // Get the results
            $results = $student->getQuizResults($quiz->ID);

            // Validate the results
            if ($quiz->required->completion && empty($results)) {
                $access = false;
                break;
            }

            if (!empty($results)) {
                if ($quiz->required->pass) {
                    $passed = false;
                    foreach ($results as $result) {
                        if ($quiz->pass <= $result->score) {
                            $passed = true;
                        }
                    }

                    if (!$passed) {
                        $access = false;
                    }
                }
            }
        }

        return $access ? $this->pass($object) : $this->fail($object);
    }

    private function buildQuizGraph(Zippy_Course $course)
    {
        $graph = array();

        foreach ($course->entries->all() as $entry) {
            $graph[$entry->getId()] = $entry->getQuiz();

            foreach ($entry->entries->all() as $subentry) {
                $graph[$subentry->getId()] = $subentry->getQuiz();
            }
        }
        
        return $graph;
    }
}
