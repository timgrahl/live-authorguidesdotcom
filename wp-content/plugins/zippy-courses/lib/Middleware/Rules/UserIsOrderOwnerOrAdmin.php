<?php

class Zippy_UserIsOrderOwnerOrAdmin_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You do not have access to view this order.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'user-is-not-order-owner-or-admin';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();
        global $current_user;
        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }

        if (current_user_can('edit_others_posts')) {
            return $this->pass($object);
        }


        $order = $object;

        if ($order->owner == $current_user->ID) {
            return $this->pass($object);
        }


        return $this->fail($object);
    }
}
