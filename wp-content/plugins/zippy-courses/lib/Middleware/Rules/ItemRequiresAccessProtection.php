<?php

class Zippy_ItemRequiresAccessProtection_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You are attempting to access a protected entry without the correct permissions.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'item-requires-access-protection';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }
        
        $id = isset($object->ID) ? $object->ID : $object->id;

        $protected_content_ids = $zippy->utilities->entry->getAllProtectedEntryIds();
        $protected_content_ids = array_merge($protected_content_ids, $zippy->core_pages->getProtectedCorePages());

        $protected_post_types = array('course', 'unit', 'lesson');

        if (!in_array($id, $protected_content_ids) && !in_array(get_post_type($id), $protected_post_types)) {
            // It doesn't need protection, so we can return true, which
            // bails us out of the stack.
            return $this->exitEarly();
        }

        return $this->pass($object);
    }
}
