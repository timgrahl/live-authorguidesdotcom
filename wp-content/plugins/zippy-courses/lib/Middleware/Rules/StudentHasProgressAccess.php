<?php

class Zippy_StudentHasProgressAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You must complete a previous lesson.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'student-has-progress-access';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        $id         = isset($object->ID) ? $object->ID : $object->id;
        
        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }
        
        if ($student == null || get_post_type($id) == 'course') {
            return $this->pass($object);
        }

        $course_id = $zippy->utilities->entry->getCourseId($id);
        $entry_ids = $zippy->utilities->course->getCourseEntryIds($course_id);

        if (!in_array($id, $entry_ids)) {
            return $this->pass($object);
        }

        $course = $student->getCourse($course_id);
        $control = $course->config->scheduling->getControlItem();

        if ($course === null) {
            return $this->pass($object);
        }

        $access = true;
        $requires_previous_lesson_completed = false;

        $entry_ids = $course->getEntryIds();
        $key = array_search($id, $entry_ids);

        $exempted_test = $course->entries->getRecursive($id);
        if ($exempted_test !== null) {
            if ($exempted_test->isExempt()) {
                return $this->pass($object);
            }
        }

        $segment = array_splice($entry_ids, 0, $key);

        $required = array();

        /**
         * First, identify all of the entries in a course that require completion
         */
        foreach ($segment as $entry_id) {
            $entry = $course->entries->getRecursive($entry_id);

            if ($entry->isRequired() && $this->entrySchedulingIsConfigured($course, $entry)) {
                $required[] = $entry_id;
            }
        }

        /*
         * Compare the entries in the Course with the required entries identified above
         */
        foreach ($segment as $entry_id) {
            // If we've reached the entry ID of the item we're testing, exit the loop
            // This prevents us from checking requirements from later entries
            if ($entry_id == $id) {
                break;
            }

            // If the entry is not required, continue. Otherwise, check to see if the entry is completed
            if (!in_array($entry_id, $required)) {
                continue;
            } else {
                $access = $student->isCompleted($entry_id);
                // If we're controlling by Unit instead of Lesson,
                // check the lesson's unit to determine if the lesson is available
                if ($control === 'unit') {
                    $lesson_unit = $zippy->utilities->entry->getEntryUnit($course, $id);
                    if ($lesson_unit) {
                        $previous_unit_completed = $this->studentCompletedPreviousUnit($lesson_unit->getId(), $course, $student);
                        $access = $previous_unit_completed || $lesson_unit->isAvailable();
                    }
                }
            }

            if (!$access) {
                break;
            }
        }

        if ($control === 'unit') {
            $this->setErrorMessage(__('You must complete a previous unit.', ZippyCourses::TEXTDOMAIN));
        }
        
        return $access ? $this->pass($object) : $this->fail($object);
    }

    private function studentCompletedPreviousUnit($id, Zippy_Course $course, ZippyCourses_Student $student)
    {
        $previous_unit = $course->entries->getPreviousUnit($id);
        if ($previous_unit === null) {
            return true;
        } else {
            return $student->isCompleted($previous_unit->getId());
        }
    }

    private function entrySchedulingIsConfigured(Zippy_Course $course, Zippy_Entry $entry)
    {
        $zippy = Zippy::instance();
        $control = $course->config->scheduling->getControlItem();
        if ($control == 'item' && $entry->getPostType() == 'unit') {
            if (count($entry->entries->all()) > 0) {
                return false;
            }
        }
        if ($control == 'unit' && $entry->getPostType() == 'lesson') {
            $lesson_unit = $zippy->utilities->entry->getEntryUnit($course, $entry->id);
            if ($lesson_unit) {
                return false;
            }
        }
        return true;
    }
}
