<?php

class Zippy_UserHasCorePageAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->id = 'user-has-core-page-access';
    }
    
    public function handle($object)
    {
        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }
    }
}
