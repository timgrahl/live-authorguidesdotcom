<?php

class Zippy_ProductRequiresExistingAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You are attempting to access a product that requires access to a different course.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'product-uses-existing-access-requirement';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();
        $id = isset($object->ID) ? $object->ID : $object->id;
        if (get_post_type($id) !== 'product') {
            return $this->exitEarly();
        }

        $product = $zippy->make('product', array($id));

        if ($product->hasExistingAccessRequirement()) {
            return $this->pass($object);
        }

        return $this->exitEarly();
        
    }
}
