<?php

class Zippy_ItemIsCorePage_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You are attempting to a page that you must be logged in to view.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'item-is-core-page';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }

        $id = isset($object->ID) ? $object->ID : $object->id;

        if ($zippy->core_pages->is($id)) {
            return $this->exitEarly();
        }

        return $this->pass($object);
    }
}
