<?php

class Zippy_UserMustBeAdmin_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You must be an administrator to view this content.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'user-must-be-admin';
    }

    public function handle($object)
    {
        global $current_user;
        
        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }
        
        return current_user_can('edit_others_posts') ? $this->pass($object) : $this->fail($object);
    }
}
