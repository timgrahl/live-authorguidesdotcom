<?php

class Zippy_StudentHasAccessToAllEntries_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You have not yet finished this course.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'student-has-access-to-all-entries';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        $id         = isset($object->ID) ? $object->ID : $object->id;
        if (get_post_type($id) != 'course') {
            $course_id = $zippy->utilities->entry->getCourseId($id);
        } else {
            $course_id = $id;
        }
        $course = $student->getCourse($course_id);


        $course_entries = $course->entries->makeFlat();

        $entry_middleware = $zippy->middleware->get('student-access');
        $access = true;
        foreach ($course_entries->all() as $entry) {
            if (!$entry_middleware->run($entry)) {
                $access = false;
                break;
            }
        }

        return $access ? $this->pass($object) : $this->fail($object);
    }
}
