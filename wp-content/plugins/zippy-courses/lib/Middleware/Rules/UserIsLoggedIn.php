<?php

class Zippy_UserIsLoggedIn_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->id = 'is-user-logged-in';
        $this->setErrorMessage(__('You must be logged in in order to view this content.', ZippyCourses::TEXTDOMAIN));
    }

    public function handle($object)
    {
        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }
        
        if (!is_user_logged_in()) {
            return $this->exitEarly(false);
        }

        return $this->pass($object);
    }
}
