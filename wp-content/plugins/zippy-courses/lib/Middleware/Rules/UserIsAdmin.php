<?php

class Zippy_UserIsAdmin_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->id = 'is-user-admin';
    }

    public function handle($object)
    {
        global $current_user;

        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }
        
        if (current_user_can('edit_others_posts')) {
            return $this->exitEarly();
        }

        return $this->pass($object);
    }
}
