<?php

class Zippy_StudentHasCourseAccess_MiddlewareRule extends Zippy_MiddlewareRule
{
    public function __construct()
    {
        $this->setErrorMessage(__('You do not have access to this course.', ZippyCourses::TEXTDOMAIN));
        $this->id = 'student-has-course-access';
    }

    public function handle($object)
    {
        $zippy = Zippy::instance();

        $student    = $zippy->cache->get('student');
        if (!$student) {
            return $this->exitEarly(false);
        }

        if ($this->checkCache($object) !== null) {
            return $this->handleCachedResult($object, $this->checkCache($object));
        }
        
        $id         = isset($object->ID) ? $object->ID : $object->id;
        $post_type  = isset($object->post_type) ? $object->post_type : get_post_type($id);

        $course_ids = $zippy->utilities->entry->getCourseIds($id);

        if ($post_type == 'course') {
            $course_ids[] = $id;
        }

        $access = false;
        foreach ($student->getCourseIds() as $course_id) {
            if (in_array($course_id, $course_ids)) {
                $access = true;
                break;
            }
        }

        return $access ? $this->pass($object) : $this->exitEarly(false);
    }
}
