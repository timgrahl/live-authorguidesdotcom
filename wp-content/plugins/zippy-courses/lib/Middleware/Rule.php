<?php

abstract class Zippy_MiddlewareRule implements Zippy_RepositoryObject
{
    const PASS      = 1;
    const EXIT_PASS = 2;

    const FAIL      = -1;
    const EXIT_FAIL = -2;

    public $id;
    public $next;
    public $passed = true;
    public $error_message;

    protected $cache;

    abstract public function handle($object);

    public function checkCache($object)
    {
        if ($this->cache === null) {
            $this->setupCache();
        }

        $object_id  = $this->getObjectId($object);
        $cached     = $this->cache->get($object_id);

        return apply_filters('zippy_courses_middleware_rule_cache', $cached, $object);
    }

    public function cache($object, $result)
    {
        $zippy = Zippy::instance();

        if ($this->cache === null) {
            $this->setupCache();
        }
        
        $object_id = $this->getObjectId($object);

        if ($object_id) {
            if ($this->cache->get($object_id)) {
                $value = $this->cache->get($object_id);
                $value['value'] = $result;

                $this->cache->set($object_id, $value);
            } else {
                $expires = $zippy->utilities->datetime->getNow();
                $expires->modify('+2 hours');

                $this->cache->set($object_id, array(
                    'value'     => $result,
                    'expires'   => $expires->format('U')
                ));
            }

            $this->persistCache();
        }
    }

    private function persistCache()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');
        if (!$student) {
            return;
        }

        $student->setMiddlewareRulesCacheData($this->getId(), $this->cache->data);
    }

    public function handleCachedResult($object, $value)
    {
        switch ($value['value']) {
            case Zippy_MiddlewareRule::EXIT_PASS:
                return $this->exitEarly(Zippy_MiddlewareRule::EXIT_PASS);
                break;
            case Zippy_MiddlewareRule::EXIT_FAIL:
                return $this->exitEarly(Zippy_MiddlewareRule::EXIT_FAIL);
                break;
            case Zippy_MiddlewareRule::FAIL:
                return $this->fail($object, true);
                break;
            case Zippy_MiddlewareRule::PASS:
            default:
                return $this->pass($object, true);
                break;
        }
    }

    public function setupCache()
    {
        global $currentuser;

        $zippy = Zippy::instance();

        $this->cache    = $zippy->make('cache');
        $student        = $zippy->cache->get('student');
        
        if (!$student) {
            return;
        }

        $data = $student->getMiddlewareRulesCache($this->getId());

        foreach ($data as $key => $value) {
            $this->cache->set($key, $value);
        }
    }

    public function next($object)
    {
        if ($this->next instanceof Zippy_MiddlewareRule) {
            return call_user_func(array($this->next, 'handle'), $object);
        }

        return;
    }

    public function exitEarly($passed = true)
    {
        $this->passed = $passed;
        $this->next = null;
        return;
    }

    public function pass($object, $cached = false)
    {
        if (!$cached) {
            $this->cache($object, Zippy_MiddlewareRule::PASS);
        }
        $this->passed = true;

        if ($this->next instanceof Zippy_MiddlewareRule) {
            return call_user_func(array($this->next, 'handle'), $object);
        }
    }

    protected function fail($object, $cached = false)
    {
        if (!$cached) {
            $this->cache($object, Zippy_MiddlewareRule::FAIL);
        }
        $this->passed = false;
        
        if ($this->next instanceof Zippy_MiddlewareRule) {
            return call_user_func(array($this->next, 'handle'), $object);
        }
    }

    public function setNext(Zippy_MiddlewareRule $middleware)
    {
        $this->next = $middleware;
        return $this;
    }

    public function getStackSummary(array $summary = array())
    {
        if ($this->next) {
            $summary = $this->next->getStackSummary($summary);
        }

        $summary[] = get_class($this);

        return $summary;
    }

    public function getSummary()
    {
        return array_reverse($this->getStackSummary());
    }

    public function getErrorMessage()
    {
        $default = str_replace(array('_', 'ZippyCourses', 'Middleware'), '', get_class($this));
        
        return $this->error_message !== null
            ? $this->error_message
            : sprintf(__("Failed while when checking %s", ZippyCourses::TEXTDOMAIN), $default);
    }

    public function setErrorMessage($error_message)
    {
        $this->error_message = $error_message;
    }

    public function getErrors($errors = array())
    {
        if (!$this->passed) {
            $errors[$this->getId()] = $this->getErrorMessage();
        }

        if ($this->next instanceof Zippy_MiddlewareRule) {
            $errors = call_user_func_array(array($this->next, 'getErrors'), array($errors));
        }

        return $errors;
    }

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the value of id.
     *
     * @param mixed $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getObjectId($object)
    {
        return isset($object->ID) ? $object->ID : $object->id;
    }
}
