<?php

abstract class Zippy_Middleware implements Zippy_RepositoryObject
{
    public $id;

    public $error_messages;
    public $rules = array();
    protected $errors = array();
    protected $processed = false;

    public function __construct($id)
    {
        $zippy = Zippy::instance();

        $this->rules = $zippy->make('middleware_rules');
        $this->id = $id;

        $this->initialize();
    }

    public function initialize()
    {
        $this->setupCache();
        $this->defaultRules();
        $this->defaultErrorMessages();
    }

    public function setupCache()
    {
        $zippy = Zippy::instance();

        $this->cache = $zippy->make('cache');

        $student = $zippy->cache->get('student');
        if (!$student) {
            return;
        }

        $cache = $student->getMiddlewareCache($this->getId());

        if (is_array($cache)) {
            foreach ($cache as $id => $item) {
                $this->cache->set($id, $item['value']);
            }
        }
    }

    public function persistCache()
    {
        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');
        if (!$student) {
            return;
        }

        $student->setMiddlewareCacheData($this->getId(), $this->cache->data);
    }

    abstract public function defaultRules();
    abstract public function defaultErrorMessages();

    public function run($object = null)
    {
        $zippy = Zippy::instance();

        if ($object === null) {
            $post_id = $zippy->utilities->post->getPostId();
            $object = get_post($post_id);
        }

        if (!is_object($object)) {
            return true;
        }

        if (empty($this->rules)) {
            return true;
        }

        $object_id = isset($object->ID) ? $object->ID : $object->id;
        if ($this->cache->exists($object_id)) {
            $item = $this->cache->get($object_id);
            if ($item) {
                $this->errors = $item['errors'];
                return $item['value'];
            }
        }

        $stack = $this->prepare();
        $results = $stack->handle($object);
       
        $this->processed    = true;
        $this->errors       = $stack->getErrors();
        
        $value = count($this->errors) < 1;
        $expires = $zippy->utilities->datetime->getNow();
        $expires->modify('+2 hours');

        $this->cache->set($object_id, array(
            'value'     => $value,
            'errors'    => $this->errors,
            'expires'   => $expires->format('U')
        ));

        $this->persistCache();

        return $value;
    }

    private function prepare()
    {
        if ($this->rules->count() < 1) {
            return false;
        }

        $rules = array_reverse($this->rules->all());

        $stack = array_shift($rules);

        foreach ($rules as $rule) {
            $stack = $rule->setNext($stack);
        }

        return $stack;
    }

    public function getErrors()
    {
        return $this->processed ? $this->errors : null;
    }

    public function getErrorMessages()
    {
        return implode(' ', $this->errors);
    }

    public function getErrorMessagesList()
    {
        $list = '<ul>';
        foreach ($this->getErrors() as $error) {
            $list .= '<li>' . $error . '</li>';
        }
        $list .= '</ul>';

        return $list;
    }

    public function renderErrorMessages($before = '', $after = '')
    {
        $message = $this->error_messages['before'] . $this->getErrorMessagesList() . $this->error_messages['after'];
        return '<div class="zippy-alert zippy-alert-error">' .
            apply_filters('zippy_middleware_error_messages', $message, $this) .
        '</div>';
    }

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}
