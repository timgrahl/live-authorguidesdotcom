<?php

class Zippy_Middlewares extends Zippy_Repository
{
    protected static $instance;

    protected function __construct()
    {
        $this->addValidType('Zippy_Middleware');
    }

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }
}
