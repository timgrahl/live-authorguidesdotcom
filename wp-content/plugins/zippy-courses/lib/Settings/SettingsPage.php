<?php

/**
 * A connector for the automatic setup and rendering of settings pages in WordPress,
 * not to be confused with the Zippy_AdminPage class.
 *
 * Zippy_SettingsPage is only for use with settings related tasks and organization.
 *
 * Zippy_AdminPage is for use with creating and modifying admin pages in WordPress.
 */
class Zippy_SettingsPage extends Zippy_Model
{
    public $id;
    private $sections = array();

    public function __construct($id, array $args = array())
    {
        $this->id = $id;
    }

    public function createSection($name, $title)
    {
        $zippy = Zippy::instance();

        $section = $zippy->make('settings_section');

        $section->setId($name)
                ->setName($name)
                ->setTitle($title)
                ->setPage($this);

        $this->sections[$name] = $section;

        return $this->sections[$name];
    }

    public function register()
    {
        foreach ($this->sections as $section) {
            $section->register();
        }
    }

    public function getId()
    {
        return $this->id;
    }
}
