<?php

class Zippy_SettingsPages extends Zippy_Repository
{
    private static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        // Assign object_class as it is required
        $this->addValidType('Zippy_SettingsPage');

        $this->hooks();
    }

    public function register()
    {
        foreach ($this->all() as $page) {
            $page->register();
        }
    }

    public function hooks()
    {
        add_action('admin_init', array($this, 'register'));
    }

    public function fetch($id)
    {
        if ($this->get($id) !== null) {
            return $this->get($id);
        }

        $page = new Zippy_SettingsPage($id);
        $this->add($page);

        return $page;
    }
}
