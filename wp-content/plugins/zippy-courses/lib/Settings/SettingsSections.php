<?php

class Zippy_SettingsSections extends Zippy_Repository
{
    private static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->addValidType('Zippy_SettingsSection');

        $this->hooks();
    }

    public function register()
    {
        foreach ($this->all() as $section) {
            $section->register();
        }
    }

    /**
     * Integrate factory with relavent WordPress hooks
     * @return void
     */
    protected function hooks()
    {
        add_action('admin_init', array($this, 'register'));
    }
}
