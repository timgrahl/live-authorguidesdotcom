<?php

class Zippy_SettingsSection extends Zippy_Model
{
    public $id;
    protected $name;
    protected $title;
    protected $page;
    protected $fields = array();

    public function __construct($id, array $args = array())
    {
        $this->name = $id;
    }

    public function createField($name, $label, $type = 'text', $options = array())
    {
        $this->fields[$name] = new Zippy_SettingsField($name, $label, $type, $options);
        $this->fields[$name]->setSection($this);

        return $this->fields[$name];
    }
    
    public function register()
    {
        add_settings_section(
            $this->getName(),
            $this->getTitle(),
            array($this, 'render'),
            $this->getPage()->getId()
        );

        $this->registerFields();
        $this->registerSetting();
    }

    private function registerFields()
    {
        foreach ($this->fields as $field) {
            $field->register();
        }
    }

    private function registerSetting()
    {
        register_setting($this->getPage()->getId(), $this->getName());
    }

    public function getName()
    {
        return $this->name;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setPage(Zippy_SettingsPage $page)
    {
        $this->page = $page;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }
}
