<?php

class Zippy_SettingsField extends Zippy_FormField
{
    protected $options;
    protected $type;
    protected $label;
    protected $name;
    protected $value;
    protected $description;
    protected $section;

    public function __construct($name, $label, $type = 'text', $options = array())
    {
        $this->name     = $name;
        $this->label    = $label;
        $this->type     = $type;
        $this->options  = $options;
    }

    public function render()
    {
        $html = '';
        switch($this->type) {
            case 'text':
            case 'number':
                $html .= '<input type="' . $this->type . '" value="' . $this->getValue() . '" name="' . $this->getSettingName() . '" ' . $this->renderOptions() . '/>';
                break;
            case 'textarea':
                $html .= '<textarea name="' . $this->getSettingName() . '">' . $this->getValue() . '</textarea>';
                break;
            case 'select':
                $html .= '<select name="' . $this->getSettingName() . '">';
                foreach ($this->options as $key => $value) {
                    $html .= '<option value="' . $key . '" ' . selected($key, $this->getValue(), false) . '>' . $value . '</option>';
                }
                $html .= '</select>';
                break;
            case 'checkbox':
                foreach ($this->options as $key => $value) {
                    $checked = is_array($this->getValue()) && in_array($key, $this->getValue()) ? ' checked="checked" ' : '';
                    $html .= '<label><input type="checkbox" value="' . $key . '" ' . $checked . ' name="' . $this->getSettingName() . '[]">' . $value . '</label>';
                }
                break;
            case 'html':
            case 'raw':
                $html .= $this->getValue();
                break;

            default:
                break;
        }

        $html .= $this->renderDescription();

        echo $html;
    }

    public function getValue($use_default = true)
    {
        if ($this->value !== null) {
            return $this->value;
        }

        $settings = (array) get_option($this->getSection()->getName());
        return isset($settings[$this->getName()]) ? $settings[$this->getName()] : false;
    }

    public function getSection()
    {
        return $this->section;
    }

    public function setSection(Zippy_SettingsSection $section)
    {
        $this->section = $section;
        return $this;
    }

    private function getSettingName()
    {
        return $this->section->getName() . '[' . $this->getName() . ']';
    }

    public function getName()
    {
        return $this->name;
    }

    public function register()
    {
        add_settings_field(
            $this->getSettingName(),
            $this->getLabel(),
            array($this, 'render'),
            $this->getSection()->getPage()->getId(),
            $this->getSection()->getName()
        );
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function renderDescription()
    {
        $description = $this->getDescription();
        return !empty($description) ? '<p class="description">' . $this->getDescription() . '</p>' : '';
    }
    public function renderOptions()
    {
        $displayed_options = null;
        foreach ($this->options as $option) {
            switch ($option) {
                case 'readonly':
                    $displayed_options .= 'readonly';
                    break;
                
                default:
                    break;
            }
        }
        return $displayed_options;
    }
}
