<?php

class Zippy_Access extends Zippy_Listener
{
    protected $cache;

    private static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->cache = new Zippy_Cache;

        add_action('init', array($this, 'register'), 1);
        add_filter('template_redirect', array($this, 'thankYou'), 11);
        add_filter('template_redirect', array($this, 'buy'), 11);
    }

    public function claim(ZippyCourses_Student $student, $transaction)
    {
        $zippy = Zippy::instance();

        if ($transaction->getOwner() < 1) {
            // Make & Save changes to the Transaction
            $transaction->setOwner($student->getId());
            $order = $transaction->getOrder();
            
            if (!$order) {
                $order = $zippy->make('order');
                $order->setProduct($transaction->getProduct());
            }

            if (current_user_can('edit_others_posts')) {
                $zippy->sessions->flashMessage(
                    __('Your order has been completed, but you are currently logged in as an administrator, so your order cannot be claimed. Visit the "Orders" menu to view your completed order.', ZippyCourses::TEXTDOMAIN),
                    'warning'
                );
                wp_redirect($zippy->core_pages->getUrl('dashboard'));
                exit;
            }
            $order->setOwner($student->getId());
            $order->save();

            $zippy->activity->insert($student->getId(), $order->getId(), 'order');
            $zippy->activity->insert($student->getId(), $order->getProduct()->getId(), 'sale');

            $transaction->setOrder($order);
            $transaction->save();

            $courses = array();
            $course_ids = $zippy->utilities->product->getCourses($order->getProduct()->getId());

            foreach ($course_ids as $course_id) {
                $courses[] = get_the_title($course_id);
                $zippy->activity->insert($student->getId(), $course_id, 'join_course');
            }

            $zippy->events->fire(new ZippyCourses_NewStudent_Event($student, $order));
            
            $zippy->sessions->flashMessage(sprintf(__('You have joined the following course(s): %s', ZippyCourses::TEXTDOMAIN), '<strong>' . implode(', ', $courses) . '</strong>'));
        }
    }

    public function add(ZippyCourses_Student $student, Zippy_Product $product)
    {
        $zippy = Zippy::instance();

        if ($student->getId() < 1) {
            return;
        }

        $products = $student->getProductsOwned();
        $products[] = $product->getId();

        $student->saveMeta('products', array_filter(array_unique($products)));

        // Let the system know!
        $zippy->events->fire(new ZippyCourses_JoinProduct_Event($student, $product));
    }

    public function revoke(ZippyCourses_Student $student, Zippy_Product $product)
    {
        $zippy = Zippy::instance();

        $products = $student->getProductsOwned();
        
        if (($key = array_search($product->getId(), $products)) !== false) {
            unset($products[$key]);
        }

        $student->saveMeta('products', array_filter(array_unique($products)));

        // Let the system know!
        $zippy->events->fire(new ZippyCourses_LeaveProduct_Event($student, $product));
    }

    public function handle(Zippy_Event $event)
    {
        switch ($event->getEventName()) {
            case 'TransactionStatusChange':
                $this->updateTransactionAccess($event);
                break;

            case 'OrderStatusChange':
                $this->updateOrderStatusAccess($event);
                $this->handleEmailSubscriptions($event);
                break;

            case 'OrderOwnerChange':
                $this->updateOrderOwnerAccess($event);
                $this->handleEmailSubscriptions($event);
                break;
            
            default:
                break;
        }
    }

    public function register()
    {
        $zippy = Zippy::instance();

        $zippy->events->register('TransactionStatusChange', $this);
        $zippy->events->register('OrderStatusChange', $this);
        $zippy->events->register('OrderOwnerChange', $this);
    }

    public function updateTransactionAccess(ZippyCourses_TransactionStatusChange_Event $event)
    {
        $transaction    = $event->transaction;
        $status         = $event->new_status;
        $student        = $transaction->getOwner() ? new ZippyCourses_Student($transaction->getOwner()) : false;

        if ($status == 'refund') {
            // We only revoke Order access if it's a single payment refund, as it is possible that a subscription or payment plan
            // is still going on even though a single payment within it has been refunded.
            if ($transaction->getType() == 'single') {
                $order = $transaction->getOrder();
                $order->setStatus('revoke');
                $order->save();
            }
        }

        if ($status == 'cancel') {
            $order = $transaction->getOrder();
            $order->setStatus('cancel');
            $order->save();
        }
    }

    public function updateOrderStatusAccess(ZippyCourses_OrderStatusChange_Event $event)
    {
        $order          = $event->order;
        $order->fetchTransactions();

        $status         = $event->new_status;
        $student        = $order->getOwner() ? new ZippyCourses_Student($order->getOwner()) : false;

        $product = $order->getProduct();

        if ($student && $product && ($status == 'complete' || $status == 'active')) {
            $this->add($student, $product);
        }

        if ($student && $product && ($status == 'revoked' || $status == 'cancelled')) {
            $this->revoke($student, $product);
        }
    }

    public function updateOrderOwnerAccess(ZippyCourses_OrderOwnerChange_Event $event)
    {
        $order          = $event->order;
        $order->fetchTransactions();

        $status = $order->getStatus();

        $student        = $event->new_owner > 0 ? new ZippyCourses_Student($event->new_owner) : false;
        $product        = $order->getProduct();

        if ($student && $product && ($status == 'complete' || $status == 'active')) {
            $this->add($student, $product);
        }

        if ($student && $product && ($status == 'revoke' || $status == 'cancel')) {
            $this->revoke($student, $product);
        }
    }

    public function thankYou()
    {
        global $post, $zippy_transaction_key;

        $zippy = Zippy::instance();

        $student = $zippy->cache->get('student');

        if (is_object($post) && $post->ID == $zippy->core_pages->get('thank_you') && $student !== null) {
            $zippy_transaction_key = !empty($zippy_transaction_key) ? $zippy_transaction_key : $zippy->cache->get('zippy_transaction_key');
            
            if ($zippy_transaction_key !== null) {
                $transaction = $zippy->make('transaction');
                $transaction->buildByTransactionKey($zippy_transaction_key);

                $zippy->access->claim($student, $transaction);

                $zippy->sessions->flashMessage(
                    __("Thank you for your purchase! Please enjoy your courses.", ZippyCourses::TEXTDOMAIN),
                    'success'
                );
                
                $redirect = apply_filters('zippy_courses_purchase_complete_redirect', $zippy->core_pages->getUrl('dashboard'), $student->getId());
                wp_redirect($redirect);
                exit;
            }
        }
    }

    public function buy()
    {
        global $post;

        $zippy = Zippy::instance();
        
        if (!is_object($post) || !$zippy->core_pages->is($post->ID, 'buy')) {
            return;
        }

        $product = $zippy->utilities->product->getProductByBuyQueryVar();

        $settings = get_option('zippy_payment_general_settings', array());
        $register_first =  isset($settings['payment_flow']) && $settings['payment_flow'] == '0';

        if ($register_first && !is_user_logged_in()) {
            $redirect_to = $zippy->core_pages->getUrl('buy') . $product->getId() . '?buy-now';
            $url = $zippy->core_pages->getUrl('register') . '?redirect_to=' . urlencode($redirect_to);
            $login_url = $zippy->core_pages->getUrl('login') . '?redirect_to=' . urlencode($zippy->core_pages->getUrl('buy') . $product->getId() . '?buy-now');

            $zippy->sessions->flashMessage(
                sprintf(
                    __('You must register to purchase %s. If you already have an account, <a href="%s">click here</a> to login.', ZippyCourses::TEXTDOMAIN),
                    '<strong>' . $product->getTitle() . '</strong>',
                    $login_url
                ),
                'warning'
            );

            wp_redirect($url);
            exit;
        }
    }

    public function grant($student_id, $product_id, $show_message = false)
    {
        $zippy = Zippy::instance();

        $student = $zippy->make('student', array($student_id));
        $product = $zippy->make('product', array($product_id));

        if ($student && $product) {
            $student->fetch();

            $products_not_owned = $zippy->utilities->product->getProductsNotOwned($student);
            
            if ($products_not_owned->get($product_id)) {
                $transaction = $zippy->make('transaction');
                $transaction->setProduct($product);
                $transaction->setOwner($student->getId());
                $transaction->setType('free');
                $transaction->setTotal(0);
                $transaction->setStatus('complete');
                $transaction->save();

                $courses = $product->getCourses();
                foreach ($courses as $key => $course_id) {
                    $courses[$key] = get_the_title($course_id);
                }

                $courses = array();
                $course_ids = $zippy->utilities->product->getCourses($product->getId());

                foreach ($course_ids as $course_id) {
                    $courses[] = get_the_title($course_id);
                    $zippy->activity->insert($student->getId(), $course_id, 'join_course');
                }

                if ($show_message) {
                    $zippy->sessions->flashMessage(
                        sprintf(
                            __('You have joined the following course(s): %s', ZippyCourses::TEXTDOMAIN),
                            '<strong>' . implode(', ', $courses) . '</strong>'
                        )
                    );
                }

                $order = $transaction->getOrder();
                $zippy->events->fire(new ZippyCourses_NewStudent_Event($student, $order));
            }
        }
    }

    public function handleEmailSubscriptions(Zippy_Event $event)
    {
        $zippy = Zippy::instance();

        $order = $event->order;

        $owner = $order->getOwner();
        if (!$owner) {
            return;
        }

        $valid_status = array('complete', 'active');
        $invalid_status = array('fail', 'revoke', 'refund');
        
        $status = $order->getStatus();
        $product = $order->getProduct();

        if ($product === null) {
            return;
        }

        if (in_array($status, $valid_status)) {
            $student = $zippy->make('student', array($owner));
            $zippy->events->fire(new ZippyCourses_JoinProduct_Event($student, $product));
        }

        if (in_array($status, $invalid_status)) {
            $student = $zippy->make('student', array($owner));
            $zippy->events->fire(new ZippyCourses_LeaveProduct_Event($student, $product));
        }
    }
}
