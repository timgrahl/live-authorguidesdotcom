<?php

class Zippy_FormProcessors
{
    private static $instance;

    private $metaboxes;

    private function __construct()
    {
        add_action('template_redirect', array($this, 'process'));
    }

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function process()
    {
        if (isset($_POST['zippy_form'])) {
            $form = $_POST['zippy_form'];
        }
    }
}