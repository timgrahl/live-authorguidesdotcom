<?php

abstract class Zippy_FormProcessor
{
    /**
     * Form to process
     * @var Zippy_Form
     */
    protected $form;

    /**
     * Validator
     * @var Zippy_Validator
     */
    protected $validator;

    /**
     * Store results of form validation for use later
     * @var array
     */
    protected $validation_results = array();

    public function __construct(Zippy_Form $form)
    {
        $zippy = Zippy::instance();

        $this->form = $form;
        $this->validator = $zippy->make('validator');
    }

    abstract public function fail();
    abstract public function execute();

    public function process()
    {
        if ($this->validate()) {
            $this->execute();
        } else {
            $this->fail();
        }

        return;
    }

    /**
     * Determines whether a form is valid based on it's fields validation rules.
     * @return boolean
     */
    public function validate()
    {
        $valid = true;

        if (!empty($_POST)) {
            $this->validateFields();

            foreach ($this->validation_results as $rules) {
                if (!$valid) {
                    continue;
                }

                foreach ($rules as $rule) {
                    if (!$valid) {
                        continue;
                    }

                    if (!$rule) {
                        $valid = false;
                    }
                }
            }
        }

        return $valid;
    }

    /**
     * Gets the Form to process.
     *
     * @return Zippy_Form
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Sets the Form to process.
     *
     * @param Zippy_Form $form the form
     *
     * @return self
     */
    public function setForm(Zippy_Form $form)
    {
        $this->form = $form;

        return $this;
    }

    public function validateFields()
    {
        $fields = $this->form->getFields()->all();

        foreach ($fields as $field) {
            $this->validateField($field);
        }
    }

    public function validateField(Zippy_FormField $field)
    {
        $results = array();

        if (!isset($this->validation_results[$field->getName()])) {
            $this->validation_results[$field->getName()] = array();
        }

        foreach ($field->getRules() as $rule) {
            $results[$rule['type']] = $this->validator->validate($rule['type'], $field->getValue(), $rule['args']);
        }

        $this->validation_results[$field->getName()] = $results;

        return $results;
    }

    public function getValidationMessages()
    {
        $messages = array();

        foreach ($this->form->getFields()->all() as $field) {
            $messages = array_merge($messages, $this->getValidationMessagesForField($field));
        }

        return $messages;
    }

    private function getValidationMessagesForField(Zippy_FormField $field)
    {
        $messages = array();

        if (isset($this->validation_results[$field->getName()])) {
            foreach ($this->validation_results[$field->getName()] as $key => $valid) {
                if (!$valid) {
                    switch($key) {
                        case 'minlength':
                            $rule   = $field->getRule('minlength');
                            $minlength  = $rule['args'][0];

                            $messages[] = sprintf(__('The %s field must be at least %s characters long.', ZippyCourses::TEXTDOMAIN), $field->getLabel(), $minlength);
                            break;
                        case 'maxlength':
                            $rule   = $field->getRule('maxlength');
                            $maxlength  = $rule['args'][0];

                            $messages[] = sprintf(__('The %s field must have more than %s characters.', ZippyCourses::TEXTDOMAIN), $field->getLabel(), $maxlength);
                            break;
                        case 'required':
                            $messages[] = sprintf(__('The %s field is required.', ZippyCourses::TEXTDOMAIN), $field->getLabel());
                            break;
                        case 'username_exists':
                            $messages[] = __('The username does not exist.', ZippyCourses::TEXTDOMAIN);
                            break;
                        case 'email_exists':
                            $messages[] = __('The email does not exist.', ZippyCourses::TEXTDOMAIN);
                            break;
                        case 'unique_username':
                            $messages[] = __('That username is taken.', ZippyCourses::TEXTDOMAIN);
                            break;
                        case 'unique_email':
                            $messages[] = __('That email is taken.', ZippyCourses::TEXTDOMAIN);
                            break;
                        case 'equalTo':
                            $rule = $field->getRule($key);
                            $name = $rule['args'][0];
                            $name = ucwords(str_replace(array('-', '_'), ' ', $name));

                            $messages[] = sprintf(__('The %s field does not match the %s field.', ZippyCourses::TEXTDOMAIN), $field->getLabel(), $name);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        return $messages;
    }
}
