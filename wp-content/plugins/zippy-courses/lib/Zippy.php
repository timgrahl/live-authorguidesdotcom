<?php

class Zippy
{
    /**
     * Singleton instance for our app
     * @var \Zippy
     */
    public static $instance = null;

    /**
     * Instance of our Custom Post Type builder
     * @var \Zippy_CustomPostTypes
     */
    public $post_types;

    /**
     * Instance of our Metabox Builder
     * @var \Zippy_Metaboxes
     */
    public $metaboxes;

    /**
     * Instance of our Metabox Regions
     * @var \Zippy_Metaboxes
     */
    public $metabox_regions;

    /**
     * References for our IoC container
     * @var array
     */
    private $ioc = array();

    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
        // Set up the Inversion of Control item first
        $this->ioc      = Zippy_IoC::instance();

        // Now we can set up our app's dependencies
        $this->api              = $this->make('api');
        $this->log              = $this->make('log');
        $this->customizer       = $this->make('customizer');
        $this->currencies       = $this->make('currencies');
        $this->queries          = $this->make('queries');
        $this->utilities        = $this->make('utilities');
        $this->sessions         = $this->make('sessions');
        $this->events           = $this->make('dispatcher');
        $this->post_types       = $this->make('post_type_repository');
        $this->forms            = $this->make('forms_repository');
        $this->cache            = $this->make('cache');
        $this->access           = $this->make('access');
        $this->activity         = $this->make('activity');
        $this->core_pages       = $this->make('core_pages');
        $this->middleware       = $this->make('middlewares');

        if (is_admin()) {
            $this->admin_pages      = $this->make('admin_pages_repository');
            $this->settings_pages   = $this->make('settings_pages_repository');
            $this->metaboxes        = $this->make('metabox_repository');
            $this->metabox_regions  = $this->make('metabox_regions');
            $this->rss              = $this->make('rss');
            $this->license          = $this->make('license');
        }
    }

    public function make($key, $args = array())
    {
        return $this->ioc->make($key, $args);
    }

    /**
     * Make a new instance from a class file
     *
     * @todo   Add support for singletons
     *
     * @since  1.0.0
     *
     * @param  string   $file       Path to a file
     * @param  array    $args       A list of argument keys that will retrieved from the file header and then passed to
     *                              to the constructor.  Example would be $args = array('id'), $terms = array('id' => 'Id')
     * @param  array    $headers    A list of file headers to look for
     *
     * @uses   get_file_data
     *
     * @return mixed               null | instanceof class contained in $file
     */
    public function makeFromFile($file, $args = array(), $terms = array())
    {
        $terms = array_merge(array('class' => 'Class'), $terms);
        $data = get_file_data($file, $terms);

        if (empty($data['class'])) {
            return null;
        } else {
            if (!class_exists($data['class'])) {
                require_once($file);
            }

            $r   = new ReflectionClass($data['class']);

            $parameters = array();
            foreach ($args as $key) {
                if (isset($data[$key])) {
                    $parameters[$key] =  $data[$key];
                }
            }

            $instance = $r->newInstanceArgs($parameters);

            return $instance;
        }
    }

    public function bind($key, $class, $singleton = false)
    {
        $this->ioc->bind($key, $class, $singleton);
        return $this;
    }

    public static function setup()
    {
        $zippy = Zippy::instance();

        $zippy = self::instance();
        $zippy->make('ZippyCourses');
    }

    public function log($message, $type = 'LOG')
    {
        $this->log->log($message, $type);
    }
}
