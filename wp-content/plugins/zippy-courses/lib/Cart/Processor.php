<?php

class Zippy_TransactionProcessor extends Zippy_Listener
{
    public function __construct()
    {
        $this->register();
    }

    public function register()
    {
        $zippy = Zippy::instance();
        $zippy->events->register('SaveTransaction', $this);
        $zippy->events->register('TransactionStatusChange', $this);
    }

    public function handle(Zippy_Event $event)
    {
        $zippy = Zippy::instance();

        switch ($event->getEventName()) {
            case 'SaveTransaction':
                $this->handleSaveTransaction($event);
                break;

            case 'TransactionStatusChange':
                $this->handleTransactionStatusChange($event);
                break;
            
            default:
                break;
        }
    }

    private function handleSaveTransaction(ZippyCourses_SaveTransaction_Event $event)
    {
        $zippy = Zippy::instance();

        $transaction    = $event->transaction;

        if ($transaction->getStatus() == 'pending' &&
            $transaction->getOrder() === null &&
            $transaction->getRecurringId() !== null
        ) {
            $this->_createTransactionOrder($transaction);
        }
        if ($transaction->getStatus() == 'refunded') {
            $this->refundTransaction($transaction);
        }
    }

    private function handleTransactionStatusChange(ZippyCourses_TransactionStatusChange_Event $event)
    {
        if ($event->new_status == 'complete' && $event->old_status != 'complete') {
            $this->completeTransaction($event->transaction);
        }

        if ($event->new_status == 'refund' && $event->old_status != 'refund') {
            $this->refundTransaction($event->transaction);
        }
    }

    private function refundTransaction(Zippy_Transaction $transaction)
    {
        $transaction->setStatus('refund');
        $transaction->save();
        
        $order = $transaction->getOrder();
        if ($order && $order->allTransactionsAreRefunded()) {
            $order->setStatus('revoke');
            $order->save();
        }
    }

    private function completeTransaction(Zippy_Transaction $transaction)
    {
        $zippy = Zippy::instance();

        if (($order = $transaction->getOrder()) === null) {
            $order = $this->_createTransactionOrder($transaction);
        }

        if ($order->getStatus() !== 'revoke') {
            $order->setStatus($order->calculateStatus());
        }
        
        $order->save();
    }

    /**
     * When a transaction is first completed, it needs an order, so create it here.
     *
     * @since 1.0.0
     * @param  Zippy_Transaction $transaction [description]
     * @return [type]                         [description]
     */
    private function _createTransactionOrder(Zippy_Transaction $transaction)
    {
        $zippy = Zippy::instance();

        $order =  $zippy->make('order');
        $order->addTransaction($transaction);
        $order->setProduct($transaction->getProduct());
        $order->setOwner($transaction->getOwner());
        $order->save();

        $transaction->setOrder($order);
        $transaction->save();

        return $order;
    }
}
