<?php

abstract class Zippy_PaymentGatewayTransactionAdapter
{
    protected $raw;
    protected $normalized;

    public function __construct($data)
    {
        $this->normalize($data);
    }

    public function getNormalizedData()
    {
        return $this->normalized;
    }
    
    abstract public function normalize($input);

    /*
    protected function normalizeProduct($input) {}
    protected function normalizeMode($input) {}
    protected function normalizeGateway($input) {}
    protected function normalizeTax($input) {}
    protected function normalizeTotal($input) {}
    protected function normalizeFee($input) {}
    protected function normalizeCurrency($input) {}
    protected function normalizeType($input) {}
    protected function normalizeMethod($input) {}
    protected function normalizeRecurringId($input) {}
    protected function normalizeRecurring($input) {}
    protected function normalizeStatus($input) {}
    protected function normalizeTimestamp($input) {}
    protected function normalizeCustomer($input) {}
    */
}
