<?php

class Zippy_Currencies
{
        
    /**
     * Non exhausitve list of world currencies
     *
     * Many are commented out as they are not common and not officially supported 
     * by our payment processors, but could be in the future.
     * 
     * @var array [key = country code, value = array( name, ascii )]
     */
    public $currencies = array(
        "USD" => array("US Dollars","0024"),  
        // "ARS" => array("Argentina Pesos","0024"),  
        // "AWG" => array("Aruba Guilders (also called Florins)","0192"),  
        "AUD" => array("Australia Dollars","0024"),  
        // "BSD" => array("Bahamas Dollars","0024"),  
        // "BBD" => array("Barbados Dollars","0024"),  
        // "BEF" => array("Belgium Francs","20a3"),  
        // "BZD" => array("Belize Dollars","0024"),  
        // "BMD" => array("Bermuda Dollars","0024"),  
        // "BOB" => array("Bolivia Bolivianos","0024"),  
        "BRL" => array("Brazil Reais","0052-0024"),  
        // "BRC" => array("Brazil Cruzeiros","20a2"),  
        // "GBP" => array("Britain (United Kingdom) Pounds","00a3"),  
        // "BND" => array("Brunei Darussalam Dollars","0024"),  
        // "KHR" => array("Cambodia Riels","17db"),  
        "CAD" => array("Canada Dollars","0024"),  
        // "KYD" => array("Cayman Islands Dollars","0024"),  
        // "CLP" => array("Chile Pesos","0024"),  
        // "CNY" => array("China Yuan Renminbi","5143"),  
        // "COP" => array("Colombia Pesos","20b1"),  
        // "CRC" => array("Costa Rica Colón","20a1"),  
        // "CUP" => array("Cuba Pesos","20b1"),  
        // "CYP" => array("Cyprus Pounds","00a3"),  
        "DKK" => array("Denmark Kroner","006B-0072"),  
        // "DOP" => array("Dominican Republic Pesos","20b1"),  
        // "XCD" => array("East Caribbean Dollars","0024"),  
        // "EGP" => array("Egypt Pounds","00a3"),  
        // "SVC" => array("El Salvador Colón","20a1"),  
        "GBP" => array("English Pounds","00a3"),  
        "EUR" => array("Euro","20ac"),  
        // "XEU" => array("European Currency Unit","20a0"),  
        // "FKP" => array("Falkland Islands Pounds","00a3"),  
        // "FJD" => array("Fiji Dollars","0024"),  
        // "FRF" => array("France Francs","20a3"),  
        // "GIP" => array("Gibraltar Pounds","00a3"),  
        // "GRD" => array("Greece Drachmae","20af"),  
        // "GGP" => array("Guernsey Pounds","00a3"),  
        // "GYD" => array("Guyana Dollars","0024"),  
        // "NLG" => array("Holland (Netherlands) Guilders (also called Florins)","0192"),  
        "HKD" => array("Hong Kong Dollars","0048-004B-0024"),  
        "HUF" => array( "Hungariant Forint", "0046-0074" ),
        // "INR" => array("India Rupees","20a8"),  
        // "IRR" => array("Iran Rials","fdfc"),  
        // "IEP" => array("Ireland Punt","00a3"),  
        // "IMP" => array("Isle of Man Pounds","00a3"),  
        "ILS" => array("Israeli New Shekels","20aa"),  
        // "ITL" => array("Italy Lire","20a4"),  
        // "JMD" => array("Jamaica Dollars","0024"),  
        "JPY" => array("Japan Yen","00a5"),  
        // "JEP" => array("Jersey Pounds","00a3"),  
        // "KPW" => array("Korea (North) Won","20a9"),  
        // "KRW" => array("Korea (South) Won","20a9"),  
        // "LAK" => array("Laos Kips","20ad"),  
        // "LBP" => array("Lebanon Pounds","00a3"),  
        // "LRD" => array("Liberia Dollars","0024"),  
        // "LUF" => array("Luxembourg Francs","20a3"),  
        // "MYR" => array( "Malaysian Ringgit", "0052-004D")
        // "MTL" => array("Malta Liri","20a4"),  
        // "MUR" => array("Mauritius Rupees","20a8"),  
        "MXN" => array("Mexico Pesos","0024"),  
        // "MNT" => array("Mongolia Tugriks","20ae"),  
        // "NAD" => array("Namibia Dollars","0024"),  
        // "NPR" => array("Nepal Rupees","20a8"),  
        // "ANG" => array("Netherlands Antilles Guilders (also called Florins)","0192"),  
        // "NLG" => array("Netherlands Guilders","0192"),  
        "NOK" => array("Norwegian Kroner","006B-0072"),  
        "NZD" => array("New Zealand Dollars","0024"),  
        // "NGN" => array("Nigeria Nairas","20a6"),  
        // "KPW" => array("North Korea Won","20a9"),  
        // "OMR" => array("Oman Rials","fdfc"),  
        // "PKR" => array("Pakistan Rupees","20a8"),  
        // "PEN" => array("Peru Nuevos Soles","0053-002f-002e"),  
        "PHP" => array("Philippines Pesos","20b1"),  
        "PLN" => array("Polish złoty", "007a-0142"),
        // "QAR" => array("Qatar Rials","fdfc"),  
        "RUB" => array("Russia Rubles","0440-0443-0441"),  
        // "SHP" => array("Saint Helena Pounds","00a3"),  
        // "SAR" => array("Saudi Arabia Riyals","fdfc"),  
        // "SCR" => array("Seychelles Rupees","20a8"),  
        "SGD" => array("Singapore Dollars","0024"),  
        // "SBD" => array("Solomon Islands Dollars","0024"),  
        // "ZAR" => array("South Africa Rand","0052"),  
        // "KRW" => array("South Korea Won","20a9"),  
        // "ESP" => array("Spain Pesetas","20a7"),  
        // "LKR" => array("Sri Lanka Rupees","0bf9"),  
        // "SRD" => array("Suriname Dollars","0024"),  
        "SEK" => array("Sweden Kronor","0053-0045-004B-0020"),
        "CHF" => array("Swiss Franc","0043-0048-0106-0020"),
        // "SYP" => array("Syria Pounds","00a3"),  
        "TWD" => array("Taiwan New Dollars","5143"),  
        "THB" => array("Thailand Baht","0e3f"),  
        // "TTD" => array("Trinidad and Tobago Dollars","0024"),  
        // "TRY" => array("Turkey New Lira","20a4"),  
        "TRL" => array("Turkey Liras","20a4"),  
        // "TVD" => array("Tuvalu Dollars","0024") 
    );

    public function getCurrencies() {
        return $this->currencies;
    }

    public function getSymbol($currency) {

        $c = $this->getCurrency($currency);

        if (!array_key_exists($currency, $this->currencies) || !is_array($c)) {
            return false;
        }

        $output = '';

        $pieces = explode('-', end($c));

        foreach ($pieces as $piece) {
            $output .= html_entity_decode('&#x' . trim($piece)) . ';';
        }

        return $output;
    }

    public function getCurrency($currency)
    {
        return array_key_exists($currency, $this->currencies ) ? $this->currencies[$currency] : false;
    }

}