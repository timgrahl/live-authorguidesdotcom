<?php

/**
 * A Zippy_Order is primarily a repository of transactions that has specialized function
 * for saving order
 */
class Zippy_Order implements Zippy_RepositoryObject
{
    /**
     * WP Post ID
     * @var int|string
     */
    public $id;

    /**
     * Title of Order
     * @var string
     */
    public $title = '';

    /**
     * Transactions for this order
     * @var Zippy_Transactions
     */
    public $transactions;

    /**
     * Customer who purchased the order
     * @var Zippy_Customer
     */
    public $customer;

    /**
     * Date order was
     * @var DateTime
     */
    public $date;

    public $owner = -1;

    /**
     * Product
     * @var Zippy_Product
     */
    public $product;

    /**
     * Cached details
     * @var array
     */
    public $details;

    /**
     * Order Notes
     * @var array
     */
    public $notes;

    public $status;

    /**
     * Student
     * @var Zippy_Student
     */
    public $student;

    public function __construct(Zippy_Transactions $transactions)
    {
        $this->transactions = $transactions;
    }

    public function fetchTransactions()
    {
        return $this->transactions->fetchOrderTransactions($this->id);
    }

    public function addTransaction(Zippy_Transaction $txn)
    {
        $this->transactions->add($txn);
        return $this;
    }

    /**
     * Save this transaction to the database.  Either create it if does not exist,
     * or update it if it does.
     * @return $this
     */
    public function save()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $old_owner = $this->id ? $wpdb->get_var($wpdb->prepare("SELECT post_author FROM $wpdb->posts WHERE ID = %s", $this->id)) : -1;

        if ($this->id) {
            $this->update();
        } else {
            $this->create();
        }

        $new_owner = $this->owner;

        if ($old_owner !== $new_owner) {
            $zippy->events->fire(new ZippyCourses_OrderOwnerChange_Event($this, $old_owner, $new_owner));
        }

        // Fire events
        $zippy->events->fire(new ZippyCourses_OrderSave_Event($this));

        return $this;
    }

    /**
     * Create a new transaction in the database
     *
     * @uses wp_insert_post
     * @uses self::saveMeta
     *
     * @return $this|WP_Error
     */
    private function create()
    {
        global $wpdb;

        $zippy = Zippy::instance();
            
        $this->title = $this->getNextOrderNumber();

        $data = array(
          'post_title'    => $this->title,
          'post_content'  => ' ',
          'post_status'   => 'publish',
          'post_author'   => $this->owner,
          'post_type'     => 'zippy_order'
        );

        $new_order_id = wp_insert_post($data, true);

        if (!is_wp_error($new_order_id)) {
            // Merge and save
            $this->id = $new_order_id;
            $this->saveMeta();

            // Housekeeping
            $this->incrementNextOrderNumber();

            // Fire events
            $zippy->events->fire(new ZippyCourses_NewOrder_Event($this));

            return $this;
        }

        return $new_order_id;
    }

    public function saveMeta()
    {
        $zippy = Zippy::instance();

        $old_status     = get_post_meta($this->id, 'status', true);
        $new_status     = $this->getStatus();

        update_post_meta($this->id, 'status', $new_status);

        if (is_object($this->getProduct())) {
            update_post_meta($this->id, 'product', $this->getProduct()->getId());
        }
        
        update_post_meta($this->id, 'details', $this->getDetails());

        if ($old_status !== $new_status) {
            $zippy->events->fire(new ZippyCourses_OrderStatusChange_Event($this, $old_status, $new_status));
        }
    }

    public function update()
    {
        $data = array(
            'ID'            => $this->id,
            'post_title'    => $this->title,
            'post_author'   => $this->owner
        );

        wp_update_post($data);

        $this->saveMeta();
    }

    public function getUniqueId()
    {
    }

    public function generateUniqueId()
    {
    }


    private function log()
    {
    }

    private function getNextOrderNumber()
    {
        if (!get_option('zippy_order_number')) {
            add_option('zippy_order_number', 100000);
        }

        return get_option('zippy_order_number');
    }

    private function incrementNextOrderNumber()
    {
        $next = $this->getNextOrderNumber();
        $next += 1;

        update_option('zippy_order_number', $next);

        return $this;
    }

    private function getPostArgs()
    {
    }

    public function build($post_id)
    {
        $zippy = Zippy::instance();

        $postdata = get_post($post_id);

        $meta       = array(
            'status'                 => '',
            'product'                => 0,
            'notes'                  => array(),
            'cached_product_details' => array()
        );

        $meta = array_merge($meta, array_map(create_function('$v', 'return maybe_unserialize(reset($v));'), get_post_meta($post_id)));

        $this->id       = $post_id;
        $this->date     = $zippy->utilities->datetime->getDate($postdata->post_date);
        $this->owner    = $postdata->post_author;
        $this->title    = $postdata->post_title;
        $this->status   = $meta['status'];
        $this->notes    = is_array($meta['notes']) ? $meta['notes'] : array();
        $this->product  = !empty($meta['product']) ? $zippy->make('product', array('id' => $meta['product'])) : null;
    }

    public static function getByVendorId($order_id)
    {
    }

    /**
     * Gets the WP Post ID.
     *
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the WP Post ID.
     *
     * @param int|string $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the Transactions for this order.
     *
     * @return Zippy_TransactionRepository
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * Sets the Transactions for this order.
     *
     * @param Zippy_TransactionRepository $transactions the transactions
     *
     * @return self
     */
    public function setTransactions(Zippy_TransactionRepository $transactions)
    {
        $this->transactions = $transactions;

        return $this;
    }

    /**
     * Gets the Customer who purchased the order.
     *
     * @return Zippy_Customer
     */
    public function getCustomer()
    {
        if ($this->customer === null) {
            if (!$this->transactions->count()) {
                $this->fetchTransactions();
            }

            if ($this->transactions->first()) {
                $this->customer = $this->transactions->first()->getCustomer();
            }
        }

        return $this->customer;
    }

    /**
     * Sets the Customer who purchased the order.
     *
     * @param Zippy_Customer $customer the customer
     *
     * @return self
     */
    public function setCustomer(Zippy_Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Gets the Student.
     *
     * @return Zippy_Student
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Sets the Student.
     *
     * @param Zippy_Student $student the student
     *
     * @return self
     */
    public function setStudent(Zippy_Student $student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Gets the value of owner.
     *
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Sets the value of owner.
     *
     * @param mixed $owner the owner
     *
     * @return self
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Check if an Owner is set
     * @return boolean
     */
    public function hasOwner()
    {
        $owner = $this->getOwner();
        return !empty($owner);
    }

    /**
     * Gets the value of status.
     *
     * @return mixed
     */
    public function getStatus()
    {
        if ($this->status === null) {
            $this->status = 'pending';
        }

        return $this->status;
    }

    /**
     * Sets the status of the Order
     *
     * Valid Values: pending|active|complete|refund|cancel|revoke
     *
     * pending   - An order that has been created, but is waiting for a confirmed transaction.
     * active    - A status for recurring Orders that are in good standing, but not completed.
     * complete  - A status for completed single payments or payment plans
     * cancel    - A status for cancelled subcription or payment plans
     * refund    - An order with refunded transactions
     * revoke    - An order that has been revoked manually.
     *
     * @param mixed $status the status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function checkCompletion()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        if (!$this->transactions->count()) {
            $this->transactions->fetchByMeta('order_id', $this->id);
        }

        if (!$this->transactions->count()) {
            return false;
        }

        switch ($this->getType()) {
            case 'single':
                $completed = count($this->transactions->where('status', 'complete'));
                $refunded  = count($this->transactions->where('status', 'refund'));

                return $completed && !$refunded;
                break;

            case 'subscription':
                $num_cancelled      = count($this->transactions->where('status', 'cancelled'));
                return $num_cancelled < 1;
                break;

            case 'payment-plan':
                $num_payments       = $product->getNumPayments();
                $num_transactions   = count($this->transactions->all());
                $num_cancelled      = count($this->transactions->where('status', 'cancelled'));
                
                return $num_transactions >= $num_payments && $num_cancelled < 1;

                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Gets the Product.
     *
     * @return Zippy_Product
     */
    public function getProduct()
    {
        $zippy = Zippy::instance();

        if ($this->product === null) {
            $product_id = get_post_meta($this->getId(), 'product', true);

            if (!empty($product_id)) {
                $this->product = $zippy->make('product', array($product_id));
            }
        }

        return $this->product;
    }

    /**
     * Sets the Product.
     *
     * @param Zippy_Product $product the product
     *
     * @return self
     */
    public function setProduct(Zippy_Product $product)
    {
        $this->product = $product;

        return $this;
    }

    public function getTotal()
    {
        if (count($this->transactions->all()) < 1) {
            $this->fetchTransactions();
        }

        $transaction = $this->transactions->first();

        return $transaction ? $transaction->getTotal() : 0;
    }

    public function getTransactionsTotalString()
    {
        return $this->getCurrencySymbol() . number_format_i18n($this->getTransactionsTotal(), 2);
    }

    public function getTransactionsTotal()
    {
        $zippy = Zippy::instance();

        if (count($this->transactions->all()) < 1) {
            $this->fetchTransactions();
        }
        
        $total = 0;

        foreach ($this->transactions->all() as $transaction) {
            $total += $transaction->getTotal();
        }

        return $total;
    }

    public function getTotalString()
    {
        $zippy = Zippy::instance();

        if (count($this->transactions->all()) < 1) {
            $this->fetchTransactions();
        }

        $transaction = $this->transactions->first();
        if (!$transaction) {
            return '';
        }

        $amount = 0;
        foreach ($this->transactions->all() as $t) {
            $amount += $t->getTotal();
        }

        return $this->getCurrencySymbol() . number_format_i18n($amount, 2);       
    }

    public function getCurrency()
    {
        if (count($this->transactions->all()) < 1) {
            $this->fetchTransactions();
        }

        $transaction = $this->transactions->first();

        return $transaction ? $transaction->getCurrency() : 0;
    }

    public function getCurrencySymbol()
    {
        $zippy = Zippy::instance();
        
        return $zippy->currencies->getSymbol($this->getCurrency());
    }

    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the Date order was.
     *
     * @param DateTime $date the date
     *
     * @return self
     */
    public function setDate(DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    public function getStartDate($course_id)
    {
        $zippy = Zippy::instance();

        $product = $this->getProduct();

        $course = $zippy->make('course', array('id' => $course_id));

        $date = $this->getDate();

        if ($product->getLaunchWindowsEnabled()) {
            foreach ($product->getLaunchWindows() as $lw) {
                $open_date = $zippy->utilities->datetime->getDate($lw->open_date);
                $close_date = $zippy->utilities->datetime->getDate($lw->close_date);
                $start_date = $zippy->utilities->datetime->getDate($lw->start_date);

                if ($date->format('U') >= $open_date->format('U') &&
                    $date->format('U') <= $close_date->format('U')
                ) {
                    $date = clone($start_date);
                    break;
                }
            }
        } else {
            $type = $course->getConfig()->scheduling->getType();
            $start_date = $course->getConfig()->scheduling->getStartDate();

            if ($type == 'drip') {
                $date = $course->getConfig()->scheduling->getStartType() == 'join'
                    ? ($this->getDate() instanceof DateTime
                            ? $this->getDate()
                            : $date
                        )
                    : (!empty($start_date)
                        ? $zippy->utilities->datetime->getDate($start_date)
                        : ($this->getDate() instanceof DateTime
                            ? $this->getDate()
                            : $date
                        ));

                $date->modify('midnight');
            } else {
                $date = $this->getDate() instanceof DateTime
                            ? $this->getDate()
                            : $zippy->utilities->datetime->getToday();
            }
        }

        return $date;
    }

    public function getExpirationDate($course_id)
    {
        $zippy = Zippy::instance();

        $product = $this->getProduct();
        $course = $zippy->make('course', array('id' => $course_id));

        if ($product->getLaunchWindowsEnabled()) {
            $current_date = $this->getDate();
            foreach ($product->getLaunchWindows() as $lw) {
                $open_date          = isset($lw->open_date) ? $zippy->utilities->datetime->getDate($lw->open_date) : $zippy->utilities->datetime->getNow();
                $close_date         = isset($lw->close_date) ? $zippy->utilities->datetime->getDate($lw->close_date) : $zippy->utilities->datetime->getNow();
                $start_date         = isset($lw->start_date) ? $zippy->utilities->datetime->getDate($lw->start_date) : $zippy->utilities->datetime->getNow();
                $expiration_date    = isset($lw->expiration_date) && !empty($lw->expiration_date) ? $zippy->utilities->datetime->getDate($lw->expiration_date) : null;
                
                if ($current_date->format('U') >= $open_date->format('U') &&
                    $current_date->format('U') <= $close_date->format('U')
                ) {
                    $date = $expiration_date instanceof DateTime ? clone($expiration_date) : $expiration_date;
                    break;
                }
                // If no launch window fits this product, don't set an expiration date
                $date = null;
            }
        } elseif ($product->getExpirationOverride()) {
            $start_date = $this->getStartDate($course_id);
            $date = $product->getExpirationDate($start_date);
        } else {
            $type                   = $course->getConfig()->scheduling->getEndType();
            $expiration_date        = $course->getConfig()->scheduling->getExpirationDate();
            $expiration_duration    = $course->getConfig()->scheduling->getExpirationDuration();
            $start_date             = $course->getConfig()->scheduling->getStartDate();
            switch ($type) {
                case 'date':
                    $date = clone($expiration_date);
                    break;
                case 'duration':
                    $date = $this->getStartDate($course->getId());
                    $date->modify("+{$expiration_duration} days");
                    break;
                case 'none':
                default:
                    $date = null;
                    break;
            }
        }
        return $date;
    }

    public function getGateway()
    {
        if (!$this->transactions->count()) {
            $this->fetchTransactions();
        }

        $transaction = $this->transactions->first();

        return $transaction !== null ? $transaction->getGateway() : null;
    }

    public function getDetails()
    {
        if ($this->details === null) {
            if (!$this->transactions->count()) {
                $this->fetchTransactions();
            }

            $transaction = $this->transactions->first();

            if ($transaction) {
                $this->details = array(
                    'type'          => $transaction->getType(),
                    'recurring'     => $transaction->getRecurring(),
                    'num_payments'  => $transaction->getNumPayments(),
                    'amount'        => $transaction->getTotal(),
                );
            } else {
                $this->details = array();
            }
        }

        return $this->details;
    }

    /**
     * Sets the Cached details.
     *
     * @param array $details the details
     *
     * @return self
     */
    public function setDetails(array $details)
    {
        $this->details = $details;

        return $this;
    }

    public function getType()
    {
        $details = $this->getDetails();

        return isset($details['type']) ? $details['type'] : 'single';
    }

    /**
     * Gets the the string of the type
     *
     * @return mixed
     */
    public function getTypeLabel()
    {
        $label = '';

        switch ($this->getType()) {
            case 'free':
                $label = __('Free', ZippyCourses::TEXTDOMAIN);
                break;
            case 'single':
                $label = __('Single Payment', ZippyCourses::TEXTDOMAIN);
                break;
            case 'subscription':
                $label = __('Subscription', ZippyCourses::TEXTDOMAIN);
                break;
            case 'payment_plan':
            case 'payment-plan':
                $label = __('Payment Plan', ZippyCourses::TEXTDOMAIN);
                break;
            default:
                break;
        }

        return $label;
    }

    public function getNumPayments()
    {
        $details = $this->getDetails();

        return $details['num_payments'];
    }

    /**
     * Determines the current status of the order
     * @return [type] [description]
     */
    public function calculateStatus()
    {
        if ($this->getStatus() == 'revoke') {
            return 'revoke';
        }

        switch ($this->getType()) {
            case 'free':
            case 'single':
                $status = $this->transactions->first()->getStatus();
                break;

            case 'subscription':
                $num_cancelled      = count($this->transactions->where('status', 'cancel'));
                $status = $num_cancelled < 1 ? 'active' : 'cancel';
                break;

            case 'payment-plan':
                $num_payments       = $this->getNumPayments();
                $num_transactions   = $this->transactions->count();
                $num_cancelled      = count($this->transactions->where('status', 'cancel'));
                $num_completed      = count($this->transactions->where('status', 'complete'));
                $num_refunded       = count($this->transactions->where('status', 'refund'));
                    
                if ($num_cancelled > 0) {
                    $status = 'cancel';
                    break;
                }

                if ($num_completed >= $num_payments) {
                    $status = 'complete';
                } else {
                    $status = 'active';
                }

                break;
            default:
                $status = 'pending';
                break;
        }

        return $status;
    }

    /**
     * Gets the Title of Order.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the Title of Order.
     *
     * @param string $title the title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Gets the Order Notes.
     *
     * @return array
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Sets the Order Notes.
     *
     * @param array $notes the notes
     *
     * @return self
     */
    public function addNote(array $note)
    {
        if (is_array($note) &&
            isset($note['content']) &&
            isset($note['timestamp'])
        ) {
            $this->notes[] = $note;
        }

        return $this;
    }

    public function saveNotes()
    {
        update_post_meta($this->getId(), 'notes', $this->getNotes());
    }

    /**
     * Check if all the Transactions in an Order have been refunded
     * @return bool
     */
    public function allTransactionsAreRefunded()
    {
        $this->fetchTransactions();
        $num_cancelled      = count($this->transactions->where('status', 'cancel'));
        $num_completed      = count($this->transactions->where('status', 'complete'));
        $num_refunded       = count($this->transactions->where('status', 'refund'));

        if ($num_completed === 0 && $num_refunded > 0) {
            return true;
        }
        return false;
    }
}
