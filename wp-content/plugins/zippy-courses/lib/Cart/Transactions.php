<?php

class Zippy_Transactions extends Zippy_Repository
{
    /**
     * A list of the valid repository object types
     * @var array
     */
    public $valid_types = array('Zippy_Transaction');

    public function getTotal()
    {
        $total = 0;

        foreach ($this->all() as $txn) {
            $total += $txn->getTotal();
        }

        return $total;
    }

    public function fetchOrderTransactions($order_id = 0)
    {
        $this->fetchByMeta('order_id', $order_id);
        
        return $this->all();
    }

    public function fetchByOwner($user_id)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_author = %d AND post_type = %s ORDER BY ID ASC", $user_id, 'transaction');
        $post_ids = $wpdb->get_col($sql);

        foreach ($post_ids as $post_id) {
            $transaction = $zippy->make('transaction');
            $transaction->build($post_id);

            $this->add($transaction);
        }

        return $this->all();
    }
}
