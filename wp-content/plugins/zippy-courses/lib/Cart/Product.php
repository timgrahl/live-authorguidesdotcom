<?php

class Zippy_Product implements Zippy_RepositoryObject
{
    public $id;
    public $uid;
    public $title;
    public $content;
    public $courses_and_tiers = array();
    public $launch_windows = array();
    public $launch_windows_enabled = false;
    /**
     * @var WP_Post
     */
    public $data;
    public $date;
    public $type;
    public $pricing;
    public $frequency;
    public $num_payments;
    public $amount;
    
    public function __construct($id)
    {
        $this->id = $id;

        $this->fetchPostdata();
        $this->fetchPricing();
        $this->fetchCourseAndTier();
        $this->fetchLaunchWindows();
        $this->fetchExistingAccess();
    }

    public function getId()
    {
        return $this->id;
    }

    public function hasExistingAccessRequirement()
    {
        return ($this->existing_access->enabled && $this->existing_access->required_access_enabled);
    }

    public function hasRevokedCourse()
    {
        return ($this->existing_access->enabled && $this->existing_access->revoked_access_enabled);
    }

    public function getRequiredCourses()
    {
        return $this->existing_access->required_access_courses;
    }

    public function getRevokedCourses()
    {
        return $this->existing_access->revoked_access_courses;
    }

    public function getData($key = '')
    {
        return isset($this->data) && isset($this->data->{$key}) ? $this->data->{$key} : $this->data;
    }

    public function fetchPostdata()
    {
        $data = get_post($this->id);

        if ($data) {
            $this->setData($data);
            $this->title    = $data->post_title;
            $this->content  = $data->post_content;
        }
    }

    public function fetchExistingAccess()
    {
        $zippy = Zippy::instance();
        $existing_access = $zippy->utilities->getJsonMeta($this->id, 'product_existing_access');

        $this->existing_access = new stdClass;
        $this->existing_access->enabled = isset($existing_access->enabled) ? $existing_access->enabled : false;
        
        $this->existing_access->required_access_enabled = isset($existing_access->requiredAccess)
            ? $existing_access->requiredAccess : false;

        $this->existing_access->required_access_courses = isset($existing_access->requiredCourses)
            ? $existing_access->requiredCourses : array();
        
        $this->existing_access->revoked_access_enabled = isset($existing_access->revokeAccess)
            ? $existing_access->revokeAccess : false;

        $this->existing_access->revoked_access_courses = isset($existing_access->revokedCourses)
            ? $existing_access->revokedCourses : array();
    }

    public function fetchCourseAndTier()
    {
        $zippy = Zippy::instance();

        $this->courses_and_tiers = $zippy->utilities->product->getCoursesAndTiers($this->id);

        return $this;
    }

    public function fetchPricing()
    {
        $zippy = Zippy::instance();

        $pricing = $zippy->utilities->getJsonMeta($this->id, 'pricing');

        if ($pricing) {
            $this->type = $pricing->type;

            switch ($this->type) {
                case 'subscription':
                    $this->amount = $pricing->price->subscription->amount;
                    $this->frequency = $pricing->price->subscription->frequency;
                    $this->num_payments = 0; // 0 = recurring indefinitely
                    break;
                case 'payment-plan':
                    $this->amount = $pricing->price->payment_plan->amount;
                    $this->frequency = $pricing->price->payment_plan->frequency;
                    $this->num_payments = $pricing->price->payment_plan->num_payments;
                    break;
                case 'single':
                default:
                    $this->amount = $pricing->price->single->amount;
                    $this->num_payments = 1;
                    break;
            }
        }
    }

    /**
     * Sets the value of id.
     *
     * @param mixed $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the value of title.
     *
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets the value of title.
     *
     * @param mixed $title the title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Gets the the type of Product (single|subscription|payment-plan).
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Gets the the string of the type
     *
     * @return mixed
     */
    public function getTypeLabel()
    {
        $label = '';

        switch ($this->getType()) {
            case 'free':
                $label = __('Free', ZippyCourses::TEXTDOMAIN);
                break;
            case 'single':
                $label = __('Single Payment', ZippyCourses::TEXTDOMAIN);
                break;
            case 'subscription':
                $label = __('Subscription', ZippyCourses::TEXTDOMAIN);
                break;
            case 'payment_plan':
            case 'payment-plan':
                $label = __('Payment Plan', ZippyCourses::TEXTDOMAIN);
                break;
            default:
                break;
        }

        return $label;
    }

    /**
     * Sets the the type of Product (single|subscription|payment-plan).
     *
     * @param mixed $type the type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets the value of frequency.
     *
     * @return mixed
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Sets the value of frequency.
     *
     * @param mixed $frequency the frequency
     *
     * @return self
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Gets the value of num_payments.
     *
     * @return mixed
     */
    public function getNumPayments()
    {
        return $this->num_payments;
    }

    /**
     * Sets the value of num_payments.
     *
     * @param mixed $num_payments the num payments
     *
     * @return self
     */
    public function setNumPayments($num_payments)
    {
        $this->num_payments = $num_payments;

        return $this;
    }

    /**
     * Gets the value of amount.
     *
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Gets the value of amount.
     *
     * @return mixed
     */
    public function getPriceString()
    {
        $zippy = Zippy::instance();

        $currency = $zippy->utilities->cart->getCurrency();
        $symbol = $zippy->currencies->getSymbol($currency);

        $string = '';

        switch ($this->getType()) {
            case 'subscription':
                $string = $symbol . number_format_i18n($this->amount, 2) . ' ' . __('per', ZippyCourses::TEXTDOMAIN) . ' ';

                switch ($this->getFrequency()) {
                    case 'year':
                        $string .= __('year', ZippyCourses::TEXTDOMAIN);
                        break;
                    case 'week':
                        $string .= __('week', ZippyCourses::TEXTDOMAIN);
                        break;
                    case 'day':
                        $string .= __('day', ZippyCourses::TEXTDOMAIN);
                        break;
                    case 'month':
                    default:
                        $string .= __('month', ZippyCourses::TEXTDOMAIN);
                        break;
                }
                
                break;
            case 'payment_plan':
            case 'payment-plan':
                $string = $symbol . number_format_i18n($this->amount, 2) . ' ' . __('per', ZippyCourses::TEXTDOMAIN) . ' ';

                switch ($this->getFrequency()) {
                    case 'year':
                        $string .= __('year', ZippyCourses::TEXTDOMAIN);
                        break;
                    case 'week':
                        $string .= __('week', ZippyCourses::TEXTDOMAIN);
                        break;
                    case 'day':
                        $string .= __('day', ZippyCourses::TEXTDOMAIN);
                        break;
                    case 'month':
                    default:
                        $string .= __('month', ZippyCourses::TEXTDOMAIN);
                        break;
                }

                switch ($this->getFrequency()) {
                    case 'year':
                        $frequency = _n('year', 'years', $this->getNumPayments(), ZippyCourses::TEXTDOMAIN);
                        break;
                    case 'week':
                        $frequency = _n('week', 'weeks', $this->getNumPayments(), ZippyCourses::TEXTDOMAIN);
                        break;
                    case 'day':
                        $frequency = _n('day', 'days', $this->getNumPayments(), ZippyCourses::TEXTDOMAIN);
                        break;
                    case 'month':
                    default:
                        $frequency = _n('month', 'months', $this->getNumPayments(), ZippyCourses::TEXTDOMAIN);
                        break;
                }

                $string .= ' ' . __('for', ZippyCourses::TEXTDOMAIN) . ' ' . $this->getNumPayments() . ' ' . $frequency;
                
                break;
            case 'free':
                $string = __('Free!', ZippyCourses::TEXTDOMAIN);
                break;
            case 'single':
            default:
                $string = $symbol . number_format_i18n($this->amount, 2);
                break;
        }

        return apply_filters('zippy_filter_product_price_text', $string, $this);
    }

    /**
     * Sets the value of amount.
     *
     * @param mixed $amount the amount
     *
     * @return self
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Sets the value of data.
     *
     * @param WP_Post $data the data
     *
     * @return self
     */
    public function setData(WP_Post $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Gets the value of course.
     *
     * @return mixed
     */
    public function getCoursesAndTiers()
    {
        return $this->courses_and_tiers;
    }

    /**
     * Gets the value of course.
     *
     * @return mixed
     */
    public function getCourses()
    {
        $courses = array();

        foreach ($this->courses_and_tiers as $course) {
            if (!empty($course['tiers'])) {
                $courses[] = $course['id'];
            }
        }

        return $courses;
    }

    /**
     * Sets the value of course.
     *
     * @param mixed $course the course
     *
     * @return self
     */
    public function setCourse($course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Gets the value of tier.
     *
     * @return mixed
     */
    public function getTier()
    {
        return $this->tier;
    }

    /**
     * Sets the value of tier.
     *
     * @param mixed $tier the tier
     *
     * @return self
     */
    public function setTier($tier)
    {
        $this->tier = $tier;

        return $this;
    }

    public function fetchLaunchWindows()
    {
        $zippy = Zippy::instance();

        $launch_windows = $zippy->utilities->getJsonMeta($this->id, 'launch_windows');

        if (!empty($launch_windows)) {
            $this->launch_windows = $launch_windows->launch_windows;
            $this->launch_windows_enabled = $launch_windows->enabled;
        }
    }

    public function getLaunchWindows()
    {
        return $this->launch_windows_enabled ? $this->launch_windows : null;
    }

    /**
     * Gets the value of launch_windows_enabled.
     *
     * @return mixed
     */
    public function getLaunchWindowsEnabled()
    {
        return $this->launch_windows_enabled;
    }

    /**
     * Sets the value of launch_windows_enabled.
     *
     * @since 1.0.0
     *
     * @param boolean $launch_windows_enabled
     *
     * @return self
     */
    public function setLaunchWindowsEnabled($launch_windows_enabled)
    {
        $this->launch_windows_enabled = $launch_windows_enabled;

        return $this;
    }

    public function getCurrency()
    {
        $zippy = Zippy::instance();

        return $zippy->utilities->cart->getCurrency();
    }

    /**
     * Gets the value of uid.
     *
     * @return mixed
     */
    public function getUid()
    {
        if ($this->uid === null) {
            $this->uid = $this->fetchUid();
        }

        return $this->uid;
    }

    /**
     * Sets the value of uid.
     *
     * @param mixed $uid the uid
     *
     * @return self
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    public function fetchUid()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $uid = get_post_meta($this->getId(), 'uid', true);

        if (!empty($uid)) {
            return $uid;
        }

        $uid        = $zippy->utilities->generateUniqueAlphanumeric(8);
        $meta_key   = 'uid';

        $exists = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT meta_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s",
                $meta_key,
                $uid
            )
        );

        if ($exists === null) {
            add_post_meta($this->getId(), $meta_key, $uid, false);
            return $uid;
        } else {
            return $this->fetchUid();
        }
    }

    public function isFree()
    {
          return $this->getType() == 'free' || ($this->getType() == 'single' && $this->getAmount() == 0);
    }

    public function getExpirationOverride()
    {
        return get_post_meta($this->getId(), 'overrides_enabled', true) == '1' && get_post_meta($this->getId(), 'expiration_override', true) == '1';
    }

    public function getExpirationDate(DateTime $start_date)
    {
        $overrides = get_post_meta($this->getId(), 'overrides_enabled', true);
        if ($overrides !== '1') {
            return null;
        }

        $expiration_override = get_post_meta($this->getId(), 'expiration_override', true);
        $expiration = get_post_meta($this->getId(), 'expiration', true);

        if ($expiration_override && $expiration !== '') {
            $expiration_date = clone($start_date);

            $expiration_date->modify("+{$expiration} days");
            return $expiration_date;
        }

        return null;
    }

    public function setEmailContent(Zippy_Email $email)
    {
        $message_type = $email->type;
        $email->content = $this->getCustomMessage($email->content, $message_type);
        $email->title = $this->getCustomEmailSubject($email->title, $message_type);
        return $this;
    }

    public function getCustomMessage($default_message, $message_type)
    {
        $zippy = Zippy::instance();
        $messages = $zippy->utilities->getJsonMeta($this->id, 'confirmation_messages');
        if (empty($messages)) {
            return $default_message;
        }
        
        switch ($message_type) {
            case 'thank_you':
                $enabled = $messages->custom_thank_you_enable;
                $message = $messages->custom_thank_you_text;
                break;
            case 'registration':
                $enabled = $messages->custom_registration_enable;
                $message = $messages->custom_registration_text;
                break;
            case 'purchase_receipt':
                $enabled = $messages->custom_purchase_receipt_enable;
                $message = $messages->custom_purchase_receipt_text;
                break;
            case 'registration_reminder':
                $enabled = $messages->custom_registration_reminder_enable;
                $message = $messages->custom_registration_reminder_text;
                break;
            default:
                return $default_message;
                break;

        }
        if ($enabled && !empty($message)) {
            return $message;
        }
        return $default_message;
    }
    public function getCustomEmailSubject($default_subject, $message_type)
    {
        $zippy = Zippy::instance();
        $messages = $zippy->utilities->getJsonMeta($this->id, 'confirmation_messages');
        switch ($message_type) {
            case 'registration':
                $enabled = $messages->custom_registration_enable;
                $subject = $messages->custom_registration_subject;
                break;
            case 'purchase_receipt':
                $enabled = $messages->custom_purchase_receipt_enable;
                $subject = $messages->custom_purchase_receipt_subject;
                break;
            case 'registration_reminder':
                $enabled = $messages->custom_registration_reminder_enable;
                $subject = $messages->custom_registration_reminder_subject;
                break;
            default:
                return $default_subject;
                break;

        }
        if ($enabled && !empty($subject)) {
            return $subject;
        }
        return $default_subject;
    }
}
