<?php

class Zippy_Transaction implements Zippy_RepositoryObject
{
    /**
     * Post ID
     * @var string|int
     */
    public $id;

    /**
     * Post Title
     * @var string
     */
    public $title;

    /**
     * Transaction Key
     * @var string
     */
    public $key;

    /**
     * Payment gateway
     * @var string
     */
    public $gateway;

    /**
     * Gateway Vendor ID
     * @var string
     */
    public $vendor_id;

    /**
     * Customer who completed the transaction
     * @var Zippy_Customer
     */
    public $customer;

    /**
     * Owner
     * @var int
     */
    public $owner = -1;

    /**
     * Product that transaction is attached to
     * @var Zippy_Product
     */
    public $product;

    /**
     * Order that the Transaction is associated with
     * @var Zippy_Order
     */
    public $order;

    /**
     * Timestamp for the transaction
     * @var DateTime
     */
    public $timestamp;

    /**
     * Status of the transaction
     * @var string
     */
    public $status;


    /**
     * Transaction's currency
     * @var string
     */
    public $currency;

    /**
     * How much was charged for the transaction
     * @var float
     */
    public $total;

    /**
     * Fee associated with the transaction
     * @var float
     */
    public $fee;
    
    /**
     * How much tax was paid for transaction
     * @var float
     */
    public $tax;

    /**
     * 'live' or 'test'
     * @var string
     */
    public $mode;

    /**
     * 'gateway' or 'card'
     * @var string
     */
    public $method;

    /**
     * The type of transaction (payment|refund)
     * @var string
     */
    public $type;

    /**
     * Is this a recurring transaction
     * @var boolean
     */
    public $recurring;

    /**
     * The ID of the recurring set of transactions
     * @var int
     */
    public $recurring_id;

    /**
     * The number of payments in a recurring setup
     * @var int
     */
    public $num_payments;

    /**
     * The raw data that created the transaction
     * @var int
     */
    public $raw;

    /**
     * Product details may change, so we include a cached version of the product with every Transaction, which can be used
     * until an Order is created to maintain consistent permissions and completion details.
     *
     * @since 1.0.0
     *
     * @var array
     */
    public $cached_product_details;

    public function __construct(array $data = array())
    {
        $zippy = Zippy::instance();

        $this->currency = $zippy->utilities->cart->getCurrency();
        
        $this->importData($data);
        $this->timestamp = $zippy->utilities->datetime->getNow();
    }

    public function importData(array $data = array())
    {
        $zippy = Zippy::instance();

        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                if ($key == 'product') {
                    $this->{$key} = $value instanceof Zippy_Product ? $value : $zippy->make('product', array('id' => $value));
                } elseif ($key == 'timestamp') {
                    $this->{$key} = $value instanceof DateTime ? $value : $zippy->utilities->datetime->getDateFromTimestamp($value);

                    // TODO: Standardize timezone here
                    $this->{$key}->setTimezone($zippy->utilities->datetime->getTimezone());
                } else {
                    $this->{$key} = $value;
                }
            }
        }
    }

    /**
     * Fetch a transaction from the database
     * @param  int|string $post_id
     * @return new self
     */
    public function build($post_id)
    {
        $zippy = Zippy::instance();

        $postdata = get_post($post_id);

        $customer   = $zippy->make('customer');
        $order      = $zippy->make('order');

        $meta       = array(
            'order_id' => '',
            'vendor_id' => '',
            'vendor' => '',
            'recurring' => false,
            'recurring_id' => 0,
            'customer' => '',
            'transaction_key' => '',
            'details' => array(),
        );

        $meta = array_merge($meta, array_map(create_function('$v', 'return maybe_unserialize(reset($v));'), get_post_meta($post_id)));

        $this->id                       = $post_id;
        $this->title                    = get_the_title($this->id);
        $this->key                      = $meta['transaction_key'];
        $this->gateway                  = $meta['vendor'];
        $this->vendor_id                = $meta['vendor_id'];
        $this->recurring                = (bool) $meta['recurring'];
        $this->recurring_id             = $meta['recurring_id'];
        $this->customer                 = $customer->importData((array) $meta['customer']);
        $this->owner                    = $postdata->post_author > 0 ? $postdata->post_author : -1;
        $this->currency                 = isset($meta['details']['currency']) ? $meta['details']['currency'] : null;
        $this->total                    = isset($meta['details']['amount']) ? $meta['details']['amount'] : null;

        $order_id = !empty($meta['order_id']) ? $meta['order_id'] : false;

        if ($order_id) {
            $order->build($meta['order_id']);
            $this->order        = $order;
        }

        $this->importData($meta['details']);
    }

    /**
     * Save this transaction to the database.  Either create it if does not exist,
     * or update it if it does.
     * @return $this
     */
    public function save()
    {
        $zippy = Zippy::instance();

        if ($this->id) {
            $this->update();
        } else {
            $this->create();
        }

        // Fire events
        $event = new ZippyCourses_SaveTransaction_Event($this);
        $zippy->events->fire($event);

        return $this;
    }

    /**
     * Create a new transaction in the database
     *
     * @uses wp_insert_post
     * @uses self::saveMeta
     *
     * @return $this|WP_Error
     */
    private function create()
    {
        global $wpdb;

        $zippy = Zippy::instance();
        
        $this->title = $this->getNextTransactionNumber();

        $data = array(
          'post_title'    => $this->title,
          'post_content'  => ' ',
          'post_status'   => 'publish',
          'post_author'   => $this->owner,
          'post_type'     => 'transaction'
        );

        $new_txn_id = wp_insert_post($data, true);

        if (!is_wp_error($new_txn_id)) {
            // Merge and save
            $this->id = $new_txn_id;
            $this->saveMeta();

            // Housekeeping
            $this->incrementNextTransactionNumber();

            return $this;
        }

        return $new_txn_id;
    }

    /**
     * Updates an existing transaction in the database
     *
     * @uses wp_update_post
     * @uses self::saveMeta
     *
     * @return $this|WP_error
     */
    private function update()
    {
        $zippy = Zippy::instance();

        $data = array(
            'ID'            => $this->id,
            'post_author'   => $this->owner
        );

        wp_update_post($data);

        $this->saveMeta();
    }

    /**
     * Save the meta for this transaction
     *
     * @uses update_post_meta
     * @uses self::getDetails
     *
     * @return void
     */
    private function saveMeta()
    {
        $zippy = Zippy::instance();

        $details    = (array) get_post_meta($this->id, 'details', true);

        update_post_meta($this->id, 'details', $this->getDetails());
        update_post_meta($this->id, 'customer', $this->getCustomer());
        update_post_meta($this->id, 'vendor', $this->getGateway());
        update_post_meta($this->id, 'vendor_id', $this->getVendorId());
        update_post_meta($this->id, 'transaction_key', $this->getKey());
        update_post_meta($this->id, 'recurring', $this->getRecurring());
        update_post_meta($this->id, 'recurring_id', $this->getRecurringId());

        if ($this->order) {
            update_post_meta($this->id, 'order_id', $this->order->id);
        }

        // Check for Status Change
        $old_status     = isset($details['status']) ? $details['status'] : '';
        $new_status     = $this->status;

        if ($old_status !== $new_status) {
            $zippy->events->fire(new ZippyCourses_TransactionStatusChange_Event($this, $old_status, $new_status));
        }
    }

    public function getByVendorId()
    {
        if (!$this->vendor_id) {
            return false;
        }

        global $wpdb;

        $existing = new WP_Query(array(
            'post_type' => 'transaction',
            'meta_query' => array(
                array(
                    'key' => 'vendor',
                    'value' => $this->gateway
                ),
                array(
                    'key' => 'vendor_id',
                    'value' => $this->vendor_id
                )
            )
        ));

        return empty($existing->posts) ? false : reset($existing->posts);
    }

    public function buildByTransactionKey($key)
    {
        global $wpdb;

        $post_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'transaction_key', $key));

        return $post_id ? $this->build($post_id) : false;
    }

    /**
     * Merge the existing transaction data with this one
     * @param  WP_Post $post
     * @return $this
     */
    private function mergeWithExisting(WP_Post $post)
    {
        $this->id = $post->id;
        $this->title = $post->post_title;

        return $this;
    }

    private function getNextTransactionNumber()
    {
        if (!get_option('zippy_transaction_number')) {
            add_option('zippy_transaction_number', 100000);
        }

        return get_option('zippy_transaction_number');
    }

    private function incrementNextTransactionNumber()
    {
        $next = $this->getNextTransactionNumber();
        $next += 1;

        update_option('zippy_transaction_number', $next);

        return $this;
    }

    public function getDetails()
    {
        return array(
            'vendor_id'             => $this->getVendorId(),
            'total'                 => $this->getTotal(),
            'fee'                   => $this->getFee(),
            'tax'                   => $this->getTax(),
            'currency'              => $this->getCurrency(),
            'type'                  => $this->getType(),
            'timestamp'             => $this->getTimestamp()->format('U'),
            'status'                => $this->getStatus(),
            'mode'                  => $this->getMode(),
            'method'                => $this->getMethod(),
            'num_payments'          => $this->getNumPayments(),
            'product'               => $this->getProduct()->getId(),
            'raw'                   => $this->getRaw(),
        );
    }

    /**
     * Gets the Payment gateway.
     *
     * @return string
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * Sets the Payment gateway.
     *
     * @param $gateway the gateway
     *
     * @return self
     */
    public function setGateway($gateway)
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Gets the Customer who completed the transaction.
     *
     * @return Zippy_Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Sets the Customer who completed the transaction.
     *
     * @param Zippy_Customer $customer the customer
     *
     * @return self
     */
    public function setCustomer(Zippy_Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Gets the Product that transaction is attached to.
     *
     * @return Zippy_Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Sets the Product that transaction is attached to.
     *
     * @param Zippy_Product $product the product
     *
     * @return self
     */
    public function setProduct(Zippy_Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Gets the Order that the Transaction is associated with.
     *
     * @return Zippy_Order
     */
    public function getOrder()
    {
        return $this->order ? $this->order : $this->fetchOrder();
    }

    public function fetchOrder()
    {
        if ($this->id) {
            $zippy = Zippy::instance();
            
            $order_id = get_post_meta($this->id, 'order_id', true);

            if (!empty($order_id)) {
                $this->order = $zippy->make('order');
                $this->order->build($order_id);

                return $this->order;
            }
        }

        return;
    }

    /**
     * Sets the Order that the Transaction is associated with.
     *
     * @param Zippy_Order $order the order
     *
     * @return self
     */
    public function setOrder(Zippy_Order $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Gets the Status of the transaction.
     *
     * @return string
     */
    public function getStatus()
    {
        if ($this->status === null) {
            $this->status = 'pending';
        }

        return $this->status;
    }

    /**
     * Sets the Status of the transaction.
     *
     * Valid Values: pending|complete|refund|cancel
     *
     * pending  - An order that has been created, but is waiting for a confirmed transaction.
     * active   - A status for recurring Orders that are in good standing, but not completed.
     * complete - A status for completed single payments or payment plans
     * refund   - An order with refunded transactions
     * fail     - A status for failed transactions
     * cancel   - A status for recurring transactions that have been cancelled
     *
     * @param string $status the status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Gets the Fee associated with the transaction.
     *
     * @return float
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Sets the Fee associated with the transaction.
     *
     * @param float $fee the fee
     *
     * @return self
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Gets the How much was charged for the transaction.
     *
     * @return float
     */
    public function getTotal()
    {
        if ($this->total === null && $this->product instanceof Zippy_Product) {
            $this->total = $this->product->getAmount();
        }

        return $this->total;
    }

    public function getTotalString()
    {
        return $this->getCurrencySymbol() . number_format_i18n($this->getTotal(), 2);
    }

    /**
     * Sets the How much was charged for the transaction.
     *
     * @param float $total the total
     *
     * @return self
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Gets the How much tax was paid for transaction.
     *
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Sets the How much tax was paid for transaction.
     *
     * @param float $tax the tax
     *
     * @return self
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Gets the 'live' or 'test'.
     *
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Sets the 'live' or 'test'.
     *
     * @param string $mode the mode
     *
     * @return self
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Gets the The type of transaction (single|subscription|payment-plan).
     *
     * @return string
     */
    public function getType()
    {
        if ($this->type === null && $this->product instanceof Zippy_Product) {
            $this->type = $this->product->getType();
        }
        
        return $this->type;
    }

    /**
     * Sets the The type of transaction (single|subscription|payment-plan).
     *
     * @param string $type the type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets the Is this a recurring transaction.
     *
     * @return boolean
     */
    public function getRecurring()
    {
        return $this->recurring;
    }

    /**
     * Sets the Is this a recurring transaction.
     *
     * @param boolean $recurring the recurring
     *
     * @return self
     */
    public function setRecurring($recurring)
    {
        $this->recurring = $recurring;

        return $this;
    }

    /**
     * Gets the The ID of the recurring set of transactions.
     *
     * @return int
     */
    public function getRecurringId()
    {
        return $this->recurring_id;
    }

    /**
     * Sets the The ID of the recurring set of transactions.
     *
     * @param int $recurring_id the recurring id
     *
     * @return self
     */
    public function setRecurringId($recurring_id)
    {
        $this->recurring_id = $recurring_id;

        return $this;
    }

    /**
     * Sets the payment method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Sets the payment method
     *
     * @param string $method the method
     *
     * @return self
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Gets the Timestamp for the transaction.
     *
     * @return DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Sets the Timestamp for the transaction.
     *
     * @param DateTime $timestamp the timestamp
     *
     * @return self
     */
    public function setTimestamp(DateTime $timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Gets the Gateway Vendor ID.
     *
     * @return string
     */
    public function getVendorId()
    {
        return $this->vendor_id;
    }

    /**
     * Sets the Gateway Vendor ID.
     *
     * @param string $vendor_id the vendor id
     *
     * @return self
     */
    public function setVendorId($vendor_id)
    {
        $this->vendor_id = $vendor_id;

        return $this;
    }

    /**
     * Gets the Post ID.
     *
     * @return string|int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the Post ID.
     *
     * @param string|int $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the Post Title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the Post Title.
     *
     * @param string $title the title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Sets the The raw data that created the transaction.
     *
     * @param int $raw the raw data
     *
     * @return self
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Gets the The raw data that created the transaction.
     *
     * @return int
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Gets the Transaction's currency.
     *
     * @return string
     */
    public function getCurrency()
    {
        if ($this->currency === null) {
            $this->currency = $this->product->getCurrency();
        }

        return $this->currency;
    }

    public function getCurrencySymbol()
    {
        $zippy = Zippy::instance();
        
        return $zippy->currencies->getSymbol($this->getCurrency());
    }

    /**
     * Sets the Transaction's currency.
     *
     * @param string $currency the currency
     *
     * @return self
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Gets the Transaction Key.
     *
     * @return string
     */
    public function getKey()
    {
        if ($this->key === null) {
            $this->key = $this->generateKey();
        }

        return $this->key;
    }

    /**
     * Sets the Transaction Key.
     *
     * @param string $key the key
     *
     * @return self
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    public function generateKey($length = 8)
    {
        global $wpdb;

        $chars      = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
        $key        = '';

        for ($i=0; $i < $length; $i++) {
            $key .= $chars[rand(0, (count($chars) - 1))];
        }

        $exists = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", 'transaction_key', $key));

        return $exists === null ? $key : $this->generateKey();
    }

    /**
     * Gets the Owner.
     *
     * @return int
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Sets the Owner.
     *
     * @param int $owner the owner
     *
     * @return self
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    public function getNumPayments()
    {
        if ($this->num_payments === null) {
            $this->num_payments = $this->getProduct()->getNumPayments();
        }

        return $this->num_payments;
    }

    /**
     * Sets the The number of payments in a recurring setup.
     *
     * @param int $num_payments the num payments
     *
     * @return self
     */
    public function setNumPayments($num_payments)
    {
        $this->num_payments = $num_payments;

        return $this;
    }

    /**
     * Gets the the string of the type
     *
     * @return mixed
     */
    public function getTypeLabel()
    {
        $label = '';

        switch ($this->getType()) {
            case 'free':
                $label = __('Free', ZippyCourses::TEXTDOMAIN);
                break;
            case 'single':
                $label = __('Single Payment', ZippyCourses::TEXTDOMAIN);
                break;
            case 'subscription':
                $label = __('Subscription', ZippyCourses::TEXTDOMAIN);
                break;
            case 'payment_plan':
            case 'payment-plan':
                $label = __('Payment Plan', ZippyCourses::TEXTDOMAIN);
                break;
            default:
                break;
        }

        return $label;
    }
}
