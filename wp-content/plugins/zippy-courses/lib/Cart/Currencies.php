<?php
/**
 *
 * @since 1.0.0
 *
 * UNUSED CURRENCIES CHART
 *
 * CODE          SYMBOL                  CURRENCY
 * ---------------------------------------------------------------------------------------------------------------------
 * "ARS"       |  "0024"             |   "Argentina Pesos"
 * "AWG"       |  "0192"             |   "Aruba Guilders (also called Florins)"
 * "BSD"       |  "0024"             |   "Bahamas Dollars"
 * "BBD"       |  "0024"             |   "Barbados Dollars"
 * "BEF"       |  "20a3"             |   "Belgium Francs"
 * "BZD"       |  "0024"             |   "Belize Dollars"
 * "BMD"       |  "0024"             |   "Bermuda Dollars"
 * "BOB"       |  "0024"             |   "Bolivia Bolivianos"
 * "BRC"       |  "20a2"             |   "Brazil Cruzeiros"
 * "GBP"       |  "00a3"             |   "Britain (United Kingdom) Pounds"
 * "BND"       |  "0024"             |   "Brunei Darussalam Dollars"
 * "KHR"       |  "17db"             |   "Cambodia Riels"
 * "KYD"       |  "0024"             |   "Cayman Islands Dollars"
 * "CLP"       |  "0024"             |   "Chile Pesos"
 * "CNY"       |  "5143"             |   "China Yuan Renminbi"
 * "COP"       |  "20b1"             |   "Colombia Pesos"
 * "CRC"       |  "20a1"             |   "Costa Rica Colón"
 * "CUP"       |  "20b1"             |   "Cuba Pesos"
 * "CYP"       |  "00a3"             |   "Cyprus Pounds"
 * "DOP"       |  "20b1"             |   "Dominican Republic Pesos"
 * "XCD"       |  "0024"             |   "East Caribbean Dollars"
 * "EGP"       |  "00a3"             |   "Egypt Pounds"
 * "SVC"       |  "20a1"             |   "El Salvador Colón"
 * "XEU"       |  "20a0"             |   "European Currency Unit"
 * "FKP"       |  "00a3"             |   "Falkland Islands Pounds"
 * "FJD"       |  "0024"             |   "Fiji Dollars"
 * "FRF"       |  "20a3"             |   "France Francs"
 * "GIP"       |  "00a3"             |   "Gibraltar Pounds"
 * "GRD"       |  "20af"             |   "Greece Drachmae"
 * "GGP"       |  "00a3"             |   "Guernsey Pounds"
 * "GYD"       |  "0024"             |   "Guyana Dollars"
 * "NLG"       |  "0192"             |   "Holland (Netherlands) Guilders (also called Florins)"
 * "INR"       |  "20a8"             |   "India Rupees"
 * "IRR"       |  "fdfc"             |   "Iran Rials"
 * "IEP"       |  "00a3"             |   "Ireland Punt"
 * "IMP"       |  "00a3"             |   "Isle of Man Pounds"
 * "ITL"       |  "20a4"             |   "Italy Lire"
 * "JMD"       |  "0024"             |   "Jamaica Dollars"
 * "JEP"       |  "00a3"             |   "Jersey Pounds"
 * "KPW"       |  "20a9"             |   "Korea (North) Won"
 * "KRW"       |  "20a9"             |   "Korea (South) Won"
 * "LAK"       |  "20ad"             |   "Laos Kips"
 * "LBP"       |  "00a3"             |   "Lebanon Pounds"
 * "LRD"       |  "0024"             |   "Liberia Dollars"
 * "LUF"       |  "20a3"             |   "Luxembourg Francs"
 * "MYR"       |  "005-004D"         |   "Malaysian Ringgit"
 * "MTL"       |  "20a4"             |   "Malta Liri"
 * "MUR"       |  "20a8"             |   "Mauritius Rupees"
 * "MNT"       |  "20ae"             |   "Mongolia Tugriks"
 * "NAD"       |  "0024"             |   "Namibia Dollars"
 * "NPR"       |  "20a8"             |   "Nepal Rupees"
 * "ANG"       |  "0192"             |   "Netherlands Antilles Guilders (also called Florins)"
 * "NLG"       |  "0192"             |   "Netherlands Guilders"
 * "NGN"       |  "20a6"             |   "Nigeria Nairas"
 * "KPW"       |  "20a9"             |   "North Korea Won"
 * "OMR"       |  "fdfc"             |   "Oman Rials"
 * "PKR"       |  "20a8"             |   "Pakistan Rupees"
 * "PEN"       |  "0053-002-002e"    |   "Peru Nuevos Soles"
 * "QAR"       |  "fdfc"             |   "Qatar Rials"
 * "SHP"       |  "00a3"             |   "Saint Helena Pounds"
 * "SAR"       |  "fdfc"             |   "Saudi Arabia Riyals"
 * "SCR"       |  "20a8"             |   "Seychelles Rupees"
 * "SBD"       |  "0024"             |   "Solomon Islands Dollars"
 * "ZAR"       |  "0052"             |   "South Africa Rand"
 * "KRW"       |  "20a9"             |   "South Korea Won"
 * "ESP"       |  "20a7"             |   "Spain Pesetas"
 * "LKR"       |  "0bf9"             |   "Sri Lanka Rupees"
 * "SRD"       |  "0024"             |   "Suriname Dollars"
 * "SYP"       |  "00a3"             |   "Syria Pounds"
 * "TTD"       |  "0024"             |   "Trinidad and Tobago Dollars"
 * "TRY"       |  "20a4"             |   "Turkey New Lira"
 * "TVD"       |  "0024"             |   "Tuvalu Dollars"
 * ---------------------------------------------------------------------------------------------------------------------
 */


class Zippy_Currencies extends Zippy_Repository
{
    /**
     * List of supported world currencies
     *
     * @var array [key = country code, value = array( name, ascii )]
     */
    public $currencies = array(
        "USD" => array("US Dollars","0024"),
        "AUD" => array("Australia Dollars","0024"),
        "BRL" => array("Brazil Reais","0052-0024"),
        "CAD" => array("Canada Dollars","0024"),
        "DKK" => array("Denmark Kroner","006B-0072"),
        "GBP" => array("English Pounds","00a3"),
        "EUR" => array("Euro","20ac"),
        "HKD" => array("Hong Kong Dollars","0048-004B-0024"),
        "HUF" => array("Hungariant Forint", "0046-0074"),
        "ILS" => array("Israeli New Shekels","20aa"),
        "JPY" => array("Japan Yen","00a5"),
        "MXN" => array("Mexico Pesos","0024"),
        "NOK" => array("Norwegian Kroner","006B-0072"),
        "NZD" => array("New Zealand Dollars","0024"),
        "PHP" => array("Philippines Pesos","20b1"),
        "PLN" => array("Polish złoty", "007a-0142"),
        "RUB" => array("Russia Rubles","0440-0443-0441"),
        "SGD" => array("Singapore Dollars","0024"),
        "SEK" => array("Sweden Kronor","0053-0045-004B-0020"),
        "CHF" => array("Swiss Franc","0043-0048-0046-0020"),
        "TWD" => array("Taiwan New Dollars","5143"),
        "THB" => array("Thailand Baht","0e3f"),
        "TRL" => array("Turkey Liras","20a4"),
    );

    public function getCurrencies()
    {
        return $this->currencies;
    }

    public function getSymbol($currency)
    {

        $c = $this->getCurrency($currency);

        if (!array_key_exists($currency, $this->currencies) || !is_array($c)) {
            return false;
        }

        $output = '';

        $pieces = explode('-', end($c));

        foreach ($pieces as $piece) {
            $output .= html_entity_decode('&#x' . trim($piece)) . ';';
        }

        return $output;

    }

    public function getCurrency($currency)
    {
        return array_key_exists($currency, $this->currencies) ? $this->currencies[$currency] : false;
    }

    public function getList()
    {
        $list = array();

        foreach ($this->currencies as $k => $c) {
            $list[$k] = reset($c);
        }

        return $list;
    }
}
