<?php

class Zippy_Orders extends Zippy_Repository
{
    /**
     * A list of the valid repository object types
     * @var array
     */
    public $valid_types = array('Zippy_Order');

    public function fetchByOwner($user_id)
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $query_id = "zippy_owner_{$user_id}_orders";

        if (($query = $zippy->queries->get($query_id)) === null) {
            $query = $zippy->make('query', array('id' => $query_id));
            $sql = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_author = %d AND post_type = %s", $user_id, 'zippy_order');

            $query->setSql($sql);

            $zippy->queries->add($query);
        }

        $post_ids = $query->getCol();

        foreach ($post_ids as $post_id) {
            $order = $zippy->make('order');
            $order->build($post_id);

            $this->add($order);
        }

        return $this->all();
    }
    public function getUnclaimedOrdersByEmail($email)
    {
        global $wpdb;
        $zippy = Zippy::instance();

        $order_ids = $zippy->utilities->orders->getAllUnclaimedOrderIds();
        // Query to get ID, customer details and order_id for the unclaimed orders
        $order_ids_list = implode($order_ids, ',');

        $sql = "SELECT p.ID, pm.meta_value AS `order_id`, pm2.meta_value AS `customer`
        FROM $wpdb->posts as p
        LEFT JOIN $wpdb->postmeta as pm
        ON (p.ID = pm.post_id)
        LEFT JOIN $wpdb->postmeta as pm2
        ON (p.ID = pm2.post_id)
        WHERE p.post_type = 'transaction'
        AND pm.meta_key = 'order_id'
        AND pm2.meta_key = 'customer'
        AND pm.meta_value IN ($order_ids_list)
        ";

        $transactions = $wpdb->get_results($sql);

        foreach ($transactions as $transaction) {
            if (!isset($transaction->customer)) {
                continue;
            }

            $transaction->customer = unserialize($transaction->customer);
            if (isset($transaction->customer->email) && ($transaction->customer->email == $email)) {
                $order = $zippy->make('order');
                $order->build($transaction->order_id);
                $this->add($order);
            }
        }

        return $this->all();
    }
}
