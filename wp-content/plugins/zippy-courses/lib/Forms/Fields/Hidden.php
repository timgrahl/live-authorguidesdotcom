<?php

class Zippy_Hidden_FormField extends Zippy_FormField
{
    public $type = 'hidden';
    
    public function __construct($name)
    {
        $this->id = $this->name = $name;
    }
}
