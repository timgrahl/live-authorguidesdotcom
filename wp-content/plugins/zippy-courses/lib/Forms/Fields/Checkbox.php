<?php

class Zippy_Checkbox_FormField extends Zippy_FormField
{
    public $type = 'checkbox';
    public $options = array();
    
    public function __construct($name, array $options = array())
    {
        $this->id = $this->name = $name;
        $this->options = $options;
    }
}
