<?php

class Zippy_Text_FormField extends Zippy_FormField
{
    public $type = 'text';
    
    public function __construct($name, $label)
    {
        $this->id = $this->name = $name;
        $this->label = $label;
    }
}
