<?php

class Zippy_Radio_FormField extends Zippy_FormField
{
    public $type = 'radio';
    public $options = array();
    
    public function __construct($name, array $options = array())
    {
        $this->id = $this->name = $name;
        $this->options = $options;
    }
}
