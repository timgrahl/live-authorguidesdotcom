<?php

class Zippy_Select_FormField extends Zippy_FormField
{
    public $type = 'select';
    public $options = array();
    public function __construct($name, $label, $options)
    {
        $this->id = $this->name = $name;
        $this->label = $label;
        $this->options = $options;
    }
}
