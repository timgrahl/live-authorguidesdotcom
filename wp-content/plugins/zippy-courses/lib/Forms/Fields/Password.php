<?php

class Zippy_Password_FormField extends Zippy_FormField
{
    public $type = 'password';
    protected $private = true;
        
    public function __construct($name, $label)
    {
        $this->id = $this->name = $name;
        $this->label = $label;
    }
}
