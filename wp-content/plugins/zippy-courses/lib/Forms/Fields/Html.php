<?php

class Zippy_Html_FormField extends Zippy_FormField
{
    public $type = 'html';
    
    public function __construct($name, $value)
    {
        $this->id = $this->name = $name;
        $this->value = $value;
    }
}
