<?php

abstract class Zippy_FormField implements Zippy_RepositoryObject
{
    public $id;
    protected $name;
    protected $value;
    protected $default;
    protected $label;
    protected $label_visibility = true;
    protected $type;
    protected $placeholder;
    protected $description;
    protected $value_position = 'inside';
    protected $rules = array();
    protected $validation_messages = array();
    protected $private = false;
    protected $name_visibility = true;
    protected $classes = array();
    protected $container_classes = array();

    /**
     * Whether or not we should save the field as meta
     * @var boolean
     */
    protected $savable = true;

    /**
     * Array of attributes for field
     * @var array
     */
    protected $attributes = array();

    /**
     * Array of data attributes for field
     * @var array
     */
    protected $data = array();

    /**
     * Zippy_View for the field
     * @var Zippy_FormFieldView
     */
    protected $view;

    /**
     * Options for field
     * @var array
     */
    protected $options = array();

    /**
     * Gets the Options for field.
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Sets the Options for field.
     *
     * @param array $options the options
     *
     * @return self
     */
    public function setOptions(array $options = array())
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    protected function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function hideName()
    {
        return $this->setNameVisibility(false);
    }

    public function showName()
    {
        return $this->setNameVisibility(true);
    }


    /**
     * Gets the value of name_visibility.
     *
     * @return mixed
     */
    public function getNameVisibility()
    {
        return $this->name_visibility;
    }

    /**
     * Sets the value of name_visibility.
     *
     * @param mixed $name_visibility the show name
     *
     * @return self
     */
    public function setNameVisibility($name_visibility)
    {
        $this->name_visibility = $name_visibility;

        return $this;
    }

    /**
     * Gets the value of label.
     *
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Sets the value of label.
     *
     * @param mixed $label the label
     *
     * @return self
     */
    protected function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Gets the Zippy_View for the field.
     *
     * @return Zippy_FormFieldView
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Sets the Zippy_View for the field.
     *
     * @param Zippy_FormFieldView $view the view
     *
     * @return self
     */
    protected function setView(Zippy_FormFieldView $view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Gets the value of type.
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the value of type.
     *
     * @param string $type The field type
     *
     * @return self
     */
    protected function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets the Array of attributes for field.
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Sets the Array of attributes for field.
     *
     * @param array $attributes the attributes
     *
     * @return self
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    public function addAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
        return $this;
    }

    public function deleteAttribute($key)
    {
        if (isset($this->attributes[$key])) {
            unset($this->attributes[$key]);
        }

        return $this;
    }

/**
     * Gets the Array of data for field.
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Sets the Array of data for field.
     *
     * @param array $data the data
     *
     * @return self
     */
    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    public function addData($key, $value)
    {
        $this->data[$key] = $value;
        return $this;
    }

    public function deleteData($key)
    {
        if (isset($this->data[$key])) {
            unset($this->data[$key]);
        }

        return $this;
    }

    /**
     * Gets the value of value.
     *
     * If $use_default is set to true, then we return a default value.  This is used in forms where we want to show a
     * pre-populated value instead of an empty submission.
     *
     * @return mixed
     */
    public function getValue($use_default = false)
    {
        if ($this->type == 'file' && isset($_FILES[$this->getName()]['tmp_name'])) {
            $this->value = $_FILES[$this->getName()];
            return $this->value;
        }

        if (isset($_POST[$this->getName()])) {
            $value = $_POST[$this->getName()];
            return !$use_default || !empty($value) ? $value : $this->getDefault();
        }

        return $this->value;
    }

    /**
     * Sets the value of value.
     *
     * @param mixed $value the value
     *
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    public function validate($rules)
    {
        $this->rules = $this->parseValidationRules($rules);
        return $this;
    }

    private function parseValidationRules($rules)
    {
        $rules = explode(',', $rules);
        $output = array();

        foreach ($rules as $rule) {
            $parsed = $this->parseRule($rule);
            $output[$parsed['type']] = $parsed;
        }

        return $output;
    }

    private function parseRule($rule)
    {
        $parsed = array();
        $args = explode(':', $rule);

        $parsed['type'] = array_shift($args);
        $parsed['args'] = $args;

        return $parsed;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getRule($key)
    {
        return isset($this->rules[$key]) ? $this->rules[$key] : false;
    }

    public function showsPostedValue()
    {
        return !$this->private;
    }



    /**
     * Gets the Whether or not we should save the field as meta.
     *
     * @return boolean
     */
    public function getSavable()
    {
        return $this->savable;
    }

    /**
     * Sets the Whether or not we should save the field as meta.
     *
     * @param boolean $savable the savable
     *
     * @return self
     */
    public function setSavable($savable = true)
    {
        $this->savable = $savable;

        return $this;
    }

    /**
     * Gets the value of description.
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the value of description.
     *
     * @param mixed $description the description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Gets the value of default.
     *
     * @return mixed
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Sets the value of default.
     *
     * @param mixed $default the default
     *
     * @return self
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the value of placeholder.
     *
     * @return mixed
     */
    public function getPlaceholder()
    {
        return $this->placeholder;
    }

    /**
     * Sets the value of placeholder.
     *
     * @param mixed $placeholder the placeholder
     *
     * @return self
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;

        return $this;
    }
    /**
     * Gets the value of label_visibility.
     *
     * @return mixed
     */
    public function getLabelVisibility()
    {
        return $this->label_visibility;
    }

    /**
     * Sets the value of label_visibility.
     *
     * @param mixed $label_visibility the label visibility
     *
     * @return self
     */
    public function setLabelVisibility($label_visibility)
    {
        $this->label_visibility = $label_visibility;

        return $this;
    }

    /**
     * Gets the value of validation_messages.
     *
     * @return mixed
     */
    public function getValidationMessages()
    {
        return $this->validation_messages;
    }

    /**
     * Sets the value of validation_messages.
     *
     * @param mixed $message The message
     *
     * @return self
     */
    public function addValidationMessage($key, $message)
    {
        $this->validation_messages[$key] = $message;

        return $this;
    }

    /**
     * Gets the value of class.
     *
     * @return mixed
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * Sets the value of class.
     *
     * @param mixed $class the class
     *
     * @return self
     */
    public function addClass($class)
    {
        $this->classes[] = $class;

        return $this;
    }

    /**
     * Gets the value of container_class.
     *
     * @return mixed
     */
    public function getContainerClasses()
    {
        return $this->container_classes;
    }

    /**
     * Sets the value of container_class.
     *
     * @param mixed $container_class the container class
     *
     * @return self
     */
    public function addContainerClass($container_class)
    {
        $this->container_classes[] = $container_class;

        return $this;
    }
}
