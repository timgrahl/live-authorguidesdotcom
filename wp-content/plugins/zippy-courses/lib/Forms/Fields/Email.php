<?php

class Zippy_Email_FormField extends Zippy_FormField
{
    public $type = 'email';

    public function __construct($name, $label)
    {
        $this->id = $this->name = $name;
        $this->label = $label;
    }
}
