<?php

class Zippy_File_FormField extends Zippy_FormField
{
    public $type = 'file';
    
    public function __construct($name, $label)
    {
        $this->id = $this->name = $name;
        $this->label = $label;
    }
}
