<?php

class Zippy_DatePicker_FormField extends Zippy_FormField
{
    public $type = 'datepicker';

    
    public function __construct($name, $label)
    {
        $this->id = $this->name = $name;
        $this->label = $label;
        wp_enqueue_style(
            'jqury-ui-css',
            '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css',
            array(),
            '1.11.4',
            'screen'
        );
        $this->addJqueryUI();

    }
    public function addJqueryUI()
    {
         wp_enqueue_script('jquery-ui-datepicker');
         wp_enqueue_script('zippy-datepicker-js', ZippyCourses::$url . 'assets/js/public/zippy-datepicker.js', array('zippy-plugins'), ZippyCourses::VERSION, true);
    }
}
