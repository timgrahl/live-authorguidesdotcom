<?php

abstract class Zippy_Form implements Zippy_RepositoryObject
{
    public $id;
    protected $method = 'POST';
    protected $action;
    protected $processor;
    protected $submit_text;
    protected $submit_classes = array('zippy-submit', 'zippy-input-submit', 'zippy-button', 'zippy-button-primary');
    
    protected $before;
    protected $after;
    protected $top;
    protected $bottom;


    /**
     * An array of CSS classes1
     * @var array
     */
    protected $classes = array('zippy-form');

    /**
     * Fields Repository
     * @var Zippy_FormFieldsRepository
     */
    public $fields;

    public function __construct($id)
    {
        $zippy = Zippy::instance();

        $this->id       = $id;
        $this->fields   = $zippy->make('fields_repository');
        $this->submit_text = __('Submit', ZippyCourses::TEXTDOMAIN);
        
        $this->defaultFields();

        $this->fields = apply_filters('zippy_form_fields', $this->fields, $this->id);

        $this->afterSetup();
    }

    /**
     * Define the default fields for your form
     * @return void
     */
    abstract public function defaultFields();

    public function addHtml($name, $value)
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('html_field', array('name' => $name, 'value' => $value));
        
        $this->fields->add($field);

        return $field;
    }

    public function addHiddenField($name)
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('hidden_field', array('name' => $name));
        
        $this->fields->add($field);

        return $field;
    }

    public function addTextField($name, $label)
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('text_field', array('name' => $name, 'label' => $label));
        
        $this->fields->add($field);

        return $field;
    }

    public function addFileField($name, $label)
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('file_field', array('name' => $name, 'label' => $label));
        
        $this->fields->add($field);

        return $field;
    }

    public function addEmailField($name, $label)
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('email_field', array('name' => $name, 'label' => $label));
        
        $this->fields->add($field);

        return $field;
    }

    public function addPasswordField($name, $label)
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('password_field', array('name' => $name, 'label' => $label));

        $this->fields->add($field);

        return $field;
    }

    public function addTextarea($name, $label)
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('textarea_field', array('name' => $name, 'label' => $label));
        $this->fields->add($field);

        return $field;
    }

    public function addSelectField($name, $label, array $options = array())
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('select_field', array('name' => $name, 'label' => $label, 'options' => $options));
        $this->fields->add($field);

        return $field;
    }

    public function addCheckboxField($name, array $options = array())
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('checkbox_field', array('name' => $name, 'options' => $options));
        $this->fields->add($field);

        return $field;
    }

    public function addRadioField($name, array $options = array())
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('radio_field', array('name' => $name, 'options' => $options));
        $this->fields->add($field);

        return $field;
    }

    public function addCheckboxesField($name, $label, array $options = array())
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('checkbox_field', array('name' => $name, 'label' => $label, 'options' => $options));
        $this->fields->add($field);

        return $field;
    }

    public function addRadioButtonsField($name, $label, array $options = array())
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('radio_button_field', array('name' => $name, 'label' => $label, 'options' => $options));
        $this->fields->add($field);

        return $field;
    }

    public function addDatePickerField($name, $label)
    {
        $zippy = Zippy::instance();

        $field = $zippy->make('datepicker_field', array('name' => $name, 'label' => $label));

        $this->fields->add($field);

        return $field;
    }

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the value of id.
     *
     * @param mixed $id the id
     *
     * @return self
     */
    protected function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the value of method.
     *
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Sets the value of method.
     *
     * @param mixed $method the method
     *
     * @return self
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Gets the Fields Repository.
     *
     * @return Zippy_FormFieldsRepository
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Sets the Fields Repository.
     *
     * @param Zippy_FormFieldsRepository $fields the fields
     *
     * @return self
     */
    public function setFields(Zippy_FormFieldsRepository $fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * Gets the An array of CSS classes.
     *
     * @return array
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * Sets the An array of CSS classes.
     *
     * @param array $classes the classes
     *
     * @return self
     */
    public function setClasses(array $classes)
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * Adds an item to the CSS classes array
     *
     * @param string $class The class
     *
     * @return self
     */
    public function addClass($class)
    {
        $this->classes[] = $class;

        return $this;
    }

    public function getSubmitText()
    {
        return $this->submit_text;
    }

    public function setSubmitText($text)
    {
        $this->submit_text = $text;
        return $this;
    }

    /**
     * Gets the value of action.
     *
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Sets the value of action.
     *
     * @param mixed $action the action
     *
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Gets the value of top.
     *
     * @return mixed
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * Sets the value of top.
     *
     * @param mixed $top the top
     *
     * @return self
     */
    public function setTop($top)
    {
        $this->top = $top;

        return $this;
    }

    /**
     * Gets the value of after.
     *
     * @return mixed
     */
    public function getAfter()
    {
        return $this->after;
    }

    /**
     * Sets the value of after.
     *
     * @param mixed $after the after
     *
     * @return self
     */
    public function setAfter($after)
    {
        $this->after = $after;

        return $this;
    }

    /**
     * Gets the value of bottom.
     *
     * @return mixed
     */
    public function getBottom()
    {
        return $this->bottom;
    }

    /**
     * Sets the value of bottom.
     *
     * @param mixed $bottom the bottom
     *
     * @return self
     */
    public function setBottom($bottom)
    {
        $this->bottom = $bottom;

        return $this;
    }

    public function addSubmitClass($class)
    {
        $this->submit_classes[] = $class;
    }

    public function getSubmitClasses()
    {
        return $this->submit_classes;
    }

    public function getValues()
    {
        $output = array();

        foreach ($this->fields->all() as $field) {
            $output[$field->getId()] = $field->getValue();
        }

        return $output;
    }

    /**
     * Gets the value of before.
     *
     * @return mixed
     */
    public function getBefore()
    {
        return $this->before;
    }

    /**
     * Sets the value of before.
     *
     * @param mixed $before the before
     *
     * @return self
     */
    protected function setBefore($before)
    {
        $this->before = $before;

        return $this;
    }

    public function afterSetup()
    {
        
    }
}
