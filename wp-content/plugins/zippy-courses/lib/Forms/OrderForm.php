<?php

abstract class Zippy_OrderForm extends Zippy_Form
{

    protected $product;

    /**
     * An array of CSS classes
     * @var array
     */
    protected $classes = array('zippy-form', 'zippy-order-form');

    public function __construct($id)
    {
        global $post;

        $zippy = Zippy::instance();

        $this->id       = $id;
        $this->fields   = $zippy->make('fields_repository');

        parent::__construct($id);
    }

    public function defaultFields()
    {
        $zippy = Zippy::instance();
        $this->addHiddenField('product_id')->setValue($this->getProduct()->getId());
        do_action('zippy_courses_order_form_fields', $this);
    }

    public function getProduct()
    {
        global $post;

        $zippy = Zippy::instance();

        if ($this->product === null) {
            $product_id = $this->fields->get('product_id')->getValue();
            $this->product = $product_id
                ? $zippy->make('product', array('id' => $product_id))
                : ($post->post_type == 'product'
                    ? $zippy->utilities->product->getById($post->ID)
                    : $zippy->utilities->product->getProductByBuyQueryVar()
                );
        }

        return $this->product;
    }
}
