<?php

class Zippy_Forms extends Zippy_Repository
{
    public $valid_types = array('Zippy_Form');
    public $references = array();

    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_action('template_redirect', array($this, 'process'));
        add_action('admin_init', array($this, 'process'));
    }

    public function get($id = '', array $args = array())
    {
        return isset($this->items[$id]) ? $this->items[$id] : $this->create($id, $args);
    }

    public function create($id, array $args = array())
    {
        $class = isset($this->references[$id]) ? $this->references[$id] : false;

        if ($class) {
            $reflection = new ReflectionClass($class);

            $form = $reflection->newInstanceArgs(array($id));

            $this->add($form);

            return $form;
        }

        return;
    }

    public function register($id, $class)
    {
        $this->references[$id] = $class;
    }

    public function process()
    {
        $form_id    = filter_input(INPUT_POST, 'zippy_form', FILTER_SANITIZE_STRING);
        $form       = !empty($form_id) ? $this->get($form_id) : false;

        if ($form) {
            $reflection = new ReflectionClass(get_class($form) . 'Processor');
            $processor = $reflection->newInstanceArgs(array($form));
            $processor->process();
        }
    }
}
