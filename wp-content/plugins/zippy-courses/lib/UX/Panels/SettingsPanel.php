<?php

class Zippy_SettingsPanel extends Zippy_Panel
{
    public $id;
    public $section;

    public function __construct(Zippy_SettingsSection $section)
    {
        $this->id = $section->getId();
        $this->section = $section;
    }

    public function getId()
    {
        return $this->section->getId();
    }

    public function getTitle()
    {
        return $this->section->getTitle();
    }

    public function getSection()
    {
        return $this->section;
    }
}
