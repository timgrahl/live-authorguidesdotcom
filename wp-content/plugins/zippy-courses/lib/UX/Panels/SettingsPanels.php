<?php

class Zippy_SettingsPanels extends Zippy_Repository
{
    public $valid_types = array('Zippy_SettingsPanel');

    private $page;

    public function __construct($page)
    {
        $this->page = $page;
    }

    public function generate()
    {
        global$wp_settings_sections, $wp_settings_fields;

        $zippy = Zippy::instance();

        if (isset($wp_settings_sections[$this->page])) {
            foreach ($wp_settings_sections[$this->page] as $key => $section) {
                if (is_array($section['callback']) && $section['callback'][0] instanceof Zippy_SettingsSection) {
                    $panel = $zippy->make('settings_panel', array($section['callback'][0]));
                    $this->add($panel);
                }
            }
        }
    }

    public function getPage()
    {
        return $this->page;
    }
}
