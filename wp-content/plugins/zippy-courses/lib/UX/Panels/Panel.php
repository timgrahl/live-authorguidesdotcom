<?php

class Zippy_Panel implements Zippy_RepositoryObject
{
    public $id;
    public $title;
    public $content;
    public $view;
    public $current = false;

    public function __construct($id, $title, $content = '')
    {
        $this->id           = $id;
        $this->title        = $title;
        $this->content      = $content;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function setCurrent($current)
    {
        $this->current = (bool) $current;
        return $this;
    }

    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Gets the value of view.
     *
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Sets the value of view.
     *
     * @param mixed $view the view
     *
     * @return self
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }
}
