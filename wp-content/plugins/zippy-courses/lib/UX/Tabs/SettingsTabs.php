<?php

class Zippy_SettingsTabs extends Zippy_Repository
{
    public $valid_types = array('Zippy_SettingsTab');

    public function generate($key)
    {
        global $wp_settings_sections;

        $zippy = Zippy::instance();

        $pages  = array_keys($wp_settings_sections);
        $tabs   = array();

        foreach ($pages as $page) {
            if (strpos($page, $key) === 0) {
                $tab = $zippy->make('settings_tab', array(
                    'id' => $page,
                    'title' => $this->generateTitle($key, $page)
                ));

                $tab->generatePanels($page);

                $this->add($tab);
            }
        }
    }

    private function generateTitle($key, $input)
    {
        return ucwords(str_replace('_', ' ', str_replace($key . '_', '', $input)));
    }
}
