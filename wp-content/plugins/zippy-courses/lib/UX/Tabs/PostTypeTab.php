<?php

class Zippy_PostTypeTab extends Zippy_Model
{
    public $id;
    public $title;
    public $selectors = array();
    public $post_types = array();

    public function __construct($id, $title, array $selectors = array(), array $post_types = array())
    {
        $this->id           = $id;
        $this->title        = $title;
        $this->selectors    = $selectors;
        $this->post_types   = $post_types;
    }

    public function render($active = false)
    {
        $class = $active ? 'active' : '';
        echo '<li data-tab-id="' . $this->getId() . '" data-children="' . $this->getSelectorList() . '" class="' . $class . '"><a href="">' . $this->title . '</a></li>';
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSelectorList()
    {
        $output = '';

        foreach ($this->selectors as $selector) {
            $output .= $selector . ',';
        }

        return rtrim($output, ',');
    }

    public function getPostTypes()
    {
        return $this->post_types;
    }
}
