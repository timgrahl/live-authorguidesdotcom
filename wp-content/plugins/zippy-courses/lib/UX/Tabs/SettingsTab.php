<?php

class Zippy_SettingsTab extends Zippy_Tab
{
    public $id;
    public $title;
    public $view;
    public $content;

    public function __construct($id, $title, $content = '')
    {
        $this->id           = $id;
        $this->title        = $title;
        $this->content      = $content;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function setView(Zippy_View $view)
    {
        $this->view = $view;
        return $this;
    }

    public function generatePanels($page)
    {
        $zippy = Zippy::instance();

        $panels = $zippy->make('settings_panels', array($page));
        $panels->generate();

        $this->setView($zippy->make('settings_panels_view', array($panels)));
    }
}
