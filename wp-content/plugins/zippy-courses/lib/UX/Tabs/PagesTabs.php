<?php

class Zippy_Pages_Tabs extends Zippy_Repository
{
    public function __construct()
    {
        $this->addValidType('Zippy_Tab');
    }

    public function generate($pages)
    {
        $zippy = Zippy::instance();

        foreach ($pages as $page => $file) {
            $tab = $zippy->make('tab', array(
                'id' => $page,
                'title' => $this->generateTitle($page)
            ));

            $file_view = $zippy->make('file_view', array('file' => $file));
            $tab->setView($file_view);

            $this->add($tab);
        }
    }

    private function generateTitle($input)
    {
        return ucwords(str_replace(array('-', '_'), ' ', $input));
    }
}
