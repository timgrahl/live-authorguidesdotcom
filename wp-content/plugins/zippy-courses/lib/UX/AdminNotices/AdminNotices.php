<?php

class Zippy_AdminNotices extends Zippy_Repository
{
    public $valid_types = array('Zippy_AdminNotice');

    private static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        add_action('admin_notices', array($this, 'render'));
    }

    public function render()
    {
        foreach ($this->all() as $notice) {
            $notice->show();
        }
    }
}
