<?php

class Zippy_AdminNotice implements Zippy_RepositoryObject
{
    /**
     * ID or slug of Notice
     *
     * @since   1.0.0
     *
     * @var     string
     */
    public $id;

    /**
     * Message to be displayed
     *
     * @since   1.0.0
     *
     * @var     string
     */
    public $message;

    /**
     * Type of notice
     *
     * @since   1.0.0
     *
     * @var     string
     */
    public $type;

    /**
     * Action Buttons for Modal
     * @var array
     */
    public $action_buttons = array();
    

    /**
     * CSs Classes
     * @var array
     */
    public $classes = array();

    /**
     * Can this notice be dismissed
     *
     * @since   1.0.0
     *
     * @var     boolean
     */
    public $dismissible = false;

    public function __construct($id, $message, $type = 'success')
    {
        $this->id       = $id;
        $this->message  = $message;
        $this->type     = $type;
    }

    /**
     * Gets the ID or slug of Notice.
     *
     * @since     1.0.0
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the ID or slug of Notice.
     *
     * @since     1.0.0
     *
     * @param string $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the Message to be displayed.
     *
     * @since     1.0.0
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Sets the Message to be displayed.
     *
     * @param string $message the message
     *
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Gets the Type of notice.
     *
     * @since     1.0.0
     *
     * @return     string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the Type of notice.
     *
     * Valid types: error, warning, success
     *
     * @since     1.0.0
     *
     * @param     string $type the type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets the Can this notice be dismissed.
     *
     * @since     1.0.0
     *
     * @return    boolean
     */
    public function getDismissible()
    {
        return $this->dismissible;
    }

    /**
     * Sets the Can this notice be dismissed.
     *
     * @since     1.0.0
     *
     * @param     boolean $dismissible the dismissible
     *
     * @return    self
     */
    public function setDismissible($dismissible)
    {
        $this->dismissible = $dismissible;

        return $this;
    }

    /**
     * Get the CSS classes as a string
     *
     * @since     1.0.0
     *
     * @return    string
     */
    public function getClasses()
    {
        $classes = array('notice', 'zippy-admin-notice');

        switch ($this->getType()) {
            case 'error':
                $classes[] = 'error';
                break;
            case 'warning':
                $classes[] = 'warning';
                break;
            case 'success':
                $classes[] = 'updated';
                break;
            default:
                break;
        }

        if (count($this->getActionButtons())) {
            $classes[] = 'has-buttons';
        }

        if ($this->getDismissible()) {
            $classes[] = 'is-dismissible';
        }

        $classes[] = "zippy-{$this->id}-admin-notice";

        return implode(' ', array_merge($classes, $this->classes));
    }

    /**
     * Add a class to the list
     *
     * @param     string    $class  CSS Class
     *
     * @since     1.0.0
     *
     * @return    self
     */
    public function addClass($class)
    {
        $this->classes[] = $class;

        return $this;
    }

    /**
     * Remove a class from the list.  You cannot remove defaults
     *
     * @param     string    $class  CSS Class
     *
     * @since     1.0.0
     *
     * @return    self
     */
    public function removeClass($class)
    {
        if ($key = array_search($class, $this->classes) !== false) {
            unset($this->classes[$key]);
        }

        return $this;
    }

    /**
     * Add an action button
     * @param type array $button   Must have text and url keys, or it will not be added
     * @return type
     */
    public function addActionButton($text = '', $url = '', $class = '')
    {
        $this->action_buttons[] = array(
            'text'  => $text,
            'url'   => $url,
            'class' => $class
        );

        return $this;
    }

    /**
     * Gets the Action Buttons for Modal.
     *
     * @return array
     */
    public function getActionButtons()
    {
        return $this->action_buttons;
    }

    /**
     * Sets the Action Buttons for Modal.
     *
     * @param array $action_buttons the action buttons
     *
     * @return self
     */
    public function setActionButtons(array $action_buttons)
    {
        foreach ($action_buttons as $key => $button) {
            if (!is_array($button) || !isset($button['text']) || !isset($button['url']) || !isset($button['class'])) {
                unset($action_buttons[$key]);
            }
        }

        $this->action_buttons = array_values($action_buttons);

        return $this;
    }

    public function show()
    {
        echo $this->render();
    }

    public function render()
    {
        $output  = '';
        $output .= '<div class="' . $this->getClasses() . '">';
            $output .= wpautop($this->_renderActionButtons());
            $output .= wpautop($this->getMessage());
        $output .= '</div>';

        return $output;
    }

    public function _renderActionButtons()
    {
        $output = '<div class="zippy-notice-action-buttons">';
        foreach ($this->getActionButtons() as $button) {
            $output .= '<a href="' . $button['url'] . '" class="' . $button['class'] . ' button">' . $button['text'] . '</a> ';
        }
        $output .= '</div>';

        return $output;
    }
}
