<?php


if ( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

abstract class Zippy_ListTable extends WP_List_Table
{
    public $singular;
    public $plural;
    public $screen;
    public $per_page = 20;
    public $item_type = 'entry';
    public $columns;
    public $filters;

    public function __construct($args = array())
    {
        $zippy = Zippy::instance();

        parent::__construct(array(
            'singular' => $this->getSingular(),
            'plural'   => $this->getPlural(),
            'screen'   => $this->getScreen(),
        ));

        $this->columns = $zippy->make('list_table_columns');
        $this->setupColumns();

        $this->filters  = $zippy->make('list_table_filters');
        $this->setupFilters();
    }

    abstract public function getData();

    abstract public function setupColumns();
    abstract public function setupFilters();

    public function sortData($data)
    {
        $zippy = Zippy::instance();

        $orderby = filter_input(INPUT_GET, 'orderby');
        if (empty($orderby)) {
            return $data;
        }

        $column = $this->columns->get($orderby);
        if ($column) {
            return $column->sort($data);
        }

        return $data;
    }

    public function prepare_items()
    {
        $this->items = $this->sortData($this->filterData($this->getData()));

        $this->_pagination_args['total_items'] = count($this->items);
        $this->_pagination_args['total_pages'] = ceil(count($this->items) / $this->per_page);

        $this->items = array_slice($this->items, $this->per_page * ($this->get_pagenum() - 1), $this->per_page);
    }

    public function get_columns()
    {
        $columns = array_merge(array('cb' => '<input type="checkbox" />'), $this->columns->toArray('label'));
        return apply_filters("manage_{$this->item_type}_columns", $columns);
    }

    public function get_sortable_columns()
    {
        $output = array();
        $sortable = $this->columns->where2('sortable', true, '===');

        foreach ($sortable->all() as $item) {
            $output[$item->getId()] = array($item->getId(), false);
        }
       
        return $output;
    }

    public function get_hidden_columns()
    {
        $output = array();
        $hidden = $this->columns->where2('hidden', true, '===');

        foreach ($hidden->all() as $item) {
            $output[$item->getId()] = array($item->getId(), false);
        }

        return $output;
    }

    /**
     * Generate HTML for a single row on the users.php admin panel.
     *
     * @since 1.0.0

     * @access public
     *
     * @param object $item  The current object
     *
     * @return string Output for a single row.
     */
    public function single_row($item)
    {
        $checkbox = '<label class="screen-reader-text" for="' . $this->getItemType() . '_' . $item->getId() . '">' . __('Select') . '</label>'
                        . '<input type="checkbox" name="' . $this->getItemType() . '[]" id="' . $this->getItemType() . '_' . $item->getId() . '" value="' . $item->getId() . '" />';

        $r = '<tr id="' . $this->getItemType() . '-' . $item->getId() . '">';

        list($columns, $hidden, $sortable) = $this->get_column_info();

        foreach ($columns as $column_name => $column_display_name) {
            if ('cb' === $column_name) {
                $r .= "<th scope='row' class='check-column'>$checkbox</th>";
            } else {
                $tableCol = $this->columns->get($column_name);

                $output = $tableCol !== null ? $tableCol->render($item) : '';

                $r .= "<td>";
                    $r .= apply_filters('manage_students_custom_column', $output, $column_name, $item);
                $r .= "</td>";
            }
        }

        $r .= '</tr>';

        return $r;
    }

    /**
     * Generate the list table rows.
     *
     * @since 3.1.0
     * @access public
     */
    public function display_rows()
    {
        foreach ($this->items as $item) {
            echo "\n\t" . $this->single_row($item);
        }
    }

    protected function extra_tablenav( $which ) {
        $this->_showFilters();        
    }

    public function getPlural()
    {
        return $this->plural;
    }

    public function getSingular()
    {
        return $this->singular;
    }

    public function getScreen()
    {
        return $this->screen;
    }

    public function getItemType()
    {
        return $this->item_type;
    }

    protected function _showFilters()
    {
        foreach ($this->filters->all() as $filter) {
            $filter->show();
        }

        if ($this->filters->count()) {
            echo '<a href="" class="zippy-submit-list-table-filters button">Filter</a>';
        }
    }

    public function filterData(array $items)
    {
        foreach ($this->filters->all() as $filter) {
            foreach ($items as $id => $item) {
                if (!$filter->filter($item)) {
                    unset($items[$id]);
                }
            }
        }

        return $items;
    }
}
