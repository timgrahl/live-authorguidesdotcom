<?php

abstract class Zippy_ListTableFilter implements Zippy_RepositoryObject
{
    /**
     * Column ID
     * @var string
     */
    public $id;

    /**
     * Post Types
     * @var array
     */
    public $post_types = array();

    /**
     * For Post Type
     * @var boolean
     */
    public $for_post_type = true;

    /**
     * Label
     * @var string
     */
    public $label;

    /**
     * Position of extra_nav, either top or bottom
     * @var string
     */
    public $position = '';

    /**
     * Options
     * @var array
     */
    public $options = array();

/**
 *--------------------------------------------------------------------------
 * ABSTRACT METHODS
 *--------------------------------------------------------------------------
 */
    
    /**
     * Render the nav
     *
     * @since 1.0.0
     *
     * @return void
     */
    abstract public function render();

    /**
     * Filter the results
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function filter($query)
    {
        return $query;
    }

    /**
     * Filter the results
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function filterClauses($clauses)
    {
        return $clauses;
    }
    
/**
 *--------------------------------------------------------------------------
 * SHARED METHODS
 *--------------------------------------------------------------------------
 */
    
    public function __construct($id, $label)
    {
        $this->id       = $id;
        $this->label    = $label;

        $this->_defaultOptions();
    }

    
    /**
     * Register this column with WordPress's built in filter hooks
     *
     * @since  1.0.0
     *
     * @uses   add_filter
     * @uses   add_action
     *
     * @return void
     */
    public function register()
    {
        if ($this->getForPostType()) {
            $post_type = filter_input(INPUT_GET, 'post_type');
            if (in_array($post_type, $this->post_types)) {
                add_filter("restrict_manage_posts", array($this, 'show'));
                add_filter("parse_query", array($this, 'filter'));
                add_filter('posts_clauses', array($this, 'filterClauses'));
            }
        }
    }

    public function show()
    {
        echo $this->render();
    }

/**
 *--------------------------------------------------------------------------
 * GETTERS & SETTERS
 *--------------------------------------------------------------------------
 */
   
    /**
     * Gets the Column ID.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the Column ID.
     *
     * @param string $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the Post Types.
     *
     * @return array
     */
    public function getPostTypes()
    {
        return $this->post_type;
    }

    /**
     * Add a Post Type.
     *
     * @param string $post_type the post type
     *
     * @return self
     */
    public function addPostType($post_type)
    {
        if (!in_array($post_type, $this->post_types)) {
            $this->post_types[] = $post_type;
        }

        return $this;
    }

    /**
     * Gets the Label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Sets the Label.
     *
     * @param string $label the label
     *
     * @return self
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Gets the Column Position.
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the Column Position.
     *
     * @param string $position the position
     *
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Gets whether this is for a post type column table
     *
     * @return boolean
     */
    public function getForPostType()
    {
        return $this->for_post_type;
    }

    /**
     * Sets whether this is for a post type column table
     *
     * @param boolean $for_post_type
     *
     * @return self
     */
    public function setForPostType($for_post_type)
    {
        $this->for_post_type = $for_post_type;

        return $this;
    }

    public function sort($data)
    {
        return $data;
    }
}
