<?php
/**
 * A repository that contains the columns that will be added to a particular Post Type
 *
 * @since 1.0.0
 */
class Zippy_ListTable_Navs extends Zippy_Repository
{
    public $valid_types = array('Zippy_ListTable_Nav');
}
