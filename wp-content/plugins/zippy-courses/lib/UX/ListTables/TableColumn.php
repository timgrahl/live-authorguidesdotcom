<?php

abstract class Zippy_ListTableColumn implements Zippy_RepositoryObject
{
    /**
     * Column ID
     * @var string
     */
    public $id;

    /**
     * Post Types
     * @var array
     */
    public $post_types = array();

    /**
     * For Post Type
     * @var boolean
     */
    public $for_post_type = true;

    /**
     * Label
     * @var string
     */
    public $label;

    /**
     * Column Position, either an index or a column name
     * @var string
     */
    public $position;

    /**
     * Column Positioning Method
     * @var string
     */
    public $position_method = 'before';

    /**
     * Sortable
     * @var boolean
     */
    public $sortable = false;

/**
 *--------------------------------------------------------------------------
 * ABSTRACT METHODS
 *--------------------------------------------------------------------------
 */
    
    /**
     * Render the contents of the column
     *
     * @since 1.0.0
     *
     * @param  string   $column  Slug of the column
     * @param  int      $item   Can be an ID of an object, or an object itself
     *
     * @hooked manage_{$post_type}_posts_custom_column
     *
     * @return void
     */
    abstract public function render($item);
    
/**
 *--------------------------------------------------------------------------
 * SHARED METHODS
 *--------------------------------------------------------------------------
 */
    
    public function __construct($id, $label)
    {
        $this->id       = $id;
        $this->label    = $label;
    }

    /**
     * Register this column with WordPress's built in column hooks
     *
     * @since  1.0.0
     *
     * @uses   add_filter
     * @uses   add_action
     *
     * @return void
     */
    public function register()
    {
        if ($this->getForPostType()) {
            foreach ($this->post_types as $post_type) {
                add_filter("manage_{$post_type}_posts_columns", array($this, 'addColumn'));
                add_action("manage_{$post_type}_posts_custom_column", array($this, 'renderColumn'), 10, 2);
            }
        }
    }

    /**
     * Add column in the correct position
     *
     * @since 1.0.0
     *
     * @param array $columns Array of Columns for the post type
     */
    public function addColumn($columns)
    {
        $position   = $this->position;
        $method     = $this->position_method;

        if (!is_numeric($position)) {
            $keys = array_keys($columns);
            $index = array_search($position, $keys);

            // If array_search does not find a key, then simply add it to the end of the array
            $index = $index !== false ? $index : count($keys);
        } else {
            $index = $position;
        }

        $index = $method == 'before' ? $index : $index + 1;

        $before = array_slice($columns, 0, $index);
        $after  = array_slice($columns, $index);

        $columns = array_merge($before, array($this->getId() => $this->getLabel()), $after);

        return $columns;
    }

    /**
     * Conditionally render the column
     *
     * @since 1.0.0
     *
     * @param  string $column  Column slug
     * @param  int    $post_id Post ID
     *
     * @return void
     */
    public function renderColumn($column, $post_id)
    {
        if ($column == $this->getId()) {
            $this->render($post_id);
        }
    }

/**
 *--------------------------------------------------------------------------
 * GETTERS & SETTERS
 *--------------------------------------------------------------------------
 */
   
    /**
     * Gets the Column ID.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the Column ID.
     *
     * @param string $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the Post Types.
     *
     * @return array
     */
    public function getPostTypes()
    {
        return $this->post_type;
    }

    /**
     * Add a Post Type.
     *
     * @param string $post_type the post type
     *
     * @return self
     */
    public function addPostType($post_type)
    {
        if (!in_array($post_type, $this->post_types)) {
            $this->post_types[] = $post_type;
        }

        return $this;
    }

    /**
     * Gets the Label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Sets the Label.
     *
     * @param string $label the label
     *
     * @return self
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Gets the Column Position.
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the Column Position.
     *
     * @param string $position the position
     *
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Gets the Column Positioning Method.
     *
     * @return string
     */
    public function getPositionMethod()
    {
        return $this->position_method;
    }

    /**
     * Sets the Column Positioning Method.
     *
     * @param string $position_method the position method
     *
     * @return self
     */
    public function setPositionMethod($method)
    {
        $valid = array('before', 'after');

        $this->position_method = in_array($method, $valid) ? $method : 'before';

        return $this;
    }

    /**
     * Gets the Sortable.
     *
     * @return boolean
     */
    public function getSortable()
    {
        return $this->sortable;
    }

    /**
     * Sets the Sortable.
     *
     * @param boolean $sortable the sortable
     *
     * @return self
     */
    public function setSortable($sortable)
    {
        $this->sortable = $sortable;

        return $this;
    }

    /**
     * Gets whether this is for a post type column table
     *
     * @return boolean
     */
    public function getForPostType()
    {
        return $this->for_post_type;
    }

    /**
     * Sets whether this is for a post type column table
     *
     * @param boolean $for_post_type
     *
     * @return self
     */
    public function setForPostType($for_post_type)
    {
        $this->for_post_type = $for_post_type;

        return $this;
    }

    public function sort($data) { return $data; }
}
