<?php
/**
 * A repository that contains the columns that will be added to a particular Post Type
 *
 * @since 1.0.0
 */
class Zippy_ListTableFilters extends Zippy_Repository
{
    public $valid_types = array('Zippy_ListTableFilter');

    public function register()
    {
        foreach ($this->all() as $filter) {
            $filter->register();
        }
    }

    public function __construct()
    {
        add_action('init', array($this, 'register'));
    }
}
