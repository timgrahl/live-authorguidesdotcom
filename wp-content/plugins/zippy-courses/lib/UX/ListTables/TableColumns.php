<?php
/**
 * A repository that contains the columns that will be added to a particular Post Type
 *
 * @since 1.0.0
 */
class Zippy_ListTableColumns extends Zippy_Repository
{
    public $valid_types = array('Zippy_ListTableColumn');

    public function __construct()
    {
        add_action('init', array($this, 'register'));
    }

    public function register()
    {
        foreach ($this->all() as $column) {
            $column->register();
        }
    }
}
