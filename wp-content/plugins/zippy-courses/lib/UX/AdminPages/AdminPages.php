<?php

class Zippy_AdminPages extends Zippy_Repository
{
    private static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->addValidType('Zippy_AdminPage');

        $this->hooks();
    }


    protected function hooks()
    {
        add_action('admin_menu', array($this, 'register'), 9);
    }

    public function register()
    {
        foreach ($this->all() as $page) {
            $page->register();
        }
    }
}
