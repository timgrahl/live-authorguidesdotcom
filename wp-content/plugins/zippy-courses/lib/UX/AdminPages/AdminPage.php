<?php

class Zippy_AdminPage extends Zippy_Model
{
    protected $slug;
    protected $title;
    protected $menu_title;
    protected $view;
    protected $icon;
    protected $parent;
    protected $context = 'default';
    protected $position = 59;
    protected $capability = 'edit_others_posts';

    public function __construct($slug, $title, $menu_title = '')
    {
        $this->slug         = $this->id = $slug;
        $this->title        = $title;
        $this->menu_title   = !empty($menu_title) ? $menu_title : $title;
    }

    public function register()
    {
        if ($this->getParent() != null) {
            return $this->registerChildPage();
        }

        return $this->registerAdminPage();
    }

    private function registerChildPage()
    {
        switch ($this->parent) {
            case 'tools':
            case 'tools.php':
                break;
            default:
                if ($this->getParent() == 'hidden') {
                    add_submenu_page(null, $this->getTitle(), $this->getMenuTitle(), $this->capability, $this->slug, array($this, 'render'));
                } else {
                    add_submenu_page($this->getParent(), $this->getTitle(), $this->getMenuTitle(), $this->capability, $this->slug, array($this, 'render'));
                }
                break;
        }
    }

    public function registerAdminPage()
    {
        add_menu_page($this->getTitle(), $this->getMenuTitle(), $this->capability, $this->slug, array($this, 'render'), $this->icon, $this->position);
    }

    public function render()
    {
        echo $this->wrap($this->renderView());
    }

    private function renderView()
    {
        if (!($this->view instanceof Zippy_View)) {
            $zippy = Zippy::instance();
            $file = $this->getViewFile();
            if ($file) {
                $this->view = $zippy->make('file_view', array('file' => $file));
            }
        }

        return $this->view instanceof Zippy_View ? $this->view->render() : '';
    }

    private function getViewFile()
    {
        $file = $this->view === null ? ZippyCourses::$path . 'assets/views/admin-pages/' . $this->slug . '.php' : $this->view;
        
        return file_exists($file) && is_readable($file) ? $file : false;
    }

    public function wrap($input)
    {
        $output = '<div class="wrap">';

        if (!empty($this->title)) {
            $output .= '<h1>';
            $output .= apply_filters('zippy_admin_page_title', $this->title, $this->id);
            $output .= '</h1>';
        }

        $output .= $input . '</div>';

        return $output;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    public function setCapability($capability)
    {
        $this->capability = $capability;
        return $this;
    }

    public function setContext($context)
    {
        $this->context = $context;
        return $this;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getMenuTitle()
    {
        if ($this->menu_title === null) {
            return $this->getTitle();
        }

        return $this->menu_title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setView($view)
    {
        if ($view instanceof Zippy_View) {
            $this->view = $view;
        } else {
            if (file_exists($view) && is_readable($view)) {
                $zippy = Zippy::instance();
                $this->view = $zippy->make('file_view', array('file' => $view));
            }
        }

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }
}
