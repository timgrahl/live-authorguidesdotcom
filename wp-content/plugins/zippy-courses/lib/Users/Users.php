<?php

class Zippy_Users extends Zippy_Repository
{
    public function __construct()
    {
        $this->addValidType('Zippy_User');
        $this->addValidType('ZippyCourses_Student');
    }
}
