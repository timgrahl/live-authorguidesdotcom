<?php

abstract class Zippy_User implements Zippy_RepositoryObject
{
    public $ID;
    public $ip;
    public $password;
    public $email;
    public $first_name;
    public $last_name;
    public $full_name;
    public $display_name;
    public $username;
    public $joined;
    public $fetched = false;

    public function __construct($ID = 0)
    {
        $this->ID = $ID;
        $this->id = &$this->ID;

        $this->IP = $this->getRealIpAddress();
    }

    public function getFullName()
    {
        if ($this->full_name === null) {
            $this->full_name = $this->first_name . ' ' . $this->last_name;
        }

        return $this->full_name;
    }

    public function getId()
    {
        return $this->ID;
    }

    public function getEmail()
    {
        if ($this->email === null) {
            $this->fetch();
        }

        return $this->email;
    }

    /**
     * Sets the value of email.
     *
     * @param mixed $email the email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getDisplayName()
    {
        if ($this->display_name === null) {
            $this->fetch();
        }

        return $this->display_name;
    }

    public function setDisplayName($display_name)
    {
        $this->display_name = $display_name;
        return $this; 
    }

    public function getFirstName()
    {
        if ($this->first_name !== null) {
            return $this->first_name;
        }

        if ($this->full_name !== null) {
            $this->analyzeName($this->full_name);

            return $this->first_name;
        }

        $this->fetch();

        return $this->first_name;
    }
/**
     * Sets the value of first_name.
     *
     * @param mixed $first_name the first name
     *
     * @return self
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName()
    {
        if ($this->last_name !== null) {
            return $this->last_name;
        }

        if ($this->full_name !== null) {
            $this->analyzeName($this->full_name);

            return $this->last_name;
        }

        $this->fetch();
        
        return $this->last_name;
    }

/**
     * Sets the value of last_name.
     *
     * @param mixed $last_name the last name
     *
     * @return self
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * splits single name string into salutation, first, last, suffix
     *
     * @param string $name
     * @return array
     */
    public function analyzeName($name)
    {
        $results = array();

        $salutations = array('Mr', 'Mrs', 'Miss', 'Dr', 'Ms', 'Prof', 'Rev', 'Mister', 'Md');
        $suffixes     = array('Jr', 'Junior', 'Sr', 'Senior', 'II', 'III');

        $segments = explode(' ', $name);
        $size = count($segments);

        // If the first item has a period, or it's a known salutation...
        if (mb_strpos(reset($segments), '.') === false && !in_array(reset($segments), $salutations)) {
            $results['salutation']  = '';
            $results['first']       = array_shift($segments);
        } else {
            $results['salutation']  = array_shift($segments);
            $results['first']       = array_shift($segments);
        }

        // Check for suffixes, and then combine the rest into last name
        if (mb_strpos(end($segments), '.') === false && !in_array(end($segments), $suffixes)) {
            $results['suffix']  = '';
        } else {
            $results['suffix']  = array_pop($segments);
        }

        $middle_and_last = $this->splitMiddleAndLastName($segments);

        $results['middle']  = $middle_and_last['middle'];
        $results['last']    = $middle_and_last['last'];

        $this->first_name   = $results['first'];
        $this->last_name    = $results['last'];

        return $results;
    }

    /**
     * Analyze and split middle and last names
     *
     * @since 1.0.0
     *
     * @param  array    $segments   An array of name items in a name
     *
     * @return array    array('middle' => $middle, last' => $last)
     */
    private function splitMiddleAndLastName(array $segments)
    {
        $analysis = array();
        $results = array();

        foreach ($segments as $segment) {
            preg_match_all('/\b([A-Z]+)/', $segment, $matches);
            $analysis[] = array('text' => $segment, 'capitalized' => (count($matches[0]) > 0));
        }

        switch(count($analysis)) {
            case 1:
                $mapped = array_map(create_function('$a', 'return $a["text"];'), $analysis);

                $results['middle']  = '';
                $results['last']    = implode(' ', $mapped);
                break;
            default:
                // Are they all capitalized?
                $capitalized = array_filter($analysis, create_function('$a', 'return $a["capitalized"];'));
                $all_capitalized = count($capitalized) == count($analysis);
                $last = end($analysis);
                $last_is_capitalized = $last['capitalized'];

                if ($all_capitalized) {
                    $mapped = array_map(create_function('$a', 'return $a["text"];'), $analysis);
                    $results['middle']  = array_shift($mapped);
                    $results['last']    = implode(' ', $mapped);
                } else {
                    $words = array(0 => '');
                    $current = 0;
                    foreach ($analysis as $entry) {
                        if ($entry['capitalized'] == true) {
                            $words[$current] .= "{$entry['text']} ";
                            $current++;
                            $words[$current] = '';
                        } else {
                            $words[$current] .= "{$entry['text']} ";
                        }
                    }

                    switch (count($words)) {
                        case 0:
                            $results['middle']  = '';
                            $results['last']    = '';
                            break;
                        case 1:
                            $results['middle']  = '';
                            $results['last']    = trim(implode(' ', $words));
                            break;
                        default:
                            $results['middle']  = trim(array_shift($words));
                            $results['last']    = trim(implode(' ', $words));
                            break;
                    }
                }

                break;
        }
        
        return $results;
    }

    public function getUsername()
    {
        if ($this->username === null) {
            $this->fetch();
        }

        return $this->username;
    }

    /**
     * Sets the value of username.
     *
     * @param mixed $username the username
     *
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }
    

    public function fetch()
    {
        $data = get_userdata($this->ID);
        if ($data && $this->ID > 0) {
            $this->first_name   = $data->first_name;
            $this->last_name    = $data->last_name;
            $this->full_name    = $this->getFullName();
            $this->email        = $data->user_email;
            $this->username     = $data->user_login;
            $this->display_name = $data->display_name;
            $this->joined       = new DateTime($data->data->user_registered);
        }

        $this->fetched = true;
    }

    /**
     * Get a unique password recovery URL for the user
     *
     * @since 1.0.0
     *
     * @return string   url|empty
     */
    public function getPasswordRecoveryUrl()
    {
        $zippy = Zippy::instance();
        
        $url = $zippy->core_pages->getUrl('reset_password');
        $key = $this->generateNewPasswordRecoveryKey();

        return $url ? $url . "?uid={$this->ID}&key=$key" : '';
    }

    /**
     * Generate and store a unique password recovery key for the user
     *
     * @since   1.0.0
     *
     * @return  string
     */
    private function generateNewPasswordRecoveryKey()
    {
        global $wpdb;

        $zippy = Zippy::instance();

        $recovery_key = $zippy->utilities->generateUniqueAlphanumeric(16);
        $meta_key = 'password_recovery_key';
        $exists = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT umeta_id FROM $wpdb->usermeta WHERE meta_key = %s AND meta_value = %s",
                $meta_key,
                $recovery_key
            )
        );

        if ($exists === null) {
            add_user_meta($this->ID, $meta_key, $recovery_key, false);
            return $recovery_key;
        } else {
            return $this->generateNewPasswordRecoveryKey();
        }
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getProfileLink()
    {
        return get_edit_user_link($this->ID);
    }

    public function getJoined($format = '')
    {
        if ($this->joined instanceof DateTime) {
            return !empty($format) ? date_i18n($format, $this->joined->format('U')) : $this->joined;
        }

        return null;
    }

    public function setJoined(DateTime $joined)
    {
        $this->joined = $joined;

        return $this;
    }

    public function getJoinDateString()
    {
        if ($this->joined instanceof DateTime) {
            return date_i18n(get_option('date_format'), $this->joined->format('U'));
        }

        return '';
    }

    public function update()
    {
        global $wpdb;

        if ($this->fetched) {
            $old = $wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->users WHERE ID = %s", $this->getId()));

            $check_username = username_exists($this->getUsername());
            $check_username = $check_username !== false ? username_exists($this->getUsername()) == $this->getId() : true;
            
            $check_email    = email_exists($this->getEmail());
            $check_email    = $check_email !== false ? email_exists($this->getEmail()) == $this->getId() : true;

            $data = array();

            if ($check_username) {
                $data['user_login'] = $this->getUsername();
            }

            if ($check_email) {
                $data['user_email'] = $this->getEmail();
            }

            $where = array('ID' => $this->getId());

            $data['display_name'] = $this->getDisplayName();

            if (count($data)) {
                $wpdb->update($wpdb->users, $data, $where);
            }

            update_user_meta($this->getId(), 'first_name', $this->getFirstName());
            update_user_meta($this->getId(), 'last_name', $this->getLastName());
            update_user_meta($this->getId(), 'display_name', $this->getDisplayName());
        }
    }

    public function toJson()
    {
        return json_encode($this);
    }

    /**
     * Sets the value of ID.
     *
     * @param mixed $ID the 
     *
     * @return self
     */
    public function setId($ID)
    {
        $this->ID = $ID;

        return $this;
    }

    public function getRealIpAddress()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        
        return $ip;
    }

    /**
     * Sets the value of full_name.
     *
     * @param mixed $full_name the full name
     *
     * @return self
     */
    public function setFullName($full_name)
    {
        $this->full_name = $full_name;

        return $this;
    }
}
