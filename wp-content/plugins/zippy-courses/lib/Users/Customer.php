<?php

class Zippy_Customer extends Zippy_User
{
    public $email;

    public function importData(array $data = array())
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }

        return $this;
    }
}
