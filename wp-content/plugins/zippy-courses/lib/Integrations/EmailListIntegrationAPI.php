<?php

abstract class Zippy_EmailListIntegrationAPI
{
    protected $service;

    public function __construct($service)
    {
        $this->service = $service;
    }

    protected function getTransientName($endpoint)
    {
        return 'zippy_' . $this->service . '_' . $endpoint;
    }

    public function getBinding($endpoint)
    {
        return $this->service . '_' . $endpoint;
    }

    abstract public function getSettingValues();
    abstract public function getLists();
    abstract public function fetchLists();
    abstract public function getList($id);
    abstract public function getContact(ZippyCourses_Student $student, Zippy_EmailList $list);

    abstract public function subscribe(ZippyCourses_Student $student, Zippy_EmailList $list);
    abstract public function unsubscribe(ZippyCourses_Student $student, Zippy_EmailList $list);

    /**
     * Get lists as a JSON Object
     * @since  1.0.0
     * @return json
     */
    public function getListsJSON()
    {
        echo json_encode($this->getListsDropdownOptions());
        die();
    }

    /**
     * Get lists as an object ready for json encoding
     * @since  1.0.0
     * @return json
     */
    public function getListsDropdownOptions()
    {
        $output = array();

        $obj = new stdClass;
            $obj->text = '';
            $obj->value = 0;

        $output[] = $obj;
        
        foreach ($this->getLists()->all() as $list) {
            $obj = new stdClass;
            $obj->text = $list->getName();
            $obj->value = $list->getId();

            $output[] = $obj;
        }
        
        return $output;
    }

    public function getSubscriptionAttempted(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $zippy = Zippy::instance();

        $attempts = $this->getSubscriptionAttempts();
        $attempted = false;

        $email = $student->getEmail();

        if (empty($email)) {
            return true;
        }

        foreach ($attempts as $list_id => $emails) {
            if ($list->id != $list_id) {
                continue;
            }

            if (in_array($student->getEmail(), $emails)) {
                $attempted = true;
            }

            if (!$attempted) {
                break;
            }
        }

        return $attempted;
    }

    public function getSubscriptionAttempts()
    {
        $zippy = Zippy::instance();

        return is_array($zippy->cache->get("{$this->service}_subscription_attempts"))
            ? $zippy->cache->get("{$this->service}_subscription_attempts")
            : array();
    }

    public function logSubscriptionAttempt(ZippyCourses_Student $student, Zippy_EmailList $list)
    {
        $zippy = Zippy::instance();
        
        $attempts = $this->getSubscriptionAttempts();
        $attempts[$list->id][] = $student->getEmail();

        $zippy->cache->set("{$this->service}_subscription_attempts", $attempts);
    }
}
