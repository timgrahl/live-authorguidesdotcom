<?php

abstract class Zippy_EmailListIntegration extends Zippy_Listener
{
    public $id;
    

    public $enabled = false;

    public $service;
    
    public $name;

    protected $lists;

    protected $api;

    public function __construct()
    {
        if (defined('ZIPPY_DOING_UPGRADE') && ZIPPY_DOING_UPGRADE) {
            return;
        }

        add_action('init', array($this, 'register'));
        add_action('admin_init', array($this, 'clearTransients'));

        $this->addTabs();
        $this->setup();
    }

    /**
     * Setup the integration with the appropriate settings and hooks,
     * such as landing page integration list and settings
     * @return void
     */
    abstract public function setup();

    /**
     * Set register and set up the settings for the email list
     * @return void
     */
    abstract public function settings();

    /**
     * Integrate with the correct metaboxes using zippy_metabox filter
     * @return void
     */
    abstract public function metaboxes($metaboxes);
    
    /**
     * Use API to add a user to the list
     * @param  Zippy_User      $student
     * @param  Zippy_EmailList $list
     * @return bool
     */
    protected function subscribeStudent(Zippy_User $student, Zippy_EmailList $list)
    {
        if ($this->api->getSubscriptionAttempted($student, $list)) {
            return;
        }

        $student->fetch();
        
        $this->api->logSubscriptionAttempt($student, $list);
        $this->api->subscribe($student, $list);
    }

    /**
     * Use API to remove a user from a list
     * @param  Zippy_User      $student
     * @param  Zippy_EmailList $list
     * @return bool
     */
    protected function unsubscribeStudent(Zippy_User $student, Zippy_EmailList $list)
    {
        $this->api->unsubscribe($student, $list);
    }


    public function handleJoinProduct(ZippyCourses_JoinProduct_Event $event)
    {
        if (!$this->isEnabled()) {
            return;
        }
        
        $zippy = Zippy::instance();

        $student = $event->student;
        $product = $event->product;

        if ($student->getId()) {
            $this->_subscribeToProductList($student, $product);
            $this->_subscribeToProductCourseAndTierLists($student, $product);
        }
    }


    private function _subscribeToProductCourseAndTierLists(Zippy_User $student, Zippy_Product $product)
    {
        $zippy = Zippy::instance();
        
        $courses_and_tiers = $product->getCoursesAndTiers();

        foreach ($courses_and_tiers as $course) {
            $course_id  = $course['id'];
            $tiers      = $course['tiers'];

            $services = array_filter((array) $zippy->utilities->getJsonMeta($course_id, 'email_lists', true));

            foreach ($services as $service) {
                if ($service->service !== $this->id) {
                    continue;
                }

                if ($service->per_tier) {
                    foreach ($service->tiers as $tier) {
                        if ($tier && in_array($tier->ID, $tiers)) {
                            if ($tier->list) {
                                $list = $zippy->make('email_list', array('id' => $tier->list, 'name' => ''));
                                $this->subscribeStudent($student, $list);
                            }
                        }
                    }
                } else {
                    if ($service->list) {
                        $list = $zippy->make('email_list', array('id' => $service->list, 'name' => ''));
                        $this->subscribeStudent($student, $list);
                    }
                }
            }
        }
    }

    private function _subscribeToProductList(Zippy_User $student, Zippy_Product $product)
    {
        $zippy = Zippy::instance();

        $services = array_filter((array) $zippy->utilities->getJsonMeta($product->getId(), 'email_lists', true));

        $service_list = null;
        foreach ($services as $service) {
            if ($service->service == $this->id) {
                $service_list = $service->list;
            }

            if ($service_list !== null) {
                break;
            }
        }

        if ($service_list) {
            $list = $list = $zippy->make('email_list', array('id' => $service_list, 'name' => ''));
            $this->subscribeStudent($student, $list);
        }
    }

    private function _unsubscribeToProductCourseAndTierLists(Zippy_User $student, Zippy_Product $product)
    {
        $zippy = Zippy::instance();
        
        $courses_and_tiers = $product->getCoursesAndTiers();

        foreach ($courses_and_tiers as $course) {
            $course_id  = $course['id'];
            $tiers      = $course['tiers'];

            $services = array_filter((array) $zippy->utilities->getJsonMeta($course_id, 'email_lists', true));
            foreach ($services as $service) {
                if ($service->service !== $this->id) {
                    continue;
                }

                if ($service->per_tier) {
                    foreach ($service->tiers as $tier) {
                        if ($tier && in_array($tier->ID, $tiers)) {
                            $list = $list = $zippy->make('email_list', array('id' => $tier->list, 'name' => ''));
                            $this->unsubscribeStudent($student, $list);
                        }
                    }
                } else {
                    $list = $list = $zippy->make('email_list', array('id' => $service->list, 'name' => ''));
                    $this->unsubscribeStudent($student, $list);
                }
            }
        }
    }

    private function _unsubscribeToProductList(Zippy_User $student, Zippy_Product $product)
    {
        $zippy = Zippy::instance();

        $services = array_filter((array) $zippy->utilities->getJsonMeta($product->getId(), 'email_lists', true));
        
        $service_list = null;
        foreach ($services as $service) {
            if ($service->service == $this->id) {
                $service_list = $service->list;
            }

            if ($service_list !== null) {
                break;
            }
        }

        if ($service_list) {
            $list = $list = $zippy->make('email_list', array('id' => $service_list, 'name' => ''));

            $this->unsubscribeStudent($student, $list);
        }
    }

    public function handleLeaveProduct(ZippyCourses_LeaveProduct_Event $event)
    {
        if (!$this->isEnabled()) {
            return;
        }

        $zippy = Zippy::instance();

        $student = $event->student;
        $product = $event->product;

        $this->_unsubscribeToProductList($student, $product);
        $this->_unsubscribeToProductCourseAndTierLists($student, $product);
    }

    public function handleLandingPageSubscribe(ZippyCourses_LandingPageSubscribe_Event $event)
    {
        if (!$this->isEnabled()) {
            return;
        }

        $zippy = Zippy::instance();

        $service = $event->service;
        $student = $event->student;
        $list    = $event->list;

        if ($service == $this->id) {
            $this->subscribeStudent($student, $list);
        }
    }

    public function handle(Zippy_Event $event)
    {
        $method = 'handle' . $event->getEventName();

        if (method_exists($this, $method)) {
            call_user_func_array(array($this, $method), array($event));
        }
    }

    /**
     * Handle the registration of events
     * @return void
     */
    public function register()
    {
        $zippy = Zippy::instance();

        $zippy->events->register('JoinProduct', $this);
        $zippy->events->register('LeaveProduct', $this);
        $zippy->events->register('LandingPageSubscribe', $this);
    }

    /**
     * Make sure that tabs are set up where needed
     * @return void
     */
    protected function addTabs()
    {
        add_filter('zippy_enable_course_email_list_tab', '__return_true');
        add_filter('zippy_enable_product_email_list_tab', '__return_true');
    }

    public function getSettingsName()
    {
        return 'zippy_' . $this->service . '_email_integration_settings';
    }

    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the value of id.
     *
     * @param mixed $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the value of service.
     *
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Sets the value of service.
     *
     * @param mixed $service the service
     *
     * @return self
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }


    public function metaboxLists($data, $field)
    {
        if ($field == $this->id . '_lists') {
            $data = json_encode($this->api->getListsDropdownOptions());
        }

        return $data;
    }

    public function clearTransients()
    {
        if (!isset($_POST[$this->getSettingsName()])) {
            return;
        }

        global $wpdb;

        $sql = $wpdb->prepare(
            "DELETE FROM $wpdb->options WHERE option_name LIKE %s OR option_name LIKE %s",
            '%_transient_zippy_' . $this->id . '%',
            '%_transient_timeout_zippy_' . $this->id . '%'
        );

        $wpdb->query($sql);
    }
}
