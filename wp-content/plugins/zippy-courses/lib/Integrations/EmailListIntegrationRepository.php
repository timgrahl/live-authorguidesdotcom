<?php

class Zippy_EmailListIntegrationRepository extends Zippy_Repository
{
    public $valid_types = array('Zippy_EmailListIntegration');

    /**
     * Singleton instance for our repository
     * @var \Zippy_EmailListIntegrationRepository
     */
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_action('init', array($this, 'setup'));
    }

    public function setup()
    {
        foreach ($this->items as $item) {
            $item->setup();
        }
    }
}
