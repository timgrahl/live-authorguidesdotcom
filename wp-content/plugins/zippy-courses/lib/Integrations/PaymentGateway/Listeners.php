<?php

class Zippy_PaymentGateway_Listeners extends Zippy_Repository
{
    public $valid_types = array('Zippy_PaymentGateway_Listener');

    /**
     * Singleton instance for our repository
     * @var Zippy_PaymentGateway_Listeners
     */
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $zippy = Zippy::instance();

        $listener = $zippy->make('payment_gateway_listener', array('id' => 'paypal'));
        $this->add($listener);

        add_action('init', array($this, 'register'), 9);
    }

    public function register()
    {
        foreach ($this->all() as $listener) {
            $listener->register();
        }
    }
}
