<?php

abstract class Zippy_PaymentGatewayIntegration implements Zippy_RepositoryObject
{

    /**
     * Handles error checking that an integration is properly constructed
     * and at least has the correct starting information.
     */
    public function __construct()
    {
        add_filter('zippy_classmap', array($this, 'map'));
        add_action('init', array($this, 'register'), 8);
    }

    /**
     * Add dependencies to the Zippy Courses classmap
     * @return array $classes
     */
    abstract protected function map($classes);

    /**
     * Register the gateway with Zippy Courses
     * @return void
     */
    abstract protected function register();

    /**
     * Add Action hooks for the integration
     * @return void
     */
    abstract protected function actions();

    /**
     * Add Filter hooks for the integration
     * @return void
     */
    abstract protected function filters();

    /**
     * Create the settings panels for this integration
     * @return void
     */
    abstract protected function settings();

    public function getSettingsName()
    {
        return 'zippy_' . $this->service . '_payment_gateway_settings';
    }
}
