<?php

class Zippy_PaymentGateways extends Zippy_Repository
{
    public $valid_types = array('Zippy_PaymentGateway');

    /**
     * Singleton instance for our repository
     * @var \Zippy_EmailListIntegrationRepository
     */
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_action('init', array($this, 'registerListeners'));
    }

    public function registerListeners()
    {
        foreach ($this->items as $item) {
            $item->registerListener();
        }
    }
}
