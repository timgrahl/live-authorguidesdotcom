<?php

abstract class Zippy_PaymentGateway implements Zippy_RepositoryObject
{
    public function __construct()
    {
    }
    
    abstract public function setup();
}