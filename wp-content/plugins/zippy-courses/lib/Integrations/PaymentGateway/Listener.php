<?php

abstract class Zippy_PaymentGatewayListener extends Zippy_Listener
{
    public $gateway;

    public function __construct($gateway)
    {
        $this->gateway = $gateway;
    }

    public function register()
    {
        $zippy = Zippy::instance();
        $zippy->events->register('TransactionNotification', $this);
    }

    public function handle(Zippy_Event $event)
    {
        if ($this->validateGateway($event)) {
            $this->process($event);
        }
    }

    abstract protected function process(Zippy_Event $event);
}
