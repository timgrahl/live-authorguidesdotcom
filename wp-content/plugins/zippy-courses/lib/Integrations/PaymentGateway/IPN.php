<?php

class Zippy_PaymentGatewayIPN
{
    public function __construct()
    {
        add_action('template_redirect', array($this, 'thankYou'));
        add_action('template_redirect', array($this, 'receive'));
    }

    public function receive()
    {
        $gateway = get_query_var('zippy_ipn');

        if ($gateway) {
            $zippy = Zippy::instance();
            $event = new ZippyCourses_TransactionNotification_Event($gateway);
            $zippy->events->fire($event);
            exit;
        }
    }

    public function thankYou()
    {
        global $post;
        $zippy = Zippy::instance();

        if (is_object($post) && $zippy->core_pages->is($post->ID, 'thank_you')) {
            $zippy = Zippy::instance();
            $zippy->events->fire(new ZippyCourses_TransactionNotification_Event());
        }
    }
}
