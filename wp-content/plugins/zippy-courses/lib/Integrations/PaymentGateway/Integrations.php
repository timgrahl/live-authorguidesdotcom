<?php

class Zippy_PaymentGatewayIntegrations extends Zippy_Repository
{
    public $valid_types = array('Zippy_PaymentGatewayIntegration');

    /**
     * Singleton instance for our repository
     * @var \Zippy_PaymentGatewayIntegrations
     */
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_action('init', array($this, 'setup'), 9);
    }

    public function setup()
    {
        foreach ($this->items as $item) {
            $item->setup();
        }
    }
}
