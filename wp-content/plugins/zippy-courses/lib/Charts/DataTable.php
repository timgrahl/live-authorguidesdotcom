<?php
abstract class Zippy_DataTable
{
    public $filtered = array();
    public $hidden = array();
    
    public function filterValue($key, $value)
    {
        return $value;
    }

    public function render()
    {
        $output = '<table>';
            $output .= $this->renderDataHeaders();
            $output .= $this->renderDataRows();
        $output .= '</table>';
        
        return $output;
    }

    public function renderDataHeaders()
    {
        $headers = $this->dataset->getDataHeaders();
            
        $output  = '<thead><tr>';
        foreach ($headers as $key => $header) {
            $output .= '<th>' . $header . '</th>';
        }
        $output .= '</tr></thead>';

        return $output;
    }

    public function renderDataRows()
    {
        $output = '<tbody>';

        foreach ($this->dataset->getData() as $row) {
            $output .= '<tr>';
            foreach ($row as $key => $value) {
                if (!in_array($key, $this->hidden)) {
                    $output .= '<td>' . $this->filterValue($key, $value) . '</td>';
                }
            }
            $output .= '</tr>';
        }

        $output .= '</tbody>';

        return $output;
    }
}
