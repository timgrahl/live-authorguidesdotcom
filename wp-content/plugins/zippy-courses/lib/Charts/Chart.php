<?php

abstract class Zippy_Chart
{
    /**
     * A key => value array of data
     *
     * @since 1.0.0
     * @var array
     */
    public $data = array();

    /**
     * The title of the chart
     * @var string
     */
    public $title;

    public $width = 780;
    public $height = 260;

    abstract public function fetchData();

    public function render()
    {
        $keys   = json_encode(array_keys($this->data));
        $values = json_encode(array_values($this->data));

        $html = '<div class="zippy-chart" data-keys=\'' . $keys . '\' data-values="' . $values . '">';
            $html .= '<canvas height="' . $this->height . 'px" width="' . $this->width . 'px"></canvas>';
        $html .= '</div>';

        return $html;
    }

    public function show()
    {
        echo $this->render();
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * Gets the value of width.
     *
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Sets the value of width.
     *
     * @param mixed $width the width
     *
     * @return self
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Gets the value of height.
     *
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Sets the value of height.
     *
     * @param mixed $height the height
     *
     * @return self
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }
}