<form role="search" method="get" class="search-form form-inline" action="<?php echo home_url(); ?>">
    <div class="form-group">
        <label class="sr-only" for="search-form">Search for:</label>
        <input type="search" id="search-form" class="form-control" placeholder="<?php _e('Search…', ZippySocialTriggers::TEXTDOMAIN); ?>" value="" name="s" title="<?php _e('Search for:', ZippySocialTriggers::TEXTDOMAIN); ?>">
        <input type="submit" class="zippy-button zippy-button-primary search-submit" value="<?php _e('Search', ZippySocialTriggers::TEXTDOMAIN); ?>">
    </div>
</form>