<?php do_action('zippy_entry_content_before'); ?>
<div class="row entry-content">
    <div class="col-xs-12">
        <?php do_action('zippy_entry_content_top'); ?>
            <?php the_content(); ?>    
        <?php do_action('zippy_entry_content_bottom'); ?>
    </div>
</div>
<?php do_action('zippy_entry_content_after');
