<div class="entry-navigation">
    <div class="previous-links">
        <?php previous_post_link('%link', '&laquo; ' . __('Previous', ZippySocialTriggers::TEXTDOMAIN)); ?>
        <?php previous_posts_link('%link', '&laquo; ' . __('Previous', ZippySocialTriggers::TEXTDOMAIN)); ?>
    </div>

    <div class="next-links">
        <?php next_post_link('%link', __('Next', ZippySocialTriggers::TEXTDOMAIN) . ' &raquo;'); ?>
        <?php next_posts_link('%link', __('Next', ZippySocialTriggers::TEXTDOMAIN) . ' &raquo;'); ?>
    </div>
</div>