<article <?php post_class(); ?>>
<header class="entry-header fourohfour-header row">
    <div class="col-xs-12">
        <h2 class="entry-title fourohfour-title"><?php _e('Oops! We couldn\'t find what you were looking for.', ZippySocialTriggers::TEXTDOMAIN); ?></h2>
    </div>
</header>

<div class="entry-content row">
    <div class="col-xs-12">
        <p><?php _e('Please feel free to search for what you\'re looking for', ZippySocialTriggers::TEXTDOMAIN); ?>:</p>
        <?php get_search_form(); ?>
    </div>
</div>
</article>