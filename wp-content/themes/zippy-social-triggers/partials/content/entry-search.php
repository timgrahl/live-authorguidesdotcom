<?php

global $wp_query;

$search_term = $wp_query->query['s'];

?>

<header class="search-header entry-header row">
    <div class="col-xs-12">
        <h2 class="search-title entry-title"><?php _e('Search', ZippySocialTriggers::TEXTDOMAIN); ?></h2>

        <form role="search" method="get" class="search-form form-inline" action="<?php echo home_url(); ?>">
            <div class="form-group">
                <label class="sr-only" for="search-form"><?php _e('Search for:', ZippySocialTriggers::TEXTDOMAIN); ?></label>
                <input type="search" id="search-form" class="form-control" placeholder="Search…" value="<?php echo $search_term; ?>" name="s" title="<?php _e('Search for:', ZippySocialTriggers::TEXTDOMAIN); ?>">
                <input type="submit" class="zippy-button zippy-button-primary search-submit" value="<?php _e('Search', ZippySocialTriggers::TEXTDOMAIN); ?>">
            </div>
        </form>
    </div>
</header>
