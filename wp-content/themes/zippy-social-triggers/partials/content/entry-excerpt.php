<?php
    global $post;
    $excerpt = trim($post->post_excerpt) == '' ? wp_trim_words($post->post_content, 40) : $post->post_excerpt;
?>

<?php do_action('zippy_entry_content_before'); ?>
<div class="row entry-content entry-excerpt">
    <div class="col-xs-12">
        <?php do_action('zippy_entry_content_top'); ?>
            <?php echo wpautop($excerpt); ?>    
            <div class="text-right">
                <a href="<?php the_permalink(); ?>" class="zippy-button zippy-button-primary"><?php _e('Read Post', ZippySocialTriggers::TEXTDOMAIN); ?></a>
            </div>
        <?php do_action('zippy_entry_content_bottom'); ?>
    </div>
</div>
<?php do_action('zippy_entry_content_after');
