<header class="row <?php ZTF_Template::cssClass('header'); ?>">
    <div class="col-xs-12">
        <?php if (is_home() || is_search()) : ?>
            <h2 class="<?php ZTF_Template::cssClass('title'); ?>"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php else : ?>
            <h2 class="<?php ZTF_Template::cssClass('title'); ?>"><?php the_title(); ?></h2>
        <?php endif; ?>

        <?php if (is_home() || get_post_type() == 'post'): ?>
            <div class="entry-meta row">
                <div class="col-xs-6">
                    Posted on <?php the_date('M d, Y'); ?>
                </div>
                <div class="col-xs-6 text-right">
                    <a href="<?php echo get_comments_link(); ?>" class="comments-link"><?php echo get_comments_number(); ?> <?php _e('Comments', ZippySocialTriggers::TEXTDOMAIN); ?></a>
                </div>
            </div>
        <?php endif; ?>

        <?php do_action('zippy_social_triggers_entry_header_bottom'); ?>
    </div>
</header>