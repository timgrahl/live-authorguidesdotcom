<?php
    global $post;

    $zippy_social_triggers = ZippySocialTriggers::instance();
    $zippy_framework = ZippyThemeFramework::instance();

    $has_unit_nav = $zippy_social_triggers->hasUnitNav($post->ID);
    
    if ($zippy_framework->utilities->hasFeaturedMedia($post->ID)) : ?>
        <?php do_action('zippy_featured_media_before'); ?>   
        <div class="row entry-featured-media">
            <?php do_action('zippy_featured_media_top'); ?>
            <div class="<?php echo $has_unit_nav ? 'col-sm-8' : ''; ?> col-xs-12">
                <?php the_post_thumbnail(); ?>
            </div>
            <?php if ($has_unit_nav) : ?>
            <div class="col-sm-4 col-xs-12 zippy-social-triggers-unit-nav">
                <?php echo ZTF_Utilities_Course::getUnitEntriesHtml(); ?>
            </div>
            <?php endif; ?>
            <?php do_action('zippy_featured_media_bottom'); ?>   
        </div>
        <?php do_action('zippy_featured_media_after'); ?>
    <?php endif; ?>
