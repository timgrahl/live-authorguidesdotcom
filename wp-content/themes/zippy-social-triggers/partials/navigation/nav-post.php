<?php if (is_single() && !is_page()) : ?>
<div class="entry-navigation row">
    <div class="previous-links col-xs-6">
        <?php previous_post_link('%link', '&laquo; ' . __('Previous', ZippySocialTriggers::TEXTDOMAIN)); ?>
    </div>

    <div class="next-links col-xs-6">
        <?php next_post_link('%link', __('Next', ZippySocialTriggers::TEXTDOMAIN) . ' &raquo;'); ?>
    </div>
</div>
<?php endif; ?>