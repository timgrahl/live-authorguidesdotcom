<?php

$zippy_social_triggers      = ZippySocialTriggers::instance();

$nav_header = ' <div class="container"><div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#%1$s">
      <span class="sr-only">' . __('Toggle navigation', ZippySocialTriggers::TEXTDOMAIN) . '</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="' . esc_url(home_url('/')) . '" rel="home">' . $zippy_social_triggers->getMenuLogo() . '</a>
  </div>';

wp_nav_menu(array(
    'theme_location'  => 'top',
    'container'       => 'nav',
    'container_class' => 'navbar navbar-default navbar-inverse navbar-static-top',
    'container_id'    => 'primary-nav',
    'menu_class'      => 'nav navbar-nav navbar-right',
    'menu_id'         => 'top-nav',
    'echo'            => true,
    'fallback_cb'     => '',
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => '',
    'items_wrap'      => $nav_header . '<div class="navbar-collapse collapse" id="%1$s"><ul class="%2$s">%3$s</ul></div></div>',
    'depth'           => 0,
    'walker'          => new ZippySocialTriggers_NavWalker
));
