<?php if (is_search()): ?>
    
    <?php get_template_part('partials/search/search', 'pagination'); ?>

<?php elseif (is_page()): ?>


<?php elseif (function_exists('is_bbpress') && is_bbpress()) : ?>
<?php else: ?>
    <nav class="entry-navigation row">
        <div class="col-xs-6 previous-links">
            <?php previous_posts_link(); // previous_post_link('%link', '&laquo; Previous'); ?>
        </div>

        <div class="col-xs-6 next-links">
            <?php next_posts_link(); // next_post_link('%link', 'Next &raquo;'); ?>
        </div>
    </nav>
<?php endif; ?>
