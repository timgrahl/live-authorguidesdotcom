<article class="<?php echo $comment_classes; ?>" id="<?php echo $comment_id; ?>">
    <div class="comment-avatar"><img src="<?php echo $avatar ?>" /></div>
    <section class="comment-body">
        <section class="comment-content">
            <header class="comment-header">
                <?php if ($author_url != '') : ?>
                <a href="<?php echo $author_url; ?>" class="comment-author"><?php echo $author; ?></a>
                <?php else : ?>
                <span class="comment-author"><?php echo $author; ?></span>
                <?php endif; ?>
                <span class="comment-meta"> - <a href="<?php echo $permalink; ?>"><time datetime="<?php echo $time; ?>"><?php echo $time_string; ?></time></a></span>
            </header>
                    
            <?php echo wpautop($content); ?>
        
            <footer class="comment-footer">
                <?php echo $reply_link; ?>
            </footer>
        </section>
    
