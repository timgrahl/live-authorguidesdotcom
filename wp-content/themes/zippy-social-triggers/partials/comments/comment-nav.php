<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
    <h1 class="screen-reader-text"><?php _e( 'Comment navigation', ZippySocialTriggers::TEXTDOMAIN ); ?></h1>
    <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', ZippySocialTriggers::TEXTDOMAIN ) ); ?></div>
    <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', ZippySocialTriggers::TEXTDOMAIN ) ); ?></div>
</nav><!-- #comment-nav-below -->
<?php endif; // Check for comment navigation. ?>