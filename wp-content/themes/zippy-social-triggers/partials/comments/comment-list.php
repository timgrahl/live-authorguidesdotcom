<?php
if (have_comments()) :
    $default_title = __('Have a question? Ask it here!', ZippySocialTriggers::TEXTDOMAIN);
    $comment_title = get_theme_mod('options_comments_title', $default_title);

    $comment_title = $comment_title == '' ? $default_title : $comment_title;
?>
    <?php $zippy_social_triggers = ZippySocialTriggers::instance(); ?>
    <h3 class="comments-title"><?php echo $comment_title; ?></h3>

    <div class="comments-number">
        <?php echo get_comments_number(); ?>
    </div>
    <div class="comment-list">
        <?php
            wp_list_comments(array(
                'style'      => 'div',
                'avatar_size'=> 80,
                'callback' => array($zippy_social_triggers, 'comment'),
                'end-callback' => array($zippy_social_triggers, 'commentClose')
            ));
        ?>
    </div><!-- .comment-list -->

    <?php get_template_part('partials/comments/comment', 'nav'); ?>

<?php
endif; // have_comments()
