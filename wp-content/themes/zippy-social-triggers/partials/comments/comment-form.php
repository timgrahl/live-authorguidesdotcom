<?php

// Get the comment title set in the theme options, if it exists
$comment_title = get_theme_mod('options_comments_title', __('Share with other students. What did you learn in this lesson?', ZippySocialTriggers::TEXTDOMAIN));

// It appears this still gets saved as blank if you edit and remove a comments title.
// So this check confirms that the title isn't empty
if (strlen( $comment_title ) == 0 ){
	$comment_title = 'Share with other students. What did you learn in this lesson?';
}


if (comments_open()) {
    comment_form( array(
        'logged_in_as' => false,
        'id_submit' => 'comment_submit',
        'comment_notes_after' => false,
        'class_submit' => 'btn btn-primary',
        'title_reply' => $comment_title
    ));
} else {
    get_template_part('partials/comments/comment', 'closed');
}
