<?php
    global $post;

    $zippy_framework = ZippyThemeFramework::instance();
    $excerpt = $zippy_framework->utilities->getExcerpt($post->ID);
?>

<?php do_action('zippy_entry_content_before'); ?>
<div class="row entry-content entry-excerpt">
    <div class="col-xs-12">
        <?php do_action('zippy_entry_content_top'); ?>
            <?php echo wpautop($excerpt); ?>    
        <?php do_action('zippy_entry_content_bottom'); ?>
    </div>
</div>
<?php do_action('zippy_entry_content_after');
