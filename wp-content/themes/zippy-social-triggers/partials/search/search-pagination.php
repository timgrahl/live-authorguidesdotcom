<?php

global $paged, $wp_query, $wp_rewrite;

$zippy_framework = ZippyThemeFramework::instance();

$search_term    = $wp_query->query['s'];
$max_pages      = $wp_query->max_num_pages;
$current_page   = $paged ? $paged : 1;
$num_results    = $wp_query->found_posts;
$posts_per_page = get_option('posts_per_page');

$next_page = $current_page < $max_pages ? ($current_page + 1) : $max_pages;
$prev_page = $current_page > 1 ? ($current_page - 1) : 1;

$start  = ($current_page - 1) * $posts_per_page + 1;
$end    = ($current_page * $posts_per_page) > $num_results ? $num_results : ($current_page * $posts_per_page);

$output  = '<footer class="entry-footer search-footer row">';
$output .= '<div class="col-xs-12">';
$output .= '<nav class="search-pagination">';
$output .= '<p class="search-result-count">' . sprintf(__('Showing %s  -  %s of %s results.', ZippySocialTriggers::TEXTDOMAIN), $start, $end, $num_results) . '</p>';
$output .= '<ul class="pagination">';
$output .= '<li' . ($current_page == '1' ? ' class="disabled"' : '' ). '><a href="' . $zippy_framework->utilities->getSearchResultPageUrl($prev_page) . '" aria-label="' . __('Previous', ZippySocialTriggers::TEXTDOMAIN) . '"><span aria-hidden="true">&laquo;</span></a></li>';

for ($i=1; $i <= $max_pages; $i++) {
    $output .= '<li' . ($i == $current_page ? ' class="active"' : '' ). '><a href="' . $zippy_framework->utilities->getSearchResultPageUrl($i) . '">' . $i . '</a></li>';
}

$output .= '<li' . ($current_page == $max_pages ? ' class="disabled"' : '') . '><a href="' . $zippy_framework->utilities->getSearchResultPageUrl($next_page) . '" aria-label="' . __('Next', ZippySocialTriggers::TEXTDOMAIN) . '"><span aria-hidden="true">&raquo;</span></a></li>';
$output .= '</ul>';
$output .= '</nav>';
$output .= '</div>';
$output .= '</footer>';

echo $output;
