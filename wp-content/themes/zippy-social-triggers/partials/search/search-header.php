<?php

global $wp_query;

$search_term = isset($wp_query->query['s']) ? $wp_query->query['s'] : '';

?>

<h3><?php _e('Search', ZippySocialTriggers::TEXTDOMAIN); ?></h3>
<form role="search" method="get" class="search-form form-inline" action="<?php echo home_url(); ?>">
    <div class="form-group">
        <label class="sr-only" for="search-form"><?php _e('Search for:', ZippySocialTriggers::TEXTDOMAIN); ?></label>
        <input type="search" id="search-form" class="form-control" placeholder="<?php _e('Search', ZippySocialTriggers::TEXTDOMAIN); ?>…" value="<?php echo $search_term; ?>" name="s" title="<?php _e('Search for:', ZippySocialTriggers::TEXTDOMAIN); ?>">
        <input type="submit" class="zippy-button zippy-button-primary search-submit" value="<?php _e('Search', ZippySocialTriggers::TEXTDOMAIN); ?>">
    </div>
</form>
