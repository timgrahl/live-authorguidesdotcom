<header class="row <?php ZTF_Template::cssClass('header'); ?>">
    <div class="col-xs-12">
        <?php if (is_home() || is_search()) : ?>
            <h2 class="<?php ZTF_Template::cssClass('title'); ?>"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php else : ?>
            <h2 class="<?php ZTF_Template::cssClass('title'); ?>"><?php the_title(); ?></h2>
        <?php endif; ?>
    </div>
</header>