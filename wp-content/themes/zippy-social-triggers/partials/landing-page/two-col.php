<?php

global $post, $current_user;

if (!class_exists('Zippy')) {
    return;
}

$zippy      = Zippy::instance(); 
$student    = $zippy->cache->get('student');
$student_id = !is_null($student) ? $student->getId() : 0;
$first_name = !is_null($student) ? $student->getFirstName() : '';
$last_name  = !is_null($student) ? $student->getLastName() : '';
$full_name  = !is_null($student) ? $student->getFullName() : '';
$email      = !is_null($student) ? $student->getEmail() : '';
$student_id = !is_null($student) ? $student->getId() : 0;

$login_url  = $zippy->core_pages->getUrl('login');

$landing_page_options   = $zippy->utilities->getJsonMeta($post->ID, 'landing_page', true);
$landing_page_type      = isset($landing_page_options->type) ? $landing_page_options->type : 'product';

$thank_you_url          = isset($landing_page_options->thank_you_url) ? $landing_page_options->thank_you_url : '';
$disclaimer             = isset($landing_page_options->disclaimer) ? $landing_page_options->disclaimer : __('We will never share your email address.', ZippySocialTriggers::TEXTDOMAIN);
$student_prompt         = is_object($landing_page_options->student) && isset($landing_page_options->student->headline) && !empty($landing_page_options->student->headline) ? $landing_page_options->student->headline : __('Yay! You are already a member.', ZippySocialTriggers::TEXTDOMAIN);
$student_btn            = is_object($landing_page_options->student) && isset($landing_page_options->student->button) && !empty($landing_page_options->student->button) ? $landing_page_options->student->button : __('Join this Course', ZippySocialTriggers::TEXTDOMAIN);
$visitor_prompt         = is_object($landing_page_options->visitor) && isset($landing_page_options->visitor->headline) && !empty($landing_page_options->visitor->headline) ? $landing_page_options->visitor->headline : __('Become a member!', ZippySocialTriggers::TEXTDOMAIN);
$visitor_btn            = is_object($landing_page_options->visitor) && isset($landing_page_options->visitor->button) && !empty($landing_page_options->visitor->button) ? $landing_page_options->visitor->button : __('Register Now', ZippySocialTriggers::TEXTDOMAIN);

$btn    = is_user_logged_in() ? $student_btn : $visitor_btn;
$prompt = is_user_logged_in() ? $student_prompt : $visitor_prompt;
?>

<article <?php post_class(); ?>>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <?php get_template_part('partials/content/entry', 'header'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?php get_template_part('partials/content/entry', 'featured-media'); ?>
                <?php get_template_part('partials/content/entry', 'content'); ?>
                <?php get_template_part('partials/content/entry', 'footer'); ?>
            </div>
            <div class="col-sm-5 col-sm-offset-1 zippy-landing-page-registration">
            <?php if (!is_user_logged_in()): ?>
                <h3><?php echo $prompt; ?></h3>
                <?php if ($landing_page_type == 'email') : ?>
                <form method="POST" class="zippy-form landing-page-subscribtion-form" action="">
                    <div class="form-group">
                        <label class="sr-only"><?php _e('Full Name', ZippySocialTriggers::TEXTDOMAIN); ?></label>
                        <input class="form-control" type="text" name="full_name" placeholder="<?php _e('Full Name', ZippySocialTriggers::TEXTDOMAIN); ?>" value="<?php echo $full_name; ?>"/>
                    </div>
                    <div class="form-group">
                        <label class="sr-only"><?php _e('Email', ZippySocialTriggers::TEXTDOMAIN); ?></label>
                        <input class="form-control" type="text" name="user_email" placeholder="<?php _e('Email Address', ZippySocialTriggers::TEXTDOMAIN); ?>" value="<?php echo $student_email; ?>" />
                    </div>

                    <div class="form-group text-center">
                        <input type="submit" class="zippy-button zippy-button-primary btn-lg" value="<?php echo $btn; ?>" />
                    </div>

                    <div class="form-group text-center">
                        <p class="small"><em><?php echo $disclaimer; ?></em></p>
                    </div>

                    <input type="hidden" name="landing_page_id" value="<?php echo $post->ID; ?>" />
                    <input type="hidden" name="zippy_form" value="landing_page_list" />
                <?php else : ?>
                <form method="POST" class="zippy-form landing-page-registration-form" action="">
                    <div class="form-group">
                        <label class="sr-only"><?php _e('Full Name', ZippySocialTriggers::TEXTDOMAIN); ?></label>
                        <input class="form-control" type="text" name="full_name" placeholder="<?php _e('Full Name', ZippySocialTriggers::TEXTDOMAIN); ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only"><?php _e('Email', ZippySocialTriggers::TEXTDOMAIN); ?></label>
                        <input class="form-control" type="text" name="user_email" placeholder="<?php _e('Email Address', ZippySocialTriggers::TEXTDOMAIN); ?>" />
                    </div>                    
                    <div class="form-group">
                        <label class="sr-only"><?php _e('Username', ZippySocialTriggers::TEXTDOMAIN); ?></label>
                        <input class="form-control" type="text" name="user_login" placeholder="<?php _e('Username', ZippySocialTriggers::TEXTDOMAIN); ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only"><?php _e('Password', ZippySocialTriggers::TEXTDOMAIN); ?></label>
                        <input class="form-control" type="password" name="user_pass" placeholder="<?php _e('Password', ZippySocialTriggers::TEXTDOMAIN); ?>" />
                    </div>

                    <div class="form-group text-center">
                        <input type="submit" class="zippy-button zippy-button-primary btn-lg" value="<?php echo $btn; ?>" />
                    </div>

                    <input type="hidden" name="student_id" value="<?php echo $student_id; ?>" />
                    <input type="hidden" name="landing_page_id" value="<?php echo $post->ID; ?>" />
                    <input type="hidden" name="zippy_form" value="landing_page_product" />

                    <div class="form-group text-center">
                        <p class="small"><em><?php echo $disclaimer; ?></em></p>
                    </div>
                        
                    <?php endif; ?>
                </form>

                <p class="text-center"><small><?php _e('Already a member?', ZippySocialTriggers::TEXTDOMAIN); ?> <a href="<?php echo $login_url . '?redirect_to=' . get_permalink(); ?>"><?php _e('Login here', ZippySocialTriggers::TEXTDOMAIN); ?></a></small></p>
            <?php else : ?>
                <h3><?php echo $prompt; ?></h3>
                <form method="POST" class="zippy-form" action="">

                    <input type="hidden" name="zippy_form" value="landing_page_product" />
                    <input type="hidden" name="student_id" value="<?php echo $current_user->ID; ?>" />
                    <input type="hidden" name="landing_page_id" value="<?php echo $post->ID; ?>" />

                    <div class="form-group text-center">
                        <input type="submit" class="zippy-button zippy-button-primary btn-lg" value="<?php echo $btn; ?>" />
                    </div>
                </form>
            <?php endif; ?>
            </div>
        </div>
    </div>
</article>