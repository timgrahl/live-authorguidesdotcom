<?php
/*
Template Name: Landing Page
*/

global $wp_query;

$zippy_social_triggers      = ZippySocialTriggers::instance();
$zippy_framework    = ZippyThemeFramework::instance();

get_header();

if ($zippy_framework->layout->getLayout() == '1') {
    get_sidebar();
}

if (have_posts()) {
?>
    
    <div class="col-sm-12">

    <?php
    
    // Start the Loop.
    while (have_posts()) {
        the_post();

        get_template_part('partials/landing-page/two-col');

    }
?>
    </div>

<?php
}
get_footer();
