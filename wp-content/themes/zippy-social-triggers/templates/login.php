<?php
/*
Template Name: Login
*/

global $post;

$zippy_social_triggers      = ZippySocialTriggers::instance();

get_template_part('partials/headers/header', 'login');
?>

<div class="login-form-container">
    <h1 class="site-title text-center"><?php echo $zippy_social_triggers->getLoginPageLogo(); ?></h1>

    <p class="signin-prompt"><?php printf(__('Sign into %s', ZippySocialTriggers::TEXTDOMAIN), get_bloginfo('name')); ?></p>

    <?php get_template_part('partials/content/entry', 'content'); ?>

    <?php 
    if (class_exists('Zippy') && is_object($post)) {
        $zippy = Zippy::instance();

         if (!$zippy->core_pages->is($post->ID, 'login')) {
            echo do_shortcode('[zippy_login_form]');
         }
    } ?>

    <?php if($zippy_social_triggers->hasInfoURL()) : ?>
    <div class="not-a-member">
        <h3><?php printf(__('Not a member of %s?', ZippySocialTriggers::TEXTDOMAIN), get_bloginfo('name')); ?></h3>
        <p><a href="<?php echo $zippy_social_triggers->getInfoURL(); ?>"><?php _e('Click here to find out more', ZippySocialTriggers::TEXTDOMAIN); ?></a></p>
    </div>
<?php endif; ?>
</div>

<?php
get_footer();
