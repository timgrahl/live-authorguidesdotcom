<?php $zippy_framework = ZippyThemeFramework::instance(); ?>

<section class="<?php echo $zippy_framework->layout->getSidebarContainerClasses(); ?> sidebar">
    <?php dynamic_sidebar('primary-sidebar'); ?>
</section>