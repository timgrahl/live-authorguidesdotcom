<?php $zippy_framework = ZippyThemeFramework::instance(); ?>

                        </div><!-- .row -->
                    </div><!-- .container -->
                </div><!-- .col-sm-12 -->
            </div><!-- .site-body -->
        
        <?php if ($zippy_framework->layout->hasFooter()) : ?>
            <?php do_action('zippy_footer_before'); ?>   
            <footer class="row site-footer">
                <?php do_action('zippy_footer_top'); ?>   
                <div class="col-sm-4 text-left">
                    <?php dynamic_sidebar( 'footer-left' ); ?>
                </div>

                <div class="col-sm-4 text-center">
                    <?php dynamic_sidebar( 'footer-middle' ); ?>
                </div>

                <div class="col-sm-4 text-right">
                    <?php dynamic_sidebar( 'footer-right' ); ?>
                </div>
                
                <?php do_action('zippy_footer_bottom'); ?>   

            </footer>
            <?php do_action('zippy_footer_after'); ?>   
        <?php endif; ?>
        
        </main> <!-- .container-fluid -->
        
        <?php wp_footer(); ?>
    </body>
</html>