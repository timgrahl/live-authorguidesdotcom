<?php

/**
 * @todo Concat, minify and include javascripts
 * @todo Include modernizr
 *
 */

global $wp_query;
$zippy_framework = ZippyThemeFramework::instance();

get_header();

if ($zippy_framework->layout->getLayout() == '1') {
    get_sidebar();
}

if (have_posts()) { 
?>
    
    <div class="<?php echo $zippy_framework->layout->getContentContainerClasses(); ?>">

    <?php
    
    if (is_search()) {
        get_template_part('partials/content/entry', 'search');
    }

    // Start the Loop.
    while (have_posts()) {
        the_post();

        /**
         * Include the post format-specific template for the content. If you want to
         * use this in a child theme, then include a file called called content-___.php
         * (where ___ is the post format) and that will be used instead.
         */
        get_template_part('content', get_post_format());

    }

    get_template_part('partials/navigation/nav', 'posts');
    ?>

    </div>

    <?php
} else {
    ?>
    <div class="<?php echo $zippy_framework->layout->getContentContainerClasses(); ?>">
    <?php
    // If no content, include the "No posts found" template.
    get_template_part('partials/content/entry', 'none');
    ?>
    </div>
    <?php
}

if ($zippy_framework->layout->getLayout() == '0') {
    get_sidebar();
}

get_footer();
