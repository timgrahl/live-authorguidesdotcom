<?php

/**
 * Search Loop - Single Topic
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div id="post-<?php bbp_topic_id(); ?>" <?php bbp_topic_class(); ?>>
	
	<div class="bbp-topic-header col-sm-12">

		<div class="bbp-meta">
			<a href="<?php bbp_topic_permalink(); ?>" class="bbp-topic-permalink">#<?php bbp_topic_id(); ?></a>
		</div><!-- .bbp-meta -->

		<div class="bbp-topic-title">

			<?php do_action( 'bbp_theme_before_topic_title' ); ?>

			<h3>
				<?php _e( 'Topic: ', ZippySocialTriggers::TEXTDOMAIN ); ?> 
				<a href="<?php bbp_topic_permalink(); ?>"><?php bbp_topic_title(); ?></a>

				<?php if ( function_exists( 'bbp_is_forum_group_forum' ) && bbp_is_forum_group_forum( bbp_get_topic_forum_id() ) ) : ?>

					<?php _e( 'in group forum ', ZippySocialTriggers::TEXTDOMAIN ); ?>

				<?php else : ?>

					<?php _e( 'in forum ', ZippySocialTriggers::TEXTDOMAIN ); ?>

				<?php endif; ?>

				<a href="<?php bbp_forum_permalink( bbp_get_topic_forum_id() ); ?>"><?php bbp_forum_title( bbp_get_topic_forum_id() ); ?></a>
				
			</h3>

			<?php do_action( 'bbp_theme_after_topic_title' ); ?>


		</div><!-- .bbp-topic-title -->

		<div class="bbp-meta">

			<span class="bbp-topic-post-date"><?php bbp_topic_post_date( bbp_get_topic_id() ); ?></span>

		</div><!-- .bbp-meta -->

	</div><!-- .bbp-topic-header -->

</div><!-- #post-<?php bbp_topic_id(); ?> -->
