<?php

/**
 * Replies Loop - Single Reply
 *
 * @package bbPress
 * @subpackage Theme
 */

?>
<div <?php bbp_reply_class(); ?>>

	<div class="bbp-reply-author col-sm-2 hidden-xs">

		<?php do_action( 'bbp_theme_before_reply_author_details' ); ?>

		<?php bbp_reply_author_link( array( 'sep' => '<br />', 'show_role' => true ) ); ?>

		<?php if ( bbp_is_user_keymaster() ) : ?>

			<?php do_action( 'bbp_theme_before_reply_author_admin_details' ); ?>

			<div class="bbp-reply-ip"><?php bbp_author_ip( bbp_get_reply_id() ); ?></div>

			<?php do_action( 'bbp_theme_after_reply_author_admin_details' ); ?>

		<?php endif; ?>

		<?php do_action( 'bbp_theme_after_reply_author_details' ); ?>

	</div><!-- .bbp-reply-author -->

	<div class="bbp-reply-content entry-content col-sm-10 col-xs-12">

		<div id="post-<?php bbp_reply_id(); ?>" class="bbp-reply-header">

			<div class="bbp-meta">

				<span class="hidden visible-inline-xs">Posted by <a href=""><?php bbp_reply_author(); ?></a> on</span> <span class="bbp-reply-post-date"><?php bbp_reply_post_date(); ?></span>
				
				<?php if ( bbp_is_single_user_replies() ) : ?>

					<span class="bbp-header">
						<?php _e( 'in reply to: ', ZippySocialTriggers::TEXTDOMAIN ); ?>
						<a class="bbp-topic-permalink" href="<?php bbp_topic_permalink( bbp_get_reply_topic_id() ); ?>"><?php bbp_topic_title( bbp_get_reply_topic_id() ); ?></a>
					</span>

				<?php endif; ?>

				<a href="<?php bbp_reply_url(); ?>" class="bbp-reply-permalink">#<?php bbp_reply_id(); ?></a>

			</div><!-- .bbp-meta -->

		</div><!-- #post-<?php bbp_reply_id(); ?> -->

		<?php do_action( 'bbp_theme_before_reply_content' ); ?>

		<?php bbp_reply_content(); ?>

		<?php do_action( 'bbp_theme_after_reply_content' ); ?>
	
		<?php do_action( 'bbp_theme_before_reply_admin_links' ); ?>

		<?php bbp_reply_admin_links(); ?>

		<?php do_action( 'bbp_theme_after_reply_admin_links' ); ?>

	</div><!-- .bbp-reply-content -->

</div><!-- .reply -->
