<?php

/**
 * bbPress User Profile Edit Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<form id="bbp-your-profile" action="<?php bbp_user_profile_edit_url( bbp_get_displayed_user_id() ); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">

	<?php do_action( 'bbp_user_edit_before' ); ?>

	<div class="bbp-form">
		<h3><?php _e( 'Name', ZippySocialTriggers::TEXTDOMAIN ) ?></h3>

		<?php do_action( 'bbp_user_edit_before_name' ); ?>

		<div class="form-group">
			<label for="first_name" class="control-label col-sm-2"><?php _e( 'First Name', ZippySocialTriggers::TEXTDOMAIN ) ?></label>
			<div class="col-sm-6">
				<input type="text" name="first_name" id="first_name" value="<?php bbp_displayed_user_field( 'first_name', 'edit' ); ?>" class="form-control" tabindex="<?php bbp_tab_index(); ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="last_name" class="control-label col-sm-2"><?php _e( 'Last Name', ZippySocialTriggers::TEXTDOMAIN ) ?></label>
			<div class="col-sm-6">
				<input type="text" name="last_name" id="last_name" value="<?php bbp_displayed_user_field( 'last_name', 'edit' ); ?>" class="form-control" tabindex="<?php bbp_tab_index(); ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="nickname" class="control-label col-sm-2"><?php _e( 'Nickname', ZippySocialTriggers::TEXTDOMAIN ); ?></label>
			<div class="col-sm-6">
				<input type="text" name="nickname" id="nickname" value="<?php bbp_displayed_user_field( 'nickname', 'edit' ); ?>" class="form-control" tabindex="<?php bbp_tab_index(); ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="display_name" class="control-label col-sm-2"><?php _e( 'Display Name', ZippySocialTriggers::TEXTDOMAIN ) ?></label>
			<div class="col-sm-6">
				<?php bbp_edit_user_display_name(); ?>
			</div>
		</div>

		<?php do_action( 'bbp_user_edit_after_name' ); ?>

	</div>

	<div class="bbp-form ">
		<h3><?php _e( 'Contact Info', ZippySocialTriggers::TEXTDOMAIN ) ?></h3>

		<?php do_action( 'bbp_user_edit_before_contact' ); ?>

		<div class="form-group">
			<label for="url" class="control-label col-sm-2"><?php _e( 'Website', ZippySocialTriggers::TEXTDOMAIN ) ?></label>
			<div class="col-sm-6">
				<input type="text" name="url" id="url" value="<?php bbp_displayed_user_field( 'user_url', 'edit' ); ?>" class="form-control code" tabindex="<?php bbp_tab_index(); ?>" />
			</div>
		</div>

		<?php foreach ( bbp_edit_user_contact_methods() as $name => $desc ) : ?>

			<div class="form-group">
				<label for="<?php echo esc_attr( $name ); ?>" class="form-control"><?php echo apply_filters( 'user_' . $name . '_label', $desc ); ?></label>
				<div class="col-sm-6">
					<input type="text" name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $name ); ?>" value="<?php bbp_displayed_user_field( $name, 'edit' ); ?>" class="form-control" tabindex="<?php bbp_tab_index(); ?>" />
				</div>
			</div>

		<?php endforeach; ?>

		<?php do_action( 'bbp_user_edit_after_contact' ); ?>

	</div>

	<div class="bbp-form ">
		<h3><?php bbp_is_user_home_edit() ? _e( 'About Yourself', ZippySocialTriggers::TEXTDOMAIN ) : _e( 'About the user', ZippySocialTriggers::TEXTDOMAIN ); ?></h3>

		<?php do_action( 'bbp_user_edit_before_about' ); ?>

		<div class="form-group">
			<label for="description" class="control-label col-sm-2"><?php _e( 'Biographical Info', ZippySocialTriggers::TEXTDOMAIN ); ?></label>
			<div class="col-sm-6">
				<textarea name="description" id="description" rows="5" cols="30" tabindex="<?php bbp_tab_index(); ?>" class="form-control"><?php bbp_displayed_user_field( 'description', 'edit' ); ?></textarea>
			</div>
		</div>

		<?php do_action( 'bbp_user_edit_after_about' ); ?>

	</div>

	<div class="bbp-form ">
		<h3><?php _e( 'Account', ZippySocialTriggers::TEXTDOMAIN ) ?></h3>

		<?php do_action( 'bbp_user_edit_before_account' ); ?>

		<div class="form-group">
			<label for="user_login" class="control-label col-sm-2"><?php _e( 'Username', ZippySocialTriggers::TEXTDOMAIN ); ?></label>
			<div class="col-sm-6">
				<input type="text" name="user_login" id="user_login" value="<?php bbp_displayed_user_field( 'user_login', 'edit' ); ?>" disabled="disabled" class="form-control" tabindex="<?php bbp_tab_index(); ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="email" class="control-label col-sm-2"><?php _e( 'Email', ZippySocialTriggers::TEXTDOMAIN ); ?></label>
			<div class="col-sm-6">
				<input type="text" name="email" id="email" value="<?php bbp_displayed_user_field( 'user_email', 'edit' ); ?>" class="form-control" tabindex="<?php bbp_tab_index(); ?>" />
			</div>

			<?php

			// Handle address change requests
			$new_email = get_option( bbp_get_displayed_user_id() . '_new_email' );
			if ( !empty( $new_email ) && $new_email !== bbp_get_displayed_user_field( 'user_email', 'edit' ) ) : ?>

				<span class="updated inline">

					<?php printf( __( 'There is a pending email address change to <code>%1$s</code>. <a href="%2$s">Cancel</a>', ZippySocialTriggers::TEXTDOMAIN ), $new_email['newemail'], esc_url( self_admin_url( 'user.php?dismiss=' . bbp_get_current_user_id()  . '_new_email' ) ) ); ?>

				</span>

			<?php endif; ?>

		</div>

		<div class="password form-group">
			<label for="pass1" class="control-label col-sm-2"><?php _e( 'New Password', ZippySocialTriggers::TEXTDOMAIN ); ?></label>
			<div class="col-sm-6">
				<input type="password" name="pass1" id="pass1" size="16" value="" autocomplete="off" tabindex="<?php bbp_tab_index(); ?>" class="form-control" />
				<span class="description"><?php _e( 'If you would like to change the password type a new one. Otherwise leave this blank.', ZippySocialTriggers::TEXTDOMAIN ); ?></span>
			</div>
		</div>

		<div class="password form-group">
			<label for="pass2" class="control-label col-sm-2"><?php _e( 'Repeat Password', ZippySocialTriggers::TEXTDOMAIN ); ?></label>
			<div class="col-sm-6">
				<input type="password" name="pass2" id="pass2" size="16" value="" autocomplete="off" tabindex="<?php bbp_tab_index(); ?>" class="form-control" />
				<span class="description"><?php _e( 'Type your new password again.', ZippySocialTriggers::TEXTDOMAIN ); ?></span>
			</div>
		</div>

		<div class="password form-group">
			<div class="col-sm-6 col-sm-offset-2">
				<div id="pass-strength-result"></div>
				<span class="description indicator-hint"><?php _e( 'Your password should be at least ten characters long. Use upper and lower case letters, numbers, and symbols to make it even stronger.', ZippySocialTriggers::TEXTDOMAIN ); ?></span>
			</div>
		</div>


		<?php do_action( 'bbp_user_edit_after_account' ); ?>

	</div>

	<?php if ( current_user_can( 'edit_users' ) && ! bbp_is_user_home_edit() ) : ?>

		<div class="bbp-form ">
			<h3><?php _e( 'User Role', ZippySocialTriggers::TEXTDOMAIN ); ?></h3>

			<?php do_action( 'bbp_user_edit_before_role' ); ?>

			<?php if ( is_multisite() && is_super_admin() && current_user_can( 'manage_network_options' ) ) : ?>

				<div class="form-group">
					<label for="super_admin" class="control-label col-sm-2"><?php _e( 'Network Role', ZippySocialTriggers::TEXTDOMAIN ); ?></label>
					<label>
						<input class="checkbox" type="checkbox" id="super_admin" name="super_admin"<?php checked( is_super_admin( bbp_get_displayed_user_id() ) ); ?> tabindex="<?php bbp_tab_index(); ?>" />
						<?php _e( 'Grant this user super admin privileges for the Network.', ZippySocialTriggers::TEXTDOMAIN ); ?>
					</label>
				</div>

			<?php endif; ?>

			<?php bbp_get_template_part( 'form', 'user-roles' ); ?>

			<?php do_action( 'bbp_user_edit_after_role' ); ?>

		</div>

	<?php endif; ?>

	<?php do_action( 'bbp_user_edit_after' ); ?>

	<div class="submit">
		<div class="form-group">
			<div class="col-sm-6 col-sm-offset-2">
			<?php bbp_edit_user_form_fields(); ?>
				<button type="submit" tabindex="<?php bbp_tab_index(); ?>" id="bbp_user_edit_submit" name="bbp_user_edit_submit" class="zippy-button zippy-button-primary"><?php bbp_is_user_home_edit() ? _e( 'Update Profile', ZippySocialTriggers::TEXTDOMAIN ) : _e( 'Update User', ZippySocialTriggers::TEXTDOMAIN ); ?></button>
			</div>
		</div>
	</div>

</form>