<?php

/**
 * Password Protected
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div id="bbpress-forums" class="row">
    <div class="col-xs-12">
    	<div class="bbp-form" id="bbp-protected">
    		<h3><?php _e( 'Protected', ZippySocialTriggers::TEXTDOMAIN ); ?></h3>

    		<?php echo get_the_password_form(); ?>

    	</div>
    </div>
</div>