<?php

/**
 * Topics Loop
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<?php do_action( 'bbp_template_before_topics_loop' ); ?>

<div id="bbp-forum-<?php bbp_forum_id(); ?>" class="bbp-topics">
	<div class="col-xs-12">
		<div class="bbp-header">
		
			<div class="forum-titles">
				<div class="bbp-topic-title col-md-7 col-sm-9 col-xs-12"><?php _e( 'Topic', ZippySocialTriggers::TEXTDOMAIN ); ?></div>
				<div class="bbp-topic-voice-count col-md-1 hidden-sm hidden-xs"><?php _e( 'Voices', ZippySocialTriggers::TEXTDOMAIN ); ?></div>
				<div class="bbp-topic-reply-count col-md-1 hidden-sm hidden-xs"><?php bbp_show_lead_topic() ? _e( 'Replies', ZippySocialTriggers::TEXTDOMAIN ) : _e( 'Posts', ZippySocialTriggers::TEXTDOMAIN ); ?></div>
				<div class="bbp-topic-freshness col-md-3 col-sm-3 hidden-xs"><?php _e( 'Freshness', ZippySocialTriggers::TEXTDOMAIN ); ?></div>
			</div>

		</div>

		<div class="bbp-body">
			<div class="col-xs-12">
				<?php while ( bbp_topics() ) : bbp_the_topic(); ?>

					<?php bbp_get_template_part( 'loop', 'single-topic' ); ?>

				<?php endwhile; ?>
			</div>
		</div>
		
		<?php bbp_get_template_part( 'pagination', 'topics' ); ?>

	</div><!-- #bbp-forum-<?php bbp_forum_id(); ?> -->
</div>
<?php do_action( 'bbp_template_after_topics_loop' ); ?>
