<?php

/**
 * Search 
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<form role="search" method="get" id="bbp-search-form" class="form-inline" action="<?php bbp_search_url(); ?>">
	<div class="form-group">
		<label class="screen-reader-text hidden sr-only" for="bbp_search"><?php _e( 'Search for:', ZippySocialTriggers::TEXTDOMAIN ); ?></label>
		<input type="hidden" name="action" value="bbp-search-request" />
		<input tabindex="<?php bbp_tab_index(); ?>" type="text" value="<?php echo esc_attr( bbp_get_search_terms() ); ?>" name="bbp_search" id="bbp_search" placeholder="Search Forums..." class="form-control" />
        <input type="submit" class="zippy-button zippy-button-primary search-submit" value="Search">
	</div>
</form>
