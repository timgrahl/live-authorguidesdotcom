<?php $zippy_framework = ZippyThemeFramework::instance(); ?>

<?php if (is_home()) : ?>
    <article <?php post_class(); ?>>
        <div class="col-xs-12">
            <?php get_template_part('partials/content/entry', 'header'); ?>
            <?php get_template_part('partials/content/entry', 'featured-media'); ?>
            <?php get_template_part('partials/content/entry', 'excerpt'); ?>
            <?php get_template_part('partials/content/entry', 'footer'); ?>
        </div>
    </article>
<?php elseif (is_search()): ?>
    <?php get_template_part('partials/loops/loop', 'search'); ?>    
<?php else : ?>
    <article <?php post_class(); ?>>
        <div class="col-xs-12">
            <?php get_template_part('partials/content/entry', 'header'); ?>
            <?php get_template_part('partials/content/entry', 'featured-media'); ?>
            <?php get_template_part('partials/content/entry', 'content'); ?>
            <?php get_template_part('partials/content/entry', 'footer'); ?>

            <?php get_template_part('partials/navigation/nav', 'post'); ?>

            <?php comments_template(); ?>
        </div>
    </article>
<?php endif; ?>
