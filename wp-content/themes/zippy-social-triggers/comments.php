<?php
/**
 * The template for displaying Comments
 *
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() ) {
    return;
}
?>
<div class="row">
    <div class="col-xs-12">
        <a name="comments"></a>
        <?php if (comments_open() || get_comments_number() > 0) : ?>
            <p style="margin: 0;"><a href="#" class="hidden-lg hidden-md comment-toggle">
                <?php _e('Show Comments', ZippySocialTriggers::TEXTDOMAIN); ?>
            </a></p>
        <?php endif; ?>
        <div class="row">
            <div class="comments col-sm-12">

                <?php get_template_part('partials/comments/comment', 'list'); ?>
                <?php get_template_part('partials/comments/comment', 'form'); ?>

            </div><!-- .comments -->
        </div>
    </div>
</div>