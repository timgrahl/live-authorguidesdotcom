<?php

$fileMap = array(
    'ZippySocialTriggersFramework' => 'zippy-theme-framework/zippy-theme-framework.php',
    'ZippySocialTriggers' => 'classes/zippy-social-triggers.php'
);

// Include the framework autoloader
if (!function_exists('zippy_theme_framework_autoload')) {
    function zippy_theme_framework_autoload($class_name)
    {
        
        if (strpos($class_name, 'ZTF_') === false) {
            return;
        }
        
        $class_name = str_replace('ZTF_', '', $class_name);

        $components = explode('_', $class_name);
        $file = '';

        foreach ($components as $k => $v) {
            // Add dashes before camel cased words
            $string = preg_replace('/(?<=\\w)(?=[A-Z])/', "-$1", $v);

            $file .= ($k + 1) < count($components) ? $string . DIRECTORY_SEPARATOR : $string . '.php';

        }

        //see if the file exsists
        $filepath = dirname(__FILE__) . strtolower('/zippy-theme-framework/' . $file);

        if (file_exists($filepath)) {
            require_once( $filepath );
            return;
        }

    }
    spl_autoload_register('zippy_theme_framework_autoload');
}

function zippy_social_triggers_autoload($class_name)
{
    
    if (strpos($class_name, 'ZippySocialTriggers_') === false) {
        return;
    }
    
    $class_name = str_replace('ZippySocialTriggers_', '', $class_name);

    $components = explode('_', $class_name);
    $file = '';

    foreach ($components as $k => $v) {
        // Add dashes before camel cased words
        $string = preg_replace('/(?<=\\w)(?=[A-Z])/', "-$1", $v);

        $file .= ($k + 1) < count($components) ? $string . DIRECTORY_SEPARATOR : $string . '.php';

    }

    //see if the file exsists
    $filepath = dirname(__FILE__) . strtolower('/classes/' . $file);

    if (file_exists($filepath)) {
        require_once( $filepath );
        return;
    }

}
spl_autoload_register('zippy_social_triggers_autoload');

foreach ($fileMap as $file) {
    require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . $file);
}
