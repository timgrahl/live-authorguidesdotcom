<?php

class ZTF_Widget
{
        
    public function __construct($title, $id, $description = '')
    {
        register_sidebar(array(
            'name'          => __($title, '@todo'),
            'id'            => $id,
            'description'   => __($description, '@todo'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ));

    }
}
