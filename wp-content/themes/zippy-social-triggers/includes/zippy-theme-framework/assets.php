<?php

class ZTF_Assets
{
    private $path;
    private $url;

    public function __construct()
    {
        $this->path = dirname(__FILE__) . '/assets/';
        $this->url = get_stylesheet_directory_uri() . '/includes/zippy-theme-framework/assets/';

        add_action('admin_print_scripts', array($this, 'scripts'));
        add_action('admin_print_scripts', array($this, 'styles'));
    }

    public function styles()
    {
        wp_enqueue_style('zippy-theme-framework', $this->url . 'css/admin.css', array(), '1.0.0', 'screen');
    }

    public function scripts()
    {
        wp_enqueue_script('jquery');
        wp_enqueue_script('zippy-theme-framework', $this->url . 'js/zippy-theme-framework.js', array('jquery'), '1.0.0', true);        
    }
}
