<?php

class ZTF_Template {

    public static function cssClass($region)
    {
        switch ($region)
        {
            case 'header':
                self::headerClasses();
                break;
            case 'title':
                self::titleClasses();
                break;
            default:
                break;
        }
        
    }

    private static function headerClasses()
    {
        global $post;

        echo 'entry-header ' . $post->post_type . '-header';
    }

    private static function titleClasses()
    {
        global $post;

        echo 'entry-title ' . $post->post_type . '-title';
    }
}
