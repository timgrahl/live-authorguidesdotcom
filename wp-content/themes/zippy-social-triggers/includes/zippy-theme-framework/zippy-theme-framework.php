<?php

class ZippyThemeFramework
{
    /**
     * Instance of singleton
     * @var \ZippyThemeFramework
     */
    private static $instance;

    /**
     * Sets up metaboxes for the framework
     * @var \ZTF_Metaboxes
     */
    public $metaboxes;

    /**
     * Sets up JS and CSS assets for the framework
     * @var \ZTF_Assets
     */
    private $assets;

    /**
     * Layout helper
     * @var \ZTF_Layout
     */
    public $layout;

    /**
     * Landing Page helper
     * @var \ZTF_LandingPage
     */
    public $landing_page;

    /**
     * Utilities for ZTF, mostly helper functions and data
     * @var \ZTF_Utilities
     */
    public $utilities;

    /**
     * Adds default customizer functionality and sets up object for theme to use
     * @var \ZTF_Customizer
     */
    public $customizer;

    /**
     * Sets up the ZippyThemeFramework object
     *
     * @since 1.0.0
     */
    private function __construct()
    {
        $this->assets       = new ZTF_Assets;
        $this->layout       = new ZTF_Layout;
        $this->metaboxes    = new ZTF_Metaboxes;
        $this->customizer   = new ZTF_Customizer;
        $this->landing_page = new ZTF_LandingPage;
        $this->utilities    = new ZTF_Utilities;
        
        $this->setup();
    }

    /**
     * Get an instance of ZippyThemeFramework
     *
     * @since 1.0.0
     * @return \ZippyThemeFramework
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Initialize some of the pieces that are needed for the framework,
     * such as the default customizer fields and action hooks.
     *
     * @since 1.0.0
     * @return void
     */
    private function setup()
    {
        // And with all the data set up, call our hooks
        $this->hooks();
    }

    /**
     * Register hooks for the framework
     *
     * @since 1.0.0
     * @return void
     */
    private function hooks()
    {
        add_action('admin_menu', array($this, 'options'));
        add_action('init', array($this, 'init'), 99);
    }
    
    /**
     * Register admin menu for Framework
     *
     * @since 1.0.0
     * @return void
     */
    public function options()
    {

    }

    /**
     * Perform actions that need to happen later in the sequence, such as registering customizer vars
     *
     * @since 1.0.0
     * @return void
     */
    public function init()
    {
        $this->customizer->add($this->layouts());
        $this->customizer->register();
    }

    private function layouts()
    {
        $layout_panel = array('layout' => array('name' => __('Layout', ZippySocialTriggers::TEXTDOMAIN), 'sections' => array()));

        foreach ($this->utilities->getPostTypes() as $post_type) {
            $post_type_object = get_post_type_object($post_type);
            $plural_name = $post_type_object->labels->name;

            $section = array(
                'name' => $plural_name,
                'controls' => array(
                    'layout' => array(
                        'name' => __('Layout', ZippySocialTriggers::TEXTDOMAIN),
                        'type' => 'select',
                        'description' => sprintf(__('Change the default layout for %s.', ZippySocialTriggers::TEXTDOMAIN), $plural_name),
                        'options' => array(
                            __('Right Sidebar', ZippySocialTriggers::TEXTDOMAIN),
                            __('Left Sidebar', ZippySocialTriggers::TEXTDOMAIN),
                            __('No Sidebar', ZippySocialTriggers::TEXTDOMAIN)
                        )
                    ),
                    'header_visibility' => array(
                        'name' => __('Header', ZippySocialTriggers::TEXTDOMAIN),
                        'type' => 'select',
                        'description' => __('Is the header visible?', ZippySocialTriggers::TEXTDOMAIN),
                        'options' => array('1' => 'On', '0' => 'Off')
                    ),
                    'menu_visibility' => array(
                        'name' => __('Menu', ZippySocialTriggers::TEXTDOMAIN),
                        'type' => 'select',
                        'description' => __('Is the menu visible?', ZippySocialTriggers::TEXTDOMAIN),
                        'options' => array('1' => 'On', '0' => 'Off')
                    ),
                    'footer_visibility' => array(
                        'name' => __('Footer', ZippySocialTriggers::TEXTDOMAIN),
                        'type' => 'select',
                        'description' => __('Is the footer visible?', ZippySocialTriggers::TEXTDOMAIN),
                        'options' => array('1' => 'On', '0' => 'Off')
                    )
                )
            );

            $layout_panel['layout']['sections'][$post_type] = $section;
        }

        return $layout_panel;
    }
}
