<?php

class ZTF_Metaboxes
{
    public $fields_to_save = array(
        'zippy_template_options',
        'landing_page_options',
        'landing_page_type',
        'landing_page_list_service',
        'landing_page_thank_you_url'
    );

    public function __construct()
    {
        $this->fields_to_save = apply_filters('zippy_theme_framework_fields_to_save', $this->fields_to_save);
        
        add_action('add_meta_boxes', array($this, 'register'));
        add_action('save_post', array($this, 'save'), 10, 3);
    }

    public function register()
    {
        $this->registerAll('zippy-theme-framework-layout', __('Layout', ZippySocialTriggers::TEXTDOMAIN), array($this, 'layout'), 'side', 'default');
    }

    private function registerAll($id, $title, $callback, $context, $priority)
    {
        $zippy_framework = ZippyThemeFramework::instance();

        foreach ($zippy_framework->utilities->getPostTypes() as $post_type) {
            add_meta_box($id, $title, $callback, $post_type, $context, $priority);
        }
    }

    public function save($post_id, $post, $update)
    {
        foreach ($this->fields_to_save as $meta_key) {
            if (isset($_POST[$meta_key])) {
                update_post_meta($post_id, $meta_key, $_POST[$meta_key]);
            } else {
                delete_post_meta($post_id, $meta_key);
            }
        }
    }

    public function layout()
    {
        global $post;

        // Get and setup vars for view
        $post_type = $post->post_type;

        $defaults = array(
            'override'  => 0,
            'layout'    => get_theme_mod('layout_' . $post_type . '_layout', '0'),
            'menu'      => get_theme_mod('layout_' . $post_type . '_menu_visibility', '1'),
            'header'    => get_theme_mod('layout_' . $post_type . '_header_visibility', '1'),
            'footer'    => get_theme_mod('layout_' . $post_type . '_footer_visibility', '1')
        );
        $layout = (array) get_post_meta($post->ID, 'zippy_template_options', true);
        $layout = isset($layout[0]) && $layout[0] == '' ? array() : $layout;

        $layout = array_merge($defaults, $layout);

        // Fetch the file
        ob_start();
        require_once(dirname(__FILE__) . '/metaboxes/layout.php');
        $mb = ob_get_clean();

        echo $mb;
    }

    private function addFieldsToSave($post_type, $fields)
    {
        if (!isset($this->fields_to_save[$post_type])) {
            $this->fields_to_save[$post_type] = array();
        }

        foreach ($fields as $field) {
            if (!in_array($field, $this->fields_to_save[$post_type])) {
                $this->fields_to_save[$post_type][] = $field;
            }
        }

        return $this;
    }
}
