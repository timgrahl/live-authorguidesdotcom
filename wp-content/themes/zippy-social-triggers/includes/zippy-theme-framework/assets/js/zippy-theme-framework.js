+function ($) {
    'use strict';

    var ZippyThemeFramework = {
        metaboxes: {
            layout: {
                init: function() {
                    var self = this;

                    // Configure metabox element
                    this.$el = $('#zippy-theme-framework-layout');
                    
                    // Run functions
                    this.toggleState();

                    // Register events
                    this.$el.find('input[type="checkbox"]').on('change', function() { self.toggleState(); });

                },
                isActive: function() {
                    return this.$el.find('input[type=checkbox]:checked').length > 0;
                },
                toggleState: function() {
                    if(this.isActive()) {
                        this.$el.find('select').removeAttr('disabled');
                    } else {
                        this.$el.find('select').attr('disabled', 'disabled');
                    }
                }
            },
            landing_page: {
                init: function() {
                    var self = this;
                    
                    $('#page_template').on('change', function() { self.toggleVis(); });
                    $('select[name="landing_page_type"]').on('change', function() { self.toggleTypeVis(); });
                    $('.landing-page-list-service').on('change', function() { self.toggleServiceVis(); });

                    self.toggleTypeVis();
                    self.toggleServiceVis();
                    self.toggleVis();
                },
                toggleVis: function() {
                    var self = this;
                    if($('#page_template').val() == 'templates/landing-page.php') {
                        $('#ztf-landing-page-mb').show();
                        self.hideLayoutDropdown();
                    } else {
                        $('#ztf-landing-page-mb').hide();
                        self.showLayoutDropdown();
                    }
                },
                toggleTypeVis: function() {
                    if($('select[name="landing_page_type"]').val() == 'course') {
                        $('.landing-page-email-list').hide();
                        $('.landing-page-course-registration').show();
                        $('.student-form-details').show();
                    } else {
                        $('.landing-page-email-list').show();
                        $('.landing-page-course-registration').hide();
                        $('.student-form-details').hide();
                    }
                },
                toggleServiceVis: function() {
                    var service = $('.landing-page-list-service').val();
                    $('.landing-page-list').hide();
                    $('.landing-page-list-' + service).show();
                },
                showLayoutDropdown: function() {
                    var $select = $('#zippy-theme-framework-layout select[name="zippy_template_options[layout]"]');
                    $select.parent().show();
                },
                hideLayoutDropdown: function() {
                    var $select = $('#zippy-theme-framework-layout select[name="zippy_template_options[layout]"]');
                    $select.parent().hide();
                }

            }
        },
        init: function() {
            // Register our events
            this.metaboxes.layout.init();
            this.metaboxes.landing_page.init();
        }
    };

    ZippyThemeFramework.init();



}(jQuery);