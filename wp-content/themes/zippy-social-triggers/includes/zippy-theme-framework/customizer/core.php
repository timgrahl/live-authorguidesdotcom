<?php

class ZTF_Customizer_Core
{

    public $id;
    public $panels = array();
    public $sections = array();

    public function __construct($panels = array())
    {
        foreach ($panels as $panel_id => $panel_details) {
            $this->addPanel($panel_id, $panel_details);
        }
    }

    public function addPanel($id, $details)
    {

        $this->panels[ $id ] = new ZTF_Customizer_Panel($id, $details);
        $this->panels[ $id ]->setCustomizer($this);
        return $this->panels[ $id ];
    }

    public function addSection($id, $title, $description = '', $priority = 100)
    {
        $this->sections[ $id ] = new ZTF_Customizer_Section($id, $title, $description, $priority);
        return $this->sections[ $id ];
    }

    public function register()
    {
        global $wp_customize;

        foreach ($this->sections as $section) {
            $section->register();
        }

        foreach ($this->panels as $panel) {
            $panel->register();
        }
    }

    public function renderCSS()
    {
        $output = '<style>';
        foreach ($this->sections as $section) {
            $output .= $section->renderCSS();
        }

        foreach ($this->panels as $panel) {
            $output .=$panel->renderCSS();
        }
        $output .= '</style>';

        return $output;
    }
}
