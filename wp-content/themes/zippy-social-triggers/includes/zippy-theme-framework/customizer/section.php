<?php

class ZTF_Customizer_Section
{
    public $id;
    public $name;
    public $title;
    public $description;
    public $priority;
    public $panel;
    public $controls = array();

    public function __construct($id, array $details)
    {
        $this->id           = $id;
        $this->title        = isset($details['name']) ? $details['name'] : '';
        $this->description  = isset($details['description']) ? $details['description'] : '';
        $this->priority     = isset($details['priority']) ? $details['priority'] : 110;
        $controls     = isset($details['controls']) ? $details['controls'] : array();

        foreach ($controls as $control_id => $control_details) {
            $this->addControl($control_id, $control_details);
        }
    }

    public function register()
    {
        global $wp_customize;

        $args = array(
            'priority'          => $this->priority,
            'capability'        => 'edit_theme_options',
            'theme_supports'    => '',
            'title'             => __($this->title, 'zippy_theme_framework'),
            'description'       => __($this->description, 'zippy_theme_framework')
        );

        if (isset($this->panel)) {
            $args['panel'] = $this->panel->getName();
        }

        $wp_customize->add_section($this->getSectionName(), $args);

        foreach ($this->controls as $control) {
            $control->register();
        }
    }

    public function setPanel(ZTF_Customizer_Panel $panel)
    {
        $this->panel = $panel;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name ? $this->name : $this->id;
    }

    public function addControl($id, $details)
    {
        $this->controls[$id] = new ZTF_Customizer_Control($id, $details);
        $this->controls[$id]->setSection($this);
        return $this->controls[$id];
    }

    public function renderCSS()
    {
        $output = '';
        
        foreach ($this->controls as $control) {
            $output .= $control->renderCSS();
        }

        return $output;
    }

    public function getRules()
    {
        $output = array();

        foreach ($this->controls as $control) {
            foreach ($control->getRules() as $selector => $rules) {
                $output[$selector] = isset($output[$selector]) ? $output[$selector] : array();

                foreach ($rules as $property => $value) {
                    $output[$selector][$property] = $value;
                }
            }
        }

        return $output;
    }

    public function getSectionName()
    {
        $name = '';

        if ($this->panel) {
            $name .= $this->panel->id . '_';
        }

        $name .= $this->id;

        return $name;
    }
}
