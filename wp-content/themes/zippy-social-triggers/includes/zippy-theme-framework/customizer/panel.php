<?php

class ZTF_Customizer_Panel
{
    public $id;
    public $title;
    public $description;
    public $priority;
    public $sections = array();
    public $customizer;

    public function __construct($id, array $details)
    {
        $this->id           = $id;
        $this->title        = isset($details['name']) ? $details['name'] : '';
        $this->description  = isset($details['description']) ? $details['description'] : '';
        $this->priority     = isset($details['priority']) ? $details['priority'] : 110;
        $this->sections     = isset($details['sections']) ? $details['sections'] : array();

        foreach ($this->sections as $section_id => $section_details) {
            $this->addSection($section_id, $section_details);
        }
    }

    public function setCustomizer($customizer)
    {
        $this->customizer = $customizer;
        return $this;
    }

    public function register()
    {
        global $wp_customize;

        if (!is_object($wp_customize)) {
            return;
        }

        $wp_customize->add_panel($this->getName(), array(
            'priority'          => $this->priority,
            'capability'        => 'edit_theme_options',
            'theme_supports'    => '',
            'title'             => __($this->title, 'zippy_theme_framework'),
            'description'       => __($this->description, 'zippy_theme_framework')
        ));

        foreach ($this->sections as $section) {
            $section->register();
        }
    }

    public function getName()
    {
        $name = '';

        if ($this->customizer) {
            $name .= $this->customizer->id . '_';
        }

        $name .= $this->id;

        return $name;
    }

    public function addSection($id, $details)
    {
        $this->sections[$id] = new ZTF_Customizer_Section($id, $details);
        $this->sections[$id]->setPanel($this);
        return $this->sections[$id];
    }

    public function renderCSS()
    {
        $output = '';
        foreach ($this->sections as $section) {
            $output .= $section->renderCSS();
        }

        return $output;
    }

    public function getRules()
    {
        $rules = array();

        foreach ($this->sections as $section) {
            $rules = array_merge($section->getRules(), $rules);
        }

        return $rules;
    }
}
