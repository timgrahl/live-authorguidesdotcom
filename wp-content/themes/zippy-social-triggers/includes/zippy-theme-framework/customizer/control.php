<?php

class ZTF_Customizer_Control
{
    public $section;
    public $type;
    public $default;
    public $id;
    public $label;
    public $description;
    public $choices = array();
    public $settings_filters = array();
    public $rules = array();

    protected $name;
    protected $setting_name;

    public function __construct($id, array $details)
    {
        $this->id           = $id;
        $this->label        = isset($details['name']) ? $details['name'] : '';
        $this->default      = isset($details['default']) ? $details['default'] : '';
        $this->description  = isset($details['description']) ? $details['description'] : '';
        $this->priority     = isset($details['priority']) ? $details['priority'] : 110;
        $this->choices      = isset($details['options']) ? $details['options'] : array();
        $this->type         = isset($details['type']) ? $details['type'] : 'text';

        if (isset($details['rules'])) {
            $this->addRules($details['rules']);
        }
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getDefault()
    {
        return $this->default;
    }

    public function setDefault($default)
    {
        $this->default = $default;
        return $this;
    }

    public function setChoices(array $choices)
    {
        $this->choices = $choices;
        return $this;
    }

    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    public function register()
    {
        global $wp_customize;

        $settings_args = array(
            'type'          => 'theme_mod',
            'capability'    => 'edit_theme_options',
            'default'       => $this->default
        );

        $control_args = array(
            'label'         => __($this->label, 'zippy_theme_framework'),
            'section'       => $this->section->getSectionName(),
            'settings'      => $this->getSettingName(),
            'priority'      => $this->priority,
            'description'   => __($this->description, 'zippy_theme_framework'),
            'type'          => $this->type
        );

        if (isset($this->choices)) {
            $control_args['choices'] = $this->choices;
        }

        $wp_customize->add_setting($this->getSettingName(), $settings_args);

        if ($this->type && $this->type == 'color') {
            $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, $this->getName(), $control_args));
        } elseif ($this->type && $this->type == 'image') {
            $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, $this->getName(), $control_args));
        } elseif ($this->type && ($this->type == 'file' || $this->type == 'upload')) {
            $wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize, $this->getName(), $control_args));
        } else {
            $wp_customize->add_control($this->getName(), $control_args);
        }
    }

    public function getName()
    {
        $name = '';

        if ($this->section) {
            $name .= $this->section->getSectionName() . '_';
        }

        $name .= $this->id;

        return $this->filter('control', 'name', $name);
    }

    public function getSettingName()
    {
        $name = '';

        if ($this->section) {
            $name .= $this->section->getSectionName() . '_';
        }

        $name .= $this->id;

        return $this->filter('setting', 'name', $name);
    }

    public function getSectionName()
    {
        $name = '';
        if ($this->section) {
            return $this->section->getSectionName();
        }

        return $this->filter('setting', 'name', $name);
    }

    public function filter($type, $field, $value)
    {
        switch ($type) {
            case 'setting':
                if ($this->section && !empty($this->section->settings_filters)) {
                    foreach ($this->section->settings_filters as $key => $search_replace) {
                        $value = str_replace($search_replace[0], $search_replace[1], $value);
                    }
                }
                break;
            default:
                break;
        }

        return $value;
    }

    public function addRules(array $rules_set)
    {
        foreach ($rules_set as $selector => $rules) {
            $this->rules[$selector] = array();
            foreach ($rules as $property => $value) {
                $this->rules[$selector][$property] = $value;
            }
        }

        return $this;
    }

    public function renderCSS()
    {
        $output = '';

        foreach ($this->rules as $selector => $rules) {
            $output .= $selector . '{' . "\n";
            foreach ($rules as $property => $value) {
                error_log(print_r($value, true));
                $needs_to_be_rendered = false;
                if ($value == '%value%') {
                    $default = $this->getDefault();
                    $stored = get_theme_mod($this->getSettingName(), $default);

                    $needs_to_be_rendered = strcasecmp($default, $stored) !== 0;
                    $value = $stored;
                }

                if ($needs_to_be_rendered) {
                    $output .= "\t" . $property . ': ' . $value .";\n";
                }
            }
            $output .= '}' . "\n";
        }

        return $output;
    }

    public function getRules()
    {
        $output = array();

        foreach ($this->rules as $selector => $rules) {
            $output[$selector] = isset($output[$selector]) ? $output[$selector] : array();

            foreach ($rules as $property => $value) {
                if (is_array($value)) {
                    if (strpos($value[0], '%value%') !== false) {
                        $default = $this->getDefault();
                        $val = get_theme_mod($this->getSettingName(), $default);
                        $value[0] = !$val ? '' : str_replace('%value%', $val, $value[0]);

                        if ($val != $default) {
                            $output[$selector][$property] = $value;
                        }
                        
                    }
                } else {
                    if (strpos($value, '%value%') !== false) {
                        $default = $this->getDefault();
                        $val = get_theme_mod($this->getSettingName(), $default);

                        $value = !$val || (strcasecmp($default, $val) == 0) ? '' : str_replace('%value%', $val, $value);

                        $output[$selector][$property] = $value;
                    } else {
                        $output[$selector][$property] = $value;
                    }
                }
            }
        }

        return $output;
    }
}
