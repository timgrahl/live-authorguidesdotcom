<?php

class ZTF_LandingPage
{
    public function __construct()
    {
        add_filter('body_class', array($this, 'bodyClass'));
    }

    public function bodyClass($classes)
    {
        if (is_page_template('templates/landing-page.php')) {
            $classes[] = 'zippy-landing-page';
        }

        return $classes;
    }
}
