<?php

class ZTF_Customizer
{
    public $id;
    public $panels = array();
    public $sections = array();

    public function __construct($panels = array())
    {
        $this->add($panels)        ;
    }

    public function addPanel($id, $details)
    {
        $this->panels[$id] = new ZTF_Customizer_Panel($id, $details);
        $this->panels[$id]->setCustomizer($this);
        return $this->panels[ $id ];
    }

    public function addSection($id, $title, $description = '', $priority = 100)
    {
        $this->sections[ $id ] = new ZTF_Customizer_Section($id, $title, $description, $priority);
        return $this->sections[ $id ];
    }

    public function register()
    {
        global $wp_customize;

        foreach ($this->sections as $section) {
            $section->register();
        }

        foreach ($this->panels as $panel) {
            $panel->register();
        }
    }

    public function add($panels)
    {
        foreach ($panels as $panel_id => $panel_details) {
            $this->addPanel($panel_id, $panel_details);
        }

        return $this;
    }

    public function renderCSS()
    {
        $zippy_framework = ZippyThemeFramework::instance();

        $css_rules = $this->getRules();

        $output = "\n\n" . '<style type="text/css" media="screen">' . "\n";
        foreach ($css_rules as $selector => $rules) {
            foreach ($rules as $key => $rule) {
                if (empty($rule)) {
                    unset($rules[$key]);
                }
            }

            if(empty($rules)) {
                unset($css_rules[$selector]);
                continue;
            }

            $output .= $selector . ' {' . "\n";
            foreach ($rules as $property => $value) {
                if (is_array($value)) {
                    $val    = $value[0];
                    $func   = $value[1];
                    $amt    = $value[2];

                    if ($val) {
                        $computed_value = call_user_func_array(array('ZTF_Utilities_Color', 'getHover'), array($val, (255 * $amt)));
                        $output .= "\t" . $property . ': ' . $computed_value .";\n";
                    }
                } else {
                    if ($value != '') {
                        if ($property == 'background-image') {
                            $output .= "\t" . $property . ': url("' . $value . '")' . ";\n";
                            $output .= "\t" .  'background-repeat:no-repeat;' . "\n";
                        } elseif($property == 'background-size') {
                            
                            if ($value == '%stretch_smart%') {
                                $url = isset($rules['background-image']) ? $rules['background-image'] : false;
                                $attachment_id = $zippy_framework->utilities->getAttachmentIdByURL($url);

                                if ($attachment_id) {
                                    $attachment = wp_get_attachment_image_src($attachment_id);
                                    $output .= $attachment[1] > $attachment[2] 
                                        ? "\t" . $property . ': auto 100%;' . "\n"
                                        : "\t" . $property . ': 100% auto;' . "\n";
                                }
                            } else {
                                $output .= "\t" . $property . ': ' . $value .";\n";    
                            }
                            
                        } else {
                            $output .= "\t" . $property . ': ' . $value .";\n";
                        }
                    }
                }
            }
            $output .= '}' . "\n\n";
        }
        $output .= '</style>' . "\n\n";

        return $output;
    }

    public function getRules()
    {
        $rules = array();

        foreach ($this->sections as $section) {
            $rules = array_merge($section->getRules(), $rules);
        }

        foreach ($this->panels as $panel) {
            $rules = array_merge($panel->getRules(), $rules);
        }

        return $rules;
    }

    private function lightenOrDarken($value, $amount)
    {
        return ZTF_Utilities_Color::getHover($value, (255 * $amount));
    }
}
