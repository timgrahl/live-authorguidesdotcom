<?php

class ZTF_Layout
{

    public $options;

    public function __construct()
    {
    }

    public function getOptions()
    {
        $zippy_framework = ZippyThemeFramework::instance();

        $defaults = $this->getDefaults();

        $post_id = $zippy_framework->utilities->getPostId();

        $options = (array) get_post_meta($post_id, 'zippy_template_options', true);

        if (isset($options['override']) && is_array($options['override'])) {
            foreach ($defaults as $key => $value) {
                $this->options[ $key ] = array_key_exists($key, $options) ? $options[$key] : $value;
            }
        } else {
            $this->options = $defaults;
        }

        if (isset($this->options[0])) {
            unset($this->options[0]);
        }
    }

    public function getDefaults()
    {
        global $post;

        if (is_front_page() || is_search()) {
            $post_id = get_option('page_on_front');
        } elseif (is_home()) {
            $post_id = get_option('page_for_posts');
        } elseif (is_404()) {
            $post_id = 0;
        } else {
            $post_id = $post->ID;
        }

        $post_type = get_post_type($post_id);

        $defaults = array(
            'layout' => get_theme_mod('layout_' . $post_type . '_layout', '0'),
            'menu'   => get_theme_mod('layout_' . $post_type . '_menu_visibility', '1'),
            'header' => get_theme_mod('layout_' . $post_type . '_header_visibility', '1'),
            'footer' => get_theme_mod('layout_' . $post_type . '_footer_visibility', '1')
        );

        return $defaults;
    }

    public function getContentContainerClasses($classes = '')
    {
        if (!is_array($classes)) {
            $classes = array_filter(explode(' ', $classes));
        }

        global $wp_query;

        switch($this->getLayout()) {
            case '2': // No sidebar
                $is_bbp_user_profile = isset($wp_query->bbp_is_single_user) && $wp_query->bbp_is_single_user;

                if (get_post_type() == 'forum' || get_post_type() == 'topic' || $is_bbp_user_profile) {
                    $classes[] = 'col-sm-12';
                } else {
                    $classes[] = 'col-md-9';
                    $classes[] = 'center-block';
                }
                break;
            case '0': // Right Sidebar
            case '1': // Left Sidebar
            default:
                $classes[] = 'col-md-8';
                break;
        }

        return implode(' ', $classes);
    }

    public function getSidebarContainerClasses($classes = '')
    {
        if (!is_array($classes)) {
            $classes = array_filter(explode(' ', $classes));
        }

        switch($this->getLayout()) {
            case '2': // No sidebar
                $classes[] = 'hide';
                break;
            case '0': // Right Sidebar
            case '1': // Left Sidebar
            default:
                $classes[] = 'col-md-4';
                break;
        }

        return implode(' ', $classes);

    }

    public function getLayout()
    {
        return apply_filters('zippy_framework_layout_type', $this->getOption('layout', '0'));
    }

    public function hasMenu()
    {
        if ($this->options === null) {
            $this->getOptions();
        }

        return $this->getOption('menu');
    }

    public function hasFooter()
    {
        if ($this->options === null) {
            $this->getOptions();
        }

        return $this->getOption('footer');
    }

    public function hasHeader()
    {
        return $this->getOption('header');
    }

    public function getOption($key, $default = 1)
    {
        if ($this->options === null) {
            $this->getOptions();
        }

        return isset($this->options[$key]) && $this->options[$key] != '' ? $this->options[$key] : $default;
    }
}
