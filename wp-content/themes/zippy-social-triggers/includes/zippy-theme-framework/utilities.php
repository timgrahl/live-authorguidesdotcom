<?php

class ZTF_Utilities
{
    protected $post_types;

    public function getAttachmentIdByURL($url)
    {
        // Split the $url into two parts with the wp-content directory as the separator
        $parsed_url  = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );

        // Get the host of the current site and the host of the $url, ignoring www
        $this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
        $file_host = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );

        // Return nothing if there aren't any $url parts or if the current host and $url host do not match
        if ( ! isset( $parsed_url[1] ) || empty( $parsed_url[1] ) || ( $this_host != $file_host ) ) {
            return;
        }

        global $wpdb;

        $attachment = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid LIKE %s", '%' . $parsed_url[1] . '%'));

        // Returns null if no attachment is found
        return $attachment;
    }
    
    public function getExcerpt($post_id, $length = 40)
    {
        global $wpdb;

        $excerpt = '';

        if (get_post_type($post_id) == 'course') {
            $public = is_array(get_post_meta($post_id, 'enable-public-details', true));

            if ($public) {
                $description = get_post_meta($post_id, 'description', true);
                $content = get_post_meta($post_id, 'zippy_directory_content', true);

                $excerpt = trim($description) == '' ? wp_trim_words($content, $length) : $description;
            } else {
                $excerpt = '<em>' . __('You do not have permission to access this course.', ZippySocialTriggers::TEXTDOMAIN) . '</em>';
            }
            
        } else {
            $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->posts WHERE ID = %s", $post_id));

            if ($item) {
                $excerpt = trim($item->post_excerpt) == '' ?  $excerpt = strip_shortcodes(wp_trim_words($item->post_content, $length)) : $item->post_excerpt;
            }
        }
        return $excerpt;
        
    }

    public static function removeFunctionFromHook($function_name, $hook)
    {
        global $wp_filter;

        foreach ($wp_filter as $action => $priorities) {
            if ($action == $hook) {
                foreach ($priorities as $priority => $functions) {
                    foreach ($functions as $name => $function) {
                        if (is_array($function['function'])) {
                            if (strpos($name, $function_name) !== false) {
                                unset( $wp_filter[ $action ][ $priority ][ $name ] );
                            }
                        }
                    }
                }
            }
        }
    }

    public function getPostTypes()
    {
        if ($this->post_types === null) {
            $post_types = get_post_types(array(
                'public'   => true,
                '_builtin' => false
            ));

            $this->post_types = array('post', 'page') + $post_types;
        }

        return $this->post_types;
    }

    public function hasFeaturedMedia($post_id = null)
    {
        if (!class_exists('Zippy')) {
            return false;
        }
        
        $zippy = Zippy::instance();

        $post_id = $post_id === null ? $this->getPostId() : $post_id;

        if ($post_id) {
            $featured_media = $zippy->utilities->getJsonMeta($post_id, 'featured_media', true);

            $image = isset($featured_media->image) ? $featured_media->image : '';
            $video = isset($featured_media->video) ? $featured_media->video : '';

            $type  = empty($video) ? 'image' : 'video';

            switch ($type) {
                case 'video':
                    return (bool) $video;
                    break;
                case 'image':
                default:
                    return has_post_thumbnail($post_id);
                    break;
            }
        }

        return false;
    }

    public function getPostId()
    {
        global $post;

        $post_id = false;

        if (is_front_page() || is_search()) {
            $post_id = get_option('page_on_front');
        } elseif (is_home()) {
            $post_id = get_option('page_for_posts');
        } elseif (is_404()) {
            $post_id = 0;
        } else {
            if (is_object($post)) {
                $post_id = $post->ID;
            }
        }

        return $post_id;
    }

    public function getAvatarUrl($email, $size = 75){
        
        $avatar = get_avatar($email, 75);

        preg_match("/src='(.*?)'/i", $avatar, $matches);

        return isset($matches[1]) ? $matches[1] : false;
    }

    public function getSearchResultPageUrl($page_num) {
        global $wp_rewrite, $wp_query;

        $search_term    = $wp_query->query['s'];

        if ($wp_rewrite->using_permalinks()) {
            $url = $page_num > 1 ? home_url('page/' . $page_num . '/') . '?s=' . $search_term : home_url() . '?s=' . $search_term;
        } else {
            $url = $page_num > 1 ? home_url() . '?s=' . $search_term . '&paged=' . $page_num : home_url() . '?s=' . $search_term;
        }

        return $url;
    }

    public function dashboardShowsEntryFeaturedImage() {
        return get_theme_mod('options_course_dashboard_show_featured_image', true);
    }

    public function dashboardShowsEntryExcerpt() {
        return get_theme_mod('options_course_dashboard_show_excerpt', true);
    }

    public function directoryShowsEntryFeaturedImage() {
        return get_theme_mod('options_course_directory_show_featured_image', true);
    }

    public function directoryShowsEntryExcerpt() {
        return get_theme_mod('options_course_directory_show_excerpt', true);
    }


}
