<?php

class ZTF_Utilities_Color
{
        /**
     * Change the brightness of the passed in color
     *
     * $diff should be negative to go darker, positive to go lighter and
     * is subtracted from the decimal (0-255) value of the color
     * 
     * @param string $hex color to be modified
     * @param string $diff amount to change the color
     * @source 
     * @return string hex color
     */
    public static function lighten($hex, $diff) {

        $rgb = str_split(trim($hex, '# '), 2);

        foreach ( $rgb as &$hex ) {

            $dec = hexdec($hex);
            $dec += $diff;
            
            $dec = max(0, min(255, $dec));

            $hex = str_pad(dechex($dec), 2, '0', STR_PAD_LEFT);

        }

        return '#'.implode($rgb);
        
    }

    public static function darken($hex, $diff) {

        $rgb = str_split(trim($hex, '# '), 2);

        foreach ($rgb as &$hex) {

            $dec = hexdec($hex);
            $dec -= $diff;
            
            $dec = max(0, min(255, $dec));

            $hex = str_pad(dechex($dec), 2, '0', STR_PAD_LEFT);

        }

        return '#'.implode($rgb);
        
    }

    public static function getBrightness($hex) {
    
        $rgb = str_split(trim($hex, '# '), 2);
        $sum = 0;

        foreach ($rgb as $hex) {
            $sum += hexdec($hex);
        }

        return $sum / 765;

    }

    public static function getHover($hex, $diff = 25)
    {
        return self::getBrightness($hex) < .5 ? self::lighten($hex, $diff) : self::darken($hex, $diff);
    }
}