<?php

class ZTF_Utilities_Course
{
    public static function getUnitEntries()
    {
        global $post;

        if (!class_exists('Zippy') || !is_object($post)) {
            return array();
        }

        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');

        $course  = $zippy->utilities->entry->getCourse($post->ID);

        switch ($post->post_type) {
            case 'unit':
                $unit    = $course->entries->getRecursive($post->ID);
                break;
            case 'lesson':
            default:
                $unit    = $zippy->utilities->entry->getEntryUnit($course, $post->ID);
                break;
        }

        $entries = $unit instanceof Zippy_Entry ? $unit->entries->all() : array();

        return $entries;
    }

    public static function getUnitEntriesHtml()
    {
        if (!class_exists('Zippy')) {
            return;
        }
        
        global $post;
        $entries = self::getUnitEntries();
        $output = '';

        $output .= '<div class="zippy-unit-entries">';

        foreach ($entries as $entry) {
            $classes = $entry->getId() == $post->ID ? 'zippy-unit-entry zippy-current-entry' : 'zippy-unit-entry';
            $output .=  '<div class="' . $classes . '">';

            $output .= $entry->isAvailable() ? '<a href="' . get_permalink($entry->getId()) . '">' : '<a href="' . get_permalink($entry->getId()) . '" class="disabled">';

            $output .= get_the_title($entry->getId());

            if (!$entry->isAvailable()) {
                $output .= '<span class="zippy-entry-available"><strong>' . __('Available', ZippySocialTriggers::TEXTDOMAIN) . ':</strong> ' . $entry->getAvailabilityMessage() . '</span>';
            }
            $output .=      '</a>' .
                        '</div>';
        }
        $output .= '</div>';

        return $output;
    }
}
