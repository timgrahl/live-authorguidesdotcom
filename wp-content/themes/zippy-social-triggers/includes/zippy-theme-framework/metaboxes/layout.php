<p><label><input type="checkbox" value="1" name="zippy_template_options[override][]" <?php if (is_array($layout['override'])) { echo 'checked';} ?>/> <?php _e('Override default layout settings?', ZippySocialTriggers::TEXTDOMAIN); ?></label></p>

<p>
<label><?php _e('Layout', ZippySocialTriggers::TEXTDOMAIN); ?></label>
<select name="zippy_template_options[layout]">

    <option value="0" <?php selected('0', $layout['layout'], true); ?>><?php _e('Right Sidebar', ZippySocialTriggers::TEXTDOMAIN); ?></option>
    <option value="1" <?php selected('1', $layout['layout'], true); ?>><?php _e('Left Sidebar', ZippySocialTriggers::TEXTDOMAIN); ?></option>
    <option value="2" <?php selected('2', $layout['layout'], true); ?>><?php _e('No Sidebars', ZippySocialTriggers::TEXTDOMAIN); ?></option>
</select>
</p>

<p>
<label><?php _e('Menu Visibility', ZippySocialTriggers::TEXTDOMAIN); ?></label>
<select name="zippy_template_options[menu]">
    <option value="1" <?php selected('1', $layout['menu'], true); ?>><?php _e('On', ZippySocialTriggers::TEXTDOMAIN); ?></option>
    <option value="0" <?php selected('0', $layout['menu'], true); ?>><?php _e('Off', ZippySocialTriggers::TEXTDOMAIN); ?></option>
</select>
</p>

<p>
<label><?php _e('Header Visibility', ZippySocialTriggers::TEXTDOMAIN); ?></label>
<select name="zippy_template_options[header]">
    <option value="1" <?php selected('1', $layout['header'], true); ?>><?php _e('On', ZippySocialTriggers::TEXTDOMAIN); ?></option>
    <option value="0" <?php selected('0', $layout['header'], true); ?>><?php _e('Off', ZippySocialTriggers::TEXTDOMAIN); ?></option>
</select>
</p>

<p>
<label><?php _e('Footer Visibility', ZippySocialTriggers::TEXTDOMAIN); ?></label>
<select name="zippy_template_options[footer]">
    <option value="1" <?php selected('1', $layout['footer'], true); ?>><?php _e('On', ZippySocialTriggers::TEXTDOMAIN); ?></option>
    <option value="0" <?php selected('0', $layout['footer'], true); ?>><?php _e('Off', ZippySocialTriggers::TEXTDOMAIN); ?></option>
</select>
</p>