<?php

class ZippySocialTriggers
{

    private static $instance;

    const VERSION = '1.1.2';
    const TEXTDOMAIN = 'zippy-social-triggers';

    private $path;
    private $url;
    private $override;
    private $filters;

    private static $config;
    public static $layout;

    private function __construct()
    {
        $this->override     = new ZippySocialTriggers_PluginOverride;
        $this->filters      = new ZippySocialTriggers_Filters;

        $this->url  = get_template_directory_uri();
        $this->path  = get_template_directory();
        
        self::$config = new ZippySocialTriggers_Config;

        // Activation
        add_action('init', array($this, 'install'));
        add_action('init', array($this, 'activate'));

        // Deactivation
        add_action('switch_theme', array($this, 'deactivate'));

        // Assets
        add_action('wp_enqueue_scripts', array($this, 'scripts'));
        add_action('wp_enqueue_scripts', array($this, 'styles'));
        add_action('wp_head', array($this, 'fonts'));

        // UI
        add_action('init', array($this, 'widgets'));
        add_action('init', array($this, 'menus'));
        
        add_action('zippy_entry_content_top', array($this, 'downloads'));

        add_action('zippy_social_triggers_entry_header_bottom', array($this, 'lessonCompleteButton'));

        // Favicon
        add_action('wp_head', array($this, 'favicon'));

        // Customizer CSS
        add_action('wp_head', array($this, 'customizerCss'));


    }

    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function scripts()
    {
        wp_enqueue_script('jquery');

        wp_enqueue_script('bootstrap-js', $this->url . '/assets/js/bootstrap-sass/bootstrap.min.js', array('jquery'), self::VERSION, true);
        
        wp_enqueue_script('fitvids', $this->url . '/assets/js/FitVids/jquery.fitvids.js', array('jquery'), self::VERSION, true);
        wp_enqueue_script('perfect-scrollbar', $this->url . '/assets/js/perfect-scrollbar/perfect-scrollbar.js', array('jquery'), self::VERSION, true);
        wp_enqueue_script('zippy-social-triggers', $this->url . '/assets/js/zippy-social-triggers.js', array('jquery', 'fitvids', 'perfect-scrollbar'), self::VERSION, true);

        wp_localize_script(
            'zippy-social-triggers',
            'ZippySocialTriggers',
            array(
                'complete_button_text' => __('Mark Lesson Complete', ZippySocialTriggers::TEXTDOMAIN),
                'uncomplete_button_text' => __('Lesson Completed!', ZippySocialTriggers::TEXTDOMAIN),
                'show_comments_text' => __('Show Comments', ZippySocialTriggers::TEXTDOMAIN),
                'hide_comments_text' => __('Hide Comments', ZippySocialTriggers::TEXTDOMAIN),
            )
        );
    }

    public function styles()
    {
        // Load our main stylesheet.
        wp_enqueue_style('zippy-social-triggers-styles', get_stylesheet_uri(), array(), self::VERSION, 'screen');
    }

    public function fonts()
    {
        echo '<link href="//fonts.googleapis.com/css?family=Open+Sans:800,600,400,300" rel="stylesheet" type="text/css">';
    }

    public function widgets()
    {
        $sidebars       = new ZTF_Widget('Primary Sidebar', 'primary-sidebar');
        $footer_one     = new ZTF_Widget('Footer Left', 'footer-left');
        $footer_two     = new ZTF_Widget('Footer Middle', 'footer-middle');
        $footer_three   = new ZTF_Widget('Footer Right', 'footer-right');
    }

    public function menus()
    {
        register_nav_menu('top', 'Top Menu');
    }

    public static function getColor($key)
    {
        if (self::$instance === null) {
            self::instance();
        }

        return self::$config->getColor($key);
    }

    public function downloads()
    {
        global $post;

        if (!is_object($post)) {
            return;
        }

        if (!class_exists('Zippy')) {
            return;
        }

        $zippy = Zippy::instance();
        $downloads = $zippy->utilities->getJsonMeta($post->ID, 'downloads', true);

        $output = '';

        if ($downloads && is_array($downloads) && count($downloads)) {
            $output .= '<div class="zippy-entry-core-downloads zippy-downloads">';
            $output .= '<ul>';

            foreach ($downloads as $download) {
                $output .= '<li><a href="' . $zippy->utilities->entry->getSecureDownloadUrl($download->uid) . '" class="zippy-download zippy-download-' . $download->type . '">' .
                    __('Download', ZippySocialTriggers::TEXTDOMAIN) . ' ' . $download->title .
                '</a></li>';
            }

            $output .= '</ul>';
            $output .= '</div>';
        }
        
        echo $output;
    }

    public function comment($comment, $args, $depth)
    {
        $zippy_framework        = ZippyThemeFramework::instance();

        $has_children           = empty( $args['has_children'] ) ? false : true;
        $comment_classes        = $has_children ? get_comment_class('parent') : get_comment_class();
        $comment_classes        = implode(' ', $comment_classes);
        $comment_id             = 'comment-' . get_comment_ID();
        $author                 = $comment->comment_author;
        $author_url             = $comment->comment_author_url;
        $avatar_size            = isset($args['avatar_size']) ? $args['avatar_size'] : false;
        $avatar                 = $zippy_framework->utilities->getAvatarUrl($comment->comment_author_email);
        $depth                  = $depth;
        $time                   = get_comment_time();
        $time_string            = human_time_diff(get_comment_time('U'), current_time('timestamp')) . ' ago';
        $content                = get_comment_text();
        $add_below              = $comment_id;
        $reply_link             = get_comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'])));
        $awaiting_moderation    = $comment->comment_approved == '0';
        $permalink              = get_comment_link();

        include( $this->path . '/partials/comments/comment-single.php' );
    }

    public function commentClose()
    {
        echo '</section></article>';
    }

    /**
     * Do these actions during installation, but not on activation
     * This will only happen once EVER, because there is no uninstall
     * hook available for themes.
     *
     * @since 1.0.0
     * @return void
     */
    public function install()
    {
        $installed = (bool) get_option('zippy_social_triggers_install');

        if (!$installed) {
            // Install actions

            // Add marker so it doesn't run in future
            add_option('zippy_social_triggers_install', time());
        }
    }

    /**
     * Do these actions during activation
     *
     * @since 1.0.0
     * @return void
     */
    public function activate()
    {
        $activate = (bool) get_option('zippy_social_triggers_activation');

        if (!$activate) {
            // Customize the Layout of Courses, Units and Lessons
            $post_types = array('course', 'unit', 'lesson', 'quiz', 'course_order', 'forum', 'topic', 'reply');

            foreach ($post_types as $post_type) {
                set_theme_mod('layout_' . $post_type . '_layout', '2');   // No sidebars
                set_theme_mod('layout_' . $post_type . '_menu', '1');     // On
                set_theme_mod('layout_' . $post_type . '_header', '1');   // On
                set_theme_mod('layout_' . $post_type . '_footer', '1');   // On
            }

            // Customize the layout of some core pages
            if (class_exists('Zippy')) {
                $zippy = Zippy::instance();
                
                $overrides = array('override' => array('1'), 'layout' => '2', 'menu' => '1', 'header' => '1', 'footer' => '1');

                $pages = array(
                    $zippy->core_pages->get('dashboard'),
                    $zippy->core_pages->get('account'),
                    $zippy->core_pages->get('edit_account'),
                    $zippy->core_pages->get('order_history'),
                    $zippy->core_pages->get('thank_you'),
                    $zippy->core_pages->get('forgot_password'),
                    $zippy->core_pages->get('reset_password'),
                    $zippy->core_pages->get('register')
                );

                foreach ($pages as $page_id) {
                    if ($page_id) {
                        update_post_meta($page_id, 'zippy_template_options', $overrides);
                    }
                }
            }

            // Add marker so it doesn't run in future
            add_option('zippy_social_triggers_activation', time());
        }
    }

    /**
     * Perform these actions during deactivation
     *
     * @since 1.0.0
     * @return void
     */
    public function deactivate()
    {
        delete_option('zippy_social_triggers_activation');
    }

    public function customizerCss()
    {
        $zippy_framework = ZippyThemeFramework::instance();

        echo $zippy_framework->customizer->renderCSS();
    }

    public function getHeaderLogo()
    {
        $logo = get_theme_mod('logos_header_image', '');
        $output = $logo != '' ? '<img src="' . $logo . '" alt="' . get_bloginfo('name') . '" />' : get_bloginfo('name');
        
        return $output;
    }

    public function getLoginPageLogo()
    {
        $logo = get_theme_mod('logos_login_page_image', '') == '' ? get_theme_mod('logos_header_image', '') : get_theme_mod('logos_login_page_image', '');

        $output = $logo != '' ? '<img src="' . $logo . '" alt="' . get_bloginfo('name') . '" />' : get_bloginfo('name');
        
        return $output;
    }

    public function getMenuLogo()
    {
        $logo = get_theme_mod('logos_menu_image', '');
        $output = $logo != '' ? '<img src="' . $logo . '" alt="' . get_bloginfo('name') . '" />' : get_bloginfo('name');
        
        return $output;
    }


    public function hasUnitNav($post_id)
    {
        if (!class_exists('Zippy')) {
            return false;
        }

        $zippy = Zippy::instance();

        $post_type  = get_post_type($post_id);
        
        $is_unit_or_lesson = $post_type == 'unit' || $post_type == 'lesson';
        if (!$is_unit_or_lesson) {
            return false;
        }

        $is_unit_nav_enabled = get_theme_mod('options_course_nav_unit_nav', '1');
        if (!$is_unit_nav_enabled) {
            return false;
        }

        switch ($post_type) {
            case 'unit':
                $course  = $zippy->utilities->entry->getCourse($post_id);
                $unit    = $course->entries->getRecursive($post_id);
                
                $needs_nav = $unit instanceof Zippy_Entry && $unit->entries->count();
                break;
            default:
                $course  = $zippy->utilities->entry->getCourse($post_id);
                $unit    = $zippy->utilities->entry->getEntryUnit($course, $post_id);

                $needs_nav = $unit instanceof Zippy_Entry && $unit->entries->count();
                break;
        }

        return $needs_nav;
    }

    public function lessonCompleteButton()
    {
        if (!class_exists('Zippy')) {
            return false;
        }
        
        global $post;

        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');

        if (is_single() && is_object($post)) {
            $completed = $student !== null ? $student->isCompleted($post->ID) : false;

            echo '<div class="zippy-complete-entry-region" data-id="' . $post->ID . '" data-status="' . (int) $completed . '"></div>';

            // echo '<div class="zippy-entry-complete zippy-complete-entry" data-id="' . $post->ID . '" data-status="' . (int) $completed . '"></div>';
        }
    }

    public function getFavicon()
    {
        return get_theme_mod('options_favicon_file', false);
    }

    public function favicon()
    {
        $favicon = $this->getFavicon();
        if ($favicon) {
            echo '<link rel="icon" type="image/ico" href="' . $favicon . '"/>';
        }
    }

    public function getInfoURL()
    {
        return get_theme_mod('options_landing_page_info_url', false);
    }

    public function hasInfoURL()
    {
        return (bool) $this->getInfoURL();
    }
}
