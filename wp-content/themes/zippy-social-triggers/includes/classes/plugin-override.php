<?php

class ZippySocialTriggers_PluginOverride {

    /**
     * Set up the default overrides for a plugin in our framework
     */
    public function __construct()
    {
        add_action('init', array($this, 'contentFilters'), 999);
        add_action('init', array($this, 'headerFilters'), 999);
        add_action('init', array($this, 'footerFilters'), 999);
    }

    public function contentFilters()
    {
        ZTF_Utilities::removeFunctionFromHook('entryDownloads', 'the_content');
    }

    public function headerFilters()
    {
        ZTF_Utilities::removeFunctionFromHook('includeStyles', 'wp_head');
    }

    public function footerFilters()
    {
        ZTF_Utilities::removeFunctionFromHook('zippy_attribution', 'wp_footer');
    }
}
