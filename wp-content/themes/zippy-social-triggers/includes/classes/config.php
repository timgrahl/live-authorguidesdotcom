<?php

class ZippySocialTriggers_Config
{
    protected $framework;

    public function __construct()
    {
        $this->framework = ZippyThemeFramework::instance();

        add_theme_support('html5');
        add_theme_support('post-thumbnails');
        add_theme_support('widgets');
        add_theme_support('menus');
        
        // Let WordPress manage the document title.
        add_theme_support( 'title-tag' );

        $this->framework->customizer->add($this->getPanels());
    }
    
    public static $colors = array(
        'blue'              => '#4444EE',
        'dark_blue'         => '#081B24',
        'orange'            => '#FF9622',
        'white'             => '#FFFFFF',
        'black'             => '#000000',
        'eggshell'          => '#F2F2F2',
        'lightest_gray'     => '#EEEEEE',
        'lighter_gray'      => '#CCCCCC',
        'light_gray'        => '#AAAAAA',
        'gray'              => '#888888',
        'dark_gray'         => '#555555',
        'darker_gray'       => '#333333',
        'darkest_gray'      => '#111111'
    );

    public static function getColor($key)
    {
        return isset(self::$colors[$key]) ? self::$colors[$key] : self::$colors['gray'];
    }

    public function getPanels()
    {

        $panels = array(
            'logos' => array(
                'name' => __('Logos', ZippySocialTriggers::TEXTDOMAIN),
                'priority' => 110,
                'sections' => array(
                    'header' => array(
                        'name' => __('Header', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'image' => array(
                                'name'      => 'Header Logo',
                                'type'      => 'image',
                                'rules' => array(
                                    'zippy-landing-page-registration' => array(
                                        'color' => '%value%'
                                    )
                                ),
                                'description' => __('This logo should be no wider than 600px wide, and will scale automatically on mobile.', ZippySocialTriggers::TEXTDOMAIN)
                            )
                        )
                    ),
                    'login_page' => array(
                        'name' => __('Login Page', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'image' => array(
                                'name'      => 'Login Page Logo',
                                'type'      => 'image',
                                'description' => __('This logo should be no wider than 600px wide, and will scale automatically on mobile. If there is a Header logo and no Login page logo, the header logo will be used on the login page.', ZippySocialTriggers::TEXTDOMAIN)
                            )
                        )
                    ),
                    'menu' => array(
                        'name' => __('Menu', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'image' => array(
                                'name'      => 'Menu Logo',
                                'type'      => 'image',
                                'description' => __('This logo should be no taller than 50px, and no wider than 400px.', ZippySocialTriggers::TEXTDOMAIN)
                            )
                        )
                    )
                )
            ),
            'colors' => array(
                'name' => __('Colors & Backgrounds', ZippySocialTriggers::TEXTDOMAIN),
                'priority' => 111,
                'sections' => array(
                    'general' => array(
                        'name' => __('General', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'bg' => array(
                                'name' => __('Background Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('white'),
                                'rules' => array(
                                    'body' => array(
                                        'background' => '%value%'
                                    )
                                )
                            ),
                            'title_bg' => array(
                                'name' => __('Title Background', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('lightest_gray'),
                                'rules' => array(
                                    '.entry-header' => array(
                                        'background' => '%value%'
                                    )
                                )
                            ),
                            'title_color' => array(
                                'name' => __('Title Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('orange'),
                                'rules' => array(
                                    '.entry-title' => array(
                                        'color' => '%value%'
                                    )
                                )
                            ),
                            'text' => array(
                                'name' => __('Text Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('darkest_gray'),
                                'rules' => array(
                                    'body' => array(
                                        'color' => '%value%'
                                    )
                                )
                            ),
                            'link' => array(
                                'name' => __('Link Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('orange'),
                                'rules' => array(
                                    'a, .pagination > li a, .zippy-course-entries > .zippy-lesson .zippy-lesson-header a.zippy-lesson-title, .zippy-unit-header a.zippy-unit-title, .zippy-lesson-header a.zippy-lesson-title, .zippy-courses-directory .zippy-course .zippy-course-title a' => array(
                                        'color' => '%value%'
                                    ),
                                    'a:hover, a:focus, a:active, .pagination > li a:hover, .zippy-course-entries > .zippy-lesson .zippy-lesson-header a.zippy-lesson-title:hover, .zippy-unit-header a.zippy-unit-title:hover, .zippy-lesson-header a.zippy-lesson-title:hover, .zippy-courses-directory .zippy-course .zippy-course-title a:hover, .zippy-courses-directory .zippy-course .zippy-course-title a:focus, .zippy-courses-directory .zippy-course .zippy-course-title a:active' => array(
                                        'color' => array( '%value%', 'lightenOrDarken', '.15')
                                    ),
                                    '.zippy-courses-directory .zippy-course-title a' => array(
                                        'color' => '%value%'
                                    ),
                                    '.pagination > li > a:focus, .pagination > li.active a, .pagination > li.active a:hover, .pagination > li.active a:focus, .pagination > li.active a:active, .pagination > li a:hover' => array(
                                        'background' => '%value%',
                                        'color' => self::getColor('white')
                                    )
                                )
                            )
                        )
                    ),
                    'landing_page' => array(
                        'name' => __('Landing Page', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'headline_bg' => array(
                                'name'      => 'Headline Background',
                                'type'      => 'color',
                                'default'   => '#FFFFFF',
                                'rules' => array(
                                    '.zippy-landing-page .page-header' => array(
                                        'background' => '%value%',
                                        'border-color' => '%value%'
                                    )
                                )
                            ),
                            'headline_color' => array(
                                'name'      => 'Color',
                                'type'      => 'color',
                                'default'   => '#000000',
                                'rules' => array(
                                    '.zippy-landing-page .page-title' => array(
                                        'color' => '%value%'
                                    )
                                )
                            ),
                            'form_bg' => array(
                                'name'      => 'Form Background',
                                'type'      => 'color',
                                'default'   => self::getColor('eggshell'),
                                'rules' => array(
                                    '.zippy-landing-page-registration' => array(
                                        'background' => '%value%'
                                    )
                                )
                            ),
                            'form_color' => array(
                                'name'      => 'Form Text Color',
                                'type'      => 'color',
                                'default'   => self::getColor('black'),
                                'rules' => array(
                                    '.zippy-landing-page-registration h3, .zippy-landing-page-registration p' => array(
                                        'color' => '%value%'
                                    )
                                )
                            ),
                        )
                    ),
                    'login' => array(
                        'name' => __('Login Page', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'bg' => array(
                                'name' => __('Background Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('darkBlue'),
                                'rules' => array(
                                    '.zippy-login-template .container-fluid' => array(
                                        'background-color' => '%value%'
                                    )
                                )
                            ),
                            'bg_image' => array(
                                'name' => __('Image', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'image',
                                'default' => '',
                                'rules' => array(
                                    '.zippy-login-template .container-fluid' => array(
                                        'background-image'  => '%value%',
                                        'background-size'   => '%stretch_smart%'
                                    )
                                ),
                                'description' => __('This image should be very large to appear crisp - at least 1280px by 720px, up to 1920px by 1090px.', ZippySocialTriggers::TEXTDOMAIN)
                            ),
                            'text' => array(
                                'name' => __('Text Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('white'),
                                'rules' => array(
                                    '.zippy-login-template .site-title, .zippy-login-template .signin-prompt, .zippy-login-template .not-a-member h3, .zippy-login-template .not-a-member p' => array(
                                        'color' => '%value%'
                                    )
                                )
                            ),
                            'link' => array(
                                'name' => __('Link Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('white'),
                                'rules' => array(
                                    '.zippy-login-template .not-a-member a, .zippy-login-template .forgot-password-link' => array(
                                        'color' => '%value%'
                                    ),
                                    '.zippy-login-template .not-a-member a:hover, .zippy-login-template .forgot-password-link:hover' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.1')
                                    )
                                )
                            ),
                        )
                    ),
                    'header' => array(
                        'name' => __('Header', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'image' => array(
                                'name' => __('Image', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'image',
                                'default' => '',
                                'rules' => array(
                                    '.site-header' => array(
                                        'background' => 'url("%value%") center center',
                                        'background-size' => '100% auto'
                                    )
                                ),
                                'description' => __('This image should be very wide to scale smoothly across the header background, around 1280px by 120px, up to 1920px by 180px.', ZippySocialTriggers::TEXTDOMAIN)
                            ),
                            'bg' => array(
                                'name' => __('Background Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('dark_blue'),
                                'rules' => array(
                                    '.site-header' => array(
                                        'background-color' => '%value%'
                                    )
                                )
                            ),
                            'text' => array(
                                'name' => __('Text Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('white'),
                                'rules' => array(
                                    '.site-header .site-title' => array(
                                        'color' => '%value%'
                                    )
                                )
                            )
                        )
                    ),
                    'alerts' => array(
                        'name' => __('Alerts', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'success_bg' => array(
                                'name' => __('Success Background', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => '#DFF0D8',
                                'rules' => array(
                                    '.alert.alert-success, .zippy-alert.zippy-success, .zippy-entry-completed' => array(
                                        'background' => '%value%',
                                        'border-color' => array('%value%', 'lightenOrDarken', '.1')
                                    )
                                )
                            ),
                            'success_text' => array(
                                'name' => __('Success Text', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => '#3c763d',
                                'rules' => array(
                                    '.alert.alert-success, .zippy-alert.zippy-success, .zippy-entry-completed' => array(
                                        'color' => '%value%'
                                    ),
                                    '.alert.alert-success a, .zippy-alert.zippy-success a, .zippy-entry-completed a' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.2')
                                    ),
                                    '.alert.alert-success a:hover, .alert.alert-success a:active, .alert.alert-success a:focus, .zippy-entry-completed a:focus, .zippy-entry-completed a:active, .zippy-entry-completed a:hover, .zippy-alert.zippy-success a:hover, .zippy-alert.zippy-success a:active, .zippy-alert.zippy-success a:focus' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.1')
                                    )
                                )
                            ),
                            'info_bg' => array(
                                'name' => __('Info Background', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => '#FCF8E3',
                                'rules' => array(
                                    '.alert.alert-warning, .zippy-alert.zippy-warning' => array(
                                        'background' => '%value%',
                                        'border-color' => array('%value%', 'lightenOrDarken', '.1')
                                    )
                                )
                            ),
                            'info_text' => array(
                                'name' => __('Info Text', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => '#8A6D3B',
                                'rules' => array(
                                    '.alert.alert-warning, .zippy-alert.zippy-warning' => array(
                                        'color' => '%value%'
                                    ),
                                    '.alert.alert-warning a, .zippy-alert.zippy-warning a' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.2')
                                    ),
                                    '.alert.alert-warning a:hover, .alert.alert-warning a:active, .alert.alert-warning a:focus, .zippy-alert.zippy-warning a:focus, .zippy-alert.zippy-warning a:active, .zippy-alert.zippy-warning a:hover' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.1')
                                    )
                                )
                            ),
                            'error_bg' => array(
                                'name' => __('Error Background', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => '#F2DEDE',
                                'rules' => array(
                                    '.alert.alert-error, .alert.alert-danger, .zippy-alert.zippy-error' => array(
                                        'background' => '%value%',
                                        'border-color' => array('%value%', 'lightenOrDarken', '.1')
                                    )
                                )
                            ),
                            'error_text' => array(
                                'name' => __('Error Text', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => '#A94442',
                                'rules' => array(
                                    '.alert.alert-error, .alert.alert-danger, .zippy-alert.zippy-error' => array(
                                        'color' => '%value%'
                                    ),
                                    '.alert.alert-error a, .alert.alert-danger a, .zippy-alert.zippy-error a' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.2')
                                    ),
                                    '.alert.alert-error a:hover, .alert.alert-danger a:hover, .alert.alert-error a:active, .alert.alert-danger a:active, .alert.alert-error a:focus, .alert.alert-danger a:focus, .zippy-alert.zippy-error a:hover, .zippy-alert.zippy-error a:focus, , .zippy-alert.zippy-error a:active' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.1')
                                    )
                                )
                            ),
                        )
                    ),
                    'menu' => array(
                        'name' => __('Menu', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'bg' => array(
                                'name' => __('Background Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('black'),
                                'rules' => array(
                                    '#primary-nav, #primary-nav .menu-item:hover .sub-menu, #primary-nav .menu-item:hover .sub-menu .menu-item a, .navbar-inverse .navbar-toggle' => array(
                                        'background' => '%value%',
                                        'border-color' => '%value%'
                                    ),
                                    '.navbar-inverse .navbar-toggle:hover, .navbar-inverse .navbar-toggle:focus' => array(
                                        'background' => array('%value%', 'lightenOrDarken', '.1')
                                    )
                                )
                            ),
                            'text' => array(
                                'name' => __('Text Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('lightest_gray'),
                                'rules' => array(
                                    '#primary-nav' => array(
                                        'color' => '%value%'   
                                    )
                                )
                            ),
                            'link' => array(
                                'name' => __('Link Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('lightest_gray'),
                                'rules' => array(
                                    '.navbar-inverse .navbar-nav > li > a, #primary-nav .menu-item .sub-menu .menu-item a' => array(
                                        'color' => '%value%'
                                    ),
                                    '.navbar-inverse .navbar-nav > li > a:hover, .navbar-inverse .navbar-nav > li > a:focus, .navbar-inverse .navbar-nav > li > a:active, #primary-nav .menu-item .sub-menu .menu-item a:hover, #primary-nav .menu-item .sub-menu .menu-item a:active, #primary-nav .menu-item .sub-menu .menu-item a:focus' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.1')
                                    ),
                                    '#primary-nav .navbar-brand' => array(
                                        'color' => '%value%'
                                    ),
                                    '#primary-nav .navbar-inverse .navbar-brand:hover, #primary-nav .navbar-inverse .navbar-brand:focus, #primary-nav .navbar-inverse .navbar-brand:active' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.1')
                                    ),
                                    '.navbar-inverse .navbar-toggle' => array(
                                        'border-color' => '%value%'   
                                    ),
                                    '.navbar-inverse .navbar-toggle .icon-bar' => array(
                                        'background' => '%value%'   
                                    ),
                                    '.navbar-inverse .navbar-toggle:hover' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.1')
                                    ),
                                    '.navbar-inverse .navbar-toggle:hover .icon-bar' => array(
                                        'background' => array('%value%', 'lightenOrDarken', '.1')
                                    )
                                )
                            )
                        )
                    ),
                    'breadcrumbs' => array(
                        'name' => __('Breadcrumbs', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'bg' => array(
                                'name' => __('Background Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('darkest_gray'),
                                'rules' => array(
                                    '.course-nav .navbar-inverse, .forum-nav .navbar-inverse' => array(
                                        'background' => '%value%',
                                        'border-color' => '%value%'
                                    )
                                )
                            ),
                            'text' => array(
                                'name' => __('Text Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('lighter_gray'),
                                'rules' => array(
                                    '.course-nav .breadcrumb > li, .forum-nav .breadcrumb > li' => array(
                                        'color' => '%value%'
                                    )
                                )
                            ),
                            'link' => array(
                                'name' => __('Link Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('lighter_gray'),
                                'rules' => array(
                                    '.course-nav .breadcrumb > li a, .forum-nav .breadcrumb > li a' => array(
                                        'color' => '%value%'
                                    ),
                                    '.course-nav .breadcrumb > li a:hover, .forum-nav .breadcrumb > li a:hover' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.15')
                                    )
                                )
                            )
                        )
                    ),
                    'unit_nav' => array(
                        'name' => __('Entry Navigation', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'bg' => array(
                                'name' => __('Active Background', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('black'),
                                'rules' => array(
                                    '.zippy-social-triggers-unit-nav .zippy-unit-entry a' => array(
                                        'background' => '%value%'
                                    ),
                                    '.zippy-social-triggers-unit-nav .zippy-unit-entry a:hover' => array(
                                        'background' => array('%value%', 'lightenOrDarken', '.15')
                                    )
                                )
                            ),
                            'active_bg' => array(
                                'name' => __('Active Background', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('orange'),
                                'rules' => array(
                                    '.zippy-social-triggers-unit-nav .zippy-unit-entry.zippy-current-entry a' => array(
                                        'background' => '%value%'
                                    ),
                                    '.zippy-social-triggers-unit-nav .zippy-unit-entry.zippy-current-entry a:hover' => array(
                                        'background' => array('%value%', 'lightenOrDarken', '.15')
                                    )
                                )
                            )
                        )
                    ),
                    'button' => array(
                        'name' => __('Buttons', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'primary_bg' => array(
                                'name' => __('Primary Button Background', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('orange'),
                                'rules' => array(
                                    '.btn-primary, .zippy-button.zippy-button-primary, .zippy-button-primary, .zippy-button.zippy-complete-lesson, .comment-toggle' => array(
                                        'background' => '%value%',
                                        'border-color' => '%value%'
                                    ),
                                    '.btn-primary:hover, .zippy-button.zippy-button-primary:hover, .zippy-button-primary:hover,  .btn-primary:focus, .zippy-button.zippy-button-primary:focus, .zippy-button-primary:focus, .btn-primary:active, .zippy-button.zippy-button-primary:active, .zippy-button-primary:active, .zippy-button.zippy-complete-lesson:hover, .zippy-button.zippy-complete-lesson:active, .zippy-button.zippy-complete-lesson:focus,  .comment-toggle:hover,  .comment-toggle:focus, .comment-toggle:active' => array(
                                        'background' => array('%value%', 'lightenOrDarken', '.15'),
                                        'border-color' => array('%value%', 'lightenOrDarken', '.15')
                                    )
                                )
                            ),
                            'primary_text' => array(
                                'name' => __('Primary Button Text', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('white'),
                                'rules' => array(
                                    '.btn-primary, .zippy-button.zippy-button-primary, .zippy-button-primary, .zippy-button.zippy-complete-lesson' => array(
                                        'color' => '%value%'
                                    ),
                                    '.btn-primary:hover, .zippy-button.zippy-button-primary:hover, .zippy-button-primary:hover, .btn-primary:focus, .zippy-button.zippy-button-primary:focus, .zippy-button-primary:focus, .btn-primary:active, .zippy-button.zippy-button-primary:active, .zippy-button-primary:active, .zippy-button.zippy-complete-lesson:hover, .zippy-button.zippy-complete-lesson:active, .zippy-button.zippy-complete-lesson:focus' => array(
                                        'color' => '%value%'
                                    )
                                )
                            ),
                            'secondary_bg' => array(
                                'name' => __('Secondary Button Background', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('lighter_gray'),
                                'rules' => array(
                                    '.zippy-button, .nav-previous a, .nav-next a, .zippy-quiz-review, .zippy-previous, .zippy-next, .entry-navigation a' => array(
                                        'background' => '%value%',
                                        'border-color' => '%value%'
                                    ),
                                    '.zippy-button:hover, .nav-previous a:hover, .nav-next a:hover, .zippy-button:focus, .nav-previous a:focus, .nav-next a:focus, .zippy-button:active, .nav-previous a:active, .nav-next a:active, .zippy-quiz-review:active, .zippy-quiz-review:hover, .zippy-quiz-review:focus, .zippy-previous:hover, .zippy-next:hover, .zippy-previous:focus, .zippy-next:focus, .zippy-previous:active, .zippy-next:active, .entry-navigation a:hover,  .entry-navigation a:active, .entry-navigation a:focus' => array(
                                        'background' => array('%value%', 'lightenOrDarken', '.1'),
                                        'border-color' => array('%value%', 'lightenOrDarken', '.1')
                                    )
                                )
                            ),
                            'secondary_text' => array(
                                'name' => __('Secondary Button Text', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('gray'),
                                'rules' => array(
                                    '.zippy-button, .nav-previous a, .nav-next a, .zippy-quiz-review, .zippy-previous, .zippy-next, .entry-navigation a, .zippy-previous, .zippy-next, .entry-navigation a' => array(
                                        'color' => '%value%'
                                    ),
                                    '.zippy-button:hover, .nav-previous a:hover, .nav-next a:hover, .zippy-button:focus, .nav-previous a:focus, .nav-next a:focus, .zippy-button:active, .nav-previous a:active, .nav-next a:active, .zippy-quiz-review:active, .zippy-quiz-review:hover, .zippy-quiz-review:focus, .zippy-previous:hover, .zippy-next:hover, .zippy-previous:focus, .zippy-next:focus, .zippy-previous:active, .zippy-next:active, .entry-navigation a:hover,  .entry-navigation a:active, .entry-navigation a:focus' => array(
                                        'color' => '%value%'
                                    )
                                )
                            )
                        )
                    ),
                    'sidebar' => array(
                        'name' => __('Sidebar', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'heading' => array(
                                'name' => __('Heading Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('black'),
                                'rules' => array(
                                    '.sidebar .widget-title' => array(
                                        'color' => '%value%'
                                    )
                                )
                            ),
                            'text' => array(
                                'name' => __('Text Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('darker_gray'),
                                'rules' => array(
                                    '.sidebar, .sidebar p, .sidebar li' => array(
                                        'color' => '%value%'
                                    )
                                )
                            ),
                            'link' => array(
                                'name' => __('Link Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('orange'),
                                'rules' => array(
                                    '.sidebar a' => array(
                                        'color' => '%value%'
                                    ),
                                    '.sidebar a:hover, .sidebar a:focus, .sidebar a:active' => array(
                                        'color' => array('%value%', 'lightenOrDarken', '.1')
                                    )
                                )
                            )
                        )
                    ),
                    'footer' => array(
                        'name' => __('Footer', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'bg' => array(
                                'name' => __('Background Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('dark_blue'),
                                'rules' => array(
                                    '.site-footer' => array(
                                        'background' => '%value%'
                                    )
                                )
                            ),
                            'text' => array(
                                'name' => __('Text Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('white'),
                                'rules' => array(
                                    '.site-footer' => array(
                                        'color' => '%value%'
                                    )
                                )
                            ),
                            'link' => array(
                                'name' => __('Link Color', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'color',
                                'default' => self::getColor('white'),
                                'rules' => array(
                                    '.site-footer a' => array(
                                        'color' => '%value%'
                                    )
                                )
                            )
                        )
                    )
                )
            ),
            'options' => array(
                'name' => __('Theme Options', ZippySocialTriggers::TEXTDOMAIN),
                'priority' => 112,
                'sections' => array(
                    /*
                    'course_dashboard' => array(
                        'name' => __('Course Dashboard', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'show_excerpt' => array(
                                'name' => __('Show Unit & Lesson Excerpts?', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'select',
                                'options' => array(
                                    '1' => 'Yes',
                                    '0' => 'No'
                                )
                            ),
                            'show_featured_image' => array(
                                'name' => __('Show Unit & Lesson Featured Images?', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'select',
                                'options' => array(
                                    '1' => 'Yes',
                                    '0' => 'No'
                                )
                            )
                        )
                    ),
                    'course_directory' => array(
                        'name' => __('Course Directory', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'show_excerpt' => array(
                                'name' => __('Show Unit & Lesson Excerpts?', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'select',
                                'options' => array(
                                    '1' => 'Yes',
                                    '0' => 'No'
                                )
                            ),
                            'show_featured_image' => array(
                                'name' => __('Show Unit & Lesson Featured Images?', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'select',
                                'options' => array(
                                    '1' => 'Yes',
                                    '0' => 'No'
                                )
                            )
                        )
                    ),
                    'courses' => array(
                        'name' => __('Courses', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'show_unit_lesson_excerpt' => array(
                                'name' => __('Show Unit & Lesson Excerpts?', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'select',
                                'options' => array(
                                    '1' => 'Yes',
                                    '0' => 'No'
                                ),
                                'description' => __('Should excerpts be visible for units and lessons on the Course home page?', ZippySocialTriggers::TEXTDOMAIN)
                            )
                        )
                    ),
                    */
                    'landing_page' => array(
                        'name' => __('Landing Page', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'info_url' => array(
                                'name' => __('Info URL', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'url',
                                'default' => '',
                                'description' => __('This URL will be used on the Login Page template to redirect people to a page with more information if they would like to become a member. No link will be displayed if this field is left empty.', ZippySocialTriggers::TEXTDOMAIN)
                            ),
                        )
                    ),
                    'favicon' => array(
                        'name' => __('Favicon', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'file' => array(
                                'name' => __('File', ZippySocialTriggers::TEXTDOMAIN),
                                'description' => __('This file must be a .ico file to function properly.', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'upload',
                                'default' => ''
                            )
                        )
                    ),
                    /*
                    'social' => array(
                        'name' => __('Social Settings', ZippySocialTriggers::TEXTDOMAIN),
                        'description' => __('Fill in the links below for any resources you would like to share.  They will appear in the header of your site. These are site specific, not course specific.', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'facebook' => array(
                                'name' => __('Facebook Page URL', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'url'
                            ),
                            'twitter' => array(
                                'name' => __('Twitter Profile URL', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'url'
                            ),
                            'support' => array(
                                'name' => __('Support Channel URL', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'url',
                                'description' => __('Link to where people can access support.', ZippySocialTriggers::TEXTDOMAIN)
                            ),
                        )
                    ),
                    */
                    'comments' => array(
                        'name' => __('Comments', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'title' => array(
                                'name' => __('Title', ZippySocialTriggers::TEXTDOMAIN),
                                'description' => __('This title will appear above the comments on all post types.', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'text'
                            )
                            /*
                            'show_on_hierarchical' => array(
                                'name' => __('Show comments closed notification on hierarchical post types?', ZippySocialTriggers::TEXTDOMAIN),
                                'description' => __('This will also appear on any post type that is hierarchical, such as pages.', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'select',
                                'options' => array(
                                    '1' => 'Yes',
                                    '0' => 'No'
                                )
                            ),
                            'show_on_non_hierarchical' => array(
                                'name' => __('Show comments closed notification on non-hierarchical post types?', ZippySocialTriggers::TEXTDOMAIN),
                                'description' => __('This will also appear on any post type that is not hierarchical, such as blog posts.', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'select',
                                'options' => array(
                                    '1' => 'Yes',
                                    '0' => 'No'
                                )
                            )
                            */
                        )
                    ),
                    'course_nav' => array(
                        'name' => __('Course Navigation', ZippySocialTriggers::TEXTDOMAIN),
                        'controls' => array(
                            'breadcrumbs' => array(
                                'name' => __('Show Breadcrumbs', ZippySocialTriggers::TEXTDOMAIN),
                                'description' => __('Display breadcrumbs beneath the site header when viewing any part of a Course.', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'select',
                                'options' => array(
                                    '1' => 'Yes',
                                    '0' => 'No'
                                )
                            ),
                            'unit_nav' => array(
                                'name' => __('Show Unit Navigation', ZippySocialTriggers::TEXTDOMAIN),
                                'description' => __('Unit Navigation shows on Units and Lessons next to featured media.', ZippySocialTriggers::TEXTDOMAIN),
                                'type' => 'select',
                                'options' => array(
                                    '1' => 'Yes',
                                    '0' => 'No'
                                )
                            )
                        )
                    )

                )
            )
        );

        return $panels;
    }
}
