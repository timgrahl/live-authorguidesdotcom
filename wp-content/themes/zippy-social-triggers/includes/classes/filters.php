<?php

class ZippySocialTriggers_Filters {

    public function __construct()
    {
        add_filter('zippy_form_fields', array($this, 'loginForm'), 10, 2);

        add_filter('zippy_filter_course_entry_html', array($this, 'courseEntryHtml'), 10, 2);
        add_filter('zippy_filter_course_entries_unit_html', array($this, 'courseEntriesUnitHtml'), 10, 2);
        add_filter('zippy_course_entry_list_heading', array($this, 'courseEntriesTitle'), 9, 1);

        // bbPress
        add_filter('bbp_get_forum_pagination_links', array($this, 'forumPagination'), 10, 1);
        add_filter('bbp_get_topic_pagination_links', array($this, 'topicPagination'), 10, 1);

        // Framework
        add_filter('zippy_framework_layout_type', array($this, 'removeUserProfileSidebars'), 10, 1);
        add_filter('zippy_framework_layout_type', array($this, 'searchAndHomePage'), 10, 1);
    }
        
    public function loginForm($fields, $form_id)
    {
        if ($form_id == 'login') {
            foreach ($fields->all() as $field) {
                $field->setLabelVisibility(false);
            }
        }

        return $fields;
    }

    public function courseEntriesTitle($title)
    {
        return '<div class="col-xs-12">' . __('Units &amp; Lessons', ZippySocialTriggers::TEXTDOMAIN) . '</div>';
    }


    public function courseEntryHtml($html, Zippy_Entry $entry)
    {
        if (!class_exists('Zippy')) {
            return $html;
        }

        $zippy = Zippy::instance();
        $student = $zippy->cache->get('student');

        $settings               = get_option('zippy_customizer_course_options', array());
        $show_excerpt           = isset($settings['show_excerpt']) ? $settings['show_excerpt'] == 1 : true;
        $show_featured_media    = isset($settings['show_featured_media']) ? $settings['show_featured_media'] == 1 : true;

        $complete   = $student->isCompleted($entry->getId()) ? ' zippy-entry-complete ' : '';
        $post_type  = $entry->getPostType();
        $excerpt    = $entry->getExcerpt();

        $col = $show_featured_media && has_post_thumbnail($entry->getId()) ? '9' : '12';

        $classes = array('zippy-entry', 'zippy-item', 'zippy-' . $post_type, $complete);

        $output = '<div class="' . implode(' ', $classes) . '">';
            
        if ($show_featured_media && has_post_thumbnail($entry->getId())) {
            $output .= '<div class="col-sm-3 zippy-entry-thumbnail">';
            $thumbnail_id = get_post_thumbnail_id($entry->getId());
            $image = wp_get_attachment_image_src($thumbnail_id, 'medium');
            
            if ($entry->isAvailable()) {
                $output .= '<a href="' . get_permalink($entry->getId()) . '"><img src="' . $image[0] . '" /></a>';
            } else {
                $output .= '<img src="' . $image[0] . '" />';
            }
            $output .= '</div>';
        }

        $output .= '<div class="col-sm-' . $col .'">';
        $output .= '<div class="zippy-entry-header">';
        $output .= '<h4 class="zippy-entry-title">';
        if ($entry->isAvailable()) {
            $output .= '<a href="' . get_permalink($entry->getId()) . '">' . get_the_title($entry->getId()) . '</a>';
        } else {
            $output .= get_the_title($entry->getId());
            $output .= '<span class="zippy-entry-available"><strong>' . __('Available', ZippySocialTriggers::TEXTDOMAIN) . ':</strong> ' . $entry->getAvailabilityMessage() . '</span>';
        }
        $output .= '</h4>';
        $output .= '</div>';
        

        if ($show_excerpt && $excerpt != '') {
            $output .= '<div class="zippy-entry-description">';
                $output .= wpautop($excerpt);
            $output .= '</div>';
        }

        $output .= '</div>';
        $output .= '</div>';

        return $output;
    }

    public function courseEntriesUnitHtml($html, Zippy_Entry $unit)
    {
        $output = '<div class="zippy-unit">';
        $output .= '<div class="col-xs-12">';

        $output .= '<div class="zippy-unit-header">';
        $output .= '<div class="col-xs-12">';
        $output .= '<h3 class="zippy-unit-title">';
        
        if ($unit->isAvailable()) {
            $output .= '<a href="' . get_permalink($unit->getId()) . '" >' . get_the_title($unit->getId()) . '</a>';
        } else {
            $output .= get_the_title($unit->getId());
        }

        $output .= '<span class="zippy-unit-entry-count">' . $unit->entries->count() . ' entries</span>';

        if ($unit->entries->count()) {
            $output .= '<span class="zippy-unit-entries-toggle">+</span>';
        }
        $output .= '</h3>';
        $output .= '</div>';
        $output .= '</div>';
      
        $output .= '<div class="zippy-unit-entries">';
        $output .= '<div class="col-xs-12">';
        foreach ($unit->entries->all() as $entry) {
            $view = new ZippyCourses_Entry_View($entry);
            $output .= $view->render();
        }
        $output .= '</div>';
        $output .= '</div>';

        $output .= '</div>';
        $output .= '</div>';

        return $output;
    }

    public function forumPagination($links)
    {

        global $wp_rewrite, $paged, $wp_query;

        $bbp = bbpress();

        $forum_id = bbp_get_forum_id();

        $max_pages = $bbp->topic_query->max_num_pages;
        $current_page = $paged <= $max_pages ? ($paged ? $paged : 1) : $max_pages;

        $base_url = $this->getBaseUrl($forum_id);
        $prev_url = $current_page <= '1' ? $base_url : $base_url . ($current_page - 1);
        $next_url = $current_page >= $max_pages ? $base_url . $max_pages : $base_url . ($current_page + 1);

        $output = '<nav>';
            $output .= '<ul class="pagination">';
                $output .= '<li' . ($current_page == '1' ? ' class="disabled"' : '' ). '><a href="#" aria-label="' . __('Previous', ZippySocialTriggers::TEXTDOMAIN) . '"><span aria-hidden="true">&laquo;</span></a></li>';
                for ($i=1; $i <= $max_pages ; $i++) { 
                    // If pretty permalinks are enabled, make our pagination pretty
                    $url = $base_url . $i;

                    $output .= '<li' . ($i == $current_page ? ' class="active"' : '' ). '><a href="' . $url . '">' . $i . '</a></li>';
                }
                $output .= '<li' . ($current_page == $max_pages ? ' class="disabled"' : '') . '><a href="#" aria-label="' . __('Next', ZippySocialTriggers::TEXTDOMAIN)  . '"><span aria-hidden="true">&raquo;</span></a></li>';
            $output .= '</ul>';
        $output .= '</nav>';

        return $output;
    }

    public function topicPagination($links)
    {

        global $wp_rewrite, $paged, $wp_query;

        $bbp = bbpress();

        $topic_id = bbp_get_topic_id();

        $max_pages = $bbp->reply_query->max_num_pages;
        $current_page = $paged <= $max_pages ? ($paged ? $paged : 1) : $max_pages;

        $base_url = $this->getBaseUrl($topic_id);
        $prev_url = $current_page <= '1' ? $base_url : $base_url . ($current_page - 1);
        $next_url = $current_page >= $max_pages ? $base_url . $max_pages : $base_url . ($current_page + 1);

        $output = '<nav>';
        $output .= '<ul class="pagination">';
        $output .= '<li' . ($current_page == '1' ? ' class="disabled"' : '' ). '><a href="' . $prev_url . '" aria-label="' . __('Previous', ZippySocialTriggers::TEXTDOMAIN) . '"><span aria-hidden="true">&laquo;</span></a></li>';
        for ($i=1; $i <= $max_pages; $i++) {
            // If pretty permalinks are enabled, make our pagination pretty
            $url = $base_url . $i;

            $output .= '<li' . ($i == $current_page ? ' class="active"' : '' ). '><a href="' . $url . '">' . $i . '</a></li>';
        }
        $output .= '<li' . ($current_page == $max_pages ? ' class="disabled"' : '') . '><a href="' . $next_url . '" aria-label="' . __('Next', ZippySocialTriggers::TEXTDOMAIN) . '"><span aria-hidden="true">&raquo;</span></a></li>';
        $output .= '</ul>';
        $output .= '</nav>';

        return $output;
    }

    public function removeUserProfileSidebars($layout)
    {
        global $wp_query;

        return isset($wp_query->bbp_is_single_user) && $wp_query->bbp_is_single_user ? '2' : $layout;
    }

    public function searchAndHomePage($layout)
    {
        $zippy_framework = ZippyThemeFramework::instance();
        $post_id = $zippy_framework->utilities->getPostId();

        return $layout;
    }

    private function getBaseUrl($item_id)
    {
        global $wp_rewrite, $paged, $wp_query;


        if ($item_id) {
            if ($wp_rewrite->using_permalinks()) {
                $url = trailingslashit(get_permalink($item_id)) . user_trailingslashit($wp_rewrite->pagination_base);
            } else {
                $url = add_query_arg('paged', '', get_permalink($item_id));
            }
        } else {
            // If pretty permalinks are enabled, make our pagination pretty
            if ( $wp_rewrite->using_permalinks() ) {

                // User's topics
                if ( bbp_is_single_user_topics() ) {
                    $base = bbp_get_user_topics_created_url( bbp_get_displayed_user_id() );

                // User's favorites
                } elseif ( bbp_is_favorites() ) {
                    $base = bbp_get_favorites_permalink( bbp_get_displayed_user_id() );

                // User's subscriptions
                } elseif ( bbp_is_subscriptions() ) {
                    $base = bbp_get_subscriptions_permalink( bbp_get_displayed_user_id() );

                // Root profile page
                } elseif ( bbp_is_single_user() ) {
                    $base = bbp_get_user_profile_url( bbp_get_displayed_user_id() );

                // View
                } elseif ( bbp_is_single_view() ) {
                    $base = bbp_get_view_url();

                // Topic tag
                } elseif ( bbp_is_topic_tag() ) {
                    $base = bbp_get_topic_tag_link();

                // Page or single post
                } elseif ( is_page() || is_single() ) {
                    $base = get_permalink();

                // Forum archive
                } elseif ( bbp_is_forum_archive() ) {
                    $base = bbp_get_forums_url();

                // Topic archive
                } elseif ( bbp_is_topic_archive() ) {
                    $base = bbp_get_topics_url();

                // Default
                } else {
                    $base = '';
                }

                // Use pagination base
                $base = trailingslashit( $base ) . user_trailingslashit( $wp_rewrite->pagination_base );
                $url = $base;

            // Unpretty pagination
            } else {
                $url = add_query_arg( 'paged', '' );
            }
        }
        
        return $url;
    }

    public function bbpUserProfileTitles($title) {

        if (is_main_query()) {

            // User's topics
            if ( bbp_is_single_user_topics() ) {
                $title = sprintf(__('%s\'s Topics', ZippySocialTriggers::TEXTDOMAIN), bbp_get_displayed_user_field( 'user_nicename' ));

            // User's favorites
            } elseif ( bbp_is_favorites() ) {
                $title = sprintf(__('%s\'s Favorites', ZippySocialTriggers::TEXTDOMAIN), bbp_get_displayed_user_field( 'user_nicename' ));

            // User's subscriptions
            } elseif ( bbp_is_single_user_replies() ) {
                $title = sprintf(__('%s\'s Replies', ZippySocialTriggers::TEXTDOMAIN), bbp_get_displayed_user_field( 'user_nicename' ));
            // User's subscriptions
            } elseif ( bbp_is_subscriptions() ) {
                $title = sprintf(__('%s\'s Subscriptions', ZippySocialTriggers::TEXTDOMAIN), bbp_get_displayed_user_field( 'user_nicename' ));
            
            // User's subscriptions
            } elseif ( bbp_is_single_user_profile() ) {
                $title = sprintf(__('%s\'s Profile', ZippySocialTriggers::TEXTDOMAIN), bbp_get_displayed_user_field( 'user_nicename' ));
            // Default
            } else {
                $title = $title;
            }

        }

        return $title;       
    }
}
