<?php

require_once('includes/autoload.php');
require_once('includes/updater.php');

// Set up theme object
function zippy_social_triggers_setup()
{
    ZippySocialTriggers::instance();
}
add_action('init', 'zippy_social_triggers_setup', 9);

function zippy_social_triggers_lang_setup(){
    load_theme_textdomain(ZippySocialTriggers::TEXTDOMAIN, get_template_directory() . '/lang');
}
add_action('after_setup_theme', 'zippy_social_triggers_lang_setup');

function zippy_social_triggers_course_menu()
{
    if(is_search() || is_home()) {
        return;
    }
    
    global $post;

    if(!class_exists('Zippy') || !is_object($post)) {
        return;
    }

    $zippy = Zippy::instance();
    $student = $zippy->cache->get('student');

    $course_id = $post->post_type == 'course' ? $post->ID : $zippy->utilities->entry->getCourseId($post->ID);

    if ($student !== null && $course_id) {
        $course =   $student->getCourse($course_id);

        if ($course == null) {
            return;
        }

        $has_breadcrumbs =  get_theme_mod('options_course_nav_breadcrumbs', '1');

        if ($has_breadcrumbs):
        ?>
        <div class="course-nav row">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#course-navbar-collapse">Quick Nav</button> -->
                        <ol class="breadcrumb">
                                <li><a class="navbar-brand" href="<?php echo get_permalink($course_id); ?>"><?php echo get_the_title($course_id); ?></a></li>

                            <?php if ($post->post_type == 'unit') : ?>
                                <li class="active"><a class="" href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></li>
                            <?php endif; ?>

                            <?php if ($post->post_type != 'unit' && $post->post_type != 'course'): ?>
                                <?php $unit_id = $zippy->utilities->entry->getEntryUnitId($course, $post->ID); ?>

                                <?php if ($unit_id && $unit_id !== $post->ID) : ?>
                                    <li><a class="" href="<?php echo get_permalink($unit_id); ?>"><?php echo get_the_title($unit_id); ?></a></li>
                                <?php endif; ?>

                                <li class="active"><?php echo $post->post_title; ?></li>
                            <?php endif; ?>
                        </ol>
                    </div>
                </div>
            </nav>
        </div>
        <?php
        endif;
    }
}
add_action('zippy_header_after', 'zippy_social_triggers_course_menu');

function zippy_social_triggers_bbpress_breadcrumbs() {

    global $post;

    if (function_exists('is_bbpress') && is_bbpress()) {
        
        $before = '<div class="row forum-nav">
            <nav class="navbar navbar-inverse">
                <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <ol class="breadcrumb">';
                       
        $after = '</ol>
            </nav>
        </div>';

        bbp_breadcrumb(array(
            // HTML
            'before'          => $before,
            'after'           => $after,

            // Separator
            'sep'             => ' ',
            'pad_sep'         => 0,
            'sep_before'      => '',
            'sep_after'       => '',

            // Crumbs
            'crumb_before'    => '<li>',
            'crumb_after'     => '</li>',

            // Home
            'include_home'    => true,
            'home_text'       => 'Home',

            // Forum root
            'include_root'    => true,
            'root_text'       => 'Forums',

            // Current
            'include_current' => true,

            'current_before'  => '',
            'current_after'   => ''
        
        ));
    }
}
add_action('zippy_header_after', 'zippy_social_triggers_bbpress_breadcrumbs');
add_action('zippy_filter_include_customizer_styles', '__return_false');
