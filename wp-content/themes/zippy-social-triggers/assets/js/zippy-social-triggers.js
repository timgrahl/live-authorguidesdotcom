+function ($) {
    'use strict';

    var ZippySocialTriggers = {
        login: {
            setHeightToMatchWindow: function() {
                if ($('body').hasClass('zippy-login-template')) {
                    if($(document).height() > $('body').height()) {
                        var dh = $(document).height();
                        var fh = $('.site-footer').outerHeight();
                        var diff = (dh - fh - 200) + 'px';

                        $('.login-form-container').outerHeight(diff);
                    }
                }
            }            
        },
        comments: {
            toggle: function(e) {
                e.preventDefault();

                var $toggle = $(e.currentTarget);

                $('.comments').slideToggle();

                if($toggle.text() == ZippySocialTriggers.show_comments_text) {
                    $toggle.text(ZippySocialTriggers.hide_comments_text);
                } else {
                    $toggle.text(ZippySocialTriggers.show_comments_text);
                }
            }
        },
        lessons: {
            handleDisabledLinks: function(e) {
                e.preventDefault();
            },
            complete: function(e) {
                e.preventDefault();

                var $button = $(e.currentTarget);
                var data = {
                    action: 'complete_lesson',
                    lesson_id: $( e.currentTarget ).data('id'),
                    user_id: ZippyCourses.student_id
                };

                $.ajax({
                    url: ZippyCourses.ajaxurl,
                    data: data,
                    type: 'POST',
                    success: function(response) {
                        $('.zippy-lesson-complete-toggle').text(ZippySocialTriggers.uncomplete_button_text).removeClass('zippy-complete-lesson').addClass('zippy-uncomplete-lesson');
                    }
                });
            },
            uncomplete: function(e) {
                e.preventDefault();

                var $button = $(e.currentTarget);
                var data = {
                    action: 'uncomplete_lesson',
                    lesson_id: $( e.currentTarget ).data('id'),
                    user_id: ZippyCourses.student_id
                };

                $.ajax({
                    url: ZippyCourses.ajaxurl,
                    data: data,
                    type: 'POST',
                    success: function(response) {
                        // $('zippy-social-triggers-lesson-complete-button, zippy-social-triggers-lesson-uncomplete-button').text('Mark Lesson Complete').addClass('zippy-social-triggers-lesson-complete-button').removeClass('zippy-social-triggers-lesson-uncomplete-button').blur();
                        $('.zippy-lesson-complete-toggle').text(ZippySocialTriggers.complete_button_text).addClass('zippy-complete-lesson').removeClass('zippy-uncomplete-lesson');
                    }
                });
            }
        },
        units: {
            toggle: function(e) {
                var $el = $(e.currentTarget);
                var $parent = $el.parents('.zippy-unit');

                $parent.find('.zippy-unit-entries').slideToggle();
                $el.text(($el.text() == '–' ? '+' : '–'));
            }
        },
        unit_nav: {
            init: function() {
                if($('.zippy-social-triggers-unit-nav').length < 1) {
                    return;
                }

                ZippySocialTriggers.unit_nav.refresh();

            },
            onResize: function() {
                ZippySocialTriggers.unit_nav.init();
            },
            destroy: function() {
                $('.zippy-social-triggers-unit-nav').perfectScrollbar('destroy');
                $('.zippy-social-triggers-unit-nav').height('auto');
            },
            refresh: function() {
                if($(window).width() < 768) {
                    ZippySocialTriggers.unit_nav.destroy();
                } else {
                    ZippySocialTriggers.unit_nav.setup();
                }
            },
            setup: function() {
                var h = $('.entry-featured-media div').first().height();
                $('.zippy-social-triggers-unit-nav').height(h);

                $('.zippy-social-triggers-unit-nav').perfectScrollbar();
            }
        },
        video: {
            fit: function() {
                var selectors = [
                    "iframe[src^='http://player.vimeo.com']", 
                    "iframe[src^='http://www.youtube.com']", 
                    "iframe[src^='https://www.youtube.com']", 
                    "iframe[src^='http://www.kickstarter.com']",
                    "iframe[src^='http://www.funnyordie.com']",
                    "iframe[src^='http://media.mtvnservices.com']",
                    "iframe[src^='http://trailers.apple.com']",
                    "iframe[src^='http://www.brightcove.com']",
                    "iframe[src^='http://blip.tv']",
                    "iframe[src^='http://break.com']",
                    "iframe[src^='http://www.traileraddict.com']",
                    "iframe[src^='http://d.yimg.com']",
                    "iframe[src^='http://movies.yahoo.com']",
                    "iframe[src^='http://www.dailymotion.com']",
                    "iframe[src^='http://s.mcstatic.com']",
                    "iframe[src^='http://fast.wistia.net']",
                    "iframe[src^='//fast.wistia.net']",
                    "iframe[src^='http://www.twitch.tv']",
                    "iframe[src^='https://fast.wistia.net']",
                    "object", 
                    "embed",
                    "video"
                ];

                $('.entry-featured-media, .entry-content').fitVids({customSelector: selectors, ignore: '.fitvidsignore, .fitvidsignore video, .fitvidsignore object, .fitvidsignore iframe, .fitvidsignore embed, iframe.fitvidsignore'});
                ZippySocialTriggers.unit_nav.onResize();
            }
        },
        header: {
            verticalCenterNav: function() {
                if($(window).width() > 768) {
                    var header_height       = $('#top-nav').height();
                    var menu_item_height    = $('#top-nav .menu-item').height();

                    var diff = header_height - menu_item_height;
                    var margin_top = (diff / 2) + 'px';

                    $('#top-nav .menu-item').css({'margin-top': margin_top});
                } else {
                    $('#element').attr('style', function(i, style) {
                        return style.replace(/margin-top[^;]+;?/g, '');
                    });
                }
            }
        },
        landing_page: {
            validate: function() {
                if($('.zippy-landing-page-registration').length > 0) {
                    $( '.zippy-landing-page-registration .landing-page-registration-form' ).validate({
                        rules: {
                            user_email: {
                                required: true,
                                email: true,
                                remote: {
                                    url: ZippyCourses.ajaxurl,
                                    type: "post",
                                    data: {
                                        action: 'validate_registration_email'                       
                                    }, 
                                    complete: function( response ) {}
                                }
                            },
                            user_login: {
                                required: true,
                                minlength: 3,
                                remote: {
                                    url: ZippyCourses.ajaxurl,
                                    type: "post",
                                    data: {
                                        action: 'validate_registration_username'                        
                                    }, 
                                    complete: function( response ) {}
                                }
                            },
                            user_pass: {
                                required: true,
                                minlength: 7,
                            }
                        },
                        submitHandler: function( form ) {
                            form.submit();
                        }                    
                    });
                }
            }
        },
        init: function() {
            // Register our events
                
            // Login Page
            $(window).load(this.login.setHeightToMatchWindow);

            // Comments
            $('.comment-toggle').on('click', ZippySocialTriggers.comments.toggle);

            // Disabled links
            $('a.disabled').on('click', ZippySocialTriggers.lessons.handleDisabledLinks);

            // Unit Nav
            $(window).load(this.unit_nav.init);

            // Unit entries expansion 
            $('.zippy-unit-entries-toggle').on('click', ZippySocialTriggers.units.toggle);
    
            // Lessons
            // $('.site-body').on('click', '.zippy-social-triggers-lesson-complete-button', ZippySocialTriggers.lessons.complete);
            // $('.site-body').on('click', '.zippy-complete-lesson', ZippySocialTriggers.lessons.complete);
            // $('.site-body').on('click', '.zippy-social-triggers-lesson-uncomplete-button', ZippySocialTriggers.lessons.uncomplete);
            // $('.site-body').on('click', '.zippy-uncomplete-lesson', ZippySocialTriggers.lessons.uncomplete);
            

            // Media
            this.video.fit();

            // Header
            $(window).load(ZippySocialTriggers.header.verticalCenterNav);

            // Resizing
            $(window).on('resize', ZippySocialTriggers.header.verticalCenterNav);
            $(window).on('resize', ZippySocialTriggers.unit_nav.onResize);
            $(window).on('resize', ZippySocialTriggers.login.setHeightToMatchWindow);

            // Landing Page stuff
            this.landing_page.validate();
        }

    };

    ZippySocialTriggers.init();

}(jQuery);

