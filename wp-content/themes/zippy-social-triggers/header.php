<?php 
$zippy_framework    = ZippyThemeFramework::instance(); 
$zippy_social_triggers      = ZippySocialTriggers::instance(); 
?><!doctype html>
<!--[if IE 7]>
<html class="no-js ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="no-js ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="<?php bloginfo('charset'); ?>">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>

    <?php if ($zippy_framework->layout->hasMenu()) : ?>
        <?php get_template_part('partials/navigation/nav', 'primary'); ?>
    <?php endif; ?>

    <main class="container-fluid">
        <!--[if lt IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <?php if ($zippy_framework->layout->hasHeader()) : ?>
        <?php do_action('zippy_header_before'); ?>
        <header class="row site-header">
            <div class="col-sm-12">
                <?php do_action('zippy_header_top'); ?>
                <h1 class="site-title"><?php echo $zippy_social_triggers->getHeaderLogo(); ?></h1>
                <?php do_action('zippy_header_bottom'); ?>
            </div>
        </header>
        <?php do_action('zippy_header_after');
        endif;
        ?>

        <div class="row site-body">
            <div class="col-sm-12">
                <div class="container">
                    <div class="row">