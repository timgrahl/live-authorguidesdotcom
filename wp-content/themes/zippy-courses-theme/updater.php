<?php

// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
// IMPORTANT: change the name of this constant to something unique to prevent conflicts with other plugins using this system
if( !defined( 'ZC_EDD_STORE_URL' ) ) {
    define( 'ZC_EDD_STORE_URL', 'https://zippycourses.com' ); 
}

// the name of your product. This is the title of your product in EDD and should match the download title in EDD exactly
define( 'ZC_EDD_CORE_THEME', 'Zippy Courses Theme' ); // IMPORTANT: change the name of this constant to something unique to prevent conflicts with other plugins using this system

if( !defined( 'ZC_THEME_VERSION' ) ) {
    define( 'ZC_THEME_VERSION', '0.9.14' );
}

if( !class_exists( 'EDD_SL_Theme_Updater' ) ) {
    // load our custom theme updater
    require_once( dirname( __FILE__ ) . '/vendors/EDD/EDD_SL_Theme_Updater.php' );
}

function zippy_courses_core_theme_updater()
{

    if(!is_admin() || ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) || ( defined( 'DOING_CRON' ) && DOING_CRON ))
        return;

    $edd_updater = new EDD_SL_Theme_Updater( array( 
            'remote_api_url' => ZC_EDD_STORE_URL,                   // our store URL that is running EDD
            'version'        => ZC_THEME_VERSION,       // the current theme version we are running
            'license'        => 'DEFAULT-ZC-THEME-LICENSE',         // the license key (used get_option above to retrieve from DB)
            'item_name'      => ZC_EDD_CORE_THEME,                    // the name of this theme
            'author'         => 'Zippy Courses',                    // the author's name
            'url'            => home_url()
        )
    );
}
add_action( 'admin_init', 'zippy_courses_core_theme_updater' );

function zippy_courses_core_theme_activation() {
    
    if(!is_admin() || ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) || ( defined( 'DOING_CRON' ) && DOING_CRON ))
        return;

    // We only want to try to activate
    $now = new DateTime();
    $last_attempted = get_option( 'zippy_courses_core_theme_last_activation_attempt' );

    $diff = $now->format('U') - $last_attempted;

    if ( $diff > 3600 ) { // Only do it once an hour

        $status = get_option('zippy_courses_core_theme_license_status', 'invalid');

        if ($status == 'invalid') {
            $license = 'DEFAULT-ZC-THEME-LICENSE';
                
            // data to send in our API request
            $api_params = array( 
                'edd_action'=> 'activate_license', 
                'license'   => $license, 
                'item_name' => urlencode( ZC_EDD_CORE_THEME ),
                'url'       => home_url()
            );
            
            // Call the custom API.
            $response = wp_remote_get( add_query_arg( $api_params, ZC_EDD_STORE_URL ) );

            // make sure the response came back okay
            if ( is_wp_error( $response ) ) {
                return false;
            }

            // decode the license data
            $license_data = json_decode( wp_remote_retrieve_body( $response ) );

            update_option( 'zippy_courses_core_theme_license_status', $license_data->license );
        }

        update_option('zippy_courses_core_theme_last_activation_attempt', $now->format('U'));
    }
}
add_action('admin_init', 'zippy_courses_core_theme_activation');