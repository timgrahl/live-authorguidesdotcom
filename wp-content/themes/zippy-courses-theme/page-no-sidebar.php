<?php
/**
 * Template Name: No Sidebar
 */

get_header();

?>
<main class="site-content" role="main">

	<?php  if( have_posts() ) : the_post(); ?>
	
	<header class="page-header">
		<div class="container">
			<?php the_title( '<h2 class="entry-title page-title">', '</h2>' ); ?>				
		</div>
	</header>

	<?php endif; ?>

		<div id="primary" class="content-area">
		<div class="container">
			<div class="row">
				<div id="content" class="entry-content col-md-8 col-md-offset-2" role="main">

					<?php if ( has_post_thumbnail() && !( is_search() || is_archive() ) ) : ?>
					<div class="entry-featured-image">
						<?php the_post_thumbnail( 'large' ); ?>
					</div>
					<?php endif; ?>
					
					<?php the_content(); ?>
				</div><!-- #content -->
			</div>
		</div>
	</div><!-- #primary -->

</main>
<?php

get_footer();
