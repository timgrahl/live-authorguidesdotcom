<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
  <head>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    
    <!-- WP links -->
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
  </head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">

		<header id="site-header">
		<?php 

			require_once( ZC_THEME_DIR . '/lib/class-bootstrap-nav-walker.php' );

			$nav_header = '	<div class="container"><div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#%1$s">
			      <span class="sr-only">Toggle navigation</span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			    </button>
			    <a class="navbar-brand" href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . get_wpcs_logo() . '</a>
			  </div>';

			$options = array(
				'theme_location'  => 'primary',
				'container'       => 'nav',
				'container_class' => 'navbar navbar-default navbar-inverse navbar-static-top',
				'container_id'    => 'primary-nav',
				'menu_class'      => 'nav navbar-nav navbar-right',
				'menu_id'         => 'wpcs-primary-nav',
				'echo'            => true,
				'fallback_cb'     => '',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => $nav_header . '<div class="collapse navbar-collapse" id="%1$s"><ul class="%2$s">%3$s</ul></div></div>',
				'depth'           => 0,
				'walker'          => new Bootstrap_Nav_Walker()
			);

			if( zippy_layout_has_menu() ) {
				
				$menu = wp_nav_menu( $options );

				if( $menu === false ) {
					echo $nav_header;
				}
			}

		?>
		</header>
