<?php
/**
 * Template Name: No Sidebar
 */

get_header();

?>
<main class="site-content" role="main">

	<?php  if( have_posts() ) : the_post(); ?>
	
	<header class="page-header">
		<div class="container">
			<h2 class="entry-title page-title">Forums</h2>
		</div>
	</header>

	<?php endif; ?>
	
		<div id="bbp-primary" class="content-area">
			<?php the_content(); ?>
		</div>
	

</main>
<?php

get_footer();
