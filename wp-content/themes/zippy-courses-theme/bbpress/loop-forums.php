<?php

/**
 * Forums Loop
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<?php do_action( 'bbp_template_before_forums_loop' ); ?>

<div id="forums-list-<?php bbp_forum_id(); ?>" class="bbp-forums">

	<div class="bbp-header">

		<div class="forum-titles">
			<div class="bbp-forum-info col-md-7 col-sm-9 col-xs-12"><?php _e( 'Forum', 'bbpress' ); ?></div>
			<div class="bbp-forum-topic-count col-md-1 hidden-sm hidden-xs"><?php _e( 'Topics', 'bbpress' ); ?></div>
			<div class="bbp-forum-reply-count col-md-1 hidden-sm hidden-xs"><?php bbp_show_lead_topic() ? _e( 'Replies', 'bbpress' ) : _e( 'Posts', 'bbpress' ); ?></div>
			<div class="bbp-forum-freshness col-md-3 col-sm-3 hidden-xs"><?php _e( 'Freshness', 'bbpress' ); ?></div>
		</div>

	</div><!-- .bbp-header -->

	<div class="bbp-body">

		<?php while ( bbp_forums() ) : bbp_the_forum(); ?>

			<?php bbp_get_template_part( 'loop', 'single-forum' ); ?>

		<?php endwhile; ?>

	</div><!-- .bbp-body -->

	<div class="bbp-footer">
	</div><!-- .bbp-footer -->

</div><!-- .forums-directory -->

<?php do_action( 'bbp_template_after_forums_loop' ); ?>
