<?php

/**
 * Pagination for pages of topics (when viewing a forum)
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<?php do_action( 'bbp_template_before_pagination_loop' ); ?>

<div class="bbp-pagination row">

	<div class="bbp-pagination-count col-sm-6">

		<?php bbp_forum_pagination_count(); ?>

	</div>

	<div class="bbp-pagination-links col-sm-6">

		<?php bbp_forum_pagination_links(); ?>
		
	</div>
	
</div>

<?php do_action( 'bbp_template_after_pagination_loop' ); ?>
