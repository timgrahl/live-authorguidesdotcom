<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 */

global $post;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header row">
		<?php
			if ( is_single() ) :
				the_title( '<h2 class="entry-title"><span class="post-type-indicator">' . apply_filters( 'zippy_theme_post_type_indicator', get_post_type() ) . '</span>', '</h2>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><span class="post-type-indicator">' . apply_filters( 'zippy_theme_post_type_indicator', get_post_type() ) . '</span>', '</a></h2>' );
			endif;
		?>

		<?php if ( 'post' == get_post_type() ) : ?>
			<div class="entry-meta">
				
				<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) ) : ?>
					in <span class="cat-links"><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'wpcs' ) ); ?></span>
				<?php endif; ?>

				<?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
					<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'wpcs' ), __( '1 Comment', 'wpcs' ), __( '% Comments', 'wpcs' ) ); ?></span>
				<?php endif; ?>
			
			</div><!-- .entry-meta -->
		<?php endif; ?>
		
	</header><!-- .entry-header -->

	<div class="entry-featured-image">
		<?php the_post_thumbnail( 'large' ); ?>
	</div>

	<?php if ( is_search() || is_home() || is_archive() || is_category() || is_tag() || is_tax() ) : ?>
		<div class="entry-content entry-summary row">
			<div class="col-sm-10 col-sm-offset-1">
				<?php the_excerpt(); ?>
			</div><!-- .col-sm-8 -->
		</div><!-- .entry-summary -->
	<?php else : ?>

	<div class="entry-content row">
		<div class="col-sm-10 col-sm-offset-1">
			<?php
				the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wpcs' ) );
				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'wpcs' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
				) );
			?>

			<div class="single-entry-navigation">
				<div class="next-post">
					<?php previous_post_link( '%link', '&laquo; Previous' ); ?>
				</div>
				<div class="prev-post">
				<?php next_post_link( '%link', 'Next &raquo;' ); ?>
				</div>
			</div>

		</div>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<?php $tags = get_the_tags(); ?>
	<footer class="row">

		<div class="col-sm-8 post-tags">
			<?php if( $tags ) : ?>
				<span class="tag-links"><strong>Tags: </strong><?php the_tags( '', ', ', '' ); ?></span>	
			<?php endif; ?>
		</div>
		<div class="col-sm-4 text-right no-padding-right">
			<?php if( is_home() || is_archive() || is_search() ) : ?>
			<a href="<?php the_permalink(); ?>" class="btn btn-read-post"><?php _e( 'Read Post &raquo;', 'wpcs' ); ?></a>
			<?php endif;  ?>
		</div>
	</footer>

	
</article><!-- #post-## -->

<?php
// If comments are open or we have at least one comment, load up the comment template.
if ( comments_open($post->ID) || get_comments_number() ) {
	comments_template();
}
?>

