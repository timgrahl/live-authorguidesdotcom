<?php
/**
 * Zippy Courses functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Zippy Courses 0.9
 */

require_once(dirname(__FILE__) . '/updater.php');

if( !class_exists( 'Zippy_Theme_Layout' ) ) {
	// load our custom theme layout tool
	include( dirname( __FILE__ ) . '/lib/layout.php' );
}
 
if( !defined( 'ZC_THEME_DIR' ) ) {
	define( 'ZC_THEME_DIR', dirname( __FILE__ ) );
}

if( !defined( 'ZC_THEME_NAMESPACE' ) ) {
	define( 'ZC_THEME_NAMESPACE', 'wpcs' );
}

if( !defined( 'ZC_THEME_URL' ) ) {
	define( 'ZC_THEME_URL', get_template_directory_uri()  );
}

if ( ! function_exists( 'zippy_theme_setup' ) ) :

/**
 * Zippy Courses setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since WP Zippy Courses 1.0
 */
function zippy_theme_setup() {

	require_once( dirname( __FILE__ ) . '/lib/deprecated.php' );

	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Enable BBPress Support
	add_theme_support( 'bbpress' );
	
	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => __( 'Primary (Top) Menu ', 'wpcs' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );

}
endif; // wpcs_setup
add_action( 'after_setup_theme', 'zippy_theme_setup' );

if( is_admin() ) {
	require_once( dirname( __FILE__ ) . '/lib/admin.php' );
}


/**
 * Register three Zippy Courses widget areas.
 *
 * @since Zippy Courses 0.9
 *
 * @return void
 */
function wpcs_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'wpcs' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Main sidebar that appears on the left.', 'wpcs' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'wpcs' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears in the footer section of the site.', 'wpcs' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'wpcs_widgets_init' );

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Zippy Courses 0.9
 *
 * @return void
 */
function wpcs_scripts() {

	global $current_user, $wpcs_event_id;

	wp_enqueue_script('jquery');
	wp_enqueue_script( 'bootstrap-js', '//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js', array( 'jquery' ), ZC_THEME_VERSION, true );
	wp_enqueue_script( 'wpcs-plugins-js', ZC_THEME_URL . '/assets/js/plugins.js', array( 'bootstrap-js', 'jquery' ), ZC_THEME_VERSION, true );
	wp_enqueue_script( 'wpcs-js', ZC_THEME_URL . '/assets/js/wpcs.js', array( 'wpcs-plugins-js' ), ZC_THEME_VERSION, true );

	wp_localize_script( 'wpcs-js', 'CourseSoftware' , array( 
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'student_id' => $current_user->ID,
		'event_id' => $wpcs_event_id
	));

	// Load our main stylesheet.
	wp_enqueue_style( 'zippy-courses-style', get_stylesheet_uri() );

}
add_action( 'wp_enqueue_scripts', 'wpcs_scripts' );

/**
 * Extend the default WordPress body classes.
 *
 * @since Zippy Courses 0.9
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function wpcs_body_classes( $classes ) {

	global $post, $template;

	if( basename( $template ) == 'page-login.php' )
		$classes[] = 'zippy-login-page';

	return $classes;
}
add_filter( 'body_class', 'wpcs_body_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Zippy Courses 0.9
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function wpcs_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'wpcs' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'wpcs_wp_title', 10, 2 );


/**
 * This removes the attribution from the wp_footer 
 * because this theme uses it in zippy_footer_bottom
 * @return void
 */
function zippy_theme_remove_attribution_from_footer() {

	global $wp_filter;

	foreach( $wp_filter as $hook => $priorities ) {

		if( $hook == 'wp_footer' ) {

			foreach( $priorities as $priority => $functions ) {

				foreach( $functions as $name => $function ) {

					if( is_array( $function['function'] ) ) {

						if( strpos( $name, 'zippy_attribution' ) !== false )
							unset( $wp_filter[ $hook ][ $priority ][ $name ] );

					}
				}
			}
		}
	}
}
add_action( 'template_redirect', 'zippy_theme_remove_attribution_from_footer' );

function zippy_theme_favicon()
{
    $favicon = get_theme_mod('options_favicon_file', false);
    if ($favicon) {
        echo '<link rel="icon" type="image/ico" href="' . $favicon . '"/>';
    }        
}
add_action('wp_head', 'zippy_theme_favicon');

function wpcs_fonts() {
	echo '<link href="//fonts.googleapis.com/css?family=Open+Sans:600,400,300" rel="stylesheet" type="text/css">';
}
add_action( 'wp_head', 'wpcs_fonts' );

function wpcs_course_header( $post_id = 0 ) {
	global $post;
	$post_id = $post_id ? $post_id : $post->ID;
	echo wpcs_get_course_header( $post_id );
}

function wpcs_get_course_header( $post_id ) {
	
	global $post;

	$html = '<header class="course-header">' . 
				'<div class="container">';
					
					if( $post->post_type == 'lesson' ) {

						$unit = Course_Utilities::getLessonUnit( $post->ID );
						if( $unit ) {
							$html .= '<h2 class="entry-title course-title"><a href="' . get_permalink( $post_id ) . '">' . get_the_title( $post_id ) . '</a></h2>';
							$html .= '<h2 class="unit-title">' . get_the_title( $unit ) . '</h2>';
						} else {
							$html .= '<h2 class="entry-title course-title">&nbsp;</h2>';
							$html .= '<h2 class="orphan-lesson-course-title"><a href="' . get_permalink( $post_id ) . '">' . get_the_title( $post_id ) . '</a></h2>';
						}
					}

					if( $post->post_type == 'unit' || $post->post_type == 'course' ) {
						$html .= '<h2 class="entry-title course-title"><a href="' . get_permalink( $post_id ) . '">' . get_the_title( $post_id ) . '</a></h2>';
					}

					$html .= '<ul class="course-links">' . 
						'<li><a href="' . get_permalink( $post_id ) . '" class="course-overview">Course Overview</a></li>' . 
						'<li class="quick-nav-trigger"><a class="course-jump">Quick Nav</a>' . wpcs_get_course_quick_nav( $post_id ) . '</li>' .
					 '</ul>';

				$html .= '</div>' .
			'</header><!-- .entry-header -->';

	return $html;

}


function zippy_theme_show_hide_lessons_toggle( $unit_id ) {
	echo '<a class="zippy-theme-unit-lesson-toggle">[Show Lessons]</a>';
}
add_filter( 'zippy_hook_course_unit_top', 'zippy_theme_show_hide_lessons_toggle', 10, 1 );

function wpcs_course_quick_nav( $post_id = 0 ) {
	echo wpcs_get_course_quick_nav( $post_id );
}

function wpcs_get_course_quick_nav( $post_id = 0 ) {

	$previous = Course_Utilities::getPreviousLesson();
	$next = Course_Utilities::getNextLesson();

	$entries = Course_Utilities::get_course_entries( $post_id );

	$html = '<div class="course-quick-nav">';
		$html .= '<h4>Units in this Course</h4>';
		
		$html .= '<ul>';

		$i = 1;
		foreach( $entries as $entry ) {

			if( get_post_type( $entry ) == 'unit' ) {
				$html .= '<li><a href="' . get_permalink( $entry ) . '">' . get_the_title( $entry ) . '</a></li>';
			}

			if( get_post_type( $entry ) == 'lesson' ) {
				$html .= Course_Access::has_access( $entry ) ? '<li><a href="' . get_permalink( $entry ) . '">' . get_the_title( $entry ) . '</a></li>' : '';
			}

		}

		$html .= '</ul>';

		if( $previous ) {
			$html .= '<ul>';					
			$html .= '<li><a href="' . get_permalink( $previous ) . '">Previous Lesson</a></li>';
			$html .= '</ul>';
		}

		if( $next ) {
			$html .= '<ul>';					
			$html .= '<li><a href="' . get_permalink( $next ) . '">Next Lesson</a></li>';	
			$html .= '</ul>';					
		}

			
		$html .= '</ul>';
	$html .= '</div>';

	return $html;
}

function next_posts_link_attributes() {
    return 'class="next-posts"';
}
add_filter('next_posts_link_attributes', 'next_posts_link_attributes');

function prev_posts_link_attributes() {
    return 'class="prev-posts"';
}
add_filter('previous_posts_link_attributes', 'prev_posts_link_attributes');

function get_wpcs_logo( $type = 'default' ) {

	// Changed the Theme customizer details
	$old = get_theme_mod( 'wpcs_logo', '' );
	$new = $type == 'login' 
			? ( get_theme_mod( 'logo_login', '' ) == '' 
					? get_theme_mod( 'logo_primary', '' )
					: get_theme_mod( 'logo_login', '' ) )
			: get_theme_mod( 'logo_primary', '' );
	$default = '';

	$logo = $old == '' 
		? ( $new == '' ? $default : $new ) 
		: ( $new == '' ? $old : $new );

	$html = '';

	if( $logo == '' ) {	
		$html = get_bloginfo( 'name' );
	} else {
		
		global $wpdb;

		$img = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->posts WHERE guid = %s", $logo ) );
		$src = wp_get_attachment_image_src( $img->ID, 'full' );
		$html = '<img src="' . $src[0] . '" class="logo" width="' . $src[1] . '" height="' . $src[2] . '">';

	}

	return $html;

}

function wpcs_logo() {	
	echo get_wpcs_logo();
}

function zippy_customizer_render_css() {
	require_once dirname( __FILE__ ) . '/lib/customizer-css.php';
}
add_action( 'wp_head', 'zippy_customizer_render_css' );

function zippy_theme_activation() {
  
  	$check = get_option('zippy_theme_activation_check');

  	if ( $check != "set" ) {
    	// DO INITIALISATION STUFF

      	// Add marker so it doesn't run in future
    	add_option('zippy_theme_activation_check', "set");

	}

}
add_action('init', 'zippy_theme_activation');

function zippy_theme_deactivation($newname, $newtheme) {
	delete_option( 'zippy_theme_activation_check' );
}
add_action("switch_theme", "zippy_theme_deactivation", 10 , 2);  

function wpcs_tint_color( $color, $amount ) {

	$color_r = substr( str_replace( '#', '', $color ), 0, 2 );
	$color_r_hover = dechex ( floor( hexdec( $color_r ) * $amount ) );

	$color_g = substr( str_replace( '#', '', $color ), 2, 2 );
	$color_g_hover = dechex ( floor( hexdec( $color_g ) * $amount ) );

	$color_b = substr( str_replace( '#', '', $color ), 4, 2 );
	$color_b_hover = dechex ( floor( hexdec( $color_b ) * $amount ) );

	return '#' . $color_r_hover . $color_g_hover . $color_b_hover;

}

function zippy_theme_reset_password( $output, $user_id ) {

	$output = '<form action="" class="form-horizontal" method="POST" role="form">' . "\n" . 
		  '<div class="form-group">' . "\n" . 
		    '<label for="user_pass1"  class="col-sm-4 control-label">Password</label>' . "\n" . 
		    '<div class="col-sm-6">' . "\n" .
		    '<input type="password" class="form-control" id="user_pass1" name="user_pass1" placeholder="Password">' . "\n" . 
		    '</div>' . "\n" .
		  '</div>' . "\n" . 
		  '<div class="form-group">' . "\n" . 
		    '<label for="user_pass2"  class="col-sm-4 control-label">Confirm Password</label>' . "\n" . 
		    '<div class="col-sm-6">' . "\n" .
		    '<input type="password" class="form-control" id="user_pass2" name="user_pass2" placeholder="Confirm Password">' . "\n" . 
		    '</div>' . "\n" .
		  '</div>' . "\n" . 
		  '<div class="col-sm-offset-4 col-sm-6">' . "\n" .
		  	'<input type="hidden" name="user" value="' . $user_id . '" />' . "\n" . 
		  	'<input type="hidden" name="course_action" value="reset_password" />' . "\n" . 
		  	'<p><button type="submit" class="btn btn-primary">Reset Password</button></p>' . "\n" . 
		  '</div>' . "\n" . 
		'</form>' . "\n";

	return $output;

}

add_action( 'bbp_template_before_single_topic', 'zippy_bbp_topic_title' );
function zippy_bbp_topic_title() {
	
	global $post;

	$html = '<header class="entry-header row">';

		$html .= '<div class="col-sm-12"><h2 class="entry-title"><span class="post-type-indicator">' . apply_filters( 'zippy_theme_post_type_indicator', $post->post_type ) . '</span>' . $post->post_title . '</h2>' . bbp_get_topic_subscription_link( array( 'before' => '' )) . ' ' . bbp_get_user_favorites_link() . '</div>';

	$html .= '</header>';

	echo $html;

}

function zippy_bbp_search_title() {
	
	global $post;

	$html = '<header class="entry-header row">';

		$html .= '<div class="col-sm-12"><h2 class="entry-title"><span class="post-type-indicator">' . apply_filters( 'zippy_theme_post_type_indicator', 'Search Results for:' ) . '</span>' . bbp_get_search_terms() . '</h2></div>';

	$html .= '</header>';

	echo $html;

}
add_action( 'bbp_template_before_search', 'zippy_bbp_search_title' );

function zippy_bbp_topics_tag_title() {
	
	global $post;

	$html = '<header class="entry-header row">';

		$html .= '<div class="col-sm-12"><h2 class="entry-title"><span class="post-type-indicator">' . apply_filters( 'zippy_theme_post_type_indicator', 'Tagged:' ) . '</span>' . bbp_get_topic_tag_name() . '</h2></div>';

	$html .= '</header>';

	echo $html;

}
add_action( 'bbp_template_before_topics_index', 'zippy_bbp_topics_tag_title' );

function zippy_bbp_forum_title() {
	
	global $post;

	$html = '<header class="entry-header row">';

		$html .= '<div class="col-sm-12"><h2 class="entry-title"><span class="post-type-indicator">' . apply_filters( 'zippy_theme_post_type_indicator', $post->post_type ) . '</span>' . $post->post_title . '</h2>' . bbp_get_forum_subscription_link( array( 'before' => '' )) . ' ' . bbp_get_user_favorites_link() . '</div>';


	$html .= '</header>';

	echo $html;

}
add_action( 'bbp_template_before_single_forum', 'zippy_bbp_forum_title' );

function zippy_bbp_forums_title() {
	
	global $post;

	$html = '<header class="entry-header row">';

		$html .= '<div class="col-sm-12"><h2 class="entry-title"><span class="post-type-indicator">' . apply_filters( 'zippy_theme_post_type_indicator', 'Forums' ) . '</span></h2></div>';

	$html .= '</header>';

	echo $html;

}
add_action( 'bbp_template_before_forums_index', 'zippy_bbp_forums_title' );

function zippy_bbp_replace_title( $title ) {
	
	if( is_bbpress() )
		$title = 'Forums';

	return $title;

}
// add_filter( 'the_title', 'zippy_bbp_replace_title' );

add_action( 'bbp_get_single_topic_description', create_function( '', "return '';" ));
add_action( 'bbp_get_single_forum_description', create_function( '', "return '';" ));

function zippy_bbp_breadcrumbs_and_search() {
	?>
	
	<div class="zippy-bbp-top-nav">
		<div class="container">
			<div class="row">
				<div class="bbp-breadcrumb col-md-9">
					<?php bbp_breadcrumb( array( 'before' => '', 'after'  => '' )); ?>
				</div>
				<?php if ( bbp_allow_search() ) : ?>
				<div class="bbp-search-form col-md-3">
					<?php bbp_get_template_part( 'form', 'search' ); ?>
				</div>
				<?php endif; ?>				
			</div>
		</div>
	</div>
	
	<?php
}

/**
 * Class to create a custom menu control
 */


function zippy_theme_customizer_setup( $wp_customize ) {

	require_once( dirname( __FILE__ ) . '/lib/customizer.php' );

	$wp_customize->remove_section( 'background_image' );
	$wp_customize->remove_section( 'colors' );

	$defaults = array(
		'black' 		=> '#000000',
		'white' 		=> '#FFFFFF',
		'eggshell'		=> '#FAFAFA',
		'teal' 			=> '#15D3EB',
		'dark_gray' 	=> '#222222',
		'light_gray' 	=> '#CCCCCC',
		'lighter_gray' 	=> '#DDDDDD',
		'lightest_gray'	=> '#EEEEEE',
		'gray' 			=> '#777777',
		'dark_blue'		=> '#222233',
		'blue'			=> '#3b7db5',
		'green' 		=> '#33AA55'
	);

	$zc = new Zippy_Theme_Customizer();	

	$options = $zc->add_panel( 'options', 'Theme Options', '', 110 );
		$favicon_options = $options->add_section( 'favicon', 'Favicon' );
		$favicon_options->add_control( 'file', 'File', 'This file must be a .ico to function properly' )->set_type('upload');

	$layout = $zc->add_panel( 'layout', 'Default Layouts', '', 110 );

		$posts_and_pages_layout	= $layout->add_section( 'posts_and_pages', 'Posts and Pages' );
		$posts_and_pages_layout->add_setting_filter( 'name', array( 'zippy_basic_theme_layout', 'zippy' ) );

			$posts_and_pages_layout->add_control( 'layout', 'Layout', 'Change the default layout for Posts and Pages' )->set_choices( array( 'Right Sidebar', 'Left Sidebar', 'No Sidebar' ) )->set_type( 'select' );
			$posts_and_pages_layout->add_control( 'menu_visibility', 'Menu', 'Is the menu visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );
			$posts_and_pages_layout->add_control( 'header_visibility', 'Header', 'Is the header visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );
			$posts_and_pages_layout->add_control( 'footer_visibility', 'Footer', 'Is the footer visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );

		$courses_layout 		= $layout->add_section( 'courses', 'Courses' );
		$courses_layout->add_setting_filter( 'name', array( 'zippy_basic_theme_layout', 'zippy' ) );

			$courses_layout->add_control( 'layout', 'Layout', 'Change the default layout for Posts and Pages' )->set_choices( array( 'Right Sidebar', 'Left Sidebar', 'No Sidebar' ) )->set_type( 'select' );
			$courses_layout->add_control( 'menu_visibility', 'Menu', 'Is the menu visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );
			$courses_layout->add_control( 'header_visibility', 'Header', 'Is the header visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );
			$courses_layout->add_control( 'footer_visibility', 'Footer', 'Is the footer visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );

		$units_layout			= $layout->add_section( 'units', 'Units' );
		$units_layout->add_setting_filter( 'name', array( 'zippy_basic_theme_layout', 'zippy' ) );

			$units_layout->add_control( 'layout', 'Layout', 'Change the default layout for Posts and Pages' )->set_choices( array( 'Right Sidebar', 'Left Sidebar', 'No Sidebar' ) )->set_type( 'select' );
			$units_layout->add_control( 'menu_visibility', 'Menu', 'Is the menu visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );
			$units_layout->add_control( 'header_visibility', 'Header', 'Is the header visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );
			$units_layout->add_control( 'footer_visibility', 'Footer', 'Is the footer visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );

		$lessons_layout			= $layout->add_section( 'lessons', 'Lessons' );
		$lessons_layout->add_setting_filter( 'name', array( 'zippy_basic_theme_layout', 'zippy' ) );

			$lessons_layout->add_control( 'layout', 'Layout', 'Change the default layout for Posts and Pages' )->set_choices( array( 'Right Sidebar', 'Left Sidebar', 'No Sidebar' ) )->set_type( 'select' );
			$lessons_layout->add_control( 'menu_visibility', 'Menu', 'Is the menu visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );
			$lessons_layout->add_control( 'header_visibility', 'Header', 'Is the header visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );
			$lessons_layout->add_control( 'footer_visibility', 'Footer', 'Is the footer visible?' )->set_choices( array( '1' => 'On', '0' => 'Off' ) )->set_type( 'select' );

	$colors = $zc->add_panel( 'colors', 'Colors', '', 105 );

		$site_colors = $colors->add_section( 'site', 'Site' );
			$site_colors->add_control( 'bg', 'Background Color' )->set_type( 'color' )->set_default( $defaults['eggshell'] );
			$site_colors->add_control( 'text', 'Text Color' )->set_type( 'color' )->set_default( $defaults['dark_gray'] );
			$site_colors->add_control( 'link', 'Link Color' )->set_type( 'color' )->set_default( $defaults['blue'] );
			$site_colors->add_control( 'button_primary_bg', 'Primary Button Background' )->set_type( 'color' )->set_default( $defaults['blue'] );
			$site_colors->add_control( 'button_primary_text', 'Primary Button Text Color' )->set_type( 'color' )->set_default( $defaults['white'] );
			$site_colors->add_control( 'button_secondary_bg', 'Secondary Button Background' )->set_type( 'color' )->set_default( $defaults['lighter_gray'] );
			$site_colors->add_control( 'button_secondary_text', 'Secondary Button Text Color' )->set_type( 'color' )->set_default( $defaults['gray'] );
			$site_colors->add_control( 'button_complete_bg', 'Complete Lesson Button Background' )->set_type( 'color' )->set_default( $defaults['green'] );
			$site_colors->add_control( 'button_complete_text', 'Complete Lesson Button Text Color' )->set_type( 'color' )->set_default( $defaults['white'] );
			
		$login_colors = $colors->add_section( 'login', 'Login', 'These colors will be used on your Login Page template and no others.' );
			$login_colors->add_control( 'bg' , 'Background Color' )->set_type( 'color' )->set_default( $defaults['eggshell'] );
			$login_colors->add_control( 'form_bg' , 'Form Background Color ' )->set_type( 'color' )->set_default( $defaults['white'] );
			$login_colors->add_control( 'text', 'Text Color' )->set_type( 'color' )->set_default( $defaults['dark_gray'] );
			$login_colors->add_control( 'link', 'Login Link Color' )->set_type( 'color' )->set_default( $defaults['blue'] );
			$login_colors->add_control( 'button_bg', 'Login Button Background Color' )->set_type( 'color' )->set_default( $defaults['blue'] );
			$login_colors->add_control( 'button_text', 'Login Button Text Color' )->set_type( 'color' )->set_default( $defaults['white'] );

		$menu_colors = $colors->add_section( 'menu', 'Menu' );
			$menu_colors->add_control( 'bg', 'Background Color' )->set_type( 'color' )->set_default( $defaults['dark_gray'] );
			$menu_colors->add_control( 'link', 'Link Color' )->set_type( 'color' )->set_default( $defaults['light_gray'] );

		$header_colors = $colors->add_section( 'header', 'Primary Header', 'The <strong>Primary Header</strong> appears directly below the top menu, and is separate from the <strong>Content Header</strong>.' );
			$header_colors->add_control( 'bg', 'Background Color' )->set_type( 'color' )->set_default( $defaults['teal'] );
			$header_colors->add_control( 'text', 'Text Color' )->set_type( 'color' )->set_default( $defaults['white'] );

		$content_colors = $colors->add_section( 'content', 'Content', 'These colors are used to change the colors for the headings and content of posts, course, unit or lesson. To change the Page Header, use the <strong>Primary Header</strong> section in this panel and to change Text or Link color, use the <strong>Site</strong> section.' );
			$content_colors->add_control( 'bg', 'Background Color' )->set_type( 'color' )->set_default( $defaults['white'] );
			$content_colors->add_control( 'header_bg', 'Header Background Color' )->set_type( 'color' )->set_default( $defaults['dark_blue'] );
			$content_colors->add_control( 'header_text', 'Header Text Color' )->set_type( 'color' )->set_default( $defaults['white'] );
			$content_colors->add_control( 'footer_bg', 'Footer Background Color' )->set_type( 'color' )->set_default( $defaults['lighter_gray'] );
			$content_colors->add_control( 'footer_text', 'Footer Text Color' )->set_type( 'color' )->set_default( $defaults['dark_gray'] );

		$sidebar_colors = $colors->add_section( 'sidebar', 'Sidebar' );
			$sidebar_colors->add_control( 'heading', 'Heading Color' )->set_type( 'color' )->set_default( $defaults['dark_gray'] );

		$footer_colors = $colors->add_section( 'footer', 'Footer' );
			$footer_colors->add_control( 'bg', 'Background Color' )->set_type( 'color' )->set_default( $defaults['black'] );
			$footer_colors->add_control( 'text', 'Text Color' )->set_type( 'color' )->set_default( $defaults['lighter_gray'] );
			$footer_colors->add_control( 'link', 'Link Color' )->set_type( 'color' )->set_default( $defaults['lightest_gray'] );

	$logos = $zc->add_section( 'logo', 'Logos', '', 30 );
		$logos->add_control( 'primary', 'Primary Logo', 'This logo should be no taller than 40px.  It will appear on the left in the menu bar. If you do not set a Login Page Logo below, it will also display on the Login Page template.' )->set_type( 'image' );
		$logos->add_control( 'login', 'Login Page Logo', 'This logo can be larger, up to 300px wide and as tall as you would like.' )->set_type( 'image' );

	$zc->register();

}
add_action( 'customize_register', 'zippy_theme_customizer_setup' );

