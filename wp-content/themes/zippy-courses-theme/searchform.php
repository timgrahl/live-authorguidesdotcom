<form role="search" method="get" class="search-form form-inline row" action="<?php echo home_url(); ?>">
	<div class="col-sm-8">
		<label class="sr-only" for="search-form">Search for:</label>
		<input type="search" id="search-form" class="form-control" placeholder="Search…" value="" name="s" title="Search for:">
	</div>
	<div class="col-sm-4">
		<input type="submit" class="zippy-button zippy-button-primary search-submit" value="Search">
	</div>
</form>