<?php
/**
 */
global $wp_query;

get_header(); 

?>

<header class="archive-header">
	<div class="container">
		<h2 class="entry-title archive-title">Archive: <strong><?php single_month_title(' '); ?></strong></h2>
	</div>
</header>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div class="container">
			<div id="content" class="site-content col-sm-8" role="main">
			<?php while( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
			<?php posts_nav_link(); ?>
			</div>

			<?php get_sidebar(); ?>
		</div>
	</div>

<?php 
get_footer(); 
?>
