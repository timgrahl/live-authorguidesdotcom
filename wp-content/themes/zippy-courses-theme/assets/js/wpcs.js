(function ( $ ) {
	"use strict";

	$( '.comment-toggle' ).on( 'click', function( e ){
		e.preventDefault();
		$( '#comments' ).toggleClass( 'hidden-xs');
		$( '#comments' ).toggleClass( 'hidden-sm');

		$( this ).text( 'Hide Comments' );
	});
	
	$( '.course-header .course-grid-view, .course-header .course-list-view' ).click( function( e ) {
		e.preventDefault();
		
		var view = $( this ).data( 'courseView' );
		$( this ).addClass( 'active-view' ).siblings().removeClass( 'active-view' );

		$( '.course' ).removeClass( 'grid-view' ).removeClass( 'list-view' ).addClass( view );

	});

	$( '.unit-header .show-hide-unit-lessons' ).on( 'click', function( e ) {
		$( this ).parents( '.unit' ).find( '> .entry-content' ).slideToggle();
		var text = $( this ).text();
		$( this ).text( ( text == '[Show Lessons]') ? '[Hide Lessons]' : '[Show Lessons]' );
	});
	
	if($( '.edit-account-form' ).length > 0) {
		$( '.edit-account-form' ).validate({
			rules: {
				email: {
					email: true,
					required: true
				},
				user_firstname: {
					required: true,
					minlength: 2
				},
				user_lastname: {
					required: true,
					minlength: 2
				}

			}
		});
	}

	if($( '.change-password-form' ).length > 0) {
		$( '.change-password-form' ).validate({
			rules: {
				user_pass1: {
					required: true,
					minlength: 7,
					equalTo: '#user_pass2'
				},
				user_pass2: {
					required: true,
					minlength: 7
				}

			}
		});
	}

	if($( '.registration-form' ).length > 0) {
		$( '.registration-form' ).validate({
			rules: {
				first_name: {
					required: true,
					minlength: 2
				},
				last_name: {
					required: true,
					minlength: 2
				},
				user_email: {
					required: true,
					email: true,
					remote: {
						url: CourseSoftware.ajaxurl,
						type: "post",
						data: {
							action: 'validate_registration_email'						
						}, 
						complete: function( response ) {}
					}
				},
				user_login: {
					required: true,
					minlength: 3,
					remote: {
						url: CourseSoftware.ajaxurl,
						type: "post",
						data: {
							action: 'validate_registration_username'						
						}, 
						complete: function( response ) {}
					}
				},
				user_pass: {
					required: true,
					minlength: 7,
				},
				user_pass_2: {
					equalTo: '#user_password'	
				}

			},
			submitHandler: function( form ) {
				$( 'input[name="user_email"]').removeAttr( 'disabled' );
				form.submit();
			}
		
		});
	}

	if($( '.request-refund-form' ).length > 0) {
		$( '.request-refund-form' ).validate({
			rules: {
				'refund-request-reason': {
					required: true
				}
			},
			submitHandler: function( form ) {
				form.submit();
			}
		
		});	
	}

	$( '.entry-content' ).fitVids();
	$( '.entry-content' ).fitVids({ customSelector: "iframe[src^='http://fast.wistia.net'], iframe[src^='http://fast.wistia.net'], iframe[src^='//fast.wistia.net']"});
	$( '.entry-featured-image' ).fitVids({ customSelector: "iframe, object, embed"});

	$( '.zippy-theme-unit-lesson-toggle' ).on( 'click', function(e) {
		e.preventDefault();

		var $link = $( e.currentTarget );
		var $unit = $link.parent();

		$unit.find( '.zippy-unit-entries' ).slideToggle();

		if( $link.text() == '[Show Lessons]' ) {
			$link.text( '[Hide Lessons]' );
		} else {
			$link.text( '[Show Lessons]' );
		}
		
	});
}(jQuery));