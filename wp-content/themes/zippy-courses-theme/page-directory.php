<?php
/**
 * Template Name: Course Directory
 */

get_header();

?>
<main class="site-content" role="main">

	<?php  if( have_posts() ) : the_post(); ?>
		<?php if( zippy_layout_has_header() ) : ?>
			<header class="page-header">
				<div class="container">
					<?php the_title( '<h2 class="entry-title page-title">', '</h2>' ); ?>				
				</div>
			</header>
		<?php endif; ?>
	<?php endif; ?>

		<div id="primary" class="content-area">
		<div class="container">
			<div class="row">

				<?php if( zippy_layout_get_layout() === '1' ) : ?>
					<?php get_sidebar( 'primary' ); ?>	
				<?php endif; ?>

				<div id="content" class="entry-content <?php echo zippy_layout_get_content_container_classes(); ?>" role="main">

					<?php if ( has_post_thumbnail() && !( is_search() || is_archive() ) ) : ?>
					<div class="entry-featured-image">
						<?php the_post_thumbnail( 'large' ); ?>
					</div>
					<?php endif; ?>
					
					<?php the_content(); ?>

					<?php 
					if( class_exists( 'Zippy_Theme_Utilities' ) ) {
						Zippy_Theme_Utilities::course_directory(); 
					}
					?>

				</div><!-- #content -->
				
				<?php if( zippy_layout_get_layout() === '0' ) : ?>
					<?php get_sidebar( 'primary' ); ?>	
				<?php endif; ?>

			</div>
		</div>

	</div><!-- #primary -->

</main>
<?php

get_footer();

