<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); 
?>

<div id="main-content" class="main-content">
	
	<?php if( zippy_layout_has_header() ) : ?>
		<?php if( is_home() || get_post_type() == 'post' ) : ?>
			<header class="page-header">
				<div class="container">
					<h2 class="entry-title blog-title page-title">Blog</h2>
				</div>
			</header>
		<?php endif; ?>

		<?php if( get_post_type() == 'course' ) : ?>
			<header class="page-header">
				<div class="container">
					<p class="zippy-entry-type">Course</p>
					<h2 class="entry-title course-title page-title"><?php the_title(); ?></h2>
				</div>
			</header>
		<?php endif; ?>

		<?php if( get_post_type() == 'unit' || get_post_type() == 'lesson' ) : ?>
			<?php $parent_id = wp_get_post_parent_id( get_the_ID() ); ?>
			<header class="page-header">
				<div class="container">
					<p class="zippy-entry-type">Course</p>
					<h2 class="entry-title course-title page-title"><a href="<?php echo get_permalink( $parent_id ); ?>"><?php echo get_the_title( $parent_id ); ?></a></h2>
				</div>
			</header>
		<?php endif; ?>
	<?php endif; ?>

	<div id="primary" class="content-area">
		<div class="container">
			
			<?php if( zippy_layout_get_layout() === '1' ) : ?>
				<?php get_sidebar( 'primary' ); ?>	
			<?php endif; ?>

			<div id="content" class="site-content <?php echo zippy_layout_get_content_container_classes(); ?>" role="main">

				<?php
					if ( have_posts() ) :
						// Start the Loop.
						while ( have_posts() ) : the_post();

							/*
							 * Include the post format-specific template for the content. If you want to
							 * use this in a child theme, then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							get_template_part( 'content', get_post_format() );

						endwhile;
						// Previous/next post navigation.
						

					else :
						// If no content, include the "No posts found" template.
						get_template_part( 'content', 'none' );

					endif;

				?>

				<div class="posts-nav">
					<?php posts_nav_link(); ?>
				</div>

			</div><!-- #content -->

			<?php if( zippy_layout_get_layout() === '0' ) : ?>
				<?php get_sidebar( 'primary' ); ?>	
			<?php endif; ?>

		</div><!-- .container -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_footer();
