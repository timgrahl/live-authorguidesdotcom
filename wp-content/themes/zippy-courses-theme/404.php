<?php
/**
 * The template for displaying 404 pages (not found).
 *
 */

get_header(); ?>

<main class="site-content" role="main">

	<header class="page-header">
		<div class="container">
			<h2 class="entry-title page-title"><?php  echo apply_filters( 'zippy_404_page_title', 'Oops! That page can&rsquo;t be found.' ); ?></h2>	
		</div>
	</header>

	<div id="primary" class="content-area">

		<div class="container">
			
			<div class="row">

				<div id="content" class="entry-content col-md-10 col-md-offset-1" role="main">
				<?php if( is_user_logged_in() && class_exists( 'Course_CorePages' ) ) : ?>
					<p>Sorry about that! Don't worry, you can click your heels three times and then click the button below to return to the home page or your Dashboard.</p>
					<p><a href="<?php home_url(); ?>" class="btn btn-primary">Back to Home</a> <a href="<?php echo Course_CorePages::get( 'dashboard' ); ?>" class="btn btn-default">Back to Dashboard</a></p>
				<?php else : ?>
					<p>Sorry about that! Don't worry, you can click your heels three times and then click the button below to return to the home page.</p>
					<p><a href="<?php home_url(); ?>" class="btn btn-primary">Back to Home</a></p>
				<?php endif; ?>
				</div><!-- #content -->
				
			</div>

		</div>
	</div><!-- #primary -->

<?php get_footer(); ?>
