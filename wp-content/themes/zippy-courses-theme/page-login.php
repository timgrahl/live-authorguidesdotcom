<?php
/**
 *
 * Template Name: Login
 * 
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 */

get_header( 'login' ); 
?>

<main class="site-content" role="main">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-sm-offset-4">

				<h2 class="site-title">
					<?php echo get_wpcs_logo( 'login' ); ?>
				</h2>
				<div class="row login-box">
					<div class="col-sm-12">
						<?php do_action( 'wpcs_notifications' ); ?>
						<?php echo do_shortcode( '[course_login_form]' ); ?>	
						<p style="padding-top: 10px; margin-bottom: 0; text-align: center; clear: both;"><small><a href="<?php echo home_url( 'forgot-password' ); ?>">Forgot your password?</a></small></p>
					</div>
				</div>

				<?php do_action( 'zippy_theme_after_login_form' ); ?>
			</div>
		</div>
	</div>
</main>

<?php

get_footer( 'login' );
