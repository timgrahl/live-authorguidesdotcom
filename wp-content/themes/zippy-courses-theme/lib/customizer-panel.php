<?php

class Zippy_Theme_Customizer_Panel {

	public $sections = array();

	public $id;
	public $title;
	public $description;
	public $priority;

	public $customizer;

	public function __construct( $id, $title, $description = '', $priority = 100 ) {

		$this->id 			= $id;
		$this->title 		= $title;
		$this->description 	= $description;
		$this->priority 	= $priority;

	}

	public function set_customizer( $customizer ) {
		$this->customizer = $customizer;
	}

	public function register() {

		global $wp_customize;

		$wp_customize->add_panel( $this->get_name(), array(
		    'priority' => $this->priority,
		    'capability' => 'edit_theme_options',
		    'theme_supports' => '',
		    'title' => __( $this->title, 'zippy-basic-theme' ),
		    'description' => __( $this->description, 'zippy-basic-theme' ),
		));

		foreach( $this->sections as $section ) {
			$section->register();
		}

	}

	public function get_name() {

		$name = '';

		if( $this->customizer )
			$name .= $this->customizer->id . '_';

		$name .= $this->id;

		return $name;

	}

	public function add_section( $id, $title, $description = '', $priority = 100 ) {
	
		$section = new Zippy_Theme_Customizer_Section( $id, $title, $description, $priority );
		$section->set_panel( $this );

		$this->sections[ $id ] = $section;
		return $this->sections[ $id ];

	}

}
