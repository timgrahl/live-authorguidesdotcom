<?php

/**
 * @since 		0.9.4
 * @deprecated 	as of 0.9.13 
 * 
 * Please use Zippy_Theme_Layout::zippy_layout_get_menu instead
 *
 */
function zippy_layout_has_menu() {

	$layout = class_exists( 'Zippy_Theme_Layout' ) ? Zippy_Theme_Layout::get_instance() : false;

	return is_object( $layout ) ? $layout->has_menu() : true;

}

/**
 * @since 		0.9.4
 * @deprecated 	as of 0.9.13
 * 
 * Please use Zippy_Theme_Layout::zippy_layout_has_footer instead
 *
 */
function zippy_layout_has_footer() {

	$layout = class_exists( 'Zippy_Theme_Layout' ) ? Zippy_Theme_Layout::get_instance() : false;

	return is_object( $layout ) ? $layout->has_footer() : true;

}

/**
 * @since 		0.9.4
 * @deprecated 	as of 0.9.13
 * 
 * Please use Zippy_Theme_Layout::zippy_layout_has_header instead
 *
 */
function zippy_layout_has_header() {

	$layout = class_exists( 'Zippy_Theme_Layout' ) ? Zippy_Theme_Layout::get_instance() : false;

	return is_object( $layout ) ? $layout->has_header() : true;

}

/**
 * @since 		0.9.4
 * @deprecated 	as of 0.9.13
 * 
 * Please use Zippy_Theme_Layout::zippy_layout_get_layout instead
 *
 */
function zippy_layout_get_layout() {

	$layout = class_exists( 'Zippy_Theme_Layout' ) ? Zippy_Theme_Layout::get_instance() : false;

	return is_object( $layout ) ? $layout->get_layout() : '0';

}

/**
 * @since 		0.9.4
 * @deprecated 	as of 0.9.13
 * 
 * Please use Zippy_Theme_Layout::get_content_container_classes instead
 *
 */
function zippy_layout_get_content_container_classes() {

	$layout = class_exists( 'Zippy_Theme_Layout' ) ? Zippy_Theme_Layout::get_instance() : false;

	return is_object( $layout ) ? $layout->get_content_container_classes() : '';

}
