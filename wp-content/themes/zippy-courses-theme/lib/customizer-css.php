<?php

function zippy_select_color( $old = '', $new = '', $default = '' ) {
	$color = $old == '' 
		? ( $new == '' ? $default : $new ) 
		: ( $new == '' ? $old : $new );

	return strtoupper( $color );
}

$colors = array();

$zippy_color_defaults = array(
	'black' 		=> '#000000',
	'white' 		=> '#FFFFFF',
	'eggshell'		=> '#FAFAFA',
	'teal' 			=> '#15D3EB',
	'dark_gray' 	=> '#222222',
	'light_gray' 	=> '#CCCCCC',
	'lighter_gray' 	=> '#DDDDDD',
	'lightest_gray'	=> '#EEEEEE',
	'gray' 			=> '#777777',
	'dark_blue'		=> '#222233',
	'green'			=> '#33AA55',
	'blue'			=> '#3b7db5'
);

/***************************************************
 * Site Page 									   *
 ***************************************************/
$colors['site'] = array();

// Background (Not the Content Background)
$old 		= get_theme_mod( 'zippy_bg_color' );
$new 		= get_theme_mod( 'colors_site_bg' );
$default 	= $zippy_color_defaults['white'];

$colors['site']['background'] = zippy_select_color( $old, $new, $default );

// Text Color
$old 		= '';
$new 		= get_theme_mod( 'colors_site_text' );
$default 	= $zippy_color_defaults['dark_gray'];

$colors['site']['text'] = zippy_select_color( $old, $new, $default );

// Link Color
$old 		= get_theme_mod( 'wpcs_link_color' );
$new 		= get_theme_mod( 'colors_site_link' );
$default 	= $zippy_color_defaults['blue'];

$colors['site']['link'] = zippy_select_color( $old, $new, $default );

// Buttons
$colors['site']['buttons'] = array();
	
	// Primary Buttons
	$colors['site']['buttons']['primary'] = array();

		// Background
		$old 		= get_theme_mod( 'wpcs_primary_button_bg_color' );
		$new 		= get_theme_mod( 'colors_site_button_primary_bg' );
		$default 	= $zippy_color_defaults['blue'];

		$colors['site']['buttons']['primary']['background'] = zippy_select_color( $old, $new, $default );

		// Text
		$old 		= get_theme_mod( 'wpcs_primary_button_text_color' );
		$new 		= get_theme_mod( 'colors_site_button_primary_text' );
		$default 	= $zippy_color_defaults['white'];

		$colors['site']['buttons']['primary']['text'] = zippy_select_color( $old, $new, $default );



	// Secondary Buttons
	$colors['site']['buttons']['secondary'] = array();

		// Background
		$old 		= get_theme_mod( 'wpcs_secondary_button_bg_color' );
		$new 		= get_theme_mod( 'colors_site_button_secondary_bg' );
		$default 	= $zippy_color_defaults['blue'];

		$colors['site']['buttons']['secondary']['background'] = zippy_select_color( $old, $new, $default );

		// Text
		$old 		= get_theme_mod( 'wpcs_secondary_button_text_color' );
		$new 		= get_theme_mod( 'colors_site_button_secondary_text' );
		$default 	= $zippy_color_defaults['white'];

		$colors['site']['buttons']['secondary']['text'] = zippy_select_color( $old, $new, $default );

	// Secondary Buttons
	$colors['site']['buttons']['complete'] = array();

		// Background
		$new 		= get_theme_mod( 'colors_site_button_complete_bg' );
		$default 	= $zippy_color_defaults['green'];

		$colors['site']['buttons']['complete']['background'] = zippy_select_color( '', $new, $default );

		// Text
		$new 		= get_theme_mod( 'colors_site_button_complete_text' );
		$default 	= $zippy_color_defaults['white'];

		$colors['site']['buttons']['complete']['text'] = zippy_select_color( '', $new, $default );

/***************************************************
 * Login Page 									   *
 ***************************************************/

$colors['login'] = array();

// Background		
$old 		= get_theme_mod( 'wpcs_home_bg_color' );
$new 		= get_theme_mod( 'colors_login_bg' );
$default 	= $zippy_color_defaults['lightest_gray'];

$colors['login']['background'] = zippy_select_color( $old, $new, $default );
	
// Text
$old 		= '';
$new 		= get_theme_mod( 'colors_login_text' );
$default 	= $zippy_color_defaults['dark_gray'];

$colors['login']['text'] = zippy_select_color( $old, $new, $default );
	
// Link
$old 		= get_theme_mod( 'wpcs_link_color' );
$new 		= get_theme_mod( 'colors_login_link' );
$default 	= $zippy_color_defaults['blue'];

$colors['login']['link'] = zippy_select_color( $old, $new, $default );

// Form
$old 		= '';
$new 		= get_theme_mod( 'zippy_colors_' );
$default 	= $zippy_color_defaults['eggshell'];

$colors['login']['form'] = zippy_select_color( $old, $new, $default );

// Login Button
$colors['login']['button'] = array();

	// Background
	$old 		= get_theme_mod( 'wpcs_primary_button_bg_color' );
	$new 		= get_theme_mod( 'colors_login_button_bg' );
	$default 	= $zippy_color_defaults['blue'];

	$colors['login']['button']['background'] = zippy_select_color( $old, $new, $default );

	// Text
	$old 		= get_theme_mod( 'wpcs_primary_button_text_color' );
	$new 		= get_theme_mod( 'colors_login_button_text' );
	$default 	= $zippy_color_defaults['white'];

	$colors['login']['button']['text'] = zippy_select_color( $old, $new, $default );

/**************************************************
 * Menu
 **************************************************/
$colors['menu'] = array();
// Background
$old 		= get_theme_mod( 'wpcs_nav_bg_color' );
$new 		= get_theme_mod( 'colors_menu_bg' );
$default 	= $zippy_color_defaults['dark_gray'];

$colors['menu']['background'] = zippy_select_color( $old, $new, $default );

// Link
$old 		= get_theme_mod( 'wpcs_header_link_color' );
$new 		= get_theme_mod( 'colors_menu_link' );
$default 	= $zippy_color_defaults['lighter_gray'];

$colors['menu']['link'] = zippy_select_color( $old, $new, $default );



/**************************************************
 * Page Header
 **************************************************/
$colors['header'] = array();
// Background		
$old 		= get_theme_mod( 'wpcs_header_bg_color' );
$new 		= get_theme_mod( 'colors_header_bg' );
$default 	= $zippy_color_defaults['teal'];

$colors['header']['background'] = zippy_select_color( $old, $new, $default );

// Text
$old 		= get_theme_mod( 'wpcs_header_heading_color' );
$new 		= get_theme_mod( 'colors_header_text' );
$default 	= $zippy_color_defaults['white'];

$colors['header']['text'] = zippy_select_color( $old, $new, $default );
	
/**************************************************
 * Content
 **************************************************/
$colors['content'] = array();
// Background		
$old 		= '';
$new 		= get_theme_mod( 'colors_content_bg' );
$default 	= $zippy_color_defaults['white'];

$colors['content']['background'] = zippy_select_color( $old, $new, $default );

// Header 
$colors['content']['header']  = array();

	// Background
	$old 		= get_theme_mod( 'wpcs_entry_header_bg' );
	$new 		= get_theme_mod( 'colors_content_header_bg' );
	$default 	= $zippy_color_defaults['dark_blue'];

	$colors['content']['header']['background'] = zippy_select_color( $old, $new, $default );

	// Text
	$old 		= get_theme_mod( 'wpcs_entry_header_color' );
	$new 		= get_theme_mod( 'colors_content_header_text' );
	$default 	= $zippy_color_defaults['white'];

	$colors['content']['header']['text'] = zippy_select_color( $old, $new, $default );

// Header 
$colors['content']['footer']  = array();

	// Background
	$old 		= '';
	$new 		= get_theme_mod( 'colors_content_footer_bg' );
	$default 	= $zippy_color_defaults['lighter_gray'];

	$colors['content']['footer']['background'] = zippy_select_color( $old, $new, $default );

	// Text
	$old 		= '';
	$new 		= get_theme_mod( 'colors_content_footer_text' );
	$default 	= $zippy_color_defaults['dark_gray'];

	$colors['content']['footer']['text'] = zippy_select_color( $old, $new, $default );


/**************************************************
 * Sidebar
 **************************************************/
$colors['sidebar'] = array();
// Text
$old 		= '';
$new 		= get_theme_mod( 'colors_sidebar_heading' );
$default 	= $zippy_color_defaults['dark_gray'];

$colors['sidebar']['heading'] = zippy_select_color( $old, $new, $default );

/**************************************************
 * Footer
 **************************************************/

$colors['footer'] = array();

// Background
$old 		= get_theme_mod( 'wpcs_footer_bg_color' );
$new 		= get_theme_mod( 'colors_footer_bg' );
$default 	= $zippy_color_defaults['black'];

$colors['footer']['background'] = zippy_select_color( $old, $new, $default );

// Text
$old 		= get_theme_mod( 'wpcs_footer_text_color' );
$new 		= get_theme_mod( 'colors_footer_text' );
$default 	= $zippy_color_defaults['lighter_gray'];

$colors['footer']['text'] = zippy_select_color( $old, $new, $default );

// Link
$old 		= get_theme_mod( 'wpcs_footer_link_color' );
$new 		= get_theme_mod( 'colors_footer_link' );
$default 	= $zippy_color_defaults['lightest_gray'];

$colors['footer']['link'] = zippy_select_color( $old, $new, $default );

/**************************************************
 * Now we render!
 **************************************************/

require_once( dirname( __FILE__ ) . '/color-calculator.php' );

$calc = new Zippy_Theme_Util_Color_Calculator();

$output = '<style type="text/css" media="screen">';
	
	// Primary body styles
	$output .= 'body { background: ' . $colors['site']['background'] . '; color: ' . $colors['site']['text'] . ';}';
	$output .= 'a { color: ' . $colors['site']['link'] . ';}';

	// Login
	$output .= 'body.zippy-login-page { background: ' . $colors['login']['background'] . '; color: ' . $colors['login']['text'] . ';}';
	$output .= 'body.zippy-login-page .site-title { color: ' . $colors['login']['text'] . ';}';
	$output .= 'body.zippy-login-page .login-box { background: ' . $colors['login']['form'] . ';}';
	$output .= 'body.zippy-login-page a { color: ' . $colors['login']['link'] . ';}';
	$output .= 'body.zippy-login-page .zippy-button.zippy-button-primary { background: ' . $colors['login']['button']['background'] . '; color: ' . $colors['login']['button']['text'] . ';}';
	$output .= 'body.zippy-login-page .zippy-button.zippy-button-primary:hover, body.zippy-login-page .zippy-button.zippy-button-primary:active, body.zippy-login-page .zippy-button.zippy-button-primary:focus { background: ' . $calc->get_hover( $colors['login']['button']['background'] ) . '; color: ' . $colors['login']['button']['text'] . ';}';

	// $output .= 'body.zippy-login-page a { color: ' . $colors['login']['link'] . ';}';

	// Buttons

		// Primary
		$output .= '#comment_submit, .btn-primary, .zippy-button.zippy-button-primary, .zippy-continue, .zippy-quiz-next, .zippy-quiz-prev { background: ' . $colors['site']['buttons']['primary']['background'] . '; color: ' . $colors['site']['buttons']['primary']['text'] . '; }';

			// Hover and other states
			$output .= '#comment_submit:active, #comment_submit:focus, #comment_submit:hover, .active#comment_submit, .active.zippy-button.zippy-button-primary, .active.zippy-continue, .btn-primary.active, .btn-primary:active, .btn-primary:focus, .btn-primary:hover, .open .btn-primary.dropdown-toggle, .open .dropdown-toggle#comment_submit, .open .dropdown-toggle.zippy-button.zippy-button-primary, .open .dropdown-toggle.zippy-continue, .zippy-button.zippy-button-primary:active, .zippy-button.zippy-button-primary:focus, .zippy-button.zippy-button-primary:hover, .zippy-continue:active, .zippy-continue:focus, .zippy-continue:hover, .zippy-quiz-next:hover, .zippy-quiz-prev:hover, .zippy-quiz-next:active, .zippy-quiz-prev:active, .zippy-quiz-next:focus, .zippy-quiz-prev:focus { background: ' . $calc->get_hover( $colors['site']['buttons']['primary']['background'] ) . '; color: ' . $colors['site']['buttons']['primary']['text'] . ';}';

		// Secondary
		$output .= 	'.btn-secondary, .zippy-next, .zippy-previous, .next-posts, .prev-posts, .single-entry-navigation a { background: ' . $colors['site']['buttons']['secondary']['background'] . '; color: ' . $colors['site']['buttons']['secondary']['text'] . '; }';

		$output .= 	'.btn-secondary:hover, .zippy-next:hover, .zippy-previous:hover, .btn-secondary:active, .zippy-next:active, .zippy-previous:active, .btn-secondary:focus, .zippy-next:focus, .zippy-previous:focus, .next-posts:hover, .prev-posts:hover, .next-posts:active, .prev-posts:active, .next-posts:focus, .prev-posts:focus, .single-entry-navigation a:hover, .single-entry-navigation a:active, .single-entry-navigation a:focus { background: ' . $calc->get_hover( $colors['site']['buttons']['secondary']['background'] ) . '; color: ' . $colors['site']['buttons']['secondary']['text'] . '; }';

		// Complete
		$output .= 	'.zippy-button.zippy-complete-lesson { background: ' . $colors['site']['buttons']['complete']['background'] . '; color: ' . $colors['site']['buttons']['complete']['text'] . '; }';

		$output .= 	'.zippy-button.zippy-complete-lesson:hover, .zippy-button.zippy-complete-lesson:active, .zippy-button.zippy-complete-lesson:focus { background: ' . $calc->get_hover( $colors['site']['buttons']['complete']['background'] ) . '; color: ' . $colors['site']['buttons']['complete']['text'] . '; }';

	// Page Header

	$output .= 	'.page-header, .course-header, .order-header, .blog-header { background: ' . $colors['header']['background'] . ';}';

	$output .= 	'.order-header .blog-title a, .order-header .course-title a, .order-header .order-title a, .order-header .page-title a, .page-header .blog-title a, .page-header .course-title a, .page-header .page-title a, .order-header .zippy-entry-type, .page-header .zippy-entry-type { color: ' . $colors['header']['text'] . ';}';

	// Sidebar
	$output .= '#sidebar .widget-title { color: ' . $colors['sidebar']['heading'] . ';}';

	// Menu
	$output .= 	'.navbar-inverse .navbar-collapse, .navbar-inverse, #primary-nav .menu-item:hover .sub-menu, .nav > li > a, .navbar-inverse .navbar-toggle:hover, .navbar-inverse .navbar-toggle:focus, #primary-nav { background: ' . $colors['menu']['background'] . '; border-color: ' . $calc->get_hover( $colors['menu']['background'] ) . ';}';

	$output .= 	'#site-header { background: ' . $colors['menu']['background'] . '; }';

	$output .= 	'#primary-nav a:hover { background: ' . $calc->get_hover( $colors['menu']['background'] ) . '; color: ' . $colors['menu']['link'] . '; }';

	$output .= 	'.navbar-inverse .navbar-nav > li > a, .navbar-brand, .navbar-inverse .navbar-brand, .navbar-brand:hover, .navbar-inverse .navbar-brand:hover, #primary-nav .menu-item .sub-menu .menu-item a, .navbar-inverse .navbar-toggle:hover, .navbar-inverse .navbar-toggle:focus { color: ' . $colors['menu']['link'] . '; }';

	// Footer
	$output .= 	'.site-footer { background: ' . $colors['footer']['background'] . '; color: ' . $colors['footer']['text'] . ';}';
	$output .= 	'.site-footer a { color: ' . $colors['footer']['link'] . '; }';
	$output .= 	'.site-footer a:hover { color: ' . $calc->lighten( $colors['footer']['link'], 25 ) . '; }';

	// Content 

	$output .= 	'body.archive .post header .entry-title, body.search .post header .entry-title, body.search .page header .entry-title, body.search .lesson header .entry-title, body.search .course header .entry-title, body.blog .post header .entry-title, body.blog body.search .page header .entry-title, body.search body.blog .page header .entry-title, body.blog body.search .lesson header .entry-title, body.search body.blog .lesson header .entry-title, body.blog body.search .course header .entry-title, body.search body.blog .course header .entry-title, body.single-post .post header .entry-title, body.single-post body.search .page header .entry-title, body.search body.single-post .page header .entry-title, body.single-post body.search .lesson header .entry-title, body.search body.single-post .lesson header .entry-title, body.single-post body.search .course header .entry-title, body.search body.single-post .course header .entry-title,  .zippy .zippy-course-entry .zippy-course-header, .zippy #content > article header .entry-title, .bbpress .entry-header, .bbpress .entry-header .entry-title {' .
				'background: ' . $colors['content']['header']['background'] . ';' .
			'}';

	$output .=	'body.archive .post header .entry-title a:hover, body.search .post header .entry-title a:hover, body.search .page header .entry-title a:hover, body.search .lesson header .entry-title a:hover, body.search .course header .entry-title a:hover, body.blog .post header .entry-title a:hover, body.blog body.search .page header .entry-title a:hover, body.search body.blog .page header .entry-title a:hover, body.blog body.search .lesson header .entry-title a:hover, body.search body.blog .lesson header .entry-title a:hover, body.blog body.search .course header .entry-title a:hover, body.search body.blog .course header .entry-title a:hover, body.single-post .post header .entry-title a:hover, body.single-post body.search .page header .entry-title a:hover, body.search body.single-post .page header .entry-title a:hover, body.single-post body.search .lesson header .entry-title a:hover, body.search body.single-post .lesson header .entry-title a:hover, body.single-post body.search .course header .entry-title a:hover, body.search body.single-post .course header .entry-title a:hover, .zippy #content > article header .entry-title a:hover { ' . 
			'background: ' . $calc->get_hover( $colors['content']['header']['background'] ) . ';' .
					'}';

	$output .= 	'body.archive .post header .entry-meta, body.search .post header .entry-meta, body.search .page header .entry-meta, body.search .lesson header .entry-meta, body.search .course header .entry-meta, body.blog .post header .entry-meta, body.blog body.search .page header .entry-meta, body.search body.blog .page header .entry-meta, body.blog body.search .lesson header .entry-meta, body.search body.blog .lesson header .entry-meta, body.blog body.search .course header .entry-meta, body.search body.blog .course header .entry-meta, body.single-post .post header .entry-meta, body.single-post body.search .page header .entry-meta, body.search body.single-post .page header .entry-meta, body.single-post body.search .lesson header .entry-meta, body.search body.single-post .lesson header .entry-meta, body.single-post body.search .course header .entry-meta, body.search body.single-post .course header .entry-meta, .zippy #content > article header .entry-meta, .bbpress .bbp-pagination  {' . 
						'background: ' . $calc->darken( $colors['content']['header']['background'], 25 ) . ';' .
					'}';

	$output .= 	'body.archive .post header .entry-title a, body.search .post header .entry-title a, body.search .page header .entry-title a, body.search .lesson header .entry-title a, body.search .course header .entry-title a, body.blog .post header .entry-title a, body.blog body.search .page header .entry-title a, body.search body.blog .page header .entry-title a, body.blog body.search .lesson header .entry-title a, body.search body.blog .lesson header .entry-title a, body.blog body.search .course header .entry-title a, body.search body.blog .course header .entry-title a, body.single-post .post header .entry-title a, body.single-post body.search .page header .entry-title a, body.search body.single-post .page header .entry-title a, body.single-post body.search .lesson header .entry-title a, body.search body.single-post .lesson header .entry-title a, body.single-post body.search .course header .entry-title a, body.search body.single-post .course header .entry-title a, .zippy-course-entry .zippy-course-header .zippy-course-title a, .zippy-course-entry:hover .zippy-course-header .zippy-course-title a, .zippy-course-entry .zippy-course-header .zippy-course-progress p, .entry-header .entry-title  {' . 
						'color: ' . $colors['content']['header']['text'] . ';' .
					'}';

	$output .= '.zippy-course-entry, #content > article { background: ' . $colors['content']['background'] . '; border-color: ' . $calc->get_hover( $colors['site']['background'] ) . '; }';

	$output .= '#content > article > footer, .zippy-course-entry .zippy-course-footer, .zippy-course-footer { background: ' . $colors['content']['footer']['background'] . '; border-color: ' . $calc->get_hover( $colors['content']['footer']['background'] )  . '; }';

	$output .= '.zippy-course-entry .zippy-course-footer .zippy-course-continue { background: ' . $colors['content']['footer']['background'] . '; color: ' . $colors['content']['footer']['text'] . ';}';
	$output .= '.zippy-course-entry .zippy-course-footer .zippy-course-continue:hover { background: ' . $calc->get_hover( $colors['content']['footer']['background'] ) . '; color: ' . $colors['content']['footer']['text'] . ';}';

	$output .= '.zippy-course-entry .zippy-course-footer .zippy-course-continue { border-color: ' . $calc->get_hover( $colors['content']['footer']['background'] ) . '; }';

	$output .= '#content > article > footer .btn-read-post:hover { background:  ' . $calc->get_hover( $colors['content']['footer']['background'] ) . '; color: ' . $colors['content']['footer']['text'] . '; }';

	$output .= '#content > article > footer .btn-read-post { background: ' . $colors['content']['footer']['background'] . '; color: ' . $colors['content']['footer']['text'] . '; }';

$output .= '</style>';

echo $output;