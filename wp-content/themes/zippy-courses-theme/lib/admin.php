<?php

function zippy_theme_metaboxes() {
	zippy_theme_layout_mb();
}

function zippy_theme_layout_mb() {

	$post_types = array( 'page', 'post', 'course', 'unit', 'lesson' );
	
	foreach( $post_types as $pt ) {
		zippy_theme_layout_cpt_mb( $pt );
	}

	zippy_theme_course_public_layout_mb();

}

function zippy_theme_layout_cpt_mb( $post_type ) {

	$mb = new MetaBox( 'Template Options', array( 'post_type' => $post_type ) );

	$options = $mb->addSection( 'zippy_template_options', array(
		'ownsChildren' => true
	));

	$options->addCheckboxField( 'override', '', array( '1' => 'Override Default Layout?' ) );

	$options->addSelectField( 'layout', 'Layout: ', array( 'Right Sidebar', 'Left Sidebar', 'No Sidebar' ) )->addContainerClass( 'zippy-layout-option' );
	$options->addSelectField( 'menu', 'Navigation: ', array( '1' => 'On', '0' => 'Off' ) )->addContainerClass( 'zippy-layout-option' );
	$options->addSelectField( 'header', 'Header: ', array( '1' => 'On', '0' => 'Off' ) )->addContainerClass( 'zippy-layout-option' );
	$options->addSelectField( 'footer', 'Footer: ', array( '1' => 'On', '0' => 'Off' ) )->addContainerClass( 'zippy-layout-option' );

	add_meta_box( 'zippy-template-options', 'Template Options', array( $mb, 'render' ), $post_type, 'side', 'default' );

}

function zippy_theme_course_public_layout_mb() {

	$post_type = 'course';

	$mb = new MetaBox( 'Template Options', array( 'post_type' => $post_type ) );

	$options = $mb->addSection( 'zippy_public_template_options', array(
		'ownsChildren' => true
	));

	$options->addCheckboxField( 'override', '', array( '1' => 'Override Default Layout?' ) );

	$options->addSelectField( 'layout', 'Layout: ', array( 'Right Sidebar', 'Left Sidebar', 'No Sidebar' ) )->addContainerClass( 'zippy-layout-option' );
	$options->addSelectField( 'menu', 'Navigation: ', array( '1' => 'On', '0' => 'Off' ) )->addContainerClass( 'zippy-layout-option' );
	$options->addSelectField( 'header', 'Header: ', array( '1' => 'On', '0' => 'Off' ) )->addContainerClass( 'zippy-layout-option' );
	$options->addSelectField( 'footer', 'Footer: ', array( '1' => 'On', '0' => 'Off' ) )->addContainerClass( 'zippy-layout-option' );

	add_meta_box( 'zippy-public-template-options', 'Public Template Options', array( $mb, 'render' ), $post_type, 'course_pricing_options_side', 'default' );

}

// This relies on the Zippy Courses plugin MetaBox class
if( class_exists( 'MetaBox' ) ) {
	add_action( 'admin_init', 'zippy_theme_metaboxes' );	
}