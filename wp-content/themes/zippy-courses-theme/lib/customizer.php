<?php

require_once dirname( __FILE__ ) . '/customizer-panel.php';
require_once dirname( __FILE__ ) . '/customizer-section.php';
require_once dirname( __FILE__ ) . '/customizer-control.php';

class  Zippy_Theme_Customizer {

	public $panels = array();
	public $sections = array();

	public $id;

	public function __construct( $id = 'zippy_basic_theme' ) {
		$this->id = $id;		
	}

	public function add_panel( $id, $title, $description = '', $priority = 100 ) {
		
		$panel = new Zippy_Theme_Customizer_Panel( $id, $title, $description, $priority );
		$panel->set_customizer( $this );

		$this->panels[ $id ] = $panel;
		
		return $this->panels[ $id ];

	}

	public function add_section( $id, $title, $description = '', $priority = 100 ) {
	
		$section = new Zippy_Theme_Customizer_Section( $id, $title, $description, $priority );

		$this->sections[ $id ] = $section;
		return $this->sections[ $id ];

	}

	public function register() {

		global $wp_customize;

		foreach( $this->sections as $section ) {
			$section->register();
		}

		foreach( $this->panels as $panel ) {
			$panel->register();
		}
	}
}
