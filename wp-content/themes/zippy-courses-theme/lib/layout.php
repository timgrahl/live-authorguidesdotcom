<?php

class Zippy_Theme_Layout {

	public $options = array();

	/**
	 * Instance of this class.
	 *
	 * @since   0.9
	 *
	 * @var      object
	 */
	protected static $instance = null;	

	public function __construct() {
		$this->get_options();	
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since    0.9
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;

	}
	
	public function get_options() {

		global $post;

		if( !is_object( $post ) )
			return;
		
		$defaults = $this->get_default_options();

		if( is_front_page() ) {
			$post_id = get_option( 'page_on_front' );
		} elseif( is_home() ) {
			$post_id = get_option( 'page_for_posts' );
		} elseif( is_404() ) {
			$post_id = 0;
		} else {
			$post_id = $post->ID;
		}

		$options = (array) get_post_meta( $post_id, 'zippy_template_options', true );

		if( get_post_type() == 'course' ) {

			$public = get_post_meta( $post_id, 'enable-public-details', true );

			if( is_array( $public ) && !is_user_logged_in() ) {
				$options = (array) get_post_meta( $post_id, 'zippy_public_template_options', true );				
			}

		}

		if( isset( $options['override'] ) && is_array( $options['override'] ) ) {

			foreach ( $defaults as $key => $value) {
				$this->options[ $key ] = array_key_exists( $key, $options ) ? $options[$key] : $value;
			}

		} else {
			$this->options = $defaults;
		}

		if( isset( $this->options[0] ) )
			unset( $this->options[0] );

	}

	public function get_default_options() {

		global $post;

		if( is_front_page() ) {
			$post_id = get_option( 'page_on_front' );
		} elseif( is_home() ) {
			$post_id = get_option( 'page_for_posts' );
		} elseif( is_404() ) {
			$post_id = 0;
		} else {
			$post_id = $post->ID;
		}

		switch( get_post_type( $post_id ) ) {

			case 'course' :

				$defaults = array(
					'layout' => get_theme_mod( 'layout_courses_layout', '0' ),
					'menu' 	 => get_theme_mod( 'layout_courses_menu_visibility', '1' ),
					'header' => get_theme_mod( 'layout_courses_header_visibility', '1' ),
					'footer' => get_theme_mod( 'layout_courses_footer_visibility', '1' )
				);
				break;
			case 'unit' :
				$defaults = array(
					'layout' => get_theme_mod( 'layout_units_layout', '0' ),
					'menu'   => get_theme_mod( 'layout_units_menu_visibility', '1' ),
					'header' => get_theme_mod( 'layout_units_header_visibility', '1' ),
					'footer' => get_theme_mod( 'layout_units_footer_visibility', '1' )
				);
				break;
			case 'lesson' :
				$defaults = array(
					'layout' => get_theme_mod( 'layout_lessons_layout', '0' ),
					'menu'   => get_theme_mod( 'layout_lessons_menu_visibility', '1' ),
					'header' => get_theme_mod( 'layout_lessons_header_visibility', '1' ),
					'footer' => get_theme_mod( 'layout_lessons_footer_visibility', '1' )
				);
				break;
			case 'post' :
			case 'page' :
			default:
				$defaults = array(
					'layout' => get_theme_mod( 'layout_posts_and_pages_layout', '0' ),
					'menu'   => get_theme_mod( 'layout_posts_and_pages_menu_visibility', '1' ),
					'header' => get_theme_mod( 'layout_posts_and_pages_header_visibility', '1' ),
					'footer' => get_theme_mod( 'layout_posts_and_pages_footer_visibility', '1' )
				);
				break;
		}

		if( $defaults['layout'] == '' )
			$defaults['layout'] = '0';
		
		return $defaults;

	}

	public function get_content_container_classes() {

		switch( $this->options['layout'] ) {
			case '2' : // No sidebar
				$classes = 'col-md-10 col-md-offset-1';
				break;
			case '0' : // Right Sidebar
			case '1' : // Left Sidebar
			default :
				$classes = 'col-md-8';
				break;
		}

		return $classes;

	}

	public function get_sidebar_container_classes() {

		switch( $this->options['layout'] ) {
			case '2' : // No sidebar
				$classes = 'hide';
				break;
			case '0' : // Right Sidebar
			case '1' : // Left Sidebar
			default :
				$classes = 'col-md-4';
				break;
		}

		return $classes;

	}

	public function get_layout() {
		return isset( $this->options['layout'] ) && $this->options['layout'] != '' ? $this->options['layout'] : '0';
	}

	public function has_menu() {
		return isset( $this->options['menu'] ) && $this->options['menu'] != '' ? $this->options['menu'] : '1';
	}

	public function has_footer() {
		return isset( $this->options['footer'] ) &&$this->options['footer'] != '' ? $this->options['footer'] : '1';
	}

	public function has_header() {
		return isset( $this->options['header'] ) && $this->options['header'] != '' ? $this->options['header'] : '1';
	}

}