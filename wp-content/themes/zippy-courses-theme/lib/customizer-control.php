<?php

class Zippy_Theme_Customizer_Control {

	public $section, $type, $default, $id, $label, $description;
	protected $name, $setting_name;
	
	public $choices = array();
	public $settings_filters = array();

	public function __construct( $id, $label, $description = '', $priority = 100 ) {
		$this->label = $label;
		$this->id = $id;
		$this->description = $description;
		$this->priority = $priority;
	}

	public function set_type( $type ) {
		$this->type = $type;
		return $this;
	}

	public function set_default( $default ) {
		$this->default = $default;
		return $this;
	}

	public function set_choices( array $choices ) {
		$this->choices = $choices;
		return $this;
	}

	public function set_section( $section ) {
		$this->section = $section;
		return $this;
	}

	public function register() {

		global $wp_customize;

		$settings_args = array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options'
		);

			if( isset( $this->default ) )
				$settings_args['default'] = $this->default;

		$control_args = array(
			'label'    => __( $this->label, 'zippy-basic-theme' ),
			'section'  => $this->section->get_name(),
			'settings' => $this->get_setting_name(),
			'priority' => $this->priority,
			'description' 	=> __( $this->description, 'zippy-basic-theme' )
		);

			if( isset( $this->choices ) )
				$control_args['choices'] = $this->choices;

			if( isset( $this->type ) )
				$control_args['type'] = $this->type;

		$wp_customize->add_setting( $this->get_setting_name(), $settings_args );

		if( $this->type && $this->type == 'color' ) {
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, $this->get_name(), $control_args ) );			
		} elseif( $this->type && $this->type == 'image' ) {
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, $this->get_name(), $control_args ) );		
		} elseif( $this->type && $this->type == 'upload' ) {
			$wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize, $this->get_name(), $control_args ) );		
		} else {
			$wp_customize->add_control( $this->get_name(), $control_args );			
		}

	}

	public function get_name() {

		$name = '';

		if( $this->section ) {

			if( $this->section->panel )
				$name .= $this->section->panel->id . '_';

			$name .= $this->section->id . '_';

		}

		$name .= $this->id;

		return $this->filter( 'control', 'name', $name );

	}

	public function get_setting_name() {

		$name = '';

		if( $this->section ) {

			if( $this->section->panel )
				$name .= $this->section->panel->id . '_';

			$name .= $this->section->id . '_';

		}

		$name .= $this->id;

		return $this->filter( 'setting', 'name', $name );

	}

	public function filter( $type, $field, $value ) {

		switch( $type ) {
			case 'setting' :
				if( $this->section && !empty( $this->section->settings_filters ) ) {

					foreach( $this->section->settings_filters as $key => $search_replace ) {
						$value = str_replace( $search_replace[0], $search_replace[1], $value );
					}

				}
				break;
			default:
				break;
		}

		return $value;

	}


}
