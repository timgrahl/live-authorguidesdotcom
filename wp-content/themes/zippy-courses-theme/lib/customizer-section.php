<?php

class  Zippy_Theme_Customizer_Section {

	public $controls = array();

	public $id;
	public $name;
	public $title;
	public $description;
	public $priority;
	public $panel;

	public $settings_filters = array();

	public function __construct( $id, $title, $description = '', $priority = 100 ) {

		$this->id 			= $id;
		$this->title 		= $title;
		$this->description 	= $description;
		$this->priority 	= $priority;

	}

	public function add_setting_filter( $name, array $search_replace ) {
		$this->settings_filters[ $name ] = $search_replace;
	}

	public function register() {

		global $wp_customize;

		$args = array(
			'priority' => $this->priority,
		    'capability' => 'edit_theme_options',
		    'theme_supports' => '',
		    'title' => __( $this->title, 'zippy-basic-theme' ),
		    'description' => __( $this->description, 'zippy-basic-theme' )
		);

		if( isset( $this->panel ) )
			$args[ 'panel' ] = $this->panel->get_name();

		$wp_customize->add_section( $this->get_name(), $args );

		foreach( $this->controls as $control ) {
			$control->register();
		}

	}

	public function set_panel( Zippy_Theme_Customizer_Panel $panel ) {
		$this->panel = $panel;
	}

	public function set_name( $name ) {
		$this->name = $name;
	}

	public function get_name() {
		return $this->name ? $this->name : $this->id;
	}

	public function add_control( $id, $title, $description = '', $priority = 100 ) {
	
		$control = new Zippy_Theme_Customizer_Control( $id, $title, $description, $priority );
		$control->set_section( $this );

		$this->controls[ $id ] = $control;
		return $this->controls[ $id ];
	
	}

}

