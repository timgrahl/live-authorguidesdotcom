<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 */

?>
	<?php if( zippy_layout_has_footer() ) : ?>	
		<footer id="colophon" class="site-footer" role="contentinfo">
			
			<div class="container">
				
				<div class="footer-widgets row">
					<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</div>
				
			</div>

			<p class="text-center no-margin">Copyright &copy; <?php echo date( 'Y' ); ?> <?php echo get_bloginfo( 'name' ); ?></p>

			<?php do_action( 'zippy_footer_bottom' ); ?>

		</footer><!-- #colophon -->
	<?php endif; ?>

	<?php wp_footer(); ?>

</body>
</html>