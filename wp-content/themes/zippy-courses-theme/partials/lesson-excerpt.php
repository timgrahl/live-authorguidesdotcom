<?php global $lesson_class, $wpcs_student; ?>

<?php if( Course_Access::has_tier_access( $post->ID ) ): ?>

	<article id="lesson-<?php the_ID(); ?>" <?php post_class( $lesson_class . ' row' ); ?>>
	<?php $lesson_class = ( $lesson_class == 'striped' ) ? '' : 'striped'; ?>

		<header class="entry-header col-md-4 col-sm-12">
			
			<h3 class="entry-title">
				<?php if( Course_Utilities::is_lesson_complete( $post->ID ) ) : ?>
					<span class="completed">&#10004;</span>
				<?php endif; ?>

				<?php if( Course_Access::has_lesson_access( $post->ID ) ): ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<?php else: ?>
					<?php the_title(); ?>
				<?php endif; ?>
			</h3>

		</header>

		<section class="entry-content col-md-6 col-sm-12">
		
			<div class="entry-description">
				<?php 

				if( !empty( $post->post_excerpt ) ) {
					echo wpautop( $post->post_excerpt );
				} else {
					echo wpautop( wp_trim_words( $post->post_content, 55 ) );
				}
				?>
			</div>
		 	
		</section>

		<footer class="col-md-2 col-sm-12 text-right no-padding-right">
			<?php if( Course_Access::has_access( $post->ID ) ): ?>
				<a href="<?php the_permalink(); ?>" class="btn btn-default">View &raquo;</a>
			<?php else: ?>
				<?php $date = Course_Access::get_lesson_access_date( $post->ID ); ?>
				<p class="lesson-available"><strong>Available:</strong><br/><?php echo $date->format( 'M d, Y' ); ?></p>
			<?php endif; ?>
		</footer>

	</article>
	
<?php endif; ?>