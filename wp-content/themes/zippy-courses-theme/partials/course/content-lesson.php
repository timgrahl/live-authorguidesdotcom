<?php 

global $post; 

wpcs_course_header( $post->post_parent );
$unit = Course_Utilities::getLessonUnit( $post->ID );
?>

<div class="container">
	<div class="row">
		<article <?php post_class( 'post_box col-sm-10 col-sm-offset-1' ); ?>>

			<header class="entry-header row">
				<div class="col-sm-12">
					<?php
						if ( is_single() || is_page() ) :
							the_title( '<h2 class="entry-title"><span class="post-type-indicator">' . apply_filters( 'zippy_theme_post_type_indicator', 'Lesson' ) . '</span>', '</h2>' );
						else :
							the_title( '<h2 class="entry-title"><span class="post-type-indicator">' . apply_filters( 'zippy_theme_post_type_indicator', 'Lesson' ) . '</span><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						endif;

					?>

					<?php if( Course_Utilities::is_lesson_complete( $post->ID ) ) : ?>
						<span class="lesson-completed">Completed!</span>
					<?php else : ?>
						<a href="" class="complete-lesson btn btn-primary" data-id="<?php echo $post->ID; ?>">Complete Lesson</a>
					<?php endif; ?>
					
				</div>
			</header><!-- .entry-header -->

			<?php do_action( ZC_THEME_NAMESPACE . '_content_before' ); ?>
			<section class="entry-content row">
				<div class=" col-sm-10 col-sm-offset-1">
				<?php 
					
					do_action( ZC_THEME_NAMESPACE . '_content_top' );
					
					the_content();

					do_action( ZC_THEME_NAMESPACE . '_content_bottom' ); 


				?>

					<nav class="lesson-next-previous">
						<?php echo Course_Utilities::getPreviousLessonButton(); ?>
						<?php echo Course_Utilities::getNextLessonButton(); ?>	
					</nav>
				</div>

			</section>

			<?php 
			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) {
				comments_template();
			// }
			?>

			<?php do_action( ZC_THEME_NAMESPACE . '_content_after' ); ?>

		</article>
	</div>
</div>