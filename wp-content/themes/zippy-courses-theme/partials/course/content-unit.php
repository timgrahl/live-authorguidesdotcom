<?php
	
	global $lesson_class, $post;

	$unit_entries = get_post_meta( $post->ID, 'entries', true );
	$unit_entries = $unit_entries ? explode( ',', $unit_entries ) : array();

?>
<article <?php post_class( 'course-unit row' ); ?>>
	
	<header class="unit-header">
		<a id="unit-<?php the_ID(); ?>" name="unit-<?php the_ID(); ?>"></a>
		<a href="<?php echo get_permalink( $post->ID ); ?>"><?php the_title( '<h3 class="entry-title unit-title">', '</h3>' ); ?></a>
		<a class="show-hide-unit-lessons">[Show Lessons]</a>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		
		get_template_part( 'partials/unit', 'lessons' );
		
		if( empty( $unit_entries ) ) {
			echo '<div class="row"><div class="col-sm-12"><p>There are no lessons in this unit yet.</p></div></div>';
		}

		?>
	</div>

</article>