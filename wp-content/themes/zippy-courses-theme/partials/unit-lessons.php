<?php

global $post;

$unit_entries = get_post_meta( $post->ID, 'entries', true );
$unit_entries = $unit_entries ? explode( ',', $unit_entries ) : array();

echo '<div class="unit-lessons">';
foreach( $unit_entries as $unit_entry_id ) {

	if( get_post_status( $unit_entry_id ) == 'publish' ) {

		$unit_entry = get_post( $unit_entry_id );
		$post = $unit_entry;

		setup_postdata( $post );
		
		get_template_part( 'partials/lesson', 'excerpt' );

		wp_reset_postdata();

	}

}
echo '</div>';