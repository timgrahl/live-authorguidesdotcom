<?php 

global $post; 

wpcs_course_header( $post->post_parent );
?>

<div class="container">
	<div class="row">
		<article <?php post_class( 'post_box  col-sm-10 col-sm-offset-1' ); ?>>

			<header class="entry-header row">
				<div class="col-sm-12">
					<?php
						if ( is_single() || is_page() ) :
							the_title( '<h2 class="entry-title"><span class="post-type-indicator">' . apply_filters( 'zippy_theme_post_type_indicator', 'Unit' ) . '</span>', '</h2>' );
						else :
							the_title( '<h2 class="entry-title"><span class="post-type-indicator">' . apply_filters( 'zippy_theme_post_type_indicator', 'Unit' ) . '</span><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						endif;

					?>

				</div>
			</header><!-- .entry-header -->

			<?php do_action( ZC_THEME_NAMESPACE . '_content_before' ); ?>
			<section class="entry-content row">
				<div class="col-sm-10 col-sm-offset-1">
				<?php 
					
					do_action( ZC_THEME_NAMESPACE . '_content_top' );
					
					the_content();

					do_action( ZC_THEME_NAMESPACE . '_content_bottom' ); 
				?>
				</div>

			</section>

			<?php do_action( ZC_THEME_NAMESPACE . '_content_after' ); ?>
			<?php 
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
			?>
		</article>
	</div>
</div>