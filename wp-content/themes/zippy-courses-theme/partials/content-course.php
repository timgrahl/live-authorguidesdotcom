<?php wpcs_course_header(); ?>

<article <?php post_class( 'post_box entry-content grid-view' ); ?>>
	<div class="container">
		
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
			<?php the_content(); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
						
				<?php
				
				$entries = get_post_meta( $post->ID, '_lessons-and-units_order', true );
				$entries = explode( ',', $entries );

				?>

				<div class="course-entries">
				<h2 class="course-entries-heading">Units &amp; Lessons</h2>
				<?php
				if( Course_Access::is_pending( $post->ID) ) {
					echo '<p><strong>Access is pending</strong></p>';	
				} else {

					foreach( $entries as $entry_id ) :

						$entry = get_post( $entry_id, OBJECT );
						$classes = get_post_class( '', $entry_id );
						
						switch( $entry->post_type ) {
							
							case 'unit' :
								
									$post = $entry;						
									setup_postdata( $post );

									get_template_part( 'partials/course/content', 'unit' );	

									wp_reset_postdata();

								break;

							case 'lesson' : 

								if( Course_Access::has_tier_access( $entry->ID ) && get_post_status( $entry->ID ) == 'publish' ) {

									$post = $entry;						
									setup_postdata( $post );

									get_template_part( 'partials/lesson', 'excerpt' );

									wp_reset_postdata();

								} else {

								}

								break;

							default: 	
								break;

						}

					endforeach;

				}

				?>
				</div>
			</div>
		</div>
	</div>
</article>