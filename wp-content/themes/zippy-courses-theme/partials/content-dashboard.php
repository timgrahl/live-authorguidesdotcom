<div class="row">
	<article class="entry-content col-sm-8 col-sm-offset-2">
		<?php
			the_content();
		?>
	</article>
</div>

<div class="row">
	<article class="entry-content col-sm-12">
		<?php echo do_shortcode( '[dashboard]' ) ?>
	</article>
</div>
