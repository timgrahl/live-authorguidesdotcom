<?php
// Add typekit code to <head>
add_action('wp_head', 'typekit_wp_head');
function typekit_wp_head(){
    ?>
	<script src="//use.typekit.net/yul1bwz.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '1516579138638243');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=1516579138638243&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
    <?php
}

// This setups scripts for the AJAX call I make when someone clicks the link to show all of the lessons.
function enqueue_scripts_styles_init() {
	wp_enqueue_script( 'ajax-script', get_stylesheet_directory_uri().'/js/script.js', array('jquery'), 1.0 ); // jQuery will be included automatically
	wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) ); // setting ajaxurl
	wp_enqueue_style( 'printcss', get_stylesheet_directory_uri() . "/print.css", array(), '1.0', 'print' );
	wp_enqueue_script( 'tooltipster', get_stylesheet_directory_uri().'/js/jquery.tooltipster.min.js', array(), 1.0 );
	wp_enqueue_style( 'tooltipstercss', get_stylesheet_directory_uri() . "/css/tooltipster.css", array(), '1.0', 'screen' );
}
add_action('init', 'enqueue_scripts_styles_init');

// Add some JS to the footer for the Ninja Forms textarea hack, plus my ajax call.
//
// For ninja forms, this allows people to leave notes on each individual lesson. By default, I'm hiding all of the 
// fields. This js shows the field that is labeled with the current post.
add_action('wp_footer', 'lessonshow_wp_foot');
function lessonshow_wp_foot(){
	global $wp_query;
	
	if ( !isset($post_id) ) {
		$post_id = $wp_query->post->ID;
    }
    ?>
<script type="text/javascript">
	jQuery(document).ready(function(){ 
		// Hiding all of the NinjaForms Fields
		jQuery('.lesson-<?php echo $post_id; ?>-wrap').css({ display: "block" });
		
		jQuery('.postid-110 #ninja_forms_form_7 .textarea-wrap').css({ display: "block" });
		
		// Tracking completed lesson click
		jQuery(".zippy-social-triggers-lesson-complete-button").click(function() {
			analytics.track('Lesson Completed', {
			  lessonId: '<?php echo get_the_ID(); ?>',
			  lessonName: '<?php echo get_the_title(); ?>'
			});
		});
		
		// Track Lesson Feedback NinjaForm submit
		jQuery("#ninja_forms_field_10").click(function() {
			analytics.track('Lesson Feedback Submitted', {
			  lessonId: '<?php echo get_the_ID(); ?>',
			  lessonName: '<?php echo get_the_title(); ?>'
			});
		});
		
		//Tooltipster activition!
		jQuery('.tooltip').tooltipster();
	});
	<?php
	
	// There is a link underneath the main intro video that allows people to show
	// all of the lessons instead of just their roadmap. I wanted to save it when people
	// click that link so they don't have to click it everytime.
	//
	// This code checks to see if a user meta entry exists. If it does, it will show all of
	// the lessons. If not, it will make an ajax call when someone clicks the link to show all
	// lessons that will save the user meta.
	global $current_user;
	$current_user = wp_get_current_user();
	
	$showlessons = get_user_meta($current_user->ID, 'show_all_lessons', true);
	
	if(empty($showlessons)) {
	?>
	jQuery(function($){
		jQuery('a.showall').click(function () {
			jQuery('a.showall').hide();
			jQuery('.zippy-course-entries').show();
			$.post(ajax_object.ajaxurl, {
				action: 'save_show_all_lessons',
				//post_id: $(this).find('input.post_id').attr('value')
			}, function(data) {
				//alert(data); // alerts 'ajax submitted'
			});
		});
	});
	<?php 
	} else {
	?>
		jQuery('a.showall').hide();
		jQuery('.post-19 .zippy-course-entries').show();
    <?php
	}
	?>
	
	jQuery(function($){
		jQuery('a.roadmapreset').click(function () {
			jQuery.post(ajax_object.ajaxurl, {
				action: 'roadmap_reset',
				//post_id: $(this).find('input.post_id').attr('value')
			}, function(data) {
				//alert(data); // alerts 'ajax submitted'
				location.reload();
				return false;
			});
		});
	});
	</script>
	<?php
}

// Puts a big admin notice on the live site so I only make local changes
add_action( 'wp_before_admin_bar_render', 'timgrahldotcom_notice_admin_bar_render' ); 
function timgrahldotcom_notice_admin_bar_render() {
	global $wp_admin_bar;
	if (get_option('siteurl') == 'http://authorguides.com') {
		$wp_admin_bar->add_menu( array(
			'parent' => false, // use 'false' for a root menu, or pass the ID of the parent menu
			'id' => 'timgrahldotcom-alert', // link ID, defaults to a sanitized title value
			'title' => __("<span style='color:red;'>YOU ARE ON AUTHORGUIDES.COM! NO CHANGES!!!</span>"), // link title
			'href' => admin_url( 'options-reading.php' ), // name of file
		));
	}
}

// It wasn't showing the lesson downloads by default. This forces it too.
function lesson_downloads_func(){
	$filters = Course_Filters::get_instance();
	return $filters->lesson_downloads('');
}
add_shortcode( 'lesson_downloads', 'lesson_downloads_func' );



// This is the part that processes the intake survey and saves which
// roadmap they should be on.
function ninja_forms_register_example(){
  add_action( 'ninja_forms_post_process', 'ninja_forms_example' );
}
add_action( 'init', 'ninja_forms_register_example' );

function ninja_forms_example(){
  global $ninja_forms_processing;
  
  $formID = $ninja_forms_processing->get_form_ID();
  $action = $ninja_forms_processing->get_action();
  
  // IF it is the student survey
  if($formID == 8 and $action == 'submit') { 
  //Get all the user submitted values
	  $all_fields = $ninja_forms_processing->get_all_fields();
      $mystring = $userid = '';
	  $picker['longgame'] = $picker['list'] = $picker['influencer'] = $picker['bestseller'] = 0;
	  if( is_array( $all_fields ) ){ //Make sure $all_fields is an array.
	    //Loop through each of our submitted values.
	    foreach( $all_fields as $field_id => $user_value ){
			// Q: How much time do you have until your book launch?
	    	if($field_id == 38) {
	    		if(strcmp($user_value,'Less than 6 months') == 0) {
	    			$picker['longgame'] += 1;
					$picker['list'] += 1;
					$picker['bestseller'] += -1;
					$picker['influencer'] += -1;
	    		} else if(strcmp($user_value,'More than 6 months') == 0) {
					$picker['influencer'] += 2;
					$picker['bestseller'] += 1;
				} else {
					$picker['longgame'] += 1;
					$picker['bestseller'] += -2;
					$picker['influencer'] += -2;
					$picker['list'] += -2;
				}
	    	}
			// Q: How big is your email list?
	    	if($field_id == 40) {
	    		if(strcmp($user_value,'More than 500') == 0) {
	    			$picker['bestseller'] += 1;
					$picker['list'] += 1;
	    		} else {
					$picker['influencer'] += 1;
					$picker['longgame'] += 1;
				}
	    	}
			// Q: Are you connected to other Influencers?
	    	if($field_id == 41) {
	    		if(strcmp($user_value,'Yes') == 0) {
	    			$picker['bestseller'] += 1;
					$picker['influencer'] += 1;
	    		} else {
					$picker['list'] += 1;
					$picker['longgame'] += 1;
	    			$picker['bestseller'] += -1;
	    			$picker['influencer'] += -1;
				}
	    	}
			if($field_id == 44) {
				$userid = $user_value;
			}
	    }
		
		//sorts the array based on the highest number and adds it as user meta
		arsort($picker);
		reset($picker);
		$launchtype = key($picker);
		update_user_meta( $userid, 'launch_type', $launchtype );
		if($launchtype == 'bestseller') {
			global $current_user;
			$current_user = wp_get_current_user();
			update_user_meta($current_user->ID, 'show_all_lessons', 'true');
		}
		
		
		/* This stuff was for testing.
		
		foreach ($picker as $key => $val) {
		    $mystring .= "$key = $val\n";
		}
		
		mail('tim@outthinkgroup.com','Testing Ninja Forms', $mystring . "\n\nUser: " . $userid );
		*/
	  }
  }
}


function getcoursenotes_shortcode( $att ) {
	global $current_user;
	$current_user = wp_get_current_user();
	
	$userid = $current_user->ID;
	
	$args = array(
	  'form_id'   => 7,
	  'user_id'   => $userid,
	);
	ob_start();
	// This will return an array of sub objects.
	$subs = Ninja_Forms()->subs()->get( $args );
	echo "<h1 class='printnotes' style='display:none;'>My Launch a Bestseller Notes</h1>";
	// This is a basic example of how to interact with the returned objects.
	// See other documentation for all the methods and properties of the submission object.
	foreach ( $subs as $sub ) {
	  $form_id = $sub->form_id;
	  $user_id = $sub->user_id;
	  // Returns an array of [field_id] => [user_value] pairs
	  $all_fields = $sub->get_all_fields();
	  //print_r($all_fields);
	  foreach($all_fields as $key => $field) {
		  if(!empty($field) and $key != 22 and $key != 23 and $key != 24) {
		  	$finfo = ninja_forms_get_field_by_id($key);
			echo "<div class='anote'><h3>" . $finfo['data']['label'] . "</h3><div>".apply_filters('be_the_content',$field)."</div></div>";
		  }
	  }
	  // Echoes out the submitted value for a field
	  //echo $sub->get_field( 34 );
	}
	return ob_get_clean();
}
add_shortcode( 'getcoursenotes', 'getcoursenotes_shortcode' );

// This shortcode checks to see which launch type has been saved for them and then grabs
// the right content from the pages I created. If they haven't taken the intake survey, it
// shows the survey.
function getstarted_shortcode( $atts ) {
	global $current_user;
	$current_user = wp_get_current_user();
	
	$userid = $current_user->ID;
	$launch_type = get_user_meta($userid, 'launch_type', true);
	$launchpageid = 0;

	if(strcmp($launch_type,'bestseller') == 0) {
		$launchpageid = 130;
	} else if(strcmp($launch_type,'list') == 0) {
		$launchpageid = 132;
	} else if(strcmp($launch_type,'influencer') == 0) {
		$launchpageid = 134;
	} else if(strcmp($launch_type,'longgame') == 0) {
		$launchpageid = 136;
	}
	
	if($launchpageid > 0) {
		$launchpage = get_post($launchpageid);
		$mycontent = apply_filters('be_the_content',$launchpage->post_content);
		ob_start();
		if(strcmp($launch_type,'bestseller') != 0) {
		?>
		<h2>Your Custom Roadmap</h2>

		<p>Based on the results of your survey, I have created this custom roadmap to help you get the most out of the course for your launch.</p>

		<p>I highly recommend you start with the lessons outlined below.</p>
		<?php } ?>
		<h2><?php echo $launchpage->post_title; ?></h2>
		<?php
		echo $mycontent;
		?>
		<a href="#" class="roadmapreset tooltip" title="Click to reset your roadmap and retake the survey."><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-reset.png" alt="Reset Roadmap" /></a>
		<?php
		return ob_get_clean();
	} else {
		ob_start();
		?>
		<div class="intake-survey">
			<h2>Introduction Survey</h2>
			<p>Take this short survey to build a customized roadmap through Launch a Bestseller.</p>
			<?php
			if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 8 ); }
			?>
		</div>
		<?php
		return ob_get_clean();
	}

}
add_shortcode( 'getstarted', 'getstarted_shortcode' );

// A shortcode to I don't have to keep copy/pasting the wistia embed code. Also, this allows me to
// tweak the embed code everywhere at once.
function wistia_shortcode( $atts ) {
	// Attributes
	$atts = shortcode_atts(array('videoid' => ''), $atts );
	ob_start();
	?>
	<div class="videowrapper" style="max-width:100%;">
		<iframe src="//fast.wistia.net/embed/iframe/<?php echo $atts['videoid']; ?>?videoFoam=true" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="817" height="511"></iframe><script src="//fast.wistia.net/assets/external/E-v1.js"></script>

		<script charset="ISO-8859-1" src="http://fast.wistia.com/static/concat/iframe-api-v1.js"></script>
	</div>
	<?php
	return ob_get_clean();
}
add_shortcode( 'wistia', 'wistia_shortcode' );

// These build the individual roadmaps. The $launchids array hold all of the post
// ids.
function lablaunch_shortcode( $atts ) {
	global $wpdb;
	// Attributes
	$atts = shortcode_atts(array('launch' => 'bestseller'), $atts );
	ob_start();
	?>
	<div class="zippy-course-entries custom-road-map" style="display:block;">
		<div class="zippy-course-entries-unit zippy-unit">
	    	<div class="col-xs-12">
	<?php
	$launchids = array();
	if($atts['launch'] == 'longgame') {
		$launchids = array(22,23,24,66,25,26,27,67,68,69,97,155,156,158,163);
		?>
		    	<h3 class="zippy-unit-title">Long Game Launch Road Map<span class="zippy-unit-entry-count"><?php echo count($launchids)?> lessons</span><span class="zippy-unit-entries-toggle">+</span></h3>
		    	<div class="zippy-unit-entries">
		        	<div class="col-xs-12">
		<?php
	} else if($atts['launch'] == 'influencer') {
		$launchids = array(22,23,24,66,25,26,27,67,68,69,97,155,156,158,160,161,161,163);
		?>
		    	<h3 class="zippy-unit-title">Influencer Launch Road Map<span class="zippy-unit-entry-count"><?php echo count($launchids)?> lessons</span><span class="zippy-unit-entries-toggle">+</span></h3>
		    	<div class="zippy-unit-entries">
		        	<div class="col-xs-12">
		<?php
	} else if($atts['launch'] == 'list') {
		$launchids = array(140,142,143,144,145,146,147,148,149,150,151,152,153,155,156,157,158,160,161,162,163);
		?>
		    	<h3 class="zippy-unit-title">List Launch Road Map<span class="zippy-unit-entry-count"><?php echo count($launchids)?> lessons</span><span class="zippy-unit-entries-toggle">+</span></h3>
		    	<div class="zippy-unit-entries">
		        	<div class="col-xs-12">
		<?php
	}
	foreach($launchids as $lid) {
		$lpage = get_post($lid);
		$result = $wpdb->get_var( "SELECT post_id FROM $wpdb->postmeta WHERE `meta_key` LIKE 'entries' AND `meta_value` LIKE '%".$lid."%'" );
		?>
					<div class="zippy-lesson">
						<div class="col-sm-12">
							<div class="zippy-lesson-title">
								<a href="<?php echo get_permalink($lid); ?>" class="zippy-lesson-title"><?php echo $lpage->post_title; ?></a> <span class="launchcat">/ <?php echo get_the_title( $result ); ?></span>
							</div>
						</div>
					</div>
		<?php			
	}
	?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a class="showall">Click Here to access all of the lessons in <em>Launch a Bestseller</em> at any time.</a>
	<?php
	return ob_get_clean();
}
add_shortcode( 'lablaunch', 'lablaunch_shortcode' );

// Added the wistia shortcode as a button in the editor
add_action('admin_print_footer_scripts','eg_quicktags');
function eg_quicktags() {
?>
<script type="text/javascript" charset="utf-8">
/* Adding Quicktag buttons to the editor Wordpress ver. 3.3 and above
* - Button HTML ID (required)
* - Button display, value="" attribute (required)
* - Opening Tag (required)
* - Closing Tag (required)
* - Access key, accesskey="" attribute for the button (optional)
* - Title, title="" attribute (optional)
* - Priority/position on bar, 1-9 = first, 11-19 = second, 21-29 = third, etc. (optional)
*/
QTags.addButton( 'eg_wistia', 'wistia', '[wistia videoid=]');
</script>
<?php
}

// this is the bit of code that gets called by the ajax script above. It sets the user meta entry.
function ajax_save_show_all_lessons() {
	global $current_user;
	$current_user = wp_get_current_user();
	update_user_meta($current_user->ID, 'show_all_lessons', 'true');
	die(); // stop executing script
}
add_action( 'wp_ajax_save_show_all_lessons', 'ajax_save_show_all_lessons' ); // ajax for logged in users
add_action( 'wp_ajax_nopriv_save_show_all_lessons', 'ajax_save_show_all_lessons' ); // ajax for not logged in users

// this is the bit of code that gets called by the ajax script above. It sets the user meta entry.
function ajax_roadmap_reset() {
	global $current_user;
	$current_user = wp_get_current_user();
	
	if ( ! delete_user_meta($current_user->ID, 'launch_type') ) {
  	  echo "Ooops! Error while deleting this information!";
	} else {
		echo "Success!";
	}
	if ( ! delete_user_meta($current_user->ID, 'show_all_lessons') ) {
  	  echo "Ooops! Error while deleting this information!";
	} else {
		echo "Success!";
	}
	exit;
}
add_action( 'wp_ajax_roadmap_reset', 'ajax_roadmap_reset' ); // ajax for logged in users
add_action( 'wp_ajax_nopriv_roadmap_reset', 'ajax_roadmap_reset' ); // ajax for not logged in users


add_shortcode( 'getstarted', 'getstarted_shortcode' );

// A shortcode to I don't have to keep copy/pasting the wistia embed code. Also, this allows me to
// tweak the embed code everywhere at once.
function zippydata_shortcode( $atts ) {
	global $wpdb;
	
	ob_start();
	$zdresults = $wpdb->get_results("SELECT * FROM srh37th_usermeta WHERE meta_key = 'completed_entries'");
	foreach($zdresults as $zd) {
		$zdstacks = unserialize($zd->meta_value);
		if(!empty($zdstacks)) {
			foreach($zdstacks as $zdkey => $zdstacks) {
				if($zdkey == 19) {
					$zdcounts = $wpdb->get_results("SELECT DISTINCT(ua.item_id) FROM srh37th_zippy_user_activity as ua WHERE ua.type = 'view' AND ua.user_id = '".$zd->user_id."'");
					if($wpdb->num_rows > 34) {
						$user_info = get_userdata($zd->user_id);
						echo $user_info->user_email . "<br />";
					}
				}
			}
		}
	}
	
	?>
	
	<?php
	return ob_get_clean();
}
add_shortcode( 'zippydata', 'zippydata_shortcode' );


// There was a punch of erroneous stuff tied to the the_content filter, so I created a
// new, base one.
add_filter( 'be_the_content', 'wptexturize'        );
add_filter( 'be_the_content', 'convert_chars'      );
add_filter( 'be_the_content', 'wpautop'            );
add_filter( 'be_the_content', 'shortcode_unautop'  );
add_filter( 'be_the_content', 'do_shortcode'       );

?>