<?php
/*
Template Name: Login
*/

global $post;

$zippy_social_triggers      = ZippySocialTriggers::instance();

get_template_part('partials/headers/header', 'login');
?>

<div class="login-form-container">
    <h1 class="site-title text-center"><?php echo $zippy_social_triggers->getLoginPageLogo(); ?></h1>

    <p class="signin-prompt">Sign into <?php bloginfo('name'); ?></p>

    <?php get_template_part('partials/content/entry', 'content'); ?>

    <?php 
        echo do_shortcode('[zippy_login_form]');
	?>

	<br /><br />
	<span style="color:white;">Problems? Questions? Email <a href="mailto:support@authorguides.com">support@authorguides.com</a>.</span>

    <?php if($zippy_social_triggers->hasInfoURL()) : ?>
    <div class="not-a-member">
        <h3>Not a member of <?php bloginfo('name'); ?>?</h3>
        <p><a href="<?php echo $zippy_social_triggers->getInfoURL(); ?>">Click here to find out more</a></p>
    </div>
<?php endif; ?>
</div>

<?php
get_footer();
