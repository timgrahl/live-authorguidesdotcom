<?php

// require_once("../vendor/autoload.php");
require_once("../lib/easypost.php");

\EasyPost\EasyPost::setApiKey('qKGLdpCoU82NL4FgSdtINQ');

// create addresses
$to_address_params = array("name"    => $_POST['name'],
                           "street1" => $_POST['address1'],
                           "street2" => $_POST['address2'],
                           "city"    => $_POST['city'],
                           "state"   => $_POST['state'],
                           "zip"     => $_POST['zip']);
$to_address = \EasyPost\Address::create($to_address_params);

$from_address_params = array("name"    => $_POST['sender'],
                             "street1" => "3412 Benham Ave",
                             "street2" => "",
                             "city"    => "Nashville",
                             "state"   => "TN",
                             "zip"     => "37215",
                             "phone"   => "434-426-7704");
$from_address = \EasyPost\Address::create($from_address_params);


// create parcel (member package)
/*
$parcel_params = array("length"             => 8.5,
                       "width"              => 11,
                       "height"             => .5,
                       "predefined_package" => null,
                       "weight"             => 26
);
*/

// create parcel (YF1C)
$parcel_params = array("length"             => $_POST['length'],
                       "width"              => $_POST['width'],
                       "height"             => $_POST['height'],
                       "predefined_package" => null,
                       "weight"             => $_POST['weight']
);
$parcel = \EasyPost\Parcel::create($parcel_params);

$shipment = \EasyPost\Shipment::create(array(
    "from_address" => $from_address,
    "to_address" => $to_address,
    "parcel" => $parcel
  ));

  // reference an element of $shipment->rates or
  // $shipment->lowest_rate() returns the lowest rate
  $whatrate = "!LibraryMail" . $_POST['mailrate'];
$shipment->buy($shipment->lowest_rate(null, $whatrate));

echo "<img src='".$shipment->postage_label->label_url."' width='400' />";
